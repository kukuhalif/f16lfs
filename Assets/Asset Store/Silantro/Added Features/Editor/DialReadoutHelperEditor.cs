#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
//using UnityEditorInternal;

[CustomEditor(typeof(DialReadoutHelper))]
public class DialReadoutHelperEditor : Editor
{
    Color backgroundColor;
    DialReadoutHelper dialReadoutHelper;
    Animator animator;
    AnimatorController animatorController;
    bool showArray = true;
    //bool showElement = false;
    UnityEditorInternal.ReorderableList rList;

    private void OnEnable()
    {
        dialReadoutHelper = (DialReadoutHelper)target;
        dialReadoutHelper.animator = dialReadoutHelper.GetComponent<Animator>();
        animator = dialReadoutHelper.animator;
        animatorController = animator.runtimeAnimatorController as AnimatorController;

        /*if (animator != null)
        {
            // Get animator parameters
            AnimatorControllerParameter[] animParams = animatorController.parameters;
            foreach (AnimatorControllerParameter param in animParams)
            {
                DialReadoutHelper.DialReadoutData dialReadoutData = new DialReadoutHelper.DialReadoutData();
                dialReadoutData.paramName = param.name;
                dialReadoutHelper.dialReadoutDatas.Add(dialReadoutData);
            }
        }*/
    }

    public override void OnInspectorGUI()
    {
        backgroundColor = GUI.backgroundColor;

        serializedObject.Update();

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        GUILayout.Space(2f);
        GUI.color = Color.yellow;
        EditorGUILayout.HelpBox("Animation Parameters", MessageType.None);
        GUI.color = backgroundColor;
        GUILayout.Space(2f);

        if (animatorController != null)
        {
            // Get animator parameters
            AnimatorControllerParameter[] animParams = animatorController.parameters;
            string[] animParamNames = new string[animParams.Length];

            showArray = EditorGUILayout.Foldout(showArray, new GUIContent("Dial Readout List"));
            if (showArray)
            {
                EditorGUI.indentLevel++;
                var paramIdx = 0;
                foreach (AnimatorControllerParameter param in animParams)
                {
                    animParamNames[paramIdx] = param.name;
                    EditorGUILayout.LabelField("Param "+paramIdx.ToString(), param.name);
                    paramIdx++;
                }
                EditorGUI.indentLevel--;
            }

            /*
            showArray = EditorGUILayout.Foldout(showArray, new GUIContent("Dial Readout List"));
            if (showArray)
            {
                EditorGUI.indentLevel++;
                var iterator = serializedObject.FindProperty("dialReadoutDatas").GetEnumerator();
                var idx = 0;
                while (iterator.MoveNext())
                {
                    //var rect = GUILayoutUtility.GetRect(0f, 16f);
                    //var current = iterator.Current as SerializedObject;
                    var current = iterator.Current as SerializedProperty;
                    showElement = EditorGUILayout.Foldout(showElement, new GUIContent("Element"));
                    if (showElement)
                    {
                        EditorGUI.indentLevel++;
                        //var paramName = current.FindProperty('paramName');
                        dialReadoutHelper.dialReadoutDatas[idx].paramIdx = EditorGUILayout.Popup("Animation Parameter", dialReadoutHelper.dialReadoutDatas[idx].paramIdx, animParamNames);
                        dialReadoutHelper.dialReadoutDatas[idx].paramName = animParamNames[dialReadoutHelper.dialReadoutDatas[idx].paramIdx];
                        //EditorGUILayout.PropertyField(current.FindPropertyRelative("paramName"), new GUIContent("Parameter Name"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("readoutTexts"), new GUIContent("Readout Texts"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("useReadoutDigits"), new GUIContent("User Readout Digits"));

                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitOneContainer"), new GUIContent("Digit One Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitTwoContainer"), new GUIContent("Digit Two Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitThreeContainer"), new GUIContent("Digit Three Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitFourContainer"), new GUIContent("Digit Four Container"));

                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitOneContainer"), new GUIContent("Digit One Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitTwoContainer"), new GUIContent("Digit Two Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitThreeContainer"), new GUIContent("Digit Three Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitFourContainer"), new GUIContent("Digit Four Container"));

                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitOneContainer"), new GUIContent("Digit One Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitTwoContainer"), new GUIContent("Digit Two Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitThreeContainer"), new GUIContent("Digit Three Container"));
                        EditorGUILayout.PropertyField(current.FindPropertyRelative("digitFourContainer"), new GUIContent("Digit Four Container"));

                        EditorGUI.indentLevel--;
                    }
                    idx++;
                }

            EditorGUI.indentLevel--;
            }
            */        
        }

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    private void getAnimatorParameters()
    {
        if (animator != null)
        {
            // Get animator parameters
            AnimatorControllerParameter[] animParams = animatorController.parameters;
            int idx = 0;
            foreach (AnimatorControllerParameter param in animParams)
            {
                DialReadoutHelper.DialReadoutData dialReadoutData = new DialReadoutHelper.DialReadoutData();
                dialReadoutData.paramName = param.name;
                dialReadoutHelper.dialReadoutDatas.Add(dialReadoutData);
                idx++;
            }
        }

    }
}

#endif
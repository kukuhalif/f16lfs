using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisplayUnitBehavior : MonoBehaviour
{
    public MeshRenderer meshRenderer;
    public List<Material> materials = new List<Material>();
    public Camera cockpitCamera;
    int matIdx = 0;
    //bool clicked = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject()) // prevent unintended clicks
        {
            if (Input.GetMouseButtonUp(0))
            {
                //int layerMask = 1 << 2;
                //layerMask = ~layerMask;
                if (cockpitCamera != null)
                {
                    RaycastHit hit;
                    Ray ray = cockpitCamera.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray, out hit, 100.0f)) // layerMask))
                    {
                        if (hit.collider.transform.name == meshRenderer.gameObject.name)
                        {
                            if (materials.Count > 0)
                            {
                                matIdx++;
                                if (matIdx == materials.Count) 
                                    matIdx = 0; // rotate material
                                if (meshRenderer != null)
                                    meshRenderer.material = materials[matIdx];
                                else
                                    Debug.LogError("Display Unit MeshRenderer cannot be empty!");
                            }
                        }
                    }
                }
                else
                    Debug.LogError("Display Unit Camera cannot be empty!");
            }
        }
    }

    /*private void OnMouseDown()
    {
        Debug.Log("Click "+gameObject.name);
        if (materials.Count > 0)
        {
            matIdx++;
            if (matIdx == materials.Count) matIdx = 0;
            GetComponent<MeshRenderer>().material = materials[matIdx];
        }
    }*/
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


[RequireComponent(typeof(Camera))]
public class TerrainCamera : MonoBehaviour
{
    private Camera terrainCamera;
    //private float cameraHeading;

    public enum TerrainCameraType { TopDownView, SyntheticTerrainView }
    public TerrainCameraType terrainCameraType = TerrainCameraType.TopDownView;
    //public bool isTerrainCamera = false;
    public Transform aircraft;
    public Light terrainLight;
    public float mapSize = 1000f;
    public float cameraHeight = 1000f;

    // Start is called before the first frame update
    void Start()
    {
        // immediately disconnect with the parent, to enable camera move freely
        transform.SetParent(null, true);

        if (aircraft == null)
        {
            Debug.LogError("Aircraft Gameobject to follow by camera is not defined!");
            return;
        }
        if (terrainLight == null)
        {
            Debug.LogError("Terrain light is not defined!");
            return;
        }
        RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
        RenderPipelineManager.endCameraRendering += OnEndCameraRendering;
        terrainCamera = GetComponent<Camera>();
        switch (terrainCameraType)
        {
            case TerrainCameraType.TopDownView:
                terrainCamera.orthographic = true;
                terrainCamera.orthographicSize = mapSize;
                SetTopDownView();
                break;
            case TerrainCameraType.SyntheticTerrainView:
                terrainCamera.orthographic = false;
                //terrainCamera.SetReplacementShader(Shader.Find("Custom/WireFrame"), null);

                //UnityEngine.Rendering.Universal.UniversalAdditionalCameraData additionalCameraData = GetComponent<UnityEngine.Rendering.Universal.UniversalAdditionalCameraData>();
                //additionalCameraData.SetRenderer(1);
                SetSyntheticTerrainView();
                break;
        }
    }

    void OnDestroy()
    {
        RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
        RenderPipelineManager.endCameraRendering -= OnEndCameraRendering;
    }

    // Update is called once per frame
    void Update()
    {
        switch (terrainCameraType)
        {
            case TerrainCameraType.TopDownView:
                SetTopDownView();
                break;
            case TerrainCameraType.SyntheticTerrainView:
                SetSyntheticTerrainView();
                break;
        }
    }

    void OnBeginCameraRendering(ScriptableRenderContext context, Camera camera)
    {
        if (camera == terrainCamera)
            terrainLight.enabled = true;
    }

    void OnEndCameraRendering(ScriptableRenderContext context, Camera camera)
    {
        if (camera == terrainCamera)
            terrainLight.enabled = false;
    }

    void SetTopDownView()
    {
        transform.position = new Vector3(aircraft.position.x, cameraHeight, aircraft.position.z);
        Quaternion rotAngleForward = Quaternion.FromToRotation(Vector3.forward, aircraft.transform.forward);
        transform.rotation = Quaternion.Euler(new Vector3(90, rotAngleForward.eulerAngles.y, 0));
    }

    void SetSyntheticTerrainView()
    {
        transform.position = new Vector3(aircraft.position.x, aircraft.position.y + cameraHeight, aircraft.position.z);
        Quaternion rotAngleForward = Quaternion.FromToRotation(Vector3.forward, aircraft.transform.forward);
        transform.rotation = Quaternion.Euler(new Vector3(0, rotAngleForward.eulerAngles.y, aircraft.rotation.eulerAngles.z));
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class DragUI : MonoBehaviour, IDragHandler
{
    private RectTransform rectTransform;
    [SerializeField] Vector2 defaultPosition;
    [SerializeField] float zoomMinSize = 0.5f;
    [SerializeField] float zoomMaxSize = 2.0f;
    [SerializeField] float zoomRate = 5;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        float posX = PlayerPrefs.GetFloat(rectTransform.gameObject.name + ".simui.x", defaultPosition.x);
        float posY = PlayerPrefs.GetFloat(rectTransform.gameObject.name + ".simui.y", defaultPosition.y);
        float scaleX = PlayerPrefs.GetFloat(rectTransform.gameObject.name + ".simui.scalex", 1);
        float scaleY = PlayerPrefs.GetFloat(rectTransform.gameObject.name + ".simui.scaley", 1);
        rectTransform.anchoredPosition = new Vector2(posX, posY);
        rectTransform.transform.localScale = new Vector3(scaleX, scaleY, 1);
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += eventData.delta;
    }

    public void OnMouseUp()
    {
        PlayerPrefs.SetFloat(rectTransform.gameObject.name + ".simui.x", rectTransform.anchoredPosition.x);
        PlayerPrefs.SetFloat(rectTransform.gameObject.name + ".simui.y", rectTransform.anchoredPosition.y);
        PlayerPrefs.SetFloat(rectTransform.gameObject.name + ".simui.scalex", rectTransform.transform.localScale.x);
        PlayerPrefs.SetFloat(rectTransform.gameObject.name + ".simui.scaley", rectTransform.transform.localScale.y);
        PlayerPrefs.Save();
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            if (EventSystem.current.currentSelectedGameObject != null)
            {
                if (EventSystem.current.currentSelectedGameObject.name == gameObject.name)
                {
                    if (Input.GetAxis("Mouse ScrollWheel") > 0f) // backward
                    {
                        // zoom out
                        float rate = 1 + zoomRate * Time.unscaledDeltaTime;
                        SetZoom(Mathf.Clamp(rectTransform.transform.localScale.y * rate, zoomMinSize, zoomMaxSize));
                    }
                    else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // forward
                    {
                        // zoom in
                        float rate = 1 + zoomRate * Time.unscaledDeltaTime;
                        SetZoom(Mathf.Clamp(rectTransform.transform.localScale.y / rate, zoomMinSize, zoomMaxSize));
                    }
                }
            }
        }
    }

    private void SetZoom(float targetSize)
    {
        rectTransform.transform.localScale = new Vector3(targetSize, targetSize, 1);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DialReadoutHelper : MonoBehaviour
{
    [System.Serializable]
    public class DialReadoutData
    {
        [HideInInspector] public int paramIdx = 0;
        public string paramName = "";
        public TMPro.TMP_Text[] readoutTexts;
        public string readoutTextFormat = "F0";
        public bool useReadoutDigits = false;
        [ShowWhen("useReadoutDigits")] public Transform digitOneContainer;
        [ShowWhen("useReadoutDigits")] public Transform digitTwoContainer;
        [ShowWhen("useReadoutDigits")] public Transform digitThreeContainer;
        [ShowWhen("useReadoutDigits")] public Transform digitFourContainer;
        [ShowWhen("useReadoutDigits")] public float digitOneTranslation;
        [ShowWhen("useReadoutDigits")] public float digitTwoTranslation;
        [ShowWhen("useReadoutDigits")] public float digitThreeTranslation;
        [ShowWhen("useReadoutDigits")] public float digitFourTranslation;
        [ShowWhen("useReadoutDigits")] public float digitOneModulo;
        [ShowWhen("useReadoutDigits")] public float digitTwoModulo;
        [ShowWhen("useReadoutDigits")] public float digitThreeModulo;
        [ShowWhen("useReadoutDigits")] public float digitFourModulo;
        [ShowWhen("useReadoutDigits")] public float digitOneDivider;
        [ShowWhen("useReadoutDigits")] public float digitTwoDivider;
        [ShowWhen("useReadoutDigits")] public float digitThreeDivider;
        [ShowWhen("useReadoutDigits")] public float digitFourDivider;
        [ShowWhen("useReadoutDigits")] [ReadOnly] public Vector3 digitOneBasePosition;
        [ShowWhen("useReadoutDigits")] [ReadOnly] public Vector3 digitTwoBasePosition;
        [ShowWhen("useReadoutDigits")] [ReadOnly] public Vector3 digitThreeBasePosition;
        [ShowWhen("useReadoutDigits")] [ReadOnly] public Vector3 digitFourBasePosition;
    }

    public Animator animator;
    public List<DialReadoutData> dialReadoutDatas = new List<DialReadoutData>();

    void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    private void Start()
    {
        // Init digit readouts
        foreach (DialReadoutData dialReadoutData in dialReadoutDatas)
        {
            if (dialReadoutData.digitOneContainer != null) 
            { 
                dialReadoutData.digitOneBasePosition = dialReadoutData.digitOneContainer.localPosition; 
            }
            if (dialReadoutData.digitTwoContainer != null) 
            { 
                dialReadoutData.digitTwoBasePosition = dialReadoutData.digitTwoContainer.localPosition; 
            }
            if (dialReadoutData.digitThreeContainer != null) 
            { 
                dialReadoutData.digitThreeBasePosition = dialReadoutData.digitThreeContainer.localPosition; 
            }
            if (dialReadoutData.digitFourContainer != null) 
            { 
                dialReadoutData.digitFourBasePosition = dialReadoutData.digitFourContainer.localPosition; 
            }
        }
    }

    void Update()
    {
        foreach (DialReadoutData dialReadoutData in dialReadoutDatas)
        {
            var currentValue = animator.GetFloat(dialReadoutData.paramName);

            // update simple readout text value
            foreach (TMPro.TMP_Text readoutText in dialReadoutData.readoutTexts)
            {
                readoutText.text = currentValue.ToString(dialReadoutData.readoutTextFormat);
            }
            // update readout digits
            if (dialReadoutData.useReadoutDigits)
            {
                //EXTRACT DIGITS
                float digitOne = (currentValue % dialReadoutData.digitOneModulo) / dialReadoutData.digitOneDivider;
                float digitTwo = (currentValue % dialReadoutData.digitTwoModulo) / dialReadoutData.digitTwoDivider;
                float digitThree = (currentValue % dialReadoutData.digitThreeModulo) / dialReadoutData.digitThreeDivider;
                float digitFour = (currentValue % dialReadoutData.digitFourModulo) / dialReadoutData.digitFourDivider;

                //CALCULATE DIAL POSITIONS
                float digitOnePosition = digitOne * -dialReadoutData.digitOneTranslation;

                float digitTwoPosition = Mathf.Floor(digitTwo) * -dialReadoutData.digitTwoTranslation;
                float digitOneLimit = dialReadoutData.digitOneModulo - dialReadoutData.digitOneModulo / 10.0f;
                if ((digitOne * dialReadoutData.digitOneDivider) > digitOneLimit)
                {
                    digitTwoPosition += ((digitOne * dialReadoutData.digitOneDivider) - digitOneLimit) / 10.0f * -dialReadoutData.digitTwoTranslation;
                }

                float digitThreePosition = Mathf.Floor(digitThree) * -dialReadoutData.digitThreeTranslation;
                float digitTwoLimit = dialReadoutData.digitTwoModulo - dialReadoutData.digitOneModulo / 10.0f;
                if ((digitTwo * dialReadoutData.digitTwoDivider) > digitTwoLimit)
                {
                    digitThreePosition += ((digitTwo * dialReadoutData.digitTwoDivider) - digitTwoLimit) / 10.0f * -dialReadoutData.digitThreeTranslation;
                }

                float digitFourPosition = Mathf.Floor(digitFour) * -dialReadoutData.digitFourTranslation;
                float digitThreeLimit = dialReadoutData.digitThreeModulo - dialReadoutData.digitOneModulo / 10.0f;
                if ((digitThree * dialReadoutData.digitThreeDivider) > digitThreeLimit)
                {
                    digitFourPosition += ((digitThree * dialReadoutData.digitThreeDivider) - digitThreeLimit) / 10f * -dialReadoutData.digitFourTranslation;
                }

                //SET POSITIONS
                if (dialReadoutData.digitOneContainer != null) 
                { 
                    dialReadoutData.digitOneContainer.localPosition = dialReadoutData.digitOneBasePosition + new Vector3(0, digitOnePosition, 0); 
                }
                if (dialReadoutData.digitTwoContainer != null) 
                { 
                    dialReadoutData.digitTwoContainer.localPosition = dialReadoutData.digitTwoBasePosition + new Vector3(0, digitTwoPosition, 0); 
                }
                if (dialReadoutData.digitThreeContainer != null) 
                { 
                    dialReadoutData.digitThreeContainer.localPosition = dialReadoutData.digitThreeBasePosition + new Vector3(0, digitThreePosition, 0); 
                }
                if (dialReadoutData.digitFourContainer != null) 
                { 
                    dialReadoutData.digitFourContainer.localPosition = dialReadoutData.digitFourBasePosition + new Vector3(0, digitFourPosition, 0); 
                } 
            }
        }
    }
}

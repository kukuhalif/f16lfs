﻿using System;
using UnityEngine;
using Oyedoyin.Common.Misc;
using Oyedoyin.Mathematics;
using Oyedoyin.FixedWing;
using Oyedoyin.RotaryWing;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

namespace Oyedoyin.Common
{
    #region Component
    /// <summary>
    /// Use:	Handles the movement/rotation of control levers and handles
    /// </summary>
    public class SilantroDial : MonoBehaviour
    {
        public enum DialType
        {
            Speed, VerticalSpeed, Altitude, BankLevel, Compass, ArtificialHorizon, Fuel, RPM, EGT, RadarAltitude
            //AngleOfAttack, Accelerometer, FuelFlow, NozzlePosition, Mach, AirTemperature, Clock, FlapAngle, 
        }
        public DialType m_dialType = DialType.Speed;

        // ------------------------------------- by RF
        public enum SpeedUnit { Knots, FtPMin, MPS } 
        public SpeedUnit speedUnit = SpeedUnit.Knots;
        public SpeedUnit vertSpeedUnit = SpeedUnit.FtPMin; 
        public enum FuelUnit { kiloGrams, Pounds, Gallons }
        public FuelUnit fuelUnit = FuelUnit.kiloGrams;
        public enum AltitudeUnit { Meter, Feet };
        public AltitudeUnit altitudeUnit = AltitudeUnit.Feet;
        public enum AltimeterType { ThreeDigit, FourDigit }
        public AltimeterType altimeterType = AltimeterType.ThreeDigit;
        public enum AttitudeIndicatorType { Classic, GlassInstrument }
        public AttitudeIndicatorType attitudeIndicatorType = AttitudeIndicatorType.Classic;
        // -------------------------------------

        public Controller m_controller;

        public enum AnimationType { Transform, Animator }; // by RF
        public enum MovementType { Rotation, Translation }; // by RF
        public enum EngineType { TurboJet, TurboFan, TurboProp, TurboShaft, PistonEngine }; // by RF
        //public enum AnimatorPlayMode { Simulation, Animation };

        public AnimationType m_animationType = AnimationType.Transform; // by RF
        public MovementType m_movementType = MovementType.Rotation; // by RF
        public EngineType engineType = EngineType.PistonEngine; // by RF
        //public AnimatorPlayMode animatorPlayMode = AnimatorPlayMode.Simulation; // by RF
        public bool forceSimulationMode = false;

        //public string _id = "Base";
        public Transform _needle, _needle2;
        public double _currentValue, _currentValue2, _currentValue3, _currentValue4, _currentValue5, _currentValue6;

        public RotationAxis _axis, _axis2, _axis3, _axis4, _axis5, _axis6;
        public RotationDirection _direction, _direction2, _direction3, _direction4, _direction5, _direction6;
        public double _lag = 0.1;

        public float _minValue = 0f;
        public float _maxValue = 100f;
        public float _minRotation = -180f;
        public float _maxRotation = 180f;
        public float _minPosition = 0f; // by RF
        public float _maxPosition = 100f; // by RF
        public float oneDegreeDistance = 1f; // for horizon glass instrument pitch
        public float movementFactor = -50;

        private Vector3 _rotationAxis, _rotationAxis2, _rotationAxis3;
        private Quaternion _baseRotation;
        private Vector3 _translationAxis; // by RF
        private Vector3 _basePosition; // by RF

        // animation parameters 
        public Animator animDial;

        [Serializable]
        public class AnimLayer
        {
            public int layerIdx, smStateIdx;
            public string layerName, smState;
            public int paramIdx1, paramIdx2, paramIdx3;
            public string param1 = "", param2 = "", param3 = "";
            //public float param1InputMin, param1InputMax, param1OutputMin, param1OutputMax;
            //public float param2InputMin, param2InputMax, param2OutputMin, param2OutputMax;
            //public float param3InputMin, param3InputMax, param3OutputMin, param3OutputMax;
            //public bool param1SyncValue, param2SyncValue, param3SyncValue;
            //public bool param1InvertDirection, param2InvertDirection, param3InvertDirection;
       }
        public AnimLayer animLayer1 = new AnimLayer();
        public AnimLayer animLayer2 = new AnimLayer();

        // RPM data
        public SilantroTurbojet turbojet;
        public SilantroTurbofan turbofan;
        public SilantroTurboprop turboprop;
        public SilantroPiston piston;
        public SilantroTurboshaft turboshaft;

        // Readout digits
        public bool useReadout = true;

        public float digitOne, digitTwo, digitThree, digitFour;

        public float digitOneTranslation;
        public float digitTwoTranslation;
        public float digitThreeTranslation;
        public float digitFourTranslation;

        public float digitSmallestValue;
        public float digitOneModder, digitTwoModder, digitThreeModder, digitFourModder;
        //public float digitOneDivider, digitTwoDivider, digitThreeDivider, digitFourDivider;

        private Vector3 digitOneBasePosition;
        private Vector3 digitTwoBasePosition;
        private Vector3 digitThreeBasePosition;
        private Vector3 digitFourBasePosition;

        public Transform digitOneContainer;
        public Transform digitTwoContainer;
        public Transform digitThreeContainer;
        public Transform digitFourContainer;

        // Text readout
        public TMPro.TMP_Text[] readoutTexts;
        public string readoutTextFormat = "F0";

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            if (m_animationType == AnimationType.Transform)
            {
                if (_needle != null)
                {
                    if (m_movementType == MovementType.Rotation)
                    {
                        _rotationAxis = Handler.EstimateModelProperties(_direction.ToString(), _axis.ToString());
                        _rotationAxis2 = Handler.EstimateModelProperties(_direction2.ToString(), _axis2.ToString());
                        _rotationAxis3 = Handler.EstimateModelProperties(_direction3.ToString(), _axis3.ToString());
                        _baseRotation = _needle.localRotation;
                    }
                    else if (m_movementType == MovementType.Translation)
                    {
                        _translationAxis = Handler.EstimateModelProperties(_direction.ToString(), _axis.ToString());
                        _basePosition = _needle.localPosition;
                    }
                }
            }
            else if (m_animationType == AnimationType.Animator)
            {
                if (animDial != null)
                {
                    if (animLayer1 != null && animLayer1.layerName != "" && animLayer1.smState != "")
                        animDial.Play(animLayer1.layerName + "." + animLayer1.smState);
                    if (animLayer2 != null && animLayer2.layerName != "" && animLayer2.smState != "")
                        animDial.Play(animLayer2.layerName + "." + animLayer2.smState);
                }
            }

            // Readout Digits
            // ------------------------------------- by RF {
            if (digitOneContainer != null) { digitOneBasePosition = digitOneContainer.localPosition; }
            if (digitTwoContainer != null) { digitTwoBasePosition = digitTwoContainer.localPosition; }
            if (digitThreeContainer != null) { digitThreeBasePosition = digitThreeContainer.localPosition; }
            if (digitFourContainer != null) { digitFourBasePosition = digitFourContainer.localPosition; }
            // ------------------------------------- by RF }

        }
        /// <summary>
        /// 
        /// </summary>
        public void Compute()
        {
            if (m_controller.m_playMode == Controller.PlayMode.Simulation || forceSimulationMode)
            {
                CollectSimulationData();

                if (m_animationType == AnimationType.Transform)
                    MoveNeedle();
                else if (m_animationType == AnimationType.Animator)
                    AnimateNeedle();

                if (useReadout)
                    SetReadoutDigits();
                if (readoutTexts != null)
                    SetReadoutText();
            }
            else if (m_controller.m_playMode == Controller.PlayMode.Animation && !forceSimulationMode && m_animationType == AnimationType.Animator)
            {
                CollectRecordingData();
                AnimateNeedle();
                if (useReadout)
                    SetReadoutDigits();
                if (readoutTexts != null)
                    SetReadoutText();

            }
        }

        public void CollectSimulationData()
        {
            if (m_controller.m_playMode == Controller.PlayMode.Simulation || forceSimulationMode)
            {
                //****************** Get Current Value from Simulation Data *****************
                switch (m_dialType)
                {
                    case DialType.Speed: // Airspeed
                        {
                            //_currentValue = m_controller.m_core.Vkts;

                            // ------------------------------------- by RF
                            double baseSpeed = m_controller.m_core.V;

                            if (speedUnit == SpeedUnit.Knots)
                                _currentValue = baseSpeed * Constants.ms2knots;
                            else
                                _currentValue = baseSpeed;
                            // -------------------------------------
                            break;
                        }
                    case DialType.VerticalSpeed: // Vertical Speed, Climb Rate
                        {
                            //_currentValue = (m_controller.m_core.δz * 196.85) / 1000;
                            
                            // ------------------------------------- by RF
                            double baseVertSpeed = m_controller.m_core.δz;
                            if (vertSpeedUnit == SpeedUnit.FtPMin)
                                _currentValue = baseVertSpeed * Constants.toFtMin / 1000f;
                            else
                                _currentValue = baseVertSpeed;
                            // -------------------------------------
                            break;
                        }
                    case DialType.BankLevel: // BankLevel, Turn
                        {
                            var bankAngle = (float)m_controller.m_core.ф;
                            bankAngle = bankAngle % 360; // by RF
                            if (m_animationType == AnimationType.Transform)
                                _currentValue = bankAngle;
                            else if (m_animationType == AnimationType.Animator)
                            {
                                _currentValue = bankAngle;
                                _currentValue2 = Mathf.Cos(bankAngle * Mathf.Deg2Rad);
                                _currentValue3 = Mathf.Sin(bankAngle * Mathf.Deg2Rad);
                            }
                            break;
                        }
                    case DialType.Compass: // Compass, Heading
                        {
                            //_currentValue = m_controller.m_rigidbody.transform.eulerAngles.y;
                            //var heading = m_controller.m_rigidbody.transform.eulerAngles.y;
                            var heading = (float)m_controller.m_core.ψ;
                            heading = heading % 360; // by RF
                            if (m_animationType == AnimationType.Transform)
                                _currentValue = heading;
                            else if (m_animationType == AnimationType.Animator)
                            {
                                _currentValue = heading;
                                _currentValue2 = Mathf.Cos(heading * Mathf.Deg2Rad);
                                _currentValue3 = Mathf.Sin(heading * Mathf.Deg2Rad);
                            }
                            break;
                        }
                    case DialType.Altitude: // Altitude
                        {
                            //_currentValue = m_controller.m_core.m_height * Oyedoyin.Mathematics.Constants.m2ft;
                            //var altitude = (float)(m_controller.m_core.z * Constants.m2ft);

                            // ------------------------------------- by RF
                            double baseAltitude = m_controller.m_core.z;
                            double altitude;
                            switch (altitudeUnit)
                            {
                                case AltitudeUnit.Meter:
                                    altitude = baseAltitude; /// 1000f;
                                    break;
                                case AltitudeUnit.Feet:
                                    altitude = baseAltitude * Constants.m2ft;
                                    break;
                                default:
                                    altitude = baseAltitude;
                                    break;
                            }
                            if (m_animationType == AnimationType.Transform)
                                _currentValue = altitude;
                            else if (m_animationType == AnimationType.Animator)
                            {
                                _currentValue = altitude;
                                _currentValue2 = Mathf.Cos(((float)altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                _currentValue3 = Mathf.Sin(((float) altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                            }
                            // -------------------------------------
                            break;
                        }
                    case DialType.RadarAltitude: // Radar Altitude
                        {
                            //_currentValue = m_controller.m_core.m_height * Oyedoyin.Mathematics.Constants.m2ft;
                            //_currentValue = m_controller.m_core.z * Constants.m2ft;
                            // ------------------------------------- by RF
                            double baseAltitude = m_controller.m_core.z;
                            double altitude;
                            switch (altitudeUnit)
                            {
                                case AltitudeUnit.Meter:
                                    altitude = baseAltitude; /// 1000f;
                                    break;
                                case AltitudeUnit.Feet:
                                    altitude = baseAltitude * Constants.m2ft;
                                    break;
                                default:
                                    altitude = baseAltitude;
                                    break;
                            }
                            if (m_animationType == AnimationType.Transform)
                                _currentValue = altitude;
                            else if (m_animationType == AnimationType.Animator)
                            {
                                _currentValue = altitude;
                                _currentValue2 = Mathf.Cos(((float)altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                _currentValue3 = Mathf.Sin(((float)altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                            }
                            // -------------------------------------
                            break;
                        }
                    case DialType.ArtificialHorizon: // Artificial Horizon, Attitude
                        {
                            if (m_animationType == AnimationType.Transform)
                            {
                                _currentValue = m_controller.transform.eulerAngles.x; // pitch
                                _currentValue2 = m_controller.transform.eulerAngles.z; // roll
                                _currentValue3 = m_controller.transform.eulerAngles.y; // yaw
                            }
                            else if (m_animationType == AnimationType.Animator)
                            {
                                var pitch = m_controller.transform.eulerAngles.x; // pitch
                                pitch = pitch % 360;
                                var roll = m_controller.transform.eulerAngles.z; // roll
                                roll = roll % 360;

                                if (attitudeIndicatorType == AttitudeIndicatorType.Classic)
                                {
                                    _currentValue = pitch;
                                    _currentValue2 = Mathf.Cos(pitch * Mathf.Deg2Rad); // pitch X
                                    _currentValue3 = Mathf.Sin(pitch * Mathf.Deg2Rad); // pitch Y
                                }
                                else if (attitudeIndicatorType == AttitudeIndicatorType.GlassInstrument)
                                {
                                    // convert angle to be in between -90 to 90
                                    var xpitch = pitch;
                                    if (pitch >= 90 && pitch < 180)
                                        xpitch = 180 - pitch;
                                    else if (pitch >= 180 && pitch < 270)
                                        xpitch = 180 - pitch; // negative
                                    else if (pitch >= 270 && pitch < 360)
                                        xpitch = pitch - 360; // negative

                                    _currentValue = xpitch;
                                    _currentValue2 = 1; // not used in glass instrument
                                    _currentValue3 = 0; // not used in glass instrument
                                }
                                _currentValue4 = roll;
                                _currentValue5 = Mathf.Cos(roll * Mathf.Deg2Rad); // pitch X
                                _currentValue6 = Mathf.Sin(roll * Mathf.Deg2Rad); // pitch Y
                            }
                            break;
                        }
                    case DialType.Fuel: // Fuel
                        {
                            _currentValue = (m_controller.fuelLevel / m_controller.fuelCapacity) * 100;
                            break;
                        }
                    case DialType.EGT: // EGT, Power
                        {
                            _currentValue = m_controller.m_powerLevel * 100;
                            break;
                        }
                    case DialType.RPM: // RPM, Tachometer
                        {
                            if (engineType == EngineType.TurboJet && turbojet != null)
                                _currentValue = turbojet.core.coreRPM;
                            else if (engineType == EngineType.TurboFan && turbofan != null)
                                _currentValue = turbofan.core.coreRPM;
                            else if (engineType == EngineType.TurboProp && turboprop != null)
                                _currentValue = turboprop.core.coreRPM;
                            else if (engineType == EngineType.PistonEngine && piston != null)
                                _currentValue = piston.core.coreRPM;
                            else if (engineType == EngineType.TurboShaft && turboshaft != null)
                                _currentValue = turboshaft.m_RPM;
                            break;
                        }
                }
            }
        }

        public void CollectRecordingData()
        {
            //****************** Get Current Value from Recorded Data *****************
            if (animDial != null)
            {
                switch (m_dialType)
                {
                    case DialType.BankLevel: // Turn, BankLevel, Bank Angle
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "")
                            {
                                var bankAngle = animDial.GetFloat(animLayer1.param1);
                                if (m_animationType == AnimationType.Transform)
                                    _currentValue = bankAngle;
                                else if (m_animationType == AnimationType.Animator)
                                {
                                    _currentValue = bankAngle;
                                    _currentValue2 = Mathf.Cos(bankAngle * Mathf.Deg2Rad);
                                    _currentValue3 = Mathf.Sin(bankAngle * Mathf.Deg2Rad);
                                }
                            }
                            break;
                        }
                    case DialType.Compass: // Airspeed
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "")
                            {
                                var heading = animDial.GetFloat(animLayer1.param1);
                                if (m_animationType == AnimationType.Transform)
                                    _currentValue = heading;
                                else if (m_animationType == AnimationType.Animator)
                                {
                                    _currentValue = heading;
                                    _currentValue2 = Mathf.Cos(heading * Mathf.Deg2Rad);
                                    _currentValue3 = Mathf.Sin(heading * Mathf.Deg2Rad);
                                }
                            }
                            break;
                        }
                    case DialType.Altitude: // Altitude
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "")
                            {
                                var altitude = animDial.GetFloat(animLayer1.param1); ;
                                if (m_animationType == AnimationType.Transform)
                                    _currentValue = altitude;
                                else if (m_animationType == AnimationType.Animator)
                                {
                                    _currentValue = altitude;
                                    _currentValue2 = Mathf.Cos((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                    _currentValue3 = Mathf.Sin((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                }
                            }
                            break;
                        }
                    case DialType.ArtificialHorizon: // Horizon, Attitude
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "" && animLayer2.param1 != null && animLayer2.param1 != "")
                            {
                                var pitch = animDial.GetFloat(animLayer1.param1); // pitch
                                var roll = animDial.GetFloat(animLayer2.param1); // roll
                                if (attitudeIndicatorType == AttitudeIndicatorType.Classic)
                                {
                                    _currentValue = pitch;
                                    _currentValue2 = Mathf.Cos(pitch * Mathf.Deg2Rad); // pitch X
                                    _currentValue3 = Mathf.Sin(pitch * Mathf.Deg2Rad); // pitch Y
                                }
                                else if (attitudeIndicatorType == AttitudeIndicatorType.GlassInstrument)
                                {
                                    if (pitch >= 90 && pitch < 180)
                                        pitch = 180 - pitch;
                                    else if (pitch >= 180 && pitch < 270)
                                        pitch = 180 - pitch; // negative
                                    else if (pitch >= 270 && pitch < 360)
                                        pitch = pitch - 360; // negative
                                    _currentValue = pitch;
                                    _currentValue2 = 1;
                                    _currentValue3 = 0;
                                }
                                _currentValue4 = roll;
                                _currentValue5 = Mathf.Cos(roll * Mathf.Deg2Rad); // pitch X
                                _currentValue6 = Mathf.Sin(roll * Mathf.Deg2Rad); // pitch Y
                            }
                            break;
                        }
                    default:
                        {
                            if (animLayer1 != null)
                            {
                                if (animLayer1.param1 != null && animLayer1.param1 != "")
                                    _currentValue = animDial.GetFloat(animLayer1.param1);
                                if (animLayer1.param2 != null && animLayer1.param2 != "")
                                    _currentValue2 = animDial.GetFloat(animLayer1.param2);
                                if (animLayer1.param3 != null && animLayer1.param3 != "")
                                    _currentValue3 = animDial.GetFloat(animLayer1.param3);
                            }
                            if (animLayer2 != null)
                            {
                                if (animLayer2.param1 != null && animLayer2.param1 != "")
                                    _currentValue4 = animDial.GetFloat(animLayer2.param1);
                                if (animLayer2.param2 != null && animLayer2.param2 != "")
                                    _currentValue5 = animDial.GetFloat(animLayer2.param2);
                                if (animLayer2.param3 != null && animLayer2.param3 != "")
                                    _currentValue6 = animDial.GetFloat(animLayer2.param3);
                            }
                            break;
                        }
                }
            }
            
        }

        public void MoveNeedle()
        {
            if (_needle != null)
            {
                //****************** ARTIFICIAL HORIZON (Special Animation) *****************
                if (m_dialType == DialType.ArtificialHorizon)
                {
                    var pitch = (float)_currentValue;
                    var roll = (float)_currentValue2;
                    var yaw = (float)_currentValue3;
                    var pitchDirection = _direction;
                    var rollDirection = _direction2;
                    var yawDirection = _direction3;
                    var pitchRotationAxis = _rotationAxis;
                    var rollRotationAxis = _rotationAxis2;
                    var yawRotationAxis = _rotationAxis3;

                    if (pitchDirection == RotationDirection.CCW)
                        pitch *= -1f;
                    if (rollDirection == RotationDirection.CCW)
                        roll *= -1f;
                    if (yawDirection == RotationDirection.CCW)
                        yaw *= -1f;

                    var pitchEffect = Quaternion.AngleAxis(pitch, pitchRotationAxis);
                    var rollEffect = Quaternion.AngleAxis(roll, rollRotationAxis);
                    var yawEffect = Quaternion.AngleAxis(yaw, yawRotationAxis);

                    if (attitudeIndicatorType == AttitudeIndicatorType.Classic)
                    {
                        var angleEffect = pitchEffect * rollEffect * yawEffect;
                        _needle.localRotation = _baseRotation * angleEffect;
                    }
                    else if (attitudeIndicatorType == AttitudeIndicatorType.GlassInstrument)
                    {
                        // roll
                        _needle2.localRotation = Quaternion.Inverse(rollEffect);
                        if (pitch > 180)
                            pitch -= 360;
                        else if (pitch < -180)
                            pitch += 360;

                        // pitch
                        _needle.localPosition = _basePosition + _needle.transform.up * pitch * oneDegreeDistance;
                    }

                    /*if (_needle != null) // pitch
                    {
                        float pitchValue = Mathf.DeltaAngle(0, -m_controller.m_rigidbody.transform.rotation.eulerAngles.x);
                        float extension = _minPosition + movementFactor * Mathf.Clamp(pitchValue, _minValue, _maxValue) / 10f;
                        //_needle.anchoredPosition3D = new Vector3(_needle.anchoredPosition3D.x, extension, _needle.anchoredPosition3D.z);
                        _needle.localPosition = new Vector3(_needle.localPosition.x, extension, _needle.localPosition.z);
                    }

                    if (_needle2 != null) // roll
                    {
                        float rollValue = Mathf.DeltaAngle(0, -m_controller.m_rigidbody.transform.eulerAngles.z);
                        float rotation = Mathf.Clamp(rollValue, -180, 180);
                        _needle2.localEulerAngles = new Vector3(_needle2.localEulerAngles.x, _needle2.localEulerAngles.y, rotation);
                    }*/

                }
                else if (m_dialType == DialType.Altitude)
                {
                    if (m_movementType == MovementType.Rotation)
                    {
                        float baseDelta = (float)_currentValue / 1000f;
                        float baseDeltaRotation = (360 * baseDelta);

                        _needle.localRotation = _baseRotation;
                        _needle.Rotate(_rotationAxis, baseDeltaRotation);
                    }
                }
                else
                {
                    //****************** General Animation) *****************
                    if (m_movementType == MovementType.Rotation)
                    {
                        float _rotationValue = Mathf.Lerp(_minRotation, _maxRotation, Mathf.InverseLerp(_minValue, _maxValue, (float)_currentValue));
                        _needle.localRotation = _baseRotation;
                        _needle.Rotate(_rotationAxis, _rotationValue);
                    }
                    else if (m_movementType == MovementType.Translation)
                    {
                        float _translationValue = Mathf.Lerp(_minPosition, _maxPosition, Mathf.InverseLerp(_minValue, _maxValue, (float)_currentValue));
                        _needle.localPosition = _basePosition;
                        _needle.Translate(_translationAxis * _translationValue);
                    }
                }
            }
            
        }

        public void AnimateNeedle()
        {
            if (animDial != null)
            {
                if (animLayer1 != null)
                {
                    if (animLayer1.param1 != "")
                        animDial.SetFloat(animLayer1.param1, (float)_currentValue);
                    if (animLayer1.param2 != "")
                        animDial.SetFloat(animLayer1.param2, (float)_currentValue2);
                    if (animLayer1.param3 != "")
                        animDial.SetFloat(animLayer1.param3, (float)_currentValue3);
                }
                if (animLayer2 != null)
                {
                    if (animLayer2.param1 != "")
                        animDial.SetFloat(animLayer2.param1, (float)_currentValue4);
                    if (animLayer2.param2 != "")
                        animDial.SetFloat(animLayer2.param2, (float)_currentValue5);
                    if (animLayer2.param3 != "")
                        animDial.SetFloat(animLayer2.param3, (float)_currentValue6);
                }
            }
        }

        //****************** READOUT DIGITS *****************
        void SetReadoutDigits()
        {
            //EXTRACT DIGITS
            digitOne = (((float)_currentValue % digitOneModder) / digitSmallestValue);
            digitTwo = Mathf.Floor(((float)_currentValue % digitTwoModder) / digitOneModder);
            digitThree = Mathf.Floor(((float)_currentValue % digitThreeModder) / digitTwoModder);
            digitFour = Mathf.Floor(((float)_currentValue % digitFourModder) / digitThreeModder);

            //CALCULATE DIAL POSITIONS
            float digitOnePosition = digitOne * -digitOneTranslation;

            float digitTwoPosition = digitTwo * -digitTwoTranslation;
            var digitOneLimit = digitOneModder - digitSmallestValue;
            if ((digitOne * digitSmallestValue) > digitOneLimit)
            {
                digitTwoPosition += ((digitOne * digitSmallestValue) - digitOneLimit) / digitSmallestValue * -digitTwoTranslation;
            }
            
            float digitThreePosition = digitThree * -digitThreeTranslation;
            var digitTwoLimit = digitTwoModder - digitSmallestValue;
            if ((digitTwo * digitOneModder) > digitTwoLimit)
            {
                digitThreePosition += ((digitTwo * digitOneModder) - digitTwoLimit) / digitSmallestValue * -digitThreeTranslation;
            }
            
            float digitFourPosition = digitFour * -digitFourTranslation;
            var digitThreeLimit = digitThreeModder - digitSmallestValue;
            if ((digitThree * digitTwoModder) > digitThreeLimit)
            {
                digitFourPosition += ((digitThree * digitTwoModder) - digitThreeLimit) / digitSmallestValue * -digitFourTranslation;
            }

            /*//EXTRACT DIGITS
            digitOne = (((float)_currentValue % 100.0f) / 20.0f); //20FT Spacing
            digitTwo = Mathf.Floor(((float)_currentValue % 1000.0f) / 100.0f);
            digitThree = Mathf.Floor(((float)_currentValue % 10000.0f) / 1000.0f);
            digitFour = Mathf.Floor(((float)_currentValue % 100000.0f) / 10000.0f);

            //CALCULATE DIAL POSITIONS
            float digitOnePosition = digitOne * -digitOneTranslation;
            float digitTwoPosition = digitTwo * -digitTwoTranslation;
            if ((digitOne * 20) > 90.0f)
            {
                digitTwoPosition += ((digitOne * 20f) - 90.0f) / 10.0f * -digitTwoTranslation;
            }
            float digitThreePosition = digitThree * -digitThreeTranslation;
            if ((digitTwo * 100) > 990.0f)
            {
                digitThreePosition += ((digitTwo * 100) - 990.0f) / 10.0f * -digitThreeTranslation;
            }
            float digitFourPosition = digitFour * -digitFourTranslation;
            if ((digitThree * 1000) > 9990.0f)
            {
                digitFourPosition += ((digitThree * 1000) - 9990.0f) / 10f * -digitFourTranslation;
            }*/

            //SET POSITIONS
            if (digitOneContainer != null) 
                digitOneContainer.localPosition = digitOneBasePosition + new Vector3(0, digitOnePosition, 0);
            if (digitTwoContainer != null) 
                digitTwoContainer.localPosition = digitTwoBasePosition + new Vector3(0, digitTwoPosition, 0);
            if (digitThreeContainer != null) 
                digitThreeContainer.localPosition = digitThreeBasePosition + new Vector3(0, digitThreePosition, 0);
            if (digitFourContainer != null) 
                digitFourContainer.localPosition = digitFourBasePosition + new Vector3(0, digitFourPosition, 0);
        }

        //****************** READOUT TEXTS *****************
        void SetReadoutText()
        {
            foreach (TMPro.TMP_Text readoutText in readoutTexts)
            {
                /*if (m_dialType == DialType.Compass)
                {
                    var readoutHeading = Mathf.Acos((float)_currentValue) * Mathf.Rad2Deg;
                    readoutText.text = readoutHeading.ToString(readoutTextFormat);
                }
                else*/
                readoutText.text = _currentValue.ToString(readoutTextFormat);
            }
        }
    }
    #endregion

    #region Editor

#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(SilantroDial))]

    public class SilantroDialEditor : Editor
    {
        Color backgroundColor;
        Color silantroColor = Color.yellow; // new Color(1, 0.4f, 0);
        SilantroDial dial;
        SerializedProperty animLayer1, animLayer2;

        /// <summary>
        /// 
        /// </summary>
        private void OnEnable()
        {
            dial = (SilantroDial)target;
            //dial.animLayer1 = new SilantroDial.AnimLayer();
            //dial.animLayer2 = new SilantroDial.AnimLayer();
            animLayer1 = serializedObject.FindProperty("animLayer1");
            animLayer2 = serializedObject.FindProperty("animLayer2");
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;
            //DrawDefaultInspector();
            serializedObject.Update();

            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Dial Type", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_dialType"), new GUIContent("Dial Type"));

            if (dial.m_dialType == SilantroDial.DialType.RPM)
            {
                GUILayout.Space(3f);
                GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Engine Type", MessageType.None);
                GUI.color = backgroundColor;

                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("engineType"), new GUIContent("Engine Type"));
                GUILayout.Space(3f);

                if (dial.engineType == SilantroDial.EngineType.TurboFan)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("turbofan"), new GUIContent("Turbofan Engine"));
                else if (dial.engineType == SilantroDial.EngineType.TurboJet)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("turbojet"), new GUIContent("Turboject Engine"));
                else if (dial.engineType == SilantroDial.EngineType.TurboProp)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("turboprop"), new GUIContent("Turboprop Engine"));
                else if (dial.engineType == SilantroDial.EngineType.PistonEngine)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("piston"), new GUIContent("Piston Engine"));
                else if (dial.engineType == SilantroDial.EngineType.TurboShaft)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("turboshaft"), new GUIContent("Turboshaft Engine"));
            }

            GUILayout.Space(3f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Dial Configuration", MessageType.None);
            GUI.color = backgroundColor;

            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_animationType"), new GUIContent("Animation Type"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("forceSimulationMode"), new GUIContent("Force Simulation Mode"));

            if (dial.m_animationType == SilantroDial.AnimationType.Transform)
            {
                // ****************************** TRANSFORM ANIMATION INSPECTOR *********************************
                if (dial.m_dialType == SilantroDial.DialType.ArtificialHorizon)
                {
                    // ****************************** ARTIFICIAL HORIZON DIAL INSPECTOR *********************************
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("attitudeIndicatorType"), new GUIContent("Attitude Indicator Type"));
                    GUILayout.Space(2f);

                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;

                    if (dial.attitudeIndicatorType == SilantroDial.AttitudeIndicatorType.Classic)
                    {

                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_axis"), new GUIContent("Pitch Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_direction"), new GUIContent("Pitch Rotation Direction"));

                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_axis2"), new GUIContent("Roll Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_direction2"), new GUIContent("Roll Rotation Direction"));

                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_axis3"), new GUIContent("Yaw Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_direction3"), new GUIContent("Yaw Rotation Direction"));

                        GUILayout.Space(15f);
                        GUI.color = silantroColor;
                        EditorGUILayout.HelpBox("Connections", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        dial._needle = EditorGUILayout.ObjectField("Horizon Indicator", dial._needle, typeof(Transform), true) as Transform;
                    }
                    else if (dial.attitudeIndicatorType == SilantroDial.AttitudeIndicatorType.GlassInstrument)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_axis2"), new GUIContent("Roll Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_direction2"), new GUIContent("Roll Rotation Direction"));

                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("oneDegreeDistance"), new GUIContent("One Degree Pitch Distance"));
                        GUILayout.Space(2f);

                        GUILayout.Space(15f);
                        GUI.color = silantroColor;
                        EditorGUILayout.HelpBox("Connections", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        dial._needle = EditorGUILayout.ObjectField("Horizon Rotation Anchor", dial._needle, typeof(Transform), true) as Transform;
                        GUILayout.Space(3f);
                        dial._needle2 = EditorGUILayout.ObjectField("Horizon Indicator", dial._needle2, typeof(Transform), true) as Transform;

                    }
                }
                else
                {
                    // ****************************** GENERAL DIAL INSPECTOR *********************************
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("_needle"), new GUIContent("Needle"));

                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_movementType"), new GUIContent("Movement Type"));

                    if (dial.m_movementType == SilantroDial.MovementType.Rotation)
                    {
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Rotation", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_axis"), new GUIContent("Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_direction"), new GUIContent("Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_lag"), new GUIContent("Lag"));

                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Limits", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_minValue"), new GUIContent("Minimum Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_maxValue"), new GUIContent("Maximum Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_minRotation"), new GUIContent("Minimum Rotation"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_maxRotation"), new GUIContent("Maximum Rotation"));
                    }
                    else if (dial.m_movementType == SilantroDial.MovementType.Translation)
                    {
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Translation", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_axis"), new GUIContent("Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_direction"), new GUIContent("Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_lag"), new GUIContent("Lag"));

                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Limits", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_minValue"), new GUIContent("Minimum Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_maxValue"), new GUIContent("Maximum Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_minPosition"), new GUIContent("Minimum Position"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("_maxPosition"), new GUIContent("Maximum Position"));
                    }
                }
            }
            else if (dial.m_animationType == SilantroDial.AnimationType.Animator)
            {
                // ****************************** ANIMATOR ANIMATION INSPECTOR *********************************

                GUILayout.Space(15f);
                GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Animator Configuration", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);

                // Get Animator Controller
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animDial"), new GUIContent("Animator Component"));
                //GUILayout.Space(3f);
                //EditorGUILayout.PropertyField(serializedObject.FindProperty("animatorPlayMode"), new GUIContent("Animator Play Mode"));

                if (dial.animDial != null)
                {
                    // Get animator parameters
                    AnimatorControllerParameter[] animParams = (dial.animDial.runtimeAnimatorController as AnimatorController).parameters;
                    string[] animParamNames = new string[animParams.Length];
                    var idx = 0;
                    foreach (AnimatorControllerParameter param in animParams)
                    {
                        animParamNames[idx] = param.name;
                        idx++;
                    }

                    // Get animator layers
                    AnimatorControllerLayer[] animLayers = (dial.animDial.runtimeAnimatorController as AnimatorController).layers;
                    string[] animLayerNames = new string[animLayers.Length];
                    idx = 0;
                    foreach (AnimatorControllerLayer layer in animLayers)
                    {
                        animLayerNames[idx] = layer.name;
                        idx++;
                    }

                    // -------------------------------------------------------- Speed
                    if (dial.m_dialType == SilantroDial.DialType.Speed)
                    {
                        AnimatorConfigInspector("Speed", ref dial.animLayer1, ref animLayer1, "Speed", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Vertical Speed
                    else if (dial.m_dialType == SilantroDial.DialType.VerticalSpeed)
                    {
                        AnimatorConfigInspector("Vertical Speed", ref dial.animLayer1, ref animLayer1, "Vertical Speed", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Turn
                    if (dial.m_dialType == SilantroDial.DialType.BankLevel)
                    {
                        AnimatorConfigInspector("Turn", ref dial.animLayer1, ref animLayer1, "Turn", "Turn X", "Turn Y", animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Compass
                    if (dial.m_dialType == SilantroDial.DialType.Compass)
                    {
                        AnimatorConfigInspector("Heading", ref dial.animLayer1, ref animLayer1, "Heading", "Heading X", "Heading Y", animParamNames, animLayerNames, animLayers);

                    }

                    // -------------------------------------------------------- Altitude
                    if (dial.m_dialType == SilantroDial.DialType.Altitude)
                    {
                        AnimatorConfigInspector("Altitude", ref dial.animLayer1, ref animLayer1, "Altitude", "Altitude X", "Altitude Y", animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Radar Altitude
                    if (dial.m_dialType == SilantroDial.DialType.RadarAltitude)
                    {
                        AnimatorConfigInspector("Radar Altitude", ref dial.animLayer1, ref animLayer1, "Radar Altitude", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Horizon
                    if (dial.m_dialType == SilantroDial.DialType.ArtificialHorizon)
                    {
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("attitudeIndicatorType"), new GUIContent("Attitude Indicator Type"));
                        GUILayout.Space(2f);

                        if (dial.attitudeIndicatorType == SilantroDial.AttitudeIndicatorType.Classic)
                        {
                            AnimatorConfigInspector("Horizon Pitch", ref dial.animLayer1, ref animLayer1, "Horizon Pitch", "Horizon Pitch X", "Horizon Pitch Y", animParamNames, animLayerNames, animLayers);
                            AnimatorConfigInspector("Horizon Roll", ref dial.animLayer2, ref animLayer2, "Horizon Roll", "Horizon Roll X", "Horizon Roll Y", animParamNames, animLayerNames, animLayers);
                        }
                        else if (dial.attitudeIndicatorType == SilantroDial.AttitudeIndicatorType.GlassInstrument)
                        {
                            AnimatorConfigInspector("Horizon Pitch", ref dial.animLayer1, ref animLayer1, "Horizon Pitch", null, null, animParamNames, animLayerNames, animLayers);
                            AnimatorConfigInspector("Horizon Roll", ref dial.animLayer2, ref animLayer2, "Horizon Roll", "Horizon Roll X", "Horizon Roll Y", animParamNames, animLayerNames, animLayers);
                        }
                    }

                    // -------------------------------------------------------- EGT, Throttle
                    if (dial.m_dialType == SilantroDial.DialType.EGT)
                    {
                        AnimatorConfigInspector("Throttle", ref dial.animLayer1, ref animLayer1, "Throttle", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Fuel
                    if (dial.m_dialType == SilantroDial.DialType.Fuel)
                    {
                        AnimatorConfigInspector("Fuel", ref dial.animLayer1, ref animLayer1, "Fuel", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- RPM
                    if (dial.m_dialType == SilantroDial.DialType.RPM)
                    {
                        AnimatorConfigInspector("RPM", ref dial.animLayer1, ref animLayer1, "RPM", null, null, animParamNames, animLayerNames, animLayers);
                    }
                }
            }

            // ****************************** READOUT DIGITS INSPECTOR *********************************
            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Readout Digits Settings", MessageType.None);
            GUI.color = backgroundColor;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("useReadout"), new GUIContent("Use Readout Digits"));
            GUILayout.Space(5f);

            if (dial.useReadout)
            {
                GUILayout.Space(5f);
                EditorGUILayout.HelpBox("Digit Value", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitSmallestValue"), new GUIContent("Digit Smallest Value"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneModder"), new GUIContent("Digit One Level"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoModder"), new GUIContent("Digit Two Level"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeModder"), new GUIContent("Digit Three Level"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourModder"), new GUIContent("Digit Four Level"));

                GUILayout.Space(5f);
                EditorGUILayout.HelpBox("Dial 'Per Digit' Translation", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneTranslation"), new GUIContent("Digit One Translation"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoTranslation"), new GUIContent("Digit Two Translation"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeTranslation"), new GUIContent("Digit Three Translation"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourTranslation"), new GUIContent("Digit Four Translation"));

                GUILayout.Space(10f);
                //GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Digit Containers", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneContainer"), new GUIContent("Digit One Container"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoContainer"), new GUIContent("Digit Two Container"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeContainer"), new GUIContent("Digit Three Container"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourContainer"), new GUIContent("Digit Four Container"));

                GUILayout.Space(10f);
                //GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Digit Display", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.LabelField("Current Value", dial.digitFour.ToString("0") + "| " + dial.digitThree.ToString("0") + "| " + dial.digitTwo.ToString("0") + "| " + dial.digitOne.ToString("0"));
            }

            // ****************************** READOUT TEXT INSPECTOR *********************************
            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Readout Texts Settings", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("readoutTexts"), new GUIContent("Readout Texts"));
            if (dial.readoutTexts != null && dial.readoutTexts.Length > 0)
            {
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("readoutTextFormat"), new GUIContent("Readout Text Format"));
            }

            serializedObject.ApplyModifiedProperties();
        }


        private void AnimatorConfigInspector(string layerName, ref SilantroDial.AnimLayer animLayer, ref SerializedProperty animLayerSO, string param1name, string param2name, string param3name, string[] animParamNames, string[] animLayerNames, AnimatorControllerLayer[] animLayers)
        {
            //SilantroDial.AnimLayer layer = new SilantroDial.AnimLayer();

            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox(layerName + " Animation Controller Configuration", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);

            if (animParamNames.Length == 0)
                EditorGUILayout.LabelField("Animator Parameter", "Not Found!");
            else
            {
                if (param1name != null)
                {
                    animLayer.paramIdx1 = EditorGUILayout.Popup(param1name + " Animation Parameter", animLayer.paramIdx1, animParamNames);
                    animLayer.param1 = animParamNames[animLayer.paramIdx1];
                    GUILayout.Space(3f);
                    /*EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param1SyncValue"), new GUIContent(param1name + " Input Value = Anim Param Value?"));
                    GUILayout.Space(3f);
                    if (animLayer.param1SyncValue == false)
                    {
                        EditorGUI.indentLevel++;
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param1InputMin"), new GUIContent("Minimum " + param1name + " Input Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param1InputMax"), new GUIContent("Maximum " + param1name + " Input Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param1OutputMin"), new GUIContent("Minimum " + param1name + " Animation Parameter Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param1OutputMax"), new GUIContent("Maximum " + param1name + " Animation Parameter Value"));
                        GUILayout.Space(3f);
                        EditorGUI.indentLevel--;
                    }
                    EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param1InvertDirection"), new GUIContent("Inverted " + param1name + " Value?"));
                    GUILayout.Space(3f);*/
                }

                if (param2name != null)
                {

                    animLayer.paramIdx2 = EditorGUILayout.Popup(param2name + " Animation Parameter", animLayer.paramIdx2, animParamNames);
                    animLayer.param2 = animParamNames[animLayer.paramIdx2];
                    GUILayout.Space(3f);
                    /*EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param2SyncValue"), new GUIContent(param2name + " Sync Input Value?"));
                    GUILayout.Space(3f);
                    if (animLayer.param2SyncValue == false)
                    {
                        EditorGUI.indentLevel++;
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param2InputMin"), new GUIContent("Minimum " + param2name + " Input Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param2InputMax"), new GUIContent("Maximum " + param2name + " Input Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param2OutputMin"), new GUIContent("Minimum " + param2name + " Animation Parameter Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param2OutputMax"), new GUIContent("Maximum " + param2name + " Animation Parameter Value"));
                        GUILayout.Space(3f);
                        EditorGUI.indentLevel--;
                    }
                    EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param2InvertDirection"), new GUIContent("Inverted " + param2name + " Value?"));
                    GUILayout.Space(3f);*/
                }

                if (param3name != null)
                {

                    animLayer.paramIdx3 = EditorGUILayout.Popup(param3name + " Animation Parameter", animLayer.paramIdx3, animParamNames);
                    animLayer.param3 = animParamNames[animLayer.paramIdx3];
                    GUILayout.Space(3f);
                    /*EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param3SyncValue"), new GUIContent(param3name + " Input Value = Anim Param Value?"));
                    GUILayout.Space(3f);
                    if (animLayer.param3SyncValue == false)
                    {
                        EditorGUI.indentLevel++;
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param3InputMin"), new GUIContent("Minimum " + param3name + " Input Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param3InputMax"), new GUIContent("Maximum " + param3name + " Input Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param3OutputMin"), new GUIContent("Minimum " + param3name + " Animation Parameter Value"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param3OutputMax"), new GUIContent("Maximum " + param3name + " Animation Parameter Value"));
                        GUILayout.Space(3f);
                        EditorGUI.indentLevel--;
                    }
                    EditorGUILayout.PropertyField(animLayerSO.FindPropertyRelative("param3InvertDirection"), new GUIContent("Inverted " + param3name + " Value?"));
                    GUILayout.Space(3f);*/
                }
            }

            if (animLayerNames.Length == 0)
                EditorGUILayout.LabelField("Animator Layer", "Not Found!");
            else
            {
                animLayer.layerIdx = EditorGUILayout.Popup("Animation Layer", animLayer.layerIdx, animLayerNames);
                animLayer.layerName = animLayerNames[animLayer.layerIdx];
                GUILayout.Space(3f);

                // Get animation states
                ChildAnimatorState[] smStates = animLayers[animLayer.layerIdx].stateMachine.states;

                if (smStates.Length == 0)
                    EditorGUILayout.LabelField("Animator State", "Not Found!");
                else
                {
                    string[] smStateNames = new string[smStates.Length];
                    var idx = 0;
                    foreach (ChildAnimatorState state in smStates)
                    {
                        smStateNames[idx] = state.state.name;
                        idx++;
                    }

                    animLayer.smStateIdx = EditorGUILayout.Popup("Animation State", animLayer.smStateIdx, smStateNames);
                    animLayer.smState = smStateNames[animLayer.smStateIdx];
                    GUILayout.Space(3f);
                }
            }
        }
    }

#endif

    #endregion
}

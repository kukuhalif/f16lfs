﻿using System;
using UnityEngine;
using Oyedoyin.Common;
using Oyedoyin.Analysis;
//using Oyedoyin.Navigation;
using Oyedoyin.Mathematics;


public class FlightDirector : MonoBehaviour
{
    public enum WaypointState { PathHold, TrackTurn }

    public Computer m_computer;
    public SilantroWaypoints m_waypoints;
    public WaypointState m_waypointState = WaypointState.PathHold;

    public int m_index;
    public Transform m_point1, m_point2, m_point3;

    public double m_turnHeading, m_pointDistance, m_turnDistance;

    public void FixedUpdate()
    {
        Transform m_aircraft = m_computer.controller.m_rigidbody.transform;
        double m_currentSpeed = m_computer.controller.m_core.V;
        int m_count = m_waypoints.m_points.Length;
        if (m_index > m_count - 1) { m_index = 0; }

        int m_step_1 = m_index + 0; if (m_step_1 > m_count - 1) { m_step_1 -= m_count; }
        int m_step_2 = m_index + 1; if (m_step_2 > m_count - 1) { m_step_2 -= m_count; }
        int m_step_3 = m_index + 2; if (m_step_3 > m_count - 1) { m_step_3 -= m_count; }

        m_point1 = m_waypoints.m_points[m_step_1];
        m_point2 = m_waypoints.m_points[m_step_2];
        m_point3 = m_waypoints.m_points[m_step_3];

        Debug.DrawLine(m_computer.transform.position, m_point1.position, Color.green);
        Debug.DrawLine(m_computer.transform.position, m_point2.position, Color.yellow);
        Debug.DrawLine(m_computer.transform.position, m_point3.position, Color.red);

        // ----------------------------------------------- Point 2
        //NavMath.AnalyseCurve(m_aircraft, m_point1.position, m_point2.position, m_point3.position, out m_turnHeading, out _, out m_pointDistance);
        m_computer.m_commands.m_commandAltitude = m_point1.position.y;
        if (double.IsNaN(m_turnHeading) || double.IsInfinity(m_turnHeading)) { m_turnHeading = 0.0; }
       // m_turnDistance = m_computer.m_autopilot.m_turnGain * ((m_currentSpeed * m_currentSpeed) /  (m_computer.controller.m_core.m_atmosphere.g * Math.Tan(m_computer.maximumTurnBank * Mathf.Deg2Rad) * Math.Tan(m_turnHeading * 0.5 * Mathf.Deg2Rad)));


        // ----------------------------------------------- Point 3
        double dt = Time.fixedDeltaTime;
        //m_CTE = NavMath.CrossTrackError(m_aircraft.position, m_point1.position, m_point2.position, out _);
        m_δCTE = (m_CTE - m_old_CTE) / dt;


        // ----------------------------------------------- Action
        if (m_waypointState == WaypointState.PathHold)
        {
            if (m_pointDistance <= m_turnDistance) { m_waypointState = WaypointState.TrackTurn; }
            //double m_trackHeading = NavMath.GetBearing(m_aircraft.transform, m_point2.position, out double m_trackDistance);
            //m_computer.m_commands.m_commandTurnRate = -m_trackSolver.Compute(m_CTE, dt);
        }
        if (m_waypointState == WaypointState.TrackTurn)
        {
            m_computer.m_commands.m_commandTurnRate = -m_computer.maximumTurnRate;
            if (m_pointDistance >= m_turnDistance)
            {
                SwitchPoint();
                m_waypointState = WaypointState.PathHold;
            }
        }


        m_old_CTE = m_CTE;
    }

    public FPID m_trackSolver;
    public double m_CTE, m_δCTE, m_old_CTE;

    void SwitchPoint() { m_index += 1; }
}

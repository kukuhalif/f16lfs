﻿using UnityEngine;
#if (ENABLE_INPUT_SYSTEM)
using UnityEngine.InputSystem;
#endif
#if VR_ACTIVE
//using UnityEngine.XR.Interaction.Toolkit;
#endif


/// <summary>
/// 
/// </summary>
namespace Oyedoyin.Common
{
    public class SilantroHandVR : MonoBehaviour
    {
        public enum HandType { Right, Left }
        public enum HandState { Free, Holding }

        // Connections
        public HandType m_handType = HandType.Right;
        public HandState m_handState = HandState.Free;

#if VR_ACTIVE
    public ActionBasedController controller;
#endif
#if (ENABLE_INPUT_SYSTEM)
    public InputActionReference thumbReference;
#endif

        public Animator m_animator;
        public SkinnedMeshRenderer m_mesh;

        // Animator Keys
        public string m_gripName = "Grip";
        public string m_triggerName = "Trigger";
        public string m_thumbName = "Thumb";

        // Data
        public float m_speed = 10;
        public float gripValue;
        public float triggerValue;
        public bool thumbPressed;
        private float m_currentTrigger;
        private float m_currentGrip;
        private float m_currentThumb;

        /// <summary>
        /// 
        /// </summary>
        protected void Update()
        {

#if VR_ACTIVE
        if (controller != null)
        {
            gripValue = controller.selectAction.action.ReadValue<float>();
            triggerValue = controller.activateAction.action.ReadValue<float>();
        }
        if (thumbReference != null) { thumbPressed = thumbReference.action.IsPressed(); }
#endif



            if (m_animator != null)
            {
                //-------------------------------------------- Thumb
                if (thumbPressed)
                {
                    if (m_currentThumb != 1)
                    {
                        m_currentThumb = Mathf.MoveTowards(m_currentThumb,
                        1, Time.deltaTime * m_speed);
                        m_animator.SetFloat(m_thumbName, m_currentThumb);
                    }
                }
                else
                {
                    if (m_currentThumb != 0)
                    {
                        m_currentThumb = Mathf.MoveTowards(m_currentThumb,
                        0, Time.deltaTime * m_speed);
                        m_animator.SetFloat(m_thumbName, m_currentThumb);
                    }
                }

                //-------------------------------------------- Grip
                if (m_currentGrip != gripValue)
                {
                    m_currentGrip = Mathf.MoveTowards(m_currentGrip,
                        gripValue, Time.deltaTime * m_speed);
                    m_animator.SetFloat(m_gripName, m_currentGrip);
                }

                //-------------------------------------------- Trigger
                if (m_currentTrigger != triggerValue)
                {
                    m_currentTrigger = Mathf.MoveTowards(m_currentTrigger,
                        triggerValue, Time.deltaTime * m_speed);
                    m_animator.SetFloat(m_triggerName, m_currentTrigger);
                }
            }

            if (m_handState == HandState.Free)
            {
                if (!m_mesh.gameObject.activeSelf) { m_mesh.gameObject.SetActive(true); }
            }
            else
            {
                if (m_mesh.gameObject.activeSelf) { m_mesh.gameObject.SetActive(false); }
            }
        }
    }
}

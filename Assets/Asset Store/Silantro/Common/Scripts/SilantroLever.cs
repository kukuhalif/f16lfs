﻿using System;
using UnityEngine;
using Oyedoyin.Common.Misc;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

namespace Oyedoyin.Common
{
    #region Component
    /// <summary>
    /// Use:	Handles the movement/rotation of control levers
    /// </summary>
    public class SilantroLever : MonoBehaviour
    {
        //public enum Type { Lever, Dial, Readout, HSI }

        public Controller m_controller;
        //public Type m_type = Type.Lever;
        //public Lever m_lever;
        //public Dial m_dial;
        //public Readout m_readout;
        //public HSI m_hsi;

        public float m_roll;
        public float m_pitch;
        public float m_yaw;
        public float m_throttle;
        public float m_prop;
        public float m_mixture;

        public enum LeverType { Stick, Throttle, Pedal, Flaps, GearIndicator, Mixture, PropellerPitch, Collective }
        public enum StickType { Joystick, Yoke, ControlColumn, }
        public enum ThrottleMode { Deflection, Sliding }
        public enum PedalType { Sliding, Hinged }
        public enum PedalMode { Individual, Combined }
        public enum AnimationType { Transform, Animator }; // by RF

        //public Controller m_controller;
        //public SilantroInstrument m_base;
        public LeverType m_leverType = LeverType.Stick;
        public StickType m_stickType = StickType.Joystick;
        public ThrottleMode m_throttleMode = ThrottleMode.Deflection;
        public AnimationType m_animationType = AnimationType.Transform; // by RF

        // ---------------------------------------------- Connections
        public Transform m_lever;
        public Transform m_yoke;

        // ---------------------------------------------- Limits
        // Joystick
        public float m_rollLimit = 20;
        public float m_pitchLimit = 5;
        // Base Lever
        public float m_deflectionLimit = 30;
        public float m_slidingLimit = 10;
        // Pedals
        public float m_pedalDeflectionLimit = 30;
        public float m_pedalSlidingLimit = 10;
        float _leverInput = 0;


        // ---------------------------------------------- Control Properties
        public Vector3 m_rollAxisDeflection;
        public Vector3 m_pitchAxisDeflection;
        public RotationAxis m_rotationAxisRoll = RotationAxis.X;
        public RotationDirection m_directionRoll = RotationDirection.CW;
        public RotationAxis m_rotationAxisPitch = RotationAxis.X;
        public RotationDirection m_directionPitch = RotationDirection.CW;

        // Base Lever Stuff
        public RotationAxis m_rotationAxis = RotationAxis.X;
        public RotationDirection m_direction = RotationDirection.CW;
        public MovementAxis m_movementAxis;
        public MovementDirection m_movementDirection;
        public Vector3 m_axisDeflection;
        Quaternion m_baseLeverRotation;
        Quaternion m_baseYokeRotation;
        Vector3 m_baseLeverPosition;

        // Pedals
        public PedalType m_pedalType = PedalType.Hinged;
        public PedalMode m_pedalMode = PedalMode.Combined;
        public Transform m_leftPedal;
        public Transform m_rightPedal;
        public RotationAxis m_rightRotationAxis = RotationAxis.X;
        public RotationAxis m_leftRotationAxis = RotationAxis.X;
        public RotationDirection m_rightDirection = RotationDirection.CW;
        public RotationDirection m_leftDirection = RotationDirection.CCW;
        Vector3 m_rightAxis, m_leftAxis;
        Vector3 m_baseRightPosition, m_baseLeftPosition;
        Quaternion m_baseRightRotation;
        Quaternion m_baseLeftRotation;

        // ------------------------------------- Animator Movement - by RF
        public Animator animLever;
        public int animParamIdx1, animParamIdx2, animLayerIdx, smStateIdx;
        public string animParam1, animParam2, animLayer, smState;
        public float animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax;
        public float animParam2InputMin, animParam2InputMax, animParam2OutputMin, animParam2OutputMax;
        public bool animParam1SyncValue, animParam2SyncValue;
        public bool animParam1InvertDirection, animParam2InvertDirection;



        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            if (m_animationType == AnimationType.Transform)
            {
                // Yaw Pedals
                if (m_leverType == LeverType.Pedal)
                {
                    if (m_leftPedal != null)
                    {
                        m_baseLeftRotation = m_leftPedal.localRotation;
                        m_baseLeftPosition = m_leftPedal.localPosition;
                        m_leftAxis = Handler.EstimateModelProperties(m_leftDirection.ToString(), m_leftRotationAxis.ToString());
                    }
                    if (m_rightPedal != null)
                    {
                        m_baseRightRotation = m_rightPedal.localRotation;
                        m_baseRightPosition = m_rightPedal.localPosition;
                        m_rightAxis = Handler.EstimateModelProperties(m_rightDirection.ToString(), m_rightRotationAxis.ToString());
                    }
                }
                // Base Levers
                if (m_lever != null)
                {
                    m_baseLeverPosition = m_controller.transform.InverseTransformPoint(m_lever.position);
                    m_baseLeverRotation = m_lever.localRotation;
                    m_axisDeflection = Handler.EstimateModelProperties(m_direction.ToString(), m_rotationAxis.ToString());
                    m_rollAxisDeflection = Handler.EstimateModelProperties(m_directionRoll.ToString(), m_rotationAxisRoll.ToString());
                    m_pitchAxisDeflection = Handler.EstimateModelProperties(m_directionPitch.ToString(), m_rotationAxisPitch.ToString());
                }
                if (m_yoke != null) { m_baseYokeRotation = m_yoke.localRotation; }
            }
            else if (m_animationType == AnimationType.Animator)
            {
                if (animLever != null)
                    animLever.Play(animLayer + "." + smState);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        //public void Compute(float _input)
        public void Compute(float _input)
        {
            if (m_controller != null)
            {
                // Control Stick
                if (m_leverType == LeverType.Stick)
                {
                    if (m_animationType == AnimationType.Transform)
                    {
                        // Yoke
                        if (m_stickType == StickType.Yoke)
                        {
                            if (m_lever != null)
                            {
                                Quaternion rollEffect = Quaternion.AngleAxis(m_roll * m_rollLimit, m_rollAxisDeflection);
                                m_lever.localRotation = m_baseLeverRotation * rollEffect;

                                Vector3 m_rgt = m_lever.right;
                                if (m_movementAxis == MovementAxis.Forward) { m_rgt = m_lever.forward; }
                                if (m_movementAxis == MovementAxis.Right) { m_rgt = m_lever.right; }
                                if (m_movementAxis == MovementAxis.Up) { m_rgt = m_lever.up; }
                                if (m_movementDirection == MovementDirection.Inverted) { m_rgt *= -1; }

                                Vector3 m_worldPosition = m_controller.transform.TransformPoint(m_baseLeverPosition);
                                Vector3 m_position = m_worldPosition + (0.01f * m_pitch * m_pitchLimit * m_rgt);
                                m_lever.position = m_position;
                            }
                        }
                        // Control Column
                        else if (m_stickType == StickType.ControlColumn)
                        {
                            if (m_yoke != null)
                            {
                                Quaternion rollEffect = Quaternion.AngleAxis(m_roll * m_rollLimit, m_rollAxisDeflection);
                                m_yoke.localRotation = m_baseYokeRotation * rollEffect;

                                Quaternion pitchEffect = Quaternion.AngleAxis(m_pitch * m_pitchLimit, m_pitchAxisDeflection);
                                m_lever.localRotation = m_baseLeverRotation * pitchEffect;
                            }
                        }
                        // Joystick
                        else if (m_stickType == StickType.Joystick)
                        {
                            if (m_lever != null)
                            {
                                Quaternion rollEffect = Quaternion.AngleAxis(m_roll * m_rollLimit, m_rollAxisDeflection);
                                Quaternion pitchEffect = Quaternion.AngleAxis(m_pitch * m_pitchLimit, m_pitchAxisDeflection);
                                m_lever.localRotation = m_baseLeverRotation * rollEffect * pitchEffect;
                            }
                        }
                    }
                    else if (m_animationType == AnimationType.Animator)
                    {
                        if (animLever != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animLever.SetFloat(animParam1, m_pitch * -1);
                                else
                                    animLever.SetFloat(animParam1, m_pitch);
                            }
                            else
                                animLever.SetFloat(animParam1, CalculateParameterValue(m_pitch, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));

                            if (animParam2SyncValue)
                            {
                                if (animParam2InvertDirection)
                                    animLever.SetFloat(animParam2, m_roll * -1);
                                else
                                    animLever.SetFloat(animParam2, m_roll);
                            }
                            else
                                animLever.SetFloat(animParam2, CalculateParameterValue(m_roll, animParam2InputMin, animParam2InputMax, animParam1OutputMin, animParam2OutputMax, animParam2InvertDirection));
                        }
                    }
                }
                // Rudder Pedals
                else if (m_leverType == LeverType.Pedal)
                {
                    if (m_animationType == AnimationType.Transform)
                    {
                        // ---------------------- ROTATE
                        if (m_pedalType == PedalType.Hinged)
                        {
                            float m_deflection = m_yaw * m_pedalDeflectionLimit;

                            if (m_rightPedal != null && m_leftPedal != null)
                            {
                                // ---------------------- ROTATE PEDALS
                                m_rightPedal.localRotation = m_baseRightRotation;
                                m_rightPedal.Rotate(m_rightAxis, m_deflection);
                                if (m_pedalMode == PedalMode.Combined)
                                {
                                    m_leftPedal.localRotation = m_baseLeftRotation;
                                    m_leftPedal.Rotate(m_leftAxis, m_deflection);
                                }
                                else
                                {
                                    m_leftPedal.localRotation = m_baseLeftRotation;
                                    m_leftPedal.Rotate(m_leftAxis, -m_deflection);
                                }
                            }
                        }


                        // ---------------------- MOVE
                        if (m_pedalType == PedalType.Sliding)
                        {
                            if (m_rightPedal != null && m_leftPedal != null)
                            {
                                float m_distance = m_yaw * (m_pedalSlidingLimit / 100);
                                //MOVE PEDALS
                                m_rightPedal.localPosition = m_baseRightPosition;
                                m_rightPedal.localPosition += m_rightAxis * m_distance;
                                m_leftPedal.localPosition = m_baseLeftPosition;
                                m_leftPedal.localPosition += m_leftAxis * m_distance;
                            }
                        }
                    }
                    else if (m_animationType == AnimationType.Animator)
                    {
                        if (animLever != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animLever.SetFloat(animParam1, m_yaw * -1);
                                else
                                    animLever.SetFloat(animParam1, m_yaw);
                            }
                            else
                                //animLever.SetFloat(animParam1, 1 - (m_controller.processedYaw + 1) / 2);
                                animLever.SetFloat(animParam1, CalculateParameterValue(m_yaw, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                        }
                    }
                }


                // Throttle
                else if (m_leverType == LeverType.Throttle || m_leverType == LeverType.Mixture || m_leverType == LeverType.PropellerPitch)
                {
                    float m_f = 0;
                    if (m_leverType == LeverType.Throttle) { m_f = m_throttle; }
                    if (m_leverType == LeverType.Mixture) { m_f = m_mixture; }
                    if (m_leverType == LeverType.PropellerPitch) { m_f = m_prop; }

                    if (m_animationType == AnimationType.Transform)
                    {
                        if (m_lever != null)
                        {
                            if (m_leverType == LeverType.Throttle && m_throttleMode == ThrottleMode.Deflection)
                            {
                                float m_rt = m_f * m_deflectionLimit;
                                m_lever.localRotation = m_baseLeverRotation;
                                m_lever.Rotate(m_axisDeflection, m_rt);
                            }
                            else
                            {
                                float m_rt = m_f * m_slidingLimit;
                                Vector3 m_rgt = m_lever.right;
                                if (m_movementAxis == MovementAxis.Forward) { m_rgt = m_lever.forward; }
                                if (m_movementAxis == MovementAxis.Right) { m_rgt = m_lever.right; }
                                if (m_movementAxis == MovementAxis.Up) { m_rgt = m_lever.up; }
                                if (m_movementDirection == MovementDirection.Inverted) { m_rgt *= -1; }

                                Vector3 m_worldPosition = m_controller.transform.TransformPoint(m_baseLeverPosition);
                                Vector3 m_position = m_worldPosition + (0.01f * m_rt * m_rgt);
                                m_lever.position = m_position;
                            }

                            m_lever.localRotation = m_baseLeverRotation;
                        }
                    }
                    else if (m_animationType == AnimationType.Animator)
                    {
                        if (animLever != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animLever.SetFloat(animParam1, m_f * -1);
                                else
                                    animLever.SetFloat(animParam1, m_f);
                            }
                            else
                                animLever.SetFloat(animParam1, CalculateParameterValue(m_f, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                        }
                    }

                }

                // Flaps
                else if (m_leverType == LeverType.Flaps)
                {
                    if (m_lever != null)
                    {
                        float m_deflection = _input * m_deflectionLimit;
                        m_lever.localRotation = m_baseLeverRotation;
                        m_lever.Rotate(m_axisDeflection, m_deflection);
                    }
                }

                // Gear
                else if (m_leverType == LeverType.GearIndicator)
                {
                    float _target = 0;

                    if (m_controller != null)
                    {
                        if (m_controller.m_gearState == Controller.GearState.Down) { _target = 0; }
                        if (m_controller.m_gearState == Controller.GearState.Up) { _target = 1; }
                        _leverInput = Mathf.MoveTowards(_leverInput, _target, m_controller._timestep * 0.5f);
                    }

                    if (m_animationType == AnimationType.Transform)
                    {
                        if (m_lever != null)
                        {
                            float m_deflection = _leverInput * m_deflectionLimit;
                            m_lever.localRotation = m_baseLeverRotation;
                            m_lever.Rotate(m_axisDeflection, m_deflection);
                        }
                    }
                    else if (m_animationType == AnimationType.Animator)
                    {
                        if (animLever != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animLever.SetFloat(animParam1, _leverInput * -1);
                                else
                                    animLever.SetFloat(animParam1, _leverInput);
                            }
                            else
                                animLever.SetFloat(animParam1, CalculateParameterValue(_leverInput, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                        }
                    }

                }

                // Collective
                else if (m_leverType == LeverType.Collective)
                {
                    if (m_animationType == AnimationType.Transform)
                    {
                        if (m_lever != null)
                        {
                            float m_deflection = m_controller._collectiveInput * m_deflectionLimit;
                            m_lever.localRotation = m_baseLeverRotation;
                            m_lever.Rotate(m_axisDeflection, m_deflection);
                        }
                    }
                    else if (m_animationType == AnimationType.Animator)
                    {
                        if (animLever != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animLever.SetFloat(animParam1, m_controller._collectiveInput * -1);
                                else
                                    animLever.SetFloat(animParam1, m_controller._collectiveInput);
                            }
                            else
                                animLever.SetFloat(animParam1, CalculateParameterValue(m_controller._collectiveInput, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                        }
                    }
                }
            }
        }

        // ************** by RF
        private float CalculateParameterValue(float input, float inputMin, float inputMax, float outputMin, float outputMax, bool invert)
        {
            float output = 0f;

            // check range
            if ((inputMin > inputMax) || (outputMin > outputMax))
            {
                Debug.LogError("Input range and/or animation parameter range is invalid. (min > max).");
                return 0;
            }

            // calculate multiplier
            float multiplier = (outputMax - outputMin) / (inputMax - inputMin);

            // project input value range to animation parameter value range
            if (invert)
            {
                output = outputMax - (input + (outputMin - inputMin)) * multiplier;
            }
            else
            {
                output = (input + (outputMin - inputMin)) * multiplier;
            }
            return output;
        }
        // ************** by RF
    }
    #endregion

    #region Editor

#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(SilantroLever))]

    public class SilantroLeverEditor : Editor
    {
        Color backgroundColor;
        Color silantroColor = Color.yellow; //new Color(1, 0.4f, 0);
        SilantroLever lever;
        //SerializedProperty lever;
        //SerializedProperty dial;
        //SerializedProperty readout;
        //SerializedProperty hsi;
        //SerializedProperty needleList;

        private static readonly GUIContent deleteButton = new GUIContent("Remove", "Delete");
        private static readonly GUILayoutOption buttonWidth = GUILayout.Width(60f);



        /// <summary>
        /// 
        /// </summary>
        private void OnEnable()
        {
            lever = (SilantroLever)target;
            //lever = serializedObject.FindProperty("m_lever");
            //dial = serializedObject.FindProperty("m_dial");
            //readout = serializedObject.FindProperty("m_readout");
            //hsi = serializedObject.FindProperty("m_hsi");
            //needleList = dial.FindPropertyRelative("m_needles");
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;
            //DrawDefaultInspector();
            serializedObject.Update();

            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leverType"), new GUIContent("Lever Type"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_animationType"), new GUIContent("Animation Type"));

            GUILayout.Space(3f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Lever Configuration", MessageType.None);
            GUI.color = backgroundColor;

            if (lever.m_animationType == SilantroLever.AnimationType.Transform)
            {

                if (lever.m_leverType == SilantroLever.LeverType.Stick)
                {
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_stickType"), new GUIContent("Stick Type"));

                    if (lever.m_stickType == SilantroLever.StickType.Joystick)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_lever"), new GUIContent("Stick Hinge "));

                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Roll Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxisRoll"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_directionRoll"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rollLimit"), new GUIContent("Maximum Deflection"));

                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Pitch Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxisPitch"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_directionPitch"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pitchLimit"), new GUIContent("Maximum Deflection"));
                    }
                    if (lever.m_stickType == SilantroLever.StickType.Yoke)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_lever"), new GUIContent("Yoke Hinge"));

                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxisRoll"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_directionRoll"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rollLimit"), new GUIContent("Deflection Limit ( °)"));

                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Movement Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_movementAxis"), new GUIContent("Movement Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_movementDirection"), new GUIContent("Movement Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pitchLimit"), new GUIContent("Movement Limit ( cm)"));
                    }
                    if (lever.m_stickType == SilantroLever.StickType.ControlColumn)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_lever"), new GUIContent("Column Hinge"));
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_yoke"), new GUIContent("Yoke Hinge"));

                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Roll Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxisRoll"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_directionRoll"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rollLimit"), new GUIContent("Maximum Deflection"));

                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Pitch Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxisPitch"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_directionPitch"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pitchLimit"), new GUIContent("Maximum Deflection"));
                    }
                }

                if (lever.m_leverType == SilantroLever.LeverType.Throttle)
                {
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_throttleMode"), new GUIContent(" "));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_lever"), new GUIContent("Hinge"));


                    if (lever.m_throttleMode == SilantroLever.ThrottleMode.Deflection)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_direction"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_deflectionLimit"), new GUIContent("Maximum Deflection"));
                    }

                    if (lever.m_throttleMode == SilantroLever.ThrottleMode.Sliding)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Sliding Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_movementAxis"), new GUIContent("Sliding Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_slidingLimit"), new GUIContent("Maximum Slide Distance"));
                    }
                }

                if (lever.m_leverType == SilantroLever.LeverType.Pedal)
                {
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pedalType"), new GUIContent("Pedal Type"));

                    if (lever.m_pedalType == SilantroLever.PedalType.Hinged)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Right Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rightPedal"), new GUIContent("Right Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rightRotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rightDirection"), new GUIContent("Rotation Deflection"));


                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Left Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leftPedal"), new GUIContent("Left Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leftRotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leftDirection"), new GUIContent("Rotation Deflection"));

                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pedalMode"), new GUIContent("Clamped Together"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pedalDeflectionLimit"), new GUIContent("Maximum Deflection"));
                    }

                    if (lever.m_pedalType == SilantroLever.PedalType.Sliding)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Right Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rightPedal"), new GUIContent("Right Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rightRotationAxis"), new GUIContent("Sliding Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rightDirection"), new GUIContent("Sliding Deflection"));


                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Left Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leftPedal"), new GUIContent("Left Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leftRotationAxis"), new GUIContent("Sliding Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_leftDirection"), new GUIContent("Sliding Deflection"));


                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Sliding Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pedalSlidingLimit"), new GUIContent("Sliding Distance (cm)"));
                    }
                }

                if (lever.m_leverType == SilantroLever.LeverType.Mixture || lever.m_leverType == SilantroLever.LeverType.PropellerPitch)
                {
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_lever"), new GUIContent("Hinge"));
                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Sliding Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_movementAxis"), new GUIContent("Sliding Axis"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_slidingLimit"), new GUIContent("Maximum Slide Distance"));
                }

                if (lever.m_leverType == SilantroLever.LeverType.Flaps ||
                    lever.m_leverType == SilantroLever.LeverType.GearIndicator ||
                    lever.m_leverType == SilantroLever.LeverType.Collective)
                {
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_lever"), new GUIContent("Hinge"));
                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_rotationAxis"), new GUIContent("Rotation Axis"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_direction"), new GUIContent("Rotation Direction"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_deflectionLimit"), new GUIContent("Maximum Deflection"));
                }
            }
            else if (lever.m_animationType == SilantroLever.AnimationType.Animator)
            {
                // Get Animator Controller
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animLever"), new GUIContent("Animator Component"));

                if (lever.animLever != null)
                {
                    // Get animator parameters
                    AnimatorControllerParameter[] animParams = (lever.animLever.runtimeAnimatorController as AnimatorController).parameters;
                    string[] animParamNames = new string[animParams.Length];
                    var idx = 0;
                    foreach (AnimatorControllerParameter param in animParams)
                    {
                        animParamNames[idx] = param.name;
                        idx++;
                    }

                    // Get animator layers
                    AnimatorControllerLayer[] animLayers = (lever.animLever.runtimeAnimatorController as AnimatorController).layers;
                    string[] animLayerNames = new string[animLayers.Length];
                    idx = 0;
                    foreach (AnimatorControllerLayer layer in animLayers)
                    {
                        animLayerNames[idx] = layer.name;
                        idx++;
                    }

                    // -------------------------------------------------------- Control Stick
                    if (lever.m_leverType == SilantroLever.LeverType.Stick)
                    {
                        AnimatorConfigInspector("Pitch", "Roll", animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Throttle
                    else if (lever.m_leverType == SilantroLever.LeverType.Throttle)
                    {
                        AnimatorConfigInspector("Throttle", null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Rudder Pedals
                    if (lever.m_leverType == SilantroLever.LeverType.Pedal)
                    {
                        AnimatorConfigInspector("Yaw", null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Flaps
                    if (lever.m_leverType == SilantroLever.LeverType.Flaps)
                    {
                        AnimatorConfigInspector("Flap", null, animParamNames, animLayerNames, animLayers);

                    }

                    // -------------------------------------------------------- Gear Indicator
                    if (lever.m_leverType == SilantroLever.LeverType.GearIndicator)
                    {
                        AnimatorConfigInspector("Gear Indicator", null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Collective
                    if (lever.m_leverType == SilantroLever.LeverType.Collective)
                    {
                        AnimatorConfigInspector("Collective", null, animParamNames, animLayerNames, animLayers);
                    }

                }
            }
            serializedObject.ApplyModifiedProperties();
        }

        private void AnimatorConfigInspector(string param1name, string param2name, string[] animParamNames, string[] animLayerNames, AnimatorControllerLayer[] animLayers)
        {
            GUILayout.Space(10f);
            GUI.color = Color.white;
            EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);

            if (param1name != null)
            {
                lever.animParamIdx1 = EditorGUILayout.Popup(param1name + " Animation Parameter", lever.animParamIdx1, animParamNames);
                lever.animParam1 = animParamNames[lever.animParamIdx1];
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1SyncValue"), new GUIContent(param1name + " Input Value = Anim Param Value?"));
                GUILayout.Space(3f);
                if (lever.animParam1SyncValue == false)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMin"), new GUIContent("Minimum " + param1name + " Input Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMax"), new GUIContent("Maximum " + param1name + " Input Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMin"), new GUIContent("Minimum " + param1name + " Animation Parameter Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMax"), new GUIContent("Maximum " + param1name + " Animation Parameter Value"));
                    GUILayout.Space(3f);
                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InvertDirection"), new GUIContent("Inverted " + param1name + " Value?"));
                GUILayout.Space(3f);
            }

            if (param2name != null)
            {

                lever.animParamIdx2 = EditorGUILayout.Popup(param2name + " Animation Parameter", lever.animParamIdx2, animParamNames);
                lever.animParam2 = animParamNames[lever.animParamIdx2];
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2SyncValue"), new GUIContent(param2name + " Input Value = Anim Param Value?"));
                GUILayout.Space(3f);
                if (lever.animParam2SyncValue == false)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2InputMin"), new GUIContent("Minimum " + param2name + " Input Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2InputMax"), new GUIContent("Maximum " + param2name + " Input Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2OutputMin"), new GUIContent("Minimum " + param2name + " Animation Parameter Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2OutputMax"), new GUIContent("Maximum " + param2name + " Animation Parameter Value"));
                    GUILayout.Space(3f);
                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2InvertDirection"), new GUIContent("Inverted " + param2name + " Value?"));
                GUILayout.Space(3f);
            }

            lever.animLayerIdx = EditorGUILayout.Popup("Animation Layer", lever.animLayerIdx, animLayerNames);
            lever.animLayer = animLayerNames[lever.animLayerIdx];
            GUILayout.Space(3f);

            // Get animation states
            ChildAnimatorState[] smStates = animLayers[lever.animLayerIdx].stateMachine.states;
            string[] smStateNames = new string[smStates.Length];
            var idx = 0;
            foreach (ChildAnimatorState state in smStates)
            {
                smStateNames[idx] = state.state.name;
                idx++;
            }

            lever.smStateIdx = EditorGUILayout.Popup("Animation State", lever.smStateIdx, smStateNames);
            lever.smState = smStateNames[lever.smStateIdx];
            GUILayout.Space(3f);
        }
    }

#endif

    #endregion
}

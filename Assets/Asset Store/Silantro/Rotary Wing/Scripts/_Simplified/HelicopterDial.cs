using System;
using UnityEngine;
using Oyedoyin.Old;
using Oyedoyin.Common;
using Oyedoyin.RotaryWing;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HelicopterSim
{
    /// <summary>
    ///
    /// 
    /// Use:		 Handles the analogue data display
    /// </summary>

    public class HelicopterDial : MonoBehaviour
    {

        // ------------------------------------- Selectibles
        public enum DialType
        {
            Speed, Altitude, Fuel, VerticalSpeed, RPM, BankLevel, Compass, ArtificialHorizon, FuelFlow, EGT, RadarAltitude //Stabilator, 
        }
        public DialType dialType = DialType.Speed;
        public enum SpeedUnit { Knots, MPH, KPH, FtPMin, MPS } // edited by RF
        public SpeedUnit speedUnit = SpeedUnit.Knots;
        public SpeedUnit vertSpeedUnit = SpeedUnit.FtPMin; // by RF
        public enum FuelUnit { kiloGrams, Pounds, Gallons }
        public FuelUnit fuelUnit = FuelUnit.kiloGrams;
        public enum AltitudeUnit { Meter, Feet };
        public AltitudeUnit altitudeUnit = AltitudeUnit.Feet;
        public enum RotationMode { Free, Clamped }
        public RotationMode rotationMode = RotationMode.Clamped;
        public enum NeedleType { Single, Dual }
        public NeedleType needleType = NeedleType.Single;
        public enum MovementType { Transform, Animator }; // by RF
        public MovementType movementType = MovementType.Transform; // by RF
        //public enum AnimatorPlayMode { Simulation, Animation }; // by RF
        //public AnimatorPlayMode animatorPlayMode = AnimatorPlayMode.Simulation; // by RF
        public bool forceSimulationMode = false;

        public enum RotationAxis { X, Y, Z }
        public RotationAxis rotationAxis = RotationAxis.X;
        public RotationAxis supportRotationAxis = RotationAxis.X;
        public RotationAxis rollRotationAxis = RotationAxis.X;
        public RotationAxis yawRotationAxis = RotationAxis.X;
        public RotationAxis pitchRotationAxis = RotationAxis.X;
        public enum RotationDirection { CW, CCW }
        public RotationDirection direction = RotationDirection.CW;
        public RotationDirection supportDirection = RotationDirection.CW;
        public RotationDirection pitchDirection = RotationDirection.CW;
        public RotationDirection rollDirection = RotationDirection.CW;
        public RotationDirection yawDirection = RotationDirection.CW;
        public enum EngineType { TurboShaft, ElectricMotor, PistonEngine }
        public EngineType engineType = EngineType.PistonEngine;

        // ------------------------------------- by RF
        //public enum ModelType { Realistic, Arcade }
        //public ModelType m_type = ModelType.Realistic;
        public enum AltimeterType { ThreeDigit, FourDigit }
        public AltimeterType altimeterType = AltimeterType.ThreeDigit;
        public enum AttitudeIndicatorType { Classic, GlassInstrument }
        public AttitudeIndicatorType attitudeIndicatorType = AttitudeIndicatorType.Classic;
        // -------------------------------------

        // ------------------------------------- Connections
        //public HelicopterController controller;
        public Transform needle, supportNeedle;
        //public PhantomControlModule dataLog;
        public SilantroTurboshaft turboshaft;
        public SilantroPiston piston;
        //public PhantomAerofoil stabilator;

        // ------------------------------------- by RF {
        public Animator animator;
        [Serializable]
        public class AnimLayer
        {
            public int layerIdx, smStateIdx;
            public string layerName, smState;
            public int paramIdx1, paramIdx2, paramIdx3;
            public string param1 = "", param2 = "", param3 = "";
            //public float param1InputMin, param1InputMax, param1OutputMin, param1OutputMax;
            //public float param2InputMin, param2InputMax, param2OutputMin, param2OutputMax;
            //public float param3InputMin, param3InputMax, param3OutputMin, param3OutputMax;
            //public bool param1SyncValue, param2SyncValue, param3SyncValue;
            //public bool param1InvertDirection, param2InvertDirection, param3InvertDirection;
        }
        public AnimLayer animLayer1 = new AnimLayer();
        public AnimLayer animLayer2 = new AnimLayer();

        public HelicopterController m_controller;

        public float minimumPosition, maximumPosition;

        // animation parameters
        public int animParamIdx1, animParamIdx2, animLayerIdx, smStateIdx;
        public string animParam1, animParam2, animLayer, smState;

        // animation clip frame
        public float maximumFrame;

        // ------------------------------------- by RF }

        // ------------------------------------- Vectors
        Vector3 axisRotation, supportAxisRotation;
        Vector3 pitchAxisRotation, yawAxisRotation, rollAxisRotation;

        private Quaternion baseNormalRotation = Quaternion.identity;
        private Vector3 baseNormalPosition; // by RF
        private Quaternion baseSupportRotation = Quaternion.identity;

        // ------------------------------------- Variables
        public float currentValue, currentSupportValue;
        public float currentValue2, currentValue3, currentValue4, currentValue5, currentValue6;
        public float minimumValue = 0.0f;
        public float maximumValue = 100.0f;
        public float MinimumAngleDegrees = 0.0f;
        public float MaximumAngleDegrees = 360.0f;

        //public float multiplier = 1f;

        // ------------------------------------- by RF {
        // ----- Readouts Digits & Texts

        public bool useReadout = true;

        public float digitOne;
        public float digitTwo;
        public float digitThree;
        public float digitFour;

        public float digitOneTranslation;
        public float digitTwoTranslation;
        public float digitThreeTranslation;
        public float digitFourTranslation;

        public float digitSmallestValue;
        public float digitOneModder, digitTwoModder, digitThreeModder, digitFourModder;

        private Vector3 digitOneBasePosition;
        private Vector3 digitTwoBasePosition;
        private Vector3 digitThreeBasePosition;
        private Vector3 digitFourBasePosition;

        public Transform digitOneContainer;
        public Transform digitTwoContainer;
        public Transform digitThreeContainer;
        public Transform digitFourContainer;

        public TMPro.TMP_Text[] readoutTexts;
        public string readoutTextFormat = "F0";


        public float oneDegreeDistance = 1f; // for horizon glass instrument pitch
        // ------------------------------------- by RF }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        public void InitializeDial()
        {
            switch (movementType)
            {
                case MovementType.Transform:
                    axisRotation = Handler.EstimateModelProperties(direction.ToString(), rotationAxis.ToString());
                    pitchAxisRotation = Handler.EstimateModelProperties(pitchDirection.ToString(), pitchRotationAxis.ToString());
                    rollAxisRotation = Handler.EstimateModelProperties(rollDirection.ToString(), rollRotationAxis.ToString());
                    yawAxisRotation = Handler.EstimateModelProperties(yawDirection.ToString(), yawRotationAxis.ToString());
                    supportAxisRotation = Handler.EstimateModelProperties(supportDirection.ToString(), supportRotationAxis.ToString());

                    if (needle != null)
                    {
                        baseNormalRotation = needle.localRotation;
                        baseNormalPosition = needle.localPosition;
                    }
                    else
                    {
                        Debug.LogError("needle for Dial " + transform.name + " has not been assigned");
                        return;
                    }
                    if (supportNeedle != null)
                        baseSupportRotation = supportNeedle.localRotation;
                    break;

                case MovementType.Animator:
                    //case MovementType.AnimationFrame:
                    //Debug.Log("Dial Type: " + dialType.ToString() + ", Animator Play: " + animLayer + "." + smState);
                    //animator.Play(animLayer + "." + smState);

                    if (animator != null)
                    {
                        if (animLayer1 != null && animLayer1.layerName != "" && animLayer1.smState != "")
                            animator.Play(animLayer1.layerName + "." + animLayer1.smState);
                        if (animLayer2 != null && animLayer2.layerName != "" && animLayer2.smState != "")
                            animator.Play(animLayer2.layerName + "." + animLayer2.smState);
                    }
                    break;
            }

            // Init digit readout
            if (digitOneContainer != null) { digitOneBasePosition = digitOneContainer.localPosition; }
            if (digitTwoContainer != null) { digitTwoBasePosition = digitTwoContainer.localPosition; }
            if (digitThreeContainer != null) { digitThreeBasePosition = digitThreeContainer.localPosition; }
            if (digitFourContainer != null) { digitFourBasePosition = digitFourContainer.localPosition; }

        }

        private void Update()
        {
            // set simulation on update
            if (m_controller.m_playMode == HelicopterController.PlayMode.Simulation || forceSimulationMode)
            {
                // ------------------------------------- by RF {
                CollectSimulationData();

                switch (movementType)
                {
                    case MovementType.Transform:
                        MoveNeedle();
                        break;
                    case MovementType.Animator:
                        AnimateNeedle();
                        break;
                }
                if (useReadout)
                    SetReadoutDigits();
                if (readoutTexts != null)
                    SetReadoutText();

                // ------------------------------------- by RF }
            }
        }

        private void LateUpdate()
        {
            // set animation on lateupdate, after getting recorded data, only for Animator movement
            if (m_controller.m_playMode == HelicopterController.PlayMode.Animation && !forceSimulationMode && movementType == MovementType.Animator)
            {
                // ------------------------------------- by RF {
                CollectRecordedData();

                /*switch (movementType)
                {
                    case MovementType.Transform:
                        MoveNeedle();
                        break;
                    case MovementType.Animator:
                        AnimateNeedle();
                        break;
                }*/
                AnimateNeedle();

                if (useReadout)
                    SetReadoutDigits();
                if (readoutTexts != null)
                    SetReadoutText();
                // ------------------------------------- by RF }
            }
        }


        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        void CollectSimulationData()
        {
            //****************** Get Current Value from Simulation Data *****************

            if (m_controller != null)
            {
                switch (dialType)
                {
                    // --------------------------------- Air/Horizontal Speed
                    case DialType.Speed:
                        {
                            //float baseSpeed = m_controller.vz;
                            //float baseSpeed = m_controller.m_horzSpeed;
                            float baseSpeed = m_controller.m_airspeed;

                            if (speedUnit == SpeedUnit.Knots)
                                currentValue = MathBase.ConvertSpeed(baseSpeed, "KTS");
                            else if (speedUnit == SpeedUnit.MPH)
                                currentValue = MathBase.ConvertSpeed(baseSpeed, "MPH");
                            else if (speedUnit == SpeedUnit.KPH)
                                currentValue = MathBase.ConvertSpeed(baseSpeed, "KPH");
                            else if (speedUnit == SpeedUnit.FtPMin)
                                currentValue = MathBase.ConvertSpeed(baseSpeed, "FtPMin");
                            break;
                        }

                    // -------------------------------- Climb Rate / Vertical Speed
                    case DialType.VerticalSpeed:
                        {
                            //float baseVertSpeed = m_controller.m_vertical_speed;
                            float baseVertSpeed = m_controller.m_vertSpeed;

                            if (vertSpeedUnit == SpeedUnit.FtPMin)
                                currentValue = MathBase.ConvertSpeed(baseVertSpeed, "FtPMin") / 1000f;
                            else if (vertSpeedUnit == SpeedUnit.Knots)
                                currentValue = MathBase.ConvertSpeed(baseVertSpeed, "KTS");
                            else if (vertSpeedUnit == SpeedUnit.MPH)
                                currentValue = MathBase.ConvertSpeed(baseVertSpeed, "MPH");
                            else if (vertSpeedUnit == SpeedUnit.KPH)
                                currentValue = MathBase.ConvertSpeed(baseVertSpeed, "KPH");
                            break;
                        }

                    // -------------------------------- Bank
                    case DialType.BankLevel:
                        {
                            //currentValue = -1 * m_controller.m_bankAngle;
                            var bankAngle = -1 * m_controller.m_bankAngle;
                            bankAngle = bankAngle % 360;
                            if (movementType == MovementType.Transform)
                                currentValue = bankAngle;
                            else if (movementType == MovementType.Animator)
                            {
                                currentValue = bankAngle;
                                currentValue2 = Mathf.Cos(bankAngle * Mathf.Deg2Rad);
                                currentValue3 = Mathf.Sin(bankAngle * Mathf.Deg2Rad);
                            }
                            break;
                        }

                    // -------------------------------- Compass
                    case DialType.Compass:
                        {
                            //currentValue = m_controller.m_heading; //by RF
                            var heading = m_controller.m_heading;
                            heading = heading % 360;
                            if (movementType == MovementType.Transform)
                                currentValue = heading;
                            else if (movementType == MovementType.Animator)
                            {
                                currentValue = heading;
                                currentValue2 = Mathf.Cos(heading * Mathf.Deg2Rad);
                                currentValue3 = Mathf.Sin(heading * Mathf.Deg2Rad);
                            }
                            break;
                        }

                    // -------------------------------- Altitude
                    case DialType.Altitude:
                        {
                            //float baseAltitude = MathBase.ConvertDistance(m_controller.transform.position.y, "FT");
                            //float baseAltitude = m_controller.transform.position.y * MathBase.toFt;
                            float baseAltitude = m_controller.m_altitude;
                            float altitude;
                            switch (altitudeUnit)
                            {
                                case AltitudeUnit.Meter:
                                    altitude = baseAltitude; /// 1000f;
                                    currentSupportValue = Mathf.Floor(baseAltitude / 10); // /10000f);
                                    break;
                                case AltitudeUnit.Feet:
                                    altitude = baseAltitude * MathBase.toFt;
                                    currentSupportValue = Mathf.Floor((baseAltitude * MathBase.toFt) / 10); // /10000f);
                                    break;
                                default:
                                    altitude = baseAltitude;
                                    currentSupportValue = Mathf.Floor(baseAltitude / 10);
                                    break;
                            }
                            if (movementType == MovementType.Transform)
                                currentValue = altitude;
                            else if (movementType == MovementType.Animator)
                            {
                                currentValue = altitude;
                                currentValue2 = Mathf.Cos((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                currentValue3 = Mathf.Sin((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                            }
                            break;
                        }

                    // -------------------------------- Radar Altitude
                    case DialType.RadarAltitude:
                        {
                            float baseAltitude = m_controller.m_radarAltitude;
                            float altitude = baseAltitude;
                            switch (altitudeUnit)
                            {
                                case AltitudeUnit.Meter:
                                    altitude = baseAltitude; /// 1000f;
                                    currentSupportValue = Mathf.Floor(baseAltitude / 10); // /10000f);
                                    break;
                                case AltitudeUnit.Feet:
                                    altitude = baseAltitude * MathBase.toFt;
                                    currentSupportValue = Mathf.Floor((baseAltitude * MathBase.toFt) / 10); // /10000f);
                                    break;
                            }
                            if (movementType == MovementType.Transform)
                                currentValue = altitude;
                            else if (movementType == MovementType.Animator)
                            {
                                currentValue = altitude;
                                currentValue2 = Mathf.Cos((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                currentValue3 = Mathf.Sin((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                            }
                            break;
                        }

                    // -------------------------------- Artificial Horizon
                    case DialType.ArtificialHorizon:
                        {
                            if (movementType == MovementType.Transform)
                            {
                                currentValue = m_controller.transform.eulerAngles.x; // pitch
                                currentValue2 = m_controller.transform.eulerAngles.z; // roll
                                currentValue3 = m_controller.transform.eulerAngles.y; // yaw
                            }
                            else if (movementType == MovementType.Animator)
                            {
                                var pitch = m_controller.transform.eulerAngles.x; // pitch
                                pitch = pitch % 360;
                                var roll = m_controller.transform.eulerAngles.z; // roll
                                roll = roll % 360;

                                if (attitudeIndicatorType == AttitudeIndicatorType.Classic)
                                {
                                    currentValue = pitch;
                                    currentValue2 = Mathf.Cos(pitch * Mathf.Deg2Rad); // pitch X
                                    currentValue3 = Mathf.Sin(pitch * Mathf.Deg2Rad); // pitch Y
                                }
                                else if (attitudeIndicatorType == AttitudeIndicatorType.GlassInstrument)
                                {
                                    // convert angle to be in between -90 to 90
                                    var xpitch = pitch;
                                    if (pitch >= 90 && pitch < 180)
                                        xpitch = 180 - pitch;
                                    else if (pitch >= 180 && pitch < 270)
                                        xpitch = 180 - pitch; // negative
                                    else if (pitch >= 270 && pitch < 360)
                                        xpitch = pitch - 360; // negative

                                    currentValue = xpitch;
                                    currentValue2 = 1; // not used in glass instrument
                                    currentValue3 = 0; // not used in glass instrument
                                }
                                currentValue4 = roll;
                                currentValue5 = Mathf.Cos(roll * Mathf.Deg2Rad); // pitch X
                                currentValue6 = Mathf.Sin(roll * Mathf.Deg2Rad); // pitch Y
                            }
                            break;
                        }

                    // -------------------------------- Fuel
                    case DialType.Fuel:
                        {
                            float baseFuel = m_controller.fuelLevel; // / multiplier;
                            if (fuelUnit == FuelUnit.kiloGrams)
                                currentValue = baseFuel;
                            else if (fuelUnit == FuelUnit.Pounds)
                                currentValue = baseFuel * 2.20462f;
                            else if (fuelUnit == FuelUnit.Gallons)
                                currentValue = baseFuel * 0.264172053f;
                            break;
                        }

                    // -------------------------------- EGT
                    case DialType.EGT:
                        {
                            float baseTemp = 0f;

                            //if (engineType == EngineType.TurboShaft) { baseTemp = shaft.Te; }
                            //if (engineType == EngineType.PistonEngine) { baseTemp = piston.Te; }
                            baseTemp = m_controller.processedThrottle * 100f;
                            //currentValue = baseTemp / 100f; if (currentValue < 0) { currentValue = 0f; }
                            currentValue = baseTemp;
                            if (currentValue < 0)
                                currentValue = 0f;
                            //currentSupportValue = Mathf.Floor(baseTemp % 100.0f) / 10f; if (currentSupportValue < 0) { currentSupportValue = 0f; }
                            currentSupportValue = Mathf.Floor(baseTemp) / 10f;
                            if (currentSupportValue < 0)
                                currentSupportValue = 0f;
                            break;
                        }

                    // -------------------------------- RPM
                    case DialType.RPM:
                        {
                            if (engineType == EngineType.TurboShaft && turboshaft != null)
                            {
                                //currentValue = shaft.core.coreFactor * 100; 
                                //currentValue = m_controller.coreRPM;
                                currentValue = m_controller.coreFactor * 100;
                            }
                            if (engineType == EngineType.PistonEngine && piston != null)
                            {
                                //currentValue = piston.core.coreFactor * 100;
                                //currentValue = m_controller.coreRPM;
                                currentValue = m_controller.coreFactor * 100;
                            }
                            currentSupportValue = Mathf.Floor(currentValue % 10.0f);
                            break;
                        }

                    // -------------------------------- Stab
                    /*case DialType.Stabilator:
                        {
                            currentValue = stabilator.controlDeflection;
                            break;
                        }*/

                    // -------------------------------- Fuel Flow
                    case DialType.FuelFlow:
                        {
                            //float baseFuel = controller.fuelSystem.engineConsumption / multiplier;
                            float baseFuel = m_controller.fuelConsumption; // / multiplier; // edited by RF
                            if (fuelUnit == FuelUnit.kiloGrams)
                                currentValue = baseFuel;
                            if (fuelUnit == FuelUnit.Pounds)
                                currentValue = baseFuel * 2.20462f;
                            if (fuelUnit == FuelUnit.Gallons)
                                currentValue = baseFuel * 0.264172053f;
                            break;
                        }

                }

                // --------------------- Clamp
                if (movementType == MovementType.Transform)
                {
                    if (rotationMode == RotationMode.Clamped)
                    {
                        currentValue = Mathf.Clamp(currentValue, minimumValue, maximumValue);
                    }
                }
            }
        }
    

        public void CollectRecordedData()
        {
            //****************** Get Current Value from Recorded Data *****************
            if (animator != null)
            {
                switch (dialType)
                {
                    case DialType.BankLevel: // Turn, BankLevel, Bank Angle
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "")
                            {
                                var bankAngle = animator.GetFloat(animLayer1.param1);
                                if (movementType == MovementType.Transform)
                                    currentValue = bankAngle;
                                else if (movementType == MovementType.Animator)
                                {
                                    currentValue = bankAngle;
                                    currentValue2 = Mathf.Cos(bankAngle * Mathf.Deg2Rad);
                                    currentValue3 = Mathf.Sin(bankAngle * Mathf.Deg2Rad);
                                }
                            }
                            break;
                        }
                    case DialType.Compass: // Airspeed
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "")
                            {
                                var heading = animator.GetFloat(animLayer1.param1);
                                if (movementType == MovementType.Transform)
                                    currentValue = heading;
                                else if (movementType == MovementType.Animator)
                                {
                                    currentValue = heading;
                                    currentValue2 = Mathf.Cos(heading * Mathf.Deg2Rad);
                                    currentValue3 = Mathf.Sin(heading * Mathf.Deg2Rad);
                                }
                            }
                            break;
                        }
                    case DialType.Altitude: // Altitude
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "")
                            {
                                var altitude = animator.GetFloat(animLayer1.param1); ;
                                if (movementType == MovementType.Transform)
                                    currentValue = altitude;
                                else if (movementType == MovementType.Animator)
                                {
                                    currentValue = altitude;
                                    currentValue2 = Mathf.Cos((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                    currentValue3 = Mathf.Sin((altitude % 1000) / 1000 * 360 * Mathf.Deg2Rad);
                                }
                            }
                            break;
                        }
                    case DialType.ArtificialHorizon: // Horizon, Attitude
                        {
                            if (animLayer1.param1 != null && animLayer1.param1 != "" && animLayer2.param1 != null && animLayer2.param1 != "")
                            {
                                var pitch = animator.GetFloat(animLayer1.param1); // pitch
                                var roll = animator.GetFloat(animLayer2.param1); // roll
                                if (attitudeIndicatorType == AttitudeIndicatorType.Classic)
                                {
                                    currentValue = pitch;
                                    currentValue2 = Mathf.Cos(pitch * Mathf.Deg2Rad); // pitch X
                                    currentValue3 = Mathf.Sin(pitch * Mathf.Deg2Rad); // pitch Y
                                }
                                else if (attitudeIndicatorType == AttitudeIndicatorType.GlassInstrument)
                                {
                                    if (pitch >= 90 && pitch < 180)
                                        pitch = 180 - pitch;
                                    else if (pitch >= 180 && pitch < 270)
                                        pitch = 180 - pitch; // negative
                                    else if (pitch >= 270 && pitch < 360)
                                        pitch = pitch - 360; // negative
                                    currentValue = pitch;
                                    currentValue2 = 1;
                                    currentValue3 = 0;
                                }
                                currentValue4 = roll;
                                currentValue5 = Mathf.Cos(roll * Mathf.Deg2Rad); // pitch X
                                currentValue6 = Mathf.Sin(roll * Mathf.Deg2Rad); // pitch Y
                            }
                            break;
                        }
                    default:
                        {
                            if (animLayer1 != null)
                            {
                                if (animLayer1.param1 != null && animLayer1.param1 != "")
                                    currentValue = animator.GetFloat(animLayer1.param1);
                                if (animLayer1.param2 != null && animLayer1.param2 != "")
                                    currentValue2 = animator.GetFloat(animLayer1.param2);
                                if (animLayer1.param3 != null && animLayer1.param3 != "")
                                    currentValue3 = animator.GetFloat(animLayer1.param3);
                            }
                            if (animLayer2 != null)
                            {
                                if (animLayer2.param1 != null && animLayer2.param1 != "")
                                    currentValue4 = animator.GetFloat(animLayer2.param1);
                                if (animLayer2.param2 != null && animLayer2.param2 != "")
                                    currentValue5 = animator.GetFloat(animLayer2.param2);
                                if (animLayer2.param3 != null && animLayer2.param3 != "")
                                    currentValue6 = animator.GetFloat(animLayer2.param3);
                            }
                            break;
                        }
                }
            }                            
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        void MoveNeedle()
        {
            if (needle != null)
            {
                if (dialType == DialType.EGT || dialType == DialType.RPM)
                {
                    float baseDelta = (currentValue - minimumValue) / (maximumValue - minimumValue);
                    float supportDelta = (currentSupportValue) / 10;

                    float baseDeltaRotation = MinimumAngleDegrees + ((MaximumAngleDegrees - MinimumAngleDegrees) * baseDelta);
                    float supportDeltaRotation = (360 * supportDelta);

                    // -----------------  Set
                    needle.transform.localRotation = baseNormalRotation; needle.transform.Rotate(axisRotation, baseDeltaRotation);
                    if (supportNeedle != null) { supportNeedle.transform.localRotation = baseSupportRotation; supportNeedle.transform.Rotate(supportAxisRotation, supportDeltaRotation); }
                }

                else if (dialType == DialType.Compass)
                {
                    //float baseDelta = currentValue / 360f; 
                    //float baseDeltaRotation = (360 * baseDelta);

                    // -----------------  Set
                    needle.transform.localRotation = baseNormalRotation;
                    //needle.transform.Rotate(axisRotation, baseDeltaRotation);
                    needle.transform.Rotate(axisRotation, currentValue);
                }

                else if (dialType == DialType.ArtificialHorizon)
                {
                    float yaw = 0f, pitch = 0f, roll = 0f;

                    if (m_controller != null)
                    {
                        yaw = m_controller.transform.eulerAngles.y;
                        pitch = m_controller.transform.eulerAngles.x;
                        roll = m_controller.transform.eulerAngles.z;
                    }
                    // ------------------------------------- by RF }
                    /*if (yawDirection == RotationDirection.CCW) { yaw *= -1f; }
                    if (pitchDirection == RotationDirection.CCW) { pitch *= -1f; }
                    if (rollDirection == RotationDirection.CCW) { roll *= -1f; }
                    */

                    var yawEffect = Quaternion.AngleAxis(yaw, yawAxisRotation);
                    var pitchEffect = Quaternion.AngleAxis(pitch, pitchAxisRotation);
                    var rollEffect = Quaternion.AngleAxis(roll, rollAxisRotation);

                    /*if (yawDirection == RotationDirection.CCW) { yawEffect = Quaternion.Inverse(yawEffect); }
                    if (pitchDirection == RotationDirection.CCW) { pitchEffect = Quaternion.Inverse(pitchEffect); }
                    if (rollDirection == RotationDirection.CCW) { rollEffect = Quaternion.Inverse(rollEffect); }
                    */

                    if (attitudeIndicatorType == AttitudeIndicatorType.Classic)
                    {
                        var angleEffect = yawEffect * pitchEffect * rollEffect;
                        needle.localRotation = baseNormalRotation * angleEffect;
                    }
                    else if (attitudeIndicatorType == AttitudeIndicatorType.GlassInstrument)
                    {
                        //needle.localRotation = baseNormalRotation * angleEffect;
                        supportNeedle.localRotation = Quaternion.Inverse(rollEffect);
                        var dialPitch = pitch;
                        if (pitch > 180) { dialPitch = pitch - 360; }
                        else if (pitch < -180) { dialPitch = pitch + 360; }
                        needle.localPosition = baseNormalPosition + needle.transform.up * dialPitch * oneDegreeDistance;
                        //Debug.Log("Pitch: " + pitch.ToString("F4"));
                    }
                }
                else if (dialType == DialType.Altitude)
                {
                    float baseDelta = currentValue / 1000f; // / 10;
                    float supportDelta = (currentSupportValue) / 10000f;

                    float baseDeltaRotation = (360 * baseDelta);
                    float supportDeltaRotation = (360 * supportDelta);

                    // -----------------  Set
                    needle.transform.localRotation = baseNormalRotation;
                    needle.transform.Rotate(axisRotation, baseDeltaRotation);
                    if (supportNeedle != null)
                    {
                        supportNeedle.transform.localRotation = baseSupportRotation;
                        supportNeedle.transform.Rotate(supportAxisRotation, supportDeltaRotation);
                    }

                    /*if (useReadout)
                    {
                        //EXTRACT DIGITS
                        digitOne = (currentValue % 100.0f) / 20.0f; //20FT Spacing
                        digitTwo = (currentValue % 1000.0f) / 100.0f;
                        digitThree = (currentValue % 10000.0f) / 1000.0f;
                        digitFour = (currentValue % 100000.0f) / 10000.0f;

                        //CALCULATE DIAL POSITIONS
                        float digitOnePosition = digitOne * -digitOneTranslation;
                        float digitTwoPosition = Mathf.Floor(digitTwo) * -digitTwoTranslation;
                        if ((digitOne * 20) > 90.0f)
                        {
                            digitTwoPosition += ((digitOne * 20f) - 90.0f) / 10.0f * -digitTwoTranslation;
                        }
                        float digitThreePosition = Mathf.Floor(digitThree) * -digitThreeTranslation;
                        if ((digitTwo * 100) > 990.0f)
                        {
                            digitThreePosition += ((digitTwo * 100) - 990.0f) / 10.0f * -digitThreeTranslation;
                        }
                        float digitFourPosition = 0f;
                        if (altimeterType == AltimeterType.FourDigit)
                        {
                            digitFourPosition = Mathf.Floor(digitFour) * -digitFourTranslation;
                            if ((digitThree * 1000) > 9990.0f)
                            {
                                digitFourPosition += ((digitThree * 1000) - 9990.0f) / 10f * -digitFourTranslation;
                            }
                        }

                        //SET POSITIONS
                        if (digitOneContainer != null) { digitOneContainer.localPosition = digitOneBasePosition + new Vector3(0, digitOnePosition, 0); }
                        if (digitTwoContainer != null) { digitTwoContainer.localPosition = digitTwoBasePosition + new Vector3(0, digitTwoPosition, 0); }
                        if (digitThreeContainer != null) { digitThreeContainer.localPosition = digitThreeBasePosition + new Vector3(0, digitThreePosition, 0); }
                        if (altimeterType == AltimeterType.FourDigit) { if (digitFourContainer != null) { digitFourContainer.localPosition = digitFourBasePosition + new Vector3(0, digitFourPosition, 0); } }
                    }*/
                }
                else if (dialType == DialType.RadarAltitude)
                {
                    // -----------------  Set
                    float radarAltitudeTranslation = currentValue / maximumValue * (maximumPosition - minimumPosition);
                    needle.localPosition = baseNormalPosition - needle.transform.up * radarAltitudeTranslation;
                }
                else if (dialType == DialType.Speed)
                {
                    float valueDelta = (currentValue - minimumValue) / (maximumValue - minimumValue);
                    float angleDeltaDegrees = MinimumAngleDegrees + ((MaximumAngleDegrees - MinimumAngleDegrees) * valueDelta);

                    // ------------------------ Set
                    needle.transform.localRotation = baseNormalRotation;
                    needle.transform.Rotate(axisRotation, angleDeltaDegrees);

                    /*if (useReadout)
                    {
                        //EXTRACT DIGITS
                        digitOne = currentValue % 10f;
                        digitTwo = (currentValue % 100.0f) / 10.0f;
                        digitThree = (currentValue % 1000.0f) / 100.0f;

                        //CALCULATE DIAL POSITIONS
                        float digitOnePosition = digitOne * -digitOneTranslation;
                        float digitTwoPosition = Mathf.Floor(digitTwo) * -digitTwoTranslation;
                        if (digitOne > 9.0f) 
                        { 
                            digitTwoPosition += (digitOne - 9.0f) * -digitTwoTranslation; 
                        }
                        float digitThreePosition = Mathf.Floor(digitThree) * -digitThreeTranslation;
                        if ((digitTwo * 10f) > 99.0f) 
                        { 
                            digitThreePosition += ((digitTwo * 10f) - 99.0f) * -digitThreeTranslation; 
                        }

                        //SET POSITIONS
                        if (digitOneContainer != null) { digitOneContainer.localPosition = digitOneBasePosition + new Vector3(0, digitOnePosition, 0); }
                        if (digitTwoContainer != null) { digitTwoContainer.localPosition = digitTwoBasePosition + new Vector3(0, digitTwoPosition, 0); }
                        if (digitThreeContainer != null) { digitThreeContainer.localPosition = digitThreeBasePosition + new Vector3(0, digitThreePosition, 0); }
                    }*/
                }
                else if (dialType == DialType.Fuel)
                {
                    float fuelAmountTranslation = m_controller.fuelLevel / m_controller.fuelCapacity * (maximumPosition - minimumPosition);
                    needle.localPosition = baseNormalPosition + needle.transform.up * fuelAmountTranslation;
                }
                else if (dialType == DialType.VerticalSpeed)
                {
                    float valueDelta = (currentValue - minimumValue) / (maximumValue - minimumValue);
                    float angleDeltaDegrees = MinimumAngleDegrees + ((MaximumAngleDegrees - MinimumAngleDegrees) * valueDelta);

                    // ------------------------ Set
                    needle.transform.localRotation = baseNormalRotation;
                    needle.transform.Rotate(axisRotation, angleDeltaDegrees);
                }
                else // generic dials
                {
                    float valueDelta = (currentValue - minimumValue) / (maximumValue - minimumValue);
                    float angleDeltaDegrees = MinimumAngleDegrees + ((MaximumAngleDegrees - MinimumAngleDegrees) * valueDelta);

                    // ------------------------ Set
                    needle.transform.localRotation = baseNormalRotation;
                    needle.transform.Rotate(axisRotation, angleDeltaDegrees);
                }
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        void AnimateNeedle()
        {
            //animator.SetFloat(animParam1, currentValue);
            if (animator != null)
            {
                if (animLayer1 != null)
                {
                    if (animLayer1.param1 != "")
                        animator.SetFloat(animLayer1.param1, currentValue);
                    if (animLayer1.param2 != "")
                        animator.SetFloat(animLayer1.param2, currentValue2);
                    if (animLayer1.param3 != "")
                        animator.SetFloat(animLayer1.param3, currentValue3);
                }
                if (animLayer2 != null)
                {
                    if (animLayer2.param1 != "")
                        animator.SetFloat(animLayer2.param1, currentValue4);
                    if (animLayer2.param2 != "")
                        animator.SetFloat(animLayer2.param2, currentValue5);
                    if (animLayer2.param3 != "")
                        animator.SetFloat(animLayer2.param3, currentValue6);
                }
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        /*void ScrubNeedle()
        {
            float frame = ((currentValue - minimumValue) / Math.Abs(maximumValue - minimumValue)) * maximumFrame;
            animator.speed = 0f;
            animator.Play(smState, animLayerIdx, (frame/maximumFrame));
        }*/

        //****************** READOUT DIGITS *****************
        void SetReadoutDigits()
        {
            float readoutValue;

            readoutValue = currentValue;
            if (readoutValue < 0) readoutValue = 0;

            //EXTRACT DIGITS
            digitOne = ((readoutValue % digitOneModder) / digitSmallestValue);
            digitTwo = Mathf.Floor((readoutValue % digitTwoModder) / digitOneModder);
            digitThree = Mathf.Floor((readoutValue % digitThreeModder) / digitTwoModder);
            digitFour = Mathf.Floor((readoutValue % digitFourModder) / digitThreeModder);

            //CALCULATE DIAL POSITIONS
            float digitOnePosition = digitOne * -digitOneTranslation;

            float digitTwoPosition = digitTwo * -digitTwoTranslation;
            var digitOneLimit = digitOneModder - digitSmallestValue;
            if ((digitOne * digitSmallestValue) > digitOneLimit)
            {
                digitTwoPosition += ((digitOne * digitSmallestValue) - digitOneLimit) / digitSmallestValue * -digitTwoTranslation;
            }

            float digitThreePosition = digitThree * -digitThreeTranslation;
            var digitTwoLimit = digitTwoModder - digitSmallestValue;
            if ((digitTwo * digitOneModder) > digitTwoLimit)
            {
                digitThreePosition += ((digitTwo * digitOneModder) - digitTwoLimit) / digitSmallestValue * -digitThreeTranslation;
            }

            float digitFourPosition = digitFour * -digitFourTranslation;
            var digitThreeLimit = digitThreeModder - digitSmallestValue;
            if ((digitThree * digitTwoModder) > digitThreeLimit)
            {
                digitFourPosition += ((digitThree * digitTwoModder) - digitThreeLimit) / digitSmallestValue * -digitFourTranslation;
            }

            /*//EXTRACT DIGITS
            digitOne = (((float)_currentValue % 100.0f) / 20.0f); //20FT Spacing
            digitTwo = Mathf.Floor(((float)_currentValue % 1000.0f) / 100.0f);
            digitThree = Mathf.Floor(((float)_currentValue % 10000.0f) / 1000.0f);
            digitFour = Mathf.Floor(((float)_currentValue % 100000.0f) / 10000.0f);

            //CALCULATE DIAL POSITIONS
            float digitOnePosition = digitOne * -digitOneTranslation;
            float digitTwoPosition = digitTwo * -digitTwoTranslation;
            if ((digitOne * 20) > 90.0f)
            {
                digitTwoPosition += ((digitOne * 20f) - 90.0f) / 10.0f * -digitTwoTranslation;
            }
            float digitThreePosition = digitThree * -digitThreeTranslation;
            if ((digitTwo * 100) > 990.0f)
            {
                digitThreePosition += ((digitTwo * 100) - 990.0f) / 10.0f * -digitThreeTranslation;
            }
            float digitFourPosition = digitFour * -digitFourTranslation;
            if ((digitThree * 1000) > 9990.0f)
            {
                digitFourPosition += ((digitThree * 1000) - 9990.0f) / 10f * -digitFourTranslation;
            }*/

            //SET POSITIONS
            if (digitOneContainer != null)
                digitOneContainer.localPosition = digitOneBasePosition + new Vector3(0, digitOnePosition, 0);
            if (digitTwoContainer != null)
                digitTwoContainer.localPosition = digitTwoBasePosition + new Vector3(0, digitTwoPosition, 0);
            if (digitThreeContainer != null)
                digitThreeContainer.localPosition = digitThreeBasePosition + new Vector3(0, digitThreePosition, 0);
            if (digitFourContainer != null)
                digitFourContainer.localPosition = digitFourBasePosition + new Vector3(0, digitFourPosition, 0);
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        void SetReadoutText()
        {
            foreach (TMPro.TMP_Text readoutText in readoutTexts)
            {
                readoutText.text = currentValue.ToString(readoutTextFormat);
            }
        }
    }
}
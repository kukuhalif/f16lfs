using UnityEngine;
using UnityEngine.EventSystems;
using Oyedoyin.Old;
using System;
using System.Runtime.CompilerServices;

namespace HelicopterSim
{
    public class HelicopterCamera : MonoBehaviour
    {
        // Camera Settings
        public enum CameraState { Exterior, Interior }
        public CameraState cameraState = CameraState.Interior;
        public enum CameraView { Back, Left, Front, Right }
        public CameraView cameraView = CameraView.Back;

        //------------------------------------------ Connections
        public Camera normalExterior;
        public Camera normalInterior;
        private Camera currentCamera;
        public Transform cameraFocus;
        public Transform cameraTarget;

        //------------------------------------------ Free/External Camera
        public float azimuthSensitivity = 1;
        public float elevationSensitivity = 1;
        public float radiusSensitivity = 10;
        private float azimuth, elevation;
        public float radius;
        public float maximumRadius = 20f;
        Vector3 filterPosition; float filterY; Vector3 filteredPosition;
        Vector3 cameraDirection;
        public float maximumInteriorVolume = 0.8f;
        [ReadOnly] [SerializeField] Vector3 lastDirection;
        public float followCameraHeight = 0.5f;
        //[ReadOnly][SerializeField] Vector3 followDirection;
        public float smoothTime = 0.3f;
        private Vector3 camVelocity = Vector3.zero;

        //------------------------------------------ Interior Camera
        public float viewSensitivity = 80f;
        public float maximumViewAngle = 80f;
        float verticalRotation, horizontalRotation;
        public Vector3 baseCameraPosition;
        public Quaternion baseCameraRotation, currentCameraRotation;
        public GameObject pilotObject;
        public float mouseSensitivity = 100.0f; public float clampAngle = 80.0f;
        public float scrollValue;
        public bool interiorZoomEnabled = true;
        public float interiorZoomSensitivity = 5;
        public float baseFOV = 60, maximumFOV = 20, currentFOV;
        //public bool exteriorZoomEnabled = true; // by RF
        //public float exteriorZoomSensitivity = 5; // by RF
        //public Quaternion deviceRotation;
        //public ScreenOrientation deviceOrientation;

        public void InitializeCamera()
        {
            if (normalExterior != null && normalExterior.GetComponent<AudioListener>() == null)
            {
                normalExterior.gameObject.AddComponent<AudioListener>();
            }
            if (normalInterior != null && normalInterior.GetComponent<AudioListener>() == null)
            {
                normalInterior.gameObject.AddComponent<AudioListener>();
            }
            if (normalInterior != null)
            {
                baseCameraPosition = normalInterior.transform.localPosition;
                baseCameraRotation = normalInterior.transform.localRotation;
                baseFOV = normalInterior.fieldOfView;
            }
            if (normalExterior != null)
            {
                radius = maximumRadius;
                lastDirection = new Vector3(0, 0, 1);
            }
            //ActivateExteriorCamera();
            ActivateInteriorCamera();
        }

        public void ActivateExteriorCamera()
        {
            // ------------------ Normal Interior Camera
            if (normalInterior != null)
            {
                normalInterior.enabled = false;
                AudioListener interiorListener = normalInterior.GetComponent<AudioListener>();
                if (interiorListener != null) { interiorListener.enabled = false; }
            }


            // ------------------ Normal Exterior Camera
            normalExterior.enabled = true;
            currentCamera = normalExterior;
            AudioListener exteriorListener = normalExterior.GetComponent<AudioListener>();
            if (exteriorListener != null) { exteriorListener.enabled = true; }
            cameraState = CameraState.Exterior;
        }

        public void ActivateInteriorCamera()
        {
            // ------------------ Normal Interior Camera
            if (normalInterior != null)
            {
                normalInterior.enabled = true;
                AudioListener interiorListener = normalInterior.GetComponent<AudioListener>();
                if (interiorListener != null) { interiorListener.enabled = true; }
            }
            else { Debug.Log("Interior Camera has not been setup"); return; }


            // ------------------ Normal Exterior Camera
            //normalExterior.enabled = false; 
            currentCamera = normalInterior;
            AudioListener exteriorListener = normalExterior.GetComponent<AudioListener>();
            if (exteriorListener != null) { exteriorListener.enabled = false; }
            cameraState = HelicopterCamera.CameraState.Interior;
        }

        public void ToggleCamera()
        {

            if (cameraState == HelicopterCamera.CameraState.Exterior)
            {
                ActivateInteriorCamera();
            }
            else
            {
                ActivateExteriorCamera();
            }
        }

        public void ChangeView(bool reversed)
        {
            if (cameraState == CameraState.Exterior)
            {
                Array cameraViewArray = Enum.GetValues(cameraView.GetType());
                int idx;
                if (!reversed)
                {
                    idx = Array.IndexOf(cameraViewArray, cameraView) + 1;
                    cameraView = (idx == cameraViewArray.Length) ? (CameraView)cameraViewArray.GetValue(0) : (CameraView)cameraViewArray.GetValue(idx);
                }
                else // reversed
                {
                    idx = Array.IndexOf(cameraViewArray, cameraView) - 1;
                    cameraView = (idx < 0) ? (CameraView)cameraViewArray.GetValue(cameraViewArray.Length - 1) : (CameraView)cameraViewArray.GetValue(idx);
                }
            }
        }

        /*public void ToggleCameraState()
        {
            if (isControllable)
            {
                ToggleCamera();
            }
        }*/

        private void LateUpdate()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (cameraState == CameraState.Exterior)
                {
                    /*if (Input.GetMouseButton(0) && Application.isFocused)
                    {
                        azimuth -= Input.GetAxis("Mouse X") * azimuthSensitivity * Time.deltaTime;
                        elevation -= Input.GetAxis("Mouse Y") * elevationSensitivity * Time.deltaTime;
                        //radius += Input.GetAxis("Mouse ScrollWheel") * radiusSensitivity * Time.deltaTime; // by RF
                    }

                    //ZOOM
                    if (Input.GetMouseButton(1) && Application.isFocused)
                    {
                        radius -= Input.GetAxis("Mouse Y") * radiusSensitivity * Time.deltaTime; // by RF
                    }

                    //CALCULATE DIRECTION AND POSITION
                    MathBase.SphericalToCartesian(radius, azimuth, elevation, out cameraDirection);

                    //CLAMP ROTATION IF AIRCRAFT IS ON THE GROUND//LESS THAN radius meters
                    if (cameraFocus.position.y < maximumRadius)
                    {
                        filterPosition = cameraFocus.position + cameraDirection;
                        filterY = filterPosition.y;
                        if (filterY < 2) filterY = 2;
                        filteredPosition = new Vector3(filterPosition.x, filterY, filterPosition.z);
                        normalExterior.transform.position = filteredPosition;
                    }
                    else
                    {
                        normalExterior.transform.position = cameraFocus.position + cameraDirection;
                    }

                    Rigidbody rb = cameraTarget.GetComponent<Rigidbody>();
                    Vector3 followDirection = new Vector3(0, 0, 0);
                    if (rb != null)
                        followDirection = -(rb.velocity.normalized);
                    normalExterior.transform.position = cameraTarget.position + (followDirection * radius) + (Vector3.up * followDirection.z);

                    //POSITION CAMERA
                    normalExterior.transform.LookAt(cameraFocus);
                    // ------------------------------------- by RF {
                    if (radius > maximumRadius)
                        radius = maximumRadius;
                    else if (radius < 5f) 
                        radius = 5f;
                    // ------------------------------------- by RF }
                    */

                    // get radius/distance
                    if (Input.GetMouseButton(1) && Application.isFocused)
                    {
                        radius -= Input.GetAxis("Mouse Y") * radiusSensitivity * Time.deltaTime;
                    }

                    if (Input.GetMouseButton(0) && Application.isFocused)
                    {
                        // free orbit when left mouse button is pressed
                        azimuth -= Input.GetAxis("Mouse X") * azimuthSensitivity * Time.deltaTime;
                        elevation -= Input.GetAxis("Mouse Y") * elevationSensitivity * Time.deltaTime;

                        // CALCULATE DIRECTION AND POSITION
                        MathBase.SphericalToCartesian(radius, azimuth, elevation, out cameraDirection);

                        // CLAMP ROTATION IF AIRCRAFT IS ON THE GROUND//LESS THAN radius meters
                        if (cameraFocus.position.y < maximumRadius)
                        {
                            filterPosition = cameraFocus.position + cameraDirection;
                            filterY = filterPosition.y;
                            if (filterY < 2) filterY = 2;
                            filteredPosition = new Vector3(filterPosition.x, filterY, filterPosition.z);
                            normalExterior.transform.position = Vector3.SmoothDamp(normalExterior.transform.position, filteredPosition, ref camVelocity, smoothTime);
                        }
                        else
                        {
                            normalExterior.transform.position = Vector3.SmoothDamp(normalExterior.transform.position, cameraFocus.position + cameraDirection, ref camVelocity, smoothTime);
                        }
                    }
                    else
                    {
                        // Follow target
                        Rigidbody rb = cameraTarget.GetComponent<Rigidbody>();
                        Vector3 followDirection;
                        Vector2 lateralDirection;
                        if (rb != null)
                        {
                            //followDirection = -(new Vector3(rb.velocity.normalized.x, 0f, rb.velocity.normalized.z));
                            lateralDirection = new Vector2(rb.velocity.x, rb.velocity.z);                            
                            //if (rb.velocity.magnitude > 1)
                            if (lateralDirection.magnitude > 2)
                            {
                                //followDirection = rb.velocity.normalized;
                                followDirection = -(new Vector3(rb.velocity.normalized.x, 0f, rb.velocity.normalized.z));
                                lastDirection = followDirection;
                            }
                            else
                                followDirection = lastDirection;
                        }
                        else
                            followDirection = lastDirection;

                        //Vector3 back = -cameraTarget.position;
                        //back.y = 0.5f; // this determines how high. Increase for higher view angle.
                        switch (cameraView)
                        {
                            case CameraView.Back:
                                followDirection.y = followCameraHeight;
                                //normalExterior.transform.position = cameraFocus.position + followDirection * radius;
                                normalExterior.transform.position = Vector3.SmoothDamp(normalExterior.transform.position, cameraFocus.position + followDirection * radius, ref camVelocity, smoothTime);
                                break;
                            case CameraView.Left:
                                normalExterior.transform.position = Vector3.SmoothDamp(normalExterior.transform.position, cameraFocus.position - Vector3.Cross(followDirection, Vector3.up) * radius, ref camVelocity, smoothTime);
                                break;
                            case CameraView.Right:
                                normalExterior.transform.position = Vector3.SmoothDamp(normalExterior.transform.position, cameraFocus.position + Vector3.Cross(followDirection, Vector3.up) * radius, ref camVelocity, smoothTime);
                                break;
                            case CameraView.Front:
                                followDirection.y = -followCameraHeight;
                                normalExterior.transform.position = Vector3.SmoothDamp(normalExterior.transform.position, cameraFocus.position - followDirection * radius, ref camVelocity, smoothTime);
                                break;
                        }
                        //normalExterior.transform.position = cameraFocus.position + Vector3.Cross(followDirection, Vector3.up) * radius;

                    }

                    //POSITION CAMERA
                    normalExterior.transform.LookAt(cameraFocus);

                    // REVERSE CALCULATE RADIUS, AZIMUTH and ELEVATION
                    //cameraDirection = normalExterior.transform.position - cameraFocus.position;
                    //MathBase.CartesianToSpherical(cameraDirection, out radius, out azimuth, out elevation);

                    if (radius > maximumRadius)
                        radius = maximumRadius;
                    else if (radius < 5f)
                        radius = 5f;

                }
                else // Interior Camera
                {
                    if (normalInterior != null && Input.GetMouseButton(0))
                    {
                        if (Application.isFocused)
                        {
                            verticalRotation += Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
                            horizontalRotation += -Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
                        }

                        //CLAMP ANGLES (You can make them independent to have a different maximum for each)
                        horizontalRotation = Mathf.Clamp(horizontalRotation, -clampAngle, clampAngle);
                        verticalRotation = Mathf.Clamp(verticalRotation, -clampAngle, clampAngle);
                        //ASSIGN ROTATION
                        currentCameraRotation = Quaternion.Euler(horizontalRotation, verticalRotation, 0.0f);
                        normalInterior.transform.localRotation = currentCameraRotation;
                    }

                    //currentCameraRotation = deviceRotation; // Test rotate camera using gyro
                    //normalInterior.transform.localRotation = currentCameraRotation;

                    //ZOOM
                    if (interiorZoomEnabled && normalInterior != null && Input.GetMouseButton(1))
                    //if (zoomEnabled && normalInterior != null)
                    {
                        currentFOV = normalInterior.fieldOfView;
                        currentFOV += Input.GetAxis("Mouse ScrollWheel") * interiorZoomSensitivity;
                        currentFOV += Input.GetAxis("Mouse Y") * interiorZoomSensitivity;
                        currentFOV = Mathf.Clamp(currentFOV, maximumFOV, baseFOV);
                        normalInterior.fieldOfView = currentFOV;
                    }
                }
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Oyedoyin.Common;

namespace AirplaneSim
{

    public class AirplaneController : Controller
    {

        // General Flight Settings
        //public enum PlayMode { Simulation, Animation }
        public PlayMode playMode = PlayMode.Simulation;
        //public enum StartMode { Cold, Hot, Flying }
        public StartMode startMode = StartMode.Cold;

        public float startSpeed;
        public float startAltitude;
        public float startClimbRate;
        public float startHeading;
        
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

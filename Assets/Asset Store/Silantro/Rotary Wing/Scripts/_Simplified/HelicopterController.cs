﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
//using Oyedoyin.Old; // by RF
using Oyedoyin.Mathematics;
using Oyedoyin.Common;
using Oyedoyin.Common.Misc;

namespace HelicopterSim
{
    public class HelicopterController : Controller
    {
        // ------------------------------------- Selectibles
        //public enum InputType { Keyboard, Mobile, Mouse }
        //public InputType inputType = InputType.Keyboard;
        //public enum ModelType { Realistic, Arcade }
        //public ModelType m_type = ModelType.Arcade;

        //public enum AugmentationType { Manual, Autopilot, Animation }
        //public AugmentationType operationMode = AugmentationType.Autopilot;

        // General Flight Settings
        //public enum PlayMode { Simulation, Animation }
        //public PlayMode playMode = PlayMode.Simulation;
        //public enum StartMode { Cold, Hot, Flying }
        //public StartMode startMode = StartMode.Cold;

        // Rotor Rotation & Visual Settings
        public enum RotationAxis { X, Y, Z }
        public RotationAxis mainRotorAxis = RotationAxis.X;
        public RotationAxis tailRotorAxis = RotationAxis.Y;
        public enum RotationDirection { CW, CCW }
        public RotationDirection mainRotorDirection = RotationDirection.CW;
        public RotationDirection tailRotorDirection = RotationDirection.CW;        
        public enum RotorVisualType { Default, Partial, Complete }
        public RotorVisualType rotorVisualType = RotorVisualType.Default;

        // Engine Status
        public enum EngineState { Off, Starting, Active }
        public EngineState CurrentEngineState = EngineState.Off; // init by RF

        // ---- Helicopter controller recordable fields
        public float recordable_none;
        //public EngineState recordable_engineState; // by RF
        public float recordable_coreMainRPM, recordable_coreTailRPM, recordable_coreFactor; // by RF
        public float recordable_audioIgnitionVolume, recordable_audioIgnitionPitch;
        public float recordable_audioShutdownVolume, recordable_audioShutdownPitch;
        public float recordable_audioEngineVolume, recordable_audioEnginePitch;
        public float recordable_audioRotorVolume, recordable_audioRotorPitch;

        // Flight Control Settings
        public enum AntiTorque { Force, Moment }
        public AntiTorque antiTorque = AntiTorque.Force;
        public enum RollYawCouple { Free, Combined }
        public RollYawCouple m_couple = RollYawCouple.Free;
        public enum MomentBalance { Off, Active }
        public MomentBalance m_balance = MomentBalance.Off;
        //public enum GroundEffect { Consider, Neglect }
        //public GroundEffect groundEffect = GroundEffect.Consider;

        // Camera Settings
        //public enum CameraState { Exterior, Interior }
        //public CameraState cameraState = CameraState.Interior;
                
        // Audio parameters
        public enum InteriorMode { Off, Active }
        public InteriorMode interiorMode = InteriorMode.Off;
        public enum SoundMode { Basic, Advanced }
        public SoundMode soundMode = SoundMode.Basic;
        //public enum m_override { Active, Off }
        //public m_override keyboard_override = m_override.Off;

        // -------------------------------------Rotor Properties
        public float mainRotorRadius = 5f;
        public float maximumRotorLift = 10000f;
        public float maximumRotorTorque = 6000f;
        public float maximumTailThrust = 2000f;
        public float MomentFactor = 20000f;
        public float mainRotorRPM = 200f;
        public float tailRotorRPM = 200f; //1000f;
        //public float direction;
        public float coreMainRPM;
        public float coreTailRPM;

        [ReadOnly] public float Ω, vz, µx, δf, δv; //, δG, h;
        public float maxAngularSpeed = 0.1f; // prev = 5
        public float rotationDrag = 50f; // previous = 2f

        // -------------------------------------Speed Limit
        public float maximumClimbRate = 2000f / (float)Constants.toFtMin; // MathBase.toFtMin; // in m/s
        public float maximumDecentSpeed = 2000f / (float)Constants.toFtMin; // MathBase.toFtMin; // in m/s;
        public float maximumTurnSpeed = 1000f / (float)Constants.toFtMin; // MathBase.toFtMin; // in m/s;
        public float maximumSpeed = 123 / (float)Constants.toFtMin; // MathBase.toKnots; // in m/s

        // ------------------------------------ Forces Calculation
        //public bool m_isclimbing;
        public float m_collective_power;
        public float m_collective_speed = 0.5f;
        public float m_throttle_speed = 0.5f; // by RF
        public float m_lift_force;
        public float m_balance_force;
        public float m_collective_balance = 1f; // original = 2f
        public float m_roll_balance_factor = 1f;
        public float m_pitch_balance_factor = 1f;
        public float m_yawMoment;
        [Range(0.01f, 1f)] public float m_couple_level = 0.4f;
        [Range(0.01f, 1f)] public float m_power_limit = 0.8f;
        //public KeyCode m_climb = KeyCode.Alpha1;
        //bool m_press_climb;
        //public KeyCode m_decend = KeyCode.Alpha2;
        //bool m_press_decend;
        public float m_tailPushRatio = 1f;
        [ReadOnly] public float m_tailPush;

        // ------------------------------------ Connections
        public Transform mainRotorPosition;
        public Transform tailRotorPosition;
        public Transform centerOfGravity;
        public Rigidbody helicopter;
        //public Camera normalExterior;
        //public Camera normalInterior;
        //private Camera currentCamera;
        //public Transform cameraFocus;
        public HelicopterController controller;
        //public Helper helper;
        //public MouseControl m_mouse;
        //public Aerofoil[] foils;
        public HelicopterBulb[] bulbs;
        public HelicopterCamera heliCamera;

        // ------------------------------------- by RF {
        public HelicopterDial[] dials;
        public HelicopterLever[] levers;
        public Animator[] animations;
        // ------------------------------------- by RF }

        // ------------------------------------ Controls
        [ReadOnly] public float processedThrottle = 0f; // init value by RF
        [ReadOnly] public float processedCollective = 0f; // init value by RF
        [ReadOnly] public float processedPitch;
        [ReadOnly] public float processedRoll;
        [ReadOnly] public float processedYaw;
        public float ϴ1c;
        public float ϴ1s;

        // ------------------------------------ Input Curve Calculation
        public AnimationCurve pitchInputCurve;
        public AnimationCurve rollInputCurve;
        public AnimationCurve yawInputCurve;
        public AnimationCurve collectiveInputCurve; 
        [Range(1, 3)] public float pitchScale = 1, rollScale = 1, yawScale = 1, collectiveScale = 1;

        // ------------------------------------ Rotor Materials
        public Material[] normalRotor;
        public Material[] blurredRotor;
        public Color blurredRotorColor;
        public Color normalRotorColor;
        public float alphaSettings;
        public float normalBalance = 0.2f;

        // ------------------------------ Limits
        public float minimumPitchCyclic = 10;
        public float maximumPitchCyclic = 10f;
        public float minimumRollCyclic = 10f;
        public float maximumRollCyclic = 10f;
        private float ϴ1sMax, ϴ1cMax;
        private float ϴ1sMin, ϴ1cMin;


        // ------------------------------------ Engine
        public float corePower;
        public bool start, shutdown, clutching, active;
        [Range(0.01f, 1f)] public float baseCoreAcceleration = 0.25f;
        [ReadOnly] public float coreRPM;
        public float factorRPM, norminalRPM, functionalRPM = 1000f;
        public float idlePercentage = 10f, coreFactor, fuelFactor;


        // ------------------------------------ Fuel
        //public float fuelCapacity = 500f;
        //[ReadOnly] public float fuelLevel = 0f;
        public float bingoFuel = 50f;
        public float fuelConsumption = 0.15f;


        // ------------------------------------ Weight & Balance
        //public float emptyWeight = 1000f;
        //public float maximumWeight;
        //public float currentWeight;
        [Tooltip("Resistance to movement on the pitch axis")] public float xInertia = 10000;
        [Tooltip("Resistance to movement in the roll axis")] public float yInertia = 5000;
        [Tooltip("Resistance to movement in the yaw axis")] public float zInertia = 8000;



        // ------------------------------------ Sounds
        public AudioSource interiorSource;
        public AudioSource exteriorSource;
        public AudioSource backSource;
        public AudioSource rotorSource, interiorBaseSource;
        public AudioClip interiorIdle, frontIdle, backIdle, sideIdle;
        public AudioClip ignitionInterior, ignitionExterior;
        public AudioClip shutdownInterior, shutdownExterior;
        public AudioClip rotorRunning;
        public float exteriorVolume, interiorVolume;
        public float pitchTarget, overidePitch;
        public float maximumRotorPitch = 1.2f;




        //------------------------------------------ Effects
        [Serializable]
        public class m_effect
        {
            public ParticleSystem m_effect_particule;
            public ParticleSystem.EmissionModule m_effect_module;
            public float m_effect_limit = 50f;
        }
        public List<m_effect> m_effects = new List<m_effect>();




        //------------------------------------------ Free Camera
        /*public float azimuthSensitivity = 1;
        public float elevationSensitivity = 1;
        public float radiusSensitivity = 10;
        private float azimuth, elevation;
        public float radius;
        public float maximumRadius = 20f;
        Vector3 filterPosition; float filerY; Vector3 filteredPosition;
        Vector3 cameraDirection;
        public float maximumInteriorVolume = 0.8f;
        


        //------------------------------------------ Interior Camera
        public float viewSensitivity = 80f;
        public float maximumViewAngle = 80f;
        float verticalRotation, horizontalRotation;
        Vector3 baseCameraPosition;
        Quaternion baseCameraRotation, currentCameraRotation;
        public GameObject pilotObject;
        public float mouseSensitivity = 100.0f; public float clampAngle = 80.0f;
        public float scrollValue;
        public float zoomSensitivity = 5;
        public float maximumFOV = 20, currentFOV, baseFOV;
        */

        //------------------------------------------ Ground Effect
        public AnimationCurve groundCorrection;
        public Vector3 groundAxis = new Vector3(0.0f, -1.0f, 0.0f);
        public LayerMask groundLayer;

        //------------------------------------------ Dust Particle Effect -- by RF
        public bool dustEnabled = true;
        public ParticleSystem dustParticle;
        public float dustAmount = 20f;

        //------------------------------------------ Force
        public AnimationCurve thrustCorrection;
        public AnimationCurve inflowCorrection;
        public float Thrust, Torque;
        public Vector3 wind;
        public Vector3 Force;
        public Vector3 Moment;
        public Vector3 balanceMoment;


        //------------------------------------------ Controls
        //public bool allOk;
        //public bool zoomEnabled = true;
        //public bool isControllable = true;


        public float m_key_overide = 0.2f;
        //bool m_roll_pressed, m_pitch_pressed, m_yaw_pressed;
        public float rawPitchInput, rawRollInput, rawYawInput, rawThrottleInput, throttleInput, collectiveInput, rawCollectiveInput, rawPropellerInput, propellerInput;

        [ReadOnly] public float m_vertical_speed;
        [ReadOnly] private float m_prevVertPos; // by RF, previous vertical position to calculate vertical speed without physics
        [ReadOnly] public float m_vertSpeed = 0f; // by RF
        [ReadOnly] private Vector2 m_prevHorzPos; // by RF, previous horizontal position to calculate horizontal speed without physics
        [ReadOnly] public float m_horzSpeed = 0f; // by RF
        [ReadOnly] public float m_airspeed;
        public float m_pitchrate;
        public float m_rollrate;
        public float m_yawrate;
        [ReadOnly] public float m_bankAngle; // by RF
        [ReadOnly] public float m_altitude; // by RF
        [ReadOnly] public float m_radarAltitude; // by RF
        [ReadOnly] public float m_heading; // by RF
        public Vector3 m_localflow; // by RF
        public Vector3 m_angular_velocity; // by RF
        public float m_turnrate;
        public float m_gforce;
        //float m_grate = 0.04f;
        float m_gspeed = 0.05f;
        float m_temp;
        //float speedUpdateDelay = 0.1f;
        //bool paused = false;

        public AnimationCurve m_density;
        public AnimationCurve m_pressure;
        public AnimationCurve m_temperature;

        public float m_air_temperature;
        public float m_air_pressure;
        public float m_air_density;
        public float m_sound_speed;
        public float m_mach_speed;

        // ------------------------------------------ Rotation
        public float pitchAngle;
        public float rollAngle;
        public float yawAngle;
        // ------------------------------------------ Rotation Rate
        public float pitchRate;
        public float rollRate;
        public float yawRate;
        //--------------------------------------------- Dead Zones
        public float pitchDeadZone = 0.05f;
        public float rollDeadZone = 0.05f;
        public float yawDeadZone = 0.05f;
        public float collectiveDeadZone = 0.05f;
        // ------------------------------------------ Command Inputs
        public float commandRollInput = 0f;
        public float commandYawInput = 0f;
        public float commandPitchInput = 0f;
        public float commandCollectiveInput = 0f;
        public float commandThrottleInput = 0f;

        float presetRollInput = 0f;
        float presetYawInput = 0f;
        float presetPitchInput = 0f;
        //float presetCollectiveInput = 0f; // by RF

        // ------------------------------------------ Performance
        public float currentSpeed;
        public float currentAltitude;
        public float currentClimbRate;
        public float currentHeading;
        public float currentDriftSpeed;
        public float timeStep;

        //public float knotSpeed, ftHeight;

        // -------------------------------- Default Position for Flying at Start
        //public float m_startSpeed = 0f;
        //public float m_startAltitude = 100f;
        //[Range(1f, 360f)] public float m_startHeading = 180f;
        //public float m_startClimbRate = 0f;

        //public FlightComputer flightComputer;

        // ------------------------------------------ AUTOPILOT
        public float commandSpeed = 0f;
        public float commandAltitude = 100f;
        public float commandHeading = 90f;
        public float commandClimbRate = 10f;

        public float m_sas_authority = 10f;
        public float m_sas_pitch;
        public float m_sas_roll;
        public float m_sas_yaw;
        public float m_sas_collective;
        public float m_αMax;

        public Vector3 m_pitch_gain;
        public Vector3 m_roll_gain;
        public Vector3 m_yaw_gain;
        public float m_pitch_factor = 1;
        public float m_roll_factor = 1;
        public float m_max_speed = 260f; // value by RF
        //public AnimationCurve m_climb_curve;
        public float safeHeight = 50f;
        public float m_climb_factor;

        public enum ControlState { Active, Off }
        public ControlState autoThrottle = ControlState.Off;
        public ControlState speedHold = ControlState.Off;
        public ControlState headingHold = ControlState.Off;
        public ControlState bankHold = ControlState.Off;
        public ControlState pitchHold = ControlState.Off;
        public ControlState altitudeHold = ControlState.Off;

        // -------------------------------------------- Command Variables / Errors
        public float pitchAngleError, rollAngleError, yawAngleError;
        public float pitchRateError, rollRateError, yawRateError;
        public float speedError;
        public float altitudeError;
        public float climbRateError;
        public float headingError;
        public float gError;

        // ------------------------------------------ Solvers
        //1. Inner Loop
        // PhantomPID? throttleSolver
        /*public PhantomPID rollRateSolver;
        public PhantomPID pitchRateSolver;
        public PhantomPID yawRateSolver;

        //2. Outer Loop A
        public PhantomPID rollAngleSolver;
        public PhantomPID pitchAngleSolver;
        public PhantomPID yawAngleSolver;
        public PhantomPID verticalClimbSolver;
        public PhantomPID forwardClimbSolver;
        public PhantomPID altitudeSolver;
        public PhantomPID turnSolver;
        public PhantomPID propellerSolver;
        public PhantomPID driftSolver;
        public PhantomPID speedSolver;
        */
        //public Quaternion deviceRotation;
        //public ScreenOrientation deviceOrientation;

        public HelicopterController()
        {
            m_type = VehicleType.SimpleHelicopter;
        }

        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        //--------------------------------------------------------------CONTROLS
        private void Start()
        {
            //Input.gyro.enabled = true; // test gyro by RF

            InitializeHelicopter();
            if (m_startMode == StartMode.Hot || m_startMode == StartMode.Flying)
            {
                StartAircraft(helicopter);
            }
            //StartCoroutine(HorizontalSpeedReckoner());
            //StartCoroutine(VerticalSpeedReckoner());
        }

        private void OnDestroy()
        {
            //ResetMaterials();
        }

        //public void SetControlState(bool state) { isControllable = state; }
        //public void ResetScene() { UnityEngine.SceneManagement.SceneManager.LoadScene(this.gameObject.scene.name); }
        /*public void StartEngine() { helper.StartEngine(); }
        public void ShutDownEngine() { helper.ShutDownEngine(); }
        public void ToggleCamera() { helper.ToggleCamera(); }
        public void ToggleLight() { helper.ToggleLightState(); }
        public void PositionAircraft() { helper.PositionAircraft(helicopter); }
        public void StartAircraft() { helper.StartAircraft(helicopter); }
        */



        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        //------------------------------------------------------STATE MANAGEMENT
        private void FixedUpdate()
        {
            if (!isPaused())
            {
                /*if (m_playMode == PlayMode.Simulation)
                {
                    recordable_engineState = CurrentEngineState;
                }
                else if (m_playMode == PlayMode.Animation)
                {
                    CurrentEngineState = recordable_engineState;
                }*/

                //if (m_playMode == PlayMode.Simulation)
                //{
                // ------------------------------------ Engine
                UpdateEngine();

                    // ------------------------------------ Forces
                    UpdateForces();
                //}
            }


            //if (mainRotorPosition != null) { UpdateVisuals(mainRotorPosition, coreMainRPM, mainRotorDirection, mainRotorAxis); }
            //if (tailRotorPosition != null) { UpdateVisuals(tailRotorPosition, coreTailRPM, tailRotorDirection, tailRotorAxis); }


            // ------------------------------------ Camera
            //if (camera != null)
            //    camera.UpdateCameras();

            // ------------------------------------ Controls
            //UpdateControls();

        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Update()
        {
            if (!isPaused())
            {
                if (m_playMode == PlayMode.Simulation)
                {
                    recordable_coreFactor = coreFactor;
                    recordable_coreMainRPM = coreMainRPM;
                    recordable_coreTailRPM = coreTailRPM;
                }
                else if (m_playMode == PlayMode.Animation)
                {
                    coreFactor = recordable_coreFactor;
                    coreMainRPM = recordable_coreMainRPM;
                    coreTailRPM = recordable_coreTailRPM;
                }

                if (mainRotorPosition != null)
                {
                    UpdateVisuals(mainRotorPosition, coreMainRPM, mainRotorDirection, mainRotorAxis);
                }
                if (tailRotorPosition != null)
                {
                    UpdateVisuals(tailRotorPosition, coreTailRPM, tailRotorDirection, tailRotorAxis);
                }

                // ------------------------------------ Camera
                //if (camera != null)
                //    camera.UpdateCameras();

                // ------------------------------------ Controls
                //UpdateControls();
            }
        }

        private void LateUpdate()
        {
            // ------------------------------------ Camera
            //UpdateCameras(); // moved to LateUpdate by RF

            // ------------------------------------ Controls

            UpdateViewControls(); 
            if (m_playMode == PlayMode.Simulation)
                UpdateFlightControls();
            else
            {
                //UpdateForces();
                AnimatedMovement();
            }

            // update info display\
            _throttleInput = processedThrottle;
            _collectiveInput = processedCollective;
          
        }
        
        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        void InitializeHelicopter()
        {
            //---------------------------------------- Engine
            helicopter = GetComponent<Rigidbody>();
            controller = GetComponent<HelicopterController>();
            bulbs = GetComponentsInChildren<HelicopterBulb>();
            dials = GetComponentsInChildren<HelicopterDial>();
            levers = GetComponentsInChildren<HelicopterLever>();
            heliCamera = GetComponentInChildren<HelicopterCamera>();

            MathBase.m_plot_atm(out m_pressure, out m_temperature, out m_density);

            //---------------------------------------- Engine
            fuelLevel = fuelCapacity;
            //fuelLevel = fuelCapacity / 2f;
            //direction = mainRotorDirection == RotationDirection.CCW ? 1 : -1f;
            Vector3 inertiaTensor = new Vector3(xInertia, yInertia, zInertia);
            helicopter.inertiaTensor = inertiaTensor;
            helicopter.angularDrag = rotationDrag;
            helicopter.maxAngularVelocity = maxAngularSpeed;
            if (m_startMode == StartMode.Hot)
            {
                //factorRPM = functionalRPM; 
                corePower = 0.5f;
                commandCollectiveInput = 0f; // 0.tf
                commandThrottleInput = 1f;
            }
            else if (m_startMode == StartMode.Flying)
            {
                factorRPM = functionalRPM;
                corePower = 1f;
                commandCollectiveInput = 0.5f; //0.7f
                commandThrottleInput = 1f;
            }

            /*if (operationMode == AugmentationType.Animation)
            {
                //helicopter.constraints = RigidbodyConstraints.FreezeAll; // disable physics
            }*/
            // by RF
            //processedThrottle = (controller.startMode == Controller.StartMode.Flying) ? 1 : commandThrottleInput;


            //----------------------------------------Setup Sounds
            GameObject soundPoint = new GameObject("_audioSources"); 
            soundPoint.transform.parent = transform; 
            soundPoint.transform.localPosition = Vector3.zero;
            if (backIdle) 
            { 
                Handler.SetupSoundSource(soundPoint.transform, backIdle, "_rear_sound_point", 150f, true, true, out backSource); 
            }
            if (ignitionInterior && interiorMode == InteriorMode.Active) 
            { 
                Handler.SetupSoundSource(soundPoint.transform, ignitionInterior, "_interior_sound_point", 50f, false, false, out interiorSource); 
            }
            if (ignitionExterior) 
            { 
                Handler.SetupSoundSource(soundPoint.transform, ignitionExterior, "_exterior_sound_point", 150f, false, false, out exteriorSource); 
            }
            if (interiorIdle && interiorMode == InteriorMode.Active) 
            { 
                Handler.SetupSoundSource(soundPoint.transform, interiorIdle, "_interior_base_point", 80f, true, true, out interiorBaseSource); 
            }
            if (rotorRunning) 
            { 
                Handler.SetupSoundSource(soundPoint.transform, rotorRunning, "_rotor_sound", 80f, true, true, out rotorSource); 
            }


            //---------------------------------------- Cameras
            /*if (camera.normalExterior != null && camera.normalExterior.GetComponent<AudioListener>() == null) 
            { 
                camera.normalExterior.gameObject.AddComponent<AudioListener>(); 
            }
            if (camera.normalInterior != null && camera.normalInterior.GetComponent<AudioListener>() == null) 
            { 
                camera.normalInterior.gameObject.AddComponent<AudioListener>(); 
            }
            if (camera.normalInterior != null)
            {
                camera.baseCameraPosition = camera.normalInterior.transform.localPosition;
                camera.baseCameraRotation = camera.normalInterior.transform.localRotation;
                camera.baseFOV = camera.normalInterior.fieldOfView;
            }
            //ActivateExteriorCamera();
            camera.ActivateInteriorCamera();
            */
            heliCamera.InitializeCamera();

            //---------------------------------------- Controls
            ϴ1sMax = Mathf.Abs(maximumRollCyclic) * Mathf.Deg2Rad;
            ϴ1sMin = Mathf.Abs(minimumRollCyclic) * Mathf.Deg2Rad;
            ϴ1cMax = Mathf.Abs(maximumPitchCyclic) * Mathf.Deg2Rad;
            ϴ1cMin = Mathf.Abs(minimumPitchCyclic) * Mathf.Deg2Rad;

            pitchInputCurve = MathBase.PlotControlInputCurve(pitchScale);
            rollInputCurve = MathBase.PlotControlInputCurve(rollScale);
            yawInputCurve = MathBase.PlotControlInputCurve(yawScale);
            collectiveInputCurve = MathBase.PlotControlInputCurve(collectiveScale); // by RF
            //MathR.PlotClimbCorrection(out thrustCorrection, maximumClimbRate);
            //if (groundEffect == GroundEffect.Consider) { MathR.PlotGroundCorrection(out groundCorrection); }
            //MathR.PlotInflowCorrection(out inflowCorrection);

            //plotClimbCurve(safeHeight, out m_climb_curve);


            if (rotorVisualType == RotorVisualType.Complete || rotorVisualType == RotorVisualType.Partial)
            {
                if (blurredRotor.Length > 0 && blurredRotor[0] != null)
                {
                    blurredRotorColor = blurredRotor[0].color;
                    alphaSettings = 0;
                }
                if (normalRotor.Length > 0 && normalRotor[0] != null) 
                { 
                    normalRotorColor = normalRotor[0].color; 
                }
            }
            ResetMaterials();

            // --------------------------- Bulbs
            foreach (HelicopterBulb bulb in bulbs) { bulb.InitializeBulb(); }

            // ------------------------- Setup Dials
            foreach (HelicopterDial dial in dials)
            {
                dial.m_controller = controller;
                //dial.dataLog = core;
                dial.InitializeDial();
            }

            // ------------------------- Setup Levers
            foreach (HelicopterLever lever in levers)
            {
                lever.m_controller = controller;
                lever.InitializeLever();
            }

            // to calculate speed
            m_prevVertPos = transform.position.y;
            m_prevHorzPos = new Vector2(transform.position.x, transform.position.z);

            // ------------------------- Dust effect
            dustParticle.gameObject.SetActive(dustEnabled);
        }

        private void ResetMaterials()
        {
            foreach (Material brotor in blurredRotor)
            {
                if (brotor != null)
                    brotor.color = new Color(blurredRotorColor.r, blurredRotorColor.g, blurredRotorColor.b, 0);
            }
            foreach (Material nrotor in normalRotor)
            {
                if (nrotor != null)
                    nrotor.color = new Color(normalRotorColor.r, normalRotorColor.g, normalRotorColor.b, 1);
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        protected void UpdateEngine()
        {

            // ------------------------------------------ Core
            // ------------------------------------- by RF {

            if (active && fuelLevel < 5)
            {
                //ShutDownEngine();
                TurnOffEngines();

            }
            if (active && isControllable == false)
            {
                //ShutDownEngine();
                TurnOffEngines();
            }
            if (active)
            {
                if (corePower < 1f)
                {
                    corePower += Time.fixedDeltaTime * baseCoreAcceleration;
                }
            }
            else // not active
                if (corePower > 0f)
                {
                    corePower -= Time.fixedDeltaTime * baseCoreAcceleration;
                }
            if (processedThrottle > 1)
            {
                processedThrottle = 1f;
            }
            if (corePower > 1)
            {
                corePower = 1f;
            }
            if (!active && corePower < 0)
            {
                corePower = 0f;
            }



            // ------------------------------------- by RF }

            if (active && fuelLevel < 1)
            {
                shutdown = true;
            }
            fuelLevel -= fuelConsumption * coreFactor * Time.fixedDeltaTime;

            // ------------------------------------------ Fuel
            if (active && fuelLevel < bingoFuel)
            {
                float startRange = 0.2f;
                float endRange = 0.85f;
                float cycleRange = (endRange - startRange) / 2f;
                float offset = cycleRange + startRange;
                fuelFactor = offset + Mathf.Sin(Time.time * 3f) * cycleRange;
            }
            else
            {
                fuelFactor = 1f;
            }


            // ------------------------------------------ States
            switch (CurrentEngineState)
            {
                case EngineState.Off:
                    StateOff(); 
                    break;
                case EngineState.Starting:
                    StateStart();
                    break;
                case EngineState.Active:
                    StateActive();
                    break;
            }


            // ------------------------------------------ RPM
            if (active)
            {
                factorRPM = Mathf.Lerp(factorRPM, norminalRPM, baseCoreAcceleration * Time.fixedDeltaTime * 2);
            }
            else
            {
                factorRPM = Mathf.Lerp(factorRPM, 0, baseCoreAcceleration * Time.fixedDeltaTime * 2f);
            }
            if (factorRPM > functionalRPM)
            {
                factorRPM = functionalRPM;
            }
            coreRPM = factorRPM * corePower * fuelFactor;
            coreFactor = coreRPM / functionalRPM;

            coreMainRPM = mainRotorRPM * coreFactor;
            coreTailRPM = tailRotorRPM * coreFactor;

            // ------------------------------------------ Volume
            if (heliCamera.cameraState == HelicopterCamera.CameraState.Exterior) 
            { 
                exteriorVolume = corePower * 0.6f; 
                interiorVolume = 0f; 
            }
            if (heliCamera.cameraState == HelicopterCamera.CameraState.Interior) 
            { 
                interiorVolume = corePower * 0.6f; 
                exteriorVolume = 0f; 
            }

            float speedFactor = ((coreRPM + (helicopter.velocity.magnitude * 1.943f) + 10f) - functionalRPM * (idlePercentage / 100f)) / 
                (functionalRPM - functionalRPM * (idlePercentage / 100f));
            pitchTarget = 0.35f + (0.7f * speedFactor);
            if (fuelFactor < 1) 
            { 
                overidePitch = pitchTarget; 
            } 
            else 
            { 
                overidePitch = fuelFactor * Mathf.Lerp(overidePitch, pitchTarget, Time.fixedDeltaTime * 0.5f); 
            }
            pitchTarget *= fuelFactor; 
            backSource.pitch = overidePitch;
            if (interiorMode == InteriorMode.Active && interiorBaseSource != null) 
            {
                interiorBaseSource.pitch = overidePitch; 
            }
            backSource.volume = exteriorVolume * coreFactor;
            exteriorSource.volume = exteriorVolume;
            //Debug.Log("Exterior Volume: " + exteriorSource.volume.ToString("0.00") + " Exterior Pitch: " + exteriorSource.pitch.ToString("0.00"));
            if (interiorMode == InteriorMode.Active && interiorBaseSource != null && interiorSource != null)
            {
                interiorSource.volume = interiorVolume;
                interiorBaseSource.volume = interiorVolume * coreFactor;
            }

            // ------------------------------------------ Rotor Sound
            float soundFactor = 1;
            if (heliCamera.cameraState == HelicopterCamera.CameraState.Interior) 
                soundFactor = 0.2f; 

            if (coreFactor < 0.01f) 
                rotorSource.Stop();
            else
            {
                if (!rotorSource.isPlaying) 
                    rotorSource.Play(); 
                rotorSource.pitch = maximumRotorPitch * coreFactor;
                rotorSource.volume = coreFactor * soundFactor;
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        protected void UpdateVisuals(Transform rotor, float rpm, RotationDirection direction, RotationAxis axis)
        {
            if (rpm > 0)
            {
                if (direction == RotationDirection.CW)
                {
                    if (axis == RotationAxis.X) { rotor.transform.Rotate(new Vector3(rpm * 5f * Time.deltaTime, 0, 0)); }
                    if (axis == RotationAxis.Y) { rotor.transform.Rotate(new Vector3(0, rpm * 5f * Time.deltaTime, 0)); }
                    if (axis == RotationAxis.Z) { rotor.transform.Rotate(new Vector3(0, 0, rpm * 5f * Time.deltaTime)); }
                }
                if (direction == RotationDirection.CCW)
                {
                    if (axis == RotationAxis.X) { rotor.transform.Rotate(new Vector3(-1f * rpm * 5f * Time.deltaTime, 0, 0)); }
                    if (axis == RotationAxis.Y) { rotor.transform.Rotate(new Vector3(0, -1f * rpm * 5f * Time.deltaTime, 0)); }
                    if (axis == RotationAxis.Z) { rotor.transform.Rotate(new Vector3(0, 0, -1f * rpm * 5f * Time.deltaTime)); }
                }
            }

            //alphaSettings = coreFactor;
            alphaSettings = coreFactor < 0.5f ? 0 : coreFactor * 2 - 1;
            if (rotorVisualType == RotorVisualType.Complete)
            {
                if (blurredRotor != null && normalRotor != null)
                {
                    foreach (Material brotor in blurredRotor) 
                    { 
                        if (brotor != null) 
                            brotor.color = new Color(blurredRotorColor.r, blurredRotorColor.g, blurredRotorColor.b, alphaSettings); 
                    }
                    foreach (Material nrotor in normalRotor) 
                    { 
                        if (nrotor != null) 
                            nrotor.color = new Color(normalRotorColor.r, normalRotorColor.g, normalRotorColor.b, (1 - alphaSettings) + normalBalance); 
                    }
                }
            }
            else if (rotorVisualType == RotorVisualType.Partial)
            {
                if (blurredRotor != null)
                {
                    foreach (Material brotor in blurredRotor) 
                    { 
                        if (brotor != null) 
                            brotor.color = new Color(blurredRotorColor.r, blurredRotorColor.g, blurredRotorColor.b, alphaSettings); 
                    }
                }
            }


            // ----------------------------------------- Effects
            foreach (m_effect effect in m_effects)
            {
                if (!effect.m_effect_module.enabled && effect.m_effect_particule != null) { effect.m_effect_module = effect.m_effect_particule.emission; }
                if (effect.m_effect_module.enabled) { effect.m_effect_module.rateOverTime = effect.m_effect_limit * coreFactor; }
            }

            // -------------------------------------------------- Ground Dust Effect
            // ------------------------------------- by RF {

            if (dustEnabled)
            {
                //Physics.Raycast(new Ray(transform.position - Vector3.up, -Vector3.up), out groundHit, 1000, groundLayer));
                Ray groundCheck = new Ray(transform.position, groundAxis);
                RaycastHit groundHit;
                Physics.Raycast(groundCheck, out groundHit); //, 1000, groundLayer);
                ParticleSystem dust = dustParticle.GetComponent<ParticleSystem>();
                ParticleSystem.EmissionModule emission = dust.emission;
                if (dust != null && emission.enabled)
                {
                    dustParticle.transform.position = groundHit.point;
                    //emission.rateOverTime = Mathf.Max((dustAmount - m_altitude) * coreFactor, 0);
                    emission.rateOverTime = Mathf.Max((dustAmount - transform.position.y) * coreFactor, 0);
                }
            }
            // ------------------------------------- by RF }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        protected void UpdateForces()
        {
            // ---------------------------------- Weight
            currentWeight = emptyWeight + fuelLevel;
            helicopter.mass = currentWeight;
            //if (m_type == ModelType.Realistic) { helicopter.centerOfMass = centerOfGravity.localPosition; }
            helicopter.centerOfMass = centerOfGravity.localPosition;
            Vector3 υw = helicopter.GetPointVelocity(mainRotorPosition.position) + wind;
            Vector3 υl = helicopter.transform.InverseTransformDirection(υw);
            Ω = (2 * Mathf.PI * (1 + coreRPM)) / 60f;
            vz = Mathf.Sqrt(υl.x * υl.x + υl.z * υl.z);
            //float δTt = 1 - ((vz * MathBase.toKnots) / maximumTurnSpeed); if (δTt < 0) { δTt = 0f; }
            float δTt = 1 - ((vz) / maximumTurnSpeed); if (δTt < 0) { δTt = 0f; }
            µx = vz / (Ω * mainRotorRadius);
            //δf = thrustCorrection.Evaluate(υl.y * MathBase.toFtMin);
            δf = thrustCorrection.Evaluate(υl.y); // * MathBase.toFtMin);
            δv = inflowCorrection.Evaluate(vz) / inflowCorrection.Evaluate(0);


            m_localflow = transform.InverseTransformDirection(helicopter.velocity);
            m_angular_velocity = transform.InverseTransformDirection(helicopter.angularVelocity);
            m_pitchrate = (float)Math.Round((-m_angular_velocity.x * Mathf.Rad2Deg), 2);
            m_yawrate = (float)Math.Round((m_angular_velocity.y * Mathf.Rad2Deg), 2);
            m_rollrate = (float)Math.Round((-m_angular_velocity.z * Mathf.Rad2Deg), 2);
            m_vertical_speed = (float)Math.Round(helicopter.velocity.y, 2);
            float m_turn_radius = (Mathf.Approximately(m_angular_velocity.x, 0.0f)) ? float.MaxValue : m_localflow.z / m_angular_velocity.x;
            float m_turn_force = (Mathf.Approximately(m_turn_radius, 0.0f)) ? 0.0f : (m_localflow.z * m_localflow.z) / m_turn_radius;
            float m_bg = m_turn_force / -9.81f; 
            m_bg += transform.up.y * (Physics.gravity.y / -9.81f);
            float m_tg = (m_bg * m_gspeed) + (m_temp * (1.0f - m_gspeed));
            m_temp = m_tg;
            m_gforce = (float)Math.Round(m_tg, 1);

            // ------------------------------------- by RF {
            //float bankAngle = helicopter.transform.eulerAngles.z; if (bankAngle > 180.0f) { bankAngle = -(360.0f - bankAngle); } // commented
            //m_turnrate = (1091f * Mathf.Tan(bankAngle * Mathf.Deg2Rad)) / MathBase.ConvertSpeed(m_airspeed + 1, "KTS"); // commented
            m_bankAngle = helicopter.transform.eulerAngles.z; if (m_bankAngle > 180.0f) { m_bankAngle = -(360.0f - m_bankAngle); }
            m_turnrate = (1091f * Mathf.Tan(m_bankAngle * Mathf.Deg2Rad)); // / MathBase.ConvertSpeed(m_airspeed + 1, "KTS");
            m_altitude = helicopter.transform.position.y; // * MathBase.toFt;
            RaycastHit hit;
            if (Physics.Raycast(helicopter.position, Vector3.down, out hit))
                m_radarAltitude = Vector3.Distance(hit.point, helicopter.position);
            Quaternion rotAngleForward = Quaternion.FromToRotation(helicopter.transform.forward, Vector3.forward);
            m_heading = 360 - rotAngleForward.eulerAngles.y;
            m_heading = (m_heading == 360) ? 0 : m_heading;
            m_airspeed = vz; // * MathBase.toKnots;

            m_vertSpeed = (transform.position.y - m_prevVertPos) / Time.fixedDeltaTime;
            m_prevVertPos = transform.position.y;

            var curHorzPos = new Vector2(transform.position.x, transform.position.z);
            m_horzSpeed = (curHorzPos - m_prevHorzPos).magnitude / Time.fixedDeltaTime;
            m_prevHorzPos = curHorzPos;

            // ------------------------------------- by RF {


            // ---------------------------------- Ambient
            float m_kalt = m_altitude / 1000f;
            m_air_density = m_density.Evaluate(m_kalt);
            m_air_pressure = m_pressure.Evaluate(m_kalt);
            m_air_temperature = m_temperature.Evaluate(m_kalt);
            m_sound_speed = Mathf.Pow((1.2f * 287f * (273.15f + m_air_temperature)), 0.5f);
            m_mach_speed = m_airspeed / m_sound_speed;


            // ---------------------------------- Control
            ϴ1s = processedPitch > 0f ? processedPitch * ϴ1sMin : processedPitch * ϴ1sMax;
            ϴ1c = processedRoll > 0f ? processedRoll * ϴ1cMin : processedRoll * ϴ1cMax;

            float β1c = -ϴ1s;
            float β1s = ϴ1c;

            // variables needed:
            // GENERAL: coreFactor
            // LIFT & THROTTLE: m_collective_balance, m_collective_power, maximumRotorLift, maximumClimbRate, maximumDecentSpeed
            // PITCH & ROLL: MomentFactor, β1s, β1c, m_balance, m_pitch_balance_factor, m_roll_balance_factor, mainRotorPosition
            // YAW: processedYaw, maximumTailThrust, centerOfGravity, tailRotorPosition, δTt

            //m_collective_power = processedCollective;

            if (m_playMode != PlayMode.Animation) // do not apply force for animated movement
            {
                //---------- LIFT & THROTTLE

                // WEIGHT FORCE
                if (helicopter.velocity.y < 0)
                {
                    m_balance_force = helicopter.mass * (Physics.gravity.y * -1) * (m_collective_balance * processedCollective * coreFactor) * (1 + Mathf.Abs(helicopter.velocity.y));
                    helicopter.AddRelativeForce(new Vector3(0, m_balance_force, 0), ForceMode.Force);
                }

                // ROTOR LIFT FORCE
                m_lift_force = maximumRotorLift * processedCollective * coreFactor;
                helicopter.AddRelativeForce(new Vector3(0, m_lift_force, 0), ForceMode.Force);
                //}

                float Fx = m_lift_force * Mathf.Sin(β1c) * Mathf.Cos(β1s);
                float Fy = -m_lift_force * Mathf.Cos(β1c) * Mathf.Sin(β1s);
                Force = new Vector3(-Fy, 0, -Fx);
                helicopter.AddRelativeForce(Force, ForceMode.Force);

                // Climb/Decent rate limiter
                //float m_climb_limit = maximumClimbRate; // / MathBase.toFtMin;
                //float m_decent_limit = maximumDecentSpeed; // / MathBase.toFtMin;
                if (helicopter.velocity.y > maximumClimbRate)
                {
                    helicopter.velocity = new Vector3(helicopter.velocity.x, maximumClimbRate, helicopter.velocity.z);
                }
                else if (helicopter.velocity.y < -maximumDecentSpeed)
                {
                    helicopter.velocity = new Vector3(helicopter.velocity.x, -maximumDecentSpeed, helicopter.velocity.z);
                }

                
                //---------- PITCH & ROLL
                float Mx = -MomentFactor * β1s * coreFactor;
                float My = -MomentFactor * β1c * coreFactor;
                //Moment = new Vector3(My, 0, Mx);
                Moment = new Vector3(My, -Mx, Mx); // edit by RF
                helicopter.AddRelativeTorque(Moment, ForceMode.Force);

                //---------- Add YAW when rolling base on speed --- by RF
                //m_tailPush = Mx * Mathf.InverseLerp(0, maximumSpeed, vz); // (new Vector2(helicopter.velocity.x, helicopter.velocity.z).magnitude));
                //helicopter.AddRelativeTorque(new Vector3(0, -m_tailPush * m_tailPushRatio, 0), ForceMode.Force);

                if (m_balance == MomentBalance.Active)
                {
                    float m_pitch = Mathf.DeltaAngle(0, -transform.rotation.eulerAngles.x);
                    float m_roll = Mathf.DeltaAngle(0, -transform.rotation.eulerAngles.z);
                    float m_pitch_moment = m_pitch_balance_factor * m_lift_force * Mathf.Sin((m_pitch - mainRotorPosition.localEulerAngles.x) * Mathf.Deg2Rad);
                    float m_roll_moment = m_roll_balance_factor * m_lift_force * Mathf.Sin(m_roll * Mathf.Deg2Rad);
                    helicopter.AddRelativeTorque(m_pitch_moment, 0f, m_roll_moment);
                }

                //---------- YAW
                m_yawMoment = processedYaw * maximumTailThrust * coreFactor * Vector3.Distance(centerOfGravity.position, tailRotorPosition.position);
                if (antiTorque == AntiTorque.Force)
                {
                    helicopter.AddForceAtPosition(maximumTailThrust * δTt * coreFactor * processedYaw * helicopter.transform.right, tailRotorPosition.position, ForceMode.Force);
                }
                else
                {
                    helicopter.AddRelativeTorque(new Vector3(0, -m_yawMoment, 0), ForceMode.Force);
                }
                
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        protected void UpdateViewControls()
        {
            if (Input.GetButtonDown("Camera Switch")) { heliCamera.ToggleCamera(); }
            if (Input.GetButtonDown("Camera View")) { heliCamera.ChangeView(false); }
            if (Input.GetButtonDown("Camera View Reverse")) { heliCamera.ChangeView(true); }
            if (Input.GetButtonDown("Toggle Data UI")) { ToggleDataUI(); }
            //if (Input.GetKeyDown(KeyCode.V)) { ToggleLightState(); }
            //if (Input.GetKeyDown(KeyCode.C)) { ToggleCamera(); }
            //if (Input.GetKeyDown(KeyCode.R)) { ResetScene(); }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        protected void UpdateFlightControls()
        {

            //if (inputType == InputType.Keyboard) || inputType == InputType.Mouse)

            // ----------------------------------------- Base
            rawPitchInput = -Input.GetAxis("Pitch");
            rawRollInput = Input.GetAxis("Roll");
            rawYawInput = Input.GetAxis("Rudder");
            // ------------------------------------- by RF {
            rawThrottleInput = Input.GetAxisRaw("Throttle");
            throttleInput = Input.GetAxis("Throttle");
            rawCollectiveInput = Input.GetAxisRaw("Collective");
            collectiveInput = Input.GetAxis("Collective");
            propellerInput = (Input.GetAxis("Propeller"));
            //rawThrottleInput = Input.GetAxis("Throttle"); // ori
            //throttleInput = (rawThrottleInput + 1) / 2; // ori
            //rawCollectiveInput = Input.GetAxis("Collective"); // ori
            //collectiveInput = (rawCollectiveInput + 1) / 2; // ori
            // ------------------------------------- by RF }


            // ----------------------------------------- Toggles
            if (Input.GetButtonDown("Start Engine")) { TurnOnEngines(false); }
            if (Input.GetButtonDown("Stop Engine")) { TurnOffEngines(); }
            if (Input.GetButtonDown("Light Switch")) { ToggleLight(); }
            if (Input.GetButtonDown("Reset Scene")) { ResetScene(); }

            ProcessCommand();
        }


        private void ProcessCommand()
        {
            timeStep = Time.deltaTime; if (timeStep > Time.fixedDeltaTime) { timeStep = Time.fixedDeltaTime; }//STUTTER PROTECTION
            //knotSpeed = vz * MathBase.toKnots;
            //ftHeight = m_altitude * MathBase.toFt;

            // --------------------------------------------- Collect Control Inputs ( added by RF
            /*basePitchInput = m_controller.rawPitchInput;
            baseRollInput = m_controller.rawRollInput;
            baseYawInput = m_controller.rawYawInput;
            baseThrottleInput = m_controller.rawThrottleInput;
            baseCollectiveInput = m_controller.rawCollectiveInput;
            basePropellerInput = m_controller.rawPropellerInput;
            //baseStabilizerTrimInput = -controller.input.stabilizerTrimInput;
            //baseTillerInput = controller.input.tillerInput;
            // ---- )
            */

            // ----------------------------------------------- Noise Filter
            if (Mathf.Abs(rawPitchInput) > pitchDeadZone) { presetPitchInput = rawPitchInput; } else { presetPitchInput = 0f; }
            if (Mathf.Abs(rawRollInput) > rollDeadZone) { presetRollInput = rawRollInput; } else { presetRollInput = 0f; }
            if (Mathf.Abs(rawYawInput) > yawDeadZone) { presetYawInput = rawYawInput; } else { presetYawInput = 0f; }

            float filteredPitchInput = ((Mathf.Abs(presetPitchInput) - pitchDeadZone) / (1 - pitchDeadZone)) * presetPitchInput;
            float filteredRollInput = ((Mathf.Abs(presetRollInput) - rollDeadZone) / (1 - rollDeadZone)) * presetRollInput;
            float filteredYawInput = ((Mathf.Abs(presetYawInput) - yawDeadZone) / (1 - yawDeadZone)) * presetYawInput;

            // ----------------------------------------------- Curve Filter
            commandPitchInput = pitchInputCurve.Evaluate(filteredPitchInput);
            commandRollInput = rollInputCurve.Evaluate(filteredRollInput);
            commandYawInput = yawInputCurve.Evaluate(filteredYawInput);

            // --------------------------------------------- Collect Performance Data
            currentAltitude = m_altitude; // / MathBase.toFt; // core.currentAltitude;
            currentClimbRate = m_vertical_speed; // (m_vertical_speed * MathBase.toFtMin) / 1000f; //core.verticalSpeed;
            currentSpeed = vz; // Vector3.Dot(controller.transform.forward, controller.helicopter.velocity); 
            currentHeading = m_heading; //core.headingDirection;
            //if (currentHeading > 180) { currentHeading -= 360f; }
            currentDriftSpeed = m_localflow.x; //core.driftSpeed;

            //--------------------------------------------- Estimate Rotation Angles
            /*float rawPitchAngle = transform.eulerAngles.x; if (rawPitchAngle > 180) { rawPitchAngle -= 360f; }
            float rawRollAngle = transform.eulerAngles.z; if (rawRollAngle > 180) { rawRollAngle -= 360f; }
            float rawYawAngle = transform.eulerAngles.y; if (rawYawAngle > 180) { rawYawAngle -= 360f; }

            rollAngle = (float)Math.Round(rawRollAngle, 2);
            pitchAngle = (float)Math.Round(rawPitchAngle, 2);
            yawAngle = (float)Math.Round(rawYawAngle, 2);
            */
            //m_αMax = -90f; foreach (PhantomRotor rotor in controller.forceRotors) { if (rotor.αMax > m_αMax) { m_αMax = rotor.αMax; } }

            /*pitchRate = m_controller.m_pitchrate; // core.pitchRate
            yawRate = m_controller.m_yawrate; // core.yawRate;
            rollRate = m_controller.m_rollrate; // core.rollRate;
            turnRate = m_controller.m_turnrate; // core.turnRate;
            */

            //--------------------------------------------- Convert raw collective input (up/down) to collective value
            float collectiveBalanceValue = 0.5f;
            float collectiveValue = (collectiveInput + 1f) / 2f;
            if (rawCollectiveInput > collectiveBalanceValue) { commandCollectiveInput = Mathf.MoveTowards(commandCollectiveInput, 1f, timeStep * m_collective_speed); }
            if (rawCollectiveInput < -collectiveBalanceValue) { commandCollectiveInput = Mathf.MoveTowards(commandCollectiveInput, 0f, timeStep * m_collective_speed); }
            //commandCollectiveInput = collectiveInput;
            /*if (rawCollectiveInput > 0) { collectiveInput = Mathf.MoveTowards(collectiveInput, 1f, timeStep * m_collective_speed); }
            if (rawCollectiveInput < 0) { collectiveInput = Mathf.MoveTowards(collectiveInput, 0f, timeStep * m_collective_speed); }            

            if (Mathf.Abs(collectiveInput) > collectiveDeadZone) { presetCollectiveInput = collectiveInput; } else { presetCollectiveInput = 0f; } // by RF
            float filteredCollectiveInput = ((Mathf.Abs(presetCollectiveInput) - collectiveDeadZone) / (1 - collectiveDeadZone)) * presetCollectiveInput; // by RF
            commandCollectiveInput = collectiveInputCurve.Evaluate(filteredCollectiveInput); // by RF
            */

            //--------------------------------------------- Convert raw throttle input (up/down) to throttle value
            if (rawThrottleInput > 0) { commandThrottleInput = Mathf.MoveTowards(commandThrottleInput, 1f, timeStep * m_throttle_speed); }
            if (rawThrottleInput < 0) { commandThrottleInput = Mathf.MoveTowards(commandThrottleInput, 0f, timeStep * m_throttle_speed); }

            //---------------------------------------------OPERATION MODE
            switch (m_playMode)
            {
                case PlayMode.Simulation: ManualControl(); break;
                //case AugmentationType.Autopilot: AssistedControl(); break;
                case PlayMode.Animation: AnimatedMovement(); break;
                //case AugmentationType.CommandAugmentation: CommandControl(); break;
                //case AugmentationType.Autonomous: brain.UpdateBrain(); break;
            }
        }

        private void AnimatedMovement()
        {
            processedPitch = 0f;
            processedRoll = 0f;
            processedYaw = 0f;
            switch (m_startMode)
            {
                case StartMode.Cold:
                    processedCollective = 0f;
                    processedThrottle = 1f;
                    break;
                case StartMode.Hot:
                case StartMode.Flying:
                    processedCollective = 1f;
                    processedThrottle = 1f;
                    break;
            }
        }

        private void ManualControl()
        {
            processedPitch = commandPitchInput;
            processedRoll = commandRollInput;
            //processedYaw = commandYawInput;
            processedYaw = (m_couple == RollYawCouple.Combined) ? (m_couple_level * -processedRoll) - commandYawInput : -commandYawInput;
            //processedCollective = Mathf.Max(collectiveInput, 0);
            processedCollective = commandCollectiveInput;
            //processedThrottle = (controller.startMode == Controller.StartMode.Flying) ? 1 : commandThrottleInput;
            processedThrottle = commandThrottleInput;

            // ---------------- Propeller Throttle Control
            /*if (propeller != null && propState == PropState.Balance)
            {
                float presetSpeed = commandSpeed / MathBase.toKnots;
                speedError = (presetSpeed - currentSpeed);
                processedPropeller = -propellerSolver.CalculateOutput(speedError, timeStep);
                if (float.IsNaN(processedPropeller) || float.IsInfinity(processedPropeller)) { processedPropeller = 0f; }
            }
            else { processedPropeller = basePropellerInput; }
            */
        }

        /*private void plotClimbCurve(float safeHeight, out AnimationCurve curve)
        {
            curve = new AnimationCurve();
            curve.AddKey(new Keyframe(0.0f * safeHeight, 0.0f));
            curve.AddKey(new Keyframe(0.1f * safeHeight, 0.1f));
            curve.AddKey(new Keyframe(0.2f * safeHeight, 0.2f));
            curve.AddKey(new Keyframe(0.4f * safeHeight, 0.4f));
            curve.AddKey(new Keyframe(0.6f * safeHeight, 0.6f));
            curve.AddKey(new Keyframe(0.8f * safeHeight, 0.8f));
            curve.AddKey(new Keyframe(1.0f * safeHeight, 1.0f));

#if UNITY_EDITOR
            for (int i = 0; i < curve.keys.Length; i++)
            {
                AnimationUtility.SetKeyLeftTangentMode(curve, i, AnimationUtility.TangentMode.Auto);
                AnimationUtility.SetKeyRightTangentMode(curve, i, AnimationUtility.TangentMode.Auto);
            }
#endif
        }*/

        // -------------------------------------------------------LIGHT CONTROL
        public void ToggleLight()
        {
            if (isControllable)
            {
                foreach (HelicopterBulb light in bulbs)
                {
                    if (light.state == HelicopterBulb.CurrentState.On)
                    {
                        light.SwitchOff();
                    }
                    else
                    {
                        light.SwitchOn();
                    }
                }
            }
        }

        public void TurnOffLights()
        {
            if (isControllable)
            {
                foreach (HelicopterBulb light in bulbs)
                {
                    if (light.state == HelicopterBulb.CurrentState.On)
                    {
                        light.SwitchOff();
                    }
                }
            }
        }

        public void ToggleDataUI()
        {
            SilantroMisc silantroMisc = GameObject.FindObjectOfType<SilantroMisc>(true);
            if (silantroMisc != null)
            {
                if (silantroMisc.gameObject.activeSelf)
                    silantroMisc.gameObject.SetActive(false);
                else
                    silantroMisc.gameObject.SetActive(true);
            }
        }

        //--------------------------------------------------------ENGINE STATE FUNCTIONS
        void StateStart()
        {
            if (clutching)
            {
                if (!exteriorSource.isPlaying)
                {
                    CurrentEngineState = EngineState.Active;
                    clutching = false;
                    StateActive();
                }
            }
            else
            {
                exteriorSource.Stop();
                if (interiorSource != null)
                {
                    interiorSource.Stop();
                }
                CurrentEngineState = EngineState.Off;
            }

            //------------------RUN
            norminalRPM = functionalRPM * (idlePercentage / 100f);
        }

        void StateOff()
        {
            if (exteriorSource.isPlaying && corePower < 0.01f) { exteriorSource.Stop(); }
            if (interiorSource != null && interiorSource.isPlaying && corePower < 0.01f) { interiorSource.Stop(); }


            //------------------START ENGINE
            if (start)
            {
                exteriorSource.clip = ignitionExterior;
                exteriorSource.Play();
                if (interiorSource != null)
                {
                    interiorSource.clip = ignitionInterior; interiorSource.Play();
                }
                CurrentEngineState = EngineState.Starting;
                clutching = true;
                active = true;
                StartCoroutine(ReturnIgnition());
            }

            //------------------RUN
            norminalRPM = 0f;
        }

        public void StateActive()
        {
            if (exteriorSource.isPlaying)
            {
                exteriorSource.Stop();
            }
            if (interiorSource != null && interiorSource.isPlaying) { interiorSource.Stop(); }

            //------------------STOP ENGINE
            if (shutdown)
            {
                exteriorSource.clip = shutdownExterior; exteriorSource.Play();
                if (interiorSource != null)
                {
                    interiorSource.clip = shutdownInterior; interiorSource.Play();
                }
                CurrentEngineState = EngineState.Off;
                active = false;
                StartCoroutine(ReturnIgnition());
            }

            //------------------RUN
            norminalRPM = (functionalRPM * (idlePercentage / 100f)) + ((functionalRPM) - (functionalRPM * (idlePercentage / 100f))) * processedThrottle;
        }

        public IEnumerator ReturnIgnition() { yield return new WaitForSeconds(0.5f); start = false; shutdown = false; }

        public override void TurnOnEngines(bool flying)
        {
            base.TurnOnEngines(flying);
            if (backIdle == null || ignitionExterior == null || shutdownExterior == null)
            {
                Debug.Log("Engine " + transform.name + " cannot start due to incorrect Audio configuration");
            }
            else
            {
                if (fuelLevel > 1f)
                {
                    if (m_startMode == StartMode.Cold)
                    {
                        start = true;
                    }
                    else if (m_startMode == StartMode.Hot || m_startMode == StartMode.Flying)
                    {
                        active = true;
                        StateActive();
                        clutching = false;
                        CurrentEngineState = EngineState.Active;
                    }
                }
                else
                {
                    Debug.Log("Engine " + transform.name + " cannot start due to low fuel");
                }
            }
        }

        public override void TurnOffEngines()
        {
            base.TurnOffEngines();
            shutdown = true;
        }

        //--------------------------------------------------------START AIRCRAFT
        public void PositionAircraft(Rigidbody craft, float startAltitude, float startHeading, float startSpeed, float startClimbRate)
        {
            Vector3 initialPosition = craft.transform.position;
            //craft.MovePosition(new Vector3(initialPosition.x, startAltitude, initialPosition.z));
            craft.transform.position = new Vector3(initialPosition.x, startAltitude, initialPosition.z);
            //craft.MoveRotation(Quaternion.Euler(new Vector3(0, startHeading, 0)));
            //craft.transform.Rotate(new Vector3(0, startHeading, 0));
            //craft.velocity = craft.transform.forward * startSpeed;
            craft.velocity = craft.transform.forward * startSpeed + craft.transform.up * startClimbRate;
        }

        public void PositionAircraft(Rigidbody craft)
        {
            Vector3 initialPosition = craft.transform.position;
            //craft.MovePosition(new Vector3(initialPosition.x, m_startAltitude, initialPosition.z));
            craft.transform.position = new Vector3(initialPosition.x, m_startAltitude, initialPosition.z);
            //craft.MoveRotation(Quaternion.Euler(new Vector3(0, m_startHeading, 0)));
            //craft.transform.Rotate(new Vector3(0, m_startHeading, 0));
            //craft.velocity = craft.transform.forward * m_startSpeed;
            craft.velocity = craft.transform.forward * m_startSpeed + craft.transform.up * m_startClimbRate;
        }

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        public void StartAircraft(Rigidbody craft)
        {
            // if (startMode == StartMode.Cold) --> use F1 button to start engine
            if (m_startMode == StartMode.Hot)
            {
                //POSITION AIRCRAFT
                //PositionAircraft(helicopter, helicopter.transform.position.x, 0, 0, 0);
                //START ENGINE
                TurnOnEngines(false);
            }
            else if (m_startMode == StartMode.Flying)
            {
                //POSITION AIRCRAFT
                PositionAircraft(craft);
                //START ENGINE
                TurnOnEngines(true);
            }
        }

        public void CommandAircraft(float speed, float altitude, float heading, float climbRate)
        {
            commandSpeed = speed;
            commandAltitude = altitude;
            commandHeading = heading;
            commandClimbRate = climbRate;
        }

        public override void Pause(bool paused)
        {
            // pause engine and visuals
            base.Pause(paused);

            // pause dust particle effect
            if (paused)
                dustParticle.Pause();
            else
                dustParticle.Play();

            // pause engine exhaust effect
            foreach (m_effect ef in m_effects)
            {
                if (paused)
                    ef.m_effect_particule.Pause();
                else
                    ef.m_effect_particule.Play();
            }

            // pause audio
            AudioListener.pause = paused;
        }

        /*private IEnumerator HorizontalSpeedReckoner()
        {

            YieldInstruction timedWait = new WaitForSeconds(speedUpdateDelay);
            //Vector3 lastPosition = transform.position;
            float lastPosition = transform.position.z;
            float lastTimestamp = Time.time;

            while (enabled)
            {
                yield return timedWait;

                var deltaPosition = transform.position.z - lastPosition;
                var deltaTime = Time.time - lastTimestamp;

                if (Mathf.Approximately(deltaPosition, 0f)) // Clean up "near-zero" displacement
                    deltaPosition = 0f;

                m_horzSpeed = deltaPosition / deltaTime;


                lastPosition = transform.position.z;
                lastTimestamp = Time.time;
            }
        }

        private IEnumerator VerticalSpeedReckoner()
        {

            YieldInstruction timedWait = new WaitForSeconds(speedUpdateDelay);
            //Vector3 lastPosition = transform.position;
            float lastPosition = transform.position.y;
            float lastTimestamp = Time.time;

            while (enabled)
            {
                yield return timedWait;

                var deltaPosition = transform.position.y - lastPosition;
                var deltaTime = Time.time - lastTimestamp;

                //if (Mathf.Approximately(deltaPosition, 0f)) // Clean up "near-zero" displacement
                //    deltaPosition = 0f;

                m_vertSpeed = deltaPosition / deltaTime;


                lastPosition = transform.position.y;
                lastTimestamp = Time.time;
            }
        }*/
    }
}

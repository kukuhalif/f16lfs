using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

namespace HelicopterSim
{

// --------------------------------------------- Controller
#region Controller
#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(HelicopterController))]
    public class ControllerEditor : Editor
    {

        Color backgroundColor;
        Color silantroColor = Color.yellow; // new Color(1.0f, 0.40f, 0f);
        HelicopterController controller;
        SerializedProperty m_helper;
        public int toolbarTab;
        public string currentTab;

        public int toolbarTabC; public string currentTabC;

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        private void OnEnable() { controller = (HelicopterController)target; m_helper = serializedObject.FindProperty("m_mouse"); }


        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;
            //DrawDefaultInspector ();
            serializedObject.Update();


            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(2f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Config", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("m_type"), new GUIContent("Type"));
            //GUILayout.Space(3f);
            /*EditorGUILayout.PropertyField(serializedObject.FindProperty("inputType"), new GUIContent("Input"));
            if(controller.inputType == Controller.InputType.Mouse)
            {
                GUILayout.Space(2f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Mouse Control", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_range"), new GUIContent("Aim Range"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_control_sensitivity"), new GUIContent("Control Sensitivity"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_mouse_sensitivity"), new GUIContent("Mouse Sensitivity"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_damp_speed"), new GUIContent("Damp Speed"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_turn_angle"), new GUIContent("Turn Angle"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_hold_key"), new GUIContent("Hold Key"));

                GUILayout.Space(5f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Output Control", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("solver"), new GUIContent("Solver Method"));
                if (controller.m_mouse.solver == MouseControl.m_solver.PID)
                {
                    GUILayout.Space(3f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Gains", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_pitch_gain"), new GUIContent("Pitch Gain"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_roll_gain"), new GUIContent("Roll Gain"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_yaw_gain"), new GUIContent("Yaw Gain"));
                }

                GUILayout.Space(6f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("keyboard_override"), new GUIContent("Keyboard Override"));
                if(controller.keyboard_override == Controller.m_override.Active)
                {
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("m_key_overide"), new GUIContent("Threshold"));
                }

                GUILayout.Space(5f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("HUD Control", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_target"), new GUIContent("Mouse Pointer"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_forward"), new GUIContent("Helicopter Pointer"));

                GUILayout.Space(5f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Camera Control", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_camera"), new GUIContent("Camera"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_camera_pivot"), new GUIContent("Camera Pivot"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_container"), new GUIContent("Camera Holder"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(m_helper.FindPropertyRelative("m_target_object"), new GUIContent("Target Object"));

                GUILayout.Space(13f);
            }
            */
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_playMode"), new GUIContent("Play Mode"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_startMode"), new GUIContent("Start Mode"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("antiTorque"), new GUIContent("Tail Control"));


            if (controller.m_startMode == HelicopterController.StartMode.Flying)
            {
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Instantenous Start (Speed = m/s, Altitude = m)", MessageType.None);
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_startSpeed"), new GUIContent("Start Speed"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_startAltitude"), new GUIContent("Start Altitude"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_startHeading"), new GUIContent("Start Heading"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_startClimbRate"), new GUIContent("Start Climb Rate"));
                GUILayout.Space(3f);
            }

            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(15f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Arcade Controls", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_couple"), new GUIContent("Roll-Yaw Couple"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_balance"), new GUIContent("Moment Balance"));
            /*GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_climb"), new GUIContent("Climb Key"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_decend"), new GUIContent("Decend Key"));
            */

            GUILayout.Space(8f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_collective_speed"), new GUIContent("Collective Speed"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_collective_balance"), new GUIContent("Lift Balance Factor"));
            if (controller.m_balance == HelicopterController.MomentBalance.Active)
            {
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_pitch_balance_factor"), new GUIContent("Pitch Balance Factor"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_roll_balance_factor"), new GUIContent("Roll Balance Factor"));
            }
            if (controller.m_couple == HelicopterController.RollYawCouple.Combined)
            {
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_couple_level"), new GUIContent("Couple Level"));
            }
            GUILayout.Space(8f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_tailPushRatio"), new GUIContent("Turning Tail Push Ratio"));
            GUILayout.Space(8f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_throttle_speed"), new GUIContent("Throttle Speed"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_power_limit"), new GUIContent("Power Limit"));
            GUILayout.Space(3f);




            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(25f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Variables", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("mainRotorRadius"), new GUIContent("Rotor Radius"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumRotorLift"), new GUIContent("Max Lift"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumRotorTorque"), new GUIContent("Max Torque"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumTailThrust"), new GUIContent("Max Tail Thrust"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("MomentFactor"), new GUIContent("Moment Factor"));
            //GUILayout.Space(3f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("mainRotorRPM"), new GUIContent("Main Rotor RPM"));
            //GUILayout.Space(3f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("tailRotorRPM"), new GUIContent("Tail Rotor RPM"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumClimbRate"), new GUIContent("Max Climb Rate (ft/min)"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumDecentSpeed"), new GUIContent("Max Decent Rate (ft/min)"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maxAngularSpeed"), new GUIContent("Max Angular Speed (rad/s)"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationDrag"), new GUIContent("Angular Drag"));

            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(10f);
            GUI.color = Color.white;
            EditorGUILayout.HelpBox("Power Control", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("baseCoreAcceleration"), new GUIContent("Engine Acceleration"));

            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("functionalRPM"), new GUIContent("Functional RPM"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("idlePercentage"), new GUIContent("Idle Percentage"));


            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(25f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Fuel & Weight", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("fuelCapacity"), new GUIContent("Fuel Capacity"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("bingoFuel"), new GUIContent("Bingo Fuel"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("fuelConsumption"), new GUIContent("Burn Rate (kg/s)"));
            GUILayout.Space(3f);
            EditorGUILayout.LabelField("Fuel Level", controller.fuelLevel.ToString("0.0") + " kg");
            GUILayout.Space(6f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("emptyWeight"), new GUIContent("Empty Weight"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumWeight"), new GUIContent("Maximum Weight"));
            GUILayout.Space(3f);
            EditorGUILayout.LabelField("Current Weight", controller.currentWeight.ToString("0.0") + " kg");



            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(15f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Inertia Settings", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("xInertia"), new GUIContent("Pitch Axis"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("zInertia"), new GUIContent("Roll Axis"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("yInertia"), new GUIContent("Yaw Axis"));

            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            /*GUILayout.Space(25f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Camera Settings", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            toolbarTabC = GUILayout.Toolbar(toolbarTabC, new string[] { "Exterior Camera", "Interior Camera" });
            switch (toolbarTabC)
            {
                case 0: currentTabC = "Exterior Camera"; break;
                case 1: currentTabC = "Interior Camera"; break;
            }


            switch (currentTabC)
            {
                case "Exterior Camera":
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("normalExterior"), new GUIContent("Exterior Camera"));
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Movement", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    serializedObject.FindProperty("elevationSensitivity").floatValue = EditorGUILayout.Slider("Elevation Sensitivity", serializedObject.FindProperty("elevationSensitivity").floatValue, 0f, 5f);
                    GUILayout.Space(3f);
                    serializedObject.FindProperty("azimuthSensitivity").floatValue = EditorGUILayout.Slider("Azimuth Sensitivity", serializedObject.FindProperty("azimuthSensitivity").floatValue, 0f, 5f);
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Positioning", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    controller.camera.maximumRadius = EditorGUILayout.FloatField("Camera Distance", controller.camera.maximumRadius);

                    break;
                case "Interior Camera":
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("normalInterior"), new GUIContent("Interior Camera"));
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Movement", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    serializedObject.FindProperty("mouseSensitivity").floatValue = EditorGUILayout.Slider("Mouse Sensitivity", serializedObject.FindProperty("mouseSensitivity").floatValue, 0f, 100f);
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Positioning", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    serializedObject.FindProperty("clampAngle").floatValue = EditorGUILayout.Slider("View Angle", serializedObject.FindProperty("clampAngle").floatValue, 10f, 210f);


                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Zoom Settings", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("zoomEnabled"));
                    if (controller.camera.zoomEnabled)
                    {
                        GUILayout.Space(3f);
                        serializedObject.FindProperty("maximumFOV").floatValue = EditorGUILayout.Slider("Minimum FOV", serializedObject.FindProperty("maximumFOV").floatValue, 0f, 100f);
                        GUILayout.Space(3f);
                        serializedObject.FindProperty("zoomSensitivity").floatValue = EditorGUILayout.Slider("Zoom Sensitivity", serializedObject.FindProperty("zoomSensitivity").floatValue, 0f, 20f);
                        GUILayout.Space(3f);
                        EditorGUILayout.LabelField("Current FOV", controller.camera.currentFOV.ToString("0.00"));
                    }
                    break;
            }
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraFocus"), new GUIContent("Camera Focus"));
            */
            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(25f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Connections", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("mainRotorPosition"), new GUIContent("Main Rotor"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("mainRotorAxis"), new GUIContent("Rotation Axis"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("mainRotorDirection"), new GUIContent("Rotation Direction"));


            GUILayout.Space(6f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("tailRotorPosition"), new GUIContent("Tail Rotor"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("tailRotorAxis"), new GUIContent("Rotation Axis"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("tailRotorDirection"), new GUIContent("Rotation Direction"));

            GUILayout.Space(6f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("centerOfGravity"), new GUIContent("COG Position"));


            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Effects Configuration", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();
            SerializedProperty ematerials = serializedObject.FindProperty("m_effects");
            GUIContent earrelLabel = new GUIContent("Effect Count");
            EditorGUILayout.PropertyField(ematerials.FindPropertyRelative("Array.size"), earrelLabel);
            GUILayout.Space(3f);
            for (int i = 0; i < ematerials.arraySize; i++)
            {
                //GUIContent label = new GUIContent("Effect " + (i + 1).ToString());
                //EditorGUILayout.PropertyField(ematerials.GetArrayElementAtIndex(i), label);

                SerializedProperty reference = ematerials.GetArrayElementAtIndex(i);
                SerializedProperty particule = reference.FindPropertyRelative("m_effect_particule");
                SerializedProperty emission = reference.FindPropertyRelative("m_effect_limit");

                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Effect : " + (i + 1).ToString(), MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(particule, new GUIContent("Particle"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(emission, new GUIContent("Emission Limit"));
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            GUILayout.Space(6f);
            GUI.color = Color.white;
            EditorGUILayout.HelpBox("Ground Dust Effect", MessageType.None);
            GUI.color = backgroundColor;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dustEnabled"), new GUIContent("Enable Dust"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dustParticle"), new GUIContent("Dust Particle FX"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dustAmount"), new GUIContent("Dust Amount"));

            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Visuals Configuration", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("rotorVisualType"), new GUIContent(" "));
            if (controller.rotorVisualType == HelicopterController.RotorVisualType.Complete)
            {
                GUILayout.Space(2f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Blurred Rotor Materials", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                SerializedProperty bmaterials = serializedObject.FindProperty("blurredRotor");
                GUIContent barrelLabel = new GUIContent("Material Count");
                EditorGUILayout.PropertyField(bmaterials.FindPropertyRelative("Array.size"), barrelLabel);
                GUILayout.Space(3f);
                for (int i = 0; i < bmaterials.arraySize; i++)
                {
                    GUIContent label = new GUIContent("Material " + (i + 1).ToString());
                    EditorGUILayout.PropertyField(bmaterials.GetArrayElementAtIndex(i), label);
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
                GUILayout.Space(5f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Normal Rotor Materials", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                SerializedProperty nmaterials = serializedObject.FindProperty("normalRotor");
                GUIContent nbarrelLabel = new GUIContent("Material Count");
                EditorGUILayout.PropertyField(nmaterials.FindPropertyRelative("Array.size"), nbarrelLabel);
                GUILayout.Space(3f);
                for (int i = 0; i < nmaterials.arraySize; i++)
                {
                    GUIContent label = new GUIContent("Material " + (i + 1).ToString());
                    EditorGUILayout.PropertyField(nmaterials.GetArrayElementAtIndex(i), label);
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
            }
            else if (controller.rotorVisualType == HelicopterController.RotorVisualType.Partial)
            {
                GUILayout.Space(2f);
                GUI.color = Color.white;
                EditorGUILayout.HelpBox("Blurred Rotor Materials", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                SerializedProperty bmaterials = serializedObject.FindProperty("blurredRotor");
                GUIContent barrelLabel = new GUIContent("Material Count");
                EditorGUILayout.PropertyField(bmaterials.FindPropertyRelative("Array.size"), barrelLabel);
                GUILayout.Space(3f);
                for (int i = 0; i < bmaterials.arraySize; i++)
                {
                    GUIContent label = new GUIContent("Material " + (i + 1).ToString());
                    EditorGUILayout.PropertyField(bmaterials.GetArrayElementAtIndex(i), label);
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
            }




            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(25f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Sound Configuration", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("soundMode"), new GUIContent("Mode"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("interiorMode"), new GUIContent("Cabin Sounds"));
            GUILayout.Space(5f);
            if (controller.soundMode == HelicopterController.SoundMode.Basic)
            {
                if (controller.interiorMode == HelicopterController.InteriorMode.Off)
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("ignitionExterior"), new GUIContent("Ignition Sound"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("backIdle"), new GUIContent("Idle Sound"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("shutdownExterior"), new GUIContent("Shutdown Sound"));
                }
                else
                {
                    toolbarTab = GUILayout.Toolbar(toolbarTab, new string[] { "Exterior Sounds", "Interior Sounds" });
                    GUILayout.Space(5f);
                    switch (toolbarTab)
                    {
                        case 0: currentTab = "Exterior Sounds"; break;
                        case 1: currentTab = "Interior Sounds"; break;
                    }
                    switch (currentTab)
                    {
                        case "Exterior Sounds":
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("ignitionExterior"), new GUIContent("Exterior Ignition Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("backIdle"), new GUIContent("Exterior Idle Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("shutdownExterior"), new GUIContent("Exterior Shutdown Sound"));
                            break;

                        case "Interior Sounds":
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("ignitionInterior"), new GUIContent("Interior Ignition Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("interiorIdle"), new GUIContent("Interior Idle Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("shutdownInterior"), new GUIContent("Interior Shutdown Sound"));
                            break;
                    }
                }
            }
            else
            {
                //GUILayout.Space(3f);
                if (controller.interiorMode == HelicopterController.InteriorMode.Off)
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("ignitionExterior"), new GUIContent("Ignition Sound"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("frontIdle"), new GUIContent("Front Idle Sound"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("sideIdle"), new GUIContent("Side Idle Sound"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("backIdle"), new GUIContent("Rear Idle Sound"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("shutdownExterior"), new GUIContent("Shutdown Sound"));
                }
                else
                {
                    toolbarTab = GUILayout.Toolbar(toolbarTab, new string[] { "Exterior Sounds", "Interior Sounds" });
                    GUILayout.Space(5f);
                    switch (toolbarTab)
                    {
                        case 0: currentTab = "Exterior Sounds"; break;
                        case 1: currentTab = "Interior Sounds"; break;
                    }
                    switch (currentTab)
                    {
                        case "Exterior Sounds":
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("ignitionExterior"), new GUIContent("Exterior Ignition Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("frontIdle"), new GUIContent("Exterior Front Idle Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("sideIdle"), new GUIContent("Exterior Side Idle Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("backIdle"), new GUIContent("Exterior Rear Idle Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("shutdownExterior"), new GUIContent("Exterior Shutdown Sound"));
                            break;

                        case "Interior Sounds":
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("ignitionInterior"), new GUIContent("Interior Ignition Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("interiorIdle"), new GUIContent("Interior Idle Sound"));
                            GUILayout.Space(2f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("shutdownInterior"), new GUIContent("Interior Shutdown Sound"));
                            break;
                    }
                }
            }

            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("rotorRunning"), new GUIContent("Rotor Running"));

            // Information
            GUILayout.Space(25f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Data Monitoring", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("vz"), new GUIContent("vz"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_airspeed"), new GUIContent("m_airspeed"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_horzSpeed"), new GUIContent("m_horzSpeed"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_vertical_speed"), new GUIContent("m_vertical_speed"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_vertSpeed"), new GUIContent("m_vertSpeed"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_altitude"), new GUIContent("m_altitude"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_radarAltitude"), new GUIContent("m_radarAltitude"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_heading"), new GUIContent("m_heading"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_bankAngle"), new GUIContent("m_bankAngle"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("fuelLevel"), new GUIContent("fuelLevel"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("coreRPM"), new GUIContent("coreRPM"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("processedThrottle"), new GUIContent("processedThrottle"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("processedCollective"), new GUIContent("processedCollective"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("processedPitch"), new GUIContent("processedPitch"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("processedRoll"), new GUIContent("processedRoll"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("processedYaw"), new GUIContent("processedYaw"));
            GUILayout.Space(5f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_tailPush"), new GUIContent("m_tailPush"));
            GUILayout.Space(5f);



            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
    #endregion

// ---------------------------------------------- Lever
#region Lever
#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(HelicopterLever))]
    public class HelicopterLeverEditor : Editor
    {
        Color backgroundColor;
        Color silantroColor = Color.yellow; // new Color(1, 0.4f, 0);
        HelicopterLever lever;


        //------------------------------------------------------------------------
        private SerializedProperty leverType;
        private SerializedProperty movementType;
        private SerializedProperty animator;
        private SerializedProperty stickType;
        private SerializedProperty leverObj;
        private SerializedProperty yokeObj;

        private SerializedProperty pitchRotationAxis;
        private SerializedProperty MaximumPitchDeflection;
        private SerializedProperty pitchAxis;

        private SerializedProperty rollRotationAxis;
        private SerializedProperty MaximumRollDeflection;
        private SerializedProperty rollAxis;



        private void OnEnable()
        {
            lever = (HelicopterLever)target;

            leverType = serializedObject.FindProperty("leverType");
            movementType = serializedObject.FindProperty("movementType"); // by RF
            animator = serializedObject.FindProperty("animFlightControl");
            stickType = serializedObject.FindProperty("stickType");
            leverObj = serializedObject.FindProperty("lever");
            yokeObj = serializedObject.FindProperty("yoke");

            pitchRotationAxis = serializedObject.FindProperty("pitchRotationAxis");
            MaximumPitchDeflection = serializedObject.FindProperty("MaximumPitchDeflection");
            pitchAxis = serializedObject.FindProperty("pitchDirection");

            rollRotationAxis = serializedObject.FindProperty("rollRotationAxis");
            MaximumRollDeflection = serializedObject.FindProperty("MaximumRollDeflection");
            rollAxis = serializedObject.FindProperty("rollDirection");            
        }


        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;
            //DrawDefaultInspector ();
            serializedObject.Update();


            // ------------------------------------- by RF {
            //GUILayout.Space(10f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("m_type"), new GUIContent("System Type"));
            //GUILayout.Space(3f);
            // ------------------------------------- by RF {

            GUILayout.Space(2f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Lever Type", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(leverType, new GUIContent("Lever Type"));

            // Get movement type
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(movementType, new GUIContent("Movement Type"));

            if (lever.movementType == HelicopterLever.MovementType.Transform)
            {


                // -------------------------------------------------------- Control Stick
                if (lever.leverType == HelicopterLever.LeverType.Stick)
                {
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(stickType, new GUIContent("Control Type"));
                    GUILayout.Space(5f);
                    if (lever.stickType == HelicopterLever.StickType.Joystick)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(leverObj, new GUIContent("Stick"));
                    }
                    else
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(leverObj, new GUIContent("Yoke Stick"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(yokeObj, new GUIContent("Yoke Wheel"));
                    }

                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(pitchRotationAxis, new GUIContent("Pitch Rotation Axis"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(MaximumPitchDeflection, new GUIContent("Max Pitch Deflection"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(pitchAxis, new GUIContent("Pitch Direction"));

                    GUILayout.Space(8f);
                    EditorGUILayout.PropertyField(rollRotationAxis, new GUIContent("Roll Rotation Axis"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(MaximumRollDeflection, new GUIContent("Max Roll Deflection"));
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(rollAxis, new GUIContent("Roll Direction"));
                }


                // -------------------------------------------------------- Collective
                else if (lever.leverType == HelicopterLever.LeverType.Collective)
                {
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(leverObj, new GUIContent("Collective Lever"));
                    GUILayout.Space(3f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationAxis"), new GUIContent("Rotation Axis"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("direction"), new GUIContent("Rotation Direction"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumDeflection"), new GUIContent("Maximum Deflection"));

                }

                // -------------------------------------------------------- Throttle
                else if (lever.leverType == HelicopterLever.LeverType.Throttle)
                {
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("lever"), new GUIContent("Throttle Handle"));
                    GUILayout.Space(2f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationAxis"), new GUIContent("Rotation Axis"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("direction"), new GUIContent("Rotation Direction"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumDeflection"), new GUIContent("Maximum Deflection"));                    
                }

                // -------------------------------------------------------- Rudder Pedals
                else if (lever.leverType == HelicopterLever.LeverType.Pedal)
                {
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("pedalType"), new GUIContent("Pedal Type"));

                    if (lever.pedalType == HelicopterLever.PedalType.Hinged)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Right Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightPedal"), new GUIContent("Right Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightPedalTube"), new GUIContent("Right Pedal Control Tube"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightRotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightDirection"), new GUIContent("Rotation Deflection"));


                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Left Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftPedal"), new GUIContent("Left Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftPedalTube"), new GUIContent("Left Pedal Control Tube"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftRotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftDirection"), new GUIContent("Rotation Deflection"));

                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("pedalMode"), new GUIContent("Clamped Together"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumDeflection"), new GUIContent("Maximum Deflection"));                        
                    }
                    else if (lever.pedalType == HelicopterLever.PedalType.Sliding)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Right Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightPedal"), new GUIContent("Right Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightRotationAxis"), new GUIContent("Sliding Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rightDirection"), new GUIContent("Sliding Deflection"));


                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Left Pedal Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftPedal"), new GUIContent("Left Pedal"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftRotationAxis"), new GUIContent("Sliding Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("leftDirection"), new GUIContent("Sliding Deflection"));


                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Sliding Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumSlidingDistance"), new GUIContent("Sliding Distance (cm)"));                        
                    }
                }

                // -------------------------------------------------------- Gear Indicator
                else if (lever.leverType == HelicopterLever.LeverType.GearIndicator)
                {
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(leverObj, new GUIContent("Lever Handle"));
                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;

                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationAxis"), new GUIContent("Rotation Axis"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("direction"), new GUIContent("Rotation Direction"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumDeflection"), new GUIContent("Maximum Deflection"));


                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Bulb Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("bulbMaterial"), new GUIContent("Bulb Material"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumBulbEmission"), new GUIContent("Maximum Emission"));
                }
            }


            else if (lever.movementType == HelicopterLever.MovementType.Animator)
            {
                // Get Animator Component
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(animator, new GUIContent("Animator Component"));

                if (lever.animFlightControl != null)
                {
                    // Get animator controller parameters
                    AnimatorControllerParameter[] animParams = (lever.animFlightControl.runtimeAnimatorController as AnimatorController).parameters;
                    string[] animParamNames = new string[animParams.Length];
                    var idx = 0;
                    foreach (AnimatorControllerParameter param in animParams)
                    {
                        animParamNames[idx] = param.name;
                        idx++;
                    }

                    // Get animator layers
                    AnimatorControllerLayer[] animLayers = (lever.animFlightControl.runtimeAnimatorController as AnimatorController).layers;
                    string[] animLayerNames = new string[animLayers.Length];
                    idx = 0;
                    foreach (AnimatorControllerLayer layer in animLayers)
                    {
                        animLayerNames[idx] = layer.name;
                        idx++;
                    }


                    // -------------------------------------------------------- Control Stick
                    if (lever.leverType == HelicopterLever.LeverType.Stick)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);

                        lever.animParamIdx1 = EditorGUILayout.Popup("Pitch Animation Parameter", lever.animParamIdx1, animParamNames);
                        lever.animParam1 = animParamNames[lever.animParamIdx1];
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1SyncValue"), new GUIContent("Pitch Input Value = Anim Param Value?"));
                        GUILayout.Space(3f);
                        if (lever.animParam1SyncValue == false)
                        {
                            EditorGUI.indentLevel++;
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMin"), new GUIContent("Minimum Pitch Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMax"), new GUIContent("Maximum Pitch Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMin"), new GUIContent("Minimum Pitch Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMax"), new GUIContent("Maximum Pitch Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUI.indentLevel--;
                        }
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InvertDirection"), new GUIContent("Inverted Pitch Value?"));
                        GUILayout.Space(3f);

                        lever.animParamIdx2 = EditorGUILayout.Popup("Roll Animation Parameter", lever.animParamIdx2, animParamNames);
                        lever.animParam2 = animParamNames[lever.animParamIdx2];
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2SyncValue"), new GUIContent("Roll Input Value = Anim Param Value?"));
                        GUILayout.Space(3f);
                        if (lever.animParam2SyncValue == false)
                        {
                            EditorGUI.indentLevel++;
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2InputMin"), new GUIContent("Minimum Roll Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2InputMax"), new GUIContent("Maximum Roll Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2OutputMin"), new GUIContent("Minimum Roll Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2OutputMax"), new GUIContent("Maximum Roll Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUI.indentLevel--;
                        }
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam2InvertDirection"), new GUIContent("Inverted Roll Value?"));
                        GUILayout.Space(3f);

                        lever.animLayerIdx = EditorGUILayout.Popup("Animation Layer", lever.animLayerIdx, animLayerNames);
                        lever.animLayer = animLayerNames[lever.animLayerIdx];
                        GUILayout.Space(3f);

                        // Get animation states
                        ChildAnimatorState[] smStates = animLayers[lever.animLayerIdx].stateMachine.states;
                        string[] smStateNames = new string[smStates.Length];
                        idx = 0;
                        foreach (ChildAnimatorState state in smStates)
                        {
                            smStateNames[idx] = state.state.name;
                            idx++;
                        }

                        lever.smStateIdx = EditorGUILayout.Popup("Animation State", lever.smStateIdx, smStateNames);
                        lever.smState = smStateNames[lever.smStateIdx];
                        GUILayout.Space(3f);
                    }



                    // -------------------------------------------------------- Collective
                    else if (lever.leverType == HelicopterLever.LeverType.Collective)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);

                        lever.animParamIdx1 = EditorGUILayout.Popup("Collective Animation Parameter", lever.animParamIdx1, animParamNames);
                        lever.animParam1 = animParamNames[lever.animParamIdx1];
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1SyncValue"), new GUIContent("Collective Input Value = Anim Param Value?"));
                        GUILayout.Space(3f);
                        if (lever.animParam1SyncValue == false)
                        {
                            EditorGUI.indentLevel++;
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMin"), new GUIContent("Minimum Collective Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMax"), new GUIContent("Maximum Collective Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMin"), new GUIContent("Minimum Collective Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMax"), new GUIContent("Maximum Collective Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUI.indentLevel--;
                        }
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InvertDirection"), new GUIContent("Inverted Collective Value?"));
                        GUILayout.Space(3f);

                        lever.animLayerIdx = EditorGUILayout.Popup("Animation Layer", lever.animLayerIdx, animLayerNames);
                        lever.animLayer = animLayerNames[lever.animLayerIdx];
                        GUILayout.Space(3f);

                        // Get animation states
                        ChildAnimatorState[] smStates = animLayers[lever.animLayerIdx].stateMachine.states;
                        string[] smStateNames = new string[smStates.Length];
                        idx = 0;
                        foreach (ChildAnimatorState state in smStates)
                        {
                            smStateNames[idx] = state.state.name;
                            idx++;
                        }

                        lever.smStateIdx = EditorGUILayout.Popup("Animation State", lever.smStateIdx, smStateNames);
                        lever.smState = smStateNames[lever.smStateIdx];
                        GUILayout.Space(3f);
                    }




                    // -------------------------------------------------------- Throttle
                    else if (lever.leverType == HelicopterLever.LeverType.Throttle)
                    {
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);

                        lever.animParamIdx1 = EditorGUILayout.Popup("Throttle Animation Parameter", lever.animParamIdx1, animParamNames);
                        lever.animParam1 = animParamNames[lever.animParamIdx1];
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1SyncValue"), new GUIContent("Throttle Input Value = Anim Param Value?"));
                        GUILayout.Space(3f);
                        if (lever.animParam1SyncValue == false)
                        {
                            EditorGUI.indentLevel++;
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMin"), new GUIContent("Minimum Throttle Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMax"), new GUIContent("Maximum Throttle Input Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMin"), new GUIContent("Minimum Throttle Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMax"), new GUIContent("Maximum Throttle Animation Parameter Value"));
                            GUILayout.Space(3f);
                            EditorGUI.indentLevel--;
                        }
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InvertDirection"), new GUIContent("Inverted Throttle Value?"));
                        GUILayout.Space(3f);

                        lever.animLayerIdx = EditorGUILayout.Popup("Animation Layer", lever.animLayerIdx, animLayerNames);
                        lever.animLayer = animLayerNames[lever.animLayerIdx];
                        GUILayout.Space(3f);

                        // Get animation states
                        ChildAnimatorState[] smStates = animLayers[lever.animLayerIdx].stateMachine.states;
                        string[] smStateNames = new string[smStates.Length];
                        idx = 0;
                        foreach (ChildAnimatorState state in smStates)
                        {
                            smStateNames[idx] = state.state.name;
                            idx++;
                        }

                        lever.smStateIdx = EditorGUILayout.Popup("Animation State", lever.smStateIdx, smStateNames);
                        lever.smState = smStateNames[lever.smStateIdx];
                        GUILayout.Space(3f);
                    }




                    // -------------------------------------------------------- Rudder Pedals
                    if (lever.leverType == HelicopterLever.LeverType.Pedal)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("pedalType"), new GUIContent("Pedal Type"));

                        if (lever.pedalType == HelicopterLever.PedalType.Hinged)
                        {
                            GUILayout.Space(10f);
                            GUI.color = Color.white;
                            EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
                            GUI.color = backgroundColor;
                            GUILayout.Space(3f);

                            lever.animParamIdx1 = EditorGUILayout.Popup("Pedal Animation Parameter", lever.animParamIdx1, animParamNames);
                            lever.animParam1 = animParamNames[lever.animParamIdx1];
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1SyncValue"), new GUIContent("Pedal Input Value = Anim Param Value?"));
                            GUILayout.Space(3f);
                            if (lever.animParam1SyncValue == false)
                            {
                                EditorGUI.indentLevel++;
                                GUILayout.Space(3f);
                                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMin"), new GUIContent("Minimum Pedal Input Value"));
                                GUILayout.Space(3f);
                                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InputMax"), new GUIContent("Maximum Pedal Input Value"));
                                GUILayout.Space(3f);
                                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMin"), new GUIContent("Minimum Pedal Animation Parameter Value"));
                                GUILayout.Space(3f);
                                EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1OutputMax"), new GUIContent("Maximum Pedal Animation Parameter Value"));
                                GUILayout.Space(3f);
                                EditorGUI.indentLevel--;
                            }
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("animParam1InvertDirection"), new GUIContent("Inverted Pedal Value?"));
                            GUILayout.Space(3f);

                            lever.animLayerIdx = EditorGUILayout.Popup("Animation Layer", lever.animLayerIdx, animLayerNames);
                            lever.animLayer = animLayerNames[lever.animLayerIdx];
                            GUILayout.Space(3f);

                            // Get animation states
                            ChildAnimatorState[] smStates = animLayers[lever.animLayerIdx].stateMachine.states;
                            string[] smStateNames = new string[smStates.Length];
                            idx = 0;
                            foreach (ChildAnimatorState state in smStates)
                            {
                                smStateNames[idx] = state.state.name;
                                idx++;
                            }

                            lever.smStateIdx = EditorGUILayout.Popup("Animation State", lever.smStateIdx, smStateNames);
                            lever.smState = smStateNames[lever.smStateIdx];
                            GUILayout.Space(3f);
                        }

                        if (lever.pedalType == HelicopterLever.PedalType.Sliding)
                        {
                            GUILayout.Space(10f);
                            GUI.color = Color.white;
                            EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
                            GUI.color = backgroundColor;
                            GUILayout.Space(3f);
                            lever.animParamIdx1 = EditorGUILayout.Popup("Pedal Animation Parameter", lever.animParamIdx1, animParamNames);
                            lever.animParam1 = animParamNames[lever.animParamIdx1];
                            GUILayout.Space(3f);
                            lever.animLayerIdx = EditorGUILayout.Popup("Animation Layer", lever.animLayerIdx, animLayerNames);
                            lever.animLayer = animLayerNames[lever.animLayerIdx];
                            GUILayout.Space(3f);

                            // Get animation states
                            ChildAnimatorState[] smStates = animLayers[lever.animLayerIdx].stateMachine.states;
                            string[] smStateNames = new string[smStates.Length];
                            idx = 0;
                            foreach (ChildAnimatorState state in smStates)
                            {
                                smStateNames[idx] = state.state.name;
                                idx++;
                            }

                            lever.smStateIdx = EditorGUILayout.Popup("Animation State", lever.smStateIdx, smStateNames);
                            lever.smState = smStateNames[lever.smStateIdx];
                            GUILayout.Space(3f);
                        }
                    }

                    // -------------------------------------------------------- Gear Indicator
                    if (lever.leverType == HelicopterLever.LeverType.GearIndicator)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(leverObj, new GUIContent("Lever Handle"));
                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                        GUI.color = backgroundColor;

                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("direction"), new GUIContent("Rotation Direction"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumDeflection"), new GUIContent("Maximum Deflection"));


                        GUILayout.Space(10f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Bulb Configuration", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("bulbMaterial"), new GUIContent("Bulb Material"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumBulbEmission"), new GUIContent("Maximum Emission"));
                    }
                }
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
    #endregion

// ---------------------------------------------------- Dial
#region Dial
#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(HelicopterDial))]
    public class HelicopterDialEditor : Editor
    {
        Color backgroundColor;
        Color silantroColor = Color.yellow; // new Color(1, 0.4f, 0);
        HelicopterDial dial;
        SerializedProperty animLayer1, animLayer2;

        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        private void OnEnable() 
        { 
            dial = (HelicopterDial)target;
            animLayer1 = serializedObject.FindProperty("animLayer1");
            animLayer2 = serializedObject.FindProperty("animLayer2");
        }


        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;
            //DrawDefaultInspector ();
            serializedObject.Update();

            // ------------------------------------- by RF {
            //GUILayout.Space(10f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("m_type"), new GUIContent("System Type"));
            //GUILayout.Space(3f);
            // ------------------------------------- by RF {

            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Dial Type", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dialType"), new GUIContent("Dial Type"));
            //GUILayout.Space(3f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationMode"), new GUIContent("Rotation Mode"));
            //GUILayout.Space(3f);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("needleType"), new GUIContent("needle Type"));

            if (dial.dialType == HelicopterDial.DialType.RPM)
            {
                GUILayout.Space(3f);
                GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Engine Type", MessageType.None);
                GUI.color = backgroundColor;

                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("engineType"), new GUIContent("Engine Type"));
                GUILayout.Space(3f);

                if (dial.engineType == HelicopterDial.EngineType.PistonEngine)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("piston"), new GUIContent("Piston Engine"));
                else if (dial.engineType == HelicopterDial.EngineType.TurboShaft)
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("turboshaft"), new GUIContent("Turboshaft Engine"));
            }

            GUILayout.Space(3f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Dial Configuration", MessageType.None);
            GUI.color = backgroundColor;

            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("movementType"), new GUIContent("Animation Type"));
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("forceSimulationMode"), new GUIContent("Force Simulation Mode"));

            if (dial.movementType == HelicopterDial.MovementType.Transform)
            {
                // ****************************** TRANSFORM ANIMATION INSPECTOR *********************************

                if (dial.dialType == HelicopterDial.DialType.ArtificialHorizon)
                //else if (dial.dialType == PhantomDial.DialType.BankLevel)
                {
                    GUILayout.Space(10f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("attitudeIndicatorType"), new GUIContent("Attitude Indicator Type"));
                    GUILayout.Space(2f);

                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Deflection Configuration", MessageType.None);
                    GUI.color = backgroundColor;

                    if (dial.attitudeIndicatorType == HelicopterDial.AttitudeIndicatorType.Classic)
                    {
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("pitchRotationAxis"), new GUIContent("Pitch Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("pitchDirection"), new GUIContent("Pitch Rotation Direction"));

                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("yawRotationAxis"), new GUIContent("Yaw Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("yawDirection"), new GUIContent("Yaw Rotation Direction"));

                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rollRotationAxis"), new GUIContent("Roll Rotation Axis"));
                        GUILayout.Space(2f);
                        GUILayout.Space(2f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rollDirection"), new GUIContent("Roll Rotation Direction"));
                    }

                    if (dial.attitudeIndicatorType == HelicopterDial.AttitudeIndicatorType.GlassInstrument)
                    {
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rollRotationAxis"), new GUIContent("Roll Rotation Axis"));
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("rollDirection"), new GUIContent("Roll Rotation Direction"));
                        GUILayout.Space(5f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("oneDegreeDistance"), new GUIContent("One Degree Pitch Distance"));
                    }

                    GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Connections", MessageType.None);
                    GUI.color = backgroundColor;

                    if (dial.attitudeIndicatorType == HelicopterDial.AttitudeIndicatorType.GlassInstrument)
                    {
                        GUILayout.Space(5f);
                        dial.supportNeedle = EditorGUILayout.ObjectField("Horizon Rotation Anchor", dial.supportNeedle, typeof(Transform), true) as Transform;
                    }
                    GUILayout.Space(5f);
                    dial.needle = EditorGUILayout.ObjectField("Horizon Indicator", dial.needle, typeof(Transform), true) as Transform;
                }                // -------------------------------- Air/Horizontal Speed
                else if (dial.dialType == HelicopterDial.DialType.Speed)
                {
                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Speedometer Type", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("speedUnit"), new GUIContent("Units"));

                    /*GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Readout Digits Settings", MessageType.None);
                    GUI.color = backgroundColor;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("useReadout"), new GUIContent("Use Readout Digits"));
                    GUILayout.Space(5f);

                    if (dial.useReadout)
                    {
                        EditorGUILayout.HelpBox("Dial 'Per Digit' Translation", MessageType.None); GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneTranslation"), new GUIContent("Digit One"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoTranslation"), new GUIContent("Digit Two"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeTranslation"), new GUIContent("Digit Three"));

                        GUILayout.Space(10f);
                        //GUI.color = silantroColor;
                        EditorGUILayout.HelpBox("Digit Containers", MessageType.None); GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneContainer"), new GUIContent("Digit One Container"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoContainer"), new GUIContent("Digit Two Container"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeContainer"), new GUIContent("Digit Three Container"));

                        GUILayout.Space(10f);
                        //GUI.color = silantroColor;
                        EditorGUILayout.HelpBox("Digit Display", MessageType.None); GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.LabelField("Current Value", dial.digitFour.ToString("0") + "| " + dial.digitThree.ToString("0") + "| " + dial.digitTwo.ToString("0") + "| " + dial.digitOne.ToString("0"));
                    }

                    GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Movement Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    // Get movement type
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("movementType"), new GUIContent("Movement Type"));
                    */
                }

                // -------------------------------- Climb/Vertical Speed
                if (dial.dialType == HelicopterDial.DialType.VerticalSpeed)
                {
                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Speedometer Type", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("vertSpeedUnit"), new GUIContent("Units"));

                    /*GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Movement Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    // Get movement type
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("movementType"), new GUIContent("Movement Type"));
                    */
                }

                // -------------------------------- Altitude
                if (dial.dialType == HelicopterDial.DialType.Altitude)
                {
                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Altimeter Type", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("altitudeUnit"), new GUIContent("Units"));

                    /*GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Readout Digits Settings", MessageType.None);
                    GUI.color = backgroundColor;
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("useReadout"), new GUIContent("Use Readout Digits"));
                    GUILayout.Space(5f);

                    if (dial.useReadout)
                    {
                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Altimeter Readout Type", MessageType.None);
                        GUI.color = backgroundColor; GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("altimeterType"), new GUIContent(" "));
                        EditorGUILayout.HelpBox("Dial 'Per Digit' Translation", MessageType.None); GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneTranslation"), new GUIContent("Digit One"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoTranslation"), new GUIContent("Digit Two"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeTranslation"), new GUIContent("Digit Three"));

                        if (dial.altimeterType == HelicopterDial.AltimeterType.FourDigit)
                        {
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourTranslation"), new GUIContent("Digit Four"));
                        }

                        GUILayout.Space(10f);
                        //GUI.color = silantroColor;
                        EditorGUILayout.HelpBox("Digit Containers", MessageType.None); GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneContainer"), new GUIContent("Digit One Container"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoContainer"), new GUIContent("Digit Two Container"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeContainer"), new GUIContent("Digit Three Container"));

                        if (dial.altimeterType == HelicopterDial.AltimeterType.FourDigit)
                        {
                            GUILayout.Space(3f);
                            EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourContainer"), new GUIContent("Digit Four Container"));
                        }

                        GUILayout.Space(10f);
                        //GUI.color = silantroColor;
                        EditorGUILayout.HelpBox("Digit Display", MessageType.None); GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        GUI.color = Color.white;
                        EditorGUILayout.LabelField("Current Value", dial.digitFour.ToString("0") + "| " + dial.digitThree.ToString("0") + "| " + dial.digitTwo.ToString("0") + "| " + dial.digitOne.ToString("0"));
                    }*/
                }

                // -------------------------------- Radar Altitude
                if (dial.dialType == HelicopterDial.DialType.RadarAltitude)
                {
                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Indicator Position Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("minimumPosition"), new GUIContent("Minimum Position"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumPosition"), new GUIContent("Maximum Position"));
                    GUILayout.Space(5f);
                }

                // -------------------------------- Fuel
                if (dial.dialType == HelicopterDial.DialType.Fuel)
                {
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Fuel Unit", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("fuelUnit"), new GUIContent(" "));

                    /*GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Movement Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    // Get movement type
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("movementType"), new GUIContent("Movement Type"));
                    */

                    //if (dial.movementType == HelicopterDial.MovementType.Transform)
                    //{
                    GUILayout.Space(10f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Indicator Position Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("minimumPosition"), new GUIContent("Minimum Position"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumPosition"), new GUIContent("Maximum Position"));
                    GUILayout.Space(5f);
                    //}

                }

                /*if (dial.dialType == HelicopterDial.DialType.Stabilator)
                {
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Connected Stabilator", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("stabilator"), new GUIContent(" "));
                }*/


                // ------------------------------- RPM
                if (dial.dialType == HelicopterDial.DialType.RPM || dial.dialType == HelicopterDial.DialType.EGT)
                {
                    /*GUILayout.Space(3f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Engine Type", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("engineType"), new GUIContent(" "));
                    GUILayout.Space(5f);

                    if (dial.engineType == HelicopterDial.EngineType.ElectricMotor)
                    {
                        //EditorGUILayout.PropertyField(serializedObject.FindProperty("engineType"), new GUIContent(" "));
                    }
                    if (dial.engineType == HelicopterDial.EngineType.TurboShaft)
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("shaft"), new GUIContent(" "));
                    else if (dial.engineType == HelicopterDial.EngineType.PistonEngine)
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("piston"), new GUIContent(" "));

                    GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Movement Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    // Get movement type
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("movementType"), new GUIContent("Movement Type"));
                    */
                }

                // --------------------------------------------------------------------------------------------------------------------
                if (dial.dialType != HelicopterDial.DialType.ArtificialHorizon) // other than Artificial Horizon Dial
                {
                    GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Transform Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);

                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Rotation Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("rotationAxis"), new GUIContent("Rotation Axis"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("direction"), new GUIContent("Rotation Direction"));

                    if (dial.needleType == HelicopterDial.NeedleType.Dual)
                    {
                        GUILayout.Space(5f);
                        GUI.color = Color.white;
                        EditorGUILayout.HelpBox("Support needle", MessageType.None);
                        GUI.color = backgroundColor;
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("supportRotationAxis"), new GUIContent("Rotation Axis"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("supportDirection"), new GUIContent("Rotation Direction"));
                    }


                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Rotation Amounts", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("MinimumAngleDegrees"), new GUIContent("Minimum Angle"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("MaximumAngleDegrees"), new GUIContent("Maximum Angle"));


                    GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Data Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("minimumValue"), new GUIContent("Minimum Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("maximumValue"), new GUIContent("Maximum Value"));
                    GUILayout.Space(3f);
                    EditorGUILayout.LabelField("Current Amount", dial.currentValue.ToString("0.00"));


                    GUILayout.Space(15f);
                    GUI.color = silantroColor;
                    EditorGUILayout.HelpBox("Connections", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);


                    if (dial.needleType == HelicopterDial.NeedleType.Dual)
                    {
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("needle"), new GUIContent("Main needle"));
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("supportNeedle"), new GUIContent("Support needle"));
                    }
                    else
                    {
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("needle"), new GUIContent("needle"));
                    }
                    
                }
            }
            else if (dial.movementType == HelicopterDial.MovementType.Animator)
            {
                GUILayout.Space(15f);
                GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Animator Configuration", MessageType.None);
                GUI.color = backgroundColor;
                GUILayout.Space(3f);

                // Get Animator Controller
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("animator"), new GUIContent("Animator Component"));
                //GUILayout.Space(3f);
                //EditorGUILayout.PropertyField(serializedObject.FindProperty("animatorPlayMode"), new GUIContent("Animator Play Mode"));

                if (dial.animator != null)
                {
                    // Get animator parameters
                    AnimatorControllerParameter[] animParams = (dial.animator.runtimeAnimatorController as AnimatorController).parameters;
                    string[] animParamNames = new string[animParams.Length];
                    var idx = 0;
                    foreach (AnimatorControllerParameter param in animParams)
                    {
                        animParamNames[idx] = param.name;
                        idx++;
                    }

                    // Get animator layers
                    AnimatorControllerLayer[] animLayers = (dial.animator.runtimeAnimatorController as AnimatorController).layers;
                    string[] animLayerNames = new string[animLayers.Length];
                    idx = 0;
                    foreach (AnimatorControllerLayer layer in animLayers)
                    {
                        animLayerNames[idx] = layer.name;
                        idx++;
                    }

                    // -------------------------------------------------------- Speed
                    if (dial.dialType == HelicopterDial.DialType.Speed)
                    {
                        AnimatorConfigInspector("Speed", ref dial.animLayer1, ref animLayer1, "Speed", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Vertical Speed
                    else if (dial.dialType == HelicopterDial.DialType.VerticalSpeed)
                    {
                        AnimatorConfigInspector("Vertical Speed", ref dial.animLayer1, ref animLayer1, "Vertical Speed", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Turn
                    if (dial.dialType == HelicopterDial.DialType.BankLevel)
                    {
                        AnimatorConfigInspector("Turn", ref dial.animLayer1, ref animLayer1, "Turn", "Turn X", "Turn Y", animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Compass
                    if (dial.dialType == HelicopterDial.DialType.Compass)
                    {
                        AnimatorConfigInspector("Heading", ref dial.animLayer1, ref animLayer1, "Heading", "Heading X", "Heading Y", animParamNames, animLayerNames, animLayers);

                    }

                    // -------------------------------------------------------- Altitude
                    if (dial.dialType == HelicopterDial.DialType.Altitude)
                    {
                        AnimatorConfigInspector("Altitude", ref dial.animLayer1, ref animLayer1, "Altitude", "Altitude X", "Altitude Y", animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Radar Altitude
                    if (dial.dialType == HelicopterDial.DialType.RadarAltitude)
                    {
                        AnimatorConfigInspector("Radar Altitude", ref dial.animLayer1, ref animLayer1, "Radar Altitude", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Horizon
                    if (dial.dialType == HelicopterDial.DialType.ArtificialHorizon)
                    {
                        GUILayout.Space(3f);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("attitudeIndicatorType"), new GUIContent("Attitude Indicator Type"));
                        GUILayout.Space(2f);

                        if (dial.attitudeIndicatorType == HelicopterDial.AttitudeIndicatorType.Classic)
                        {
                            AnimatorConfigInspector("Horizon Pitch", ref dial.animLayer1, ref animLayer1, "Horizon Pitch", "Horizon Pitch X", "Horizon Pitch Y", animParamNames, animLayerNames, animLayers);
                            AnimatorConfigInspector("Horizon Roll", ref dial.animLayer2, ref animLayer2, "Horizon Roll", "Horizon Roll X", "Horizon Roll Y", animParamNames, animLayerNames, animLayers);
                        }
                        else if (dial.attitudeIndicatorType == HelicopterDial.AttitudeIndicatorType.GlassInstrument)
                        {
                            AnimatorConfigInspector("Horizon Pitch", ref dial.animLayer1, ref animLayer1, "Horizon Pitch", null, null, animParamNames, animLayerNames, animLayers);
                            AnimatorConfigInspector("Horizon Roll", ref dial.animLayer2, ref animLayer2, "Horizon Roll", "Horizon Roll X", "Horizon Roll Y", animParamNames, animLayerNames, animLayers);
                        }
                    }

                    // -------------------------------------------------------- EGT, Throttle
                    if (dial.dialType == HelicopterDial.DialType.EGT)
                    {
                        AnimatorConfigInspector("Throttle", ref dial.animLayer1, ref animLayer1, "Throttle", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- Fuel
                    if (dial.dialType == HelicopterDial.DialType.Fuel)
                    {
                        AnimatorConfigInspector("Fuel", ref dial.animLayer1, ref animLayer1, "Fuel", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    // -------------------------------------------------------- RPM
                    if (dial.dialType == HelicopterDial.DialType.RPM)
                    {
                        AnimatorConfigInspector("RPM", ref dial.animLayer1, ref animLayer1, "RPM", null, null, animParamNames, animLayerNames, animLayers);
                    }

                    /*GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Animator Controller Configuration", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(3f);

                    if (animParamNames.Length > 0)
                    {
                        dial.animParamIdx1 = EditorGUILayout.Popup("Animation Parameter", dial.animParamIdx1, animParamNames);
                        dial.animParam1 = animParamNames[dial.animParamIdx1];
                    }
                    else
                    {
                        EditorGUILayout.LabelField("Animator Parameter", "Not Found!");
                    }
                    GUILayout.Space(3f);

                    if (animLayerNames.Length > 0)
                    {
                        dial.animLayerIdx = EditorGUILayout.Popup("Animation Layer", dial.animLayerIdx, animLayerNames);
                        dial.animLayer = animLayerNames[dial.animLayerIdx];

                        // Get animation states
                        ChildAnimatorState[] smStates = animLayers[dial.animLayerIdx].stateMachine.states;

                        if (smStates.Length > 0)
                        {
                            string[] smStateNames = new string[smStates.Length];
                            idx = 0;
                            foreach (ChildAnimatorState state in smStates)
                            {
                                smStateNames[idx] = state.state.name;
                                idx++;
                            }

                            dial.smStateIdx = EditorGUILayout.Popup("Animation State", dial.smStateIdx, smStateNames);
                            dial.smState = smStateNames[dial.smStateIdx];
                        }
                        else
                        {
                            EditorGUILayout.LabelField("Animator State", "Not Found!");
                        }
                    }
                    else
                    {
                        EditorGUILayout.LabelField("Animator Layer", "Not Found!");
                    }
                    GUILayout.Space(3f);
                    */
                }
            }

            // ****************************** READOUT DIGITS INSPECTOR *********************************
            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Readout Digits Settings", MessageType.None);
            GUI.color = backgroundColor;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("useReadout"), new GUIContent("Use Readout Digits"));
            GUILayout.Space(5f);

            if (dial.useReadout)
            {
                GUILayout.Space(5f);
                EditorGUILayout.HelpBox("Digit Value", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitSmallestValue"), new GUIContent("Digit Smallest Value"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneModder"), new GUIContent("Digit One Level"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoModder"), new GUIContent("Digit Two Level"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeModder"), new GUIContent("Digit Three Level"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourModder"), new GUIContent("Digit Four Level"));

                GUILayout.Space(5f);
                EditorGUILayout.HelpBox("Dial 'Per Digit' Translation", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneTranslation"), new GUIContent("Digit One Translation"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoTranslation"), new GUIContent("Digit Two Translation"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeTranslation"), new GUIContent("Digit Three Translation"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourTranslation"), new GUIContent("Digit Four Translation"));

                GUILayout.Space(10f);
                //GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Digit Containers", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitOneContainer"), new GUIContent("Digit One Container"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitTwoContainer"), new GUIContent("Digit Two Container"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitThreeContainer"), new GUIContent("Digit Three Container"));
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("digitFourContainer"), new GUIContent("Digit Four Container"));

                GUILayout.Space(10f);
                //GUI.color = silantroColor;
                EditorGUILayout.HelpBox("Digit Display", MessageType.None); GUI.color = backgroundColor;
                GUILayout.Space(3f);
                GUI.color = Color.white;
                EditorGUILayout.LabelField("Current Value", dial.digitFour.ToString("0") + "| " + dial.digitThree.ToString("0") + "| " + dial.digitTwo.ToString("0") + "| " + dial.digitOne.ToString("0"));
            }

            // ****************************** READOUT TEXT INSPECTOR *********************************
            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Readout Texts Settings", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("readoutTexts"), new GUIContent("Readout Texts"));
            if (dial.readoutTexts != null && dial.readoutTexts.Length > 0)
            {
                GUILayout.Space(3f);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("readoutTextFormat"), new GUIContent("Readout Text Format"));
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void AnimatorConfigInspector(string layerName, ref HelicopterDial.AnimLayer animLayer, ref SerializedProperty animLayerSO, string param1name, string param2name, string param3name, string[] animParamNames, string[] animLayerNames, AnimatorControllerLayer[] animLayers)
        {
            //SilantroDial.AnimLayer layer = new SilantroDial.AnimLayer();

            GUILayout.Space(10f);
            GUI.color = silantroColor;
            EditorGUILayout.HelpBox(layerName + " Animation Controller Configuration", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);

            if (animParamNames.Length == 0)
                EditorGUILayout.LabelField("Animator Parameter", "Not Found!");
            else
            {
                if (param1name != null)
                {
                    animLayer.paramIdx1 = EditorGUILayout.Popup(param1name + " Animation Parameter", animLayer.paramIdx1, animParamNames);
                    animLayer.param1 = animParamNames[animLayer.paramIdx1];
                    GUILayout.Space(3f);
                }

                if (param2name != null)
                {

                    animLayer.paramIdx2 = EditorGUILayout.Popup(param2name + " Animation Parameter", animLayer.paramIdx2, animParamNames);
                    animLayer.param2 = animParamNames[animLayer.paramIdx2];
                    GUILayout.Space(3f);
                }

                if (param3name != null)
                {

                    animLayer.paramIdx3 = EditorGUILayout.Popup(param3name + " Animation Parameter", animLayer.paramIdx3, animParamNames);
                    animLayer.param3 = animParamNames[animLayer.paramIdx3];
                    GUILayout.Space(3f);
                }
            }

            if (animParamNames.Length == 0)
                EditorGUILayout.LabelField("Animator Layer", "Not Found!");
            else
            {
                animLayer.layerIdx = EditorGUILayout.Popup("Animation Layer", animLayer.layerIdx, animLayerNames);
                animLayer.layerName = animLayerNames[animLayer.layerIdx];
                GUILayout.Space(3f);

                // Get animation states
                ChildAnimatorState[] smStates = animLayers[animLayer.layerIdx].stateMachine.states;

                if (smStates.Length == 0)
                    EditorGUILayout.LabelField("Animator State", "Not Found!");
                else
                {
                    string[] smStateNames = new string[smStates.Length];
                    var idx = 0;
                    foreach (ChildAnimatorState state in smStates)
                    {
                        smStateNames[idx] = state.state.name;
                        idx++;
                    }

                    animLayer.smStateIdx = EditorGUILayout.Popup("Animation State", animLayer.smStateIdx, smStateNames);
                    animLayer.smState = smStateNames[animLayer.smStateIdx];
                    GUILayout.Space(3f);
                }
            }
        }
    }
#endif
#endregion

    // --------------------------------------------- Camera
#region Camera
#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor(typeof(HelicopterCamera))]
    public class HelicopterCameraEditor : Editor
    {
        Color backgroundColor;
        Color silantroColor = Color.yellow;
        HelicopterCamera camera;
        public int toolbarTab; public string currentTab;

        private void OnEnable() { camera = (HelicopterCamera)target; }

        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;
            //DrawDefaultInspector ();
            serializedObject.Update();

            GUI.color = silantroColor;
            EditorGUILayout.HelpBox("Camera Settings", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(3f);
            toolbarTab = GUILayout.Toolbar(toolbarTab, new string[] { "Exterior Camera", "Interior Camera" });
            switch (toolbarTab)
            {
                case 0: currentTab = "Exterior Camera"; break;
                case 1: currentTab = "Interior Camera"; break;
            }

            switch (currentTab)
            {
                case "Exterior Camera":
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("normalExterior"), new GUIContent("Exterior Camera"));
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Movement", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    serializedObject.FindProperty("elevationSensitivity").floatValue = EditorGUILayout.Slider("Elevation Sensitivity", serializedObject.FindProperty("elevationSensitivity").floatValue, 0f, 5f);
                    GUILayout.Space(3f);
                    serializedObject.FindProperty("azimuthSensitivity").floatValue = EditorGUILayout.Slider("Azimuth Sensitivity", serializedObject.FindProperty("azimuthSensitivity").floatValue, 0f, 5f);
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Positioning", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    camera.maximumRadius = EditorGUILayout.FloatField("Camera Distance", camera.maximumRadius);
                    GUILayout.Space(3f);
                    camera.followCameraHeight = EditorGUILayout.FloatField("Follow Camera Height", camera.followCameraHeight);
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraFocus"), new GUIContent("Camera Focus"));
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraTarget"), new GUIContent("Camera Target"));
                    break;

                case "Interior Camera":
                    GUILayout.Space(3f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("normalInterior"), new GUIContent("Interior Camera"));
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Movement", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    serializedObject.FindProperty("mouseSensitivity").floatValue = EditorGUILayout.Slider("Mouse Sensitivity", serializedObject.FindProperty("mouseSensitivity").floatValue, 0f, 100f);
                    GUILayout.Space(5f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Camera Positioning", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(2f);
                    serializedObject.FindProperty("clampAngle").floatValue = EditorGUILayout.Slider("View Angle", serializedObject.FindProperty("clampAngle").floatValue, 10f, 210f);


                    GUILayout.Space(10f);
                    GUI.color = Color.white;
                    EditorGUILayout.HelpBox("Zoom Settings", MessageType.None);
                    GUI.color = backgroundColor;
                    GUILayout.Space(5f);
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("interiorZoomEnabled"));
                    if (camera.interiorZoomEnabled)
                    {
                        GUILayout.Space(3f);
                        serializedObject.FindProperty("maximumFOV").floatValue = EditorGUILayout.Slider("Maximum FOV", serializedObject.FindProperty("maximumFOV").floatValue, 0f, 100f);
                        GUILayout.Space(3f);
                        serializedObject.FindProperty("interiorZoomSensitivity").floatValue = EditorGUILayout.Slider("Zoom Sensitivity", serializedObject.FindProperty("zoomSensitivity").floatValue, 0f, 20f);
                        GUILayout.Space(3f);
                        EditorGUILayout.LabelField("Current FOV", camera.currentFOV.ToString("0.00"));
                    }
                    break;
            }
            serializedObject.ApplyModifiedProperties();
        }

    }
#endif
#endregion
}

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif
using UnityEngine;
using Oyedoyin.Old;

//using Oyedoyin.Rotary.LowFidelity; // by RF

namespace HelicopterSim
{

    /// <summary>
    ///
    /// 
    /// Use:		 Handles the movement/rotation of control levers and handles
    /// </summary>



    public class HelicopterLever : MonoBehaviour
    {

        // ------------------------------------- Selectibles
        public enum LeverType { Stick, Collective, Pedal, GearIndicator, Throttle }
        public LeverType leverType = LeverType.Stick;
        public enum StickType { Joystick, Yoke }
        public StickType stickType = StickType.Joystick;
        public enum PedalType { Sliding, Hinged }
        public PedalType pedalType = PedalType.Hinged;
        public enum PedalMode { Individual, Combined }
        public PedalMode pedalMode = PedalMode.Combined;
        public enum MovementType { Transform, Animator }; // by RF
        public MovementType movementType = MovementType.Transform; // by RF

        public enum RotationDirection { CW, CCW }
        public RotationDirection direction = RotationDirection.CW;
        public RotationDirection pitchDirection = RotationDirection.CW;
        public RotationDirection rollDirection = RotationDirection.CW;
        public RotationDirection rightDirection = RotationDirection.CW;
        public RotationDirection leftDirection = RotationDirection.CCW;

        public enum RotationAxis { X, Y, Z }
        public RotationAxis rotationAxis = RotationAxis.X;
        public RotationAxis rudderRotationAxis = RotationAxis.X;
        public RotationAxis rollRotationAxis = RotationAxis.X;
        public RotationAxis pitchRotationAxis = RotationAxis.X;
        public RotationAxis rightRotationAxis = RotationAxis.X;
        public RotationAxis leftRotationAxis = RotationAxis.X;

        // ------------------------------------- by RF {
        //public enum ModelType { Realistic, Arcade }
        //public ModelType m_type = ModelType.Realistic;
        // ------------------------------------- by RF }

        // ------------------------------------- Connections
        //public HelicopterController controller;
        public Transform lever, yoke;
        public Material bulbMaterial;
        public float maximumBulbEmission = 10f;
        private Color baseColor, finalColor;
        public Transform leftPedal, rightPedal, leftPedalTube, rightPedalTube;

        public HelicopterController m_controller; // by RF
        public Animator animFlightControl;
        public int animParamIdx1, animParamIdx2, animLayerIdx, smStateIdx;
        public string animParam1, animParam2, animLayer, smState;
        public float animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax;
        public float animParam2InputMin, animParam2InputMax, animParam2OutputMin, animParam2OutputMax;
        public bool animParam1SyncValue, animParam2SyncValue;
        public bool animParam1InvertDirection, animParam2InvertDirection;

        // ------------------------------------- Vectors
        Vector3 axisRotation;
        Vector3 pitchAxisRotation;
        Vector3 rollAxisRotation;
        Vector3 rightAxisRotation, leftAxisRotation;
        Vector3 initialRightPosition, initialLeftPosition;


        // ------------------------------------- Quaternions
        Quaternion initialYokeRotation;
        Quaternion InitialRotation = Quaternion.identity;
        Quaternion initialRightRotation;
        Quaternion initialLeftRotation;



        // ------------------------------------- Variables
        public float maximumDeflection;
        public float MaximumPitchDeflection;
        public float MaximumRollDeflection;
        public float rudderInput;
        public float maximumPedalDeflection = 20f;
        public float maximumSlidingDistance = 10f;
        public float currentPedalDeflection;
        public float currentDistance;
        public float collectiveAmount, throttleAmount;
        public float currentGearRotation;



        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        public void InitializeLever()
        {
            if (movementType == MovementType.Transform)
            {
                // ---------------------------------------- Setup Rotation Variables
                axisRotation = Handler.EstimateModelProperties(direction.ToString(), rotationAxis.ToString());
                if (lever != null) { InitialRotation = lever.localRotation; }
                if (stickType == StickType.Yoke && yoke != null) { initialYokeRotation = yoke.localRotation; }

                pitchAxisRotation = Handler.EstimateModelProperties(pitchDirection.ToString(), pitchRotationAxis.ToString());
                rollAxisRotation = Handler.EstimateModelProperties(rollDirection.ToString(), rollRotationAxis.ToString());

                if (leverType == LeverType.Pedal)
                {
                    initialLeftRotation = leftPedal.localRotation; initialRightRotation = rightPedal.localRotation;
                    initialLeftPosition = leftPedal.localPosition; initialRightPosition = rightPedal.localPosition;
                    rightAxisRotation = Handler.EstimateModelProperties(rightDirection.ToString(), rightRotationAxis.ToString());
                    leftAxisRotation = Handler.EstimateModelProperties(leftDirection.ToString(), leftRotationAxis.ToString());
                }
            }
            else if (movementType == MovementType.Animator)
            {
                //Debug.Log("flight control: " + flightControl.ToString());
                animFlightControl.Play(animLayer + "." + smState);
            }
        }


        // ----------------------------------------------------------------------------------------------------------------------------------------------------------
        void Update()
        {

            if (m_controller != null)
            {
                // ---------------------------------------- Control Stick
                if (leverType == LeverType.Stick)
                {
                    if (movementType == MovementType.Transform)
                    {
                        float pitch = m_controller.processedPitch * MaximumPitchDeflection;
                        float roll = m_controller.processedRoll * MaximumRollDeflection;
                        var pitchEffect = Quaternion.AngleAxis(pitch, pitchAxisRotation);
                        var rollEffect = Quaternion.AngleAxis(roll, rollAxisRotation);

                        // ----------------- Apply
                        if (stickType == StickType.Joystick) 
                        {
                            var angleEffect = rollEffect * pitchEffect; 
                            lever.localRotation = InitialRotation * angleEffect; 
                        }
                        else 
                        { 
                            lever.localRotation = InitialRotation * pitchEffect; 
                            yoke.localRotation = initialYokeRotation * rollEffect; 
                        }
                    }
                    else if (movementType == MovementType.Animator)
                    {
                        if (animFlightControl != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animFlightControl.SetFloat(animParam1, m_controller.processedPitch * -1);
                                else
                                    animFlightControl.SetFloat(animParam1, m_controller.processedPitch);
                            }
                            else
                            {
                                animFlightControl.SetFloat(animParam1, CalculateParameterValue(m_controller.processedPitch, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                            }

                            if (animParam2SyncValue)
                            {
                                if (animParam2InvertDirection)
                                    animFlightControl.SetFloat(animParam2, m_controller.processedRoll * -1);
                                else
                                    animFlightControl.SetFloat(animParam2, m_controller.processedRoll);
                            }
                            else
                            {
                                animFlightControl.SetFloat(animParam2, CalculateParameterValue(m_controller.processedRoll, animParam2InputMin, animParam2InputMax, animParam1OutputMin, animParam2OutputMax, animParam2InvertDirection));
                            }
                        }
                    }
                }

                // ---------------------------------------- Throttle
                if (leverType == LeverType.Throttle)
                {
                    if (movementType == MovementType.Transform)
                    {
                        throttleAmount = m_controller.processedThrottle;
                        throttleAmount = Mathf.Clamp(throttleAmount, 0, 1.0f);
                        throttleAmount *= maximumDeflection;
                        lever.localRotation = InitialRotation;
                        lever.Rotate(axisRotation, throttleAmount);                        
                    }
                    else if (movementType == MovementType.Animator)
                    {
                        if (animFlightControl != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animFlightControl.SetFloat(animParam1, m_controller.processedThrottle * -1);
                                else
                                    animFlightControl.SetFloat(animParam1, m_controller.processedThrottle);
                            }
                            else
                            {
                                animFlightControl.SetFloat(animParam1, CalculateParameterValue(m_controller.processedThrottle, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                            }
                        }
                    }
                }

                // ---------------------------------------- Collective
                if (leverType == LeverType.Collective)
                {
                    if (movementType == MovementType.Transform)
                    {
                        collectiveAmount = m_controller.processedCollective;
                        collectiveAmount = Mathf.Clamp(collectiveAmount, 0, 1.0f);
                        collectiveAmount *= maximumDeflection; lever.localRotation = InitialRotation;
                        lever.Rotate(axisRotation, collectiveAmount);                        
                    }
                    else if (movementType == MovementType.Animator)
                    {
                        if (animFlightControl != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animFlightControl.SetFloat(animParam1, m_controller.processedCollective * -1);
                                else
                                    animFlightControl.SetFloat(animParam1, m_controller.processedCollective);
                            }
                            else
                            {
                                //animFlightControl.SetFloat(animParam1, 1 - m_controller.processedCollective);
                                animFlightControl.SetFloat(animParam1, CalculateParameterValue(m_controller.processedCollective, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                            }
                        }
                    }
                }

                // ---------------------------------------- Gear
                if (leverType == LeverType.GearIndicator)
                {
                    /*
                    if (m_controller.gearState == PhantomController.GearState.Open) { currentGearRotation = Mathf.Lerp(currentGearRotation, 0, Time.deltaTime * 2f); }
                    else { currentGearRotation = Mathf.Lerp(currentGearRotation, maximumDeflection, Time.deltaTime * 2f); }

                    if (lever != null)
                    { lever.transform.localRotation = InitialRotation; lever.transform.Rotate(axisRotation, currentGearRotation); }

                    if (bulbMaterial != null)
                    {
                        if (m_controller.gearState == PhantomController.GearState.Open) { bulbMaterial.color = Color.green; baseColor = Color.green; }
                        else { bulbMaterial.color = Color.red; baseColor = Color.red; }
                        // ------------------- Set
                        finalColor = baseColor * Mathf.LinearToGammaSpace(maximumBulbEmission);
                        if (bulbMaterial != null) { bulbMaterial.SetColor("_EmissionColor", finalColor); }
                    }
                    */
                }

                // ---------------------------------------- Pedals
                if (leverType == LeverType.Pedal)
                {
                    if (movementType == MovementType.Transform)
                    {
                        rudderInput = m_controller.processedYaw;

                        // ---------------------- ROTATE
                        if (pedalType == PedalType.Hinged)
                        {
                            currentPedalDeflection = rudderInput * maximumDeflection;

                            // ---------------------- ROTATE PEDALS
                            rightPedal.localRotation = initialRightRotation;
                            rightPedal.Rotate(rightAxisRotation, currentPedalDeflection);
                            if (rightPedalTube != null)
                            {
                                rightPedalTube.localRotation = initialRightRotation;
                                rightPedalTube.Rotate(rightAxisRotation, -currentPedalDeflection);
                            }

                            if (pedalMode == PedalMode.Combined)
                            {
                                leftPedal.localRotation = initialLeftRotation;
                                leftPedal.Rotate(leftAxisRotation, currentPedalDeflection);
                                if (leftPedalTube != null)
                                {
                                    leftPedalTube.localRotation = initialLeftRotation;
                                    leftPedalTube.Rotate(leftAxisRotation, -currentPedalDeflection);
                                }
                            }
                            else
                            {
                                leftPedal.localRotation = initialLeftRotation;
                                leftPedal.Rotate(leftAxisRotation, -currentPedalDeflection);
                                if (leftPedalTube != null)
                                {
                                    leftPedalTube.localRotation = initialLeftRotation;
                                    leftPedalTube.Rotate(leftAxisRotation, currentPedalDeflection);
                                }
                            }
                        }

                        // ---------------------- MOVE
                        if (pedalType == PedalType.Sliding)
                        {
                            currentDistance = rudderInput * (maximumSlidingDistance / 100);
                            //MOVE PEDALS
                            rightPedal.localPosition = initialRightPosition;
                            rightPedal.localPosition += rightAxisRotation * currentDistance;
                            leftPedal.localPosition = initialLeftPosition;
                            leftPedal.localPosition += leftAxisRotation * currentDistance;
                        }                        
                    }
                    else if (movementType == MovementType.Animator)
                    {
                        if (animFlightControl != null)
                        {
                            if (animParam1SyncValue)
                            {
                                if (animParam1InvertDirection)
                                    animFlightControl.SetFloat(animParam1, m_controller.processedYaw * -1);
                                else
                                    animFlightControl.SetFloat(animParam1, m_controller.processedYaw);
                            }
                            else
                            {
                                //animFlightControl.SetFloat(animParam1, 1 - (m_controller.processedYaw + 1) / 2);
                                animFlightControl.SetFloat(animParam1, CalculateParameterValue(m_controller.processedYaw, animParam1InputMin, animParam1InputMax, animParam1OutputMin, animParam1OutputMax, animParam1InvertDirection));
                            }
                        }
                    }
                }
            }

        }

        private float CalculateParameterValue(float input, float inputMin, float inputMax, float outputMin, float outputMax, bool invert)
        {
            float output = 0f;

            // check range
            if ((inputMin > inputMax) || (outputMin > outputMax))
            {
                Debug.LogError("Input range and/or animation parameter range is invalid. (min > max).");
                return 0;
            }

            // calculate multiplier
            float multiplier = (outputMax - outputMin) / (inputMax - inputMin);

            // project input value range to animation parameter value range
            if (invert)
            {
                output = outputMax - (input + (outputMin - inputMin)) * multiplier;
            }
            else
            {
                output = (input + (outputMin - inputMin)) * multiplier;
            }
            return output;
        }
    }
}
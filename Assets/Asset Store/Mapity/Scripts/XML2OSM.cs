﻿using UnityEngine;
using System;
using System.IO;
using System.Threading;

public class XML2OSM: OSMParser
{
    /// <summary>
    /// Parse a Node from the XML
    /// </summary>
    /// <param name="strXML">String of XML data</param>
    /// <param name="strPosition"> Ref to position in the string</param>
    protected override void ParseNode(string strXML, ref int strPosition)
    {
        // Skip spaces until first char
        SkipSpaces(strXML, ref strPosition);

        ++strPosition;// Skip '<'

        // Skip spaces until first node name
        SkipSpaces(strXML, ref strPosition);
                
        // Tag Nodes
        if (IsNode(strXML, ref strPosition, "t"))
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);

            // k
            string strKey = (string)ParseAttributeValue(strXML, ref strPosition, "k");

            // v
            string strValue = (string)ParseAttributeValue(strXML, ref strPosition, "v");

            if (OnParseTagAction != null)
            {
                OnParseTagAction(strKey, strValue);
            }
        }
        // Way Node Refs
        else if (IsNode(strXML, ref strPosition, "nd"))
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);
            
            // ref
            uint uRef = (uint)ParseAttributeValue(strXML, ref strPosition, "r", ValueHint.Uint);

            if (OnParseWayNodeRefAction != null)
            {
                OnParseWayNodeRefAction( uRef );
            }
        }
        // Parse Nodes
        else if (IsNode(strXML, ref strPosition, "nod"))
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);            

            // id
            uint iId = (uint)ParseAttributeValue(strXML, ref strPosition, "i", ValueHint.Uint);

            // lat
            float fLat = (float)ParseAttributeValue(strXML, ref strPosition, "la", ValueHint.Float);

            // lon
            float fLon = (float)ParseAttributeValue(strXML, ref strPosition, "lo", ValueHint.Float);

            if (OnParseNodeAction != null)
            {
                OnParseNodeAction( iId, fLat, fLon );
            }
        }
        // Member Nodes
        else if (IsNode(strXML, ref strPosition, "mem"))
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);

            // type
            string strType = (string)ParseAttributeValue(strXML, ref strPosition, "t");

            // ref
            uint uRef = (uint)ParseAttributeValue(strXML, ref strPosition, "r", ValueHint.Uint);

            if (OnParseMemberAction != null)
            {
                OnParseMemberAction(strType, uRef);
            }
        }
        // Way Nodes
        else if (IsNode(strXML, ref strPosition, "w"))
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);
            
            // id
            uint uId = (uint)ParseAttributeValue(strXML, ref strPosition, "i", ValueHint.Uint);

            if (OnParseWayAction != null)
            {
                OnParseWayAction(uId);
            }
        }
        // Relation Nodes
        else if (IsNode(strXML, ref strPosition, "r"))
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);
            
            // id
            uint uId = (uint)ParseAttributeValue(strXML, ref strPosition, "i", ValueHint.Uint);

            if (OnParseRelationAction != null)
            {
                OnParseRelationAction(uId);
            }
        }
        // Bound Nodes
        else if( IsNode( strXML, ref strPosition, "b") )
        {
            FindFirstSpaceAfterNode(strXML, ref strPosition);

            // minlat
            float fMinlat = (float)ParseAttributeValue(strXML, ref strPosition, "m", ValueHint.Float);

            // minlon
            float fMinlon = (float)ParseAttributeValue(strXML, ref strPosition, "m", ValueHint.Float);

            // maxlat
            float fMaxlat = (float)ParseAttributeValue(strXML, ref strPosition, "m", ValueHint.Float);

            // maxlon
            float fMaxlon = (float)ParseAttributeValue(strXML, ref strPosition, "m", ValueHint.Float);

            if (OnParseBoundAction != null)
            {
                OnParseBoundAction(fMinlat, fMinlon, fMaxlat, fMaxlon);
            }
        }
    }
    
}

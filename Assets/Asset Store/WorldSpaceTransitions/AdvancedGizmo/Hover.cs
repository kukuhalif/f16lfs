﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


namespace AdvancedGizmo
{
    public class Hover : MonoBehaviour
    {
        [SerializeField] Collider[] disableOtherColliders;

        public Color hovercolor;
        private Color original;
        private Renderer rend;
        private bool selected;
        private float t = 0;

        private EventSystem ES;

        void Start()
        {
            ES = EventSystem.current;
            rend = transform.GetComponent<Renderer>();
            original = rend.material.color;
            if (rend.material.HasProperty("_BaseColor")) original = rend.material.GetColor("_BaseColor");

            SetOriginal();
        }

        void OnMouseEnter()
        {

            if (ES) if (EventSystem.current.IsPointerOverGameObject()) return;
            SetHovered();
        }

        void OnMouseExit()
        {
            if (ES) if (EventSystem.current.IsPointerOverGameObject()) return;
            if (!selected)
                SetOriginal();
        }

        void SetHovered()
        {
            if (rend == null)
                rend = transform.GetComponent<Renderer>();

            rend.material.color = hovercolor;
            if (rend.material.HasProperty("_BaseColor")) rend.material.SetColor("_BaseColor", hovercolor);

            for (int i = 0; i < disableOtherColliders.Length; i++)
            {
                disableOtherColliders[i].enabled = false;
            }
        }

        void SetOriginal()
        {
            if (rend == null)
                rend = transform.GetComponent<Renderer>();

            rend.material.color = original;
            if (rend.material.HasProperty("_BaseColor")) rend.material.SetColor("_BaseColor", original);

            for (int i = 0; i < disableOtherColliders.Length; i++)
            {
                disableOtherColliders[i].enabled = true;
            }
        }

        void OnMouseDown()
        {
            if (ES) if (EventSystem.current.IsPointerOverGameObject()) return;
            selected = true;
            if (Time.time - t < 0.3f)
            {
                SendMessageUpwards("ChangeMode");
                SetOriginal();
            }
            t = Time.time;
        }

        void Update()
        {
            if (selected && Input.GetMouseButtonUp(0))
            {
                SetOriginal();
                selected = false;
            }
        }

    }
}

﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class SelectedToGuid : EditorWindow
{
    [MenuItem("AssetDatabase/ShowGUID")]

    static void Init()
    {
        if (Selection.objects.Length == 0) return;
        SelectedToGuid window = ScriptableObject.CreateInstance<SelectedToGuid>();
        window.position = new Rect(Screen.width / 2, Screen.height / 2, 320, 82);
        window.ShowPopup();
    }

    void OnGUI()
    {
        //string[] sel = Selection.assetGUIDs;
        EditorGUILayout.LabelField(Selection.objects[0].name, EditorStyles.wordWrappedLabel);
        EditorGUILayout.TextField(Selection.assetGUIDs[0], EditorStyles.wordWrappedLabel);
        GUILayout.Space(10);
        if (GUILayout.Button("O.K.")) this.Close();
    }

    [MenuItem("Tools/List Script Types")]
    static void LSS()
    {
        Object[] objects = FindObjectsOfType(typeof(MonoBehaviour));
        List<string> typenames = new List<string>();
        foreach (Object obj in objects)
        {
            string typename = obj.GetType().Name;
            if (!typenames.Contains(typename))
            {
                Debug.Log(typename);
                typenames.Add(typename);
            }
        }
    }

    [MenuItem("Tools/List Scripts")]
    static void LS()
    {
        Object[] objects = FindObjectsOfType(typeof(MonoBehaviour));
        foreach (Object obj in objects)
        {
            Debug.Log(obj.GetType().Name + " is attached to " + obj.name);
        }
    }

}

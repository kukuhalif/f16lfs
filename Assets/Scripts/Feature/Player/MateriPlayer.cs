﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class MateriPlayer : MonoBehaviour
    {
        private MateriTreeElement currentMateri;
        private int figureIndex;
        private int cameraIndex;
        private List<Figure> figures = new List<Figure>();

        private void Start()
        {
            EventManager.AddListener<MateriEvent>(Play);
            EventManager.AddListener<NextFigureEvent>(NextFigure);
            EventManager.AddListener<PrevFigureEvent>(PreviousFigure);
            EventManager.AddListener<FreePlayEvent>(FreePlay);
            EventManager.AddListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.AddListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.AddListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MateriEvent>(Play);
            EventManager.RemoveListener<NextFigureEvent>(NextFigure);
            EventManager.RemoveListener<PrevFigureEvent>(PreviousFigure);
            EventManager.RemoveListener<FreePlayEvent>(FreePlay);
            EventManager.RemoveListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.RemoveListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.RemoveListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void Play(MateriEvent e)
        {
            cameraIndex = 0;

            if (e.materiTreeElement.MateriData.figures.Count == 0)
            {
                Debug.LogError("figure list empty");
                return;
            }

            currentMateri = e.materiTreeElement;

            // play first figure
            figureIndex = 0;
            figures = currentMateri.MateriData.figures;
            ResetMateriAction();

            PlayMateri(0);
        }

        private void FreePlay(FreePlayEvent e)
        {
            ResetMateri(e.toLandingPage);
        }

        private void PlayFigureLinkListener(PlayFigureLinkEvent e)
        {
            if (e.type != MateriElementType.Materi)
                return;

            MateriTreeElement materiTreeElement = e.contentData as MateriTreeElement;

            if (materiTreeElement.MateriData.figures.Count == 0)
            {
                Debug.LogError("figure list empty");
                return;
            }

            ResetMateriAction();

            cameraIndex = 0;
            currentMateri = materiTreeElement;
            figures = currentMateri.MateriData.figures;

            for (int i = 0; i < figures.Count; i++)
            {
                if (e.name == figures[i].name)
                {
                    figureIndex = i;
                    break;
                }
            }

            PlayMateri(0);
        }

        private void PlayCameraFigureFromLinkListener(PlayCameraFigureLinkEvent e)
        {
            if (e.type != MateriElementType.Materi)
                return;

            MateriTreeElement materiTreeElement = e.contentData as MateriTreeElement;

            if (materiTreeElement.MateriData.figures.Count == 0)
            {
                Debug.LogError("figure list empty");
                return;
            }

            ResetMateriAction();

            currentMateri = materiTreeElement;
            figures = currentMateri.MateriData.figures;
            cameraIndex = 0;

            for (int i = 0; i < figures.Count; i++)
            {
                if (e.figureName == figures[i].name)
                {
                    figureIndex = i;

                    for (int c = 0; c < figures[i].cameraDestinations.Count; c++)
                    {
                        if (e.cameraName == figures[i].cameraDestinations[c].displayName)
                        {
                            cameraIndex = c;

                            break;
                        }
                    }

                    break;
                }
            }

            PlayMateri(cameraIndex);
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(figures[figureIndex], false);

            EventManager.TriggerEvent(new DisableEnvironmentEvent(currentMateri.MateriData.disableEnvironment));
            EventManager.TriggerEvent(new SetSchematicDataEvent(currentMateri.MateriData.schematics));
        }

        private void PlayMateri(int cameraIndex)
        {
            EventManager.TriggerEvent(new DescriptionPanelEvent(currentMateri, figureIndex, cameraIndex));

            if (figures[figureIndex].cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(figures[figureIndex].cameraDestinations[cameraIndex].destination, OnPlayMateriArriveAction);
            else
                OnPlayMateriArriveAction();

            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.Materi));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, figures[figureIndex].cameraDestinations, MateriElementType.Materi));
        }

        private void PlayFigure(Figure figure, bool moveCamera)
        {
            cameraIndex = 0;

            EventManager.TriggerEvent(new MonitorCameraEvent(figures[figureIndex].monitorCameras, MateriElementType.Materi));
            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.Materi));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, figures[figureIndex].cameraDestinations, MateriElementType.Materi));

            if (moveCamera && figure.cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(figure.cameraDestinations[cameraIndex].destination, OnArriveFigureCamera);
            else
                OnArriveFigureCamera();
        }

        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(figures[figureIndex].cutaway));
            EventManager.TriggerEvent(new PartObjectEvent(true, figures[figureIndex].partObjects, SetupVirtualButton, InstantateVirtualButton, SetupAnimation, TriggerCallout, InstanteAnimation));
        }

        private void TriggerCallout()
        {
            EventManager.TriggerEvent(new HelperEvent(figures[figureIndex].callouts, figures[figureIndex].helpers));
        }

        private void ResetFigureAction()
        {
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new AnimationPlayEvent());
        }

        private void ResetMateriAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MaintenanceUIEvent());
            EventManager.TriggerEvent(new FlightScenarioUIEvent());
            EventManager.TriggerEvent(new SetSchematicDataEvent());
            EventManager.TriggerEvent(new ShowPdfEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            ResetAnimationDatas();
        }

        private void FreePlay()
        {
            EventManager.TriggerEvent(new ResetPartObjectEvent());
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MaintenanceUIEvent());
            EventManager.TriggerEvent(new FlightScenarioUIEvent());
            EventManager.TriggerEvent(new DisableEnvironmentEvent());
            EventManager.TriggerEvent(new SetSchematicDataEvent());
            EventManager.TriggerEvent(new ShowPdfEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new AnimationPlayEvent());
        }

        private void ResetMateri(bool toLandingPage)
        {
            if (toLandingPage)
            {
                var landingPage = DatabaseManager.GetCameraData().landingPageCamera;
                VirtualTrainingCamera.MoveCamera(landingPage, FreePlay);
            }
            else
                VirtualTrainingCamera.ResetCameraPosition(FreePlay);
        }

        private void PlayFigureListener(PlayFigureEvent e)
        {
            if (e.type != MateriElementType.Materi)
                return;

            if (figures.Count < 1)
                return;

            ResetFigureAction();

            cameraIndex = 0;
            figureIndex = e.index;

            PlayFigure(figures[e.index], true);
        }

        private void NextFigure(NextFigureEvent e)
        {
            if (e.type != MateriElementType.Materi)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex++;

            if (figureIndex >= figures.Count)
                figureIndex = 0;

            ResetFigureAction();

            PlayFigure(figures[figureIndex], true);
        }

        private void PreviousFigure(PrevFigureEvent e)
        {
            if (e.type != MateriElementType.Materi)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex--;

            if (figureIndex < 0)
                figureIndex = figures.Count - 1;

            ResetFigureAction();

            PlayFigure(figures[figureIndex], true);
        }

        void SetupVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            for (int i = 0; i < figures[figureIndex].virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(figures[figureIndex].virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), figures[figureIndex].virtualButtonElement[i].overrideVirtualButton));
            }
        }
        void InstantateVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void ResetAnimationDatas()
        {
            if (figures.Count <= 0 || figures[figureIndex].animationFigureDatas.Count == 0)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent());
                return;
            }

            for (int i = 0; i < figures[figureIndex].animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(figures[figureIndex].animationFigureDatas[i], true));

                if (i == figures[figureIndex].animationFigureDatas.Count - 1)
                    EventManager.TriggerEvent(new AnimationPlayEvent());
            }
        }

        void SetupAnimation()
        {
            AnimationManager.isSequenceAnimation = figures[figureIndex].isSequenceAnimation;
            for (int i = 0; i < figures[figureIndex].animationFigureDatas.Count; i++)
            {
                figures[figureIndex].animationFigureDatas[i].isShowAnimationUI = currentMateri.MateriData.figures[figureIndex].isShowUiAnimation;
                figures[figureIndex].animationFigureDatas[i].isReverseAnimationOnReset = currentMateri.MateriData.figures[figureIndex].isReverseAnimationOnReset;
                figures[figureIndex].animationFigureDatas[i].isSequential = currentMateri.MateriData.figures[figureIndex].isSequenceAnimation;
                EventManager.TriggerEvent(new AnimationPlayEvent(figures[figureIndex].animationFigureDatas[i]));
            }
        }

        void InstanteAnimation()
        {
            EventManager.TriggerEvent(new AnimationSequenceDone());
        }
    }
}

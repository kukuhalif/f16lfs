﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class TroubleshootPlayer : MonoBehaviour
    {
        private TroubleshootDataModel currentTroubleshoot;
        private List<TroubleshootFigure> figures = new List<TroubleshootFigure>();

        int figureIndex;
        int cameraIndex;

        void Start()
        {
            EventManager.AddListener<TroubleshootPlayEvent>(Play);
            EventManager.AddListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.AddListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.AddListener<NextFigureEvent>(NextFigure);
            EventManager.AddListener<PrevFigureEvent>(PreviousFigure);
            EventManager.AddListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<TroubleshootPlayEvent>(Play);
            EventManager.RemoveListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.RemoveListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.RemoveListener<NextFigureEvent>(NextFigure);
            EventManager.RemoveListener<PrevFigureEvent>(PreviousFigure);
            EventManager.RemoveListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void Play(TroubleshootPlayEvent e)
        {
            if (e.troubleshootNode == null || e.troubleshootNode.content == null || e.troubleshootNode.content.troubleshootData == null)
            {
                ResetTroubleshoot();
                return;
            }

            figureIndex = 0;
            cameraIndex = 0;
            currentTroubleshoot = e.troubleshootNode.content.troubleshootData;
            figures = currentTroubleshoot.figures;
            ResetMateriAction();
            PlayTroubleshoot(figureIndex, cameraIndex);
        }

        private void PlayFigureLinkListener(PlayFigureLinkEvent e)
        {
            if (e.type != MateriElementType.Troubleshoot)
                return;

            TroubleshootDataModel troubleshoot = e.contentData as TroubleshootDataModel;

            if (troubleshoot == null || troubleshoot.figures.Count == 0)
            {
                ResetTroubleshoot();
                return;
            }

            currentTroubleshoot = troubleshoot;
            figures = currentTroubleshoot.figures;
            figureIndex = 0;
            ResetMateriAction();

            for (int i = 0; i < troubleshoot.figures.Count; i++)
            {
                if (e.name == troubleshoot.figures[i].name)
                {
                    figureIndex = i;

                    break;
                }
            }

            PlayTroubleshoot(figureIndex, 0);
        }

        private void PlayCameraFigureFromLinkListener(PlayCameraFigureLinkEvent e)
        {
            if (e.type != MateriElementType.Troubleshoot)
                return;

            TroubleshootDataModel troubleshoot = e.contentData as TroubleshootDataModel;

            if (troubleshoot == null || troubleshoot.figures.Count == 0)
            {
                ResetTroubleshoot();
                return;
            }


            currentTroubleshoot = troubleshoot;
            figures = currentTroubleshoot.figures;
            figureIndex = 0;
            cameraIndex = 0;

            ResetMateriAction();

            for (int i = 0; i < troubleshoot.figures.Count; i++)
            {
                if (e.figureName == troubleshoot.figures[i].name)
                {
                    figureIndex = i;

                    for (int c = 0; c < troubleshoot.figures[i].cameraDestinations.Count; c++)
                    {
                        if (e.cameraName == troubleshoot.figures[i].cameraDestinations[c].displayName)
                        {
                            cameraIndex = c;

                            break;
                        }
                    }

                    break;
                }
            }

            PlayTroubleshoot(figureIndex, cameraIndex);
        }

        void ResetTroubleshoot()
        {
            VirtualTrainingCamera.ResetCameraPosition(TotalResetMateriAction);
        }

        private void PlayFigureListener(PlayFigureEvent e)
        {
            if (e.type != MateriElementType.Troubleshoot)
                return;

            if (figures.Count < 1)
                return;

            ResetMateriAction();

            cameraIndex = 0;
            figureIndex = e.index;

            PlayFigure(figures[figureIndex], true, 0);
        }

        private void NextFigure(NextFigureEvent e)
        {
            if (e.type != MateriElementType.Troubleshoot)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex++;

            if (figureIndex >= figures.Count)
                figureIndex = 0;

            ResetMateriAction();
            cameraIndex = 0;
            PlayFigure(figures[figureIndex], true, 0);
        }

        private void PreviousFigure(PrevFigureEvent e)
        {
            if (e.type != MateriElementType.Troubleshoot)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex--;

            if (figureIndex < 0)
                figureIndex = figures.Count - 1;

            ResetMateriAction();
            cameraIndex = 0;
            PlayFigure(figures[figureIndex], true, 0);
        }

        void PlayTroubleshoot(int figureIndex, int cameraIndex)
        {
            this.figureIndex = figureIndex;
            this.cameraIndex = cameraIndex;

            if (currentTroubleshoot.figures.Count == 0)
            {
                Debug.Log("empty figure");
            }
            else
            {
                if (currentTroubleshoot.figures[figureIndex].cameraDestinations.Count > 0)
                    VirtualTrainingCamera.MoveCamera(currentTroubleshoot.figures[figureIndex].cameraDestinations[cameraIndex].destination, OnPlayMateriArriveAction);
                else
                    OnPlayMateriArriveAction();
            }

            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.Troubleshoot));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, currentTroubleshoot.figures[figureIndex].cameraDestinations, MateriElementType.Troubleshoot));
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(currentTroubleshoot.figures[figureIndex], false, 0);
        }

        private void PlayFigure(Figure figure, bool moveCamera, int cameraIndex)
        {
            EventManager.TriggerEvent(new MonitorCameraEvent(currentTroubleshoot.figures[figureIndex].monitorCameras, MateriElementType.Troubleshoot));
            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.Troubleshoot));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, currentTroubleshoot.figures[figureIndex].cameraDestinations, MateriElementType.Troubleshoot));

            if (moveCamera && figure.cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(figure.cameraDestinations[cameraIndex].destination, OnArriveFigureCamera);
            else
                OnArriveFigureCamera();
        }

        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(currentTroubleshoot.figures[figureIndex].cutaway));
            EventManager.TriggerEvent(new DisableEnvironmentEvent(currentTroubleshoot.disableEnvironment));
            EventManager.TriggerEvent(new PartObjectEvent(true, currentTroubleshoot.figures[figureIndex].partObjects, SetupVirtualButton, InstantiateVirtualButton, SetupAnimation, TriggerCallout, InstantiateAnimation));
        }

        private void TriggerCallout()
        {
            EventManager.TriggerEvent(new HelperEvent(currentTroubleshoot.figures[figureIndex].callouts, currentTroubleshoot.figures[figureIndex].helpers));
        }

        private void ResetMateriAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            ResetAnimationDatas();
        }

        private void TotalResetMateriAction()
        {
            EventManager.TriggerEvent(new ResetPartObjectEvent());
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new DisableEnvironmentEvent());
            ResetAnimationDatas();
        }

        void SetupVirtualButton()
        {
            for (int i = 0; i < currentTroubleshoot.figures[figureIndex].virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(currentTroubleshoot.figures[figureIndex].virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), currentTroubleshoot.figures[figureIndex].virtualButtonElement[i].overrideVirtualButton));
            }
        }

        void InstantiateVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void SetupAnimation()
        {
            AnimationManager.isSequenceAnimation = currentTroubleshoot.figures[figureIndex].isSequenceAnimation;
            for (int i = 0; i < currentTroubleshoot.figures[figureIndex].animationFigureDatas.Count; i++)
            {
                currentTroubleshoot.figures[figureIndex].animationFigureDatas[i].isShowAnimationUI = currentTroubleshoot.figures[figureIndex].isShowUiAnimation;
                currentTroubleshoot.figures[figureIndex].animationFigureDatas[i].isReverseAnimationOnReset = currentTroubleshoot.figures[figureIndex].isReverseAnimationOnReset;
                currentTroubleshoot.figures[figureIndex].animationFigureDatas[i].isSequential = currentTroubleshoot.figures[figureIndex].isSequenceAnimation;
                EventManager.TriggerEvent(new AnimationPlayEvent(currentTroubleshoot.figures[figureIndex].animationFigureDatas[i]));
            }
        }

        void InstantiateAnimation()
        {
            EventManager.TriggerEvent(new AnimationSequenceDone());
        }

        void ResetAnimationDatas()
        {
            if (figures.Count <= 0 || figures[figureIndex].animationFigureDatas.Count == 0)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent());
                return;
            }

            for (int i = 0; i < figures[figureIndex].animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(figures[figureIndex].animationFigureDatas[i], true));

                if (i == figures[figureIndex].animationFigureDatas.Count - 1)
                    EventManager.TriggerEvent(new AnimationPlayEvent());
            }
        }
    }
}

using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using System.Linq;
using System.Collections;
//using HelicopterSim;
using Oyedoyin.Common;
using System.Collections.Generic;

namespace VirtualTraining.Feature
{
    [RequireComponent(typeof(PlayableDirector))]
    public class FlightScenarioPlayer : MonoBehaviour
    {
        [ReadOnly] public PlayableDirector playableDirector;
        PlayableDirector animationPlayer;
        [ReadOnly] public Controller aircraftController;

        FlightScenarioDataModel currentFlightScenario;
        private List<FlightScenarioFigure> figures = new List<FlightScenarioFigure>();


        int figureIndex;
        int cameraIndex;

        void Start()
        {
            FlightSimRecordingPlayer flightSimRecordingPlayer = FindObjectOfType<FlightSimRecordingPlayer>(true);
            animationPlayer = flightSimRecordingPlayer.GetComponent<PlayableDirector>();
            playableDirector = GetComponent<PlayableDirector>();
            aircraftController = FindObjectOfType<Controller>();

            EventManager.AddListener<FlightScenarioPlayEvent>(Play);
            EventManager.AddListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.AddListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.AddListener<NextFigureEvent>(NextFigure);
            EventManager.AddListener<PrevFigureEvent>(PreviousFigure);
            EventManager.AddListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<FlightScenarioPlayEvent>(Play);
            EventManager.RemoveListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.RemoveListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.RemoveListener<NextFigureEvent>(NextFigure);
            EventManager.RemoveListener<PrevFigureEvent>(PreviousFigure);
            EventManager.RemoveListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void Play(FlightScenarioPlayEvent e)
        {
            if (e.flightScenarioTreeElement == null || e.flightScenarioTreeElement.data == null || e.flightScenarioTreeElement.data.figures.Count == 0)
            {
                ResetFlightScenario();
                return;
            }

            figureIndex = 0;
            cameraIndex = 0;
            currentFlightScenario = e.flightScenarioTreeElement.data;
            figures = currentFlightScenario.figures;

            ResetFlightScenarioAction();
            currentFlightScenario = e.flightScenarioTreeElement.data;

            // check flight scenario timeline elmt type
            if (currentFlightScenario.flightScenarioTimelineElmtType != null)
            {
                FlightScenarioTimelineDataModel flightScenarioTimelineData = currentFlightScenario.flightScenarioTimelineElmtType.GetData();
                if (flightScenarioTimelineData != null)
                {
                    // change flight recordedData
                    SetAnimationPlayer(flightScenarioTimelineData.recordedFlightData);

                    // change scenario timeline
                    SetScenarioPlayer(flightScenarioTimelineData.scenarioTimeline);

                    // jump to specific timeline marker and play the recording animation segment
                    PlayScenarioStep(flightScenarioTimelineData.scenarioTimeline, currentFlightScenario.markerNum);

                    PlayFlightScenario(0, 0);
                }
            }

        }

        void SetAnimationPlayer(FlightSimRecordingData flightScenarioData)
        {
            // set playableDirector to loaded timelineAsset
            if (animationPlayer != null)
                animationPlayer.playableAsset = flightScenarioData.timeline;

            // loop on timeline root tracks
            for (int trackIdx = 0; trackIdx < flightScenarioData.timeline.rootTrackCount; trackIdx++)
            {
                // get timeline track
                TrackAsset track = flightScenarioData.timeline.GetRootTrack(trackIdx);
                // check if timeline track type is animation track
                if (track.GetType() == typeof(AnimationTrack))
                {
                    // find gameobject by name
                    GameObject go = GameObject.Find(track.name);
                    if (go == null)
                    {
                        Debug.LogWarning("Track binding Gameobject is not found!");
                    }
                    else
                    {
                        // make sure gameobject is active
                        go.SetActive(true);

                        // get gameobject animator component
                        Animator animator = go.GetComponent<Animator>();
                        if (animator == null)
                        {
                            Debug.LogWarning("Animator component is not found on gameobject!");
                        }
                        // make sure animator component is active
                        animator.enabled = true;
                        // bind gameobject with animator component
                        animationPlayer.SetGenericBinding(track, go);
                    }
                }
            }
        }

        void SetScenarioPlayer(TimelineAsset scenarioTimeline)
        {
            if (playableDirector != null)
            {
                // enable Scenario Player gameobject and component
                playableDirector.gameObject.SetActive(true);
                playableDirector.enabled = true;

                // set scenario playableDirector to loaded scenario timelineAsset
                playableDirector.playableAsset = scenarioTimeline;

                // loop on scenario timeline root tracks
                //int scenarioTrackIdx = 0;
                for (int trackIdx = 0; trackIdx < scenarioTimeline.rootTrackCount; trackIdx++)
                {
                    // get timeline track
                    TrackAsset track = scenarioTimeline.GetRootTrack(trackIdx);

                    // check if timeline track type is signal track
                    if (track.GetType() == typeof(SignalTrack))
                    {
                        // bind signal track to scenarioPlayer gameobject
                        GameObject go = gameObject;
                        playableDirector.SetGenericBinding(track, go);
                    }
                    else if (track.GetType() == typeof(ControlTrack))
                    {
                        // assign exposed reference
                    }
                }

                // play scenario
                //scenarioPlayer.playableDirector.Play();
                // paused scenario at start
                //playableDirector.Pause();
                //scenarioPlayer.Pause();
            }
        }

        void PlayScenarioStep(TimelineAsset scenarioTimeline, int markerNum)
        {
            var markers = scenarioTimeline.markerTrack.GetMarkers().ToArray();

            playableDirector.Pause();
            playableDirector.time = markers[markerNum].time;
            playableDirector.Play();
            aircraftController.Pause(false);
#if !UNITY_EDITOR
            playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1);
#endif
        }

        private void PlayFigureLinkListener(PlayFigureLinkEvent e)
        {
            if (e.type != MateriElementType.FlightScenario)
                return;

            FlightScenarioDataModel flightScenario = e.contentData as FlightScenarioDataModel;

            if (flightScenario == null || flightScenario.figures.Count == 0)
            {
                ResetFlightScenario();
                return;
            }

            currentFlightScenario = flightScenario;
            figures = currentFlightScenario.figures;
            figureIndex = 0;

            for (int i = 0; i < flightScenario.figures.Count; i++)
            {
                if (e.name == flightScenario.figures[i].name)
                {
                    figureIndex = i;

                    break;
                }
            }

            ResetFlightScenarioAction();
            PlayFlightScenario(figureIndex, 0);
        }

        private void PlayCameraFigureFromLinkListener(PlayCameraFigureLinkEvent e)
        {
            if (e.type != MateriElementType.FlightScenario)
                return;

            FlightScenarioDataModel flightScenario = e.contentData as FlightScenarioDataModel;

            if (flightScenario == null || flightScenario.figures.Count == 0)
            {
                ResetFlightScenario();
                return;
            }

            ResetFlightScenarioAction();
            currentFlightScenario = flightScenario;
            figures = currentFlightScenario.figures;
            figureIndex = 0;
            cameraIndex = 0;

            for (int i = 0; i < flightScenario.figures.Count; i++)
            {
                if (e.figureName == flightScenario.figures[i].name)
                {
                    figureIndex = i;

                    for (int c = 0; c < flightScenario.figures[i].cameraDestinations.Count; c++)
                    {
                        if (e.cameraName == flightScenario.figures[i].cameraDestinations[c].displayName)
                        {
                            cameraIndex = c;

                            break;
                        }
                    }

                    break;
                }
            }

            PlayFlightScenario(figureIndex, cameraIndex);
        }

        void ResetFlightScenario()
        {
            VirtualTrainingCamera.ResetCameraPosition(TotalResetFlightScenarioAction);
        }

        void PlayFlightScenario(int figureIndex, int cameraIndex)
        {
            this.figureIndex = figureIndex;
            this.cameraIndex = cameraIndex;

            if (currentFlightScenario.figures.Count == 0)
            {
                Debug.Log("empty figure");
            }
            else
            {
                // jump to specific timeline marker
                //PlayScenarioStep(currentFlightScenario.markerNum);
                //VirtualTrainingCamera.MoveCamera(currentFlightScenario.figures[figureIndex].cameraDestinations[cameraIndex].destination, OnPlayMateriArriveAction);

                // move camera when timeline has been playing start from the marker, wait 1 frame for timeline to start playing
                StartCoroutine(PlayFlightScenarioCamera());

                EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.FlightScenario));
                EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, currentFlightScenario.figures[figureIndex].cameraDestinations, MateriElementType.FlightScenario));
            }
        }

        private IEnumerator PlayFlightScenarioCamera()
        {
            yield return null;
            if (currentFlightScenario.figures[figureIndex].cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(currentFlightScenario.figures[figureIndex].cameraDestinations[cameraIndex].destination, OnPlayMateriArriveAction);
            else
                OnPlayMateriArriveAction();
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(currentFlightScenario.figures[figureIndex], false, cameraIndex);
        }

        private void PlayFigure(Figure figure, bool moveCamera, int cameraIndex)
        {
            EventManager.TriggerEvent(new MonitorCameraEvent(currentFlightScenario.figures[figureIndex].monitorCameras, MateriElementType.FlightScenario));
            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.FlightScenario));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, currentFlightScenario.figures[figureIndex].cameraDestinations, MateriElementType.FlightScenario));

            Debug.Log("play figure flight scenario");
            if (moveCamera && figure.cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(figure.cameraDestinations[cameraIndex].destination, OnArriveFigureCamera);
            else
                OnArriveFigureCamera();
        }

        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(currentFlightScenario.figures[figureIndex].cutaway));
            //EventManager.TriggerEvent(new DisableEnvironmentEvent(currentFlightScenario.disableEnvironment));
            EventManager.TriggerEvent(new PartObjectEvent(true, currentFlightScenario.figures[figureIndex].partObjects, SetupVirtualButton, InstantiateVirtualButton, SetupAnimation, TriggerCallout, InstantiateAnimation));
            Debug.Log("onarrive flight scenario");
        }

        private void TriggerCallout()
        {
            EventManager.TriggerEvent(new HelperEvent(currentFlightScenario.figures[figureIndex].callouts, currentFlightScenario.figures[figureIndex].helpers));
        }

        private void ResetFlightScenarioAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            //EventManager.TriggerEvent(new AnimationPlayEvent());
            ResetAnimationDatas();
            Debug.Log("reset figure flight scenario");
        }

        private void TotalResetFlightScenarioAction()
        {
            EventManager.TriggerEvent(new ResetPartObjectEvent());
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new DisableEnvironmentEvent());
            //EventManager.TriggerEvent(new AnimationPlayEvent());
            ResetAnimationDatas();
            Debug.Log("total reset figure flight scenario");
        }

        void SetupVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            for (int i = 0; i < currentFlightScenario.figures[figureIndex].virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(currentFlightScenario.figures[figureIndex].virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), currentFlightScenario.figures[figureIndex].virtualButtonElement[i].overrideVirtualButton));
            }
        }

        void InstantiateVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void SetupAnimation()
        {
            AnimationManager.isSequenceAnimation = currentFlightScenario.figures[figureIndex].isSequenceAnimation;
            Debug.Log("setup animation");
            for (int i = 0; i < currentFlightScenario.figures[figureIndex].animationFigureDatas.Count; i++)
            {
                currentFlightScenario.figures[figureIndex].animationFigureDatas[i].isShowAnimationUI = currentFlightScenario.figures[figureIndex].isShowUiAnimation;
                currentFlightScenario.figures[figureIndex].animationFigureDatas[i].isReverseAnimationOnReset = currentFlightScenario.figures[figureIndex].isReverseAnimationOnReset;
                currentFlightScenario.figures[figureIndex].animationFigureDatas[i].isSequential = currentFlightScenario.figures[figureIndex].isSequenceAnimation;
                EventManager.TriggerEvent(new AnimationPlayEvent(currentFlightScenario.figures[figureIndex].animationFigureDatas[i]));
            }
        }

        void InstantiateAnimation()
        {
            EventManager.TriggerEvent(new AnimationSequenceDone());
        }

        private void PlayFigureListener(PlayFigureEvent e)
        {
            if (e.type != MateriElementType.FlightScenario)
                return;

            if (figures.Count < 1)
                return;

            ResetFlightScenarioAction();

            cameraIndex = 0;
            figureIndex = e.index;

            PlayFigure(figures[figureIndex], true, 0);
        }

        private void NextFigure(NextFigureEvent e)
        {
            if (e.type != MateriElementType.FlightScenario)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex++;

            if (figureIndex >= figures.Count)
                figureIndex = 0;

            ResetFlightScenarioAction();
            cameraIndex = 0;
            PlayFigure(figures[figureIndex], true, 0);
        }

        private void PreviousFigure(PrevFigureEvent e)
        {
            if (e.type != MateriElementType.FlightScenario)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex--;

            if (figureIndex < 0)
                figureIndex = figures.Count - 1;

            ResetFlightScenarioAction();
            cameraIndex = 0;
            PlayFigure(figures[figureIndex], true, 0);
        }

        void ResetAnimationDatas()
        {
            if (figures.Count <= 0 || figures[figureIndex].animationFigureDatas.Count == 0)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent());
                return;
            }

            for (int i = 0; i < figures[figureIndex].animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(figures[figureIndex].animationFigureDatas[i], true));

                if (i == figures[figureIndex].animationFigureDatas.Count - 1)
                    EventManager.TriggerEvent(new AnimationPlayEvent());
            }
        }

        public void Pause()
        {
            playableDirector.Pause();
            aircraftController.Pause(true);
#if !UNITY_EDITOR
            playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0);
#endif
        }

        public void Resume()
        {
            playableDirector.Resume();
            aircraftController.Pause(false);
#if !UNITY_EDITOR
            playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1);
#endif
        }

        public void StartEngine()
        {
            aircraftController.TurnOnEngines(false);
        }

        public void StartEngineFlying()
        {
            aircraftController.TurnOnEngines(true);
        }

        public void StopEngine()
        {
            aircraftController.TurnOffEngines();
        }

        public void ShowFlightMonitorPanel()
        {
            EventManager.TriggerEvent(new FlightMonitorCameraEvent());
        }

        public void CloseFlightMonitorPanel()
        {
            EventManager.TriggerEvent(new FlightMonitorCameraCloseEvent());
        }

    }
}
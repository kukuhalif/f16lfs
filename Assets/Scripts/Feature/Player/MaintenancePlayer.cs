﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class MaintenancePlayer : MonoBehaviour
    {
        private MaintenanceDataModel currentMaintenance;
        private List<MaintenanceFigure> figures = new List<MaintenanceFigure>();

        int figureIndex;
        int cameraIndex;

        void Start()
        {
            EventManager.AddListener<MaintenancePlayEvent>(Play);
            EventManager.AddListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.AddListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.AddListener<NextFigureEvent>(NextFigure);
            EventManager.AddListener<PrevFigureEvent>(PreviousFigure);
            EventManager.AddListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MaintenancePlayEvent>(Play);
            EventManager.RemoveListener<PlayFigureLinkEvent>(PlayFigureLinkListener);
            EventManager.RemoveListener<PlayCameraFigureLinkEvent>(PlayCameraFigureFromLinkListener);
            EventManager.RemoveListener<NextFigureEvent>(NextFigure);
            EventManager.RemoveListener<PrevFigureEvent>(PreviousFigure);
            EventManager.RemoveListener<PlayFigureEvent>(PlayFigureListener);
        }

        private void Play(MaintenancePlayEvent e)
        {
            if (e.MaintenanceTreeElement == null || e.MaintenanceTreeElement.data == null || e.MaintenanceTreeElement.data.figures.Count == 0)
            {
                ResetMaintenance();
                return;
            }

            figureIndex = 0;
            cameraIndex = 0;
            currentMaintenance = e.MaintenanceTreeElement.data;
            figures = currentMaintenance.figures;
            ResetMaintenanceAction();
            PlayMaintenance(figureIndex, cameraIndex);
        }

        private void PlayFigureLinkListener(PlayFigureLinkEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            MaintenanceDataModel maintenance = e.contentData as MaintenanceDataModel;

            if (maintenance == null || maintenance.figures.Count == 0)
            {
                ResetMaintenance();
                return;
            }

            currentMaintenance = maintenance;
            figures = currentMaintenance.figures;
            figureIndex = 0;

            for (int i = 0; i < maintenance.figures.Count; i++)
            {
                if (e.name == maintenance.figures[i].name)
                {
                    figureIndex = i;

                    break;
                }
            }

            ResetMaintenanceAction();
            PlayMaintenance(figureIndex, 0);
        }

        private void PlayCameraFigureFromLinkListener(PlayCameraFigureLinkEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            MaintenanceDataModel maintenance = e.contentData as MaintenanceDataModel;

            if (maintenance == null || maintenance.figures.Count == 0)
            {
                ResetMaintenance();
                return;
            }

            ResetMaintenanceAction();
            currentMaintenance = maintenance;
            figures = currentMaintenance.figures;
            figureIndex = 0;
            cameraIndex = 0;

            for (int i = 0; i < maintenance.figures.Count; i++)
            {
                if (e.figureName == maintenance.figures[i].name)
                {
                    figureIndex = i;

                    for (int c = 0; c < maintenance.figures[i].cameraDestinations.Count; c++)
                    {
                        if (e.cameraName == maintenance.figures[i].cameraDestinations[c].displayName)
                        {
                            cameraIndex = c;

                            break;
                        }
                    }

                    break;
                }
            }

            PlayMaintenance(figureIndex, cameraIndex);
        }

        void ResetMaintenance()
        {
            VirtualTrainingCamera.ResetCameraPosition(TotalResetMaintenanceAction);
        }

        void PlayMaintenance(int figureIndex, int cameraIndex)
        {
            this.figureIndex = figureIndex;
            this.cameraIndex = cameraIndex;

            if (currentMaintenance.figures.Count == 0)
            {
                Debug.Log("empty figure");
            }
            else
            {
                if (currentMaintenance.figures[figureIndex].cameraDestinations.Count > 0)
                    VirtualTrainingCamera.MoveCamera(currentMaintenance.figures[figureIndex].cameraDestinations[cameraIndex].destination, OnPlayMateriArriveAction);
                else
                    OnPlayMateriArriveAction();
            }

            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.Maintenance));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, currentMaintenance.figures[figureIndex].cameraDestinations, MateriElementType.Maintenance));
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(currentMaintenance.figures[figureIndex], false, cameraIndex);
        }

        private void PlayFigure(Figure figure, bool moveCamera, int cameraIndex)
        {
            EventManager.TriggerEvent(new MonitorCameraEvent(currentMaintenance.figures[figureIndex].monitorCameras, MateriElementType.Maintenance));
            EventManager.TriggerEvent(new UpdateFigurePanelEvent(figures[figureIndex].name, figureIndex, figures.Count, figures[figureIndex], MateriElementType.Maintenance));
            EventManager.TriggerEvent(new UpdateFigureCameraPanelEvent(cameraIndex, currentMaintenance.figures[figureIndex].cameraDestinations, MateriElementType.Maintenance));

            if (moveCamera && figure.cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(figure.cameraDestinations[cameraIndex].destination, OnArriveFigureCamera);
            else
                OnArriveFigureCamera();
        }

        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(currentMaintenance.figures[figureIndex].cutaway));
            EventManager.TriggerEvent(new DisableEnvironmentEvent(currentMaintenance.disableEnvironment));
            EventManager.TriggerEvent(new PartObjectEvent(true, currentMaintenance.figures[figureIndex].partObjects, SetupVirtualButton, InstantiateVirtualButton, SetupAnimation, TriggerCallout, InstantiateAnimation));
        }

        private void TriggerCallout()
        {
            EventManager.TriggerEvent(new HelperEvent(currentMaintenance.figures[figureIndex].callouts, currentMaintenance.figures[figureIndex].helpers));
        }

        private void ResetMaintenanceAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            ResetAnimationDatas();
        }

        private void TotalResetMaintenanceAction()
        {
            EventManager.TriggerEvent(new ResetPartObjectEvent());
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new DisableEnvironmentEvent());
            ResetAnimationDatas();
        }

        void SetupVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            for (int i = 0; i < currentMaintenance.figures[figureIndex].virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(currentMaintenance.figures[figureIndex].virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), currentMaintenance.figures[figureIndex].virtualButtonElement[i].overrideVirtualButton));
            }
        }

        void InstantiateVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void SetupAnimation()
        {
            AnimationManager.isSequenceAnimation = currentMaintenance.figures[figureIndex].isSequenceAnimation;
            for (int i = 0; i < currentMaintenance.figures[figureIndex].animationFigureDatas.Count; i++)
            {
                currentMaintenance.figures[figureIndex].animationFigureDatas[i].isShowAnimationUI = currentMaintenance.figures[figureIndex].isShowUiAnimation;
                currentMaintenance.figures[figureIndex].animationFigureDatas[i].isReverseAnimationOnReset = currentMaintenance.figures[figureIndex].isReverseAnimationOnReset;
                currentMaintenance.figures[figureIndex].animationFigureDatas[i].isSequential = currentMaintenance.figures[figureIndex].isSequenceAnimation;
                EventManager.TriggerEvent(new AnimationPlayEvent(currentMaintenance.figures[figureIndex].animationFigureDatas[i]));
            }
        }

        void InstantiateAnimation()
        {
            EventManager.TriggerEvent(new AnimationSequenceDone());
        }

        private void PlayFigureListener(PlayFigureEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            if (figures.Count < 1)
                return;

            ResetMaintenanceAction();

            cameraIndex = 0;
            figureIndex = e.index;

            PlayFigure(figures[figureIndex], true, 0);
        }

        private void NextFigure(NextFigureEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex++;

            if (figureIndex >= figures.Count)
                figureIndex = 0;

            ResetMaintenanceAction();
            cameraIndex = 0;
            PlayFigure(figures[figureIndex], true, 0);
        }

        private void PreviousFigure(PrevFigureEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            if (figures.Count <= 1)
                return;

            figureIndex--;

            if (figureIndex < 0)
                figureIndex = figures.Count - 1;

            ResetMaintenanceAction();
            cameraIndex = 0;
            PlayFigure(figures[figureIndex], true, 0);
        }

        void ResetAnimationDatas()
        {
            if (figures.Count <= 0 || figures[figureIndex].animationFigureDatas.Count == 0)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent());
                return;
            }

            for (int i = 0; i < figures[figureIndex].animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(figures[figureIndex].animationFigureDatas[i], true));

                if (i == figures[figureIndex].animationFigureDatas.Count - 1)
                    EventManager.TriggerEvent(new AnimationPlayEvent());
            }
        }
    }
}

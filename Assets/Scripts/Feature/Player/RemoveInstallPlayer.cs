using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    // todo : trigger content event (monitor camera, animation, dll)
    public class RemoveInstallPlayer : MonoBehaviour
    {
        private RemoveInstallTreeElement root;
        private RemoveInstallTreeElement currentElement;

        private List<GameObject> targets = new List<GameObject>();
        private bool isRemoving;

        private RemoveInstallFigure currentFigure;

        private int cameraIndex;

        private List<TreeElement> children = new List<TreeElement>();

        private void Start()
        {
            EventManager.AddListener<RemoveInstallPlayEvent>(PlayRemoveInstallListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<RemoveInstallPlayEvent>(PlayRemoveInstallListener);
        }

        private void PlayRemoveInstallListener(RemoveInstallPlayEvent e)
        {
            cameraIndex = 0;

            if (e.root == null || e.currentElement == null)
            {
                ResetRemoveInstall();
                return;
            }

            if ((root != e.root || e.root == e.currentElement) || this.isRemoving != e.isRemoving)
            {
                TotalResetRemoveInstall();
            }

            this.currentElement = e.currentElement;
            this.currentFigure = e.currentElement.data.figure;
            this.root = e.root;
            this.isRemoving = e.isRemoving;

            // move camera
            if (currentFigure.cameraDestinations.Count > 0)
                VirtualTrainingCamera.MoveCamera(currentFigure.cameraDestinations[cameraIndex].destination, EnableDisableTarget);
            else
                EnableDisableTarget();
        }

        private void EnableDisableTarget()
        {
            EventManager.TriggerEvent(new UpdateFigurePanelEvent(currentFigure.name, 0, 1, currentFigure, MateriElementType.RemoveInstall));

            // initialize
            children = root.AllChilds();
            foreach (var child in children)
            {
                RemoveInstallTreeElement rite = child as RemoveInstallTreeElement;
                foreach (var target in rite.data.figure.targets)
                {
                    if (isRemoving)
                        target.gameObject.SetActive(true);
                    else
                        target.gameObject.SetActive(false);

                    if (!targets.Contains(target.gameObject))
                        targets.Add(target.gameObject);
                }
            }

            // set target
            bool currentElementFound = false;
            for (int i = 0; i < children.Count; i++)
            {
                // inverse index if installing
                int index = i;
                if (!isRemoving)
                    index = children.Count - i - 1;

                if (!currentElementFound)
                {
                    RemoveInstallTreeElement rite = children[index] as RemoveInstallTreeElement;
                    foreach (var target in rite.data.figure.targets)
                    {
                        if (isRemoving)
                            target.gameObject.SetActive(false);
                        else
                            target.gameObject.SetActive(true);
                    }

                    if (children[index].id == currentElement.id)
                        currentElementFound = true;
                }
            }
        }

        private void ResetRemoveInstall()
        {
            Debug.Log("reset remove install");
            VirtualTrainingCamera.ResetCameraPosition(TotalResetRemoveInstall);
            ResetEvent();
        }

        private void TotalResetRemoveInstall()
        {
            Debug.Log("total reset");
            foreach (var target in targets)
            {
                target.SetActive(true);
            }

            targets.Clear();
            root = null;

            ResetEvent();
        }

        private void ResetEvent()
        {
            EventManager.TriggerEvent(new MonitorCameraEvent());
        }
    }
}

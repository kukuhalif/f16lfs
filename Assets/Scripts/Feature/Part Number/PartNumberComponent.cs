using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class PartNumberComponent : MonoBehaviour
    {
        string partNumber;

        public void Setup(string partNumber)
        {
            this.partNumber = partNumber;
        }

        private void OnMouseEnter()
        {
            Show();
        }
        private void OnMouseExit()
        {

        }

        public void Show()
        {
            EventManager.TriggerEvent(new PartNumberInteractionEvent(gameObject, partNumber));
        }

        public void Hide()
        {

        }
    }
}

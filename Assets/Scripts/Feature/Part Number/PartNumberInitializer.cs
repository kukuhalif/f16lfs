using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.SceneManagement;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    // data debug
    //[System.Serializable]
    //public class PartNumberDataDebug
    //{
    //    public string id;
    //    public List<GameObjectType> gameObjects;
    //    public List<GameObject> objs;
    //    public List<GameObject> meshRenderers = new List<GameObject>();

    //    public PartNumberDataDebug(string id, List<GameObjectType> gameObjects)
    //    {
    //        this.id = id;
    //        this.gameObjects = gameObjects;
    //    }
    //}

    public class PartNumberInitializer : InitializationQueue
    {
        class PartNumberData
        {
            public GameObjectType obj;
            public string partNumber;

            public PartNumberData(GameObjectType obj, string partNumber)
            {
                this.obj = obj;
                this.partNumber = partNumber;
            }
        }

        // data debug
        //public List<PartNumberDataDebug> dataDebug;

        List<PartNumberData> partNumberDatas;

        public override void Initialize()
        {
            //dataDebug = new List<PartNumberDataDebug>();
            partNumberDatas = new List<PartNumberData>();

            var databases = DatabaseManager.GetPartNumberDatabases();
            for (int i = 0; i < databases.Count; i++)
            {
                var elements = databases[i].GetTreeElementListWrapper().GetTreeElements();
                foreach (var element in elements)
                {
                    PartNumberTreeElement pne = element as PartNumberTreeElement;
                    if (pne.PartNumberData.parts.Count == 0)
                        continue;

                    foreach (var part in pne.PartNumberData.parts)
                        partNumberDatas.Add(new PartNumberData(part, pne.name));

                    /*
                    // data debug
                    bool found = false;
                    foreach (var dd in dataDebug)
                    {
                        if (dd.id == pne.name)
                        {
                            found = true;
                            foreach (var part in pne.PartNumberData.parts)
                            {
                                dd.gameObjects.Add(new GameObjectType(part.Id));
                            }
                        }
                    }
                    if (!found)
                    {
                        List<GameObjectType> gg = new List<GameObjectType>();
                        foreach (var part in pne.PartNumberData.parts)
                        {
                            gg.Add(new GameObjectType(part.Id));
                        }
                        dataDebug.Add(new PartNumberDataDebug(pne.name, gg));
                    }
                    // end
                    */
                }
            }
        }

        public override void ObjectSetup(GameObject obj)
        {
            string partNumber = GetPartNumber(obj);
            if (partNumber == null)
                return;

            var meshes = obj.transform.GetAllComponentsInChilds<MeshRenderer>();
            for (int m = 0; m < meshes.Length; m++)
            {
                if (meshes[m] == null)
                    continue;

                PartNumberComponent partNumberComponent = meshes[m].GetComponent<PartNumberComponent>();

                if (partNumberComponent == null)
                    partNumberComponent = meshes[m].gameObject.AddComponent<PartNumberComponent>();

                partNumberComponent.Setup(partNumber);
            }
        }

        private void Start()
        {
            EventManager.AddListener<SceneReadyEvent>(SceneReady);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReady);
        }

        private string GetPartNumber(GameObject obj)
        {
            for (int i = 0; i < partNumberDatas.Count; i++)
            {
                if (partNumberDatas[i].obj.gameObject == obj)
                    return partNumberDatas[i].partNumber;
            }

            return null;
        }

        // data setup
        private void SceneReady(SceneReadyEvent e)
        {
            /*
            // data debug
            foreach (var dd in dataDebug)
            {
                dd.objs = new List<GameObject>();
                dd.meshRenderers = new List<GameObject>();
                foreach (var go in dd.gameObjects)
                {
                    dd.objs.Add(go.gameObject);
                    var meshs = go.gameObject.transform.GetAllComponentsInChilds<MeshRenderer>();

                    foreach (var mesh in meshs)
                    {
                        if (mesh != null)
                            dd.meshRenderers.Add(mesh.gameObject);
                    }
                }
            }
            */

            // clear
            partNumberDatas = null;
        }
    }
}

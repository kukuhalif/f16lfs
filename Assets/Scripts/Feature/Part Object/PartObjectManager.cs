using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class PartObjectManager : InitializationQueue
    {
        bool initialized = false;

        delegate void ResetAll();
        event ResetAll resetAll;

        delegate void HideAll(bool hide, bool reset);
        event HideAll hideAll;

        delegate void XrayAll(bool xray, bool reset);
        event XrayAll xrayAll;

        bool isContainSolo;
        bool isContainHighlight;

        Dictionary<GameObject, PartObjectState> objectStateLookup = new Dictionary<GameObject, PartObjectState>();

        static Color DEFAULT_HIGHLIGHT_COLOR;
        static Color DEFAULT_SELECT_COLOR;
        static Color DEFAULT_BLINK_COLOR;
        static Color DEFAULT_FRESNEL_COLOR;
        static Material[] XRAY_MATERIALS;
        static int HIGHLIGHT_XRAY_INDEX;

        public static Color DefaultHighlightColor { get => DEFAULT_HIGHLIGHT_COLOR; }
        public static Color DefaultSelectColor { get => DEFAULT_SELECT_COLOR; }
        public static Color DefaultBlinkColor { get => DEFAULT_BLINK_COLOR; }
        public static Color DefaultFresnelColor { get => DEFAULT_FRESNEL_COLOR; }
        public static Material SelectedXrayMaterial { get => XRAY_MATERIALS[HIGHLIGHT_XRAY_INDEX]; }
        public static Material XrayMaterial(int index) { return XRAY_MATERIALS[index]; }

        List<GameObject> activeObjects = new List<GameObject>();
        List<GameObject> toEnabledObjects = new List<GameObject>();
        List<GameObject> toDisabledObjects = new List<GameObject>();

        ObjectSelector objectSelector;
        List<GameObject> selectedXray = new List<GameObject>();

        public List<GameObject> XrayGameobjects { get => selectedXray; }

        string currentModelSceneName = "";

        public override void Initialize()
        {
            if (initialized)
                return;

            resetAll = null;
            hideAll = null;
            xrayAll = null;

            objectStateLookup.Clear();

            objectSelector = FindObjectOfType<ObjectSelector>();
        }

        public override void ObjectSetup(GameObject obj)
        {
            if (initialized)
                return;

            PartObjectState newState = new PartObjectState(obj);

            resetAll += newState.ResetAllCallback;
            hideAll += newState.HideAllCallback;
            xrayAll += newState.XrayAllCallback;

            objectStateLookup.Add(obj, newState);
        }

        private void Start()
        {
            DEFAULT_HIGHLIGHT_COLOR = DatabaseManager.GetDefaultHighlightColor();
            DEFAULT_SELECT_COLOR = DatabaseManager.GetDefaultSelectedColor();
            DEFAULT_BLINK_COLOR = DatabaseManager.GetDefaultBlinkColor();
            DEFAULT_FRESNEL_COLOR = DatabaseManager.GetDefaultFresnelColor();
            XRAY_MATERIALS = DatabaseManager.GetXrayMaterials().ToArray();

            EventManager.AddListener<SceneReadyEvent>(InitializeDoneListener);
            EventManager.AddListener<PartObjectEvent>(PartObjectListener);
            EventManager.AddListener<RegisterPartObjectEnableOrDisableEvent>(PartObjectEnableOrDisableListener);
            EventManager.AddListener<FresnelObjectsEvent>(FresnelListener);
            EventManager.AddListener<ResetPartObjectEvent>(ResetPartObjectListener);
            EventManager.AddListener<ObjectInteractionXRayEvent>(XRaySelectedListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<PartObjectEvent>(PartObjectListener);
            EventManager.RemoveListener<RegisterPartObjectEnableOrDisableEvent>(PartObjectEnableOrDisableListener);
            EventManager.RemoveListener<FresnelObjectsEvent>(FresnelListener);
            EventManager.RemoveListener<ResetPartObjectEvent>(ResetPartObjectListener);
            EventManager.RemoveListener<ObjectInteractionXRayEvent>(XRaySelectedListener);
            EventManager.RemoveListener<SceneReadyEvent>(InitializeDoneListener);

            resetAll = null;
            hideAll = null;
            xrayAll = null;
        }

        #region event listener

        private void InitializeDoneListener(SceneReadyEvent e)
        {
            initialized = true;

            if (currentModelSceneName != VirtualTrainingSceneManager.CurrentMainScene)
            {
                foreach (var state in objectStateLookup)
                {
                    Renderer[] renderers = state.Value.Target.transform.GetAllComponentsInChildsExcludeThis<Renderer>();
                    List<PartObjectState> childs = new List<PartObjectState>();
                    for (int i = 0; i < renderers.Length; i++)
                    {
                        if (renderers[i] != null && objectStateLookup.ContainsKey(renderers[i].gameObject))
                            childs.Add(objectStateLookup[renderers[i].gameObject]);
                    }
                    state.Value.SetChilds(childs);
                }

                currentModelSceneName = VirtualTrainingSceneManager.CurrentMainScene;
            }
        }

        private void ResetPartObjectListener(ResetPartObjectEvent e)
        {
            // clear selected xray
            selectedXray.Clear();

            // reset all states
            resetAll.Invoke();

            // clear active part object
            activeObjects.Clear();

            // reset current state
            isContainSolo = false;
            isContainHighlight = false;

            // reset enabled and disabled objects
            for (int i = 0; i < toEnabledObjects.Count; i++)
            {
                toEnabledObjects[i].SetActive(true);
            }
            for (int i = 0; i < toDisabledObjects.Count; i++)
            {
                toDisabledObjects[i].SetActive(false);
            }
            toEnabledObjects.Clear();
            toDisabledObjects.Clear();
        }

        private void FresnelListener(FresnelObjectsEvent e)
        {
            StartCoroutine(SetAdditionalState(e.partObjects));
        }

        private void XRaySelectedListener(ObjectInteractionXRayEvent e)
        {
            XraySelected(e.on);
        }

        public void XraySelected(bool xray)
        {
            if (xray)
            {
                List<GameObject> gameObjects = new List<GameObject>(selectedXray);
                gameObjects.AddRange(objectSelector.GetSelected());

                for (int i = 0; i < gameObjects.Count; i++)
                {
                    if (objectStateLookup.ContainsKey(gameObjects[i]))
                    {
                        var objState = objectStateLookup[gameObjects[i]];

                        if (xray)
                            objState.XrayAllCallback(true, true);
                        else
                            objState.ResetState(true);
                    }
                }

                selectedXray = gameObjects;
            }
            else
            {
                for (int i = 0; i < selectedXray.Count; i++)
                {
                    var objState = objectStateLookup[selectedXray[i]];
                    objState.ResetState(true);
                }

                selectedXray.Clear();
            }
        }

        public void XrayObject(int id)
        {
            GameObjectReference gor = GameObjectReference.GetReference(id);
            if (gor == null)
                return;

            if (!objectStateLookup.ContainsKey(gor.gameObject))
                return;

            var objState = objectStateLookup[gor.gameObject];

            objState.XrayAllCallback(true, true);

            selectedXray.Add(gor.gameObject);

            objectSelector.ClearSelection();
        }
        private void PartObjectEnableOrDisableListener(RegisterPartObjectEnableOrDisableEvent e)
        {
            if (e.defaultState)
                toEnabledObjects.Add(e.target);
            else
                toDisabledObjects.Add(e.target);
        }

        private void PartObjectListener(PartObjectEvent e)
        {
            StartCoroutine(SetState(e.objects, e.doneCallbacks));
        }

        #endregion

        private IEnumerator SetAdditionalState(List<PartObject> partObjects)
        {
            yield return null;
            for (int i = 0; i < partObjects.Count; i++)
            {
                if (partObjects[i].target.gameObject != null)
                {
                    PartObjectState state = objectStateLookup[partObjects[i].target.gameObject];
                    state.SetState(partObjects[i]);
                    activeObjects.Add(state.Target);
                }
                else
                    Debug.LogError("part object target null " + partObjects[i].action.ToString());
            }
        }

        private IEnumerator SetState(List<PartObject> partObjects, Action[] doneCallbacks)
        {
            // ganti pake ini, soalnya kalo pake trik camera culling sama clear flag hasilnya ngga bisa persis, soalnya post processingnya masih diproses dari keadaan object yang disorot kamera (bukan dari camera view)
            yield return VirtualTrainingCamera.PauseCamera();

            // reset last state
            if (isContainSolo)
            {
                hideAll.Invoke(false, true);
            }
            if (isContainHighlight || selectedXray.Count > 0)
            {
                xrayAll.Invoke(false, true);
            }
            activeObjects = activeObjects.Distinct().ToList();
            for (int i = 0; i < activeObjects.Count; i++)
            {
                objectStateLookup[activeObjects[i]].ResetState(true);
            }
            activeObjects.Clear();

            // reset last enabled disabled object
            if (toEnabledObjects.Count > 0 || toDisabledObjects.Count > 0)
            {
                for (int i = 0; i < toEnabledObjects.Count; i++)
                {
                    toEnabledObjects[i].SetActive(true);
                }
                for (int i = 0; i < toDisabledObjects.Count; i++)
                {
                    toDisabledObjects[i].SetActive(false);
                }
                toEnabledObjects.Clear();
                toDisabledObjects.Clear();
            }

            //set state to all part objects
            //set is contain solo and is contain highlight
            //set highlight xray index
            yield return null;
            isContainSolo = false;
            isContainHighlight = false;
            for (int i = 0; i < partObjects.Count; i++)
            {
                if (partObjects[i].target.gameObject != null)
                {
                    if (partObjects[i].action == PartObjectAction.Solo)
                    {
                        isContainSolo = true;
                    }
                    else if (partObjects[i].action == PartObjectAction.Highlight)
                    {
                        isContainHighlight = true;
                        HIGHLIGHT_XRAY_INDEX = partObjects[i].xrayIndex;
                    }

                    PartObjectState state = objectStateLookup[partObjects[i].target.gameObject];
                    state.SetState(partObjects[i]);
                    activeObjects.Add(state.Target);
                }
                else
                    Debug.LogError("ada part object yang kosong");
            }

            //if contain solo->hide all else reset hide all
            //if contain xray->highlight all else reset highlighted all
            yield return null;
            if (isContainSolo)
            {
                hideAll.Invoke(true, false);
            }
            if (isContainHighlight)
            {
                xrayAll.Invoke(true, false);
            }

            //invoke done callback
            for (int i = 0; i < doneCallbacks.Length; i++)
            {
                yield return null;
                doneCallbacks[i].Invoke();
            }

            // un freeze camera
            yield return null;
            VirtualTrainingCamera.CameraController.ResumeCamera();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class PartObjectState
    {
        #region target

        GameObject target;
        List<PartObjectState> childs = new List<PartObjectState>();

        #endregion

        #region default states

        int defaultLayer;
        Renderer renderer;
        Material[] defaultMaterials;
        Collider collider = null;
        List<Color> defaultEmissions = new List<Color>();
        Color colorTemp = Color.white;
        bool defaultEnabledState;

        #endregion

        #region states

        List<PartObjectAction> states = new List<PartObjectAction>();
        bool xRayAll = true, hideAll = true;

        #endregion

        public GameObject Target { get => target; }

        public PartObjectState(GameObject target)
        {
            this.target = target;
            defaultLayer = target.layer;

            renderer = target.GetComponent<Renderer>();
            if (renderer != null)
            {
                defaultMaterials = renderer.materials;
                for (int i = 0; i < defaultMaterials.Length; i++)
                {
                    defaultEmissions.Add(defaultMaterials[i].GetColor("_EmissionColor"));
                }
            }

            collider = target.GetComponent<Collider>();

            defaultEnabledState = target.activeSelf;
        }

        public void SetChilds(List<PartObjectState> childs)
        {
            this.childs = childs;
        }

        public void ResetState(bool recursive)
        {
            // reset x ray all
            xRayAll = true;

            // reset hide all
            hideAll = true;

            // object layer
            target.layer = defaultLayer;

            // enable renderer & collider
            if (renderer != null)
                renderer.enabled = true;
            if (collider != null)
                collider.enabled = true;

            // set default material
            if (renderer != null)
            {
                Material[] mats = renderer.materials;
                for (int i = 0; i < defaultMaterials.Length; i++)
                {
                    mats[i] = defaultMaterials[i];
                }
                renderer.materials = mats;
            }

            for (int s = states.Count - 1; s >= 0; s--)
            {
                switch (states[s])
                {
                    case PartObjectAction.Disable:
                        target.SetActive(true);
                        break;
                    case PartObjectAction.Highlight:
                        if (renderer != null)
                        {
                            for (int i = 0; i < defaultMaterials.Length; i++)
                            {
                                renderer.materials[i].SetFloat("_Highlight", 0);
                            }
                        }
                        break;
                    case PartObjectAction.Enable:
                        target.SetActive(false);
                        break;
                    case PartObjectAction.Select:
                        if (renderer != null)
                        {
                            for (int i = 0; i < defaultMaterials.Length; i++)
                            {
                                renderer.materials[i].SetFloat("_Highlight", 0);
                            }
                        }
                        break;
                    case PartObjectAction.Blink:
                        if (renderer != null)
                        {
                            for (int i = 0; i < defaultEmissions.Count; i++)
                            {
                                renderer.materials[i].SetFloat("_Blink", 0);
                                renderer.materials[i].SetColor("_EmissionColor", defaultEmissions[i]);
                            }
                        }
                        break;
                    case PartObjectAction.IgnoreCutaway:
                        if (renderer != null)
                        {
                            for (int i = 0; i < defaultMaterials.Length; i++)
                            {
                                renderer.materials[i].SetFloat("_IgnoreCutaway", 0);
                            }
                        }
                        break;
                    case PartObjectAction.Fresnel:
                        if (renderer != null)
                        {
                            for (int i = 0; i < defaultEmissions.Count; i++)
                            {
                                renderer.materials[i].SetFloat("_Fresnel", 0);
                                renderer.materials[i].SetColor("_EmissionColor", defaultEmissions[i]);
                                renderer.materials[i].SetFloat("_Blink", 0);
                            }
                        }
                        break;
                }
            }

            states.Clear();

            // todo : aman gini aja karena culling manager ditrigger oleh reset object position
            target.SetActive(defaultEnabledState);

            // jika tidak pakai ini
            //if (!defaultEnabledState)
            //{
            //    target.SetActive(false);
            //}
            //else
            //{
            //    bool culled = VirtualTrainingSceneManager.IsObjectCulled(Target);
            //    if (culled)
            //        target.SetActive(false);
            //    else
            //        target.SetActive(true);
            //}

            if (recursive)
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    childs[i].ResetState(true);
                }
            }
        }

        public void ResetAllCallback()
        {
            ResetState(false);
        }

        public void HideAllCallback(bool hide, bool reset)
        {
            if (reset && renderer != null)
            {
                renderer.enabled = !hide;
                if (collider != null)
                    collider.enabled = !hide;
                return;
            }

            if (renderer != null && (hideAll || states.Count == 0))
            {
                renderer.enabled = !hide;
                if (collider != null)
                    collider.enabled = !hide;
            }
        }

        public void XrayAllCallback(bool xray, bool reset)
        {
            if (reset && renderer != null)
            {
                if (defaultEnabledState)
                    target.SetActive(true);

                Material[] mats = renderer.materials;
                for (int i = 0; i < defaultMaterials.Length; i++)
                {
                    mats[i] = xray ? PartObjectManager.SelectedXrayMaterial : defaultMaterials[i];
                }

                renderer.materials = mats;

                if (collider != null)
                    collider.enabled = !xray;
                return;
            }

            if (renderer != null && (xRayAll || states.Count == 0))
            {
                if (defaultEnabledState)
                    target.SetActive(true);

                Material[] mats = renderer.materials;
                for (int i = 0; i < defaultMaterials.Length; i++)
                {
                    mats[i] = xray ? PartObjectManager.SelectedXrayMaterial : defaultMaterials[i];
                }

                renderer.materials = mats;

                if (collider != null)
                    collider.enabled = !xray;
            }
        }

        public void SetState(PartObject partObject)
        {
            states.Add(partObject.action);

            // none and solo do nothing
            switch (partObject.action)
            {
                case PartObjectAction.Solo:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    hideAll = false;

                    break;
                case PartObjectAction.Disable:

                    target.SetActive(false);

                    break;
                case PartObjectAction.Highlight:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    xRayAll = false;

                    colorTemp = partObject.useDefaultColor ? PartObjectManager.DefaultHighlightColor : partObject.customColor;
                    if (partObject.setHighlightColor && renderer != null)
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            renderer.materials[i].SetFloat("_Highlight", 1);
                            renderer.materials[i].SetColor("_HighlightColor", colorTemp);
                        }
                    }

                    break;
                case PartObjectAction.Xray:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    if (renderer != null)
                    {
                        Material[] mats = renderer.materials;
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            mats[i] = PartObjectManager.XrayMaterial(partObject.xrayIndex);
                        }

                        renderer.materials = mats;
                    }

                    if (collider != null)
                        collider.enabled = false;

                    break;
                case PartObjectAction.Enable:

                    target.SetActive(true);

                    break;
                case PartObjectAction.Select:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    colorTemp = partObject.useDefaultColor ? PartObjectManager.DefaultSelectColor : partObject.customColor;
                    if (partObject.setHighlightColor && renderer != null)
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            renderer.materials[i].SetFloat("_Highlight", 1);
                            renderer.materials[i].SetColor("_HighlightColor", colorTemp);
                        }
                    }

                    break;
                case PartObjectAction.Blink:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    colorTemp = partObject.useDefaultColor ? PartObjectManager.DefaultBlinkColor : partObject.customColor;
                    if (renderer != null)
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            renderer.materials[i].SetFloat("_Blink", 1);
                            renderer.materials[i].SetColor("_EmissionColor", colorTemp);
                            renderer.materials[i].SetFloat("_BlinkSpeed", partObject.blinkSpeed);
                        }
                    }

                    break;
                case PartObjectAction.SetLayer:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    target.layer = partObject.layerOption == LayerOption.MainCamera ? VirtualTrainingCamera.MainCameraLayer : VirtualTrainingCamera.MonitorCameraLayer;

                    break;
                case PartObjectAction.IgnoreCutaway:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    if (renderer != null)
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            renderer.materials[i].SetFloat("_IgnoreCutaway", 1);
                        }
                    }

                    break;
                case PartObjectAction.Fresnel:

                    if (defaultEnabledState)
                        target.SetActive(true);

                    colorTemp = partObject.useDefaultColor ? PartObjectManager.DefaultFresnelColor : partObject.customColor;
                    if (renderer != null)
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            renderer.materials[i].SetFloat("_Fresnel", 1);
                            renderer.materials[i].SetColor("_EmissionColor", colorTemp);

                            if (partObject.fresnelWithBlink)
                            {
                                renderer.materials[i].SetFloat("_Blink", 1);
                                renderer.materials[i].SetFloat("_BlinkSpeed", partObject.blinkSpeed);
                            }
                        }
                    }

                    break;
                case PartObjectAction.Hide:

                    if (renderer != null)
                        renderer.enabled = false;

                    if (collider != null)
                        collider.enabled = false;

                    break;
            }

            if (partObject.isRecursive)
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    childs[i].SetState(partObject);
                }
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public abstract class AnimationPlayerBase : MonoBehaviour
    {
        VirtualButtonDataModel virtualButtonDataModel;
        VirtualButtonOutputData vbOutputData;
        public AnimationFigureData figureData;
        Animator animator;
        float sliderPercentage;
        Vector3 startPosition;
        Vector3 startRotation;
        public Vector3 currentPosition;
        public Vector3 currentRotation;
        bool isInteraction;

        protected float maxSliderValue;
        protected float sliderValueTemp;

        public bool isBack;
        public bool setCurrentParam;

        public float sliderValue { get => sliderPercentage; }
        //public Animator getAnimator { get => animator; }
        public VirtualButtonDataModel getVBDataModel { get => virtualButtonDataModel; }
        public VirtualButtonOutputData getVBOutputData { get => vbOutputData; }
        public bool isInteractionObject { get => isInteraction; }
        public Vector3 getStartPosition { get => startPosition; }
        public Vector3 getStartRotation { get => startRotation; }
        public bool isAnimatedScript;

        VirtualButtonOnAnimation virtualButtonOnAnimation;
        public VirtualButtonOnAnimation getvirtualButtonOnAnimation { get => virtualButtonOnAnimation; }
        bool isVirtualButton;
        [SerializeField] VirtualButtonBase virtualbuttonBase;
        public Animator getAnimator()
        {
            if (animator == null)
            {
                animator = GetComponent<Animator>();
            }

            return animator;
        }
        public VirtualButtonBase getvirtualButtonBase { get => virtualbuttonBase; }
        public abstract void AnimatingDragTwoAxis();

        public abstract void StartPlayAnimation(float maxValue, float speed);

        public void SetSliderPercentage(float value)
        {
            if (value < 0)
                value = 0;
            else if (value > 1)
                value = 1;
            sliderPercentage = value;
        }

        public void SetMinMaxValue(float min, float max)
        {
            if (vbOutputData != null)
            {
                vbOutputData.minimalValue = min;
                vbOutputData.maximalValue = max;
            }
        }

        protected void SetVBBaseData(VirtualButtonDataModel vbDataModel, VirtualButtonOutputData vbOutputData, bool interaction)
        {
            virtualButtonDataModel = vbDataModel;
            this.vbOutputData = vbOutputData;
            isInteraction = interaction;
        }


        public void SetupVBBase(VirtualButtonOutputData vbOutputData = null)
        {
            virtualButtonDataModel = new VirtualButtonDataModel();
            if (vbOutputData != null)
                this.vbOutputData = vbOutputData;
            else
                this.vbOutputData = new VirtualButtonOutputData();

            if (isAnimatedScript == false)
                animator = GetComponent<Animator>();
        }

        public void SetupVBinAnimation(VirtualButtonOnAnimation vbOnAnimation, VirtualButtonBase vbBase, bool isVirtualButton)
        {
            this.isVirtualButton = isVirtualButton;
            virtualButtonOnAnimation = vbOnAnimation;
            virtualbuttonBase = vbBase;
            this.vbOutputData = new VirtualButtonOutputData();
        }

        protected void StartAnimator(bool condition)
        {
            animator = GetComponent<Animator>();
            if (animator != null)
            {
                animator.enabled = condition;
                Debug.Log("enable animator : " + animator.gameObject.name + " : " + condition);
            }

            startPosition = gameObject.transform.localPosition;
            startRotation = gameObject.transform.localEulerAngles;
            currentPosition = startPosition;
            currentRotation = startRotation;
        }

        public virtual void DestroyItself()
        {
            sliderPercentage = 0;
            //gameObject.SetActive(true);
            Destroy(this);
        }

        public virtual void ResetAnimation()
        {
            //sliderPercentage = 0;
            //transform.localPosition = startPosition;
            //transform.localEulerAngles = startRotation;
            //gameObject.SetActive(true);
        }

        public virtual void StopAnimation()
        {

        }

        public abstract void PauseAnimation();

        protected bool CheckModifierExcept(float value)
        {
            bool condition = false;

            if (isInteractionObject == false)
            {
                if (getVBDataModel != null)
                {
                    if (getVBDataModel.vbModifierData.Count <= 0)
                        return condition;

                    for (int i = 0; i < getVBDataModel.vbModifierData.Count; i++)
                    {
                        if (getVBDataModel.vbModifierData[i].vbModifierType == VirtualbuttonModifierType.ExceptValues)
                        {
                            for (int j = 0; j < getVBDataModel.vbModifierData[i].modifierConditionValues.Count; j++)
                            {
                                if (getVBDataModel.vbModifierData[i].modifierConditionValues[j].minValue < value &&
                                    getVBDataModel.vbModifierData[i].modifierConditionValues[j].maxValue > value)
                                {
                                    condition = true;
                                }
                            }

                            if (getVBDataModel.vbModifierData[i].defaultCondition == DefaultCondition.On)
                                condition = !condition;

                            if (condition == true && getVBDataModel.vbModifierData[i].exceptValueType == ExceptValueType.PlayPause)
                            {
                                PauseAnimation();
                            }
                        }
                    }
                }

            }
            return condition;
        }
    }
}

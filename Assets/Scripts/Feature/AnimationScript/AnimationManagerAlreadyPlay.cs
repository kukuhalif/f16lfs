using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.UI;
using System.Linq;
using System;

namespace VirtualTraining.Feature
{
    public partial class AnimationManager : MonoBehaviour // for function animation already used
    {

        void AddedDonResetToPreviously(AnimationPlayerBase animationFigureData)
        {
            bool isInList = false;

            for (int i = 0; i < animationPlayersPreviously.Count; i++)
            {
                if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimScript)
                {
                    if (CheckingAnimationScript(animationFigureData.figureData, animationPlayersPreviously[i].figureData) == true)
                    {
                        isInList = true;
                    }
                }
                else if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimManual)
                {
                    if (CheckingAnimationManual(animationFigureData.figureData, animationPlayersPreviously[i].figureData) == true)
                    {
                        isInList = true;
                    }
                }
            }

            if (isInList == false)
                animationPlayersPreviously.Add(animationFigureData);
        }

        bool CheckedDontResetAnimFigurData(AnimationFigureData animationFigureData)
        {
            for (int i = 0; i < animationPlayersPreviously.Count; i++)
            {
                if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimScript)
                {
                    if (CheckingAnimationScript(animationPlayersPreviously[i].figureData, animationFigureData) == true)
                    {
                        return true;
                    }
                }
                else if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimManual)
                {
                    if (CheckingAnimationManual(animationFigureData, animationPlayersPreviously[i].figureData) == true)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        bool CheckDontResetObject(AnimationPlayerBase animationPlayer)
        {
            if (animationPlayer.figureData == null)
                return false;

            for (int i = 0; i < animationPlayersPreviously.Count; i++)
            {
                //if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimVB
                //    && animationPlayersPreviously[i].figureData.virtualButton.virtualButtonElementType.GetData() != null)
                //{
                //    if (animationPlayer.figureData.animationType == AnimationType.AnimVB
                //    && animationPlayer.figureData.virtualButton.virtualButtonElementType.GetData() != null)
                //    {
                //        if (animationPlayer.figureData.virtualButton.virtualButtonElementType.GetId() ==
                //            animationPlayersPreviously[i].figureData.virtualButton.virtualButtonElementType.GetId())
                //        {
                //            return true;
                //        }
                //    }
                //}
                if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimScript)
                {
                    if (animationPlayer.figureData.animationType == AnimationType.AnimScript)
                    {
                        if (CheckingAnimationScript(animationPlayersPreviously[i].figureData, animationPlayer.figureData) == true)
                        {
                            return true;
                        }
                    }
                }
                else if (animationPlayersPreviously[i].figureData.animationType == AnimationType.AnimManual)
                {
                    if (CheckingAnimationManual(animationPlayer.figureData, animationPlayersPreviously[i].figureData) == true)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        bool DontResetObjectFirst(AnimationPlayerBase animationPlayer)
        {
            for (int i = 0; i < animationsDontReset.Count; i++)
            {
                if (animationsDontReset[i].ignoreIfAlreadyPlayed == false)
                    continue;

                if (animationsDontReset[i].animationType == AnimationType.AnimScript)
                {
                    if (CheckingAnimationScript(animationsDontReset[i], animationPlayer.figureData) == true)
                    {
                        return true;
                    }
                }
                else if (animationsDontReset[i].animationType == AnimationType.AnimManual)
                {
                    if (CheckingAnimationManual(animationPlayer.figureData, animationsDontReset[i]) == true)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        void AddedToDontReset()
        {
            for (int i = 0; i < animationPlayers.Count; i++)
            {
                if (animationPlayers[i].figureData != null)
                {
                    if (DontResetObjectFirst(animationPlayers[i]) == true)
                    {
                        AddedDonResetToPreviously(animationPlayers[i]);
                    }
                }
            }
        }

        void DestroyPreviouslyNotUse()
        {
            if (animationsDontReset.Count == 0)
            {
                for (int j = 0; j < animationPlayersPreviously.Count; j++)
                {
                    animationPlayersPreviously[j].DestroyItself();
                }
                animationPlayersPreviously.Clear();
            }

            for (int i = 0; i < animationsDontReset.Count; i++)
            {
                for (int j = 0; j < animationPlayersPreviously.Count; j++)
                {
                    if (animationPlayersPreviously[j].figureData.animationType == AnimationType.AnimScript)
                    {
                        if (CheckingAnimationScript(animationsDontReset[i], animationPlayersPreviously[j].figureData) == true)
                            continue;
                    }
                    else if(animationPlayersPreviously[j].figureData.animationType == AnimationType.AnimManual)
                    {
                        if (CheckingAnimationManual(animationsDontReset[i], animationPlayersPreviously[j].figureData) == true)
                            continue;
                    }

                    animationPlayersPreviously[j].DestroyItself();
                    animationPlayersPreviously.RemoveAt(j);
                }
            }
        }

        bool CheckingAnimationScript(AnimationFigureData anim1, AnimationFigureData anim2)
        {
            if (anim1.scriptedAnimationElement.GetId() == anim2.scriptedAnimationElement.GetId()
             && anim1.gameObjectInteraction.gameObject == anim2.gameObjectInteraction.gameObject)
            {
                return true;
            }

            return false;
        }

        bool CheckingAnimationManual(AnimationFigureData anim1, AnimationFigureData anim2)
        {
            if (anim1.animationName == anim2.animationName
             && anim1.animationLayer == anim2.animationLayer)
            {
                return true;
            }

            return false;
        }
    }
}

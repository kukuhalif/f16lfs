using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class AnimationPlayerManual : AnimationPlayerBase
    {
        string animName;
        int animLayer;
        string speedLayer;

        string parameterName;
        string parameterNameY;
        float maxValue;
        float minValue;
        float defaultValue;

        bool isAnimatorStatePlay;
        bool isAnimatorParameterPlay;
        float sliderPercentageTemp;

        float currentSliderX;
        float currentSliderY;
        float startParameterX;
        float startParameterY;

        float speedAnimator;
        float speedParameter;
        bool isStart;
        public float getSliderPercentageTemp { get => sliderPercentageTemp; }
        public string getStateName { get => animName; }
        public int getAnimLayer { get => animLayer; }

        public void Setup(VirtualButtonDataModel vbDataModel, bool interaction, VirtualButtonOutputData vbOutputData = null, VBStartAnimationData vbStartData = null, bool isStart = false)
        {
            this.isStart = isStart;
            VirtualButtonOutputData vbOutputDatas = new VirtualButtonOutputData();

            if (vbStartData != null)
            {
                vbOutputDatas.animationState = vbStartData.animationState;
                vbOutputDatas.animationLayer = vbStartData.animationLayer;
                vbOutputDatas.animationSpeedParameter = vbStartData.animationSpeedParameter;
            }
            else
            {
                vbOutputDatas = vbOutputData;
            }

            SetVBBaseData(vbDataModel, vbOutputDatas, interaction);
            SetAnimName(vbOutputDatas.animationState, vbOutputDatas.animationSpeedParameter, vbOutputDatas.animationLayer);
        }

        public void SetAnimName(string animState, string speed = null, string animLayer = null, bool isSetupVB = false)
        {
            if (isSetupVB)
            {
                SetupVBBase();
            }

            if (!string.IsNullOrEmpty(animState))
                animName = animState;

            CheckAnimLayer(animLayer);

            if (!string.IsNullOrEmpty(speed))
                speedLayer = speed;
            else
                speedLayer = "speed";

            SetFigureData(animLayer, animName, speedLayer);
        }
        public void SetupDataModel(VirtualButtonDataModel vbDataModel, VirtualButtonOutputData vbOutputDatas, bool interaction = false)
        {
            SetVBBaseData(vbDataModel, vbOutputDatas, interaction);
        }

        void SetFigureData(string layer, string stateName, string speedName)
        {
            if (figureData == null)
                figureData = new AnimationFigureData();

            figureData.animationLayer = layer;
            figureData.animationName = stateName;
            figureData.animationSpeedParameter = speedName;
        }

        public void SetupParameterDatas(float minValue, float maxValue, float defaultValue, string parameterX = null, string parameterY = null)
        {

            parameterName = parameterX;
            parameterNameY = parameterY;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.defaultValue = defaultValue;
        }

        void CheckAnimLayer(string animLayer)
        {
            if (!string.IsNullOrEmpty(animLayer))
            {
                Animator anim = getAnimator();
                for (int j = 0; j < anim.layerCount; j++)
                {
                    if (anim.GetLayerName(j) == animLayer)
                    {
                        this.animLayer = j;
                        break;
                    }
                }
            }
            else
            {
                this.animLayer = 0;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            StartAnimator(true);
            if (CheckModifierExcept(defaultValue) == false)
            {
                SetSliderPercentage(defaultValue);
            }

            if (getAnimator() != null)
            {
                SetupParameterOnce();
                if (!string.IsNullOrEmpty(animName))
                    getAnimator().SetFloat(speedLayer, 0);

                if (isStart)
                {
                    StartPlayAnimation(1, getVBOutputData.speed);
                }
            }
        }

        void SetupParameterOnce()
        {
            float deltaValue = Mathf.Lerp(minValue, maxValue, defaultValue);
            if (!string.IsNullOrEmpty(parameterName))
            {
                startParameterX = getAnimator().GetFloat(parameterName);
                getAnimator().SetFloat(parameterName, deltaValue);
            }

            if (!string.IsNullOrEmpty(parameterNameY))
            {
                startParameterY = getAnimator().GetFloat(parameterNameY);
                getAnimator().SetFloat(parameterNameY, deltaValue);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (isAnimatorParameterPlay)
                AnimatingParameter();

            if (isAnimatorStatePlay)
                AnimatingDragTwoAxis();

            if (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.direction != DragDirection.Both && !getVBDataModel.isSequentialDrag)
            {
                if (!string.IsNullOrEmpty(parameterName))
                {
                    float x = Mathf.Lerp(minValue, maxValue, sliderValue);
                    getAnimator().SetFloat(parameterName, x);
                }
            }
        }

        public override void AnimatingDragTwoAxis()
        {
            if (!string.IsNullOrEmpty(animName))
            {
                if (getAnimator().GetCurrentAnimatorStateInfo(animLayer).IsName(animName))
                    SetSliderPercentage(getAnimator().GetCurrentAnimatorStateInfo(animLayer).normalizedTime);

                if (getAnimator().GetCurrentAnimatorStateInfo(animLayer).loop == true)
                {
                    if (sliderValueTemp > maxSliderValue)
                    {
                        if (sliderValue < maxSliderValue)
                        {
                            SetSliderPercentage(Mathf.Round(sliderValue * 10f) / 10f);
                            isAnimatorStatePlay = false;
                            SetSliderPercentage(maxSliderValue);
                        }
                    }
                    else
                    {
                        if (sliderValue > maxSliderValue)
                        {
                            SetSliderPercentage(0);
                            getAnimator().Play(animName, animLayer, 0f);
                        }
                    }
                }
                else
                {
                    SetSliderPercentage(Mathf.Round(sliderValue * 100f) / 100f);

                    if (sliderValueTemp > maxSliderValue)
                    {
                        if (sliderValue < maxSliderValue)
                        {
                            getAnimator().SetFloat(speedLayer, 0);
                            isAnimatorStatePlay = false;
                            SetSliderPercentage(maxSliderValue);
                        }
                    }
                    else
                    {
                        if (sliderValue > maxSliderValue)
                        {
                            getAnimator().SetFloat(speedLayer, 0);
                            isAnimatorStatePlay = false;
                            SetSliderPercentage(maxSliderValue);
                        }
                    }
                }
            }
        }

        public override void StartPlayAnimation(float maxValue, float speed)
        {
            if (getAnimator() == null)
                return;

            if (CheckModifierExcept(maxValue) == true)
            {
                return;
            }

            maxSliderValue = maxValue;
            sliderValueTemp = sliderValue;

            if (!string.IsNullOrEmpty(animName))
                StartAnimationManual(speed);

            if (!string.IsNullOrEmpty(parameterName))
            {
                StartParameter(speed);
            }
        }

        void StartAnimationManual(float speed)
        {
            if (sliderValue < maxSliderValue)
                getAnimator().SetFloat(speedLayer, speed);
            else if (sliderValue > maxSliderValue)
                getAnimator().SetFloat(speedLayer, -speed);
            else
                getAnimator().SetFloat(speedLayer, 0);

            if (!string.IsNullOrEmpty(animName))
                getAnimator().Play(animName, animLayer, sliderValue);

            if (isStart && !string.IsNullOrEmpty(animName))
                getAnimator().Play(animName, animLayer, sliderValue);
            isAnimatorStatePlay = true;
        }

        void StartParameter(float speed)
        {
            SetSliderPercetageTempOnce();

            if (sliderPercentageTemp <= maxSliderValue)
                speedParameter = speed;
            else if (sliderPercentageTemp >= maxSliderValue)
                speedParameter = -speed;
            else
                speedParameter = 0;
        }

        void SetSliderPercetageTempOnce()
        {
            sliderPercentageTemp = (getAnimator().GetFloat(parameterName) - minValue) / (maxValue - minValue);
            isAnimatorParameterPlay = true;
        }

        void AnimatingParameter()
        {
            if (getVBDataModel.vbType == VirtualButtonType.Drag && (!getVBDataModel.isSequentialDrag || getVBDataModel.direction == DragDirection.Both))
                return;

            PlayOnceParameter();
        }
        void PlayOnceParameter()
        {
            if (!string.IsNullOrEmpty(parameterName))
            {
                sliderPercentageTemp += Time.deltaTime * speedParameter;

                if (sliderValue <= maxSliderValue)
                {
                    if (sliderPercentageTemp > sliderValue)
                        SetSliderPercentage(sliderPercentageTemp);
                }
                else if (sliderValue >= maxSliderValue)
                {
                    if (sliderPercentageTemp < sliderValue)
                        SetSliderPercentage(sliderPercentageTemp);
                }

                float x = Mathf.Lerp(minValue, maxValue, sliderPercentageTemp);
                getAnimator().SetFloat(parameterName, x);

                if (speedParameter > 0)
                {
                    if (sliderPercentageTemp > maxSliderValue)
                    {
                        speedParameter = 0;
                        sliderPercentageTemp = maxSliderValue;
                        x = Mathf.Lerp(minValue, maxValue, sliderPercentageTemp);
                        getAnimator().SetFloat(parameterName, x);
                        SetSliderPercentage(sliderPercentageTemp);
                        isAnimatorParameterPlay = false;
                    }
                }
                else if (speedParameter < 0)
                {
                    if (sliderPercentageTemp < maxSliderValue)
                    {
                        
                        speedParameter = 0;
                        sliderPercentageTemp = maxSliderValue;
                        x = Mathf.Lerp(minValue, maxValue, sliderPercentageTemp);
                        getAnimator().SetFloat(parameterName, x);
                        SetSliderPercentage(sliderPercentageTemp);
                        isAnimatorParameterPlay = false;
                    }
                }
            }
        }

        public void StartPlayAnimationSequentialState(float maxValue, float startSlider)
        {
            maxSliderValue = maxValue;
            sliderValueTemp = startSlider;

            if (!string.IsNullOrEmpty(animName))
            {
                if (startSlider <= maxSliderValue)
                    getAnimator().SetFloat(speedLayer, getVBOutputData.speed);
                else if (startSlider >= maxSliderValue)
                    getAnimator().SetFloat(speedLayer, -getVBOutputData.speed);
                else
                    getAnimator().SetFloat(speedLayer, 0);


                getAnimator().Play(animName, animLayer, startSlider);
            }
            SetSliderPercentage(startSlider);
            isAnimatorStatePlay = true;
        }

        public void SetBothDrag(float x, float y, bool isplayVB = false)
        {
            if (getAnimator() != null)
            {
                if (isplayVB == true)
                {
                    currentSliderX = 0;
                    currentSliderY = 0;
                }

                // todo : quick fix by bram, set value to min max
                x = Mathf.Lerp(minValue, maxValue, x);
                y = Mathf.Lerp(minValue, maxValue, y);

                //getAnimator().Play(animName);
                if (!string.IsNullOrEmpty(parameterName))
                {
                    if (setCurrentParam == false && isplayVB == false)
                        currentSliderX = getAnimator().GetFloat(parameterName);

                    //x = currentSliderX + x;
                    if (x <= maxValue && x >= minValue)
                        getAnimator().SetFloat(parameterName, x);
                }
                if (!string.IsNullOrEmpty(parameterNameY))
                {
                    if (setCurrentParam == false && isplayVB == false)
                        currentSliderY = getAnimator().GetFloat(parameterNameY);

                    //y = currentSliderY + y;
                    if (y <= maxValue && y >= minValue)
                        getAnimator().SetFloat(parameterNameY, y);

                }
                setCurrentParam = true;
            }
        }

        public void SetElasticBothDrag(float speed)
        {
            if (getAnimator() != null)
            {
                getAnimator().Play(animName);
                if (!string.IsNullOrEmpty(parameterName))
                {
                    float XDefaultValue = minValue + defaultValue * (maxValue - minValue);
                    float x = Mathf.Lerp(getAnimator().GetFloat(parameterName), XDefaultValue, speed * Time.deltaTime);
                    getAnimator().SetFloat(parameterName, x);
                }
                if (!string.IsNullOrEmpty(parameterNameY))
                {
                    float YDefaultValue = minValue + defaultValue * (maxValue - minValue);
                    float y = Mathf.Lerp(getAnimator().GetFloat(parameterNameY), YDefaultValue, speed * Time.deltaTime);
                    getAnimator().SetFloat(parameterNameY, y);
                }
            }
        }

        public override void StopAnimation()
        {
            base.StopAnimation();
            if (getAnimator() != null && !string.IsNullOrEmpty(animName))
                getAnimator().SetFloat(speedLayer, 0);
            isAnimatorStatePlay = false;
            isAnimatorParameterPlay = false;
        }

        public override void PauseAnimation()
        {
            if (getAnimator() != null && !string.IsNullOrEmpty(animName))
                getAnimator().SetFloat(speedLayer, 0);
            isAnimatorStatePlay = false;
            isAnimatorParameterPlay = false;
        }

        public override void ResetAnimation()
        {
            base.ResetAnimation();
            if (getAnimator() != null)
                getAnimator().Play(animName, animLayer, sliderValue);


        }

        public override void DestroyItself()
        {
            base.DestroyItself();
            if (getAnimator() != null)
            {
                getAnimator().Play(animName, animLayer, sliderValue);
                ResetParameterOnce();
                getAnimator().Rebind();
                //getAnimator().Play("Default", animLayer, sliderValue);
                getAnimator().Update(0);
                getAnimator().enabled = false;
            }
        }

        void ResetParameterOnce()
        {
            if (!string.IsNullOrEmpty(parameterName))
                getAnimator().SetFloat(parameterName, startParameterX);
            if (!string.IsNullOrEmpty(parameterNameY))
                getAnimator().SetFloat(parameterNameY, startParameterY);
        }
    }
}

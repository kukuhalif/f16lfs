using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class AnimationPlayerScript : AnimationPlayerBase
    {
        [System.Serializable]
        public class AnimationSliderData
        {
            public Vector3 startPosition;
            public Vector3 endPosition;
            public Vector3 startRotation;
            public Vector3 endRotation;
            public float duration;
            public float originalDuration;
            public float sliderValue;
        }

        public float totalDuration;
        float totalDurationTemp;
        float durationReduceValue;

        Vector3 startPositionTemp;
        Vector3 endPositionTemp;
        Vector3 startRotationTemp;
        Vector3 endRotationTemp;

        public float duration;
        //Vector3 currentPosition;
        //Vector3 currentRotation;
        Vector3 deltaPosition;
        Vector3 deltaRotation;

        float lerpValue;
        bool isAnimating;
        bool globalCoordinate;
        bool isReverse;

        float currentSlider;
        float currentLerpValue;
        public List<SADataKeyframe> scriptedAnimationDatas = new List<SADataKeyframe>();
        [SerializeField] List<AnimationSliderData> durationAnimations = new List<AnimationSliderData>();
        List<AnimationSliderData> durationAnimationsBackward = new List<AnimationSliderData>();
        float currentSpeed;
        
        //GameObject parentPosition;
        //GameObject parentRotation;
        //Transform parentOld;
        //Vector3 parentOldStartPosition;

        public void SetupForVirtualButton(VirtualButtonDataModel vbDataModel, VirtualButtonOutputData vbOutputData, bool interaction)
        {
            SetVBBaseData(vbDataModel, vbOutputData, interaction);
            this.globalCoordinate = vbOutputData.scriptedAnimationElement.GetData().globalCoordinate;
            this.isReverse = vbOutputData.isReverseAnimation;

            if (this.globalCoordinate)
                scriptedAnimationDatas = vbOutputData.scriptedAnimationElement.GetData().animDataGlobal;
            else
                scriptedAnimationDatas = vbOutputData.scriptedAnimationElement.GetData().animData;

            SetSliderPercentage(getVBDataModel.defaultValue);
            isAnimatedScript = true;
            Init();
        }
        public void SetupOnStart(VirtualButtonDataModel vbDataModel, VBStartAnimationData vbStartData, bool interaction)
        {
            VirtualButtonOutputData vbOutputData = new VirtualButtonOutputData();
            vbOutputData.scriptedAnimationElement = vbStartData.scriptedAnimationElement;
            vbOutputData.ObjectAnimation = vbStartData.ObjectAnimation;
            vbOutputData.isReverseAnimation = vbStartData.isReverseAnimation;
            vbOutputData.isDisabledFinished = vbStartData.isDisabledFinished;
            vbOutputData.isEnabledAtStart = vbStartData.isEnableAtStart;
            SetVBBaseData(vbDataModel, vbOutputData, interaction);
            this.globalCoordinate = vbOutputData.scriptedAnimationElement.GetData().globalCoordinate;
            this.isReverse = vbOutputData.isReverseAnimation;

            if (this.globalCoordinate)
                scriptedAnimationDatas = vbOutputData.scriptedAnimationElement.GetData().animDataGlobal;
            else
                scriptedAnimationDatas = vbOutputData.scriptedAnimationElement.GetData().animData;

            SetSliderPercentage(getVBDataModel.defaultValue);
            isAnimatedScript = true;
            Init();
        }

        public void SetupAnimation(AnimationFigureData animationFigureData)
        {
            this.globalCoordinate = animationFigureData.scriptedAnimationElement.GetData().globalCoordinate;
            this.isReverse = animationFigureData.isReverseAnimation;
            figureData = animationFigureData;
            if (this.globalCoordinate)
                scriptedAnimationDatas = animationFigureData.scriptedAnimationElement.GetData().animDataGlobal;
            else
                scriptedAnimationDatas = animationFigureData.scriptedAnimationElement.GetData().animData;

            for (int j = 0; j < scriptedAnimationDatas.Count; j++)
            {
                totalDuration += scriptedAnimationDatas[j].duration;
            }
            isAnimatedScript = true;
            VirtualButtonOutputData vbOutputData = new VirtualButtonOutputData();
            vbOutputData.isDisabledFinished = animationFigureData.isDisabledFinished;
            vbOutputData.isEnabledAtStart = animationFigureData.isEnabledAtStart;
            SetupVBBase(vbOutputData);
            Init();
        }

        public float GetTotalDuration()
        {
            totalDuration = 0;
            for (int j = 0; j < scriptedAnimationDatas.Count; j++)
            {
                totalDuration += scriptedAnimationDatas[j].duration;
            }

            return totalDuration;
        }

        public void SetupAnimationScriptTools(bool isCoordinate)
        {
            this.globalCoordinate = isCoordinate;
            SetupVBBase();
            Init();
        }

        private void Start()
        {
            //Init();
        }

        void Init()
        {
            StartAnimator(false);
            if (scriptedAnimationDatas != null)
                SetupAnimationScript();

            if (getVBOutputData.isDisabledFinished == false && isReverse)
            {
                if (durationAnimationsBackward.Count > 0)
                    this.gameObject.transform.localPosition = durationAnimationsBackward[0].startPosition;

                //if (this.gameObject.activeInHierarchy)
                //{
                //    EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(this.gameObject, true));
                //    Debug.Log("true " + gameObject.name);
                //}
                //else
                //{
                //    EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(this.gameObject, false));
                //    Debug.Log("false " + gameObject.name);
                //}
                if (getVBOutputData.isEnabledAtStart)
                {
                    this.gameObject.SetActive(true);
                }
                else
                    this.gameObject.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            AnimatingDragTwoAxis();
            //if (parentPosition != null && parentOld != null)
            //{
            //    Vector3 parentDeltaPosition = parentOld.transform.position - parentOldStartPosition;
            //    if (parentDeltaPosition != Vector3.zero)
            //    {
            //        parentPosition.transform.position = parentPosition.transform.position + (parentOld.transform.position - parentOldStartPosition);
            //        parentOldStartPosition = parentOld.transform.position;
            //    }
            //}
        }

        public override void AnimatingDragTwoAxis()
        {
            if (isAnimating)
            {
                PlayAnimation();
                if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
                {
                    if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter || getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation
                        || getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState)
                    {
                        if (isAnimating)
                        {
                            if (isReverse)
                                PlayAnimationSlider(durationAnimationsBackward);
                            else
                                PlayAnimationSlider(durationAnimations);
                        }
                    }
                    else
                    {
                        if (isReverse)
                            PlayAnimationSlider(durationAnimationsBackward);
                        else
                            PlayAnimationSlider(durationAnimations);
                    }
                }
                else
                {
                    if (isReverse)
                        PlayAnimationSlider(durationAnimationsBackward);
                    else
                        PlayAnimationSlider(durationAnimations);
                }
            }
        }

        public override void StartPlayAnimation(float maxValue, float speed)
        {
            if (CheckModifierExcept(maxValue) == true)
                return;

            transform.gameObject.SetActive(true); //selalu nyala ketika animasi dipanggil
            maxSliderValue = maxValue;

            currentLerpValue = sliderValue;
            totalDurationTemp = totalDuration * (Mathf.Abs(maxValue - currentLerpValue));
            lerpValue = 0;
            isAnimating = true;
            currentSpeed = speed;
        }

        void SetupAnimationScript()
        {
            //if (getVBOutputData.isDisabledFinished)
            //{
            //    EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(this.gameObject, true));
            //}

            if (totalDuration == 0)
            {
                for (int i = 0; i < scriptedAnimationDatas.Count; i++)
                {
                    totalDuration += scriptedAnimationDatas[i].duration;
                }
            }
            totalDurationTemp = totalDuration;

            for (int j = 0; j < scriptedAnimationDatas.Count; j++)
            {
                durationReduceValue += scriptedAnimationDatas[j].duration;

                if (j == 0)
                {

                    if (globalCoordinate)
                    {
                        startPositionTemp = gameObject.transform.position;
                        //startRotationTemp = Vector3.zero;
                        startRotationTemp = gameObject.transform.eulerAngles;
                    }
                    else
                    {
                        startPositionTemp = gameObject.transform.localPosition;
                        //startRotationTemp = Vector3.zero;
                        startRotationTemp = gameObject.transform.localEulerAngles;
                    }

                    endRotationTemp += startRotationTemp + scriptedAnimationDatas[j].deltaRotation;
                    endPositionTemp += startPositionTemp + scriptedAnimationDatas[j].deltaPosition;

                }
                else if (j > 0)
                {
                    startPositionTemp += scriptedAnimationDatas[j - 1].deltaPosition;
                    startRotationTemp += scriptedAnimationDatas[j - 1].deltaRotation;

                    endRotationTemp += scriptedAnimationDatas[j].deltaRotation;
                    endPositionTemp += scriptedAnimationDatas[j].deltaPosition;
                    sliderValueTemp += scriptedAnimationDatas[j].duration / totalDuration;
                }

                MakeAnimationDataTemp(durationAnimations, durationReduceValue, startPositionTemp, endPositionTemp, startRotationTemp, endRotationTemp, sliderValueTemp, scriptedAnimationDatas[j].duration);
            }

            if (isReverse)
            {
                durationReduceValue = 0;
                sliderValueTemp = 0;
                for (int i = durationAnimations.Count - 1; i >= 0; i--)
                {
                    durationReduceValue += scriptedAnimationDatas[i].duration;
                    if (i < durationAnimations.Count - 1)
                        sliderValueTemp += scriptedAnimationDatas[i].duration / totalDuration;

                    MakeAnimationDataTemp(durationAnimationsBackward, durationReduceValue, durationAnimations[i].endPosition, durationAnimations[i].startPosition, durationAnimations[i].endRotation, durationAnimations[i].startRotation, sliderValueTemp, durationAnimations[i].originalDuration);
                }
            }
            //InstantiateRotationParent();

        }
        void MakeAnimationDataTemp(List<AnimationSliderData> animData, float duration, Vector3 startPosition, Vector3 endPosition, Vector3 startRotation, Vector3 endRotation, float SliderValue, float originalDuration)
        {
            AnimationSliderData animationSliderBackward = new AnimationSliderData();
            animationSliderBackward.duration = duration;
            animationSliderBackward.startPosition = startPosition;
            animationSliderBackward.endPosition = endPosition;
            animationSliderBackward.startRotation = startRotation;
            animationSliderBackward.endRotation = endRotation;
            animationSliderBackward.sliderValue = SliderValue;
            animationSliderBackward.originalDuration = originalDuration;

            animData.Add(animationSliderBackward);
        }

        void PlayAnimationSlider(List<AnimationSliderData> animationDatas)
        {
            float x = sliderValue * totalDuration;

            for (int i = 0; i < animationDatas.Count; i++)
            {
                if (x <= animationDatas[i].duration)
                {
                    deltaPosition = animationDatas[i].endPosition;
                    currentPosition = animationDatas[i].startPosition;
                    deltaRotation = animationDatas[i].endRotation;
                    currentRotation = animationDatas[i].startRotation;
                    duration = animationDatas[i].duration;

                    float h = 0;

                    if (i > 0)
                    {
                        h = animationDatas[i - 1].duration / totalDuration;
                        currentSlider = (sliderValue - h) * (totalDuration / (animationDatas[i].duration - animationDatas[i - 1].duration));
                    }
                    else
                    {
                        currentSlider = sliderValue / (duration / totalDuration);
                    }
                    break;
                }
            }

            LerpTransform();
        }

        void LerpTransform()
        {
            if (globalCoordinate)
            {
                //parentRotation.transform.position = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);
                transform.position = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);
                this.transform.eulerAngles = Vector3.Lerp(currentRotation, deltaRotation, currentSlider);
            }
            else
            {
                Vector3 currPositionTemp = currentPosition - getStartPosition;
                Vector3 deltaPositionTemp = deltaPosition - getStartPosition;

                //if (parentRotation != null)
                //{
                //    if (parentPosition != null)
                //        parentRotation.transform.localPosition = Vector3.Lerp(currPositionTemp, deltaPositionTemp, currentSlider);
                //    else
                //        parentRotation.transform.localPosition = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);
                //}

                transform.localPosition = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);
                this.transform.localEulerAngles = Vector3.Lerp(currentRotation, deltaRotation, currentSlider);
            }
        }

        private void PlayAnimation()
        {
            if (lerpValue < 1)
            {
                lerpValue += (Time.deltaTime * currentSpeed) / totalDurationTemp;
                SetSliderPercentage(Mathf.Lerp(currentLerpValue, maxSliderValue, lerpValue));
            }
            else
            {
                lerpValue = 0;
                isAnimating = false;
                if (getVBOutputData.isDisabledFinished && sliderValue >= 1)
                {
                    gameObject.SetActive(false);
                }

            }
        }

        //void InstantiateRotationParent()
        //{
        //    parentPosition = new GameObject("parent rotation temp");
        //    parentPosition.transform.position = transform.position;
        //    parentOldStartPosition = transform.parent.position;

        //    Vector3 localEuler = transform.localEulerAngles;

        //    transform.localEulerAngles = Vector3.zero;
        //    Vector3 eulerZero = transform.eulerAngles;

        //    parentPosition.transform.eulerAngles = eulerZero;

        //    if (VirtualTrainingSceneManager.GameObjectRoot.gameObject != null)
        //        parentPosition.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.gameObject.transform);
        //    if (parentOld == null)
        //        parentOld = transform.parent;

        //    transform.localEulerAngles = localEuler;

        //    parentRotation = new GameObject("parent");
        //    parentRotation.transform.SetParent(parentPosition.transform);
        //    parentRotation.transform.localPosition = Vector3.zero;
        //    parentRotation.transform.localEulerAngles = transform.localEulerAngles;

        //    transform.SetParent(parentRotation.transform);
        //    transform.localEulerAngles = Vector3.zero;
        //    transform.gameObject.SetActive(true);
        //    if (durationAnimationsBackward.Count > 0 && isReverse)
        //    {
        //        if (globalCoordinate)
        //        {
        //            parentRotation.transform.position = durationAnimationsBackward[0].startPosition - getStartPosition;
        //            transform.eulerAngles = durationAnimationsBackward[0].startRotation;

        //            transform.localPosition = Vector3.zero;
        //        }
        //        else
        //        {
        //            parentRotation.transform.localPosition = durationAnimationsBackward[0].startPosition - getStartPosition;
        //            transform.localEulerAngles = durationAnimationsBackward[0].endRotation;

        //            transform.localPosition = Vector3.zero;
        //        }
        //        transform.gameObject.SetActive(false);
        //    }
        //}


        public override void StopAnimation()
        {
            base.StopAnimation();
            transform.localPosition = getStartPosition;
            transform.localEulerAngles = getStartRotation;
            isAnimating = false;
        }

        public override void PauseAnimation()
        {
            base.StopAnimation();
            isAnimating = false;
        }

        public override void ResetAnimation()
        {
            base.ResetAnimation();
            if (isReverse && getVBOutputData.isDisabledFinished == false)
            {
                if (sliderValue <= 0)
                    this.gameObject.SetActive(false);
                else
                    this.gameObject.SetActive(true);
            }
            else
            {
                //this.gameObject.SetActive(true);
            }


            isAnimating = false;
        }

        public override void DestroyItself()
        {
            SetSliderPercentage(0);
            //if (parentPosition != null)
            //{
            //    if (parentOld.name != "parent")
            //        transform.SetParent(parentOld);

            //    Destroy(parentPosition.gameObject);
            //}
            transform.localPosition = getStartPosition;
            transform.localEulerAngles = getStartRotation;

            base.DestroyItself();
        }
    }
}

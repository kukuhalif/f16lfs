using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimation : MonoBehaviour
{
    public Vector3 rotationValue;
    Vector3 currentRotation;

    bool rotating;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            rotating = true;

        if (Input.GetKeyDown(KeyCode.D))
            rotating = false;

        if (rotating)
            rotate();
    }

    void rotate()
    {
        //transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, rotationValue, Time.deltaTime);
        transform.RotateAround(transform.position, Vector3.up, 20 * Time.deltaTime);
    }
}

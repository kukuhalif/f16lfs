using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class AnimationPlayerVB : AnimationPlayerBase
    {
        bool isAnimating;
        bool isDragAnimating;
        float t;
        float duration;
        float x;
        float y;
        float sliderSequence;
        // Start is called before the first frame update
        void Start()
        {
            t = 0;
        }

        public float getDuration()
        {
            float durationTemp = 0;
            if ((getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click) ||
                (getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
            {
                durationTemp = CalculateDurationSequence();
            }
            else
                durationTemp = CalculateDurationOnce();

            return durationTemp;
        }

        // Update is called once per frame
        void Update()
        {
            AnimatingDragTwoAxis();
            CheckSliderValue();
        }
        void CheckSliderValue()
        {
            if ((getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click) ||
            (getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
            {
                for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
                {
                    if (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation ||
                    getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState ||
                    getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter)
                    {
                        if (getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue > 0 && getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue < 1)
                        {
                            float x = 0;
                            x =  (float)i / (float)getvirtualButtonBase.animationPlayerContainerInteraction.Count;
                            sliderSequence = x + (getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue / getvirtualButtonBase.animationPlayerContainerInteraction.Count);
                            SetSliderPercentage(sliderSequence);
                        }
                    }
                    else
                    {
                        if (i == 0)
                        {
                            SetSliderPercentage(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue);
                        }
                        else
                        {
                            if (getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue < sliderValue)
                            {
                                SetSliderPercentage(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue);
                            }
                        }
                    }
                }

            }
            else
            {
                for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
                {
                    if (i == 0)
                    {
                        SetSliderPercentage(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue);
                    }
                    else
                    {
                        if (getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue < sliderValue)
                        {
                            SetSliderPercentage(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue);
                        }
                    }
                }
            }
        }
        public override void StartPlayAnimation(float destinationValue, float speed)
        {
            maxSliderValue = destinationValue;
            switch (getvirtualButtonBase.virtualButtonDatas.vbType)
            {
                case VirtualButtonType.Click:
                    getvirtualButtonBase.virtualButtonDatas.isBack = true;
                    VirtualButtonClick vbClick = getvirtualButtonBase as VirtualButtonClick;
                    vbClick.maxAnimationValue = destinationValue;
                    break;
                case VirtualButtonType.Drag:
                    isDragAnimating = true;
                    break;
                case VirtualButtonType.Press:

                    break;
            }

            if ((getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click) ||
                    (getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
            {
                //StartCoroutine(TriggerVirtualButtonSequence(destinationValue));
                getvirtualButtonBase.TriggerVirtualButton(destinationValue);
            }
            else
                getvirtualButtonBase.TriggerVirtualButton(destinationValue);
        }

        IEnumerator TriggerVirtualButtonSequence(float destinationValue)
        {
            for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
            {


                if ((getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampParameter) ||
                    (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation) ||
                    (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampState))
                {
                    if (getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click)
                    {
                        getvirtualButtonBase.TriggerVirtualButton(destinationValue);
                        for (int j = 1; j < getvirtualButtonBase.virtualButtonDatas.sequentialClickDatas.Count; j++)
                        {

                            yield return new WaitUntil(() => getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue
                            >= getvirtualButtonBase.virtualButtonDatas.sequentialClickDatas[j].durations);

                            if (j < getvirtualButtonBase.virtualButtonDatas.sequentialClickDatas.Count - 1)
                                getvirtualButtonBase.TriggerVirtualButton(destinationValue);
                        }
                    }
                    else
                    {
                        getvirtualButtonBase.TriggerVirtualButton(destinationValue);
                        for (int j = 0; j < getvirtualButtonBase.virtualButtonDatas.sequentialDragDatas.Count; j++)
                        {
                            yield return new WaitUntil(() => getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue
                            >= getvirtualButtonBase.virtualButtonDatas.sequentialDragDatas[j].durations);

                            if (j < getvirtualButtonBase.virtualButtonDatas.sequentialDragDatas.Count - 1)
                                getvirtualButtonBase.TriggerVirtualButton(destinationValue);
                        }
                    }

                    yield return null;
                }
                else
                {
                    getvirtualButtonBase.TriggerVirtualButton(destinationValue);
                    if (getvirtualButtonBase.animationPlayerContainerInteraction[i] != null)
                        yield return new WaitUntil(() => getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue >= 0.96f);
                    else
                        yield return null;
                }
            }
        }

        public override void StopAnimation()
        {
            base.StopAnimation();
            getvirtualButtonBase.StopVirtualButton();
            for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
            {
                getvirtualButtonBase.animationPlayerContainerInteraction[i].SetSliderPercentage(0);
            }
        }

        public override void PauseAnimation()
        {
            base.StopAnimation();
            getvirtualButtonBase.StopVirtualButton();
        }
        public override void AnimatingDragTwoAxis()
        {
            if (isDragAnimating && getvirtualButtonBase != null)
            {
                t += Time.deltaTime * getvirtualButtonOnAnimation.speedVirtualButton / 5;
                VirtualButtonDrag vbDrag = getvirtualButtonBase as VirtualButtonDrag;
                if (getvirtualButtonOnAnimation.isOverride)
                {
                    x = Mathf.Lerp(getvirtualButtonOnAnimation.startValue, getvirtualButtonOnAnimation.endValue, t);
                    y = Mathf.Lerp(getvirtualButtonOnAnimation.startValueY, getvirtualButtonOnAnimation.endValueY, t);
                }
                else
                {
                    x = Mathf.Lerp(getvirtualButtonBase.virtualButtonDatas.defaultValue, 1, t);
                    y = Mathf.Lerp(getvirtualButtonBase.virtualButtonDatas.defaultValue, 1, t);
                }

                vbDrag.SetDeltaPositionAnimation(x, y);
            }
        }


        float CalculateDurationOnce()
        {
            duration = 0;
            for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
            {
                switch (getvirtualButtonBase.virtualButtonDatas.vbType)
                {
                    case VirtualButtonType.Click:
                        if (i == 0)
                        {
                            duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                        }
                        else
                        {
                            if (GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]) > duration)
                                duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                        }

                        break;
                    case VirtualButtonType.Drag:
                        if (i == 0)
                        {
                            if (getvirtualButtonOnAnimation.isOverride)
                                duration = (getvirtualButtonOnAnimation.endValue - getvirtualButtonOnAnimation.startValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;
                            else
                                duration = (1 - getvirtualButtonBase.virtualButtonDatas.defaultValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;
                        }
                        else
                        {
                            float durationTemp;
                            if (getvirtualButtonOnAnimation.isOverride)
                                durationTemp = (getvirtualButtonOnAnimation.endValue - getvirtualButtonOnAnimation.startValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;
                            else
                                durationTemp = (1 - getvirtualButtonBase.virtualButtonDatas.defaultValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;

                            if (durationTemp > duration)
                            {
                                duration = durationTemp;
                            }
                        }
                        break;
                    case VirtualButtonType.Press:
                        if (i == 0)
                        {
                            duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                        }
                        else
                        {
                            if (GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]) > duration)
                                duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                        }
                        break;
                }
            }
            return duration;
        }

        float GetDuration(AnimationPlayerBase animBase)
        {
            float durationTemp = 0;
            if (animBase.isAnimatedScript)
            {
                AnimationPlayerScript script = animBase as AnimationPlayerScript;
                durationTemp = script.GetTotalDuration();
            }
            else
            {
                durationTemp = animBase.getAnimator().GetCurrentAnimatorStateInfo(0).length;
            }
            return durationTemp;
        }

        float GetDurationOnParameter(AnimationPlayerBase animBase)
        {
            float durationTemp = 0;
            for (int i = 0; i < animBase.getVBOutputData.parameterDatas.Count; i++)
            {
                if (i == 0)
                {
                    float defaultValueTemp = animBase.getVBOutputData.parameterDatas[i].minimalValue + ((animBase.getVBOutputData.parameterDatas[i].maximalValue - animBase.getVBOutputData.parameterDatas[i].minimalValue) * animBase.getVBDataModel.defaultValue);
                    durationTemp = (animBase.getVBOutputData.parameterDatas[i].maximalValue - defaultValueTemp) / animBase.getVBOutputData.speed;
                }
                else
                {
                    float defaultValueTemp = animBase.getVBOutputData.parameterDatas[i].minimalValue + ((animBase.getVBOutputData.parameterDatas[i].maximalValue - animBase.getVBOutputData.parameterDatas[i].minimalValue) * animBase.getVBDataModel.defaultValue);
                    float temp = (animBase.getVBOutputData.parameterDatas[i].maximalValue - defaultValueTemp) / animBase.getVBOutputData.speed; ;
                    if (durationTemp < temp)
                    {
                        durationTemp = temp;
                    }
                }
            }
            return durationTemp;
        }
        float CalculateDurationSequence()
        {
            float durationTemporary = 0;
            if (getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click)
            {
                if (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation ||
                    getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState)
                {
                    for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
                    {
                        durationTemporary += GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                    }
                }
                else if (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter ||
                    getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
                {
                    for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
                    {
                        durationTemporary += GetDurationOnParameter(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                    }
                }
                else
                {
                    float durationTemp = 0;
                    for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
                    {
                        if (i == 0)
                        {
                            durationTemp = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                        }
                        else
                        {
                            if (durationTemp < GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]))
                            {
                                durationTemp = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                            }
                        }
                    }
                    durationTemporary += durationTemp;
                }
            }
            else if ((getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
            {
                if (getvirtualButtonOnAnimation.isOverride == true)
                    durationTemporary = (getvirtualButtonOnAnimation.endValue - getvirtualButtonOnAnimation.startValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;
                else
                    durationTemporary = 1 - (getvirtualButtonBase.virtualButtonDatas.defaultValue) / (getvirtualButtonOnAnimation.speedVirtualButton / 5);
            }
            return durationTemporary;
        }

        public override void ResetAnimation()
        {
            base.ResetAnimation();
        }
        public override void DestroyItself()
        {
            //base.DestroyItself();
            //getvirtualButtonBase.DestroyThis();
        }
    }
}

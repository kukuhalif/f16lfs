﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.UI;
using System.Linq;
using System;
namespace VirtualTraining.Feature
{
    public partial class AnimationManager : MonoBehaviour
    {
        [SerializeField] VirtualButtonManager virtualButtonManager;
        public List<AnimationPlayerBase> animationPlayers = new List<AnimationPlayerBase>();

        public List<GameObject> gameObjectSequence = new List<GameObject>();
        [SerializeField] public static bool isSequenceAnimation;
        [Range(0, 1)]
        public float sliderValue;
        public bool isPlaying;

        [SerializeField] Slider slider;
        [Range(0, 1)]
        [SerializeField] float forwardPreviousValue = 0.1f;
        float differenceValue;
        float maxDuration;
        public List<int> sequentialValues = new List<int>();
        int animationCount = 0;
        bool isAnimate;
        [SerializeField] List<GameObject> resetParent = new List<GameObject>();
        [SerializeField] List<float> durationSequenceAnimation = new List<float>();
        [SerializeField] List<AnimationQueueData> animationQueueDatas = new List<AnimationQueueData>();
        float elapsedTime;
        float speed;
        bool isReset;
        bool isReverseAnimationOnReset;

        Action resetAnimationCallback;
        Action afterAnimationCallback;

        const float MIN_VALUE = 0.01f;
        const float MAX_VALUE = 0.99f;

        [SerializeField] List<AnimationFigureData> animationsDontReset = new List<AnimationFigureData>();
        public List<AnimationPlayerBase> animationPlayersPreviously = new List<AnimationPlayerBase>();


        public void SetSlider(Slider slider, Action resetAnimationCallback, Action afterAnimationCallback)
        {
            this.slider = slider;
            this.resetAnimationCallback = resetAnimationCallback;
            this.afterAnimationCallback = afterAnimationCallback;
        }

        [System.Serializable]
        public class AnimationQueueData
        {
            public int queueSequence;
            public float animationDuration;
            public float sequenceAnimation;
            public bool isTheLongest;
            public AnimationPlayerBase animationPlayerBase;

            public AnimationQueueData(AnimationPlayerBase animationPlayerBase, int queue, float duration)
            {
                queueSequence = queue;
                animationDuration = duration;
                sequenceAnimation = 99;
                this.animationPlayerBase = animationPlayerBase;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            EventManager.AddListener<AnimationPlayEvent>(StartAnimationListener);
            EventManager.AddListener<AnimationSequenceDone>(PlayAnimationSequence);
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<AnimationPlayEvent>(StartAnimationListener);
            EventManager.RemoveListener<AnimationSequenceDone>(PlayAnimationSequence);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            afterAnimationCallback = null;
            resetAnimationCallback = null;
        }

        void ApplySettingListener(ApplySettingEvent e)
        {
            speed = e.data.cameraTransitionSpeed;
        }

        void StartAnimationListener(AnimationPlayEvent e)
        {
            if (slider == null)
                return;

            if (e.animationData == null)
            {
                elapsedTime = 0;
                AddedToDontReset();
                if (sliderValue != slider.minValue)
                {
                    isReset = true;
                }
                else
                {
                    ResetAnimation();
                    if (resetAnimationCallback != null)
                        resetAnimationCallback();
                    animationCount = 0;
                    isAnimate = false;
                }
                Debug.Log("reset animation");
                return;
            }
            else
            {
                isReverseAnimationOnReset = e.animationData.isReverseAnimationOnReset;

                if (e.isResetAnimation == true)
                {
                    if (e.animationData.ignoreIfAlreadyPlayed)
                    {
                        //if (CheckedDontResetAnimFigurData(e.animationData) == false && CheckedAnimationInAnimationPlayer(e.animationData) == true)
                        animationsDontReset.Add(e.animationData);
                    }
                    return;
                }
            }

            if (CheckedDontResetAnimFigurData(e.animationData) == true)
                return;

            if (e.animationData.animationType == AnimationType.AnimVB && e.animationData.virtualButton.virtualButtonElementType.GetData() != null)
            {
                //e.animationData.virtualButton.virtualButtonElementType.GetData().defaultValue = e.animationData.virtualButton.startValue;
                VirtualButtonBase vbBase = virtualButtonManager.InitVBAnimation(e.animationData.virtualButton.virtualButtonElementType.GetData());
                AnimationPlayerVB animBase = vbBase.gameObject.AddComponent<AnimationPlayerVB>();
                animBase.SetupVBinAnimation(e.animationData.virtualButton, vbBase, true);
                animationPlayers.Add(animBase);
                sequentialValues.Add(e.animationData.sequentialQueue);
                animationCount++;
            }
            else
            {
                if (e.animationData.animationType == AnimationType.AnimScript)
                {
                    if (e.animationData.gameObjectInteraction.gameObject != null)
                    {
                        if (e.animationData.gameObjectInteraction.gameObject.activeInHierarchy)
                        {
                            EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(e.animationData.gameObjectInteraction.gameObject, true));
                        }
                        else
                        {
                            EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(e.animationData.gameObjectInteraction.gameObject, false));
                        }
                        //GetParentOfObject(e.animationData.gameObjectInteraction.gameObject);

                        AnimationPlayerScript animScript = e.animationData.gameObjectInteraction.gameObject.AddComponent<AnimationPlayerScript>();
                        animScript.SetupAnimation(e.animationData);

                        animationPlayers.Add(animScript);
                        sequentialValues.Add(e.animationData.sequentialQueue);
                        animationCount++;
                    }
                    else
                        Debug.Log("game object interaction kosong");

                }
                else if (e.animationData.animationType == AnimationType.AnimManual)
                {
                    if (e.animationData.animatorController.gameObject != null)
                    {
                        resetParent.Add(e.animationData.animatorController.gameObject);

                        AnimationPlayerManual animManual = e.animationData.animatorController.gameObject.AddComponent<AnimationPlayerManual>();
                        animManual.SetAnimName(e.animationData.animationName, e.animationData.animationSpeedParameter, e.animationData.animationLayer, true);

                        animationPlayers.Add(animManual);
                        sequentialValues.Add(e.animationData.sequentialQueue);
                        animationCount++;
                    }
                    else
                        Debug.Log("game object animator kosong");
                }

            }
        }

        public void PlayAnimationSequence(AnimationSequenceDone e)
        {
            StartCoroutine(WaitFoCalculateMaxDuration());
            StartCoroutine(PlaySequenceDelay());
        }
        IEnumerator WaitFoCalculateMaxDuration()
        {
            yield return new WaitUntil(() => animationCount == sequentialValues.Count);
            CalculateMaxDuration();
        }

        IEnumerator PlaySequenceDelay()
        {
            yield return new WaitUntil(() => animationCount == sequentialValues.Count);
            resetParent = resetParent.Distinct().ToList();
            if (animationPlayers.Count > 0)
            {
                if (isSequenceAnimation)
                {
                    StartCoroutine(PlaySequenceAnimation());
                }
                else
                {
                    for (int i = 0; i < animationPlayers.Count; i++)
                    {
                        animationPlayers[i].StartPlayAnimation(1, animationPlayers[i].getVBOutputData.speed);
                    }
                }
                isPlaying = true;
                sliderValue = 0;
                differenceValue = slider.maxValue - slider.minValue;
            }
            isAnimate = true;
        }

        IEnumerator PlaySequenceAnimation()
        {
            //CalculateMaxDuration();
            int maxSequenceValueTemp = 0;
            for (int i = 0; i < sequentialValues.Count; i++)
            {
                if (sequentialValues[i] > maxSequenceValueTemp)
                    maxSequenceValueTemp = sequentialValues[i];
            }
            for (int i = 0; i < maxSequenceValueTemp + 1; i++)
            {
                for (int j = 0; j < sequentialValues.Count; j++)
                {
                    if (sequentialValues[j] == i && animationPlayers[j] != null)
                    {
                        if (animationPlayers[j].gameObject.activeInHierarchy == false)
                        {
                            animationPlayers[j].gameObject.SetActive(true);
                            yield return new WaitForEndOfFrame();
                        }
                        else
                            animationPlayers[j].StartPlayAnimation(1, animationPlayers[j].getVBOutputData.speed); //reset Animation slider value
                    }
                }

                for (int k = 0; k < sequentialValues.Count; k++)
                {
                    if (sequentialValues[k] == i)
                    {
                        if (animationPlayers[k] != null)
                        {
                            yield return new WaitUntil(() => animationPlayers[k].sliderValue >= MAX_VALUE);
                        }
                    }
                }
            }
        }

        void CalculateMaxDuration()
        {
            maxDuration = 0;
            animationQueueDatas.Clear();
            //Debug.Log("calculateee");
            for (int i = 0; i < animationPlayers.Count; i++)
            {
                // get duration virtual button
                if (animationPlayers[i].getvirtualButtonBase != null)
                {
                    //return;
                    AnimationPlayerVB animVB = animationPlayers[i] as AnimationPlayerVB;
                    animationQueueDatas.Add(new AnimationQueueData(animationPlayers[i], sequentialValues[i], animVB.getDuration()));
                    //maxDuration += animVB.getDuration();
                    //if (durationSequenceAnimation.Count < animationPlayers.Count)
                    //    durationSequenceAnimation.Add(maxDuration);
                }
                else
                {
                    if (animationPlayers[i].isAnimatedScript)
                    {
                        AnimationPlayerScript script = animationPlayers[i] as AnimationPlayerScript;
                        animationQueueDatas.Add(new AnimationQueueData(animationPlayers[i], sequentialValues[i], script.totalDuration));
                    }
                    else
                    {
                        AnimationPlayerManual manual = animationPlayers[i] as AnimationPlayerManual;
                        float duration = 0;
                        manual.getAnimator().SetFloat(manual.getVBOutputData.animationSpeedParameter, manual.getVBOutputData.speed);
                        manual.getAnimator().Update(0);
                        manual.getAnimator().Play(manual.getStateName, manual.getAnimLayer);
                        manual.getAnimator().Update(0);
                        if (!float.IsInfinity(manual.getAnimator().GetCurrentAnimatorStateInfo(manual.getAnimLayer).length) && !manual.getAnimator().GetCurrentAnimatorStateInfo(manual.getAnimLayer).loop)
                        {
                            //maxDuration += (manual.getAnimator().GetCurrentAnimatorStateInfo(manual.getAnimLayer).length * manual.getVBOutputData.speed);
                            duration = (manual.getAnimator().GetCurrentAnimatorStateInfo(manual.getAnimLayer).length * manual.getVBOutputData.speed);
                        }
                        else if (!manual.getAnimator().GetCurrentAnimatorStateInfo(manual.getAnimLayer).loop)
                        {
                            //maxDuration += 1;
                            duration += 1;
                        }

                        //if (durationSequenceAnimation.Count < animationPlayers.Count)
                        //    durationSequenceAnimation.Add(maxDuration);
                        animationQueueDatas.Add(new AnimationQueueData(animationPlayers[i], sequentialValues[i], duration));
                        manual.getAnimator().Play("Default", manual.getAnimLayer, 0);
                        manual.getAnimator().Update(0);
                    }
                }
            }
            animationQueueDatas.Sort(SortBySequence);
            SetupAnimationQueueData();

        }
        static int SortBySequence(AnimationQueueData p1, AnimationQueueData p2)
        {
            return p1.queueSequence.CompareTo(p2.queueSequence);
        }

        //buat ngecheck apakah value nya ada dalam list
        bool IsHasUsed(List<float> listValue, float value)
        {
            bool isUsed = false;
            if (listValue.Count <= 0)
                return isUsed;

            for (int i = 0; i < listValue.Count; i++)
            {
                if (value == listValue[i])
                {
                    isUsed = true;
                    break;
                }
            }

            return isUsed;
        }

        //buat nge check sequence value mana yang duluan
        float CheckEarlierQueue(float queue, List<float> sequenceUseds)
        {
            for (int i = 0; i < animationQueueDatas.Count; i++)
            {
                if (queue > animationQueueDatas[i].queueSequence && IsHasUsed(sequenceUseds, animationQueueDatas[i].sequenceAnimation) == false)
                    queue = animationQueueDatas[i].queueSequence;
            }

            return queue;
        }
        //untuk set animation queue data
        void SetupAnimationQueueData()
        {
            List<float> sequenceUseds = new List<float>();
            durationSequenceAnimation.Clear();
            if (durationSequenceAnimation.Count <= 0 && animationQueueDatas.Count > 0)
            {
                float queue = animationQueueDatas[0].queueSequence;

                queue = CheckEarlierQueue(queue, sequenceUseds);
                int x = 0;
                for (int i = 0; i < animationQueueDatas.Count; i++)
                {
                    if (animationQueueDatas[i].queueSequence == queue)
                    {
                        animationQueueDatas[i].sequenceAnimation = 0;

                        if (durationSequenceAnimation.Count <= 0)
                        {
                            durationSequenceAnimation.Add(animationQueueDatas[0].animationDuration);
                            x = 0;
                        }
                        else
                        {
                            if (durationSequenceAnimation[0] < animationQueueDatas[i].animationDuration)
                            {
                                durationSequenceAnimation[0] = animationQueueDatas[i].animationDuration;
                                x = i;
                            }
                        }
                    }
                }
                animationQueueDatas[x].isTheLongest = true;
                sequenceUseds.Add(0);
            }

            for (int i = 0; i < animationQueueDatas.Count; i++)
            {
                if (IsHasUsed(sequenceUseds, animationQueueDatas[i].sequenceAnimation) == false)
                {
                    float queue = animationQueueDatas[i].queueSequence;
                    queue = CheckEarlierQueue(queue, sequenceUseds);

                    int totalDurationSequence = durationSequenceAnimation.Count;
                    int x = 0;
                    for (int k = 0; k < animationQueueDatas.Count; k++)
                    {
                        if (animationQueueDatas[k].queueSequence == queue)
                        {
                            if (durationSequenceAnimation.Count == totalDurationSequence)
                            {
                                durationSequenceAnimation.Add(animationQueueDatas[k].animationDuration);
                                x = k;
                            }
                            else
                            {
                                if (durationSequenceAnimation[totalDurationSequence] < animationQueueDatas[k].animationDuration)
                                {
                                    durationSequenceAnimation[totalDurationSequence] = animationQueueDatas[k].animationDuration;
                                    x = k;
                                }
                            }

                            animationQueueDatas[k].sequenceAnimation = durationSequenceAnimation.Count - 1;
                        }
                    }
                    animationQueueDatas[x].isTheLongest = true;
                    sequenceUseds.Add(animationQueueDatas[i].sequenceAnimation);
                }
            }

            for (int i = 0; i < durationSequenceAnimation.Count; i++)
            {
                maxDuration += durationSequenceAnimation[i];
            }
        }

        private void Update()
        {
            if (isAnimate)
            {
                CheckAnimation();
                CheckSlider();
            }

            if (isReset)
            {
                if (isReverseAnimationOnReset)
                {
                    if (isSequenceAnimation)
                        elapsedTime += Time.deltaTime * speed;
                    else
                        elapsedTime += Time.deltaTime * (speed);

                    slider.value = Mathf.Lerp(slider.value, slider.minValue + MIN_VALUE, elapsedTime);
                    sliderValue = slider.value;
                    ValueChanged();

                    if (elapsedTime >= 1f)
                    {
                        isReset = false;
                        ResetAnimation();
                        animationCount = 0;
                        isAnimate = false;
                        if (resetAnimationCallback != null)
                            resetAnimationCallback();
                    }
                }
                else
                {
                    isReset = false;
                    ResetAnimation();
                    animationCount = 0;
                    isAnimate = false;
                    if (resetAnimationCallback != null)
                        resetAnimationCallback();
                }
            }
        }

        void CheckAnimation()
        {
            if (isSequenceAnimation && isPlaying)
            {
                float y = 0;
                for (int i = 0; i < animationQueueDatas.Count; i++)
                {
                    if (animationQueueDatas[i].isTheLongest)
                    {
                        y += animationQueueDatas[i].animationPlayerBase.sliderValue * (animationQueueDatas[i].animationDuration / maxDuration);
                    }
                }
                sliderValue = y;
            }
            else // bukan sequential
            {
                for (int i = 0; i < animationPlayers.Count; i++)
                {
                    if (i == 0)
                    {
                        sliderValue = animationPlayers[i].sliderValue;
                    }
                    else
                    {
                        if (animationPlayers[i].sliderValue < sliderValue)
                        {
                            sliderValue = animationPlayers[i].sliderValue;
                        }
                    }
                }
            }
        }

        public void ResetAnimation()
        {
            DestroyPreviouslyNotUse();
            sequentialValues.Clear();
            StopAllCoroutines();
            for (int i = 0; i < animationPlayers.Count; i++)
            {
                if (CheckDontResetObject(animationPlayers[i]) == false)
                    animationPlayers[i].DestroyItself();
            }
            animationPlayers.Clear();
            differenceValue = 0;
            maxDuration = 0;
            isPlaying = false;

            resetParent = resetParent.Distinct().ToList();
            //for (int i = 0; i < resetParent.Count; i++)
            //{
            //    resetParent[i].SetActive(false);
            //    resetParent[i].SetActive(true);
            //}
            resetParent.Clear();
            animationQueueDatas.Clear();
            durationSequenceAnimation.Clear();
            animationsDontReset.Clear();
        }

        //tambahin di komponen slider
        public void ValueChanged()
        {
            if (!isPlaying)
            {
                if (isSequenceAnimation)
                {
                    CheckPauseSequenceAnimation(true);
                }
                else
                {
                    for (int i = 0; i < animationPlayers.Count; i++)
                    {
                        if (animationPlayers[i] != null)
                        {
                            if (isReset == true)
                            {
                                if (CheckDontResetObject(animationPlayers[i]) == false)
                                {
                                    animationPlayers[i].StartPlayAnimation(sliderValue, animationPlayers[i].getVBOutputData.speed);
                                    animationPlayers[i].SetSliderPercentage(sliderValue);
                                }
                            }
                            else
                            {
                                animationPlayers[i].StartPlayAnimation(sliderValue, animationPlayers[i].getVBOutputData.speed);
                                animationPlayers[i].SetSliderPercentage(sliderValue);
                            }
                        }
                    }
                }
            }
        }

        void CheckSlider()
        {
            if (animationPlayers.Count > 0)
            {
                if (isPlaying)
                    slider.value = sliderValue * differenceValue;
                else
                    sliderValue = slider.value / differenceValue;

                if (isPlaying == false && slider.value >= slider.maxValue - MIN_VALUE)
                {
                    slider.value = slider.maxValue / differenceValue;
                    if (afterAnimationCallback != null)
                        afterAnimationCallback();
                }

                if (sliderValue >= MAX_VALUE)
                {
                    isPlaying = false;
                }
            }
        }
        public void ClickHandle()
        {
            isPlaying = false;
        }

        public void PlayAnimation()
        {
            isAnimate = false;
            if (slider.value >= slider.maxValue || slider.value <= slider.minValue)
            {
                slider.value = slider.minValue;
                sliderValue = 0;
                for (int i = 0; i < animationPlayers.Count; i++)
                {
                    animationPlayers[i].StopAnimation();
                    animationPlayers[i].SetSliderPercentage(0);
                }
                StartCoroutine(delayPlaySequenceAnimation());

            }
            isPlaying = !isPlaying;

            //pause condition
            if (sliderValue < 1 && sliderValue > 0)
            {
                if (isPlaying)
                {
                    if (isSequenceAnimation)
                    {
                        CheckPauseSequenceAnimation();
                        StartCoroutine(PlaySequenceAnimation());
                    }
                    else
                    {
                        for (int i = 0; i < animationPlayers.Count; i++)
                        {
                            animationPlayers[i].SetSliderPercentage(sliderValue);
                            animationPlayers[i].StartPlayAnimation(1, animationPlayers[i].getVBOutputData.speed);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < animationPlayers.Count; i++)
                    {
                        animationPlayers[i].PauseAnimation();
                    }
                }
            }
            StartCoroutine(DelayPlayAnimation());
        }

        void CheckPauseSequenceAnimation(bool DragWhenPause = false)
        {
            for (int i = 0; i < animationPlayers.Count; i++)
            {
                //animationPlayers[i].StopAnimation();
                animationPlayers[i].SetSliderPercentage(0);
            }

            float durationTemp = sliderValue * maxDuration;
            float durationSequenceQueue = 0;
            float maxDur = 0;

            for (int j = 0; j < durationSequenceAnimation.Count; j++)
            {
                maxDur += durationSequenceAnimation[j];
                if (maxDur > durationTemp)
                {
                    durationSequenceQueue = j;
                    break;
                }
            }
            maxDur = 0;
            for (int i = 0; i < animationQueueDatas.Count; i++)
            {
                if (animationQueueDatas[i].queueSequence < durationSequenceQueue)
                {
                    animationQueueDatas[i].animationPlayerBase.SetSliderPercentage(1);
                    animationQueueDatas[i].animationPlayerBase.ResetAnimation();
                }
                else if (animationQueueDatas[i].queueSequence == durationSequenceQueue)
                {
                    for (int j = 0; j < durationSequenceQueue; j++)
                    {
                        maxDur += durationSequenceAnimation[j];
                    }

                    float currentSliderVal = sliderValue - (maxDur / maxDuration);
                    float currentSliderValue = currentSliderVal / (animationQueueDatas[i].animationDuration / maxDuration);
                    animationQueueDatas[i].animationPlayerBase.SetSliderPercentage(currentSliderValue);
                    if (DragWhenPause == true)
                    {
                        if (isReset && !animationQueueDatas[i].animationPlayerBase.gameObject.activeInHierarchy)
                        {
                            //reset object at ondestroy
                        }
                        else
                        {
                            animationQueueDatas[i].animationPlayerBase.StartPlayAnimation(currentSliderValue,
                                                            animationQueueDatas[i].animationPlayerBase.getVBOutputData.speed);
                        }
                    }

                    if (sliderValue <= 0 || sliderValue >= 1)
                    {
                        animationQueueDatas[i].animationPlayerBase.ResetAnimation();
                    }
                }
                else
                {
                    animationQueueDatas[i].animationPlayerBase.SetSliderPercentage(0);
                    animationQueueDatas[i].animationPlayerBase.ResetAnimation();
                }
            }
        }

        IEnumerator DelayPlayAnimation()
        {
            yield return new WaitForSeconds(0.1f);
            isAnimate = true;
        }

        IEnumerator delayPlaySequenceAnimation()
        {
            yield return null;
            if (isSequenceAnimation)
            {
                StartCoroutine(PlaySequenceAnimation());
            }
            else
            {
                for (int i = 0; i < animationPlayers.Count; i++)
                {
                    animationPlayers[i].StartPlayAnimation(1, animationPlayers[i].getVBOutputData.speed);
                }
            }
        }

        public void ForwardAnimation()
        {
            ClickHandle();
            slider.value += (slider.maxValue - slider.minValue) * forwardPreviousValue;
            if (slider.value > slider.maxValue)
                slider.value = slider.maxValue;
        }

        public void PreviousAnimation()
        {
            ClickHandle();
            slider.value -= (slider.maxValue - slider.minValue) * forwardPreviousValue;
            if (slider.value < slider.minValue)
                slider.value = slider.minValue;
        }

        public void BeginAnimation()
        {
            slider.value = slider.minValue + MIN_VALUE;
            sliderValue = slider.value;
            ClickHandle();
            ValueChanged();
        }

        public void LastAnimation()
        {
            slider.value = slider.maxValue - MIN_VALUE;
            sliderValue = slider.value;
            ClickHandle();
            ValueChanged();
        }

        void GetParentOfObject(GameObject currentObject)
        {
            List<Animator> animators = new List<Animator>();
            animators = currentObject.transform.GetComponentsInAllParent<Animator>();
            for (int i = 0; i < animators.Count; i++)
            {
                resetParent.Add(animators[i].gameObject);
            }
        }
    }
}

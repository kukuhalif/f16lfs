using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VirtualTraining.Feature
{
    public class CullingManager : MonoBehaviour
    {
#if UNITY_EDITOR
        [MenuItem("Virtual Training Shortcut/Enable All Culled Object &K", false)]
        private static void EnabledCulledObject()
        {
            CullingManager cm = FindObjectOfType<CullingManager>();
            if (cm == null)
                return;

            cm.EnableAllCulled(true);
        }

        [MenuItem("Virtual Training Shortcut/Disable All Culled Object &L", false)]
        private static void DisabledCulledObject()
        {
            CullingManager cm = FindObjectOfType<CullingManager>();
            if (cm == null)
                return;

            cm.EnableAllCulled(false);
        }

        [MenuItem("Virtual Training Shortcut/Enable All Culled Object &K", true)]
        static bool EnabledCulledObjectValidation()
        {
            return Application.isPlaying;
        }

        [MenuItem("Virtual Training Shortcut/Disable All Culled Object &L", true)]
        static bool DisableCulledObjectValidation()
        {
            return Application.isPlaying;
        }
#endif

        private List<CullingDataModel> cullings = new List<CullingDataModel>();
        private string currentFigure;
        private List<GameObject> culledObjects = new List<GameObject>();

        private void Start()
        {
            // todo : culling jangan dipakai dulu karena ada event yang berubah
            Destroy(gameObject);

            //EventManager.AddListener<ObjectSelectionEvent>(ObjectSelectionListener);
            //EventManager.AddListener<SceneReadyEvent>(InitDoneListener);
            //EventManager.AddListener<UpdateFigurePanelEvent>(PlayFigureListener);
            //EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            //VirtualTrainingSceneManager.SetIsObjectCulledCallback(IsObjectCulled);
        }

        private void OnDestroy()
        {
            //EventManager.RemoveListener<ObjectSelectionEvent>(ObjectSelectionListener);
            //EventManager.RemoveListener<UpdateFigurePanelEvent>(PlayFigureListener);
            //EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void InitDoneListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(InitDoneListener);

            StartCoroutine(Initialization());
        }

        IEnumerator Initialization()
        {
            yield return null;

            List<CullingTreeElement> datas = DatabaseManager.GetCullingDatas();
            for (int i = 0; i < datas.Count; i++)
            {
                if (datas[i].data != null && datas[i].data.covers.Count > 0 && datas[i].data.culleds.Count > 0)
                {
                    CullingDataModel newCdm = new CullingDataModel();

                    for (int co = 0; co < datas[i].data.covers.Count; co++)
                    {
                        Collider[] collider = datas[i].data.covers[co].gameObject.transform.GetAllComponentsInChilds<Collider>();
                        for (int re = 0; re < collider.Length; re++)
                        {
                            if (collider[re] != null)
                            {
                                var cullingCover = collider[re].gameObject.AddComponent<CullingCover>();
                                cullingCover.Setup(CameraEnterCallback, CameraExitCallback);

                                GameObjectType got = new GameObjectType(collider[re].GetComponent<GameObjectReference>().Id);
                                newCdm.covers.Add(got);
                            }
                        }
                    }

                    for (int cu = 0; cu < datas[i].data.culleds.Count; cu++)
                    {
                        Collider[] collider = datas[i].data.culleds[cu].gameObject.transform.GetAllComponentsInChilds<Collider>();
                        for (int re = 0; re < collider.Length; re++)
                        {
                            if (collider[re] != null)
                            {
                                GameObjectType got = new GameObjectType(collider[re].GetComponent<GameObjectReference>().Id);
                                newCdm.culleds.Add(got);
                            }
                        }
                    }

                    cullings.Add(newCdm);
                }
            }

            yield return new WaitForSeconds(1f);

            if (cullings.Count == 0)
                Destroy(gameObject);
            else
                EnableAllCulled(false);
        }

        private void PlayFigureListener(UpdateFigurePanelEvent e)
        {
            currentFigure = e.name;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            currentFigure = "";
        }

        private void ObjectSelectionListener(ObjectSelectionEvent e)
        {
            if (e.id == 0)
                return;

            EnableCulled(GameObjectReference.GetReference(e.id).gameObject, true);
        }

        private void CameraEnterCallback(GameObject cover)
        {
            EnableCulled(cover, true);
        }

        private void CameraExitCallback(GameObject cover)
        {
            EnableCulled(cover, false);
        }

        private void EnableCulled(GameObject movedObj, bool enabled)
        {
            // find moved cover object
            for (int i = 0; i < cullings.Count; i++)
            {
                for (int c = 0; c < cullings[i].covers.Count; c++)
                {
                    if (cullings[i].covers[c].gameObject == movedObj)
                    {
                        // enable all sibling culled objects
                        for (int d = 0; d < cullings[i].culleds.Count; d++)
                        {
                            cullings[i].culleds[d].gameObject.SetActive(enabled);

                            if (enabled)
                                culledObjects.Remove(cullings[i].culleds[d].gameObject);
                            else if (!culledObjects.Contains(cullings[i].culleds[d].gameObject))
                                culledObjects.Add(cullings[i].culleds[d].gameObject);
                        }
                        continue;
                    }
                }
            }
        }

        private void EnableAllCulled(bool enable)
        {
            for (int i = 0; i < cullings.Count; i++)
            {
                for (int c = 0; c < cullings[i].culleds.Count; c++)
                {
                    cullings[i].culleds[c].gameObject.SetActive(enable);

                    if (enable)
                        culledObjects.Remove(cullings[i].culleds[c].gameObject);
                    else if (!culledObjects.Contains(cullings[i].culleds[c].gameObject))
                        culledObjects.Add(cullings[i].culleds[c].gameObject);
                }
            }
        }

        private bool IsObjectCulled(GameObject obj)
        {
            return culledObjects.Contains(obj);
        }
    }
}

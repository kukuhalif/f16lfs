using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    // ini buat detect kamera masuk ke cover object
    public class CullingCover : MonoBehaviour
    {
        public bool moved;
        Action<GameObject> cameraEnterCallback;
        Action<GameObject> cameraExitCallback;

        public void Setup(Action<GameObject> cameraEnterCallback, Action<GameObject> cameraExitCallback)
        {
            this.cameraEnterCallback = cameraEnterCallback;
            this.cameraExitCallback = cameraExitCallback;
        }

        private void Start()
        {
            EventManager.AddListener<ObjectSelectionEvent>(ObjectSelectionListener);
            EventManager.AddListener<HideObjectEvent>(HideObjectListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ObjectSelectionEvent>(ObjectSelectionListener);
            EventManager.RemoveListener<HideObjectEvent>(HideObjectListener);
        }

        private void HideObjectListener(HideObjectEvent e)
        {
            //if (e.hide)
            //{

            //}
            //else
            //{

            //}
        }

        private void ObjectSelectionListener(ObjectSelectionEvent e)
        {
            //if (e.gameobject != null && e.gameobject == gameObject)
            //    moved = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            //if (moved)
            //    return;

            //if (other.transform == VirtualTrainingCamera.CameraTransform)
            //    cameraEnterCallback.Invoke(gameObject);
        }

        private void OnTriggerExit(Collider other)
        {
            //if (moved)
            //    return;

            //if (other.transform == VirtualTrainingCamera.CameraTransform)
            //    cameraExitCallback.Invoke(gameObject);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature.Desktop
{
    public class DesktopCameraController : CameraController
    {
        // setting
        float rotateSpeed = 5f;
        float cameraTransitionSpeed = 5f;
        float zoomSpeed = 2f;
        float dragSpeed = 5f;
        [SerializeField] Transform cameraParent;

        // state
        /// <summary>
        /// rotate or drag mode
        /// </summary>
        bool isRotateMode = true;
        bool isLocked = false;

        // default
        CameraDestination defaultDestination;

        // const
        const float TOUCH_DRAG_CONST = 0.0003f;
        const float MOUSE_DRAG_CONST = 0.001f;
        const float ROTATE_CONST = 0.05f;
        const float ROTATE_SMOOTH_SPEED_TOUCH_CONST = 10f;
        const float ROTATE_SMOOTH_SPEED_MOUSE_CONST = 30f;
        const float ZOOM_PINCH_CONST = 2f;
        const float ZOOM_SCROLL_CONST = 3f;

        // cache
        [SerializeField] Camera mainCamera; // rendering priority harus lebih besar
        [SerializeField] Camera onTopCamera; // rendering priority harus lebih kecil, agar event yang diterima tetap di main camera
        Transform anchor;
        Transform cameraTarget;
        Quaternion cameraTargetOffset;
        List<PartObject> partObjects = new List<PartObject>();
        Vector3 lastFocusedPosition;
        float touchSmoothRotateSpeed;

        // camera destination
        Vector3 destinationPosition;
        Quaternion destinationRotation;
        bool isOrthographic;
        bool isRotateFromView;
        float destinationFOV;
        float destinationOrthographicSize;
        Action arriveAction;

        // state
        bool isRotating;
        bool isDragging;
        bool isDraggingZoom;
        bool isMoving, isFromServer;
        bool crossProductZoomX;
        bool crossProductZoomY;
        bool crossProductZoomZ;
        bool isCrossProductDifferent;
        Vector2 pointerDeltaPosition;
        Vector2 pointerAxisPosition;
        Vector2 rotationAxis;
        Vector2 rotateOffset;
        Vector2 dragSmoothDelta;
        float rotationOffsetZ;
        float transitionState;
        float scrollValue;
        float dragDistance;
        float lastPinchDistance;
        float lastFOV;
        float lastOrthographicSize;
        bool[] isTouchOverUI;
        bool isZoomTouchValid;
        bool isDragTouchValid;
        Vector3 lastCameraPosition;
        Quaternion lastCameraRotation;
        int touchCount;

        [SerializeField] Transform anchorParent;

        private void Start()
        {
            // set current camera
            VirtualTrainingCamera.SetCurrentCameraController(this, false, null);

            // default camera
            defaultDestination = DatabaseManager.GetCameraData().mainCamera;
            destinationPosition = defaultDestination.position;
            destinationRotation = defaultDestination.rotation;
            isOrthographic = defaultDestination.isOrthographic;
            isRotateFromView = defaultDestination.isRotateFromView;
            destinationFOV = defaultDestination.fov;
            destinationOrthographicSize = defaultDestination.orthographicSize;

            // add input listener
            VirtualTrainingInputSystem.OnStartLeftClick += OnStartLeftClick;
            VirtualTrainingInputSystem.OnLeftClickDown += OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick += OnEndLeftClick;
            VirtualTrainingInputSystem.OnMovePointer += OnMovePointer;
            VirtualTrainingInputSystem.OnStartMiddleClick += OnStartMiddleClick;
            VirtualTrainingInputSystem.OnEndMiddleClick += OnEndMiddleClick;
            VirtualTrainingInputSystem.OnStartRightClick += OnStartRightClick;
            VirtualTrainingInputSystem.OnEndRightClick += OnEndRightClick;
            VirtualTrainingInputSystem.OnMouseScroll += OnMouseScroll;
            VirtualTrainingInputSystem.OnTouchAdded += OnTouchAdded;
            VirtualTrainingInputSystem.OnTouchRemoved += OnTouchRemoved;

            // create camera anchor
            GameObject anchor = new GameObject("camera anchor");
            anchor.transform.SetParent(transform);
            this.anchor = anchor.transform;
            this.anchor.localPosition = transform.position;
            this.anchor.localRotation = transform.rotation;
            this.anchor.localPosition = FocusPosition();

            // parent this camera to camera parent
            transform.SetParent(cameraParent, true);

            // initialize touch (3 fingers)
            isTouchOverUI = new bool[3];

            // add interaction object event listener
            EventManager.AddListener<ObjectInteractionRotateEvent>(RotateModeListener);
            EventManager.AddListener<ObjectInteractionResetCameraEvent>(ResetCameraPositionListener);
            EventManager.AddListener<ObjectInteractionLockCameraEvent>(LockCameraListener);
            EventManager.AddListener<SceneReadyEvent>(InitializationDoneListener);

            // setting listener
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

            // part object listener
            EventManager.AddListener<PartObjectEvent>(PartObjectListener);

            // preset camera listener
            EventManager.AddListener<CameraPresetEvent>(CameraPresetListener);

            EventManager.TriggerEvent(new CameraControllerReadyEvent());
        }

        private void OnDestroy()
        {
            VirtualTrainingInputSystem.OnStartLeftClick -= OnStartLeftClick;
            VirtualTrainingInputSystem.OnLeftClickDown -= OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
            VirtualTrainingInputSystem.OnMovePointer -= OnMovePointer;
            VirtualTrainingInputSystem.OnStartMiddleClick -= OnStartMiddleClick;
            VirtualTrainingInputSystem.OnEndMiddleClick -= OnEndMiddleClick;
            VirtualTrainingInputSystem.OnStartRightClick -= OnStartRightClick;
            VirtualTrainingInputSystem.OnEndRightClick -= OnEndRightClick;
            VirtualTrainingInputSystem.OnMouseScroll -= OnMouseScroll;
            VirtualTrainingInputSystem.OnTouchAdded -= OnTouchAdded;
            VirtualTrainingInputSystem.OnTouchRemoved -= OnTouchRemoved;

            EventManager.RemoveListener<ObjectInteractionRotateEvent>(RotateModeListener);
            EventManager.RemoveListener<ObjectInteractionResetCameraEvent>(ResetCameraPositionListener);
            EventManager.RemoveListener<ObjectInteractionLockCameraEvent>(LockCameraListener);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<PartObjectEvent>(PartObjectListener);
            EventManager.RemoveListener<CameraPresetEvent>(CameraPresetListener);
        }

        public override void AudioListenerSetup()
        {
            AudioListener audioListener = GetComponent<AudioListener>();
            if (audioListener == null)
                gameObject.AddComponent<AudioListener>();
        }

        public override void ForceSetPositionAndRotation(Vector3 position, Vector3 rotation)
        {
            EventManager.RemoveListener<SceneReadyEvent>(InitializationDoneListener);
            isMoving = false;
            transform.localPosition = position;
            transform.localRotation = Quaternion.Euler(rotation);
        }

        public override void SetCameraDataFromServer(Vector3 position, Vector3 rotation, float fov, float orthographicSize)
        {
            isMoving = true;
            isFromServer = true;

            transitionState = 0f;
            lastCameraPosition = transform.localPosition;
            lastCameraRotation = transform.localRotation;

            lastFOV = mainCamera.fieldOfView;

            lastOrthographicSize = mainCamera.orthographicSize;
            isOrthographic = mainCamera.orthographic;

            destinationPosition = position;
            destinationRotation = Quaternion.Euler(rotation);

            destinationFOV = fov;
            destinationOrthographicSize = orthographicSize;
        }

        public override bool IsMoving()
        {
            return isMoving;
        }

        private void InitializationDoneListener(SceneReadyEvent e)
        {
            Debug.Log("move camera to default position");

            //anchor.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform, true);
            //cameraParent.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform, true);

            anchor.transform.SetParent(anchorParent, true);

            ResetCamera(null);
            EventManager.RemoveListener<SceneReadyEvent>(InitializationDoneListener);
        }

        private void PartObjectListener(PartObjectEvent e)
        {
            partObjects = e.objects;
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            rotateSpeed = e.data.rotateSpeed;
            zoomSpeed = e.data.zoomSpeed;
            dragSpeed = e.data.dragSpeed;
            cameraTransitionSpeed = e.data.cameraTransitionSpeed;
        }

        private void RotateModeListener(ObjectInteractionRotateEvent e)
        {
            isRotateMode = e.on;
        }

        private void ResetCameraPositionListener(ObjectInteractionResetCameraEvent e)
        {
            MoveCamera(destinationPosition, destinationRotation, cameraTarget, isOrthographic, isRotateFromView, destinationFOV, destinationOrthographicSize, null);
        }

        private void LockCameraListener(ObjectInteractionLockCameraEvent e)
        {
            isLocked = e.on;
            if (isLocked)
            {
                OnEndLeftClick(Vector2.zero, null);
                OnEndMiddleClick(Vector2.zero, null);
                OnEndRightClick(Vector2.zero, null);
                lastPinchDistance = 0f;
            }
        }

        private void CameraPresetListener(CameraPresetEvent e)
        {
            CameraDestination destination = DatabaseManager.GetCameraPreset(e.name);
            if (destination == null)
            {
                Debug.LogWarning("camera preset null : " + e.name);
                return;
            }

            MoveCamera(destination, null);
        }

        public override Camera GetCamera()
        {
            return mainCamera;
        }

        private void OnTouchAdded(bool isFingerOverUI)
        {
            if (isLocked)
                return;

            touchCount = VirtualTrainingInputSystem.TouchCount;

            // reset touch over ui states if this touch is first touch
            if (touchCount == 1)
            {
                for (int i = 0; i < isTouchOverUI.Length; i++)
                {
                    isTouchOverUI[i] = false;
                }
            }

            // reset touch input validation state
            isZoomTouchValid = false;
            isDragTouchValid = false;

            // set touch state
            if (touchCount <= isTouchOverUI.Length)
                isTouchOverUI[touchCount - 1] = isFingerOverUI;

            if (touchCount > 1)
            {
                OnEndLeftClick(Vector2.zero, null);
                OnEndMiddleClick(Vector2.zero, null);

                // for zoom
                if (touchCount == 2 && !isTouchOverUI[0] && !isTouchOverUI[1])
                {
                    isZoomTouchValid = true;
                }
                else
                    isZoomTouchValid = false;

                // for drag
                if (!isRotateFromView && touchCount == 3 && !isTouchOverUI[0] && !isTouchOverUI[1] && !isTouchOverUI[2])
                {
                    isDragTouchValid = true;
                    InitDrag();
                }
                else
                    isDragTouchValid = false;
            }
        }

        private void OnTouchRemoved(bool isFingerOverUI)
        {
            lastPinchDistance = 0f;
            touchCount = VirtualTrainingInputSystem.TouchCount;
            OnEndLeftClick(Vector2.zero, null);
            OnEndMiddleClick(Vector2.zero, null);
            isDragTouchValid = false;
        }

        private void InitRotate()
        {
            isRotating = true;
            touchSmoothRotateSpeed = 0f;

            anchor.localPosition = FocusPosition();

            if (isCrossProductDifferent)
            {
                anchor.localPosition = transform.localPosition;
                isCrossProductDifferent = false;
            }

            transform.SetParent(anchor, true);

            if (isRotateFromView)
            {
                rotateOffset.x = transform.localRotation.eulerAngles.y;
                rotateOffset.y = -transform.localRotation.eulerAngles.x;
                rotationOffsetZ = transform.localRotation.eulerAngles.z;
            }
            else
            {
                rotateOffset.x = anchor.localRotation.eulerAngles.y;
                rotateOffset.y = -anchor.localRotation.eulerAngles.x;
                rotationOffsetZ = anchor.localRotation.eulerAngles.z;
            }
        }

        private void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null || isLocked)
                return;

            if (VirtualTrainingInputSystem.TouchCount == 0)
            {
                isZoomTouchValid = false;
                isDragTouchValid = false;
            }

            if (isRotateMode)
                InitRotate();
            else
                InitDrag();
        }

        private void OnLeftClickDown(Vector2 axisValue)
        {
            pointerAxisPosition = axisValue;
        }

        private void OnEndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (isRotating)
            {
                isRotating = false;
                transform.SetParent(cameraParent, true);
            }

            if (!isRotateMode)
            {
                isDragging = false;
            }
        }

        private void InitDrag()
        {
            if (isRotateFromView)
                return;

            isDragging = true;
            pointerDeltaPosition = Vector2.zero;
            dragSmoothDelta = Vector2.zero;

            anchor.localPosition = FocusPosition();
            dragDistance = Vector3.Distance(anchor.localPosition, transform.localPosition);
            if (dragDistance < 0.01f)
                dragDistance = 0.01f;
        }

        private void OnStartMiddleClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (VirtualTrainingCamera.IsPulling)
                return;

            if (raycastedUI != null || isLocked)
                return;

            if (VirtualTrainingInputSystem.TouchCount == 0)
            {
                isZoomTouchValid = false;
                isDragTouchValid = false;
                InitDrag();
            }
        }

        private void OnEndMiddleClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isRotating = false;
            isDragging = false;
        }

        private void OnStartRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (VirtualTrainingCamera.IsPulling)
                return;

            if (raycastedUI != null || isLocked)
                return;

            if (VirtualTrainingInputSystem.TouchCount == 0)
            {
                isZoomTouchValid = false;
                isDragTouchValid = false;
                isDraggingZoom = true;
            }
        }

        private void OnEndRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isDraggingZoom = false;
        }

        private void OnMouseScroll(float value, GameObject raycastedUI)
        {
            if (raycastedUI != null || isLocked)
                return;

            scrollValue = value;
        }

        private void OnMovePointer(Vector2 deltaPosition)
        {
            pointerDeltaPosition = deltaPosition;
        }

        private Vector3 FocusPosition()
        {
            RaycastHit hitInfo = new RaycastHit();
            if (VirtualTrainingCamera.Raycast(ref hitInfo))
            {
                lastFocusedPosition = hitInfo.point;
                return lastFocusedPosition;
            }

            if (partObjects != null && partObjects.Count > 0)
            {
                int nearestIdx = -1;
                int nearestHighlightIdx = -1;
                float minDistance = float.MaxValue;

                for (int i = 0; i < partObjects.Count; i++)
                {
                    if (partObjects[i].target.gameObject != null &&
                        (partObjects[i].action == PartObjectAction.Solo ||
                        partObjects[i].action == PartObjectAction.Highlight ||
                        partObjects[i].action == PartObjectAction.Enable ||
                        partObjects[i].action == PartObjectAction.Blink ||
                        partObjects[i].action == PartObjectAction.SetLayer ||
                        partObjects[i].action == PartObjectAction.IgnoreCutaway ||
                        partObjects[i].action == PartObjectAction.Fresnel
                        ))
                    {
                        float distance = Vector3.Distance(transform.localPosition, partObjects[i].target.gameObject.transform.localPosition);
                        if (distance < minDistance)
                        {
                            minDistance = distance;
                            nearestIdx = i;

                            // prioritize highlight
                            if (partObjects[i].action == PartObjectAction.Highlight)
                                nearestHighlightIdx = i;
                        }
                    }
                }

                if (nearestIdx == -1)
                {
                    return lastFocusedPosition;
                }

                return partObjects[nearestHighlightIdx == -1 ? nearestIdx : nearestHighlightIdx].target.gameObject.transform.localPosition;
            }

            return lastFocusedPosition;
        }

        private float CalculatePinchZoom(Vector2 touchZero, Vector2 touchOne, float zoomDistance)
        {
            float currentDistance = Vector2.Distance(touchZero, touchOne);

            float result = 0f;

            if (currentDistance > lastPinchDistance)
            {
                result = -Time.deltaTime * zoomDistance;
            }
            else if (currentDistance < lastPinchDistance)
            {
                result = Time.deltaTime * zoomDistance;
            }

            lastPinchDistance = currentDistance;

            return result * ZOOM_PINCH_CONST;
        }

        private float CalculateZoomValue(float zoomDistance)
        {
            if (VirtualTrainingInputSystem.TouchCount == 2)
            {
                return CalculatePinchZoom(VirtualTrainingInputSystem.GetTouchPosition(0), VirtualTrainingInputSystem.GetTouchPosition(1), zoomDistance);
            }
            else if (isDraggingZoom)
            {
                return pointerDeltaPosition.y * (zoomDistance * Time.deltaTime);
            }
            else if (scrollValue != 0f)
            {
                if (scrollValue > 0)
                    return -Time.deltaTime * zoomDistance * ZOOM_SCROLL_CONST;
                else if (scrollValue < 0)
                    return Time.deltaTime * zoomDistance * ZOOM_SCROLL_CONST;
            }

            return 0f;
        }

        private bool IsCrossProductDifferent()
        {
            Vector3 crossIn = Vector3.Cross(anchor.localPosition, transform.localPosition);

            if (crossIn.x < 0 && crossProductZoomX)
                return true;

            if (crossIn.y < 0 && crossProductZoomY)
                return true;

            if (crossIn.z < 0 && crossProductZoomZ)
                return true;

            if (crossIn.x > 0 && !crossProductZoomX)
                return true;

            if (crossIn.y > 0 && !crossProductZoomY)
                return true;

            if (crossIn.z > 0 && !crossProductZoomZ)
                return true;

            return false;
        }

        public override void ResetCamera(Action arriveAction)
        {
            MoveCamera(defaultDestination, arriveAction);
        }

        public override void MoveCamera(CameraDestination cameraDestination, Action arriveAction)
        {
            MoveCamera(cameraDestination.position, cameraDestination.rotation, cameraDestination.target.gameObject == null ? null : cameraDestination.target.gameObject.transform, cameraDestination.isOrthographic, cameraDestination.isRotateFromView, cameraDestination.fov, cameraDestination.orthographicSize, arriveAction);
        }

        public override void MoveCamera(Vector3 position, Quaternion rotation, Transform cameraTarget, bool isOrthographic, bool isRotateFromView, float fov, float orthographicSize, Action arriveAction)
        {
            isMoving = true;
            isFromServer = false;

            transitionState = 0f;
            lastCameraPosition = transform.localPosition;
            lastCameraRotation = transform.localRotation;
            lastFOV = mainCamera.fieldOfView;
            lastOrthographicSize = mainCamera.orthographicSize;

            if (rotation.w == 0f && rotation.x == 0f && rotation.y == 0f && rotation.z == 0f)
                rotation = Quaternion.identity;

            OnEndLeftClick(Vector2.zero, null);
            OnEndMiddleClick(Vector2.zero, null);
            OnEndRightClick(Vector2.zero, null);

            this.cameraTarget = cameraTarget;

            destinationPosition = position;
            destinationRotation = rotation;

            this.isOrthographic = isOrthographic;
            this.isRotateFromView = isRotateFromView;
            destinationFOV = fov;
            destinationOrthographicSize = orthographicSize;
            this.arriveAction = arriveAction;
        }

        private void LateUpdate()
        {
            if (isMoving)
            {
                transitionState += Time.deltaTime * (isFromServer ? cameraTransitionSpeed * 3 : cameraTransitionSpeed);

                transform.localPosition = Vector3.Lerp(lastCameraPosition, destinationPosition, transitionState);
                transform.localRotation = Quaternion.Slerp(lastCameraRotation, destinationRotation, transitionState);

                mainCamera.fieldOfView = Mathf.Lerp(lastFOV, destinationFOV, transitionState);
                mainCamera.orthographicSize = Mathf.Lerp(lastOrthographicSize, destinationOrthographicSize, transitionState);

                if (onTopCamera != null)
                {
                    onTopCamera.fieldOfView = mainCamera.fieldOfView;
                    onTopCamera.orthographicSize = mainCamera.orthographicSize;
                }

                if (transitionState >= 1f)
                {
                    isMoving = false;

                    mainCamera.orthographic = isOrthographic;
                    if (onTopCamera != null)
                        onTopCamera.orthographic = isOrthographic;

                    anchor.localRotation = transform.localRotation;

                    if (cameraTarget != null)
                    {
                        Quaternion lookAt = Quaternion.LookRotation(cameraTarget.localPosition - transform.localPosition);
                        cameraTargetOffset = Quaternion.Inverse(lookAt) * transform.localRotation;
                    }

                    arriveAction?.Invoke();
                    arriveAction = null;
                }

                return;
            }

            if (cameraTarget != null)
            {
                Quaternion targetRotation = Quaternion.LookRotation(cameraTarget.localPosition - transform.localPosition) * cameraTargetOffset;
                transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, cameraTransitionSpeed * Time.deltaTime);
                return;
            }

            if (VirtualTrainingCamera.IsMovementEnabled)
            {
                // zoom
                if (isDraggingZoom || scrollValue != 0f || isZoomTouchValid)
                {
                    if (touchCount > 1)
                        isDraggingZoom = false;

                    if (transform.parent != anchor)
                        transform.SetParent(cameraParent, true);

                    float zoomDistance = 1f;

                    if (!isCrossProductDifferent)
                    {
                        zoomDistance = Vector3.Distance(anchor.localPosition, transform.localPosition);
                        if (zoomDistance < 0.05f)
                            zoomDistance = 0.05f;
                        else if (zoomDistance > 5f)
                            zoomDistance = 5f;
                    }

                    float zoomValue = CalculateZoomValue(zoomDistance) * zoomSpeed;

                    if (zoomValue != 0f)
                    {
                        if (mainCamera.orthographic) // orthographic zoom
                        {
                            mainCamera.orthographicSize += zoomValue;
                            if (mainCamera.orthographicSize <= 0.1f)
                                mainCamera.orthographicSize = 0.1f;

                            if (onTopCamera != null)
                                onTopCamera.orthographicSize = mainCamera.orthographicSize;
                        }
                        else if (isRotateFromView) // fov zoom
                        {
                            mainCamera.fieldOfView += zoomValue;
                            if (mainCamera.fieldOfView <= 0.1f)
                                mainCamera.fieldOfView = 0.1f;

                            if (onTopCamera != null)
                                onTopCamera.fieldOfView = mainCamera.fieldOfView;
                        }
                        else // position zoom
                        {
                            // zoom in min handling
                            if (zoomValue < 0)
                            {
                                // if camera position is in front of anchor / min zoom
                                if (IsCrossProductDifferent())
                                {
                                    isCrossProductDifferent = true;
                                }
                            }
                            else
                            {
                                Vector3 cross = Vector3.Cross(anchor.localPosition, transform.localPosition);
                                crossProductZoomX = cross.x > 0;
                                crossProductZoomY = cross.y > 0;
                                crossProductZoomZ = cross.z > 0;
                            }

                            transform.Translate(0f, 0f, -zoomValue);
                        }
                    }
                }
                else if (isDragging || isDragTouchValid) // drag
                {
                    if (isRotateMode)
                        OnEndLeftClick(Vector2.zero, null);

                    isRotateFromView = false;
                    isRotating = false;

                    Vector2 dragInput;

                    // touch drag
                    if (VirtualTrainingInputSystem.TouchCount > 0)
                        dragInput = new Vector2(-pointerDeltaPosition.x * TOUCH_DRAG_CONST * dragDistance * dragSpeed, -pointerDeltaPosition.y * TOUCH_DRAG_CONST * dragDistance * dragSpeed);
                    else // mouse drag
                        dragInput = new Vector2(-pointerDeltaPosition.x * MOUSE_DRAG_CONST * dragDistance * dragSpeed, -pointerDeltaPosition.y * MOUSE_DRAG_CONST * dragDistance * dragSpeed);

                    dragSmoothDelta = Vector2.Lerp(dragSmoothDelta, dragInput, Time.deltaTime * dragSpeed);
                    transform.Translate(dragSmoothDelta, Space.Self);
                }
                else if (isRotating) // orbit
                {
                    rotationAxis = pointerAxisPosition * rotateSpeed * ROTATE_CONST;
                    rotationAxis = rotationAxis + rotateOffset;

                    // rotate touch
                    if (VirtualTrainingInputSystem.TouchCount > 0)
                    {
                        touchSmoothRotateSpeed = Mathf.Lerp(touchSmoothRotateSpeed, rotateSpeed, Time.deltaTime * ROTATE_SMOOTH_SPEED_TOUCH_CONST);

                        if (isRotateFromView)
                            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(-rotationAxis.y, rotationAxis.x, rotationOffsetZ), touchSmoothRotateSpeed * Time.deltaTime);
                        else
                            anchor.localRotation = Quaternion.Slerp(anchor.localRotation, Quaternion.Euler(-rotationAxis.y, rotationAxis.x, rotationOffsetZ), touchSmoothRotateSpeed * Time.deltaTime);
                    }
                    else // rotate mouse
                    {
                        if (isRotateFromView)
                            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(-rotationAxis.y, rotationAxis.x, rotationOffsetZ), ROTATE_SMOOTH_SPEED_MOUSE_CONST * Time.deltaTime);
                        else
                            anchor.localRotation = Quaternion.Slerp(anchor.localRotation, Quaternion.Euler(-rotationAxis.y, rotationAxis.x, rotationOffsetZ), ROTATE_SMOOTH_SPEED_MOUSE_CONST * Time.deltaTime);
                    }
                }
            }
        }
    }
}

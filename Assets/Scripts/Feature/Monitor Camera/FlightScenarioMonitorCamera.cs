using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class FlightScenarioMonitorCamera : MonoBehaviour
    {
        [SerializeField] Camera monitorCamera;
        [SerializeField] Transform cameraTarget;

        /*float moveCameraSpeed;
        MonitorCamera currentTarget;
        bool arrive;
        float transitionState;
        Vector3 lastCameraPosition;
        Quaternion cameraTargetOffset;
        Quaternion lastCameraRotation;
        //GameObject cameraTarget;
        */

        // setting
        //float rotateSpeed = 5f;
        //float dragSpeed = 5f;
        //float touchSmoothRotateSpeed;

        // const
        //const float TOUCH_DRAG_CONST = 0.0003f;
        //const float MOUSE_DRAG_CONST = 0.001f;
        //const float ROTATE_CONST = 0.05f;
        //const float ROTATE_SMOOTH_SPEED_TOUCH_CONST = 10f;
        //const float ROTATE_SMOOTH_SPEED_MOUSE_CONST = 30f;
        //const float ZOOM_PINCH_CONST = 2f;
        //const float ZOOM_SCROLL_CONST = 3f;

        // state
        bool isMonitorCamInteraction = false;
        Vector2 pointerAxisRotation;
        Vector2 pointerAxisZoom;
        //Vector2 rotationAxis;
        //Vector2 rotateOffset;
        //float rotationOffsetZ;

        //------------------------------------------ Free Camera
        [ReadOnly] [SerializeField] float radius, azimuth, elevation;
        float filterY;
        Vector3 camVelocity = Vector3.zero;
        Vector3 filterPosition; 
        Vector3 cameraDirection;

        [SerializeField] float azimuthSensitivity = 0.1f;
        [SerializeField] float elevationSensitivity = 0.1f;
        [SerializeField] float radiusSensitivity = 0.1f;
        [SerializeField] float minRadius = 5f;
        [SerializeField] float maxRadius = 20f;
        [SerializeField] float smoothTime = 0.3f;


        private void Start()
        {
            //EventManager.AddListener<MonitorCameraEvent>(MonitorCameraListener);
            //EventManager.AddListener<PlayMonitorCameraEvent>(PlayMonitorCameraListener);
            //EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            VirtualTrainingInputSystem.OnLeftClickDown += OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick += OnEndLeftClick;
            VirtualTrainingInputSystem.OnRightClickDown += OnRightClickDown;
            VirtualTrainingInputSystem.OnEndRightClick += OnEndRightClick;
            EventManager.AddListener<MonitorCameraInteractionEvent>(InteractionEventListener);


            VirtualTrainingCamera.SetCurrentMonitorCamera(monitorCamera, null);
            monitorCamera.gameObject.SetActive(true);
            //gameObject.SetActive(false); // to avoid unnecesary update call
        }

        private void OnDestroy()
        {
            //EventManager.RemoveListener<MonitorCameraEvent>(MonitorCameraListener);
            //EventManager.RemoveListener<PlayMonitorCameraEvent>(PlayMonitorCameraListener);
            //EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            VirtualTrainingInputSystem.OnLeftClickDown -= OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
            VirtualTrainingInputSystem.OnRightClickDown -= OnRightClickDown;
            VirtualTrainingInputSystem.OnEndRightClick -= OnEndRightClick;
            EventManager.RemoveListener<MonitorCameraInteractionEvent>(InteractionEventListener);

        }

        protected virtual void InteractionEventListener(MonitorCameraInteractionEvent e)
        {
            /*if (e.interactionObject == null)
                return;
            else*/
            isMonitorCamInteraction = true;
            InitInteraction();
        }

        private void InitInteraction()
        {
            //touchSmoothRotateSpeed = 0f;

            //POSITION CAMERA
            monitorCamera.transform.LookAt(cameraTarget);
            cameraDirection = monitorCamera.transform.position - cameraTarget.position;
            CartesianToSpherical(cameraDirection, out radius, out azimuth, out elevation);


            //monitorCamera.gameObject.transform.SetParent(cameraTarget, true);

            //rotateOffset.x = cameraTarget.transform.rotation.eulerAngles.y;
            //rotateOffset.y = -cameraTarget.transform.rotation.eulerAngles.x;
            //rotationOffsetZ = cameraTarget.transform.rotation.eulerAngles.z;
        }

        /*private void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null)
                return;

            InitRotate();
        }*/

        private void OnLeftClickDown(Vector2 axisValue)
        {
            pointerAxisRotation = axisValue;
        }

        private void OnEndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (isMonitorCamInteraction)
            {
                isMonitorCamInteraction = false;
            }
        }

        private void OnRightClickDown(Vector2 axisValue)
        {
            pointerAxisZoom = axisValue;
        }

        private void OnEndRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (isMonitorCamInteraction)
            {
                isMonitorCamInteraction = false;
            }
        }

        private void LateUpdate()
        {
            if (isMonitorCamInteraction)
            {
                radius -= pointerAxisZoom.y * radiusSensitivity * Time.deltaTime;
                azimuth -= pointerAxisRotation.x * azimuthSensitivity * Time.deltaTime;
                elevation -= pointerAxisRotation.y * elevationSensitivity * Time.deltaTime;

                // Calculate direction and position
                SphericalToCartesian(radius, azimuth, elevation, out cameraDirection);

                if (cameraTarget.position.y < maxRadius)
                {
                    filterPosition = cameraTarget.position + cameraDirection;
                    filterY = filterPosition.y;
                    if (filterY < 2)
                        filterY = 2;
                    //monitorCamera.transform.position = new Vector3(filterPosition.x, filterY, filterPosition.z);
                    Vector3 filteredPosition = new Vector3(filterPosition.x, filterY, filterPosition.z);
                    monitorCamera.transform.position = Vector3.SmoothDamp(monitorCamera.transform.position, filteredPosition, ref camVelocity, smoothTime);
                }
                else
                {
                    //monitorCamera.transform.position = cameraTarget.position + cameraDirection;
                    monitorCamera.transform.position = Vector3.SmoothDamp(monitorCamera.transform.position, cameraTarget.position + cameraDirection, ref camVelocity, smoothTime);
                }

                //POSITION CAMERA
                monitorCamera.transform.LookAt(cameraTarget);
                if (radius > maxRadius)
                    radius = maxRadius;
                else if (radius < minRadius)
                    radius = minRadius;
            }
        }

        private void SphericalToCartesian(float radiusInput, float polarInput, float elevationInput, out Vector3 position)
        {
            float jester;
            jester = radiusInput * Mathf.Cos(elevationInput); 
            position.x = jester * Mathf.Cos(polarInput);
            position.y = radiusInput * Mathf.Sin(elevationInput); 
            position.z = jester * Mathf.Sin(polarInput);
        }

        public static void CartesianToSpherical(Vector3 cordinatesInput, out float outRadius, out float outPolar, out float outElevation)
        {
            if (cordinatesInput.x == 0) cordinatesInput.x = Mathf.Epsilon;
            outRadius = Mathf.Sqrt((cordinatesInput.x * cordinatesInput.x) + (cordinatesInput.y * cordinatesInput.y) + (cordinatesInput.z * cordinatesInput.z));
            outPolar = Mathf.Atan(cordinatesInput.z / cordinatesInput.x);
            if (cordinatesInput.x < 0) outPolar += Mathf.PI;
            outElevation = Mathf.Asin(cordinatesInput.y / outRadius);
        }

    }
}

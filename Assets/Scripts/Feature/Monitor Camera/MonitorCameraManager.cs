using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class MonitorCameraManager : MonoBehaviour
    {
        const float ROTATE_MULTIPLIER = 5;

        [SerializeField] Camera monitorCamera;

        float moveCameraSpeed;
        float transitionState;
        MonitorCamera currentTarget;
        bool arrive;
        bool pointerDown;
        Vector3 lastCameraPosition;
        Quaternion cameraTargetOffset;
        Quaternion lastCameraRotation;
        GameObject cameraTarget;
        GameObject anchor;
        MateriElementType currentType;

        private void Start()
        {
            EventManager.AddListener<MonitorCameraEvent>(MonitorCameraListener);
            EventManager.AddListener<PlayMonitorCameraEvent>(PlayMonitorCameraListener);
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

            VirtualTrainingInputSystem.OnLeftClickDown += PointerDown;
            VirtualTrainingInputSystem.OnEndLeftClick += PointerUp;

            VirtualTrainingCamera.SetCurrentMonitorCamera(monitorCamera, OrbitCamera);
            monitorCamera.gameObject.SetActive(true);

            anchor = new GameObject("monitor camera anchor");
            anchor.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform);
            anchor.transform.position = Vector3.zero;
            anchor.transform.rotation = Quaternion.identity;

            // to avoid unnecesary update call
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MonitorCameraEvent>(MonitorCameraListener);
            EventManager.RemoveListener<PlayMonitorCameraEvent>(PlayMonitorCameraListener);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);

            VirtualTrainingInputSystem.OnLeftClickDown -= PointerDown;
            VirtualTrainingInputSystem.OnEndLeftClick -= PointerUp;
        }

        private void PointerDown(Vector2 axisValue)
        {
            pointerDown = true;
        }

        private void PointerUp(Vector2 pointerPosition, GameObject raycastedUI)
        {
            pointerDown = false;
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            moveCameraSpeed = e.data.cameraTransitionSpeed;
        }

        private void MonitorCameraListener(MonitorCameraEvent e)
        {
            if (e.monitors == null || e.monitors.Count == 0)
            {
                // disable camera
                monitorCamera.transform.SetParent(transform);
                gameObject.SetActive(false);
            }
            else
            {
                currentType = e.type;
            }
        }

        private void PlayMonitorCameraListener(PlayMonitorCameraEvent e)
        {
            gameObject.SetActive(true);
            currentTarget = e.destination;
            arrive = false;
            transitionState = 0f;
            lastCameraPosition = monitorCamera.transform.localPosition;
            lastCameraRotation = monitorCamera.transform.localRotation;
            cameraTarget = currentTarget.destination.target.gameObject;
            monitorCamera.orthographic = currentTarget.destination.isOrthographic;

            if (currentTarget.destination.parent.gameObject == null)
                monitorCamera.transform.SetParent(anchor.transform, true);
            else
                monitorCamera.transform.SetParent(currentTarget.destination.parent.gameObject.transform, true);

            if (monitorCamera.orthographic)
                monitorCamera.orthographicSize = currentTarget.destination.orthographicSize;
            else
                monitorCamera.fieldOfView = currentTarget.destination.fov;
        }

        private void OrbitCamera(Vector2 pointerDelta)
        {
            if (!pointerDown)
                return;

            // todo : enable only in flight scenario

            //if (currentType == MateriElementType.FlightScenario)
            //{
            //Vector3 rot = anchor.transform.localRotation.eulerAngles + new Vector3(0, pointerDelta.x * ROTATE_MULTIPLIER, 0);
            //anchor.transform.localRotation = Quaternion.Slerp(Quaternion.Euler(rot), anchor.transform.localRotation, Time.deltaTime);
            //}
        }

        private void Update()
        {
            if (!arrive && monitorCamera.gameObject.activeInHierarchy)
            {
                transitionState += Time.deltaTime * moveCameraSpeed;

                if (currentTarget.destination.useParentPositionAndRotation && currentTarget.destination.parent.gameObject != null)
                {
                    monitorCamera.transform.localPosition = Vector3.Lerp(lastCameraPosition, currentTarget.destination.parent.gameObject.transform.localPosition, transitionState);
                    monitorCamera.transform.localRotation = Quaternion.Slerp(lastCameraRotation, currentTarget.destination.parent.gameObject.transform.localRotation, transitionState);
                }
                else
                {
                    monitorCamera.transform.localPosition = Vector3.Lerp(lastCameraPosition, currentTarget.destination.position, transitionState);
                    monitorCamera.transform.localRotation = Quaternion.Slerp(lastCameraRotation, currentTarget.destination.rotation, transitionState);
                }

                if (transitionState >= 1f)
                {
                    if (cameraTarget != null)
                    {
                        Quaternion lookAt = Quaternion.LookRotation(cameraTarget.transform.localPosition - monitorCamera.transform.localPosition);
                        cameraTargetOffset = Quaternion.Inverse(lookAt) * monitorCamera.transform.localRotation;
                    }


                    arrive = true;
                }
            }

            if (arrive && cameraTarget != null)
            {
                Quaternion targetRotation = Quaternion.LookRotation(cameraTarget.transform.localPosition - monitorCamera.transform.localPosition) * cameraTargetOffset;
                monitorCamera.transform.localRotation = Quaternion.Slerp(monitorCamera.transform.localRotation, targetRotation, moveCameraSpeed * Time.deltaTime);
            }
        }
    }
}

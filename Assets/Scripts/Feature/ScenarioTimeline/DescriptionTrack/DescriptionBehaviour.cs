using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using TMPro;


public class DescriptionBehaviour : PlayableBehaviour
{
    public string descriptionText;
    public Color textColor;
    //public Vector3 textPosition;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        base.ProcessFrame(playable, info, playerData);

        TMP_Text text = playerData as TMP_Text;
        text.text = descriptionText;
        //text.color = new Color(1, 1, 1, info.weight);
        text.color = textColor;
        //text.gameObject.transform.parent.localPosition = textPosition;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class DescriptionClip : PlayableAsset
{
    [TextArea(5,10)]
    public string descriptionText;
    public Color textColor = Color.white;
    //public Vector3 textPosition;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<DescriptionBehaviour>.Create(graph);
        DescriptionBehaviour descriptionBehaviour = playable.GetBehaviour();
        descriptionBehaviour.descriptionText = descriptionText;
        descriptionBehaviour.textColor = textColor;
        //descriptionBehaviour.textPosition = textPosition;

        return playable;
        //throw new System.NotImplementedException();
    }
}

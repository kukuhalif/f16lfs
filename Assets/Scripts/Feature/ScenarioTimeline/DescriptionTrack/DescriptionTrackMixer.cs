using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using TMPro;

public class DescriptionTrackMixer : PlayableBehaviour
{
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        base.ProcessFrame(playable, info, playerData);

        TextMeshProUGUI text = playerData as TextMeshProUGUI;
        string currentText = "";
        float currentAlpha = 0f;

        if (!text) return;

        int inputCount = playable.GetInputCount();
        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);

            if (inputWeight > 0f)
            {
                ScriptPlayable<DescriptionBehaviour> inputPlayable = (ScriptPlayable<DescriptionBehaviour>)playable.GetInput(i);

                DescriptionBehaviour input = inputPlayable.GetBehaviour();
                currentText = input.descriptionText;
                currentAlpha = inputWeight;
            }
        }
    }
}

using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class MarkerMenu : MonoBehaviour
{
    private PlayableDirector playableDirector;

    private void Start()
    {
        playableDirector = GetComponent<PlayableDirector>();
    }

    public void GoToMarker(int markerNum)
    {
        //int markerNum = 1;
        TimelineAsset timelineAsset = playableDirector.playableAsset as TimelineAsset;
        var markers = timelineAsset.markerTrack.GetMarkers().ToArray();
        playableDirector.time = markers[markerNum].time;
        playableDirector.Play();
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1);
    }

    public void Pause()
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0);
    }
}

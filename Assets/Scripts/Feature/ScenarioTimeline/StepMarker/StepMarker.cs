using UnityEngine;
using UnityEngine.Timeline;

[CustomStyle("StepMarker")]
public class StepMarker : Marker
{
    [TextArea] public string description;
}

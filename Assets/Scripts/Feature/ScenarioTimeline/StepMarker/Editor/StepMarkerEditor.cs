#if UNITY_EDITOR
using UnityEngine.Timeline;
using UnityEditor.Timeline;

[CustomTimelineEditor(typeof(StepMarker))]
public class StepMarkerEditor : MarkerEditor
{
    public override MarkerDrawOptions GetMarkerOptions(IMarker marker)
    {
        MarkerDrawOptions options;
        options.tooltip = marker.ToString();
        return options;
    }
}
#endif
using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class RangeLabelClip : PlayableAsset, ITimelineClipAsset
{
    public RangeLabelBehaviour template = new RangeLabelBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<RangeLabelBehaviour>.Create (graph, template);
        RangeLabelBehaviour clone = playable.GetBehaviour ();
        return playable;
    }
}

#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.Timeline;
using UnityEditor;
using UnityEditor.Timeline;

[CustomTimelineEditor(typeof(RangeLabelTrack))]
public class RangeLabelTrackEditor : TrackEditor
{
    public override TrackDrawOptions GetTrackOptions(TrackAsset track, Object binding)
    {
        var options = base.GetTrackOptions(track, binding);
        Texture2D icon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Scripts/Feature/ScenarioTimeline/RangeLabelTrack/label-icon.png", typeof(Texture2D));
        options.icon = icon;
        return options;
    }
}
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using System.Security.Cryptography;
using System.Diagnostics;

namespace VirtualTraining.Feature
{
    public class VirtualTrainingAuthentication : MonoBehaviour
    {
        const string randomString1 = "HKK&4#@hI&&(*sds3(h";
        const string randomString2 = "&7b79&JK8(*79(*98h*&#$%219J";
        const string randomString3 = "gOIO*8)&_niKU454nKfd";

        private void Start()
        {
            if (Application.isEditor)
            {
                Destroy(gameObject);
                return;
            }

            StartCoroutine(CheckAuth());
        }

        string Encrypt(string clearText, string encryptionKey)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        string GetEncryptedString(int index)
        {
            return Encrypt(Application.companyName + Application.productName + SystemInfo.deviceUniqueIdentifier + randomString3, randomString1 + (index * 6443).ToString() + randomString2);
        }

        IEnumerator CheckAuth()
        {
            string dataPath = Path.GetFullPath(Application.dataPath);
            string currentDir = Directory.GetParent(dataPath).ToString();

            // if mono build
            string managed = "/Managed";
            string MonoBleedingEdgePath = currentDir + "/MonoBleedingEdge";
            // if il2cpp build
            string cppRes = "/il2cpp_data/Resources";
            string il2cpp_dataPath = dataPath + "/il2cpp_data";

            string firstDir = "";
            string etcPath = "";

            if (Directory.Exists(MonoBleedingEdgePath))
            {
                firstDir = managed;
                etcPath = MonoBleedingEdgePath;
            }
            else
            {
                firstDir = cppRes;
                etcPath = il2cpp_dataPath;
            }

            List<string> dirs = new List<string>
            {
                dataPath + firstDir + "/UnityEngine.UISubSystem.pdb",
                dataPath + "/Resources/unity custom resources",
                dataPath + "/extraresources.res",
                etcPath + "/etc/mono/2.0/virtual.config",
                etcPath + "/etc/mono/4.0/default.config",
                etcPath + "/etc/mono/4.5/Browsers/dotNet.browser",
                etcPath + "/etc/mono/mconfig/setup.xml",
            };

            //check file access permission
            for (int i = 0; i < dirs.Count; i++)
            {
                try
                {
                    string[] lines;
                    if (File.Exists(dirs[i]))
                        lines = File.ReadAllLines(dirs[i]);
                    else
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(dirs[i]));

                        using (StreamWriter writer = new StreamWriter(dirs[i]))
                        {
                            writer.Write("");
                            writer.Close();
                        }
                        if (File.Exists(dirs[i]))
                            File.Delete(dirs[i]);
                        else
                            Quit();
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    Quit();
                }
            }

            yield return null;

            //count auth files
            int fileCount = 0;
            for (int i = 0; i < dirs.Count; i++)
            {
                if (File.Exists(dirs[i]))
                {
                    fileCount++;
                }
            }

            yield return null;

            if (fileCount != dirs.Count && fileCount != 0)
                Quit();

            if (fileCount == 0)
            {
                for (int i = 0; i < dirs.Count; i++)
                {
                    WriteFile(dirs[i], GetEncryptedString(i));
                    yield return null;
                    CheckFile(dirs[i], GetEncryptedString(i));
                }
            }
            else
            {
                for (int i = 0; i < dirs.Count; i++)
                {
                    CheckFile(dirs[i], GetEncryptedString(i));
                    yield return null;
                }
            }

            yield return null;
            Destroy(gameObject);
        }

        void WriteFile(string path, string value)
        {
            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.Write(value);
                writer.Close();
            }
        }

        void CheckFile(string path, string value)
        {
            if (!File.Exists(path))
                Quit();

            string[] datas = File.ReadAllLines(path);
            if (datas != null && datas.Length > 0)
            {
                if (datas[0] != value)
                    Quit();
            }
            else
                Quit();
        }

        void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Process.GetCurrentProcess().Kill();
#endif
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public abstract class ObjectSelectorDevice : DeviceBehaviour
    {
        protected Action<GameObjectReference> selectObjectCallback;
        protected Action clearSelectionCallback;
        protected Func<List<GameObject>> getSelectedCallback;
        protected Func<bool> allowSelectionCallback;
        protected Action snapNearObjectCallback;

        public virtual void Initialization(Action<GameObjectReference> selectObjectCallback, Action clearSelectionCallback, Func<List<GameObject>> getSelectedCallback, Func<bool> allowSelectionCallback, Action snapNearObjectCallback)
        {
            this.selectObjectCallback = selectObjectCallback;
            this.clearSelectionCallback = clearSelectionCallback;
            this.getSelectedCallback = getSelectedCallback;
            this.allowSelectionCallback = allowSelectionCallback;
            this.snapNearObjectCallback = snapNearObjectCallback;
        }

        public virtual void Deinitialization()
        {
            this.selectObjectCallback = null;
            this.clearSelectionCallback = null;
            this.getSelectedCallback = null;
            this.allowSelectionCallback = null;
            this.snapNearObjectCallback = null;
        }

        // is pressed
        public abstract bool IsButtonDown();

        // position
        public abstract float GetDistanceToAnchor(GameObject obj);
        public abstract Vector3 GetOffsetPosition(GameObject obj, float distanceToAnchor);
        public abstract void SetObjectPosition(GameObject obj, float distanceToAnchor, Vector3 offsetPos);

        // rotation
        public abstract Vector3 GetOffsetRotation(GameObject obj);
        public abstract void SetObjectRotation(GameObject obj, Vector3 rotationOffset);

    }
}

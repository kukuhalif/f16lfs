using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class ObjectSelector : InitializationQueue
    {
        class ObjectData
        {
            public int objId = 0;
            public Transform transform;
            public Vector3 defaultPosition = new Vector3();
            public Vector3 offsetPosition = new Vector3();
            public Vector3 lastPosition = new Vector3();
            public Vector3 defaultRotation = new Vector3();
            public Vector3 lastRotation = new Vector3();
            public Vector3 offsetRotation = new Vector3();
            public float distanceToAnchor;
            public List<GameObject> childs = new List<GameObject>();
            public List<GameObject> childStayPositions = new List<GameObject>();

            public ObjectData(GameObject obj)
            {
                GameObjectReference gor = obj.GetComponent<GameObjectReference>();
                if (gor != null)
                    objId = gor.Id;

                defaultPosition = obj.transform.localPosition;
                defaultRotation = obj.transform.localRotation.eulerAngles;
                transform = obj.transform;

                Transform[] childs = obj.transform.GetAllChilds();
                for (int i = 0; i < childs.Length; i++)
                {
                    Renderer render = childs[i].GetComponent<Renderer>();
                    if (render != null)
                        this.childs.Add(childs[i].gameObject);
                }
            }

            public void ResetPosition(Vector3 lastPosition, Vector3 lastRotation, float transition)
            {
                transform.localPosition = Vector3.Lerp(lastPosition, defaultPosition, transition);
                transform.localRotation = Quaternion.Slerp(Quaternion.Euler(lastRotation), Quaternion.Euler(defaultRotation), transition);
            }

            public void ChildsStay(int i)
            {
                childs[i].transform.position = childStayPositions[i].transform.position;
                childs[i].transform.rotation = childStayPositions[i].transform.rotation;
            }
        }

        // snap object const
        const float MAX_SNAP_DISTANCE = 0.1f;
        const float MAX_SNAP_ANGLE = 20f;

        // setting
        bool isPullApartActive;
        bool isMultipleSelectActive;
        bool isResetObjectActive;
        float resetSpeed;

        public bool IsPullApartActive { get => isPullApartActive; }
        public bool IsMultipleSelectActive { get => isMultipleSelectActive; }

        // cache
        float resetTransition;
        List<GameObject> selectedObjects = new List<GameObject>();
        List<GameObject> hidedObjects = new List<GameObject>();
        List<GameObject> movedObjects = new List<GameObject>();
        List<ObjectData> toResetObjects = new List<ObjectData>();
        List<GameObject> stayPositions = new List<GameObject>();
        Dictionary<GameObject, ObjectData> objectLookup = new Dictionary<GameObject, ObjectData>();

        ObjectSelectorDevice device;
        [SerializeField] OutlineEffect outline;

        public List<GameObject> HidedGameobjects { get => hidedObjects; }

        public override void Initialize()
        {
            objectLookup.Clear();
        }

        public override void ObjectSetup(GameObject obj)
        {
            if (obj.GetComponent<Renderer>() != null)
            {
                ObjectData objData = new ObjectData(obj);
                objectLookup.Add(obj, objData);
            }
        }

        private void Start()
        {
            EventManager.AddListener<ObjectInteractionMultipleSelectionEvent>(MultipleSelectionListener);
            EventManager.AddListener<ObjectInteractionPullApartEvent>(PullApartListener);
            EventManager.AddListener<ObjectInteractionResetObjectEvent>(ResetPositionListener);
            EventManager.AddListener<ObjectInteractionVisibleEvent>(VisibleListener);
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.AddListener<SceneReadyEvent>(SwitchSceneModeListener);
            EventManager.AddListener<ObjectInteractionXRayEvent>(XrayListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ObjectInteractionMultipleSelectionEvent>(MultipleSelectionListener);
            EventManager.RemoveListener<ObjectInteractionPullApartEvent>(PullApartListener);
            EventManager.RemoveListener<ObjectInteractionResetObjectEvent>(ResetPositionListener);
            EventManager.RemoveListener<ObjectInteractionVisibleEvent>(VisibleListener);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<SceneReadyEvent>(SwitchSceneModeListener);
            EventManager.RemoveListener<ObjectInteractionXRayEvent>(XrayListener);

            if (device != null)
                device.Deinitialization();
        }

        public void ClearSelection()
        {
            ClearSelectedColor();

            foreach (var stay in stayPositions)
            {
                Destroy(stay);
            }

            foreach (var selected in selectedObjects)
            {
                objectLookup[selected].childStayPositions.Clear();
            }
            selectedObjects.Clear();

            EventManager.TriggerEvent(new ObjectSelectionEvent());
        }

        public List<GameObject> GetSelected()
        {
            return selectedObjects;
        }

        public List<int> GetSelectedIds()
        {
            List<int> ids = new List<int>();
            foreach (var selected in selectedObjects)
            {
                ids.Add(objectLookup[selected].objId);
            }
            return ids;
        }

        private bool AllowSelection()
        {
            return isPullApartActive || isMultipleSelectActive;
        }

        private void SnapNearObject()
        {
            for (int i = 0; i < selectedObjects.Count; i++)
            {
                if (objectLookup.ContainsKey(selectedObjects[i]))
                {
                    var objData = objectLookup[selectedObjects[i]];
                    if (Vector3.Distance(selectedObjects[i].transform.localPosition, objData.defaultPosition) < MAX_SNAP_DISTANCE)
                    {
                        selectedObjects[i].transform.localPosition = objData.defaultPosition;
                    }

                    if (Quaternion.Angle(selectedObjects[i].transform.localRotation, Quaternion.Euler(objData.defaultRotation)) < MAX_SNAP_ANGLE)
                    {
                        selectedObjects[i].transform.localRotation = Quaternion.Euler(objData.defaultRotation);
                    }
                }
            }
        }

        private void XrayListener(ObjectInteractionXRayEvent e)
        {
            ClearSelection();
        }

        private void SwitchSceneModeListener(SceneReadyEvent e)
        {
            ResetObjectPosition();

            if (device != null)
                device.Deinitialization();

            device = e.sceneConfig.behaviourPackage.objectSelector as ObjectSelectorDevice;
            device.Initialization(SelectObject, ClearSelection, GetSelected, AllowSelection, SnapNearObject);
        }

        private void Update()
        {
            if (device == null)
                return;

            if (isResetObjectActive)
            {
                for (int i = 0; i < toResetObjects.Count; i++)
                {
                    toResetObjects[i].ResetPosition(toResetObjects[i].lastPosition, toResetObjects[i].lastRotation, resetTransition);
                }

                resetTransition += Time.deltaTime * resetSpeed;

                if (resetTransition >= 1f)
                {
                    for (int i = 0; i < toResetObjects.Count; i++)
                    {
                        toResetObjects[i].transform.localPosition = toResetObjects[i].defaultPosition;
                        toResetObjects[i].transform.localRotation = Quaternion.Euler(toResetObjects[i].defaultRotation);
                    }

                    isResetObjectActive = false;
                    toResetObjects.Clear();
                    ClearSelection();
                }

                return;
            }

            // pull apart
            if (device.IsButtonDown() && isPullApartActive && selectedObjects.Count > 0)
            {
                VirtualTrainingCamera.IsPulling = true;
                for (int i = 0; i < selectedObjects.Count; i++)
                {
                    if (objectLookup.ContainsKey(selectedObjects[i].gameObject))
                    {
                        // get selected object data
                        ObjectData objectData = objectLookup[selectedObjects[i].gameObject];

                        // set object position from pointer position
                        device.SetObjectPosition(objectData.transform.gameObject, objectData.distanceToAnchor, objectData.offsetPosition);
                        device.SetObjectRotation(objectData.transform.gameObject, objectData.offsetRotation);

                        // set childs position to stay
                        for (int p = 0; p < objectData.childs.Count; p++)
                        {
                            if (!selectedObjects.Contains(objectData.childs[p]))
                            {
                                objectData.ChildsStay(p);
                            }
                        }
                    }
                }
            }
            else
                VirtualTrainingCamera.IsPulling = false;
        }

        public void SelectObject(int id, bool pullApartActive, bool multipleSelect)
        {
            isPullApartActive = pullApartActive;
            isMultipleSelectActive = multipleSelect;

            SelectObject(GameObjectReference.GetReference(id));
        }

        private void SelectObject(GameObjectReference gor)
        {
            if (gor == null)
            {
                ClearSelection();
                return;
            }

            if (!isMultipleSelectActive)
            {
                ClearSelectedColor();
                selectedObjects.Clear();
            }

            if (!objectLookup.ContainsKey(gor.gameObject))
                return;

            EventManager.TriggerEvent(new ObjectSelectionEvent(gor.Id));

            if (!selectedObjects.Contains(gor.gameObject))
            {
                selectedObjects.Add(gor.gameObject);
            }

            if (!movedObjects.Contains(gor.gameObject))
            {
                movedObjects.Add(gor.gameObject);
            }

            outline.Enable(gor.gameObject, false);

            // update information for all selected objects
            foreach (var selected in selectedObjects)
            {
                if (objectLookup.ContainsKey(selected))
                {
                    // set object data
                    ObjectData objData = objectLookup[selected.gameObject];

                    objData.distanceToAnchor = device.GetDistanceToAnchor(selected.gameObject);
                    objData.offsetPosition = device.GetOffsetPosition(selected.gameObject, objData.distanceToAnchor);
                    objData.offsetRotation = device.GetOffsetRotation(selected.gameObject);

                    // destroy stay pos
                    foreach (var stay in objectLookup[selected].childStayPositions)
                    {
                        Destroy(stay);
                    }

                    // clear object pos
                    objectLookup[selected].childStayPositions.Clear();

                    // add object pos
                    for (int i = 0; i < objData.childs.Count; i++)
                    {
                        GameObject pos = new GameObject("stay pos - " + objData.childs[i].name);
                        pos.transform.position = objData.childs[i].transform.position;
                        pos.transform.rotation = objData.childs[i].transform.rotation;
                        pos.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform, true);
                        objData.childStayPositions.Add(pos);
                        stayPositions.Add(pos);
                    }
                }
            }
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            resetSpeed = e.data.resetObjectSpeed;
        }

        private void MultipleSelectionListener(ObjectInteractionMultipleSelectionEvent e)
        {
            isMultipleSelectActive = e.on;
        }

        private void PullApartListener(ObjectInteractionPullApartEvent e)
        {
            isPullApartActive = e.on;
        }

        private void ResetPositionListener(ObjectInteractionResetObjectEvent e)
        {
            ClearSelectedColor();
            ResetObjectPosition();
        }

        private void VisibleListener(ObjectInteractionVisibleEvent e)
        {
            if (e.visible)
            {
                for (int i = 0; i < hidedObjects.Count; i++)
                {
                    hidedObjects[i].SetActive(true);
                    EventManager.TriggerEvent(new HideObjectEvent(false, hidedObjects[i]));
                }
                hidedObjects.Clear();
            }
            else
            {
                for (int i = 0; i < selectedObjects.Count; i++)
                {
                    selectedObjects[i].SetActive(false);
                    hidedObjects.Add(selectedObjects[i]);
                }

                if (hidedObjects.Count > 0)
                {
                    for (int i = 0; i < hidedObjects.Count; i++)
                    {
                        EventManager.TriggerEvent(new HideObjectEvent(true, hidedObjects[i]));
                    }
                }
            }
        }

        public void HideObject(int id)
        {
            GameObjectReference gor = GameObjectReference.GetReference(id);
            if (gor != null)
            {
                hidedObjects.Add(gor.gameObject);
                gor.gameObject.SetActive(false);

                ClearSelection();
            }
        }

        public void UnhideObjects()
        {
            foreach (var hided in hidedObjects)
            {
                hided.SetActive(true);
            }

            hidedObjects.Clear();
            ClearSelection();
        }

        private void ClearSelectedColor()
        {
            outline.Clear();
        }

        private void ResetObjectPosition()
        {
            resetTransition = 0f;

            for (int i = 0; i < movedObjects.Count; i++)
            {
                ResetObject(movedObjects[i]);
            }

            movedObjects.Clear();
            isResetObjectActive = true;
        }

        private void ResetObject(GameObject obj)
        {
            if (objectLookup.ContainsKey(obj))
            {
                ObjectData objData = objectLookup[obj];
                objData.lastPosition = objData.transform.localPosition;
                objData.lastRotation = objData.transform.localRotation.eulerAngles;
                toResetObjects.Add(objData);

                for (int i = 0; i < objData.childs.Count; i++)
                {
                    ResetObject(objData.childs[i]);
                }
            }
        }
    }
}

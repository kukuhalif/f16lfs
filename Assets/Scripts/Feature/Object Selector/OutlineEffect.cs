using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class OutlineEffect : InitializationQueue
    {
        public enum Mode
        {
            OutlineAll,
            OutlineVisible,
            OutlineHidden,
            OutlineAndSilhouette,
            SilhouetteOnly
        }

        class OutlineData
        {
            public GameObject gameObject;
            public int originalLayer;

            public OutlineData(GameObject gameObject, int layer)
            {
                this.gameObject = gameObject;
                //this.originalLayer = layer;
                this.originalLayer = 0;
            }
        }

        private List<OutlineData> outlined = new List<OutlineData>();

        [SerializeField] Material outlineMaskMaterial;
        [SerializeField] Material outlineFillMaterial;
        [SerializeField] private Mode outlineMode;

        int outlineLayer;

        OutlineNormalVector outlineData;

        private void Awake()
        {
            outlineData = DatabaseManager.GetOutlineNormalVector();
            outlineData.InitDataLookup();

            outlineLayer = LayerMask.NameToLayer("Outlined");

            Setup();
        }

        public override void Initialize()
        {

        }

        public override void ObjectSetup(GameObject obj)
        {
            MeshFilter meshFilter = obj.GetComponent<MeshFilter>();
            if (meshFilter != null)
            {
                if (meshFilter.sharedMesh != null)
                {
                    List<Vector3> smoothNormals = outlineData.GetSmoothNormal(meshFilter.sharedMesh);
                    meshFilter.sharedMesh.SetUVs(3, smoothNormals);
                }

                SkinnedMeshRenderer skinnedMeshRenderer = obj.GetComponent<SkinnedMeshRenderer>();
                if (skinnedMeshRenderer != null)
                {
                    skinnedMeshRenderer.sharedMesh.uv4 = new Vector2[skinnedMeshRenderer.sharedMesh.vertexCount];
                }
            }
        }

        void Setup()
        {
            outlineFillMaterial.SetColor("_OutlineColor", DatabaseManager.GetDefaultSelectedColor());

            switch (outlineMode)
            {
                case Mode.OutlineAll:
                    outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                    outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                    outlineFillMaterial.SetFloat("_OutlineWidth", DatabaseManager.GetOutlineWidth());
                    break;

                case Mode.OutlineVisible:
                    outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                    outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                    outlineFillMaterial.SetFloat("_OutlineWidth", DatabaseManager.GetOutlineWidth());
                    break;

                case Mode.OutlineHidden:
                    outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                    outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
                    outlineFillMaterial.SetFloat("_OutlineWidth", DatabaseManager.GetOutlineWidth());
                    break;

                case Mode.OutlineAndSilhouette:
                    outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                    outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                    outlineFillMaterial.SetFloat("_OutlineWidth", DatabaseManager.GetOutlineWidth());
                    break;

                case Mode.SilhouetteOnly:
                    outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                    outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
                    outlineFillMaterial.SetFloat("_OutlineWidth", 0);
                    break;
            }
        }

        public void Clear()
        {
            for (int i = 0; i < outlined.Count; i++)
            {
                //outlined[i].gameObject.layer = outlined[i].originalLayer;
                outlined[i].gameObject.layer = 0;
            }
            outlined.Clear();
        }

        private bool OutlineContain(GameObject go)
        {
            for (int i = 0; i < outlined.Count; i++)
            {
                if (go == outlined[i].gameObject)
                    return true;
            }

            return false;
        }

        public void Enable(GameObject obj, bool recursive)
        {
            if (!OutlineContain(obj))
            {
                outlined.Add(new OutlineData(obj, obj.layer));
                obj.layer = outlineLayer;
            }

            if (recursive)
            {
                Transform[] childs = obj.transform.GetAllChilds();
                for (int i = 0; i < childs.Length; i++)
                {
                    if (!OutlineContain((childs[i].gameObject)))
                    {
                        outlined.Add(new OutlineData(childs[i].gameObject, childs[i].gameObject.layer));
                        childs[i].gameObject.layer = outlineLayer;
                    }
                }
            }
        }
    }
}

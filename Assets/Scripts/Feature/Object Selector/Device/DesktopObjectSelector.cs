using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature.Desktop
{
    [CreateAssetMenu(fileName = "Desktop Object Selector Controller", menuName = "Virtual Training/Device/Desktop Object Selector Behaviour")]
    public class DesktopObjectSelector : ObjectSelectorDevice
    {
        float rotateSpeed;

        bool isPosition;
        bool isRotation;

        Vector2 deltaPosition;

        public override void Initialization(Action<GameObjectReference> selectObjectCallback, Action clearSelectionCallback, Func<List<GameObject>> getSelectedCallback, Func<bool> allowSelectionCallback, Action snapNearObjectCallback)
        {
            base.Initialization(selectObjectCallback, clearSelectionCallback, getSelectedCallback, allowSelectionCallback, snapNearObjectCallback);

            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

            isPosition = false;
            isRotation = false;

            VirtualTrainingInputSystem.OnStartLeftClick += StartLeftClickListener;
            VirtualTrainingInputSystem.OnEndLeftClick += EndLeftClickListener;

            VirtualTrainingInputSystem.OnStartRightClick += StartRightClickListener;
            VirtualTrainingInputSystem.OnEndRightClick += EndRightClickListener;

            VirtualTrainingInputSystem.OnMovePointer += OnMovePointer;

            rotateSpeed = SettingUtility.LastSetting.rotateSpeed;
        }

        public override void Deinitialization()
        {
            base.Deinitialization();
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            VirtualTrainingInputSystem.OnStartLeftClick -= StartLeftClickListener;
            VirtualTrainingInputSystem.OnEndLeftClick -= EndLeftClickListener;
            VirtualTrainingInputSystem.OnStartRightClick -= StartRightClickListener;
            VirtualTrainingInputSystem.OnEndRightClick -= EndRightClickListener;
            VirtualTrainingInputSystem.OnMovePointer -= OnMovePointer;
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            rotateSpeed = e.data.rotateSpeed;
        }

        private void StartRightClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null)
                return;

            if (SelectObject())
            {
                isRotation = true;
            }
            else if (VirtualTrainingInputSystem.TouchCount == 0)
            {
                isPosition = false;
                isRotation = false;
            }
        }

        private void EndRightClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isRotation = false;
            snapNearObjectCallback.Invoke();
        }

        private void StartLeftClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null)
                return;

            if (SelectObject())
            {
                isPosition = true;
            }
            else if (VirtualTrainingInputSystem.TouchCount == 0)
            {
                isPosition = false;
                isRotation = false;
            }
        }

        private void EndLeftClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isPosition = false;
            snapNearObjectCallback.Invoke();
        }

        private bool SelectObject()
        {
            if (!allowSelectionCallback.Invoke())
            {
                selectObjectCallback.Invoke(null);
                return false;
            }

            RaycastHit hitInfo = new RaycastHit();

            if (VirtualTrainingCamera.Raycast(ref hitInfo))
            {
                GameObjectReference gor = hitInfo.collider.GetComponent<GameObjectReference>();
                selectObjectCallback.Invoke(gor);
                return true;
            }
            else
                selectObjectCallback.Invoke(null);

            return false;
        }

        private void OnMovePointer(Vector2 deltaPosition)
        {
            this.deltaPosition = deltaPosition;
        }

        public override bool IsButtonDown()
        {
            int touchCount = VirtualTrainingInputSystem.TouchCount;

            if (touchCount == 1)
                isRotation = false;
            else if (touchCount == 2)
                isPosition = false;

            return isPosition || isRotation;
        }

        public override float GetDistanceToAnchor(GameObject obj)
        {
            return Vector3.Distance(VirtualTrainingCamera.CameraPosition, obj.transform.position);
        }

        public override Vector3 GetOffsetPosition(GameObject obj, float distanceToAnchor)
        {
            Vector3 pointerScreenPosition = new Vector3(VirtualTrainingInputSystem.PointerPosition.x, VirtualTrainingInputSystem.PointerPosition.y, distanceToAnchor);
            Vector3 pointerWorldPosition = VirtualTrainingCamera.CurrentCamera.ScreenToWorldPoint(pointerScreenPosition);
            return obj.transform.position - pointerWorldPosition;
        }

        public override void SetObjectPosition(GameObject obj, float distanceToAnchor, Vector3 offsetPos)
        {
            if (!isPosition)
                return;

            Vector2 pointerPosition = VirtualTrainingInputSystem.PointerPosition;
            Vector3 pointerScreenPosition = new Vector3(pointerPosition.x, pointerPosition.y, distanceToAnchor);
            Vector3 pointerWorldPosition = VirtualTrainingCamera.CurrentCamera.ScreenToWorldPoint(pointerScreenPosition);
            obj.transform.position = pointerWorldPosition + offsetPos;
        }

        public override Vector3 GetOffsetRotation(GameObject obj)
        {
            return VirtualTrainingCamera.CameraTransform.rotation.eulerAngles - obj.transform.rotation.eulerAngles;
        }

        public override void SetObjectRotation(GameObject obj, Vector3 rotationOffset)
        {
            if (!isRotation)
                return;

            float rotX = deltaPosition.x * rotateSpeed;
            float rotY = deltaPosition.y * rotateSpeed;

            obj.transform.Rotate(VirtualTrainingCamera.CameraTransform.up, -rotX, Space.World);
            obj.transform.Rotate(VirtualTrainingCamera.CameraTransform.right, rotY, Space.World);
        }
    }
}
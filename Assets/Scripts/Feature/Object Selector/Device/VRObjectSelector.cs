using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System;
using Valve.VR;

namespace VirtualTraining.Feature.VR
{
    [CreateAssetMenu(fileName = "VR Object Selector Controller", menuName = "Virtual Training/Device/VR Object Selector Behaviour")]
    public class VRObjectSelector : ObjectSelectorDevice
    {
        public bool isPress;
        public bool isTouch;
        public GameObject handTouchObject;
        public GameObject helperObject;
        bool isPullApartActive;
        bool isMultipleActive;

        public GameObjectReference gorTemp;
        PartNumberComponent partNumber;

        [SerializeField] SteamVR_Action_Vector2 joystick;
        [SerializeField] SteamVR_Action_Boolean trigger;

        float translationSpeed = 5;
        float rotationSpeed = 50;

        float distanceAdd;
        int rotationAdd;

        public override void Initialization(Action<GameObjectReference> selectObjectCallback, Action clearSelectionCallback, Func<List<GameObject>> getSelectedCallback, Func<bool> allowSelectionCallback, Action snapNearObjectCallback)
        {
            base.Initialization(selectObjectCallback, clearSelectionCallback, getSelectedCallback, allowSelectionCallback, snapNearObjectCallback);
            EventManager.AddListener<ObjectInteractionPullApartEvent>(PullApartListener);
            EventManager.AddListener<ObjectInteractionMultipleSelectionEvent>(MultipleListener);
            isPress = false;
            isTouch = false;
            isPullApartActive = false;
            gorTemp = null;
        }

        public override void Deinitialization()
        {
            base.Deinitialization();
            EventManager.RemoveListener<ObjectInteractionPullApartEvent>(PullApartListener);
            EventManager.RemoveListener<ObjectInteractionMultipleSelectionEvent>(MultipleListener);
            gorTemp = null;
        }
        private void PullApartListener(ObjectInteractionPullApartEvent e)
        {
            isPullApartActive = e.on;
        }

        private void MultipleListener(ObjectInteractionMultipleSelectionEvent e)
        {
            isMultipleActive = e.on;
        }

        public void VRStartSelection()
        {
            RaycastHit hitInfo = new RaycastHit();

            if (VirtualTrainingCamera.Raycast(ref hitInfo))
            {
                if (isPullApartActive)
                {
                    VirtualTrainingCamera.DistanceVR = 1;
                    GameObjectReference gor = hitInfo.collider.GetComponent<GameObjectReference>();
                    if (gor != null)
                    {
                        selectObjectCallback.Invoke(gor);
                        isPress = true;
                    }
                }
            }
            else
            {
                ClearSelection();
                selectObjectCallback.Invoke(null);
            }
        }

        public void VRHoverObject()
        {
            RaycastHit hitInfo = new RaycastHit();

            if (VirtualTrainingCamera.Raycast(ref hitInfo))
            {
                if (hitInfo.collider.gameObject != null)
                {
                    if (partNumber != null)
                    {
                        if (hitInfo.collider.gameObject != partNumber)
                            HidePartNumber();
                    }

                    ShowPartNumber(hitInfo.collider.gameObject);
                }

                if (isPullApartActive)
                {
                    //VirtualTrainingCamera.DistanceVR = 1;
                    GameObjectReference gor = hitInfo.collider.GetComponent<GameObjectReference>();
                    if (gor != null)
                    {
                        ResetGorTemp();

                        gorTemp = gor;
                        gorTemp.gameObject.layer = 10;
                    }
                    else
                    {
                        ResetGorTemp();
                    }
                }
                else
                {
                    ResetGorTemp();
                }
            }
            else
            {
                HidePartNumber();
                ResetGorTemp();
            }
        }

        void ShowPartNumber(GameObject go)
        {
            if (go.GetComponent<PartNumberComponent>() != null)
            {
                partNumber = go.GetComponent<PartNumberComponent>();
                partNumber.Show();
            }
        }

        void HidePartNumber()
        {
            if (partNumber != null)
            {
                partNumber.Hide();
                partNumber = null;
            }
        }

        void ResetGorTemp()
        {
            if (gorTemp != null)
            {
                if (isMultipleActive)
                {
                    gorTemp.gameObject.layer = 1;
                    gorTemp = null;

                    for (int i = 0; i < GetSelected().Count; i++)
                    {
                        GetSelected()[i].gameObject.layer = 10;
                    }
                }
                else
                {
                    gorTemp.gameObject.layer = 1;
                    gorTemp = null;
                }

            }
        }

        public void VREndSelection()
        {
            isPress = false;
            snapNearObjectCallback.Invoke();
        }
        public override float GetDistanceToAnchor(GameObject obj)
        {
            return Vector3.Distance(VirtualTrainingCamera.handVR.position, obj.transform.position);
        }

        public override Vector3 GetOffsetPosition(GameObject obj, float distanceToAnchor)
        {
            distanceAdd = 0;
            rotationAdd = 0;
            Vector3 pointerWorldPosition = VirtualTrainingCamera.handVR.position + (VirtualTrainingCamera.handVR.forward * distanceToAnchor);
            return obj.transform.position - pointerWorldPosition;
        }

        public override void SetObjectPosition(GameObject obj, float distanceToAnchor, Vector3 offsetPos)
        {
            Vector3 pointerWorldPosition = VirtualTrainingCamera.handVR.position;
            float x = VirtualTrainingCamera.DistanceVR * distanceToAnchor;
            if (isTouch && handTouchObject != null)
            {
                Debug.Log("get postion with touch ");
                pointerWorldPosition = handTouchObject.transform.position + offsetPos;

                if (helperObject != null)
                    pointerWorldPosition = helperObject.transform.position;

                obj.transform.position = pointerWorldPosition;
            }
            else
            {
                if (trigger.state)
                {
                    if (joystick.axis.y < -0.5)
                    {
                        distanceAdd -= Time.deltaTime * translationSpeed;
                    }
                    else if (joystick.axis.y > 0.5)
                    {
                        distanceAdd += Time.deltaTime * translationSpeed;
                    }
                }

                obj.transform.position = pointerWorldPosition + (VirtualTrainingCamera.handVR.forward * (x + distanceAdd)) + offsetPos;
            }
        }

        public override Vector3 GetOffsetRotation(GameObject obj)
        {
            distanceAdd = 0;
            rotationAdd = 0;
            return Vector3.zero;
        }

        public override void SetObjectRotation(GameObject obj, Vector3 rotationOffset)
        {
            if (isTouch && handTouchObject != null && helperObject != null)
            {
                //Debug.Log("Euler not asa " + rotationOffset);
                //Vector3 testEuler = helperObject.transform.eulerAngles + VirtualTrainingCamera.handVR.eulerAngles;
                obj.transform.rotation = Quaternion.Euler(helperObject.transform.eulerAngles);
            }
            else
            {
                if (trigger.state)
                {
                    if (joystick.axis.x < -0.5)
                    {
                        rotationAdd = 1;
                    }
                    else if (joystick.axis.x > 0.5)
                    {
                        rotationAdd = -1;
                    }
                    else
                    {
                        rotationAdd = 0;
                    }
                }
                else
                {
                    rotationAdd = 0;
                }

                obj.transform.Rotate(0, rotationAdd * Time.deltaTime * rotationSpeed, 0, Space.World);
            }
        }

        public void SelectObject(GameObjectReference obj)
        {
            selectObjectCallback?.Invoke(obj);
        }

        public void ClearSelection()
        {
            clearSelectionCallback?.Invoke();
        }

        public List<GameObject> GetSelected()
        {
            if (getSelectedCallback == null)
                return new List<GameObject>();

            return getSelectedCallback.Invoke();
        }

        public override bool IsButtonDown()
        {
            return isPress;
        }
    }
}

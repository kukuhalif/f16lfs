using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;

namespace VirtualTraining.Feature
{
    public class ObjectFieldRecordingHelper : MonoBehaviour
    {
        public Component component;

        [Dropdown(nameof(GetVariables))]
        public string[] fieldNames;

        private string[] GetVariables()
        {
            FieldInfo[] compFields;
            string[] compFieldNames;
            string[] recordableCompFieldNames;

            // get all public fields of components
            compFields = component.GetType().GetFields();
            // create array of public field names
            compFieldNames = Array.ConvertAll(compFields, t => t.Name);
            // filter array of public field names with recordable prefix only
            recordableCompFieldNames = Array.FindAll(compFieldNames, s => s.Contains("recordable", StringComparison.Ordinal));
            return recordableCompFieldNames;
        }

    }
}
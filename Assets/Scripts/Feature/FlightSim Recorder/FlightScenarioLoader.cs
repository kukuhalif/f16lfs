using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.EventSystems;
using Mewlist.MassiveClouds;
using Oyedoyin.Common;
using Oyedoyin.FixedWing;
using HelicopterSim;
//using AirplaneSim;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using VirtualTraining.Feature.Desktop;
using UnityEngine.UI;
//using VirtualTraining.Feature;

namespace VirtualTraining.Feature
{
    public class FlightScenarioLoader : SceneModeManager
    {
        FixedController airplaneController;
        HelicopterController helicopterController;
#if UNITY_EDITOR
        FlightSimRecorder animationRecorder;
#endif
        FlightSimRecordingPlayer animationPlayer;
        FlightScenarioPlayer scenarioPlayer;
        AnimatorRecordingHelper[] animRecordingHelpers;
        HelicopterCamera helicopterCamera;
        SilantroCamera silantroCamera;
        //Camera monitorCamera;

        private void Awake()
        {
            // TODO: Listen to SceneReadyEvent event to setup template integration
            //EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
            EventManager.AddListener<FlightScenarioPreparePlayEvent>(FlightScenarioPreparePlayListener);
            //EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
        }

        private void OnDestroy()
        {
            //EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
            EventManager.RemoveListener<FlightScenarioPreparePlayEvent>(FlightScenarioPreparePlayListener);
            //EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
        }

        private void Start()
        {

            /*MateriTreeElement materiTreeElement = VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT;
            if (materiTreeElement == null)
                return;

            if (materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements.Count > 0)
                _recData = materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements[0].data;

            if (_recData == null)
            {
                Debug.LogError("flight scenario data is null");
                return;
            }*/

            //_instance.SetData();
            //animationPlayer.playableDirector.Play();
            //scenarioPlayer.playableDirector.Pause();

            //SceneReady();

            //EventManager.TriggerEvent(new CloseAllUIPanelEvent());
            //EventManager.TriggerEvent(new FlightScenarioUIEvent(materiTreeElement.MateriData));

            //VirtualTrainingCamera.SetCurrentMonitorCamera(monitorCamera);
            //EventManager.TriggerEvent(new MonitorCameraEvent());

        }

        private void FlightScenarioPreparePlayListener(FlightScenarioPreparePlayEvent e)
        {
            EventManager.RemoveListener<FlightScenarioPreparePlayEvent>(FlightScenarioPreparePlayListener);

            PreparePlaying();
        }

        /*private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);

            //if (VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT == null)
            //{
            //    return;
            //}

            PreparePlaying();
        }*/

        private void PreparePlaying()
        {
            //GameObject scenarioLoader;
            //FlightScenarioCameraController[] flightScenarioCameraControllers;
            //FlightScenarioMonitorCamera[] flightScenarioMonitorCameras;

            // disable local event system
            //if (localEventSystem != null)
            //    localEventSystem.gameObject.SetActive(false);

            // disable animation recorder
#if UNITY_EDITOR
            animationRecorder = FindObjectOfType<FlightSimRecorder>(true);
            if (animationRecorder != null)
            {
                animationRecorder.gameObject.SetActive(false);
                animationRecorder.enabled = false;
            }
#endif

            // enable animation player
            animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>(true);
            if (animationPlayer != null)
            {
                animationPlayer.gameObject.SetActive(true);
                animationPlayer.enabled = true;
                animationPlayer.playableDirector.enabled = true;
                // pause animation player playable director
                animationPlayer.playableDirector.Pause();
            }

            // enable flight scenario player
            scenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);
            if (scenarioPlayer != null)
            {
                scenarioPlayer.gameObject.SetActive(true);
                scenarioPlayer.enabled = true;
                scenarioPlayer.playableDirector.enabled = true;
                // pause flight scenario player playable director
                scenarioPlayer.playableDirector.Pause();
            }

            // enable flight scenario camera controller & flight scenario monitor camera
            FlightScenarioCameraController[] flightScenarioCameraControllers = FindObjectsOfType<FlightScenarioCameraController>(true);
            foreach (FlightScenarioCameraController controller in flightScenarioCameraControllers)
                controller.enabled = true;

            FlightScenarioMonitorCamera[] flightScenarioMonitorCameras = FindObjectsOfType<FlightScenarioMonitorCamera>(true);
            foreach (FlightScenarioMonitorCamera monitor in flightScenarioMonitorCameras)
                monitor.enabled = true;

            // disable helicopter camera control & airplane camera control
            helicopterCamera = FindObjectOfType<HelicopterCamera>(true);
            if (helicopterCamera != null)
                helicopterCamera.enabled = false;
            silantroCamera = FindObjectOfType<SilantroCamera>(true);
            if (silantroCamera != null)
                silantroCamera.enabled = false;

            // setup aircraft for animation playing
            helicopterController = FindObjectOfType<HelicopterController>(true);
            if (helicopterController != null)
            {
                helicopterController.gameObject.SetActive(true);
                helicopterController.m_playMode = HelicopterController.PlayMode.Animation;

                // enable kinematic (disable physics) on helicopter
                Rigidbody rb = helicopterController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = true;
                }

                // disable lever components to prevent conflicts with timeline player
                HelicopterLever[] levers = helicopterController.gameObject.GetComponentsInChildren<HelicopterLever>();
                foreach (HelicopterLever lever in levers)
                {
                    lever.enabled = false;
                }
            }

            airplaneController = FindObjectOfType<FixedController>(true);
            if (airplaneController != null)
            {
                airplaneController.gameObject.SetActive(true);
                airplaneController.m_playMode = FixedController.PlayMode.Animation;

                // enable kinematic (disable physics) on airplane
                Rigidbody rb = airplaneController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = true;
                }

                // disable lever components to prevent conflicts with timeline player
                SilantroLever[] levers = airplaneController.gameObject.GetComponentsInChildren<SilantroLever>();
                foreach (SilantroLever lever in levers)
                {
                    lever.enabled = false;
                }
            }

            // set animator recording helper mode to Play Animation
            animRecordingHelpers = FindObjectsOfType<AnimatorRecordingHelper>(true);
            foreach (AnimatorRecordingHelper animRecordingHelper in animRecordingHelpers)
            {
                animRecordingHelper.helperMode = AnimatorRecordingHelper.HelperMode.PlayAnimation;
            }

            // set slider for AnimationManager
            AnimationManager animationManager = FindObjectOfType<AnimationManager>(true);
            Slider slider = FindObjectOfType<Slider>(true);
            if (animationManager != null && slider != null)
            {
                slider.enabled = true; 
                animationManager.SetSlider(slider, null, null);
            }
        }
    }
}
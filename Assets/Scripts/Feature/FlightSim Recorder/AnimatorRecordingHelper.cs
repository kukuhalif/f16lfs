using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor.Animations;
#endif

namespace VirtualTraining.Feature
{
    //[ExecuteInEditMode]
    //[RequireComponent(typeof(Animator))]
    public class AnimatorRecordingHelper : MonoBehaviour
    {
        [System.Serializable]
        public class AnimParam
        {
            [ReadOnly] public string name = "";
            public bool recordable = false;
        }

        public enum HelperMode { RecordAnimation, PlayAnimation }
        public HelperMode helperMode = HelperMode.RecordAnimation;
        public Animator animatorToRecord;
        //[ReadOnly] public AnimatorController animatorController;

        public List<AnimParam> animParams = new List<AnimParam>();
        //[Tooltip("Maximum number of parameters = 32!")]
        //public AnimParam[] animParams = new AnimParam[32];

        // Hardcode variables. Gameobject recorder record basic variable only...
        //[ReadOnly] public float param0, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15;
        [ReadOnly] public float prop_speed, prop_verticalSpeed, prop_altitude, prop_altitudeX, prop_altitudeY, prop_heading, prop_headingX, prop_headingY;
        [ReadOnly] public float prop_horizonPitch, prop_horizonPitchX, prop_horizonPitchY, prop_horizonRoll, prop_horizonRollX, prop_horizonRollY;
        [ReadOnly] public float prop_bankAngle, prop_bankAngleX, prop_bankAngleY;
        [ReadOnly] public float prop_throttle, prop_throttle1, prop_throttle2, prop_throttle3, prop_throttle4;
        [ReadOnly] public float prop_engTemp, prop_eng1Temp, prop_eng2Temp, prop_eng3Temp, prop_eng4Temp;
        [ReadOnly] public float prop_engPress, prop_eng1Press, prop_eng2Press, prop_eng3Press, prop_eng4Press;
        [ReadOnly] public float prop_engHyd, prop_eng1Hyd, prop_eng2Hyd, prop_eng3Hyd, prop_eng4Hyd;
        [ReadOnly] public float prop_fuel, prop_fuel1, prop_fuel2, prop_fuel3, prop_fuel4;
        [ReadOnly] public float prop_rpm, prop_rpm1, prop_rpm2, prop_rpm3, prop_rpm4;
        [ReadOnly] public float prop_cyclicPitch, prop_cyclicRoll, prop_collective, prop_pedal, prop_yokePitch, prop_yokeRoll, prop_flap, prop_trim;
        [ReadOnly] public float prop_door, prop_door1, prop_door2, prop_door3, prop_door4;

        public AnimatorRecordingHelper()
        {
            // init animParam array
            /*for (int i = 0; i < 32; i++)
            {
                AnimParam animParam = new AnimParam();
                animParam.name = "";
                animParam.recordable = false;
                animParams[i] = animParam;
            }*/

            /*// Get animator
            Animator anim = gameobject.GetComponent<Animator>();
            // Get animator controller
            AnimatorController animController = anim.runtimeAnimatorController as AnimatorController;
            // Get animator parameters
            AnimatorControllerParameter[] animatorParams = animController.parameters;
            //int idx = 0;
            foreach (AnimatorControllerParameter param in animatorParams)
            {
                //animRecordingHelper.animParams[idx].name = param.name;
                //idx++;
                AnimParam animParam = new AnimParam();
                animParam.name = param.name;
                animParam.recordable = false;
                animParams.Add(animParam);
            }*/
        }


        void Awake()
        {
            //animator = gameObject.GetComponentInParent<Animator>();
            animatorToRecord = transform.parent.GetComponent<Animator>();
            if (animatorToRecord != null)
            {
#if UNITY_EDITOR
                // Get animator controller
                AnimatorController animController = animatorToRecord.runtimeAnimatorController as AnimatorController;
                // Get animator parameters
                AnimatorControllerParameter[] animatorParams = animController.parameters;
#else
                // Get animator parameters
                AnimatorControllerParameter[] animatorParams = animatorToRecord.parameters;            
#endif
                //int idx = 0;
                /*foreach (AnimatorControllerParameter param in animatorParams)
                {
                    //animRecordingHelper.animParams[idx].name = param.name;
                    //idx++;
                    AnimParam animParam = new AnimParam();
                    animParam.name = param.name;
                    animParam.recordable = false;
                    animParams.Add(animParam);
                }*/
            }
            else
                Debug.LogWarning("Animator Recording Helper need Animator component target to record.");
        }

        // Update is called once per frame
        void Update()
        {
            if (animatorToRecord != null && animParams.Count > 0)
            {
                switch (helperMode)
                {
                    case HelperMode.RecordAnimation:
                        {
                            for (int p = 0; p < animParams.Count; p++)
                            {
                                if (animParams[p].recordable)
                                {
                                    if (GetType().GetField("prop_" + animParams[p].name) != null)
                                    {
                                        GetType().GetField("prop_" + animParams[p].name).SetValue(this, animatorToRecord.GetFloat(animParams[p].name));
                                    }
                                    else
                                    {
                                        Debug.LogWarning($"Recording property prop_{animParams[p].name} is not found!");
                                    }
                                }
                            }
                            break;
                        }
                    case HelperMode.PlayAnimation:
                        {
                            for (int p = 0; p < animParams.Count; p++)
                            {
                                if (animParams[p].recordable)
                                {
                                    if (GetType().GetField("prop_" + animParams[p].name) != null)
                                    {
                                        //Debug.Log("param " + p.ToString() + ": " + (float)GetType().GetField("param" + p.ToString()).GetValue(this));
                                        animatorToRecord.SetFloat(animParams[p].name, (float)GetType().GetField("prop_" + animParams[p].name).GetValue(this));
                                    }
                                    else
                                    {
                                        Debug.LogWarning($"Recording property prop_{animParams[p].name} is not found!");
                                    }
                                }
                            }
                            break;
                        }
                }
            }
        }
    }
}

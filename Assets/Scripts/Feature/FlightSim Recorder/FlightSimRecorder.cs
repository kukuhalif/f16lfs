#if UNITY_EDITOR
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine.Timeline;
using VirtualTraining.Core;
using UnityEngine.Playables;
using Mono.Cecil;

namespace VirtualTraining.Feature
{
    public class FlightSimRecorder : MonoBehaviour
    {
        [ReadOnly] public List<GameObjectRecorder> _recorder;

        //[Tooltip("Must start with Assets/")]
        [ReadOnly] public string _saveFolderLocation;
        [ReadOnly] public string _recordingPrefix;
        //[ReadOnly] public string _clipPrefix;

        [ReadOnly] public int _skipFrame = 10;

        [ReadOnly] public List<FlightSimRecordingItem> _animationItems;

        [ReadOnly] public AircraftType _aircraftType = AircraftType.Helicopter;
        [ReadOnly] public AircraftMode _aircraftMode = AircraftMode.Hot;
        [ReadOnly] public Vector2 _aircraftLocation = new Vector2(0f, 0f);
        [ReadOnly] public float _aircraftAltitude = 100f;
        [ReadOnly] public float _aircraftSpeed = 100f;
        [ReadOnly] public float _aircraftVerticalSpeed = 10f;
        [ReadOnly] public float _aircraftHeading = 0f;

        [ReadOnly] public float _environmentTime = 12f;
        [ReadOnly] public float _environmentCloud = 0.5f;
        //[ReadOnly] public GameObject _cityLights;
        [ReadOnly] public bool _cityLightsStatus = false;

        [ReadOnly] public bool _canRecord;

        [ReadOnly] public int _selectedIndex;
        [ReadOnly] public TimelineAsset _scenarioTimeline;

        int frameCounter = 0;
        float timeCounter = 0f;

        FlightSimRecordingPlayer animationPlayer;
        FlightScenarioPlayer scenarioPlayer;

        //[ReadOnly] public bool _isScenarioCreationMode; // = true;

        /*private void Awake()
        {
            //_canRecord = false;
        }

        private void OnEnable()
        {
            foreach (AnimationItem anim in animationItem)
            {
                //CreateNewClip();
                anim.animationClip = new AnimationClip();
            }

            var savedIndex = PlayerPrefs.GetInt(_gameObject.name + "Index");

            if (savedIndex != 0)
            {
                _index = savedIndex;
            }
        }*/

        void Start()
        {
            animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>();
            scenarioPlayer = FindObjectOfType<FlightScenarioPlayer>();
            if (_canRecord) StartRecording();
        }

        public void StartRecording()
        {
            //_canRecord = true;

            _recorder = new List<GameObjectRecorder>();
            Debug.Log("component AnimationsRecorder ID: " + this.GetInstanceID().ToString());
            //var int idx = 0;
            foreach (FlightSimRecordingItem anim in _animationItems)
            {
                if (anim.animatedObject == null)
                {
                    //Debug.LogError("Animation item's GameObject cannot be empty");
                    Debug.LogError("Animation item's GameObjectReference cannot be empty");
                    continue;
                }

                GameObjectRecorder rec = new GameObjectRecorder(anim.animatedObject.gameObject);
                switch (anim.animationType)
                {
                    case FlightSimRecordingItem.FlightSimRecordingType.Transform:
                        rec.BindComponentsOfType<Transform>(anim.animatedObject.gameObject, anim.recursive);
                        break;
                    case FlightSimRecordingItem.FlightSimRecordingType.AnimatorParameter:
                        //rec.BindComponentsOfType<Animator>(anim.animatedObject, anim.recursive);
                        //Animator animator = anim.animatedObject.GetComponent<Animator>();
                        //float paramValue = animator.GetFloat(anim.animationParameterName);
                        AnimatorRecordingHelper animParamRecorder = anim.animatedObject.gameObject.GetComponent<AnimatorRecordingHelper>();
                        if (animParamRecorder == null)
                        {
                            Debug.LogError("Animation Parameter Recording Helper component is not found!");
                            continue;
                        }
                        EditorCurveBinding paramBinding;
                        for (int idx = 0; idx < animParamRecorder.animParams.Count; idx++)
                        {
                            if (animParamRecorder.animParams[idx].recordable)
                            {
                                paramBinding = EditorCurveBinding.FloatCurve("", typeof(AnimatorRecordingHelper), "prop_" + animParamRecorder.animParams[idx].name);
                                rec.Bind(paramBinding);
                            }
                        }
                        break;
                    case FlightSimRecordingItem.FlightSimRecordingType.ObjectField:
                        ObjectFieldRecordingHelper objectFieldRecorder = anim.animatedObject.gameObject.GetComponent<ObjectFieldRecordingHelper>();
                        if (objectFieldRecorder == null)
                        {
                            Debug.LogError("Object Property Recording Helper component is not found!");
                            continue;
                        }
                        EditorCurveBinding objPropBinding;
                        for (int idx = 0; idx < objectFieldRecorder.fieldNames.Length; idx++)
                        {
                            Type compType = objectFieldRecorder.component.GetType();
                            objPropBinding = EditorCurveBinding.FloatCurve("", compType, objectFieldRecorder.fieldNames[idx]);
                            rec.Bind(objPropBinding);
                        }
                        break;
                }
                _recorder.Add(rec);

                //anim.animationClip = new AnimationClip();
                //anim.animationClip.name = _clipPrefix + "_" + anim.animatedObject.name;
                Debug.Log("Animation Recording for " + anim.animatedObject.gameObject.name + " has STARTED");
                Debug.Log("Start Recording: num of recorders: " + _recorder.Count().ToString());

                /*GameObject go = anim.animatedObject; // GameObject.Find(anim.animationClip.name);
                Rigidbody rb = go.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    // disable kinematic if gameobject has rigidbody component
                    rb.isKinematic = false;
                }*/
            }
        }

        public void StopRecording()
        {
            _canRecord = false;

            //create anim folder
            /*string guid = AssetDatabase.CreateFolder(_saveFolderLocation, _clipFolder);
            string newFolderPath = AssetDatabase.GUIDToAssetPath(guid);
            char[] splitter = { '/' };
            var newFolderName = newFolderPath.Split(splitter).Last();
            */
            //var assetPath = newFolderPath + "/" + newFolderName + ".asset";

            string newAssetPath = AssetDatabase.GenerateUniqueAssetPath(_saveFolderLocation + "/" + _recordingPrefix + ".asset");
            string newAssetName = Path.GetFileNameWithoutExtension(newAssetPath);

            Debug.Log("StopRecording: num of recorders: " + _recorder.Count().ToString());

            // create animation records (scriptable object)
            FlightSimRecordingData animRecData = ScriptableObject.CreateInstance<FlightSimRecordingData>();
            animRecData.clipDatas = new List<ClipData>();

            // create recording timeline asset
            TimelineAsset timeline = ScriptableObject.CreateInstance<TimelineAsset>();
            timeline.name = newAssetName + "_timeline";
            //AssetDatabase.CreateAsset(timeline, newFolderPath + "/" + newFolderName + "_timeline.playable");

            // create default scenario timeline asset
            TimelineAsset scenarioTimeline = ScriptableObject.CreateInstance<TimelineAsset>();
            scenarioTimeline.name = "Scenario-" + newAssetName;

            // set timeline asset reference
            Debug.Log("TimelineAsset: " + timeline.ToString());
            animRecData.timeline = timeline;
            animRecData.scenarioTimeline = scenarioTimeline;

            // create Animation Record Data asset
            animRecData.recordingName = newAssetName;
            //animRecData.clipPrefix = _clipPrefix;

            animRecData.aircraftType = _aircraftType;
            animRecData.aircraftMode = _aircraftMode;
            animRecData.aircraftLocation = _aircraftLocation;
            animRecData.aircraftAltitude = _aircraftAltitude;
            animRecData.aircraftSpeed = _aircraftSpeed;
            animRecData.aircraftVerticalSpeed = _aircraftVerticalSpeed;
            animRecData.aircraftHeading = _aircraftHeading;

            animRecData.environmentTime = _environmentTime;
            animRecData.environmentCloud = _environmentCloud;
            //animRecData.cityLightObjectName = _cityLights.gameObject.name;
            animRecData.cityLightsStatus = _cityLightsStatus;

            // save animation record data asset
            //AssetDatabase.CreateAsset(animRecData, newFolderPath + "/" + newFolderName + ".asset");
            AssetDatabase.CreateAsset(animRecData, newAssetPath);

            // add timeline as asset object
            AssetDatabase.AddObjectToAsset(timeline, animRecData);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(timeline));

            // loop to all animation clip created
            var idx = 0;
            foreach (FlightSimRecordingItem anim in _animationItems)
            {
                Debug.Log("Animation Recording for " + anim.animatedObject.gameObject.name + " has STOPPED");
                EditorCurveBinding[] binding = _recorder[idx].GetBindings();
                Debug.Log("Recorder binding " + binding.ToString());

                // create timeline animation track
                AnimationTrack track = timeline.CreateTrack<AnimationTrack>(anim.animatedObject.gameObject.name);
                Debug.Log("animationTrack: " + track.ToString());

                // create clip data
                ClipData clipData = new ClipData(anim.animatedObject.gameObject.name, anim.animatedObject, anim.animationType, null);

                // create keyframe reduction options
                CurveFilterOptions compressOptions = new CurveFilterOptions()
                {
                    keyframeReduction = true,
                    unrollRotation = true,
                    positionError = 0.5f,
                    rotationError = 0.5f,
                    scaleError = 0.5f,
                    floatError = 0.5f
                };

                // save animation record to animation clip
                string clipType = "";
                switch (anim.animationType)
                {
                    case FlightSimRecordingItem.FlightSimRecordingType.Transform:
                        clipType = "TR";
                        break;
                    case FlightSimRecordingItem.FlightSimRecordingType.AnimatorParameter:
                        clipType = "AP";
                        break;
                    case FlightSimRecordingItem.FlightSimRecordingType.ObjectField:
                        clipType = "OF";
                        break;
                }
                clipData.clipAnim = new AnimationClip();
                clipData.clipAnim.name = newAssetName + "_" + clipType + "_" + anim.animatedObject.gameObject.name;
                _recorder[idx].SaveToClip(clipData.clipAnim, 60f, compressOptions);

                // add animation clip data to recording data
                animRecData.clipDatas.Add(clipData);

                // add animation clip to timeline clip
                TimelineClip timelineClip = track.CreateClip(clipData.clipAnim);
                Debug.Log("timelineClip: " + timelineClip.ToString());
                timelineClip.duration = clipData.clipAnim.length;
                AnimationPlayableAsset animationPlayableAsset = timelineClip.asset as AnimationPlayableAsset;
                animationPlayableAsset.removeStartOffset = false;

                /*// save animation record to animation clip
                anim.animationClip = new AnimationClip();
                anim.animationClip.name = _clipPrefix + "_" + anim.animatedObject.name;
                _recorder[idx].SaveToClip(anim.animationClip);

                // add animation clip to timeline clip
                TimelineClip timelineClip = track.CreateClip(anim.animationClip);
                Debug.Log("timelineClip: " + timelineClip.ToString());
                timelineClip.duration = anim.animationClip.length;
                AnimationPlayableAsset animationPlayableAsset = timelineClip.asset as AnimationPlayableAsset;
                animationPlayableAsset.removeStartOffset = false;            

                Debug.Log("New Folder Path: " + newFolderPath);
                string animPath = newFolderPath + "/" + anim.animationClip.name + ".anim";
                AssetDatabase.CreateAsset(anim.animationClip, animPath);
                */

                //Debug.Log("New Folder Path: " + newFolderPath);
                //string animPath = newFolderPath + "/" + clipData.clipAnim.name + ".anim";

                //AssetDatabase.CreateAsset(clipData.clipAnim, animPath);
                //add animation clip as asset object
                AssetDatabase.AddObjectToAsset(clipData.clipAnim, animRecData);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(clipData.clipAnim));

                idx++;
            }

            // save new default scenario timeline asset
            string scenarioAssetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/Content Files/Scenarios/Scenario-" + newAssetName + ".playable");
            AssetDatabase.CreateAsset(scenarioTimeline, scenarioAssetPath);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(scenarioTimeline));

            // ------------------ CREATE TRACKS

            // create default marker track
            scenarioTimeline.CreateMarkerTrack();
            // create default signal track
            SignalTrack signalTrack = scenarioTimeline.CreateTrack<SignalTrack>("Signal Track Gen");
            // create default label track
            RangeLabelTrack labelTrack = scenarioTimeline.CreateTrack<RangeLabelTrack>("Label Track Gen");
            // create default control track
            ControlTrack controlTrack = scenarioTimeline.CreateTrack<ControlTrack>("Recorded Flight Gen");

            // ------------------ CREATE TRACK CLIPS AND/OR MARKERS

            // add default scenario step destination marker at time = 0
            Marker startMarker = scenarioTimeline.markerTrack.CreateMarker(typeof(StepMarker), 0) as Marker;
            startMarker.name = "Step 1 Gen";

            // create control clip to control Animation Player
            TimelineClip controlClip = controlTrack.CreateClip<ControlPlayableAsset>();
            controlClip.displayName = "AnimationPlayer Gen";
            controlClip.duration = animRecData.timeline.duration;

            // set exposed reference to Animation Player gameobject
            ControlPlayableAsset controlAsset = controlClip.asset as ControlPlayableAsset;
            controlAsset.sourceGameObject.exposedName = new PropertyName("AnimationPlayer");
            if (animationPlayer != null && scenarioPlayer != null)
                scenarioPlayer.playableDirector.SetReferenceValue(controlAsset.sourceGameObject.exposedName, animationPlayer.gameObject);

            // add default pause signal at the end
            SignalEmitter pauseSignal = signalTrack.CreateMarker(typeof(SignalEmitter), animRecData.timeline.duration) as SignalEmitter;
            SignalAsset pauseSignalAsset = AssetDatabase.LoadAssetAtPath<SignalAsset>("Assets/Content Files/TimelineSignals/PauseSignal.signal");
            if (pauseSignalAsset != null)
            {
                pauseSignal.name = "End Gen";
                pauseSignal.asset = pauseSignalAsset;
            }
            else
                Debug.LogError("Pause Signal Asset is not found!");

            // add turn on engine signal at start if aircraft start mode is cold
            if (animRecData.aircraftMode == AircraftMode.Cold)
            {
                SignalEmitter turnOnEnginesSignal = signalTrack.CreateMarker(typeof(SignalEmitter), 0f) as SignalEmitter;
                SignalAsset turnOnEnginesSignalAsset = AssetDatabase.LoadAssetAtPath<SignalAsset>("Assets/Content Files/TimelineSignals/TurnOnEngines.signal");
                if (turnOnEnginesSignalAsset != null)
                {
                    turnOnEnginesSignal.name = "Turn On Engines Gen";
                    turnOnEnginesSignal.asset = turnOnEnginesSignalAsset;
                }
                else
                    Debug.LogError("Turn On Engines Signal Asset is not found!");
            }

            // add default label clip
            TimelineClip labelClip = labelTrack.CreateClip<RangeLabelClip>();
            labelClip.displayName = "Step 1";
            labelClip.start = 0;
            labelClip.duration = animRecData.timeline.duration;            

            // save unsaved assets 
            AssetDatabase.SaveAssets();

        }

        /*TimelineAsset CreateNewScenario(FlightSimRecordingData animRecAsset, FlightSimRecordingPlayer animationPlayer, FlightScenarioPlayer scenarioPlayer)
        {
            TimelineAsset scenarioTimeline = ScriptableObject.CreateInstance<TimelineAsset>();
            //scenarioTimeline.name = name;

            // get new asset path
            string newAssetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/Content Files/Scenarios/Scenario-" + animRecAsset.clipFolder + ".playable");
            // save new timeline asset
            AssetDatabase.CreateAsset(scenarioTimeline, newAssetPath);

            // ------------------ CREATE TRACKS

            // create default marker track
            scenarioTimeline.CreateMarkerTrack();
            // create default signal track
            SignalTrack signalTrack = scenarioTimeline.CreateTrack<SignalTrack>("Signal Track Gen");
            // create default label track
            RangeLabelTrack labelTrack = scenarioTimeline.CreateTrack<RangeLabelTrack>("Label Track Gen");
            // create default control track
            ControlTrack controlTrack = scenarioTimeline.CreateTrack<ControlTrack>("Recorded Flight Gen");

            // ------------------ CREATE TRACK CLIPS AND/OR MARKERS

            // add default scenario step destination marker at time = 0
            Marker startMarker = scenarioTimeline.markerTrack.CreateMarker(typeof(StepMarker), 0) as Marker;
            startMarker.name = "Step 1 Gen";

            // create control clip to control Animation Player
            TimelineClip controlClip = controlTrack.CreateClip<ControlPlayableAsset>();
            controlClip.displayName = "AnimationPlayer Gen";
            controlClip.duration = animRecAsset.timeline.duration;

            // set exposed reference to Animation Player gameobject
            ControlPlayableAsset controlAsset = controlClip.asset as ControlPlayableAsset;
            controlAsset.sourceGameObject.exposedName = new PropertyName("AnimationPlayer");
            scenarioPlayer.playableDirector.SetReferenceValue(controlAsset.sourceGameObject.exposedName, animationPlayer.gameObject);

            // add default pause signal at the end
            SignalEmitter pauseSignal = signalTrack.CreateMarker(typeof(SignalEmitter), animRecAsset.timeline.duration) as SignalEmitter;
            SignalAsset pauseSignalAsset = AssetDatabase.LoadAssetAtPath<SignalAsset>("Assets/Content Files/TimelineSignals/PauseSignal.signal");
            if (pauseSignalAsset != null)
            {
                pauseSignal.name = "End Gen";
                pauseSignal.asset = pauseSignalAsset;
            }
            else
                Debug.LogError("Pause Signal Asset is not found!");

            // add turn on engine signal at start if aircraft start mode is cold
            if (animRecAsset.aircraftMode == AircraftMode.Cold)
            {
                SignalEmitter turnOnEnginesSignal = signalTrack.CreateMarker(typeof(SignalEmitter), 0f) as SignalEmitter;
                SignalAsset turnOnEnginesSignalAsset = AssetDatabase.LoadAssetAtPath<SignalAsset>("Assets/Content Files/TimelineSignals/TurnOnEngines.signal");
                if (turnOnEnginesSignalAsset != null)
                {
                    turnOnEnginesSignal.name = "Turn On Engines Gen";
                    turnOnEnginesSignal.asset = turnOnEnginesSignalAsset;
                }
                else
                    Debug.LogError("Turn On Engines Signal Asset is not found!");
            }

            // add default label clip
            TimelineClip labelClip = labelTrack.CreateClip<RangeLabelClip>();
            labelClip.displayName = "Step 1";
            labelClip.start = 0;
            labelClip.duration = animRecAsset.timeline.duration;

            AssetDatabase.SaveAssets();

            return scenarioTimeline;
        }*/

        private void LateUpdate()
        {
            if (_canRecord)
            {
                frameCounter++;
                timeCounter += Time.deltaTime;
                if (frameCounter == _skipFrame)
                {
                    var idx = 0;
                    foreach (FlightSimRecordingItem anim in _animationItems)
                    {
                        //if (anim.animationClip == null) continue;
                        Debug.Log("Take snapshot of: " + anim.animatedObject.gameObject.name);
                        _recorder[idx].TakeSnapshot(timeCounter);
                        idx++;
                    }
                    frameCounter = 0;
                    timeCounter = 0f;
                }
            }

        }

    }
}
#endif
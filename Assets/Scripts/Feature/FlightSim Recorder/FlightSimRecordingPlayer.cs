using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace VirtualTraining.Feature
{
    [RequireComponent(typeof(PlayableDirector))]
    public class FlightSimRecordingPlayer : MonoBehaviour
    {
        public PlayableDirector playableDirector;

        // Start is called before the first frame update
        void Start()
        {
            playableDirector = GetComponent<PlayableDirector>();
        }

    }
}
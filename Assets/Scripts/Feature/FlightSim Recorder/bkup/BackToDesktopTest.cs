using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

public class BackToDesktopTest : MonoBehaviour
{
    public void Back()
    {
        EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => VirtualTrainingSceneManager.LoadSceneProgress, LoadScenarioLabel, () => VirtualTrainingSceneManager.SwitchSceneMode(SceneMode.Desktop), null, false));
    }

    private string LoadScenarioLabel()
    {
        return VirtualTrainingSceneManager.LoadSceneInfo;
    }
}

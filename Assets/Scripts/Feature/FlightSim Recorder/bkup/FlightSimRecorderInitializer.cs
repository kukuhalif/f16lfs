#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Feature
{
    [InitializeOnLoad]
    public static class FlightSimRecorderInitializer
    {
        static FlightSimRecorderInitializer()
        {
            EditorApplication.playModeStateChanged += PlayModeStateChangeHandler;
        }

        private static void PlayModeStateChangeHandler(PlayModeStateChange state)
        {
            // Debug.Log(state);

            // get FlightSimRecorderEditor instance
            /*FlightSimRecorderEditor[] flightSimRecorderEditor = Resources.FindObjectsOfTypeAll(typeof(FlightSimRecorderEditor)) as FlightSimRecorderEditor[];
            if (flightSimRecorderEditor != null)
            {
                switch (state)
                {
                    case PlayModeStateChange.EnteredPlayMode:
                    case PlayModeStateChange.EnteredEditMode:
                        flightSimRecorderEditor[0].AfterChangeMode();
                        break;
                    case PlayModeStateChange.ExitingPlayMode:
                    case PlayModeStateChange.ExitingEditMode:
                        //flightSimRecorderEditor[0].BeforeChangeMode();
                        break;
                }
            }*/
        }

    }
}
#endif
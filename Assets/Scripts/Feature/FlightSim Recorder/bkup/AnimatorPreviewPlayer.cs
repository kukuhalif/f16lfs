#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

namespace VirtualTraining.Feature
{
    //[ExecuteInEditMode]
    [RequireComponent(typeof(Animator))]
    public class AnimatorPreviewPlayer : MonoBehaviour
    {

        [ReadOnly] public Animator animator;
        AnimatorRecordingHelper animatorRecordingHelper;
        //FrameRecordingHelper frameRecordingHelper;

        [ReadOnly] public bool isPlaying = false;
        public bool reset;

        [ReadOnly] public float currentPlayTime;
        double _editorTime;
        double _editorTimeOld;
        [ReadOnly] public float _deltaTime;

        void AppUpdate()
        {
            if (animator != null)
            {
                //animator.Update(Time.deltaTime);
                //SceneView.RepaintAll();
                //Debug.Log("Editor.Update...");
                Update();
            }
        }

        public void PlayAnim()
        {
            animator = GetComponent<Animator>();
            if (!animator.isInitialized)
            {
                animator.Rebind();
            }
            animator.speed = 0f;
            animator.playableGraph.Play();
            //animator.StartPlayback();

            // Get animator layers
            AnimatorControllerLayer[] animLayers = (animator.runtimeAnimatorController as AnimatorController).layers;
            int idx = 0;
            foreach (AnimatorControllerLayer animLayer in animLayers)
            {
                AnimatorState defState = animLayers[idx].stateMachine.defaultState;
                animator.Play(defState.name, idx, 0.2f);
                animator.Update(Time.deltaTime);
                idx++;
            }
        }

        public void Play()
        {
            EditorApplication.update += AppUpdate;
            isPlaying = true;
            //PlayAnim();
            animator = GetComponent<Animator>();
            //animatorRecordingHelper = GetComponent<AnimatorRecordingHelper>();
            //animatorRecordingHelper.Init();
            //frameRecordingHelper = GetComponent<FrameRecordingHelper>();
            //frameRecordingHelper.Init();
            //animator.StartPlayback();
        }

        public void Stop()
        {
            EditorApplication.update -= AppUpdate;
            isPlaying = false;
            Reset();
            //animatorRecordingHelper.Final();
            //frameRecordingHelper.Final();
            //animator.StopPlayback();
        }

        private void Update()
        {
            if (isPlaying)
            {
                //Debug.Log("Masuk Update PreviewPlayer");
                UpdateTime();
                //animatorRecordingHelper.SetUpdate(currentPlayTime, _deltaTime);
                //frameRecordingHelper.SetUpdate(currentPlayTime, _deltaTime);

                if (reset)
                    Reset();

                animator.speed = 0f;

                // Get animator layers
                /*AnimatorControllerLayer[] animLayers = (animator.runtimeAnimatorController as AnimatorController).layers;
                int idx = 0;
                foreach (AnimatorControllerLayer animLayer in animLayers)
                {
                    AnimatorState defState = animLayers[idx].stateMachine.defaultState;
                    animator.Play(defState.name, idx, (float)currentPlayTime);
                    animator.playableGraph.Evaluate(_deltaTime);
                    animator.Update(_deltaTime);
                    idx++;
                }*/

            }
        }

        private void Reset()
        {
            currentPlayTime = 0;
            reset = false;
        }

        private void UpdateTime()
        {
            _editorTime = EditorApplication.timeSinceStartup;
            if (_editorTimeOld > 0)
            {
                _deltaTime = (float)(_editorTime - _editorTimeOld);
            }
            _editorTimeOld = _editorTime;
            currentPlayTime += _deltaTime / 10; // * 10
        }

    }
}
#endif

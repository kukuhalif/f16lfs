﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System.Linq;

namespace VirtualTraining.Feature
{

    public class VirtualButtonManager : MonoBehaviour
    {
        GameObject currentGameObject;
        public List<ScriptedAnimationTreeElement> saTreeElement = new List<ScriptedAnimationTreeElement>();
        public List<GameObject> virtualButtonPlayers = new List<GameObject>();
        public List<VirtualButtonDataModel> virtualButtonDatas;
        // Start is called before the first frame update
        void Start()
        {
            EventManager.AddListener<VirtualButtonEvent>(StartVBListener);
            EventManager.AddListener<VirtualButtonEventInstantiate>(VirtualButtonInstantiated);
            EventManager.AddListener<VirtualButtonEventDestroy>(DestroyVBListener);
            var scriptedAnimationDatabase = DatabaseManager.GetScriptedAnimationTree();
            PrintChilds(scriptedAnimationDatabase);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<VirtualButtonEvent>(StartVBListener);
            EventManager.RemoveListener<VirtualButtonEventDestroy>(DestroyVBListener);
            EventManager.RemoveListener<VirtualButtonEventInstantiate>(VirtualButtonInstantiated);
        }

        void StartVBListener(VirtualButtonEvent e)
        {
            for (int x = 0; x < e.virtualButtonDatas.Count; x++)
            {
                VirtualButtonDataModel vbData = new VirtualButtonDataModel(e.virtualButtonDatas[x]);

                // todo : quick fix by bram, variabel direction belum diupdate/dihapus
                if (vbData.axisCount == DirectionAxisCount.TwoAxis)
                    vbData.direction = DragDirection.Both;

                if (e.overrideData != null)
                {
                    for (int i = 0; i < e.overrideData.Count; i++)
                    {
                        if (e.overrideData[i].OverrideVirtualButton == OverrideVirtualButton.DefaultValue)
                        {
                            vbData.defaultValue = e.overrideData[i].defaultValue;
                        }
                    }
                }
                virtualButtonDatas.Add(vbData);
            }

            virtualButtonDatas = virtualButtonDatas.Distinct().ToList();
        }

        void VirtualButtonInstantiated(VirtualButtonEventInstantiate e)
        {

            for (int i = 0; i < virtualButtonDatas.Count; i++)
            {
                currentGameObject = new GameObject();
                currentGameObject.transform.SetParent(this.transform);
                switch (virtualButtonDatas[i].vbType)
                {
                    case VirtualButtonType.Drag:
                        VirtualButtonDrag vbPlayer = currentGameObject.AddComponent<VirtualButtonDrag>();
                        vbPlayer.virtualButtonDatas = virtualButtonDatas[i];
                        currentGameObject.name = "VBPlayer" + virtualButtonDatas[i].name;
                        virtualButtonPlayers.Add(currentGameObject);
                        break;
                    case VirtualButtonType.Click:
                        VirtualButtonClick vbPlayeClick = currentGameObject.AddComponent<VirtualButtonClick>();
                        vbPlayeClick.virtualButtonDatas = virtualButtonDatas[i];
                        currentGameObject.name = "VBPlayer" + virtualButtonDatas[i].name;
                        virtualButtonPlayers.Add(currentGameObject);
                        break;
                    case VirtualButtonType.Press:
                        VirtualButtonPress vbPlayeHold = currentGameObject.AddComponent<VirtualButtonPress>();
                        vbPlayeHold.virtualButtonDatas = virtualButtonDatas[i];
                        currentGameObject.name = "VBPlayer" + virtualButtonDatas[i].name;
                        virtualButtonPlayers.Add(currentGameObject);
                        break;
                }
            }
        }

        void DestroyVBListener(VirtualButtonEventDestroy e)
        {
            for (int i = 0; i < virtualButtonPlayers.Count; i++)
            {
                VirtualButtonBase vbPlayer = virtualButtonPlayers[i].GetComponent<VirtualButtonBase>();
                vbPlayer.DestroyThis();
            }
            virtualButtonPlayers.Clear();
            virtualButtonDatas.Clear();
        }

        void PrintChilds(ScriptedAnimationTreeElement element)
        {
            if (!element.hasChildren)
                return;

            for (int i = 0; i < element.children.Count; i++)
            {
                ScriptedAnimationTreeElement materiChild = (ScriptedAnimationTreeElement)element.children[i];
                saTreeElement.Add(materiChild);
                PrintChilds(materiChild);
            }
        }

        public VirtualButtonBase InitVBAnimation(VirtualButtonDataModel vbDataModel)
        {
            currentGameObject = new GameObject();
            currentGameObject.transform.SetParent(this.transform);
            switch (vbDataModel.vbType)
            {
                case VirtualButtonType.Drag:
                    VirtualButtonDrag vbPlayerDrag = currentGameObject.AddComponent<VirtualButtonDrag>();
                    vbPlayerDrag.virtualButtonDatas = vbDataModel;
                    vbPlayerDrag.isVBAnimation = true;
                    currentGameObject.name = "VBPlayer" + vbDataModel.name;
                    virtualButtonPlayers.Add(currentGameObject);
                    break;
                case VirtualButtonType.Click:
                    VirtualButtonClick vbPlayeClick = currentGameObject.AddComponent<VirtualButtonClick>();
                    vbPlayeClick.virtualButtonDatas = vbDataModel;
                    vbPlayeClick.isVBAnimation = true;
                    currentGameObject.name = "VBPlayer" + vbDataModel.name;
                    virtualButtonPlayers.Add(currentGameObject);
                    break;
                case VirtualButtonType.Press:
                    VirtualButtonPress vbPlayePress = currentGameObject.AddComponent<VirtualButtonPress>();
                    vbPlayePress.virtualButtonDatas = vbDataModel;
                    vbPlayePress.isVBAnimation = true;
                    currentGameObject.name = "VBPlayer" + vbDataModel.name;
                    virtualButtonPlayers.Add(currentGameObject);
                    break;
            }
            return currentGameObject.GetComponent<VirtualButtonBase>();
        }
    }
}

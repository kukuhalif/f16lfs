﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System.Linq;

namespace VirtualTraining.Feature
{
    public class EffectLerp : MonoBehaviour
    {
        VirtualButtonBase virtualButtonBase;
        VirtualButtonEffectData vbEffectData;
        public Animator animator;
        ///public float sliderValue;
        float speedTemp;
        float currentValue;
        float startValue;
        List<EffectLerp> effectLerps = new List<EffectLerp>();

        public void SetupEffectLerp(VirtualButtonEffectData vbEffectData, VirtualButtonBase virtualButtonBase)
        {
            this.virtualButtonBase = virtualButtonBase;
            this.vbEffectData = vbEffectData;
        }

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            if (animator != null)
                animator.enabled = true;
            if (!string.IsNullOrEmpty(vbEffectData.parameter))
                currentValue = animator.GetFloat(vbEffectData.parameter);
            startValue = currentValue;

            //buat ngecheck effect lerp ada lebih dari 1
            effectLerps = GetComponents<EffectLerp>().ToList();
            effectLerps.Remove(this);
        }

        void Calculate()
        {
            if (!string.IsNullOrEmpty(vbEffectData.parameter) && virtualButtonBase != null)
            {
                speedTemp = Mathf.Lerp(vbEffectData.minValue, vbEffectData.maxValue, virtualButtonBase.sliderValue);
                animator.SetFloat(vbEffectData.parameter, Mathf.Lerp(currentValue, speedTemp, Time.deltaTime * vbEffectData.speedAcc));
                currentValue = animator.GetFloat(vbEffectData.parameter);
            }
        }

        // Update is called once per frame
        void Update()
        {
            //buat ngecheck effect lerp ada lebih dari 1
            if (effectLerps.Count > 0)
            {
                for (int i = 0; i < effectLerps.Count; i++)
                {
                    if (effectLerps[i].animator == animator)
                    {
                        if (vbEffectData.parameter == effectLerps[i].vbEffectData.parameter)
                        {
                            if (effectLerps[i].virtualButtonBase.sliderValue <= virtualButtonBase.sliderValue)
                            {
                                Calculate();
                            }
                        }
                    }
                }
            }
            else
                Calculate();
        }

        public void DestroyLerp()
        {
            if (animator != null)
            {
                animator.SetFloat(vbEffectData.parameter, startValue);
                //animator.Play(vbEffectData.animationState, -1, 0);
                //animator.Play("Default", -1, 0);
                animator.Rebind();
                animator.Update(0);
                animator.enabled = false;
                Destroy(this);
            }

            //StartCoroutine(DelayDestroy());
        }

        IEnumerator DelayDestroy()
        {
            yield return new WaitForSeconds(.2f);
            if (animator != null)
                animator.enabled = false;

            DestroyImmediate(this);
        }
    }
}

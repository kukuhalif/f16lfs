using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class InteractionCursorEffect : MonoBehaviour
    {
        VirtualButtonType vbType;
        DragDirection dragDirection;
        DirectionAxisCount dragAxisCount;
        bool isBlocked;
        bool cursorInsideMonitorPanel;
        bool isDragged;

        private void Start()
        {
            VirtualTrainingInputSystem.RaycastedUIUpdated += RaycastedUIChangedListener;
            EventManager.AddListener<CursorEnterMonitorPanelEvent>(CursorEnterMonitorPanelListener);
            EventManager.AddListener<RaycastObjectFromMonitorCameraEvent>(MonitorCameraRaycastListener);
            EventManager.AddListener<VirtualButtonDragedEvent>(VBDragInteractionListener);
        }
        private void OnDestroy()
        {
            VirtualTrainingInputSystem.RaycastedUIUpdated -= RaycastedUIChangedListener;
            EventManager.AddListener<CursorEnterMonitorPanelEvent>(CursorEnterMonitorPanelListener);
            EventManager.RemoveListener<RaycastObjectFromMonitorCameraEvent>(MonitorCameraRaycastListener);
            EventManager.RemoveListener<VirtualButtonDragedEvent>(VBDragInteractionListener);
        }

        public void SetupInteractionCursor(VirtualButtonDataModel vbData)
        {
            vbType = vbData.vbType;
            dragDirection = vbData.direction;
            dragAxisCount = vbData.axisCount;
        }

        private void CursorEnterMonitorPanelListener(CursorEnterMonitorPanelEvent e)
        {
            cursorInsideMonitorPanel = e.enter;

            if (e.enter)
                VirtualTrainingCursor.DefaultCursor();
        }

        private void MonitorCameraRaycastListener(RaycastObjectFromMonitorCameraEvent e)
        {
            if (e.target == gameObject)
                SetCursor();
        }

        private void RaycastedUIChangedListener(GameObject raycastedUI)
        {
            if (raycastedUI == null)
                isBlocked = false;
            else
                isBlocked = true;
        }

        private void SetCursor()
        {
            if (vbType == VirtualButtonType.Drag)
            {
                //if (dragDirection == DragDirection.Horizontal)
                //if (dragAxisCount == DirectionAxisCount.OneAxis)
                //{
                //    VirtualTrainingCursor.VBHorizontalInteraction();
                //}
                //else if (dragDirection == DragDirection.Vertical)
                //{
                //    VirtualTrainingCursor.VBVerticalInteraction();
                //}
                //else
                //{
                    VirtualTrainingCursor.VBBothInteraction();
                //}
            }
            else if (vbType == VirtualButtonType.Click)
            {
                VirtualTrainingCursor.VBClickInteraction();
            }
            else if (vbType == VirtualButtonType.Press)
            {
                VirtualTrainingCursor.VBPressInteraction();
            }
        }

        private void OnMouseEnter()
        {
            if (cursorInsideMonitorPanel)
                return;

            VirtualTrainingInputSystem.UpdateRaycastedUI();
            if (isBlocked)
            {
                VirtualTrainingCursor.DefaultCursor();
                return;
            }

            SetCursor();
        }

        private void OnMouseExit()
        {
            if (isDragged == true)
                return;
            VirtualTrainingCursor.DefaultCursor();
        }

        void VBDragInteractionListener(VirtualButtonDragedEvent e)
        {
            if (e.isDragged)
            {
                SetCursor();
                isDragged = true;
            }
            else
            {
                VirtualTrainingCursor.DefaultCursor();
                isDragged = false;
            }
                
        }

        public void DestroyInteraction()
        {
            Destroy(this);
        }
    }
}


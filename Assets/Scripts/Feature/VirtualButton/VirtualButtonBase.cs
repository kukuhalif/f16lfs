using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public abstract class VirtualButtonBase : MonoBehaviour
    {
        bool isInvert;
        public VirtualButtonDataModel virtualButtonDatas;
        protected bool leftClickDown;
        public GameObject clickTargetCurrent;
        public float sliderValue;
        [SerializeField] List<GameObjectType> fresnelObjects = new List<GameObjectType>();
        public bool IsInvert { get => isInvert; }
        protected ClickTargetData currentClickTarget;
        protected bool isVR;
        protected Transform HandVR;

        public List<VBStartAnimationData> vbStartAnimationDatas = new List<VBStartAnimationData>();
        public List<ClickTargetData> clickTargetDatas = new List<ClickTargetData>();
        public List<AnimationPlayerBase> animationPlayerContainerInteraction = new List<AnimationPlayerBase>();
        public List<AnimationPlayerBase> animationPlayerContainerFeedback = new List<AnimationPlayerBase>();
        public List<AnimationPlayerBase> animationOnStarts = new List<AnimationPlayerBase>();
        public List<EffectLerp> effectLerpContainer = new List<EffectLerp>();
        public List<ModifierBase> modifierSetActiveContainer = new List<ModifierBase>();

        protected float distanceToObject;
        protected GameObject objectVB;
        protected bool isMonitorCamera;
        public bool isVBAnimation;
        public float vbOnAnimationValue;

        // Start is called before the first frame update
        void Start()
        {
            fresnelObjects.Clear();
            VirtualTrainingInputSystem.OnStartLeftClick += OnStartLeftClick;
            VirtualTrainingInputSystem.OnEndLeftClick += OnEndLeftClick;
            EventManager.AddListener<MonitorCameraInteractionEvent>(InteractionEventListener);
            EventManager.AddListener<RightHandTriggerEvent>(RightHandTriggered);
            EventManager.AddListener<RightHandGripEvent>(RightHandGripped);

            if (isVBAnimation == false)
            {
                for (int i = 0; i < virtualButtonDatas.clickTargetDatas.Count; i++)
                {
                    fresnelObjects.Add(virtualButtonDatas.clickTargetDatas[i].gameObject);
                }

                EventManager.TriggerEvent(new FresnelObjectsEvent(fresnelObjects, false, true));
            }

            SetupVirtualButton();
        }

        private void OnDestroy()
        {
            VirtualTrainingInputSystem.OnStartLeftClick -= OnStartLeftClick;
            VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
            EventManager.RemoveListener<MonitorCameraInteractionEvent>(InteractionEventListener);
            EventManager.RemoveListener<RightHandTriggerEvent>(RightHandTriggered);
            EventManager.RemoveListener<RightHandGripEvent>(RightHandGripped);
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (animationPlayerContainerInteraction[i] != null)
                    animationPlayerContainerInteraction[i].DestroyItself();
            }
            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (animationPlayerContainerFeedback[i] != null)
                    animationPlayerContainerFeedback[i].DestroyItself();
            }
            for (int k = 0; k < animationOnStarts.Count; k++)
            {
                if (animationOnStarts[k] != null)
                    animationOnStarts[k].DestroyItself();
            }
            for (int j = 0; j < effectLerpContainer.Count; j++)
            {
                if (effectLerpContainer[j] != null)
                    effectLerpContainer[j].DestroyLerp();
            }

            for (int e = 0; e < modifierSetActiveContainer.Count; e++)
            {
                if (modifierSetActiveContainer[e] != null)
                    modifierSetActiveContainer[e].DestroyModifier();
            }
        }

        public abstract void TriggerVirtualButton(float vbOnAnimValue = 0);

        public abstract void PlayVirtualButton();
        public abstract void StopVirtualButton();

        protected virtual void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null)
                return;
            isVR = false;
            ClickObjectCheck();
        }

        void RightHandTriggered(RightHandTriggerEvent e)
        {
            if (e.isClickDown == false)
            {
                ClickEnded();
                if (objectVB != null)
                    Destroy(objectVB);
                return;
            }

            isVR = true;
            ClickObjectCheck();
        }

        void RightHandGripped(RightHandGripEvent e)
        {
            if (e.interactionObject == null)
            {
                HandVR = null;
                ClickEnded();
                return;
            }

            isVR = true;
            HandVR = e.HandVR;
            CheckForGrip(e.interactionObject);
        }

        void CheckForGrip(GameObject currentInteractionObject)
        {
            for (int i = 0; i < clickTargetDatas.Count; i++)
            {
                if (clickTargetDatas[i].gameObject.gameObject == currentInteractionObject)
                {
                    VirtualTrainingCamera.IsMovementEnabled = false;
                    clickTargetCurrent = currentInteractionObject;
                    isInvert = clickTargetDatas[i].isInvert;
                    currentClickTarget = clickTargetDatas[i];
                    TriggerVirtualButton();
                    break;
                }
            }
        }

        void ClickObjectCheck()
        {
            Debug.Log("click object check");

            RaycastHit hitInfo = new RaycastHit();
            if (VirtualTrainingCamera.Raycast(ref hitInfo))
            {
                for (int i = 0; i < clickTargetDatas.Count; i++)
                {
                    if (clickTargetDatas[i].gameObject.gameObject == hitInfo.collider.gameObject)
                    {
                        if (isVR)
                            distanceToObject = Vector3.Distance(VirtualTrainingCamera.handVR.transform.position, hitInfo.collider.transform.position);

                        VirtualTrainingCamera.IsMovementEnabled = false;
                        clickTargetCurrent = hitInfo.collider.gameObject;
                        isInvert = clickTargetDatas[i].isInvert;
                        currentClickTarget = clickTargetDatas[i];
                        TriggerVirtualButton();

                        if (virtualButtonDatas.vbType == VirtualButtonType.Drag)
                            EventManager.TriggerEvent(new VirtualButtonDragedEvent(true));
                        break;
                    }
                }
            }
        }

        protected virtual void OnEndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            ClickEnded();
        }

        void ClickEnded()
        {
            leftClickDown = false;
            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                animationPlayerContainerFeedback[i].setCurrentParam = false;
            }
            for (int j = 0; j < animationPlayerContainerInteraction.Count; j++)
            {
                animationPlayerContainerInteraction[j].setCurrentParam = false;
            }

            isMonitorCamera = false;
            VirtualTrainingCamera.IsMovementEnabled = true;
            if (virtualButtonDatas.vbType == VirtualButtonType.Drag)
                EventManager.TriggerEvent(new VirtualButtonDragedEvent(false));

            Debug.Log("vb click ended");
        }

        protected virtual void InteractionEventListener(MonitorCameraInteractionEvent e)
        {
            if (e.interactionObject == null)
                return;
            else
            {
                for (int i = 0; i < clickTargetDatas.Count; i++)
                {
                    if (clickTargetDatas[i].gameObject.gameObject == e.interactionObject)
                    {
                        Debug.Log("monitor camera target = " + e.interactionObject.name);

                        VirtualTrainingCamera.IsMovementEnabled = false;
                        clickTargetCurrent = e.interactionObject.gameObject;
                        currentClickTarget = clickTargetDatas[i];
                        isMonitorCamera = true;
                        TriggerVirtualButton();
                        break;
                    }
                }
            }
        }
        protected virtual void SetupVirtualButton()
        {
            if (virtualButtonDatas == null)
                return;

            if (isVBAnimation == false)
                clickTargetDatas = virtualButtonDatas.clickTargetDatas;

            vbStartAnimationDatas = virtualButtonDatas.vbStartAnimations;
            sliderValue = virtualButtonDatas.defaultValue;

            for (int a = 0; a < virtualButtonDatas.vbOutputInteraction.Count; a++)
            {
                SetAnimationPlayerData(virtualButtonDatas, true, virtualButtonDatas.vbOutputInteraction[a]);
            }

            for (int b = 0; b < virtualButtonDatas.vbOutputFeedback.Count; b++)
            {
                SetAnimationPlayerData(virtualButtonDatas, false, virtualButtonDatas.vbOutputFeedback[b]);
            }

            for (int c = 0; c < virtualButtonDatas.vbStartAnimations.Count; c++)
            {
                SetAnimationOnStart(virtualButtonDatas, virtualButtonDatas.vbStartAnimations[c]);
            }

            for (int d = 0; d < virtualButtonDatas.vbEffectDatas.Count; d++)
            {
                setEffectLerp(virtualButtonDatas.vbEffectDatas[d]);
            }

            for (int e = 0; e < virtualButtonDatas.vbModifierData.Count; e++)
            {
                setModifier(virtualButtonDatas.vbModifierData[e]);
            }

            for (int f = 0; f < clickTargetDatas.Count; f++)
            {
                if (clickTargetDatas[f].gameObject != null && clickTargetDatas[f].gameObject.gameObject != null)
                {
                    InteractionCursorEffect interactionTemp = clickTargetDatas[f].gameObject.gameObject.AddComponent<InteractionCursorEffect>();
                    interactionTemp.SetupInteractionCursor(virtualButtonDatas);
                }
            }
        }

        void SetAnimationOnStart(VirtualButtonDataModel dataModel, VBStartAnimationData vbOnStart)
        {
            Transform transformObject;
            if (vbOnStart.isAnimationScript)
            {
                if (vbOnStart.ObjectAnimation.gameObject != null)
                {
                    if (vbOnStart.scriptedAnimationElement.GetData() != null)
                    {
                        transformObject = vbOnStart.ObjectAnimation.gameObject.transform;
                        AnimationPlayerScript animScript = transformObject.gameObject.AddComponent<AnimationPlayerScript>();
                        animScript.SetupOnStart(dataModel, vbOnStart, false);
                        animationOnStarts.Add(animScript);

                        if (vbOnStart.ObjectAnimation.gameObject.activeInHierarchy)
                            EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(vbOnStart.ObjectAnimation.gameObject, true));
                        else
                            EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(vbOnStart.ObjectAnimation.gameObject, false));

                        animScript.StartPlayAnimation(1, animScript.getVBOutputData.speed);
                    }
                    else
                        Debug.Log("Data Animation Script Kosong");

                }
                else
                    Debug.Log("Data Feedback Object Animasi Manual Kosong");
            }
            else
            {
                if (vbOnStart.animatorController.gameObject != null)
                {
                    transformObject = vbOnStart.animatorController.gameObject.transform;

                    AnimationPlayerManual animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                    //animManual.SetupOnStart(dataModel, vbOnStart, false,true);
                    animManual.Setup(dataModel, false, null, vbOnStart, true);
                    animationOnStarts.Add(animManual);
                }
                else
                    Debug.Log("Data Feedback Object Animasi Manual Kosong");
            }

        }

        void SetAnimationPlayerData(VirtualButtonDataModel dataModel, bool isInteractionObject, VirtualButtonOutputData vbOutputData)
        {
            Transform transformObject;
            if (vbOutputData.isAnimationScript)
            {
                if (vbOutputData.ObjectAnimation.gameObject != null)
                {
                    if (vbOutputData.scriptedAnimationElement.GetData() != null)
                    {
                        transformObject = vbOutputData.ObjectAnimation.gameObject.transform;
                        AnimationPlayerScript animScript = transformObject.gameObject.AddComponent<AnimationPlayerScript>();
                        animScript.SetupForVirtualButton(dataModel, vbOutputData, isInteractionObject);

                        if (isInteractionObject == true)
                            animationPlayerContainerInteraction.Add(animScript);
                        else
                            animationPlayerContainerFeedback.Add(animScript);

                        if (vbOutputData.ObjectAnimation.gameObject.activeInHierarchy)
                            EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(vbOutputData.ObjectAnimation.gameObject, true));
                        else
                            EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(vbOutputData.ObjectAnimation.gameObject, false));
                    }
                    else
                        Debug.Log("Data Animation Script Kosong");

                }
                else
                {
                    if (isInteractionObject)
                        Debug.Log("Data Interaction Object Animasi Manual Kosong");
                    else
                        Debug.Log("Data Feedback Object Animasi Manual Kosong");
                }
            }
            else
            {
                if (vbOutputData.animatorController.gameObject != null)
                {
                    transformObject = vbOutputData.animatorController.gameObject.transform;

                    if ((dataModel.vbType == VirtualButtonType.Click && dataModel.isSequentialClick)
                        || (dataModel.vbType == VirtualButtonType.Drag && dataModel.isSequentialDrag))
                    {
                        if (isInteractionObject)
                        {
                            if (dataModel.sequencePlayMethod == SequencePlayMethod.PlayState || dataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                            {
                                for (int i = 0; i < vbOutputData.animationDatas.Count; i++)
                                {
                                    AnimationPlayerManual animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                                    animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                                    animManual.SetAnimName(vbOutputData.animationDatas[i].animationState, vbOutputData.animationDatas[i].animationSpeedParameter,
                                        vbOutputData.animationDatas[i].animationLayer, false);
                                    animManual.SetupParameterDatas(vbOutputData.minimalValue, vbOutputData.maximalValue, dataModel.defaultValue, null, null);

                                    SetToListManual(animManual, isInteractionObject);
                                }
                            }
                            else if (dataModel.sequencePlayMethod == SequencePlayMethod.TimeStampParameter || dataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter)
                            {
                                for (int i = 0; i < vbOutputData.parameterDatas.Count; i++)
                                {
                                    AnimationPlayerManual animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                                    animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                                    animManual.SetupParameterDatas(vbOutputData.parameterDatas[i].minimalValue, vbOutputData.parameterDatas[i].maximalValue, dataModel.defaultValue,
                                        vbOutputData.parameterDatas[i].parameter, null);

                                    SetToListManual(animManual, isInteractionObject);
                                }
                            }
                        }
                        else // buat animation di feedback
                        {
                            if (vbOutputData.feedBackManualType == FeedbackManualType.State)
                            {
                                for (int i = 0; i < vbOutputData.animationDatas.Count; i++)
                                {
                                    AnimationPlayerManual animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                                    animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                                    animManual.SetAnimName(vbOutputData.animationDatas[i].animationState, vbOutputData.animationDatas[i].animationSpeedParameter,
                                        vbOutputData.animationDatas[i].animationLayer, false);
                                    animManual.SetupParameterDatas(vbOutputData.minimalValue, vbOutputData.maximalValue, dataModel.defaultValue, null, null);

                                    SetToListManual(animManual, isInteractionObject);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < vbOutputData.parameterDatas.Count; i++)
                                {
                                    AnimationPlayerManual animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                                    animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                                    animManual.SetupParameterDatas(vbOutputData.parameterDatas[i].minimalValue, vbOutputData.parameterDatas[i].maximalValue, dataModel.defaultValue,
                                        vbOutputData.parameterDatas[i].parameter, null);

                                    SetToListManual(animManual, isInteractionObject);
                                }
                            }
                        }
                    }
                    else //bukan sequential
                    {
                        List<AnimationPlayerManual> animManuallIstTemp = new List<AnimationPlayerManual>();

                        for (int i = 0; i < vbOutputData.animationDatas.Count; i++)
                        {
                            AnimationPlayerManual animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                            animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                            animManual.SetAnimName(vbOutputData.animationDatas[i].animationState, vbOutputData.animationDatas[i].animationSpeedParameter,
                                        vbOutputData.animationDatas[i].animationLayer, false);
                            animManual.SetupParameterDatas(0, 1, dataModel.defaultValue, null, null);

                            animManuallIstTemp.Add(animManual);
                        }
                        for (int i = 0; i < vbOutputData.parameterDatas.Count; i++)
                        {
                            AnimationPlayerManual animManual;
                            if (animManuallIstTemp.Count > i)
                            {
                                if (animManuallIstTemp[i] != null)
                                    animManual = animManuallIstTemp[i];
                                else
                                {
                                    animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                                    animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                                    animManuallIstTemp.Insert(i, animManual);
                                }
                            }
                            else
                            {
                                animManual = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                                animManual.SetupDataModel(dataModel, vbOutputData, isInteractionObject);
                                animManuallIstTemp.Add(animManual);
                            }

                            if (dataModel.vbType == VirtualButtonType.Drag && dataModel.direction == DragDirection.Both)
                            {
                                animManual.SetupParameterDatas(vbOutputData.parameterDatas[i].minimalValue, vbOutputData.parameterDatas[i].maximalValue, dataModel.defaultValue,
                                vbOutputData.parameterDatas[i].parameter, vbOutputData.parameterDatas[i].parameterY);
                            }
                            else
                                animManual.SetupParameterDatas(vbOutputData.parameterDatas[i].minimalValue, vbOutputData.parameterDatas[i].maximalValue, dataModel.defaultValue,
                                vbOutputData.parameterDatas[i].parameter, null);
                        }

                        for (int i = 0; i < animManuallIstTemp.Count; i++)
                        {
                            SetToListManual(animManuallIstTemp[i], isInteractionObject);
                        }
                    }
                }
                else
                {
                    if (isInteractionObject)
                        Debug.Log("Data Interaction Object Animasi Manual Kosong");
                    else
                        Debug.Log("Data Feedback Object Animasi Manual Kosong");
                }
            }
        }

        void SetToListManual(AnimationPlayerManual animManual, bool isInteractionObject)
        {
            if (isInteractionObject == true)
                animationPlayerContainerInteraction.Add(animManual);
            else
                animationPlayerContainerFeedback.Add(animManual);
        }

        void setEffectLerp(VirtualButtonEffectData effectData)
        {
            if (effectData.animator != null)
            {
                EffectLerp effectLerp = effectData.animator.gameObject.AddComponent<EffectLerp>();
                effectLerp.SetupEffectLerp(effectData, this);
                effectLerpContainer.Add(effectLerp);
            }
        }
        void setModifier(VirtualbuttonModifierData modifierData)
        {
            switch (modifierData.vbModifierType)
            {
                case VirtualbuttonModifierType.SetActiveOrNonActive:
                    ModifierSetActive modifierSetActive = this.gameObject.AddComponent<ModifierSetActive>();
                    modifierSetActive.SetModifierData(modifierData);
                    modifierSetActiveContainer.Add(modifierSetActive);
                    Debug.Log("vb modifier - set active");
                    break;
                case VirtualbuttonModifierType.Light:
                    ModifierLight modifierLight = this.gameObject.AddComponent<ModifierLight>();
                    modifierLight.SetModifierData(modifierData);
                    modifierSetActiveContainer.Add(modifierLight);
                    Debug.Log("vb modifier - light");
                    break;
                case VirtualbuttonModifierType.Skybox:
                    ModifierSkybox modifierSkybox = this.gameObject.AddComponent<ModifierSkybox>();
                    modifierSkybox.SetModifierData(modifierData);
                    modifierSetActiveContainer.Add(modifierSkybox);
                    Debug.Log("vb modifier - skybox");
                    break;
            }
        }

        //protected void PlayEffect(float value)
        //{
        //    for (int i = 0; i < effectLerpContainer.Count; i++)
        //    {
        //        //effectLerpContainer[i].sliderValue = value;
        //    }
        //}

        protected bool CheckModifierExcept(float value)
        {
            bool condition = false;

            if (virtualButtonDatas != null)
            {
                if (virtualButtonDatas.vbModifierData.Count <= 0)
                    return condition;

                for (int i = 0; i < virtualButtonDatas.vbModifierData.Count; i++)
                {
                    if (virtualButtonDatas.vbModifierData[i].vbModifierType == VirtualbuttonModifierType.ExceptValues)
                    {
                        for (int j = 0; j < virtualButtonDatas.vbModifierData[i].modifierConditionValues.Count; j++)
                        {
                            if (virtualButtonDatas.vbModifierData[i].modifierConditionValues[j].minValue < value &&
                                virtualButtonDatas.vbModifierData[i].modifierConditionValues[j].maxValue > value)
                            {
                                condition = true;
                            }
                        }

                        if (virtualButtonDatas.vbModifierData[i].defaultCondition == DefaultCondition.On)
                            condition = !condition;

                        if (condition == true && virtualButtonDatas.vbModifierData[i].exceptValueType == ExceptValueType.PlayPause)
                        {
                            for (int k = 0; k < animationPlayerContainerFeedback.Count; k++)
                            {
                                animationPlayerContainerFeedback[k].PauseAnimation();
                            }
                        }
                    }
                }
            }
            return condition;
        }
        protected void PlayVBClickSameTime(float vbOnAnimValue = 0)
        {
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                animationPlayerContainerInteraction[i].StartPlayAnimation(vbOnAnimValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
            }
            if (animationPlayerContainerFeedback.Count > 0)
            {
                for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
                {
                    animationPlayerContainerFeedback[i].StartPlayAnimation(vbOnAnimValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
                }
            }
        }

        protected void PlayVBClickNotSameTime(float vbOnAnimValue = 0)
        {
            if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampParameter ||
                virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation ||
                virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampState)
            {
                PlayVBClickSameTime();
            }
            else
            {
                if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter)
                {
                    StartCoroutine(PlayNotSameAnimationInteraction(vbOnAnimValue, true));
                }
                else
                {
                    StartCoroutine(PlayNotSameAnimationInteraction(vbOnAnimValue, false));
                    StartCoroutine(PlayNotSameAnimationFeedback(vbOnAnimValue, false));
                }

            }
        }

        IEnumerator PlayNotSameAnimationInteraction(float vbOnAnimValue = 0, bool isParameter = false)
        {
            int queueValue = 0;
            float sliderToListQueue = vbOnAnimValue * animationPlayerContainerInteraction.Count;
            float contValue = sliderToListQueue - (sliderToListQueue % 1);
            float test = sliderToListQueue % 1;
            queueValue = (int)contValue;
            sliderToListQueue = vbOnAnimValue * animationPlayerContainerInteraction.Count;

            if (queueValue < animationPlayerContainerInteraction.Count)
            {
                animationPlayerContainerInteraction[queueValue].StartPlayAnimation(sliderToListQueue % 1, animationPlayerContainerInteraction[queueValue].getVBOutputData.speed);
            }

            for (int i = animationPlayerContainerInteraction.Count - 1; i > 0; i--)
            {
                if (i > queueValue)
                {
                    animationPlayerContainerInteraction[i].StartPlayAnimation(0, animationPlayerContainerInteraction[i].getVBOutputData.speed);

                    if (animationPlayerContainerInteraction[i].getVBOutputData.isAnimationScript)
                        yield return new WaitUntil(() => animationPlayerContainerInteraction[i].sliderValue == 0);
                    else
                    {
                        AnimationPlayerManual animManuals = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                        if (isParameter)
                            yield return new WaitUntil(() => animManuals.getSliderPercentageTemp == 0);
                        else
                            yield return new WaitUntil(() => animManuals.sliderValue == 0);
                    }
                }
            }

            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (i < queueValue)
                {
                    animationPlayerContainerInteraction[i].StartPlayAnimation(1, animationPlayerContainerInteraction[i].getVBOutputData.speed);

                    if (animationPlayerContainerInteraction[i].getVBOutputData.isAnimationScript)
                        yield return new WaitUntil(() => animationPlayerContainerInteraction[i].sliderValue == 1);
                    else
                    {
                        AnimationPlayerManual animManuals = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                        if (isParameter)
                            yield return new WaitUntil(() => animManuals.getSliderPercentageTemp == 1);
                        else
                            yield return new WaitUntil(() => animManuals.sliderValue == 1);
                    }
                }
            }

            yield return null;

        }

        IEnumerator PlayNotSameAnimationFeedback(float vbOnAnimValue = 0, bool isParameter = false)
        {
            int queueValue = 0;
            float sliderToListQueue = vbOnAnimValue * animationPlayerContainerInteraction.Count;
            float contValue = sliderToListQueue - (sliderToListQueue % 1);
            float test = sliderToListQueue % 1;
            queueValue = (int)contValue;

            sliderToListQueue = vbOnAnimValue * animationPlayerContainerInteraction.Count;
            if (queueValue < animationPlayerContainerFeedback.Count)
            {
                animationPlayerContainerFeedback[queueValue].StartPlayAnimation(sliderToListQueue % 1, animationPlayerContainerFeedback[queueValue].getVBOutputData.speed);
            }

            for (int i = animationPlayerContainerFeedback.Count - 1; i > 0; i--)
            {
                if (i > queueValue)
                {
                    animationPlayerContainerFeedback[i].StartPlayAnimation(0, animationPlayerContainerFeedback[i].getVBOutputData.speed);

                    if (animationPlayerContainerFeedback[i].getVBOutputData.isAnimationScript)
                        yield return new WaitUntil(() => animationPlayerContainerFeedback[i].sliderValue == 0);
                    else
                    {
                        AnimationPlayerManual animManuals = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
                        if (isParameter)
                            yield return new WaitUntil(() => animManuals.getSliderPercentageTemp == 0);
                        else
                            yield return new WaitUntil(() => animManuals.sliderValue == 0);
                    }
                }
            }

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (i < queueValue)
                {
                    animationPlayerContainerFeedback[i].StartPlayAnimation(1, animationPlayerContainerFeedback[i].getVBOutputData.speed);

                    if (animationPlayerContainerFeedback[i].getVBOutputData.isAnimationScript)
                        yield return new WaitUntil(() => animationPlayerContainerFeedback[i].sliderValue == 1);
                    else
                    {
                        AnimationPlayerManual animManuals = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
                        if (isParameter)
                            yield return new WaitUntil(() => animManuals.getSliderPercentageTemp == 1);
                        else
                            yield return new WaitUntil(() => animManuals.sliderValue == 1);
                    }
                }
            }

        }
        public void DestroyThis()
        {
            for (int i = 0; i < clickTargetDatas.Count; i++)
            {
                if (clickTargetDatas[i].gameObject.gameObject != null)
                {
                    InteractionCursorEffect interaction = clickTargetDatas[i].gameObject.gameObject.GetComponent<InteractionCursorEffect>();
                    interaction.DestroyInteraction();
                }
            }
            DestroyImmediate(this.gameObject);
        }
    }
}

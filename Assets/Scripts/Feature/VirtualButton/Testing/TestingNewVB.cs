using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public class TestingNewVB : MonoBehaviour
{
    [SerializeField] GameObject InteractionObject;
    [SerializeField] GameObject MaxObject;
    [SerializeField] GameObject MinObject;
    [SerializeField] Vector3 positonWorld;
    [SerializeField] Vector3 screenPosition;

    [SerializeField] float minValue;
    [SerializeField] float maxValue;
    [SerializeField] float value;

    [SerializeField] Mesh mesh;
    [SerializeField] MeshFilter meshFilter;

    Vector3 startPosition;

    float lastPosMax;
    float lastPosMin;

    [SerializeField] float distanceHelper;
    float p = 10;

    private void Start()
    {
        distanceHelper = Vector3.Distance(MaxObject.transform.position, MinObject.transform.position);
        startPosition = Camera.main.WorldToScreenPoint(InteractionObject.transform.position);
        mesh = InteractionObject.GetComponent<MeshFilter>().mesh; 
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            ScreenRaycast();
            //WorldToScreen();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            MakePlane();
        }

        if (VirtualTrainingCamera.CurrentCamera != null)
            this.gameObject.transform.LookAt(VirtualTrainingCamera.CurrentCamera.transform);
    }

    void MakePlane()
    {
        mesh = InteractionObject.GetComponent<MeshFilter>().mesh;
        mesh.Clear();

        float size = Vector3.Distance(MinObject.transform.position, MaxObject.transform.position);
        size = size / 2;
        Debug.Log(size);
        mesh.vertices = new Vector3[] {
            new Vector3(-size, -size, 0.01f),
            new Vector3(size, -size, 0.01f),
            new Vector3(size, size, 0.01f),
            new Vector3(-size, size, 0.01f),

        };
        mesh.uv = new Vector2[] {
            new Vector2(0, 0), new Vector2(0, 1),
            new Vector2(5, 3), new Vector2(1, 0)
        };
        mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        mesh.RecalculateNormals();
    }
    void WorldToScreen()
    {
        Vector3 screenMaxObject = Camera.main.WorldToScreenPoint(MaxObject.transform.position);
        Vector3 screenMinObject = Camera.main.WorldToScreenPoint(MinObject.transform.position);

        float distanceToMax = Vector3.Distance(Input.mousePosition, screenMaxObject);
        float distanceToMin = Vector3.Distance(Input.mousePosition, screenMinObject);

        float resultDistance = distanceToMax + distanceToMin;

        if (distanceToMax > lastPosMax && distanceToMin > lastPosMin && lastPosMax != 0)
        {
            return;
        }

        lastPosMax = distanceToMax;
        lastPosMin = distanceToMin;

        value = ((distanceToMin / resultDistance)) * (maxValue - minValue);
    }

    void ScreenRaycast()
    {

        RaycastHit hitInfo = new RaycastHit();
        if (VirtualTrainingCamera.Raycast(ref hitInfo))
        {
            Debug.Log("test " + hitInfo.collider.gameObject.name);
            if (hitInfo.collider.gameObject == this.gameObject)
            {
                Debug.Log("kena " + gameObject.name);
            }
        }
    }
}

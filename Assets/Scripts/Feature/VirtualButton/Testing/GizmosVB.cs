using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosVB : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, .5f);
    } 
}

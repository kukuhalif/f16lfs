using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{

    [System.Serializable]
    public class ParameterSequentialData
    {
        public AnimationPlayerBase animationPlayerBase;
        public ParameterData parameterDatasInteraction;
        public string animationState;

    }

    public partial class VirtualButtonClick : VirtualButtonBase
    {
        int valueTemp;

        public List<ParameterSequentialData> parameterSequentialDatasInteraction = new List<ParameterSequentialData>();
        public List<ParameterSequentialData> parameterSequentialDatasFeedback = new List<ParameterSequentialData>();
        float sliderValueTemp;
        bool invertTemp;
        public float maxAnimationValue = 1;
        public float minAnimationValue = 0;

        void PlayVBClick()
        {
            VirtualTrainingCamera.IsMovementEnabled = false;

            if (virtualButtonDatas.isSequentialClick)
            {
                PlayVBClickSequential();
            }
            else
            {
                virtualButtonDatas.isBack = !virtualButtonDatas.isBack;
                if (animationPlayerContainerInteraction.Count > 0)
                {
                    if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop &&
                        virtualButtonDatas.isBack)
                    {
                        StartCoroutine(DelayTransitionFeedback(animationPlayerContainerInteraction[0].getVBDataModel.isBack));
                        StartCoroutine(DelayInteractionAfterAnimation());
                    }
                    else
                    {
                        for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                        {
                            if (virtualButtonDatas.isBack)
                                animationPlayerContainerInteraction[i].StartPlayAnimation(minAnimationValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                            else
                                animationPlayerContainerInteraction[i].StartPlayAnimation(maxAnimationValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);

                        }
                        StartCoroutine(DelayTransitionFeedback(animationPlayerContainerInteraction[0].getVBDataModel.isBack));
                    }
                }
            }

        }

        protected override void SetupVirtualButton()
        {
            base.SetupVirtualButton();
            valueTemp = 0;
        }

        IEnumerator DelayTransitionFeedback(bool isBack)
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            {
                if (virtualButtonDatas.isReverseLoop == true && isBack)
                {
                    yield return null;
                }
                else if (isBack)
                    yield return new WaitUntil(() => sliderValue <= 0.05f);
                else
                {
                    yield return new WaitUntil(() => sliderValue >= 0.95f);
                }
            }
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.Direct)
                yield return new WaitForSeconds(0);
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);

            if (animationPlayerContainerFeedback.Count > 0)
            {
                for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
                {
                    animationPlayerContainerFeedback[i].isBack = isBack;

                    if (animationPlayerContainerFeedback[i].isBack)
                        animationPlayerContainerFeedback[i].StartPlayAnimation(0, animationPlayerContainerFeedback[i].getVBOutputData.speed);
                    else
                        animationPlayerContainerFeedback[i].StartPlayAnimation(1, animationPlayerContainerFeedback[i].getVBOutputData.speed);
                }
            }
        }

        IEnumerator DelayInteractionAfterAnimation()
        {
            yield return new WaitUntil(() => sliderValue <= 0.05f);
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (animationPlayerContainerInteraction[i].getVBDataModel.isBack)
                    animationPlayerContainerInteraction[i].StartPlayAnimation(minAnimationValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                else
                    animationPlayerContainerInteraction[i].StartPlayAnimation(maxAnimationValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
            }
        }

        public override void TriggerVirtualButton(float vbOnAnimValue = 0)
        {
            Debug.Log("virtual button click");
            if (isVBAnimation)
            {
                VirtualTrainingCamera.IsMovementEnabled = false;
                if (virtualButtonDatas.isSequentialClick)
                {
                    PlayVBClickNotSameTime(vbOnAnimValue);
                }
                else
                {
                    PlayVBClickSameTime(vbOnAnimValue);
                }
            }
            else
            {
                PlayVBClick();
            }
        }

        void AnimationInteractionSetSlider()
        {
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (i == 0)
                    sliderValueTemp = animationPlayerContainerInteraction[i].sliderValue;

                if (virtualButtonDatas.isBack)
                {
                    if (sliderValueTemp <= animationPlayerContainerInteraction[i].sliderValue)
                        sliderValueTemp = animationPlayerContainerInteraction[i].sliderValue;
                }
                else
                {
                    if (sliderValueTemp >= animationPlayerContainerInteraction[i].sliderValue)
                        sliderValueTemp = animationPlayerContainerInteraction[i].sliderValue;
                }

            }
        }

        void AnimationFeedbackSetSlider()
        {
            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (i == 0)
                    sliderValueTemp = animationPlayerContainerFeedback[i].sliderValue;
                if (virtualButtonDatas.isBack)
                {
                    if (sliderValueTemp <= animationPlayerContainerFeedback[i].sliderValue)
                        sliderValueTemp = animationPlayerContainerFeedback[i].sliderValue;
                }
                else
                {
                    if (sliderValueTemp >= animationPlayerContainerFeedback[i].sliderValue)
                        sliderValueTemp = animationPlayerContainerFeedback[i].sliderValue;
                }
            }
        }

        private void Update()
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Direct)
            {
                if (animationPlayerContainerFeedback.Count > 0)
                {
                    AnimationFeedbackSetSlider();
                    AnimationInteractionSetSlider();
                }
                else
                {
                    AnimationInteractionSetSlider();
                }
            }
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop
                && ((virtualButtonDatas.isBack && !virtualButtonDatas.isSequentialClick) || ((isBackSequential == true || valueTemp == 0) && virtualButtonDatas.isSequentialClick)))
            {
                if (animationPlayerContainerFeedback.Count > 0)
                {
                    AnimationFeedbackSetSlider();
                }
            }
            else
            {
                AnimationInteractionSetSlider();
            }

            sliderValue = sliderValueTemp;
        }

        public override void PlayVirtualButton()
        {
            Debug.Log("play virtual button");
        }

        public override void StopVirtualButton()
        {
            Debug.Log("stop virtual button");
        }
    }
}

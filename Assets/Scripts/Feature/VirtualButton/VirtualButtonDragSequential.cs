using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Feature
{
    public partial class VirtualButtonDrag : VirtualButtonBase
    {
        void PlaySequentialDrag()
        {
            switch (virtualButtonDatas.sequencePlayMethod)
            {
                case SequencePlayMethod.TimeStampParameter:
                    SequentialDragTimeStamp();
                    break;
                case SequencePlayMethod.TimeStampScriptedAnimation:
                    SequentialDragTimeStamp();
                    break;
                case SequencePlayMethod.TimeStampState:
                    SequentialDragTimeStamp();
                    break;
                case SequencePlayMethod.PlayParameter:
                    PlaySequentialManual(true);
                    break;
                case SequencePlayMethod.PlayScriptedAnimation:
                    PlaySequentialAnimationScript();
                    break;
                case SequencePlayMethod.PlayState:
                    PlaySequentialManual(false);
                    break;
            }
        }

        void SequentialDragTimeStamp()
        {
            for (int i = 0; i < virtualButtonDatas.sequentialDragDatas.Count; i++)
            {
                if (pointerDeltaPosition > sliderCurrentValue)
                {
                    if (pointerDeltaPosition >= virtualButtonDatas.sequentialDragDatas[i].durations)
                    {
                        if (valueSequential < virtualButtonDatas.sequentialDragDatas[i].durations)
                            isPlaySequent = false;

                        sliderCurrentValue = virtualButtonDatas.sequentialDragDatas[i].durations;
                        PlayAllAnimationPlayer(i);
                    }
                }
                else if (pointerDeltaPosition < sliderCurrentValue)
                {
                    if (pointerDeltaPosition <= virtualButtonDatas.sequentialDragDatas[i].durations)
                    {
                        if (valueSequential > virtualButtonDatas.sequentialDragDatas[i].durations)
                            isPlaySequent = false;

                        sliderCurrentValue = virtualButtonDatas.sequentialDragDatas[i].durations;
                        PlayAllAnimationPlayer(i);
                        break;
                    }
                }
            }
        }

        void CheckSliderSequentData()
        {
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (i == 0)
                    sliderValue = animationPlayerContainerInteraction[i].sliderValue;
                else
                {
                    if (pointerDeltaPosition > 0)
                    {
                        if (animationPlayerContainerInteraction[i].sliderValue < sliderValue)
                            sliderValue = animationPlayerContainerInteraction[i].sliderValue;
                    }
                    else
                    {
                        if (animationPlayerContainerInteraction[i].sliderValue > sliderValue)
                            sliderValue = animationPlayerContainerInteraction[i].sliderValue;
                    }
                }
            }
        }

        void PlayAllAnimationPlayer(int valueTemp)
        {
            if (isPlaySequent == false)
            {
                valueSequential = virtualButtonDatas.sequentialDragDatas[valueTemp].durations;
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    animationPlayerContainerInteraction[i].StartPlayAnimation(valueSequential, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                }
                float valueSequential2 = virtualButtonDatas.sequentialDragDatas[valueTemp].durationSecond;

                if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferentValue)
                    StartCoroutine(PlayDelaySequentialFeedback(valueSequential2, valueTemp));
                else
                    StartCoroutine(PlayDelaySequentialFeedback(valueSequential, valueTemp));

                isPlaySequent = true;
            }
        }

        IEnumerator PlayDelaySequentialFeedback(float maxValue, int valueTemp)
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            {
                if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferentValue)
                {
                    yield return new WaitUntil(() => sliderValue == virtualButtonDatas.sequentialDragDatas[valueTemp].durations);
                }
                else
                    yield return new WaitUntil(() => sliderValue == maxValue);

            }
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                animationPlayerContainerFeedback[i].StartPlayAnimation(maxValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
            }
        }

        void PlaySequentialManual(bool isParameter)
        {
            if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis)
                return;

            if (pointerDeltaPosition < sliderCurrentValue)
            {
                float valueAnimation = (float)(valueTemp - 1) / (float)animationPlayerContainerInteraction.Count;
                if (pointerDeltaPosition < .1f)
                    pointerDeltaPosition = 0f;

                if (valueTemp > 0 && valueAnimation >= pointerDeltaPosition)
                {
                    sliderValue = valueAnimation;
                    sliderCurrentValue = valueAnimation;
                    valueTemp--;
                    PlayAnimationManual(valueTemp, 0, isParameter, 1);
                }
            }
            else
            {
                float valueAnimation = (float)(valueTemp + 1) / (float)animationPlayerContainerInteraction.Count;
                if (pointerDeltaPosition > .9f)
                    pointerDeltaPosition = 1.1f;

                if (valueTemp < animationPlayerContainerInteraction.Count && valueAnimation < pointerDeltaPosition)
                {
                    PlayAnimationManual(valueTemp, 1, isParameter, 0);
                    valueTemp++;
                    sliderCurrentValue = valueAnimation;
                    sliderValue = valueAnimation;

                    if (pointerDeltaPosition > 1)
                        pointerDeltaPosition = 1;
                }
            }
        }

        void PlayAnimationManual(int valueTemp, int maxValue, bool isParameter, float startSlider = 0)
        {
            AnimationPlayerManual animManual = animationPlayerContainerInteraction[valueTemp] as AnimationPlayerManual;
            if (isParameter)
            {
                animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
            }
            else
            {
                animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
            }
            StartCoroutine(DelayPlayAnimationManualFeedback(animManual, maxValue, valueTemp, startSlider, isParameter));
        }

        void PlaySequentialAnimationScript()
        {
            if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis)
                return;

            if (pointerDeltaPosition < sliderCurrentValue)
            {
                float valueAnimation = (float)(valueTemp - 1) / (float)animationPlayerContainerInteraction.Count;
                if (pointerDeltaPosition < .1f)
                    pointerDeltaPosition = 0f;

                if (valueTemp > 0 && valueAnimation >= pointerDeltaPosition)
                {
                    sliderValue = valueAnimation;
                    sliderCurrentValue = valueAnimation;
                    valueTemp--;
                    animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 0, valueTemp, 1));
                }
            }
            else
            {
                float valueAnimation = (float)(valueTemp + 1) / (float)animationPlayerContainerInteraction.Count;
                if (pointerDeltaPosition > .9f)
                    pointerDeltaPosition = 1.1f;

                if (valueTemp < animationPlayerContainerInteraction.Count && valueAnimation < pointerDeltaPosition)
                {
                    animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(1, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 1, valueTemp, 0));
                    valueTemp++;

                    sliderCurrentValue = valueAnimation;
                    sliderValue = valueAnimation;

                    if (pointerDeltaPosition > 1)
                        pointerDeltaPosition = 1;
                }
            }
        }
    }
}

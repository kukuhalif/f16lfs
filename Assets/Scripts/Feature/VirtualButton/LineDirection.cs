using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class LineDirection : MonoBehaviour
    {
        [SerializeField] LineRenderer lineRendererminXY;
        [SerializeField] LineRenderer lineRenderermaxX;
        [SerializeField] LineRenderer lineRenderermaxY;
        [SerializeField] LineRenderer lineRenderermaxXY;

        public void UpdateDirection2Axis(Vector3 minXY,Vector3 maxX, Vector3 maxY, Vector3 maxXY)
        {
            if (lineRendererminXY == null)
                lineRendererminXY = MakeLineRendererObject();
            if (lineRenderermaxX == null)
                lineRenderermaxX = MakeLineRendererObject();
            if (lineRenderermaxY == null)
                lineRenderermaxY = MakeLineRendererObject();
            if (lineRenderermaxXY == null)
                lineRenderermaxXY = MakeLineRendererObject();

            MakeALine(lineRendererminXY, maxX, minXY );
            MakeALine(lineRenderermaxX, maxXY, maxX);
            MakeALine(lineRenderermaxY, maxY,minXY);
            MakeALine(lineRenderermaxXY, maxXY, maxY);
        }
        public void UpdateDirection1Axis(Vector3 minXY, Vector3 maxX)
        {
            if (lineRendererminXY == null)
                lineRendererminXY = MakeLineRendererObject();

            MakeALine(lineRendererminXY, maxX, minXY);
        }


        LineRenderer MakeLineRendererObject()
        {
            GameObject newGO = new GameObject();
            newGO.transform.SetParent(this.transform);
            LineRenderer lineRenderer = newGO.AddComponent<LineRenderer>();
            lineRenderer.endWidth = 0.1f;
            lineRenderer.startWidth = 0.1f;
            return lineRenderer;
        }

        void MakeALine(LineRenderer lineRenderer, Vector3 point1, Vector3 point2)
        {
            lineRenderer.positionCount = 0;
            Vector3[] arrayTempsWorld = new Vector3[] { point1, point2 };
            lineRenderer.positionCount = arrayTempsWorld.Length;
            lineRenderer.SetPositions(arrayTempsWorld);
            lineRenderer.material.SetTextureOffset("_MainTex", Vector2.right * Time.time);
        }
    }
}

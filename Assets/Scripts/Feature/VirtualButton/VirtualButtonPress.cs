using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
namespace VirtualTraining.Feature
{
    public class VirtualButtonPress : VirtualButtonBase
    {
        bool isPressed;
        bool isHold;
        bool isMaximumInteraction;
        bool isMaximumFeedback;
        bool invertTemp;
        private void Update()
        {
            if(isVBAnimation == false)
            {
                PlayVirtualButtonPress();
                CheckSliderValue();
            }
        }
        public override void TriggerVirtualButton(float vbOnAnimValue = 0)
        {
            leftClickDown = true;
            isPressed = true;
            isHold = true;

            if (isVBAnimation)
            {
                PlayVBClickSameTime(vbOnAnimValue);
            }
            else
            {
                if (invertTemp != IsInvert)
                {
                    invertTemp = IsInvert;
                    if (sliderValue < 1 && sliderValue > 0)
                    {
                        isMaximumFeedback = !isMaximumFeedback;
                        isMaximumInteraction = !isMaximumInteraction;
                    }
                }
            }

        }
        void PlayVBPressInteraction()
        {
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (isMaximumInteraction)
                    animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.defaultValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                else
                    animationPlayerContainerInteraction[i].StartPlayAnimation(1, animationPlayerContainerInteraction[i].getVBOutputData.speed);
            }
        }

        void PlayVBPressFeedback()
        {
            if (isMaximumFeedback)
                StartCoroutine(PlayDelayAnimationFeedbackPress(virtualButtonDatas.defaultValue, false));
            else
                StartCoroutine(PlayDelayAnimationFeedbackPress(1, false));
        }
        void PlayVirtualButtonPress()
        {
            if (isHold)
            {
                VirtualTrainingCamera.IsMovementEnabled = false;
                if (sliderValue <= 1)
                {
                    PlayVBPressInteraction();
                    PlayVBPressFeedback();

                }
                isHold = false;
            }

            if (leftClickDown == false)
            {
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    if (virtualButtonDatas.isElastic)
                    {
                        if (isPressed == true)
                        {
                            switch (virtualButtonDatas.elasticType)
                            {
                                case ElasticType.Both:
                                    animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.defaultValue, virtualButtonDatas.elasticSpeed);
                                    StartCoroutine(PlayDelayAnimationFeedbackPress(virtualButtonDatas.defaultValue, true));
                                    break;
                                case ElasticType.Interaction:
                                    animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.defaultValue, virtualButtonDatas.elasticSpeed);
                                    StartCoroutine(StopDelayAnimationFeedbackPress());
                                    break;
                                case ElasticType.Feedback:
                                    animationPlayerContainerInteraction[i].PauseAnimation();
                                    StartCoroutine(PlayDelayAnimationFeedbackPress(virtualButtonDatas.defaultValue, true));
                                    break;
                            }
                        }
                    }
                    else
                    {
                        animationPlayerContainerInteraction[i].PauseAnimation();
                        StartCoroutine(StopDelayAnimationFeedbackPress());
                    }

                }
                if (virtualButtonDatas.isReturnToStart)
                {
                    if (sliderValue>= 1)
                        isMaximumInteraction = true;
                    else if (sliderValue <= 0)
                        isMaximumInteraction = false;
                }

                isPressed = false;
            }

        }

        IEnumerator PlayDelayAnimationFeedbackPress(float maxValue, bool isBackElastic)
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Direct)
                yield return new WaitForSeconds(0);
            else
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (virtualButtonDatas.isElastic)
                {
                    if (isBackElastic)
                        animationPlayerContainerFeedback[i].StartPlayAnimation(maxValue, virtualButtonDatas.elasticSpeed);
                    else
                        animationPlayerContainerFeedback[i].StartPlayAnimation(maxValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
                }
                else
                    animationPlayerContainerFeedback[i].StartPlayAnimation(maxValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
            }
        }

        IEnumerator StopDelayAnimationFeedbackPress()
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Direct)
                yield return new WaitForSeconds(0);
            else
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (virtualButtonDatas.isElastic)
                {
                    if (virtualButtonDatas.elasticType == ElasticType.Both || virtualButtonDatas.elasticType == ElasticType.Feedback)
                        animationPlayerContainerFeedback[i].StartPlayAnimation(0, animationPlayerContainerFeedback[i].getVBOutputData.speed);
                    else
                        animationPlayerContainerFeedback[i].PauseAnimation();
                }
                else
                    animationPlayerContainerFeedback[i].PauseAnimation();
            }
            if (virtualButtonDatas.isReturnToStart)
            {
                if (sliderValue >= 1)
                    isMaximumFeedback = true;
                else if (sliderValue <= 0)
                    isMaximumFeedback = false;
            }

        }

        void CheckSliderValue()
        {
            for (int j = 0; j < animationPlayerContainerInteraction.Count; j++)
            {
                if (j == 0)
                {
                    sliderValue = animationPlayerContainerInteraction[j].sliderValue;
                }
                else
                {
                    if (virtualButtonDatas.isReturnToStart)
                    {
                        if (isMaximumFeedback)
                        {
                            if (sliderValue <= animationPlayerContainerInteraction[j].sliderValue)
                                sliderValue = animationPlayerContainerInteraction[j].sliderValue;
                        }
                        else
                        {
                            if (sliderValue >= animationPlayerContainerInteraction[j].sliderValue)
                                sliderValue = animationPlayerContainerInteraction[j].sliderValue;
                        }
                    }
                    else
                    {
                        if (sliderValue > animationPlayerContainerInteraction[j].sliderValue)
                            sliderValue = animationPlayerContainerInteraction[j].sliderValue;
                    }
                }
            }
        }

        public override void PlayVirtualButton()
        {
            Debug.Log("play virtual button");
        }

        public override void StopVirtualButton()
        {
            Debug.Log("stop virtual button");
        }
    }
}

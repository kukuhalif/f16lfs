using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;
namespace VirtualTraining.Feature
{
    [System.Serializable]
    public class LineVirtualButton
    {
        public LineDirection lineDirection;
        public GameObject InteractionObject;

        public LineVirtualButton(LineDirection lineDirection, GameObject InteractionObject)
        {
            this.lineDirection = lineDirection;
            this.InteractionObject = InteractionObject;
        }
    }

    public partial class VirtualButtonDrag : VirtualButtonBase
    {
        const bool CREATE_LINE_DIRECTION = false;

        bool SetParamDrag;
        protected Vector2 pointerStart;
        protected Vector3 startPositionPointer;
        public float sliderCurrentValue;
        protected float pointerDeltaPosition;
        float x;
        float y;
        bool isPlaySequent;
        float valueSequential;
        int valueTemp;
        float deltaPositionAnimationX;
        float deltaPositionAnimationY;
        bool isPlaybyAnimation;

        Vector3 screenPos;
        Vector3 deltaPos = Vector3.zero;
        Vector3 startPostionClickTarget;

        Vector3 screenMaxPositionX; //00
        Vector3 screenMinPositionX; //01
        Vector3 screenMaxPositionY; //10
        Vector3 screenMaxPositionXY; //11

        Vector3 maxPositionWorldX;
        Vector3 minPositionWorldX;
        Vector3 maxPositionWorldY;
        Vector3 maxPositionWorldXY;

        float maxX, minX, maxY, minY;

        public List<ParameterSequentialData> parameterSequentialDatasInteraction = new List<ParameterSequentialData>();
        public List<ParameterSequentialData> parameterSequentialDatasFeedback = new List<ParameterSequentialData>();

        float limitX, limitY;
        Vector3 deltaPosTemp;
        [SerializeField] LineDirection lineDirection;
        [SerializeField] List<LineVirtualButton> lineVBDatas = new List<LineVirtualButton>();

        protected override void SetupVirtualButton()
        {
            base.SetupVirtualButton();
            valueTemp = 0;
            if (virtualButtonDatas.isSequentialDrag)
            {
                SetupAnimationPlayerBase(animationPlayerContainerInteraction, parameterSequentialDatasInteraction);
                SetupAnimationFeedback(animationPlayerContainerFeedback);
                for (int i = 0; i < virtualButtonDatas.sequentialDragDatas.Count; i++)
                {
                    if (virtualButtonDatas.sequentialDragDatas[i].durations == virtualButtonDatas.defaultValue)
                    {
                        PlayAllAnimationPlayer(valueTemp);
                        if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState || virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter ||
                            virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation)
                        {
                            valueTemp = i;
                        }
                        else
                            valueTemp = i + 1;
                    }
                }
            }
            pointerDeltaPosition = virtualButtonDatas.defaultValue;
            x = virtualButtonDatas.defaultValue;
            y = virtualButtonDatas.defaultValue;
            Debug.Log("setup vb");
            MakeLineDirectionOnStart();
            PlayVBDrag(true);
        }

        Vector3 CalculatePositionObject(GameObject interactionObject, Vector3 addedPosition)
        {
            Vector3 localpositionStart = interactionObject.transform.localPosition;
            interactionObject.transform.localPosition = new Vector3(interactionObject.transform.localPosition.x + (addedPosition.x * interactionObject.transform.lossyScale.x),
                interactionObject.transform.localPosition.y + (addedPosition.y * interactionObject.transform.lossyScale.y),
                interactionObject.transform.localPosition.z + (addedPosition.z * interactionObject.transform.lossyScale.z));

            Vector3 positionResult = interactionObject.transform.position;

            interactionObject.transform.localPosition = localpositionStart;

            return positionResult;
        }

        void MakeLineDirectionOnStart()
        {
            if (CREATE_LINE_DIRECTION)
            {
                for (int i = 0; i < clickTargetDatas.Count; i++)
                {
                    maxPositionWorldX = CalculatePositionObject(clickTargetDatas[i].gameObject.gameObject, clickTargetDatas[i].maximalPositionX);
                    minPositionWorldX = CalculatePositionObject(clickTargetDatas[i].gameObject.gameObject, clickTargetDatas[i].minimalPositionXY);
                    maxPositionWorldY = CalculatePositionObject(clickTargetDatas[i].gameObject.gameObject, clickTargetDatas[i].maximalPositionY);
                    maxPositionWorldXY = CalculatePositionObject(clickTargetDatas[i].gameObject.gameObject, clickTargetDatas[i].maximalPositionXY);

                    MakeLineDirection(clickTargetDatas[i].gameObject.gameObject);
                }
            }
        }

        void SetupScreenMode(GameObject interactionObject)
        {
            if (sliderValue == virtualButtonDatas.defaultValue)
            {
                maxPositionWorldX = CalculatePositionObject(interactionObject, currentClickTarget.maximalPositionX);
                minPositionWorldX = CalculatePositionObject(interactionObject, currentClickTarget.minimalPositionXY);
                maxPositionWorldY = CalculatePositionObject(interactionObject, currentClickTarget.maximalPositionY);
                maxPositionWorldXY = CalculatePositionObject(interactionObject, currentClickTarget.maximalPositionXY);

            }
            if (isMonitorCamera == true)
            {
                screenMaxPositionX = VirtualTrainingCamera.MonitorCamera.WorldToViewportPoint(maxPositionWorldX);
                screenMaxPositionX = CheckPositionMonitorCameraPanel(screenMaxPositionX);

                screenMinPositionX = VirtualTrainingCamera.MonitorCamera.WorldToViewportPoint(minPositionWorldX);
                screenMinPositionX = CheckPositionMonitorCameraPanel(screenMinPositionX);

                screenMaxPositionY = VirtualTrainingCamera.MonitorCamera.WorldToViewportPoint(maxPositionWorldY);
                screenMaxPositionY = CheckPositionMonitorCameraPanel(screenMaxPositionY);

                screenMaxPositionXY = VirtualTrainingCamera.MonitorCamera.WorldToViewportPoint(maxPositionWorldXY);
                screenMaxPositionXY = CheckPositionMonitorCameraPanel(screenMaxPositionXY);
            }
            else
            {
                screenMaxPositionX = VirtualTrainingCamera.CurrentCamera.WorldToScreenPoint(maxPositionWorldX);
                screenMinPositionX = VirtualTrainingCamera.CurrentCamera.WorldToScreenPoint(minPositionWorldX);
                screenMaxPositionY = VirtualTrainingCamera.CurrentCamera.WorldToScreenPoint(maxPositionWorldY);
                screenMaxPositionXY = VirtualTrainingCamera.CurrentCamera.WorldToScreenPoint(maxPositionWorldXY);
            }

            Vector3[] arrayTemps = new Vector3[] { screenMaxPositionX, screenMinPositionX, screenMaxPositionY, screenMaxPositionXY };

            for (int i = 0; i < arrayTemps.Length; i++)
            {
                if (i == 0)
                {
                    maxX = arrayTemps[i].x;
                    minX = arrayTemps[i].x;
                    maxY = arrayTemps[i].y;
                    minY = arrayTemps[i].y;
                }
                else
                {
                    if (arrayTemps[i].x > maxX)
                    {
                        maxX = arrayTemps[i].x;
                    }
                    if (arrayTemps[i].x < minX)
                        minX = arrayTemps[i].x;
                    if (arrayTemps[i].y > maxY)
                    {
                        maxY = arrayTemps[i].y;
                    }
                    if (arrayTemps[i].y < minY)
                        minY = arrayTemps[i].y;
                }

                if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis)
                {
                    //if (x != virtualButtonDatas.defaultValue)
                    //{
                    if (isVR && isMonitorCamera == false)
                    {
                        if (HandVR != null)
                        {
                            deltaPosTemp = CheckPointerPositionTwoaxis(minPositionWorldX, maxPositionWorldX, maxPositionWorldY, maxPositionWorldXY)
                                - HandVR.gameObject.transform.position;
                        }
                        else
                        {
                            if (objectVB == null)
                            {
                                MakeAnObjectVB();
                            }

                            ConvertPositiontoHandVRCamera();
                            deltaPosTemp = CheckPointerPositionTwoaxis(screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY) - VirtualTrainingCamera.handVR.GetComponent<Camera>().WorldToScreenPoint(objectVB.transform.position);
                        }
                    }
                    else if (isVR && isMonitorCamera == true)
                    {
                        Vector3 positionPointer = PositionPointerOnMonitorPanel();
                        deltaPosTemp = CheckPointerPositionTwoaxis(screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY) - positionPointer;
                    }
                    else
                    {
                        Vector3 positionPointer = new Vector3(VirtualTrainingInputSystem.PointerPosition.x, VirtualTrainingInputSystem.PointerPosition.y, 0);
                        deltaPosTemp = CheckPointerPositionTwoaxis(screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY) - positionPointer;
                    }
                    //}
                }
                else // for one axis
                {
                    Vector2 delta;
                    Vector2 temps;
                    GameObject objectPivot;
                    if (isVR && isMonitorCamera == false)
                    {
                        if (HandVR != null)
                        {
                            objectPivot = HandVR.gameObject;
                        }
                        else
                        {
                            if (objectVB == null)
                            {
                                MakeAnObjectVB();
                            }
                            objectPivot = objectVB;
                        }

                        delta = Vector3.Lerp(minPositionWorldX, maxPositionWorldX, pointerDeltaPosition);
                        temps = ProjectPointOnLineSegment(minPositionWorldX, maxPositionWorldX, objectPivot.transform.position);
                        deltaPosTemp = delta - temps;
                    }
                    else if (isVR && isMonitorCamera == true && VirtualTrainingCamera.monitorCameraPanel != null)
                    {
                        delta = Vector3.Lerp(screenMinPositionX, screenMaxPositionX, pointerDeltaPosition);
                        Vector3 positionPointer = PositionPointerOnMonitorPanel();

                        temps = ProjectPointOnLineSegment(screenMinPositionX, screenMaxPositionX, positionPointer);
                        deltaPosTemp = delta - temps;
                    }
                    else
                    {
                        delta = Vector3.Lerp(screenMinPositionX, screenMaxPositionX, pointerDeltaPosition);
                        temps = ProjectPointOnLineSegment(screenMinPositionX, screenMaxPositionX, VirtualTrainingInputSystem.PointerPosition);
                        deltaPosTemp = delta - temps;
                    }
                }
            }
        }

        Vector3 PositionPointerOnMonitorPanel()
        {
            Vector3 pointerPosition = Vector3.zero;
            if (VirtualTrainingCamera.monitorCameraPanel != null)
            {
                RectTransform rectTemp = VirtualTrainingCamera.monitorCameraPanel;
                pointerPosition = new Vector3(VirtualTrainingCamera.PositionOnMonitorCamera.x * rectTemp.rect.width, VirtualTrainingCamera.PositionOnMonitorCamera.y * rectTemp.rect.height, 0);
                return pointerPosition;
            }
            else
                Debug.Log("Monitor Panel Null");

            return pointerPosition;
        }
        void MakeAnObjectVB()
        {
            //objectVB = GameObject.CreatePrimitive(PrimitiveType.Cube);
            objectVB = new GameObject();
            objectVB.transform.SetParent(VirtualTrainingCamera.handVR.transform);
            objectVB.transform.position = clickTargetCurrent.transform.position;
        }
        void ConvertPositiontoHandVRCamera()
        {
            if (VirtualTrainingCamera.handVR.GetComponent<Camera>() != null)
            {
                screenMinPositionX = VirtualTrainingCamera.handVR.GetComponent<Camera>().WorldToScreenPoint(minPositionWorldX);
                screenMaxPositionX = VirtualTrainingCamera.handVR.GetComponent<Camera>().WorldToScreenPoint(maxPositionWorldX);
                screenMaxPositionY = VirtualTrainingCamera.handVR.GetComponent<Camera>().WorldToScreenPoint(maxPositionWorldY);
                screenMaxPositionXY = VirtualTrainingCamera.handVR.GetComponent<Camera>().WorldToScreenPoint(maxPositionWorldXY);
            }
        }

        void MakeLineDirection(GameObject interactionObject)
        {
            GameObject lineDirectionTemp = new GameObject("Line Direction");
            lineDirectionTemp.transform.SetParent(this.transform);
            LineDirection lines = lineDirectionTemp.AddComponent<LineDirection>();

            if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis)
                lines.UpdateDirection2Axis(minPositionWorldX, maxPositionWorldX, maxPositionWorldY, maxPositionWorldXY);
            else // for one axis
                lines.UpdateDirection1Axis(minPositionWorldX, maxPositionWorldX);

            LineVirtualButton lineVB = new LineVirtualButton(lines, interactionObject);
            lineVBDatas.Add(lineVB);
        }

        Vector2 CheckPositionMonitorCameraPanel(Vector2 PositionTemp)
        {
            if (VirtualTrainingCamera.monitorCameraPanel != null)
            {
                RectTransform rectTemp = VirtualTrainingCamera.monitorCameraPanel;
                PositionTemp = new Vector3(PositionTemp.x * rectTemp.rect.width, PositionTemp.y * rectTemp.rect.height, 0);
                if (isVR == false)
                {
                    PositionTemp.x = PositionTemp.x + (rectTemp.position.x - (rectTemp.rect.width * .5f));
                    PositionTemp.y = PositionTemp.y + (rectTemp.position.y - (rectTemp.rect.height * .5f));
                }

                Debug.Log("pos temp " + PositionTemp);
                return PositionTemp;
            }

            return Vector2.zero;
        }

        void SetupAnimationPlayerBase(List<AnimationPlayerBase> animationPlayerBases, List<ParameterSequentialData> parameterSequentialDatas)
        {
            for (int i = 0; i < animationPlayerBases.Count; i++)
            {
                if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter)
                {
                    for (int j = 0; j < animationPlayerBases[i].getVBOutputData.parameterDatas.Count; j++)
                    {
                        ParameterSequentialData paramSequentData = new ParameterSequentialData();
                        paramSequentData.animationPlayerBase = animationPlayerBases[i];
                        paramSequentData.parameterDatasInteraction = animationPlayerBases[i].getVBOutputData.parameterDatas[j];
                        parameterSequentialDatas.Add(paramSequentData);

                    }
                }
                else if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState)
                {
                    for (int l = 0; l < animationPlayerBases[i].getVBOutputData.animationDatas.Count; l++)
                    {
                        ParameterSequentialData paramSequentData = new ParameterSequentialData();
                        paramSequentData.animationPlayerBase = animationPlayerBases[i];
                        paramSequentData.animationState = animationPlayerBases[i].getVBOutputData.animationDatas[l].animationState;
                        parameterSequentialDatas.Add(paramSequentData);
                    }
                }
            }
        }

        void SetupAnimationFeedback(List<AnimationPlayerBase> animationPlayerBases)
        {
            for (int i = 0; i < animationPlayerBases.Count; i++)
            {
                if (animationPlayerBases[i].getVBOutputData.isAnimationScript)
                {
                    ParameterSequentialData paramSequentData = new ParameterSequentialData();
                    paramSequentData.animationPlayerBase = animationPlayerBases[i];
                    parameterSequentialDatasFeedback.Add(paramSequentData);
                }
                else
                {
                    AnimationPlayerManual animManual = animationPlayerBases[i] as AnimationPlayerManual;
                    if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    {
                        for (int j = 0; j < animManual.getVBOutputData.parameterDatas.Count; j++)
                        {
                            ParameterSequentialData paramSequentData = new ParameterSequentialData();
                            paramSequentData.animationPlayerBase = animationPlayerBases[i];
                            paramSequentData.parameterDatasInteraction = animManual.getVBOutputData.parameterDatas[j];
                            parameterSequentialDatasFeedback.Add(paramSequentData);
                        }
                    }
                    else
                    {
                        for (int l = 0; l < animationPlayerBases[i].getVBOutputData.animationDatas.Count; l++)
                        {
                            ParameterSequentialData paramSequentData = new ParameterSequentialData();
                            paramSequentData.animationPlayerBase = animationPlayerBases[i];
                            paramSequentData.animationState = animManual.getVBOutputData.animationDatas[l].animationState;
                            parameterSequentialDatasFeedback.Add(paramSequentData);
                        }
                    }
                }
            }
        }

        public void SetDeltaPositionAnimation(float valueX, float valueY)
        {
            //if (virtualButtonDatas.direction == DragDirection.Both)
            if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis)
            {
                SetParamDrag = true;
                leftClickDown = false;

                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    AnimationPlayerManual animationManual = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                    if (SetParamDrag == true)
                    {
                        animationManual.setCurrentParam = true;
                    }
                    //valueX = valueX * (animationManual.getVBOutputData.maximalValue - animationManual.getVBOutputData.minimalValue);
                    valueX = Mathf.Lerp(animationManual.getVBOutputData.minimalValue, animationManual.getVBOutputData.maximalValue, valueX);
                    //valueY = valueY * (animationManual.getVBOutputData.maximalValue - animationManual.getVBOutputData.minimalValue);
                    valueY = Mathf.Lerp(animationManual.getVBOutputData.minimalValue, animationManual.getVBOutputData.maximalValue, valueY);
                    animationManual.SetBothDrag(valueX, valueY, true);

                }
                StartCoroutine(PlayBothFeedback(valueX, valueY));

                //SetParamDrag = true;
                pointerDeltaPosition = 1;
            }
            else
            {
                deltaPositionAnimationX = valueX;
                deltaPositionAnimationY = valueY;
            }
        }

        private void Update()
        {
            if (isVBAnimation == false)
            {
                if (leftClickDown)
                    PlayVBDrag();

                if ((virtualButtonDatas.vbType == VirtualButtonType.Click && !virtualButtonDatas.isSequentialClick) || (virtualButtonDatas.vbType == VirtualButtonType.Drag && !virtualButtonDatas.isSequentialDrag && virtualButtonDatas.isElastic))
                    ElasticVBDrag();

                //if (virtualButtonDatas.isSequentialDrag)
                //    CheckSliderSequentData();
            }
        }

        public override void TriggerVirtualButton(float vbOnAnimValue = 0)
        {
            if (isVBAnimation)
            {
                if (virtualButtonDatas.isSequentialClick)
                {
                    PlayVBClickNotSameTime(vbOnAnimValue);
                }
                else
                {
                    PlayVBClickSameTime(vbOnAnimValue);
                }

            }
            else
            {
                pointerStart = VirtualTrainingInputSystem.PointerPosition;
                Vector3 pointer = new Vector3(pointerStart.x, pointerStart.y, 0);
                if (clickTargetCurrent != null)
                {
                    startPositionPointer = pointer;
                    startPostionClickTarget = clickTargetCurrent.transform.position;
                    if (virtualButtonDatas.isSequentialDrag)
                        pointerDeltaPosition = sliderValue;
                    SetupScreenMode(clickTargetCurrent);
                }

                sliderCurrentValue = sliderValue;

                SetParamDrag = false;
                limitX = 0;
                limitY = 0;
                leftClickDown = true;
            }

        }
        float WorldToScreen1axis(Vector3 screenMaxObject, Vector3 screenMinObject, Vector3 pointerCont)
        {
            Vector3 mousePosition = pointerCont + deltaPosTemp;

            Vector3 projectedPoints = ProjectPointOnLineSegment(screenMinObject, screenMaxObject, mousePosition);

            float distanceToProjected = Vector3.Distance(screenMinObject, projectedPoints);
            float distanceAll = Vector3.Distance(screenMinObject, screenMaxObject);
            return (distanceToProjected / distanceAll);
        }

        public bool IsPointInArea(Vector2 point, Vector2[] polygon)
        {
            int polygonLength = polygon.Length, i = 0;
            bool inside = false;
            // x, y for tested point.
            float pointX = point.x, pointY = point.y;
            // start / end point for the current polygon segment.
            float startX, startY, endX, endY;
            Vector2 endPoint = polygon[polygonLength - 1];
            endX = endPoint.x;
            endY = endPoint.y;
            while (i < polygonLength)
            {
                startX = endX; startY = endY;
                endPoint = polygon[i++];
                endX = endPoint.x; endY = endPoint.y;
                //
                inside ^= (endY > pointY ^ startY > pointY) /* ? pointY inside [startY;endY] segment ? */
                          && /* if so, test if it is under the segment */
                          ((pointX - endX) < (pointY - endY) * (startX - endX) / (startY - endY));
            }
            return inside;
        }

        Vector2 WorldToScreen2Axis(Vector3 minPositionX, Vector3 maxPositionX, Vector3 MaxPositionY, Vector3 MaxPositionXY)
        {
            Vector3 mousePosition;
            mousePosition = deltaPos + deltaPosTemp;

            Vector2[] areaLimit = new Vector2[] { minPositionX, maxPositionX, MaxPositionXY, MaxPositionY };
            bool isInArea = IsPointInArea(mousePosition, areaLimit);

            if (isInArea == false)
            {
                return Vector2OutsideArea(mousePosition);
            }

            Vector2 positionsScreen = Calculate2AxisinScreen(maxPositionX, minPositionX, MaxPositionY, MaxPositionXY, mousePosition);

            return positionsScreen;
        }

        Vector3 CheckPointerPositionTwoaxis(Vector3 minX, Vector3 maxX, Vector3 maxY, Vector3 maxXY)
        {
            Vector3 positionXaxis1 = Vector3.Lerp(minX, maxX, x);
            Vector3 positionXaxis2 = Vector3.Lerp(maxY, maxXY, x);
            Vector3 positionOnScreen = Vector3.Lerp(positionXaxis1, positionXaxis2, y);
            return positionOnScreen;
        }

        Vector2 Vector2OutsideArea(Vector3 mousePosition)
        {
            float minXmaxXSide = MouseDistanceToLine(mousePosition, screenMinPositionX, screenMaxPositionX);
            float mInXmaxYSide = MouseDistanceToLine(mousePosition, screenMinPositionX, screenMaxPositionY);
            float maxYmaxXYSide = MouseDistanceToLine(mousePosition, screenMaxPositionXY, screenMaxPositionY);
            float maxXmaxXYSide = MouseDistanceToLine(mousePosition, screenMaxPositionXY, screenMaxPositionX);

            float[] arrayValue = new float[] { minXmaxXSide, mInXmaxYSide, maxYmaxXYSide, maxXmaxXYSide };
            float lowest = LowestValue(arrayValue);

            if (lowest == minXmaxXSide) // sisi X bawah
            {
                return ValueOutsideArea(mousePosition, screenMinPositionX, screenMaxPositionY, screenMaxPositionX, screenMaxPositionXY, true);
            }
            else if (lowest == mInXmaxYSide) //sisi Y kanan
            {
                return ValueOutsideArea(mousePosition, screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY, false);
            }
            else if (lowest == maxYmaxXYSide) //sisi X Atas
            {
                return ValueOutsideArea(mousePosition, screenMaxPositionY, screenMinPositionX, screenMaxPositionXY, screenMaxPositionX, true);
            }
            else // sisi Y kiri
            {
                return ValueOutsideArea(mousePosition, screenMaxPositionX, screenMinPositionX, screenMaxPositionXY, screenMaxPositionY, false);
            }
        }
        float LowestValue(float[] arrayValue)
        {
            float lowest = 0;
            for (int i = 0; i < arrayValue.Length; i++)
            {
                if (i == 0)
                    lowest = arrayValue[0];
                else
                {
                    if (arrayValue[i] < lowest)
                        lowest = arrayValue[i];
                }
            }
            return lowest;
        }

        Vector2 ValueOutsideArea(Vector3 mousePosition, Vector3 vector00, Vector3 vector01, Vector3 vector10, Vector3 vector11, bool xBool)
        {
            Vector3 newPositionXmax = vector01 - vector00;
            Vector3 newPositionYmax = vector11 - vector10;

            float DistanceToMax = MouseDistanceToLine(mousePosition, vector10, (vector10 + (newPositionYmax * -100)));
            float distanceToMin = MouseDistanceToLine(mousePosition, vector00, (vector00 + (newPositionXmax * -100)));

            Vector2[] areaLimit = new Vector2[] { vector00, vector00 + (newPositionXmax * -100), vector10 + (newPositionYmax * -100), vector10 };
            bool isInArea = IsPointInArea(mousePosition, areaLimit);
            if (isInArea)
            {

                float totalDistance = distanceToMin + DistanceToMax;
                float valueTemp = distanceToMin / totalDistance;
                if (xBool == true)
                    return new Vector2(valueTemp, y);
                else
                    return new Vector2(x, valueTemp);
            }
            else
                return new Vector2(x, y);
        }

        public bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
        {

            closestPointLine1 = Vector3.zero;
            closestPointLine2 = Vector3.zero;

            float a = Vector3.Dot(lineVec1, lineVec1);
            float b = Vector3.Dot(lineVec1, lineVec2);
            float e = Vector3.Dot(lineVec2, lineVec2);

            float d = a * e - b * b;

            //lines are not parallel
            if (d != 0.0f)
            {

                Vector3 r = linePoint1 - linePoint2;
                float c = Vector3.Dot(lineVec1, r);
                float f = Vector3.Dot(lineVec2, r);

                float s = (b * f - c * e) / d;
                float t = (a * f - c * b) / d;

                closestPointLine1 = linePoint1 + lineVec1 * s;
                closestPointLine2 = linePoint2 + lineVec2 * t;

                return true;
            }

            else
            {
                return false;
            }
        }
        public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point)
        {

            //get vector from point on line to point in space
            Vector3 linePointToPoint = point - linePoint;

            float t = Vector3.Dot(linePointToPoint, lineVec);

            return linePoint + lineVec * t;
        }
        public Vector3 ProjectPointOnLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
        {

            Vector3 vector = linePoint2 - linePoint1;

            Vector3 projectedPoint = ProjectPointOnLine(linePoint1, vector.normalized, point);

            int side = PointOnWhichSideOfLineSegment(linePoint1, linePoint2, projectedPoint);

            //The projected point is on the line segment
            if (side == 0)
            {

                return projectedPoint;
            }

            if (side == 1)
            {

                return linePoint1;
            }

            if (side == 2)
            {

                return linePoint2;
            }

            //output is invalid
            return Vector3.zero;
        }
        public int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
        {

            Vector3 lineVec = linePoint2 - linePoint1;
            Vector3 pointVec = point - linePoint1;

            float debug = pointVec.magnitude;

            float dot = Vector3.Dot(pointVec, lineVec);

            //point is on side of linePoint2, compared to linePoint1
            if (dot > 0)
            {

                //point is on the line segment
                if (pointVec.magnitude <= lineVec.magnitude)
                {
                    //if (dot <= lineVec.magnitude) {

                    return 0;
                }

                //point is not on the line segment and it is on the side of linePoint2
                else
                {

                    return 2;
                }
            }

            //Point is not on side of linePoint2, compared to linePoint1.
            //Point is not on the line segment and it is on the side of linePoint1.
            else
            {

                return 1;
            }
        }

        public float MouseDistanceToLine(Vector3 DragPosition, Vector3 linePoint1, Vector3 linePoint2)
        {

            Camera currentCamera;
            Vector3 mousePosition;

#if UNITY_EDITOR
            if (Camera.current != null)
            {

                //currentCamera = Camera.current;
            }

            else
            {

                //currentCamera = Camera.main;
            }

            //convert format because y is flipped
            //mousePosition = new Vector3(Event.current.mousePosition.x, currentCamera.pixelHeight - Event.current.mousePosition.y, 0f);
            mousePosition = DragPosition;

#else
		currentCamera = VirtualTrainingCamera.CurrentCamera;
		mousePosition = DragPosition;
#endif

            Vector3 screenPos1 = linePoint1;//currentCamera.WorldToScreenPoint(linePoint1);
            Vector3 screenPos2 = linePoint2;//currentCamera.WorldToScreenPoint(linePoint2);
            Vector3 projectedPoint = ProjectPointOnLineSegment(screenPos1, screenPos2, mousePosition);

            //set z to zero
            projectedPoint = new Vector3(projectedPoint.x, projectedPoint.y, 0f);

            Vector3 vector = projectedPoint - mousePosition;
            return vector.magnitude;
        }

        Vector2 Calculate2AxisinScreen(Vector3 screenMaxX, Vector3 screenMinX, Vector3 screenMaxY, Vector3 screenMaxXY, Vector3 dragPosition)
        {
            float coorXs = x;
            float coorYs = y;

            float distanceTotalXs = Vector3.Distance(screenMinX, screenMaxX);
            float distanceTotalYs = Vector3.Distance(screenMinX, screenMaxY);

            float DistanceX1 = MouseDistanceToLine(dragPosition, screenMinX, screenMaxY);
            float DistanceX2 = MouseDistanceToLine(dragPosition, screenMaxX, screenMaxXY);
            float DistanceXTotal = DistanceX1 + DistanceX2;

            float DistanceY1 = MouseDistanceToLine(dragPosition, screenMinX, screenMaxX);
            float DistanceY2 = MouseDistanceToLine(dragPosition, screenMaxXY, screenMaxY);
            float DistanceYTotal = DistanceY1 + DistanceY2;

            //coorXs = MouseDistanceToLine(dragPosition, screenMinX, screenMaxY) / distanceTotalXs;
            coorXs = DistanceX1 / DistanceXTotal;

            //coorYs = MouseDistanceToLine(dragPosition, screenMinX, screenMaxX) / distanceTotalYs;
            coorYs = DistanceY1 / DistanceYTotal;

            Vector3 differencesX = Vector3.Lerp(screenMinX, screenMaxX, coorXs);
            Vector3 differencesY = Vector3.Lerp(screenMaxXY, screenMaxY, coorYs);

            Vector2 resultScreen = new Vector2(coorXs, coorYs);
            return resultScreen;
        }

        void PlayVBDrag(bool isFirst = false)
        {
            if (clickTargetCurrent != null)
            {
                Vector3 newScreenPos = new Vector3(VirtualTrainingInputSystem.PointerPosition.x, VirtualTrainingInputSystem.PointerPosition.y, 0);
                //deltaPos = newScreenPos - startPositionPointer;

                if (isVR)
                {
                    if (isMonitorCamera)
                    {
                        deltaPos = VirtualTrainingCamera.PositionOnMonitorCamera;
                    }
                    else
                    {
                        if (HandVR != null)
                            deltaPos = HandVR.transform.position;
                        else
                        {
                            if (virtualButtonDatas.axisCount == DirectionAxisCount.OneAxis)
                            {
                                if (objectVB != null)
                                    deltaPos = objectVB.transform.position;
                            }
                            else
                            {
                                if (objectVB != null)
                                    deltaPos = VirtualTrainingCamera.handVR.GetComponent<Camera>().WorldToScreenPoint(objectVB.transform.position);
                            }
                        }
                    }
                }
                else
                    deltaPos = VirtualTrainingInputSystem.PointerPosition;
            }

            if (virtualButtonDatas.axisCount == DirectionAxisCount.OneAxis && isFirst == false)
            {
                if (isVR && isMonitorCamera == false)
                {
                    if (HandVR != null)
                        pointerDeltaPosition = WorldToScreen1axis(maxPositionWorldX, minPositionWorldX, HandVR.position);
                    else
                        pointerDeltaPosition = WorldToScreen1axis(maxPositionWorldX, minPositionWorldX, objectVB.transform.position);
                }
                else if (isVR && isMonitorCamera == true && VirtualTrainingCamera.monitorCameraPanel != null)
                {
                    Vector3 pointerInMonitorCamera = PositionPointerOnMonitorPanel();
                    pointerDeltaPosition = WorldToScreen1axis(screenMaxPositionX, screenMinPositionX, pointerInMonitorCamera);
                }
                else
                    pointerDeltaPosition = WorldToScreen1axis(screenMaxPositionX, screenMinPositionX, VirtualTrainingInputSystem.PointerPosition);
            }
            else if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis && isFirst == false)
            {
                if (isPlaybyAnimation == false)
                {
                    if (isVR && isMonitorCamera == false)
                    {
                        if (HandVR != null)
                        {
                            x = WorldToScreen1axis(maxPositionWorldX, minPositionWorldX, HandVR.position);
                            y = WorldToScreen1axis(maxPositionWorldY, minPositionWorldX, HandVR.position);
                        }
                        else
                        {
                            ConvertPositiontoHandVRCamera();

                            Vector2 position = WorldToScreen2Axis(screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY);
                            x = position.x;
                            y = position.y;
                        }
                    }
                    else if (isVR && isMonitorCamera == true && VirtualTrainingCamera.monitorCameraPanel != null)
                    {
                        deltaPos = PositionPointerOnMonitorPanel();
                        Vector2 position = WorldToScreen2Axis(screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY);
                        x = position.x;
                        y = position.y;
                    }
                    else
                    {
                        Vector2 position = WorldToScreen2Axis(screenMinPositionX, screenMaxPositionX, screenMaxPositionY, screenMaxPositionXY);
                        x = position.x;
                        y = position.y;
                    }

                }
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    AnimationPlayerManual animationManual = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                    animationManual.SetBothDrag(x, y);
                    if (SetParamDrag == false)
                    {
                        animationManual.setCurrentParam = false;
                    }
                }
                StartCoroutine(PlayBothFeedback(x, y));

                SetParamDrag = true;
                pointerDeltaPosition = 1;
            }

            if (IsInvert)
                pointerDeltaPosition = pointerDeltaPosition * -1;

            if (sliderValue >= 0)
            {
                if (virtualButtonDatas.isSequentialDrag)
                {
                    PlaySequentialDrag();
                }
                else
                {
                    sliderValue = /*sliderCurrentValue +*/ pointerDeltaPosition;
                    for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                    {
                        animationPlayerContainerInteraction[i].SetSliderPercentage(sliderValue);

                        if (animationPlayerContainerInteraction[i].getVBOutputData.isAnimationScript)
                        {
                            animationPlayerContainerInteraction[i].StartPlayAnimation(sliderValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                        }
                        else
                        {
                            AnimationPlayerManual animManualTemp = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                            string stateName = animManualTemp.getStateName;
                            if (!string.IsNullOrEmpty(stateName) || animationPlayerContainerInteraction[i].getVBOutputData.isAnimationScript)
                                animationPlayerContainerInteraction[i].StartPlayAnimation(sliderValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                        }

                    }
                    StartCoroutine(PlayDelayAnimationFeedbackDrag(sliderValue));
                }
            }
            if (sliderValue <= 0)
                sliderValue = 0;
            if (sliderValue > 1)
                sliderValue = 1;
        }
        IEnumerator PlayDelayAnimationFeedbackDrag(float sliderValue)
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                string stateName = "";
                animationPlayerContainerFeedback[i].SetSliderPercentage(sliderValue);

                if (animationPlayerContainerFeedback[i].isAnimatedScript == false)
                {
                    AnimationPlayerManual animManualTemp = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
                    stateName = animManualTemp.getStateName;
                }

                if (virtualButtonDatas.isElastic)
                    animationPlayerContainerFeedback[i].StartPlayAnimation(sliderValue, virtualButtonDatas.elasticSpeed);
                else if (!string.IsNullOrEmpty(stateName) || animationPlayerContainerFeedback[i].getVBOutputData.isAnimationScript)
                    animationPlayerContainerFeedback[i].StartPlayAnimation(sliderValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
            }
        }

        IEnumerator DelayPlayAnimationManualFeedback(AnimationPlayerBase animBase, float maxValue, int valueTemp, float startSlider = 0, bool isParameter = false)
        {
            if (CheckModifierExcept((float)(valueTemp + 1) / (float)animationPlayerContainerInteraction.Count) == true)
                yield break;

            if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            {
                if (animBase.getVBOutputData.isAnimationScript)
                    yield return new WaitUntil(() => animBase.sliderValue == maxValue);
                else
                {
                    AnimationPlayerManual animManual = animBase as AnimationPlayerManual;
                    if (isParameter)
                        yield return new WaitUntil(() => animManual.getSliderPercentageTemp == maxValue);
                    else
                        yield return new WaitUntil(() => animBase.sliderValue == maxValue);
                }
            }
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            if (valueTemp < animationPlayerContainerFeedback.Count)
            {
                if (animationPlayerContainerFeedback[valueTemp].getVBOutputData.isAnimationScript)
                {
                    AnimationPlayerScript animScript = animationPlayerContainerFeedback[valueTemp] as AnimationPlayerScript;
                    animScript.StartPlayAnimation(maxValue, animScript.getVBOutputData.speed);
                }
                else
                {
                    AnimationPlayerManual animManual = animationPlayerContainerFeedback[valueTemp] as AnimationPlayerManual;
                    if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    {
                        Debug.Log("vb drag by parameter " + valueTemp + " " + maxValue);
                        //animManual.SetupParameterDatas(animationPlayerContainerFeedback[valueTemp].parameterDatasInteraction.minimalValue, parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.maximalValue,
                        //animationPlayerContainerFeedback[valueTemp].getVBDataModel.defaultValue, parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.parameter, null);
                        animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
                    }
                    else
                    {
                        Debug.Log("vb drag by state");
                        animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
                    }
                }
            }
        }

        IEnumerator PlayBothFeedback(float x, float y)
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (animationPlayerContainerFeedback[i].isAnimatedScript == false)
                {
                    AnimationPlayerManual animationManual = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
                    animationManual.SetBothDrag(x, y);
                    if (SetParamDrag == false)
                    {
                        animationManual.setCurrentParam = false;
                    }
                }
            }
        }

        void ElasticVBDrag()
        {
            if (virtualButtonDatas.isElastic == true && leftClickDown == false && !virtualButtonDatas.isSequentialDrag)
            {
                //if (virtualButtonDatas.direction == DragDirection.Both)
                if (virtualButtonDatas.axisCount == DirectionAxisCount.TwoAxis)
                {
                    for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                    {
                        AnimationPlayerManual animManual = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                        animManual.SetElasticBothDrag(virtualButtonDatas.elasticSpeed);
                    }
                    StartCoroutine(DelayElasticDragBoth());
                }
                else
                {
                    sliderValue = Mathf.Lerp(sliderValue, virtualButtonDatas.defaultValue, virtualButtonDatas.elasticSpeed * Time.deltaTime);
                    pointerDeltaPosition = sliderValue;
                    for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                    {
                        animationPlayerContainerInteraction[i].SetSliderPercentage(sliderValue);
                        animationPlayerContainerInteraction[i].StartPlayAnimation(sliderValue, virtualButtonDatas.elasticSpeed);
                    }
                    StartCoroutine(PlayDelayAnimationFeedbackDrag(sliderValue));
                }
            }
            else
            {
                if (leftClickDown == false)
                {
                    for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                    {
                        animationPlayerContainerInteraction[i].StopAnimation();
                    }
                    StartCoroutine(StopAnimationFeedback());
                }
            }
        }
        IEnumerator StopAnimationFeedback()
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                animationPlayerContainerFeedback[i].StopAnimation();
            }
        }
        IEnumerator DelayElasticDragBoth()
        {
            if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);


            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                AnimationPlayerManual animManual = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
                animManual.SetElasticBothDrag(virtualButtonDatas.elasticSpeed);
            }
        }

        protected float CheckX(Vector3 deltaPos)
        {
            if (virtualButtonDatas.deltaPositionX > 0)
            {
                if (deltaPos.x > 0)
                    x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x);
                else
                    x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x) * -1;
            }
            else
            {
                if (deltaPos.x < 0)
                    x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x) * -1;
                else
                    x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x);
            }
            return x;
        }

        protected float CheckY(Vector3 deltaPos)
        {
            if (virtualButtonDatas.deltaPositionY > 0)
            {
                if (deltaPos.y > 0)
                {
                    y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y);
                }
                else
                {
                    y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y) * -1;
                }
            }
            else
            {
                if (deltaPos.y < 0)
                    y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y) * -1;
                else
                    y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y);
            }

            return y;
        }

        public override void PlayVirtualButton()
        {
            Debug.Log("play virtual button");
        }

        public override void StopVirtualButton()
        {
            Debug.Log("stop virtual button");
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
namespace VirtualTraining.Feature
{
    public partial class VirtualButtonClick : VirtualButtonBase
    {
        //this is partial class for virtual button click sequential

        bool isBackSequential;

        void PlayVBClickSequential()
        {
            if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter ||
                virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation ||
                virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState)
            {
                if (IsInvert != invertTemp)
                {
                    invertTemp = IsInvert;
                    if (isBackSequential)
                    {
                        if (IsInvert)
                            valueTemp++;
                        else
                        {
                            if (valueTemp > 0)
                                valueTemp--;
                            else
                                isBackSequential = !isBackSequential;
                        }
                    }
                    else
                    {
                        if (IsInvert)
                            valueTemp--;
                        else
                            valueTemp++;
                    }
                }
            }

            PlayAllAnimationPlayer();
        }
        void PlayAllAnimationPlayer()
        {
            switch (virtualButtonDatas.sequencePlayMethod)
            {
                case SequencePlayMethod.TimeStampParameter:
                    SequentialClickTimeStamp();
                    break;
                case SequencePlayMethod.TimeStampScriptedAnimation:
                    SequentialClickTimeStamp();
                    break;
                case SequencePlayMethod.TimeStampState:
                    SequentialClickTimeStamp();
                    break;
                case SequencePlayMethod.PlayParameter:
                    PlaySequentialManual(true);
                    break;
                case SequencePlayMethod.PlayScriptedAnimation:
                    PlaySequentialAnimationScript();
                    break;
                case SequencePlayMethod.PlayState:
                    PlaySequentialManual(false);
                    break;
            }
        }

        void SequentialClickTimeStamp()
        {
            if (IsInvert)
            {
                SequentialInvert();
            }
            else
            {
                if (isBackSequential)
                {
                    if (valueTemp > 0)
                    {
                        valueTemp--;
                        PlayAnimationList(valueTemp);
                    }
                    else
                    {
                        //PlayAnimationList(-1);
                        isBackSequential = false;
                        valueTemp++;
                        PlayAnimationList(valueTemp);
                    }
                }
                else
                {
                    if (valueTemp < virtualButtonDatas.sequentialClickDatas.Count - 1)
                    {
                        valueTemp++;
                        PlayAnimationList(valueTemp);
                    }
                    else
                    {
                        valueTemp = valueTemp - 1;
                        PlayAnimationList(valueTemp);
                        isBackSequential = true;
                    }
                }
            }
        }

        void SequentialInvert()
        {
            if (isBackSequential == false)
            {
                if (valueTemp > 0)
                {
                    valueTemp = valueTemp - 1;
                    PlayAnimationList(valueTemp);
                }
                else
                {
                    //PlayAnimationList(-1);
                    valueTemp++;
                    PlayAnimationList(valueTemp);
                    isBackSequential = true;
                }
            }
            else
            {
                if (valueTemp < virtualButtonDatas.sequentialClickDatas.Count - 1)
                {
                    valueTemp++;
                    PlayAnimationList(valueTemp);
                }
                else
                {
                    valueTemp = valueTemp - 1;
                    PlayAnimationList(valueTemp);
                    isBackSequential = false;
                }
            }
        }
        void PlayAnimationList(int value)
        {
            if (isBackSequential == true && virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop == true)
            {
                if (value <= -1)
                    StartCoroutine(PlayDelayAnimationSequence(value,0));
                else
                {
                    if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferentValue)
                        StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.sequentialClickDatas[value].durationSecond,value));
                    else
                        StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.sequentialClickDatas[value].durations,value));
                }
                StartCoroutine(DelayInteractionAfterAnimationSequence(value));
            }
            else
            {
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    if (value <= -1)
                    {
                        animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.defaultValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                    }
                    else
                        animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.sequentialClickDatas[value].durations, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                }

                if (value <= -1)
                    StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.defaultValue, 0));
                else
                {
                    if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferentValue)
                        StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.sequentialClickDatas[value].durationSecond, value));
                    else
                        StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.sequentialClickDatas[value].durations, value));
                }
            }
        }

        IEnumerator PlayDelayAnimationSequence(float durationValue, int queueValue)
        {
            sliderValue = Mathf.Round(sliderValue * 100) / 100;
            Debug.Log(sliderValue + " Value" + durationValue);
            if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            {
                if (isBackSequential && virtualButtonDatas.isReverseLoop == true)
                {
                    yield return null;
                }
                else
                {
                    if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferentValue)
                    {
                        yield return new WaitUntil(() => sliderValue == virtualButtonDatas.sequentialClickDatas[queueValue].durations);
                    }
                    else
                        yield return new WaitUntil(() => sliderValue == durationValue);
                }
            }
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                animationPlayerContainerFeedback[i].StartPlayAnimation(durationValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
            }
        }
        IEnumerator DelayInteractionAfterAnimationSequence(int value)
        {
            Debug.Log(value);
            if (value == -1)
                yield return new WaitUntil(() => sliderValue == virtualButtonDatas.defaultValue);
            else
                yield return new WaitUntil(() => sliderValue == virtualButtonDatas.sequentialClickDatas[value].durations);

            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (value <= -1)
                {
                    animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.defaultValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                }
                else
                    animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.sequentialClickDatas[value].durations, animationPlayerContainerInteraction[i].getVBOutputData.speed);
            }
        }

        void PlaySequentialAnimationScript()
        {
            if (IsInvert)
            {
                SequentialAnimationScriptInvert();
            }
            else
            {
                if (isBackSequential)
                {
                    if (valueTemp >= 0)
                    {
                        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop)
                        {
                            SequentialReverseLoopAS(valueTemp);
                        }
                        else
                        {
                            animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                            StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 0, valueTemp));
                        }
                        valueTemp--;
                    }
                    else
                    {
                        animationPlayerContainerInteraction[0].StartPlayAnimation(1, animationPlayerContainerInteraction[0].getVBOutputData.speed);
                        StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 1, 0));
                        valueTemp = valueTemp + 2;
                        isBackSequential = false;
                    }
                }
                else
                {
                    if (valueTemp < animationPlayerContainerInteraction.Count)
                    {
                        animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(1, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                        StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 1, valueTemp));
                        valueTemp++;
                    }
                    else
                    {
                        isBackSequential = true;
                        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop)
                        {
                            SequentialReverseLoopAS(valueTemp - 1);
                        }
                        else
                        {
                            animationPlayerContainerInteraction[valueTemp - 1].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp - 1].getVBOutputData.speed);
                            StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp - 1], 0, valueTemp - 1));
                        }
                        valueTemp = valueTemp - 2;
                    }
                }
            }
        }

        void SequentialReverseLoopAS(int value)
        {
            StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[value], 0, value));
            if (animationPlayerContainerFeedback[value] != null)
                StartCoroutine(DelayPlayAnimationInteraction(animationPlayerContainerFeedback[value], 0, value));
            else
                animationPlayerContainerInteraction[value].StartPlayAnimation(0, animationPlayerContainerInteraction[value].getVBOutputData.speed);
        }

        void SequentialAnimationScriptInvert()
        {
            if (isBackSequential == false)
            {
                if (valueTemp >= 0)
                {
                    if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop)
                    {
                        SequentialReverseLoopAS(valueTemp);
                    }
                    else
                    {
                        animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                        StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], valueTemp, 1));
                    }

                    valueTemp--;
                }
                else
                {
                    animationPlayerContainerInteraction[0].StartPlayAnimation(1, animationPlayerContainerInteraction[0].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 1, 0));
                    valueTemp = valueTemp + 2;
                    isBackSequential = true;
                }
            }
            else
            {
                if (valueTemp < animationPlayerContainerInteraction.Count)
                {
                    animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(1, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 1, valueTemp));
                    valueTemp++;
                }
                else
                {
                    isBackSequential = false;
                    if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop)
                    {
                        SequentialReverseLoopAS(valueTemp - 1);
                    }
                    else
                    {
                        animationPlayerContainerInteraction[valueTemp - 1].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp - 1].getVBOutputData.speed);
                        StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp - 1], 1, valueTemp - 1));
                    }
                    valueTemp = valueTemp - 2;
                }
            }
        }

        void PlaySequentialManual(bool isParameter)
        {
            if (IsInvert)
            {
                SequentialManualInvert(isParameter);
            }
            else
            {
                if (isBackSequential)
                {
                    if (valueTemp >= 0)
                    {
                        PlayAnimationManual(valueTemp, 0, isParameter, 1);
                        valueTemp--;
                    }
                    else
                    {
                        isBackSequential = false;
                        PlayAnimationManual(0, 1, isParameter, 0);
                        valueTemp = valueTemp + 2;
                    }
                }
                else
                {
                    if (valueTemp < animationPlayerContainerInteraction.Count)
                    {
                        PlayAnimationManual(valueTemp, 1, isParameter, 0);
                        valueTemp++;
                    }
                    else
                    {
                        isBackSequential = true;
                        PlayAnimationManual(valueTemp - 1, 0, isParameter, 1);
                        valueTemp = valueTemp - 2;
                    }
                }
            }
        }

        void SequentialManualInvert(bool isParameter)
        {
            if (isBackSequential == false)
            {
                if (valueTemp >= 0)
                {
                    PlayAnimationManual(valueTemp, 0, isParameter, 1);
                    valueTemp--;
                }
                else
                {
                    PlayAnimationManual(0, 1, isParameter, 0);
                    valueTemp = valueTemp + 2;
                    isBackSequential = true;
                }
            }
            else
            {
                if (valueTemp < animationPlayerContainerInteraction.Count)
                {
                    PlayAnimationManual(valueTemp, 1, isParameter, 0);
                    valueTemp++;
                }
                else
                {
                    PlayAnimationManual(valueTemp - 1, 0, isParameter, 1);
                    valueTemp = valueTemp - 2;
                    isBackSequential = false;
                }
            }
        }

        void PlayAnimationManual(int valueTemp, int maxValue, bool isParameter, float startSlider = 0)
        {
            //AnimationPlayerManual animManual = parameterSequentialDatasInteraction[valueTemp].animationPlayerBase as AnimationPlayerManual;
            AnimationPlayerManual animManual = animationPlayerContainerInteraction[valueTemp] as AnimationPlayerManual;
            if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation && virtualButtonDatas.isReverseLoop
                && ((isBackSequential == true && !IsInvert) || (isBackSequential == false && IsInvert)))
            {
                StartCoroutine(DelayPlayAnimationManualFeedback(animManual, maxValue, valueTemp, isParameter, startSlider));
                if (animationPlayerContainerFeedback[valueTemp] != null)
                {
                    StartCoroutine(DelaySequentialManual(animationPlayerContainerFeedback[valueTemp], animManual, valueTemp, maxValue, isParameter, startSlider));
                }
                else
                {
                    SetSequentialManual(valueTemp, maxValue, isParameter, animManual, startSlider);
                }
            }
            else
            {
                SetSequentialManual(valueTemp, maxValue, isParameter, animManual, startSlider);
                StartCoroutine(DelayPlayAnimationManualFeedback(animManual, maxValue, valueTemp, isParameter, startSlider));
            }

        }
        void SetSequentialManual(int valueTemp, int maxValue, bool isParameter, AnimationPlayerManual animManual, float startSlider = 0)
        {
            if (isParameter)
            {
                animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
            }
            else
            {
                animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
            }
        }

        IEnumerator DelaySequentialManual(AnimationPlayerBase animBase, AnimationPlayerManual animManual, int valueTemp, int maxValue, bool isParameter, float startSlider = 0)
        {
            if (animBase.getVBOutputData.isAnimationScript)
                yield return new WaitUntil(() => animBase.sliderValue == maxValue);
            else
            {
                AnimationPlayerManual animManuals = animBase as AnimationPlayerManual;
                if (isParameter)
                    yield return new WaitUntil(() => animManuals.getSliderPercentageTemp == maxValue);
                else
                    yield return new WaitUntil(() => animBase.sliderValue == maxValue);
            }
            SetSequentialManual(valueTemp, maxValue, isParameter, animManual, startSlider);
        }
        IEnumerator DelayPlayAnimationInteraction(AnimationPlayerBase animBase, float maxValue, int valueTemp, float startSlider = 0)
        {
            if (animBase.getVBOutputData.isAnimationScript)
                yield return new WaitUntil(() => animBase.sliderValue == maxValue);
            else
            {
                AnimationPlayerManual animManual = animBase as AnimationPlayerManual;
                yield return new WaitUntil(() => animManual.getSliderPercentageTemp == maxValue);
            }

            animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
        }

        IEnumerator DelayPlayAnimationManualFeedback(AnimationPlayerBase animBase, float maxValue, int valueTemp, bool isParameter = false, float startSlider = 0)
        {
            yield return null;
            if (CheckModifierExcept((float)(valueTemp + 1) / (float)animationPlayerContainerInteraction.Count) == true)
                yield break;

            if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            {
                if (virtualButtonDatas.isReverseLoop && ((isBackSequential == true && !IsInvert) || (isBackSequential == false && IsInvert)))
                {
                    Debug.Log("animation works");
                    yield return null;
                }
                else
                {
                    if (animBase.getVBOutputData.isAnimationScript)
                        yield return new WaitUntil(() => animBase.sliderValue == maxValue);
                    else
                    {
                        AnimationPlayerManual animManual = animBase as AnimationPlayerManual;
                        if (isParameter)
                            yield return new WaitUntil(() => animManual.getSliderPercentageTemp == maxValue);
                        else
                            yield return new WaitUntil(() => animBase.sliderValue == maxValue);
                    }
                }

            }
            else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
                yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
            else
                yield return new WaitForSeconds(0);

            if (valueTemp < animationPlayerContainerFeedback.Count)
            {
                if (animationPlayerContainerFeedback[valueTemp].getVBOutputData.isAnimationScript)
                {
                    AnimationPlayerScript animScript = animationPlayerContainerFeedback[valueTemp] as AnimationPlayerScript;
                    animScript.StartPlayAnimation(maxValue, animScript.getVBOutputData.speed);
                }
                else
                {
                    AnimationPlayerManual animManual = animationPlayerContainerFeedback[valueTemp] as AnimationPlayerManual;
                    if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    {
                        animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
                    }
                    else
                    {
                        animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
                    }
                }
            }
        }
    }
}

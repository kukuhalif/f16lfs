﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class ModifierSetActive : ModifierBase
    {
        public float sliderValue;
        public VirtualButtonBase virtualButtonBase;

        private void Start()
        {
            for (int i = 0; i < ModifierData.interactionObjectDatas.Count; i++)
            {
                if (ModifierData.interactionObjectDatas[i].target.gameObject != null)
                {
                    EventManager.TriggerEvent(new RegisterPartObjectEnableOrDisableEvent(ModifierData.interactionObjectDatas[i].target.gameObject, ModifierData.interactionObjectDatas[i].target.gameObject.activeSelf));
                    
                    if (ModifierData.defaultCondition == DefaultCondition.Off)
                        ModifierData.interactionObjectDatas[i].target.gameObject.SetActive(false);
                    else
                        ModifierData.interactionObjectDatas[i].target.gameObject.SetActive(true);
                }
                else
                    Debug.LogError("virtual button set active object ada yang kosong");
            }
            virtualButtonBase = GetComponent<VirtualButtonBase>();
        }

        private void Update()
        {
            if (virtualButtonBase == null)
                return;

            sliderValue = virtualButtonBase.sliderValue;
            sliderValue = Mathf.Round(sliderValue * 10f) / 10f;

            for (int i = 0; i < ModifierData.modifierConditionValues.Count; i++)
            {
                if (sliderValue >= ModifierData.modifierConditionValues[i].minValue && sliderValue <= ModifierData.modifierConditionValues[i].maxValue)
                {
                    for (int k = 0; k < ModifierData.interactionObjectDatas.Count; k++)
                    {
                        if (ModifierData.defaultCondition == DefaultCondition.Off)
                            ModifierData.interactionObjectDatas[k].target.gameObject.SetActive(true);
                        else
                            ModifierData.interactionObjectDatas[k].target.gameObject.SetActive(false);
                    }
                }
                else
                {
                    for (int k = 0; k < ModifierData.interactionObjectDatas.Count; k++)
                    {
                        if (ModifierData.defaultCondition == DefaultCondition.Off)
                            ModifierData.interactionObjectDatas[k].target.gameObject.SetActive(false);
                        else
                            ModifierData.interactionObjectDatas[k].target.gameObject.SetActive(true);
                    }
                }
            }
        }

        public override void DestroyModifier()
        {
            base.DestroyModifier();

            //for (int i = 0; i < ModifierData.interactionObjectDatas.Count; i++)
            //{
            //    if (ModifierData.interactionObjectDatas[i].target.gameObject != null)
            //    {
                    //if (ModifierData.defaultCondition == DefaultCondition.Off)
                    //    ModifierData.interactionObjectDatas[i].gameObject.gameObject.SetActive(false);
                    //else
                    //    ModifierData.interactionObjectDatas[i].gameObject.gameObject.SetActive(true);
            //    }
            //}
        }
    }
}
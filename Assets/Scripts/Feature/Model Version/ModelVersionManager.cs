using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class ModelVersionManager : MonoBehaviour
    {
        MateriTypeModelVersion[] versions;

        // di set bukan materi, karena materi pasti yang di trigger pertama
        // supaya pas di awal tetap ter trigger model versionnya
        MateriElementType currentMateriType = MateriElementType.RemoveInstall;

        List<GameObject> enabledObjects = new List<GameObject>();
        List<GameObject> disabledObjects = new List<GameObject>();

        private void Start()
        {
            if (DatabaseManager.GetModelVersion() == null)
            {
                Destroy(gameObject);
                return;
            }

            versions = DatabaseManager.GetModelVersion().materiTypeModelVersions.ToArray();

            EventManager.AddListener<MateriEvent>(PlayMateriListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void PlayMateriListener(MateriEvent e)
        {
            if (e.materiTreeElement == null)
                ResetObjects();
            else
                PlayMateri(e.materiTreeElement.MateriData.elementType);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ResetObjects();
        }

        private void PlayMateri(MateriElementType materiElementType)
        {
            if (currentMateriType != materiElementType)
            {
                currentMateriType = materiElementType;

                SetObjectVersion(SelectObjectVersion());
            }
        }

        private MateriTypeModelVersion SelectObjectVersion()
        {
            for (int i = 0; i < versions.Length; i++)
            {
                if (versions[i].sceneMode == VirtualTrainingSceneManager.CurrentSceneMode && versions[i].materiType == currentMateriType)
                {
                    return versions[i];
                }
            }

            return null;
        }

        private void ResetObjects()
        {
            // reset enabled objects
            for (int i = 0; i < enabledObjects.Count; i++)
            {
                enabledObjects[i].SetActive(false);
            }
            enabledObjects.Clear();

            // reset disabled objects
            for (int i = 0; i < disabledObjects.Count; i++)
            {
                disabledObjects[i].SetActive(true);
            }
            disabledObjects.Clear();

            Debug.Log("reset model version");
        }

        private void SetObjectVersion(MateriTypeModelVersion version)
        {
            ResetObjects();

            if (version == null)
                return;

            // enable object if object is inactive
            for (int i = 0; i < version.enableObjects.Count; i++)
            {
                if (!version.enableObjects[i].gameObject.activeSelf)
                {
                    version.enableObjects[i].gameObject.SetActive(true);
                    enabledObjects.Add(version.enableObjects[i].gameObject);
                }
            }

            // disable object if object is active self
            for (int i = 0; i < version.disableObjects.Count; i++)
            {
                if (version.disableObjects[i].gameObject.activeSelf)
                {
                    version.disableObjects[i].gameObject.SetActive(false);
                    disabledObjects.Add(version.disableObjects[i].gameObject);
                }
            }

            Debug.Log("set model version " + version.materiType);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class FresnelObjectsEvent : VirtualTrainingEvent
    {
        public List<PartObject> partObjects;

        public FresnelObjectsEvent(List<GameObjectType> targets, Color color, bool recursive = true, bool withBlink = false)
        {
            partObjects = new List<PartObject>();
            for (int i = 0; i < targets.Count; i++)
            {
                PartObject poj = new PartObject();
                poj.action = PartObjectAction.Fresnel;
                poj.useDefaultColor = false;
                poj.customColor = color;
                poj.isRecursive = recursive;
                poj.fresnelWithBlink = withBlink;
                poj.target = targets[i];
                poj.blinkSpeed = 2f;
                partObjects.Add(poj);
            }
        }

        public FresnelObjectsEvent(List<GameObjectType> targets, bool recursive = true, bool withBlink = false)
        {
            partObjects = new List<PartObject>();
            for (int i = 0; i < targets.Count; i++)
            {
                PartObject poj = new PartObject();
                poj.action = PartObjectAction.Fresnel;
                poj.useDefaultColor = true;
                poj.isRecursive = recursive;
                poj.fresnelWithBlink = withBlink;
                poj.target = targets[i];
                poj.blinkSpeed = 2f;
                partObjects.Add(poj);
            }
        }
    }

    public class HelperEvent : VirtualTrainingEvent
    {
        public List<GameObject> callouts;
        public List<GameObject> helpers;

        public HelperEvent(List<GameObject> callouts, List<GameObject> helpers)
        {
            this.callouts = callouts;
            this.helpers = helpers;
        }

        public HelperEvent()
        {
            helpers = null;
            callouts = null;
        }
    }

    public class AnimationSequenceDone : VirtualTrainingEvent
    {

    }

    public class RegisterPartObjectEnableOrDisableEvent : VirtualTrainingEvent
    {
        public GameObject target;
        public bool defaultState;

        public RegisterPartObjectEnableOrDisableEvent(GameObject target, bool defaultState)
        {
            this.target = target;
            this.defaultState = defaultState;
        }
    }

    public class ObjectSelectionEvent : VirtualTrainingEvent
    {
        public int id;

        public ObjectSelectionEvent(int id)
        {
            this.id = id;
        }

        public ObjectSelectionEvent()
        {
            id = 0;
        }
    }

    public class HideObjectEvent : VirtualTrainingEvent
    {
        public bool hide;
        public GameObject gameobject;

        public HideObjectEvent(bool hide, GameObject gameobject)
        {
            this.hide = hide;
            this.gameobject = gameobject;
        }
    }

    public class GetCutawayObjectBoundsEvent : VirtualTrainingEvent
    {
        public Action<Bounds> GetBounds;

        public GetCutawayObjectBoundsEvent(Action<Bounds> getBounds)
        {
            GetBounds = getBounds;
        }
    }

    public class VirtualButtonEvent : VirtualTrainingEvent
    {
        public List<VirtualButtonDataModel> virtualButtonDatas;
        public List<OverrideVariableVirtualButton> overrideData;

        public VirtualButtonEvent(List<VirtualButtonDataModel> virtualButtonDatas, List<OverrideVariableVirtualButton> overrideData = null)
        {
            this.virtualButtonDatas = virtualButtonDatas;
            this.overrideData = overrideData;
        }
    }

    public class VirtualButtonEventInstantiate : VirtualTrainingEvent
    {

    }

    public class VirtualButtonEventDestroy : VirtualTrainingEvent
    {

    }

    public class VirtualButtonDragedEvent : VirtualTrainingEvent
    {
        public bool isDragged;
        public VirtualButtonDragedEvent(bool isClicked = false)
        {
            this.isDragged = isClicked;
        }
    }
}

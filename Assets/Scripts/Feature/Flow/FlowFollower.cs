﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Feature
{
    public class FlowFollower : MonoBehaviour
    {
        [SerializeField] Vector3 startPos;
        [SerializeField] Vector3 endPos;
        [SerializeField] Quaternion startRot;
        [SerializeField] Quaternion endRot;

        float lerpPos;
        float speedMultiplier;
        Func<float> GetScale;
        Func<float> GetSpeedMultiplier;
        Func<FlowLeader.Axis> GetFlipRotationAxis;
        Func<Vector3> GetOffsetPosition;

        public void SetData(Vector3 startPos, Vector3 endPos, Quaternion startRot, Quaternion endRot)
        {
            this.startPos = startPos;
            this.endPos = endPos;
            this.startRot = startRot;
            this.endRot = endRot;
        }

        public void StartPosition(Func<float> getSpeedMultiplier, Func<float> getScale, Func<FlowLeader.Axis> getFlipRotationAxis, Func<Vector3> offsetPosition)
        {
            GetFlipRotationAxis = getFlipRotationAxis;
            GetScale = getScale;
            GetSpeedMultiplier = getSpeedMultiplier;
            GetOffsetPosition = offsetPosition;
            transform.localScale = new Vector3(GetScale(), GetScale(), GetScale());
            speedMultiplier = GetSpeedMultiplier();
            lerpPos = 0f;
            transform.position = startPos;
            transform.rotation = startRot;
        }

        public void Play(float deltaTime)
        {
#if UNITY_EDITOR
            speedMultiplier = GetSpeedMultiplier();
            transform.localScale = new Vector3(GetScale(), GetScale(), GetScale());
#endif

            // forward
            if (speedMultiplier > 0)
            {
                // position
                transform.position = Vector3.Lerp(startPos, endPos, lerpPos);
                
                // rotation
                if (GetFlipRotationAxis() == FlowLeader.Axis.none)
                {
                    transform.rotation = Quaternion.Slerp(startRot, endRot, lerpPos);
                }
                else // flip rotation
                {
                    var rot = Quaternion.Slerp(startRot, endRot, lerpPos);

                    var flipAxis = GetFlipRotationAxis();
                    switch (flipAxis)
                    {
                        case FlowLeader.Axis.x:
                            rot *= Quaternion.Euler(Vector3.right * 180);
                            break;
                        case FlowLeader.Axis.y:
                            rot *= Quaternion.Euler(Vector3.up * 180);
                            break;
                        case FlowLeader.Axis.z:
                            rot *= Quaternion.Euler(Vector3.forward * 180);
                            break;
                    }

                    transform.rotation = rot;
                }

                lerpPos += (deltaTime * speedMultiplier);

                if (lerpPos >= 1f)
                {
                    lerpPos = 0f;
                }
            }
            else if (speedMultiplier < 0) // backward
            {
                // position
                transform.position = Vector3.Lerp(endPos, startPos, lerpPos);

                // rotation
                if (GetFlipRotationAxis() == FlowLeader.Axis.none)
                {
                    transform.rotation = Quaternion.Slerp(endRot, startRot, lerpPos);
                }
                else // flip rotation
                {
                    var rot = Quaternion.Slerp(endRot, startRot, lerpPos);

                    var flipAxis = GetFlipRotationAxis();
                    switch (flipAxis)
                    {
                        case FlowLeader.Axis.x:
                            rot *= Quaternion.Euler(Vector3.right * 180);
                            break;
                        case FlowLeader.Axis.y:
                            rot *= Quaternion.Euler(Vector3.up * 180);
                            break;
                        case FlowLeader.Axis.z:
                            rot *= Quaternion.Euler(Vector3.forward * 180);
                            break;
                    }

                    transform.rotation = rot;
                }

                lerpPos += (deltaTime * -speedMultiplier);

                if (lerpPos >= 1f)
                {
                    lerpPos = 0f;
                }
            }

            // offset position
            transform.position += GetOffsetPosition();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VirtualTraining.Feature
{
    public class FlowManager : MonoBehaviour
    {
        [NonSerialized] bool editMode;
        [SerializeField] bool flowLeaderOnlyWithMeshRenderer;
        [SerializeField] List<FlowLeader> flowLeaders = new List<FlowLeader>();

        Animator animator;
        int flowsCount, flowsIndex;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            editMode = false;
            flowsCount = flowLeaders.Count;
        }

        private void OnEnable()
        {
            if (editMode)
                return;

            Play();
        }

        public List<FlowLeader> GetFlowLeaders()
        {
            return flowLeaders;
        }

        public bool IsEditMode
        {
            get => editMode;
            set => editMode = value;
        }

        public void Play()
        {
            editMode = false;
            animator.enabled = false;

            for (flowsIndex = 0; flowsIndex < flowsCount; flowsIndex++)
            {
                flowLeaders[flowsIndex].StartPosition();
            }
        }

        public void EditMode()
        {
            editMode = true;
            animator.enabled = true;
            animator.Play("Stop", 0, 0);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (editMode)
                return;
#endif

            for (flowsIndex = 0; flowsIndex < flowsCount; flowsIndex++)
            {
                flowLeaders[flowsIndex].Play(Time.deltaTime);
            }
        }

#if UNITY_EDITOR

        public void AddFlowEntityComponent()
        {
            flowLeaders.Clear();
            AddFlowEntityComponent(gameObject);
            flowsCount = flowLeaders.Count;
        }
        void AddFlowEntityComponent(GameObject current)
        {
            FlowLeader oldFe = current.GetComponent<FlowLeader>();

            if (oldFe == null)
            {
                if (flowLeaderOnlyWithMeshRenderer)
                {
                    MeshRenderer meshRenderer = current.GetComponent<MeshRenderer>();
                    if (meshRenderer != null)
                    {
                        FlowLeader newFlow = current.AddComponent<FlowLeader>();
                        flowLeaders.Add(newFlow);
                    }
                }
                else if (current != gameObject)
                {
                    FlowLeader newFlow = current.AddComponent<FlowLeader>();
                    flowLeaders.Add(newFlow);
                }
            }
            else
                flowLeaders.Add(oldFe);

            for (int i = 0; i < current.transform.childCount; i++)
            {
                AddFlowEntityComponent(current.transform.GetChild(i).gameObject);
            }
        }

        public void RemoveFlowEntityComponent()
        {
            flowLeaders.Clear();
            RemoveFlowEntityComponent(gameObject);
            flowsCount = 0;
        }
        void RemoveFlowEntityComponent(GameObject current)
        {
            FlowLeader flow = current.GetComponent<FlowLeader>();
            DestroyImmediate(flow);

            for (int i = 0; i < current.transform.childCount; i++)
            {
                RemoveFlowEntityComponent(current.transform.GetChild(i).gameObject);
            }
        }

        public void BakeMovement()
        {
            if (animator == null)
            {
                EditorUtility.DisplayDialog("Warning", "Animator null", "ok");
                return;
            }

            animator.Play("Play", 0, 0);

            StartCoroutine(Baking());
        }

        IEnumerator Baking()
        {
            yield return null;

            for (int i = 0; i < flowLeaders.Count; i++)
            {
                flowLeaders[i].Bake();
            }

            yield return null;

            while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            {
                yield return null;
            }

            for (int i = 0; i < flowLeaders.Count; i++)
            {
                flowLeaders[i].StopRecording();
            }

            Debug.Log("finish");
        }

#endif
    }
}
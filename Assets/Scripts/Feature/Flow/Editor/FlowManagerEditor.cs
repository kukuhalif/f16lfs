﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Feature
{
    [CustomEditor(typeof(FlowManager))]
    [CanEditMultipleObjects]
    public class FlowManagerEditor : Editor
    {
        float speedMultiplier = 1f;
        float recordPositionRate = 0.05f;
        float followerScale = 1f;
        GameObject followerTemplate = null;
        Vector3 offsetPosition = new Vector3();
        int monitorCameraIndex;
        int flowSoloIndex;

        private void OnEnable()
        {
            flowSoloIndex = -1;
        }

        private void OnDisable()
        {
            flowSoloIndex = -1;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            FlowManager manager = target as FlowManager;

            if (Application.isPlaying)
            {
                if (GUILayout.Button(manager.IsEditMode ? "Play" : "Edit Mode"))
                {
                    manager.IsEditMode = !manager.IsEditMode;
                    if (manager.IsEditMode)
                        manager.EditMode();
                    else
                        manager.Play();
                }

                if (manager.IsEditMode)
                {
                    if (GUILayout.Button("Add FlowLeader Component"))
                    {
                        if (EditorUtility.DisplayDialog("Add flow component", "Add flow to mesh renderer / pointer object ?", "yes", "no"))
                            manager.AddFlowEntityComponent();
                    }

                    if (GUILayout.Button("Remove All FlowLeader Component"))
                    {
                        if (EditorUtility.DisplayDialog("Remove flow component", "Remove all flow leader component ?", "yes", "no"))
                            manager.RemoveFlowEntityComponent();
                    }

                    if (GUILayout.Button("Bake Movement"))
                    {
                        if (EditorUtility.DisplayDialog("Bake", "Bake movement ?", "yes", "no"))
                            manager.BakeMovement();
                    }
                }
            }

            GUILayout.Space(10f);

            GUILayout.BeginVertical("Box");
            EditorGUILayout.LabelField("Set value for all flow leaders section");

            speedMultiplier = EditorGUILayout.FloatField("Speed Multiplier", speedMultiplier);
            recordPositionRate = EditorGUILayout.Slider("Record Position Rate", recordPositionRate, 0.05f, 0.5f);
            followerScale = EditorGUILayout.FloatField("Follower Scale", followerScale);
            followerTemplate = EditorGUILayout.ObjectField("Follower Template", followerTemplate, typeof(GameObject), true) as GameObject;
            offsetPosition = EditorGUILayout.Vector3Field("Offset Position", offsetPosition);
            monitorCameraIndex = EditorGUILayout.IntField("Monitor Camera Index", monitorCameraIndex);

            if (GUILayout.Button("set value to all flow leaders"))
            {
                if (EditorUtility.DisplayDialog("flow editor", "are you sure to set record position rate, follower scale and follower template to all flow leaders ?", "yes", "no"))
                {
                    List<FlowLeader> flowLeaders = manager.GetFlowLeaders();

                    for (int i = 0; i < flowLeaders.Count; i++)
                    {
                        flowLeaders[i].SpeedMultiplier = speedMultiplier;
                        flowLeaders[i].RecordPositionRate = recordPositionRate;
                        flowLeaders[i].FollowerScale = followerScale;
                        flowLeaders[i].FollowerTemplate = followerTemplate;
                        flowLeaders[i].OffsetPosition = offsetPosition;
                        flowLeaders[i].MonitorCameraIndex = monitorCameraIndex;
                    }
                }
            }

            GUILayout.EndVertical();

            var leaders = manager.GetFlowLeaders();

            GUILayout.Space(10);

            GUILayout.BeginHorizontal();

            GUILayout.Label("Flow Leader Components");

            if (GUILayout.Button("show all flow"))
            {
                flowSoloIndex = -1;

                for (int l = 0; l < leaders.Count; l++)
                {
                    leaders[l].FollowerParent.gameObject.SetActive(true);
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            for (int i = 0; i < leaders.Count; i++)
            {
                if (flowSoloIndex == i)
                    GUI.color = Color.cyan;

                GUILayout.BeginVertical("Box");
                GUILayout.BeginHorizontal();
                GUILayout.Label("no " + (i + 1).ToString() + " , " + leaders[i].gameObject.name);

                if (flowSoloIndex != i)
                {
                    if (GUILayout.Button("solo this"))
                    {
                        flowSoloIndex = i;

                        for (int l = 0; l < leaders.Count; l++)
                        {
                            leaders[l].FollowerParent.gameObject.SetActive(flowSoloIndex == l);
                        }
                    }
                }
                else
                {
                    if (GUILayout.Button("un solo this"))
                    {
                        flowSoloIndex = -1;

                        for (int l = 0; l < leaders.Count; l++)
                        {
                            leaders[l].FollowerParent.gameObject.SetActive(true);
                        }
                    }
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                leaders[i].SpeedMultiplier = EditorGUILayout.FloatField("Speed Multiplier", leaders[i].SpeedMultiplier);
                leaders[i].RecordPositionRate = EditorGUILayout.Slider("Record Position Rate", leaders[i].RecordPositionRate, 0.05f, 0.5f);
                leaders[i].FollowerScale = EditorGUILayout.FloatField("Follower Scale", leaders[i].FollowerScale);
                leaders[i].FlipAxis = (FlowLeader.Axis)EditorGUILayout.EnumPopup("Flip Axis", leaders[i].FlipAxis);
                leaders[i].OffsetPosition = EditorGUILayout.Vector3Field("Offset Position", leaders[i].OffsetPosition);
                leaders[i].FollowerTemplate = (GameObject)EditorGUILayout.ObjectField("Follower Template", leaders[i].FollowerTemplate, typeof(GameObject), true);
                leaders[i].MonitorCameraIndex = EditorGUILayout.IntField("Monitor Camera Index", leaders[i].MonitorCameraIndex);
                GUILayout.EndVertical();

                GUI.color = Color.white;
            }
        }
    }
}

#endif
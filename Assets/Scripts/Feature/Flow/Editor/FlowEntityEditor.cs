﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Feature
{
    [CustomEditor(typeof(FlowLeader))]
    [CanEditMultipleObjects]
    public class FlowEntityEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
}

#endif
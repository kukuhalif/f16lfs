﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class FlowLeader : MonoBehaviour
    {
        public enum Axis
        {
            none, x, y, z
        }

        [Range(0.05f, 0.5f)]
        [SerializeField] float recordPositionRate = 0.05f;
        [SerializeField] float speedMultiplier = 1f;
        [SerializeField] float followerScale = 1f;
        [SerializeField] Axis flipAxis = Axis.none;
        [SerializeField] Vector3 offsetPosition;
        [SerializeField] Transform followerParent;
        [SerializeField] GameObject followerTemplate;
        [SerializeField] int monitorCameraIndex;

        List<Vector3> positions = new List<Vector3>();
        List<Quaternion> rotations = new List<Quaternion>();

        int currentFollower;
        int followerCount;

        [SerializeField] List<FlowFollower> followers = new List<FlowFollower>();

        [HideInInspector] MeshRenderer currentRenderer;

        public float RecordPositionRate { get => recordPositionRate; set => recordPositionRate = value; }
        public float FollowerScale { get => followerScale; set => followerScale = value; }
        public GameObject FollowerTemplate { get => followerTemplate; set => followerTemplate = value; }
        public Vector3 OffsetPosition { get => offsetPosition; set => offsetPosition = value; }
        public Axis FlipAxis { get => flipAxis; set => flipAxis = value; }
        public Transform FollowerParent { get => followerParent;  }
        public float SpeedMultiplier { get => speedMultiplier; set => speedMultiplier = value; }
        public int MonitorCameraIndex { get => monitorCameraIndex; set => monitorCameraIndex = value; }

        private void Awake()
        {
            currentRenderer = GetComponent<MeshRenderer>();
        }

        private void Start()
        {
            EventManager.AddListener<PlayMonitorCameraEvent>(OnOpenMonitorCamera);

            if (followerParent == null)
            {
                GameObject fp = new GameObject();
                followerParent = fp.transform;
                followerParent.SetParent(transform.parent);
                followerParent.position = new Vector3();
                followerParent.gameObject.name = "follower parent -> " + gameObject.name;
            }
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<PlayMonitorCameraEvent>(OnOpenMonitorCamera);
        }

        void OnOpenMonitorCamera(PlayMonitorCameraEvent e)
        {
            if (monitorCameraIndex <= 0)
                return;

            followerParent.gameObject.SetActive(monitorCameraIndex == e.index + 1);
        }

        public void Bake()
        {
            if (currentRenderer != null)
                currentRenderer.enabled = true;
            positions.Clear();
            rotations.Clear();
            while (followers.Count > 0)
            {
                DestroyImmediate(followers[followers.Count - 1].gameObject);
                followers.RemoveAt(followers.Count - 1);
            }
            InvokeRepeating("RecordAndCreateFollower", 0, recordPositionRate);
        }

        void RecordAndCreateFollower()
        {
            positions.Add(transform.position);
            rotations.Add(transform.rotation);

            GameObject newObj = Instantiate(followerTemplate);

            FlowFollower newFollower = newObj.GetComponent<FlowFollower>();
            if (newFollower == null)
                newFollower = newObj.AddComponent<FlowFollower>();

            newFollower.transform.position = transform.position;
            newFollower.transform.rotation = transform.rotation;
            newFollower.transform.localScale = new Vector3(followerScale, followerScale, followerScale);
            newFollower.transform.parent = followerParent;
            newFollower.gameObject.name = positions.Count.ToString();
            followers.Add(newFollower);
        }

        public void StopRecording()
        {
            CancelInvoke("RecordAndCreateFollower");
            RemoveEndPosition();
            //RemoveTooClosePosition();
            RemoveInvalidFollower();
            SetFollowersData();
        }

        void RemoveEndPosition()
        {
            int tailPosition = positions.Count;
            for (int i = 1; i < positions.Count; i++)
            {
                if (positions[i - 1] == positions[i])
                {
                    tailPosition = i;
                    break;
                }
            }

            positions.RemoveRange(tailPosition, positions.Count - tailPosition);
            rotations.RemoveRange(tailPosition, positions.Count - tailPosition);
        }

        void RemoveTooClosePosition()
        {
            float averageDistance = 0f;
            float totalDistance = 0;

            for (int i = 0; i < positions.Count - 1; i++)
            {
                totalDistance += Vector3.Distance(positions[i], positions[i + 1]);
            }

            averageDistance = totalDistance / positions.Count;

            for (int i = positions.Count - 1; i > 0; i--)
            {
                if (Vector3.Distance(positions[i], positions[i - 1]) < averageDistance / 2)
                {
                    Debug.Log(gameObject.name + " remove index " + i);
                    positions.RemoveAt(i);
                    rotations.RemoveAt(i);
                }
            }
        }

        void RemoveInvalidFollower()
        {
            List<FlowFollower> removed = new List<FlowFollower>();
            for (int i = 0; i < followers.Count; i++)
            {
                if (i >= positions.Count - 1)
                {
                    removed.Add(followers[i]);
                }
            }

            for (int i = 0; i < removed.Count; i++)
            {
                followers.Remove(removed[i]);
                DestroyImmediate(removed[i].gameObject);
            }
        }

        void SetFollowersData()
        {
            for (int i = 0; i < followers.Count; i++)
            {
                followers[i].SetData(positions[i], positions[i + 1], rotations[i], rotations[i + 1]);
            }
        }

        public void StartPosition()
        {
            followerCount = followers.Count;
            for (currentFollower = 0; currentFollower < followerCount; currentFollower++)
            {
                followers[currentFollower].StartPosition(GetSpeedMultiplier, GetFollowerScale, GetFlipAxis, GetOffsetPosition);
            }
            if (currentRenderer != null)
                currentRenderer.enabled = false;
        }

        float GetFollowerScale()
        {
            return followerScale;
        }

        float GetSpeedMultiplier()
        {
            return speedMultiplier;
        }

        Axis GetFlipAxis()
        {
            return flipAxis;
        }

        Vector3 GetOffsetPosition()
        {
            return offsetPosition;
        }

        public void Play(float deltaTime)
        {
            for (currentFollower = 0; currentFollower < followerCount; currentFollower++)
            {
                followers[currentFollower].Play(deltaTime);
            }
        }
    }
}
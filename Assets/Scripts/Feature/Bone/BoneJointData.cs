using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class BoneJointData : MonoBehaviour
    {

#if UNITY_EDITOR

        [System.Serializable]
        public class Line
        {
            public GameObjectType startAnchor;
            public GameObjectType endAnchor;
            public List<GameObjectType> joints;

            public Line()
            {
                startAnchor = new GameObjectType();
                endAnchor = new GameObjectType();
                joints = new List<GameObjectType>();
            }
        }

        [SerializeField] private List<Line> lines = new List<Line>();

        public void SetData(List<Line> lines)
        {
            this.lines = new List<Line>(lines);
        }

        public List<Line> GetData()
        {
            return new List<Line>(lines);
        }

        public void BoneJointConnectionSetupFromModelUpdater()
        {
            for (int l = 0; l < lines.Count; l++)
            {
                // duplicate configurable joint component, because model updater cant copy multiple same multiple component in one object
                for (int j = 0; j < lines[l].joints.Count; j++)
                {
                    ConfigurableJoint cj = lines[l].joints[j].gameObject.GetComponent<ConfigurableJoint>();
                    ConfigurableJoint newCj = lines[l].joints[j].gameObject.AddComponent<ConfigurableJoint>();
                    UnityEditorInternal.ComponentUtility.CopyComponent(cj);
                    UnityEditorInternal.ComponentUtility.PasteComponentValues(newCj);
                }

                // joint connection setup
                Rigidbody startAnchor = lines[l].startAnchor.gameObject.GetComponent<Rigidbody>();
                Rigidbody endAnchor = lines[l].endAnchor.gameObject.GetComponent<Rigidbody>();

                ConfigurableJoint[] cjsStart = lines[l].joints[0].gameObject.GetComponents<ConfigurableJoint>();
                ConfigurableJoint[] cjsEnd = lines[l].joints[lines[l].joints.Count - 1].gameObject.GetComponents<ConfigurableJoint>();

                if (cjsStart.Length > 1 && cjsEnd.Length > 1)
                {
                    cjsStart[0].connectedBody = startAnchor;
                    cjsStart[1].connectedBody = lines[l].joints[1].gameObject.GetComponent<Rigidbody>();

                    cjsEnd[0].connectedBody = lines[l].joints[lines[l].joints.Count - 2].gameObject.GetComponent<Rigidbody>(); ;
                    cjsEnd[1].connectedBody = endAnchor;
                }

                for (int j = 1; j < lines[l].joints.Count - 1; j++)
                {
                    ConfigurableJoint[] cjs = lines[l].joints[j].gameObject.GetComponents<ConfigurableJoint>();

                    Rigidbody rbBefore = lines[l].joints[j - 1].gameObject.GetComponent<Rigidbody>();
                    if (cjs.Length > 0)
                        cjs[0].connectedBody = rbBefore;

                    Rigidbody rbNext = lines[l].joints[j + 1].gameObject.GetComponent<Rigidbody>();
                    if (cjs.Length > 1)
                        cjs[1].connectedBody = rbNext;
                }
            }
        }

#endif
    }
}

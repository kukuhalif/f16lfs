﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class HelperManager : MonoBehaviour
    {
        List<GameObject> helpers = new List<GameObject>();

        private void Start()
        {
            EventManager.AddListener<HelperEvent>(Show);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<HelperEvent>(Show);
        }

        public void Show(HelperEvent e)
        {
            if (e.helpers == null || e.callouts == null)
            {
                Hide();
                return;
            }

            SpawnHelper(e.callouts);
            SpawnHelper(e.helpers);
        }

        private void SpawnHelper(List<GameObject> helpers)
        {
            for (int i = 0; i < helpers.Count; i++)
            {
                if (helpers[i] == null)
                {
                    Debug.LogError("ada callout / helper yang kosong disini, cek di materi editor");
                }
                else
                {

#if UNITY_EDITOR
                    Callout callout = helpers[i].GetComponent<Callout>();
                    if (callout != null)
                        callout.GeneratePrefabPath();
#endif

                    GameObject cp = Instantiate(helpers[i], VirtualTrainingSceneManager.GameObjectRoot.transform);
                    this.helpers.Add(cp);
                }
            }
        }

        public void Hide()
        {
            for (int i = 0; i < helpers.Count; i++)
            {
                Destroy(helpers[i]);
            }
            helpers.Clear();
        }
    }
}
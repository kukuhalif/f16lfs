using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class HelperParenting : MonoBehaviour
    {
        [SerializeField] public GameObjectType parent;

        private void Start()
        {
            EventManager.AddListener<SceneReadyEvent>(IniDoneListener);
        }

        private void IniDoneListener(SceneReadyEvent e)
        {
            if (parent.gameObject != null)
            {
                transform.SetParent(parent.gameObject.transform, true);
            }

            EventManager.RemoveListener<SceneReadyEvent>(IniDoneListener);
            Destroy(this);
        }
    }
}

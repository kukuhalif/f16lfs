using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Feature
{
    public class EditorObjectOnly : MonoBehaviour
    {
        private void Start()
        {
            if (!Debug.isDebugBuild)
                Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.Feature
{
    public class DisplayController : MonoBehaviour
    {
        public TextMeshProUGUI textCount;
        int count = 0;
        public void Counting()
        {
            count++;
            textCount.text = count.ToString();
        }
    }
}

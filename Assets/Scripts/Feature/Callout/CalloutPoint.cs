﻿using UnityEngine;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Feature
{
    public class CalloutPoint : MonoBehaviour
    {
        Transform target;
        Vector3 offset;

        public Transform Target { get => target; set => target = value; }
        public Vector3 Offset { get => offset; set => offset = value; }

#if UNITY_EDITOR
        bool dragged;
#endif

        private void Start()
        {
            CalculateTargetObject();
        }

        public void ResetTarget()
        {
            target = null;
        }

        public void UpdatePosition()
        {
#if UNITY_EDITOR
            for (int i = 0; i < UnityEditor.Selection.gameObjects.Length; i++)
            {
                if (UnityEditor.Selection.gameObjects[i] == gameObject)
                {
                    ResetTarget();
                    dragged = true;
                    return;
                }
            }

            if (dragged)
            {
                CalculateTargetObject();
                dragged = false;
            }
#endif

            if (target == null)
                return;

            transform.position = target.position - offset;
        }

        public void CalculateTargetObject()
        {
            bool attached = false;
            float size = 0.001f;
            while (!attached && size <= 15f)
            {
                Collider[] cols = Physics.OverlapSphere(transform.position, size, VirtualTrainingSceneManager.ModelLayer);

                for (int i = 0; i < cols.Length; i++)
                {
                    target = cols[i].transform;
                    offset = target.position - transform.position;

                    attached = true;
                }

                if (!attached)
                {
                    size += 0.001f;
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;
using UnityEngine.Serialization;

namespace VirtualTraining.Feature
{
    public class Callout : MonoBehaviour
    {
#if UNITY_EDITOR
        [Header("lokasi callout prefab")]
        [SerializeField] private string prefabPath;
        public string PrefabPath { get => prefabPath; }
#endif

        [System.NonSerialized]
        public bool created = false;
        public Transform pivotPoint;

        // -1 = main camera
        // else, second monitor index
        [Header("tampil di monitor ke berapa (no dimulai dari 1)")]
        [FormerlySerializedAs("secondMonitorIndex")] public int monitorCameraIndex = -1;

        // ini angka cantik
        public float lineLengthScale = 10f;
        public float boxScaleConstant = 0.035f;
        public float lineWidthConstant = 2.5f;

        // ini angka cantik
        [HideInInspector]
        [System.NonSerialized]
        public float minScale = 0.0001f;
        [HideInInspector]
        [System.NonSerialized]
        public float maxScale = 0.5f;
        [HideInInspector]
        [System.NonSerialized]
        public float zoomScale = 0.01f;

        [System.NonSerialized]
        public CalloutElement copySource = null;

        [System.NonSerialized]
        CalloutPoint[] points;

        int idx;

        private void Start()
        {
            EventManager.AddListener<PlayMonitorCameraEvent>(OnOpenMonitorCamera);
            EventManager.AddListener<ApplyUIThemesEvent>(ApplyUIThemeListener);
            points = transform.GetAllComponentsInChilds<CalloutPoint>();
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<PlayMonitorCameraEvent>(OnOpenMonitorCamera);
            EventManager.RemoveListener<ApplyUIThemesEvent>(ApplyUIThemeListener);

            for (int i = 0; i < points.Length; i++)
            {
                Destroy(points[i].gameObject);
            }
        }

        private void Update()
        {
            for (idx = 0; idx < points.Length; idx++)
            {
                points[idx].UpdatePosition();
            }
        }

        public void GeneratePrefabPath()
        {
#if UNITY_EDITOR

            prefabPath = UnityEditor.PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(gameObject);
            UnityEditor.EditorUtility.SetDirty(gameObject);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        private void ApplyUIThemeListener(ApplyUIThemesEvent e)
        {
            var theme = DatabaseManager.GetUiTheme();

            Image[] backgrounds = transform.GetAllComponentsInChilds<Image>();
            for (int i = 0; i < backgrounds.Length; i++)
            {
                if (backgrounds[i] != null)
                    backgrounds[i].color = theme.calloutBackgroundColor;
            }

            List<Text> texts = new List<Text>();
            List<TMP_Text> tmTexts = new List<TMP_Text>();

            GetText<Text>(transform, texts);
            GetText<TMP_Text>(transform, tmTexts);

            for (int i = 0; i < texts.Count; i++)
            {
                texts[i].color = theme.calloutTextColor;
            }

            for (int i = 0; i < tmTexts.Count; i++)
            {
                tmTexts[i].color = theme.calloutTextColor;
            }
        }

        void GetText<T>(Transform obj, List<T> textList)
        {
            T[] texts = obj.GetComponents<T>();
            for (int i = 0; i < texts.Length; i++)
            {
                textList.Add(texts[i]);
            }
            for (int i = 0; i < obj.childCount; i++)
            {
                GetText<T>(obj.GetChild(i), textList);
            }
        }

        void OnOpenMonitorCamera(PlayMonitorCameraEvent e)
        {
            if (monitorCameraIndex <= 0)
                return;

            gameObject.SetActive(monitorCameraIndex == e.index + 1);
        }

        public void CreatePivot()
        {
            if (pivotPoint == null && !string.IsNullOrEmpty(gameObject.scene.name))
            {
                GameObject pivot = new GameObject("pivot");
                pivotPoint = pivot.transform;
                pivotPoint.SetParent(transform);
            }
        }

        public void On(bool on)
        {
            gameObject.SetActive(on);
            if (on)
            {
                created = true;
            }
        }

        public void SetMargin(float top, float bottom, float left, float right)
        {
            var txts = transform.GetAllComponentsInChilds<TextMeshProUGUI>();
            foreach (var txt in txts)
            {
                txt.margin = new Vector4(left, top, right, bottom);
            }
        }
    }

}
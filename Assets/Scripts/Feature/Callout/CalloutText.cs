﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.Feature
{
    public class CalloutText : MonoBehaviour
    {

#if UNITY_EDITOR

        [SerializeField] bool dontUseObjectName;
        [HideInInspector] public string content = "Enter text ...";
        List<Text> texts = new List<Text>();
        List<TMP_Text> tmTexts = new List<TMP_Text>();

        private void OnEnable()
        {
            GetTextComponent();
        }

        private void Update()
        {
            SetText();
        }

        public void GetTextComponent()
        {
            texts.Clear();
            tmTexts.Clear();
            GetText<Text>(transform, texts);
            GetText<TMP_Text>(transform, tmTexts);
        }

        public void SetText()
        {
            if (!dontUseObjectName)
                content = gameObject.name;

            if (string.IsNullOrEmpty(content))
                return;

            for (int i = 0; i < texts.Count; i++)
            {
                texts[i].text = content;
            }
            for (int i = 0; i < tmTexts.Count; i++)
            {
                tmTexts[i].text = content;
            }
        }

        void GetText<T>(Transform obj, List<T> textList)
        {
            T[] texts = obj.GetComponents<T>();
            for (int i = 0; i < texts.Length; i++)
            {
                textList.Add(texts[i]);
            }
            for (int i = 0; i < obj.childCount; i++)
            {
                GetText<T>(obj.GetChild(i), textList);
            }
        }
#endif
    }

}
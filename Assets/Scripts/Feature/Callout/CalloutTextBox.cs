﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.Feature
{
    public class CalloutTextBox : MonoBehaviour
    {
        Transform camTransform;
        [SerializeField] Image background;
        [SerializeField] TMP_Text text;

        private void Start()
        {

            EventManager.AddListener<CameraChangedEvent>(ChangeCameraListener);

#if UNITY_EDITOR

            if (background == null)
            {
                var images = transform.GetAllComponentsInChilds<Image>();
                for (int g = 0; g < images.Length; g++)
                {
                    SetBackground(images[g]);
                }
            }
#endif

            background.color = DatabaseManager.GetUiTheme().calloutBackgroundColor;

            if (text == null)
                text = transform.GetComponentInChildsExcludeThis<TMP_Text>();

            text.color = DatabaseManager.GetUiTheme().calloutTextColor;
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<CameraChangedEvent>(ChangeCameraListener);
        }

        void ChangeCameraListener(CameraChangedEvent e)
        {
            GetCamera();
        }

        public void SetBackground(Image background)
        {
            this.background = background;
        }

        private void OnEnable()
        {
            GetCamera();
        }

        private void GetCamera()
        {
            if (gameObject.layer == VirtualTrainingCamera.MonitorCameraLayer)
                camTransform = VirtualTrainingCamera.MonitorCameraTransform;
            else
                camTransform = VirtualTrainingCamera.CameraTransform;
        }

        private void LateUpdate()
        {
            if (camTransform == null)
            {
                GetCamera();
                return;
            }

            if (VirtualTrainingCamera.IsVR)
                transform.LookAt(2 * transform.position - camTransform.position);
            else
                transform.rotation = camTransform.rotation;
        }
    }
}
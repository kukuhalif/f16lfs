#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;

namespace VirtualTraining.Feature
{
    public static class CalloutShortcut
    {
        class DoCreatePrefabAsset : EndNameEditAction
        {
            // Subclass and override this method to create specialised prefab asset creation functions
            protected virtual GameObject CreateGameObject(string name)
            {
                GameObject newGameObject = new GameObject(name);
                newGameObject.AddComponent<Callout>();
                return newGameObject;
            }

            public override void Action(int instanceId, string pathName, string resourceFile)
            {
                GameObject go = CreateGameObject(Path.GetFileNameWithoutExtension(pathName));
                GameObject prefab = PrefabUtility.SaveAsPrefabAsset(go, pathName);
                GameObject.DestroyImmediate(go);

                EditorGUIUtility.PingObject(prefab);
                Selection.activeGameObject = prefab;
            }
        }

        static void CreatePrefabAsset(string name, DoCreatePrefabAsset createAction)
        {
            string directory = GetSelectedAssetDirectory();
            string path = Path.Combine(directory, $"{name}.prefab");
            ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, createAction, path, EditorGUIUtility.FindTexture("Prefab Icon"), null);
        }

        static string GetSelectedAssetDirectory()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (Directory.Exists(path))
                return path;
            else
                return Path.GetDirectoryName(path);
        }

        [MenuItem("Assets/Create/Callout Prefab", isValidateFunction: false, priority: -20)]
        public static void CreatePrefab()
        {
            CreatePrefabAsset("New Callout", ScriptableObject.CreateInstance<DoCreatePrefabAsset>());
        }
    }
}

#endif
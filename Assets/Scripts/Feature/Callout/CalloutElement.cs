﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.Serialization;

namespace VirtualTraining.Feature
{
    [RequireComponent(typeof(CalloutText))]
    public class CalloutElement : MonoBehaviour
    {
        Callout theParent;

        [SerializeField]
        Transform textBox;
        float minBoxScale = 7e-05f;
        float maxBoxScale = 1f;

        [SerializeField]
        Transform helperParent;

        [SerializeField]
        List<GameObject> points = new List<GameObject>();
        [SerializeField]
        [FormerlySerializedAsAttribute("_lines")]
        List<LineRenderer> lines = new List<LineRenderer>();

        public bool inversePivot = false;
        public bool ignoreX, ignoreY, ignoreZ;
        [SerializeField] float ignoreXOffset;
        [SerializeField] float ignoreYOffset;
        [SerializeField] float ignoreZOffset;
        [SerializeField] bool ignoreAll;
        [SerializeField] bool setupIgnoreAll;

        public float lineLengthAdd;

        [HideInInspector]
        [System.NonSerialized]
        public float cameraDistance = 0.005f;

        Vector3 direction;
        [SerializeField] [HideInInspector] Vector3 ignoreAllOffset = new Vector3();

        Transform cameraTransform;

        private void Start()
        {
            GetCamera();

#if UNITY_EDITOR
#else
setupIgnoreAll = false;
#endif
        }

        private void GetCamera()
        {
            if (gameObject.layer == VirtualTrainingCamera.MonitorCameraLayer)
                cameraTransform = VirtualTrainingCamera.MonitorCameraTransform;
            else
                cameraTransform = VirtualTrainingCamera.CameraTransform;
        }

        private void OnEnable()
        {
            if (theParent == null)
            {
                theParent = GetComponentInParent<Callout>();
            }

            if (helperParent == null)
            {
                GameObject helper = new GameObject("TextAndLine");
                helperParent = helper.transform;
                helperParent.SetParent(transform);
            }

            if (textBox == null)
            {
                Object obj = Instantiate(Resources.Load("Callout/CalloutTextBox"));
                textBox = (obj as GameObject).transform;
                textBox.name = "TextBox";
                textBox.SetParent(helperParent);
            }

            //get last position for ignore all position
            if (ignoreAll)
            {
                ignoreAllOffset = textBox.position - GetMeanVector(points);
            }
        }

        private void OnDisable()
        {
            //get last position for ignore all position
            if (ignoreAll)
            {
                ignoreAllOffset = textBox.position - GetMeanVector(points);
            }
        }

        public void Setup(Callout parent)
        {
            theParent = parent;
        }

        public void CreateCalloutPoint()
        {
            GameObject point = new GameObject("Point " + (points.Count + 1).ToString(), typeof(CalloutPoint));
            point.transform.SetParent(transform);
            point.transform.position = new Vector3(0, 0, 0);
            points.Add(point);

            Object obj = Instantiate(Resources.Load("Callout/CalloutLine"));
            GameObject line = obj as GameObject;
            line.name = "line " + points.Count;
            line.transform.SetParent(helperParent);
            lines.Add(line.GetComponent<LineRenderer>());
            SetText();
        }

        void SetText()
        {
#if UNITY_EDITOR
            CalloutText ct = GetComponent<CalloutText>();
            ct.GetTextComponent();
            ct.SetText();
#endif
        }

        public void DeletePointAndLine(int index)
        {
            DestroyImmediate(points[index]);
            DestroyImmediate(lines[index]);

            points.RemoveAt(index);
            lines.RemoveAt(index);

            SetText();
        }

        public GameObject GetPoint(int index)
        {
            return points[index];
        }

        public void SetPoint(int index, Vector3 position)
        {
            points[index].transform.position = position;
        }

        public void SetPoint(GameObject po, int index)
        {
            points[index] = po;
        }

        public LineRenderer GetLine(int index)
        {
            return lines[index];
        }

        public void SetLine(LineRenderer li, int index)
        {
            lines[index] = li;
        }

        public int PointCount()
        {
            return points.Count;
        }

        private Vector3 GetMeanVector(List<GameObject> positions)
        {
            if (positions.Count == 0)
                return Vector3.zero;
            float x = 0f;
            float y = 0f;
            float z = 0f;
            foreach (GameObject pos in positions)
            {
                x += pos.transform.position.x;
                y += pos.transform.position.y;
                z += pos.transform.position.z;
            }
            return new Vector3(x / positions.Count, y / positions.Count, z / positions.Count);
        }

        private void LateUpdate()
        {
            // if pivot not setup yet then abort !
            if (theParent.pivotPoint == null)
                return;

            // get camera transform
            if (cameraTransform == null)
                GetCamera();
            else
            {
                // get average points position
                Vector3 averagePoint = GetMeanVector(points);

                // only on play mode
                if (Application.isPlaying)
                {
                    // set size berdasarkan jarak dari kamera game
                    cameraDistance = Vector3.Distance(cameraTransform.position, averagePoint) * theParent.zoomScale;
                    cameraDistance = Mathf.Clamp(cameraDistance, theParent.minScale, theParent.maxScale);
                }

                // set texbox position
                if (points.Count > 0)
                {
                    if (ignoreAll)
                    {
                        // if ignore all x y and z
                        if (setupIgnoreAll)
                            ignoreAllOffset = textBox.position - GetMeanVector(points);
                        else
                            textBox.position = GetMeanVector(points) + ignoreAllOffset;
                    }
                    else
                    {
                        // get direction of text box
                        direction = theParent.pivotPoint.position - averagePoint;

                        if (ignoreX)
                            direction.x = ignoreXOffset;
                        if (ignoreY)
                            direction.y = ignoreYOffset;
                        if (ignoreZ)
                            direction.z = ignoreZOffset;

                        // normalize magnitude (length)
                        direction.Normalize();

                        if (inversePivot)
                            textBox.position = averagePoint + direction * ((theParent.lineLengthScale + lineLengthAdd) * cameraDistance);
                        else
                            textBox.position = averagePoint - direction * ((theParent.lineLengthScale + lineLengthAdd) * cameraDistance);
                    }
                }

                float lineWidth = cameraDistance * theParent.boxScaleConstant * theParent.lineWidthConstant;

                for (int i = 0; i < lines.Count; i++)
                {
                    if (points[i] != null && lines[i] != null)
                        lines[i].SetPosition(0, points[i].transform.position);

                    if (lines[i] != null)
                    {
                        lines[i].SetPosition(1, textBox.position);
                        lines[i].startWidth = lineWidth;
                        lines[i].endWidth = lineWidth;
                    }
                }

                float boxScale = cameraDistance * theParent.boxScaleConstant;
                boxScale = Mathf.Clamp(boxScale, minBoxScale, maxBoxScale);
                textBox.localScale = new Vector3(boxScale, boxScale, boxScale);
            }
        }
    }
}
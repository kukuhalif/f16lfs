using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(RectTransform))]
    public abstract class RoundedCorner : UIElement
    {
        // todo : sementara buat mobile

        bool disableRoundedCorner = true;

        private bool roundedEnabled = false;

        private Material material;
        private Mask mask;
        private Image image;
        protected Graphic graphic;

        protected abstract string MaterialPath { get; }

        protected override void Awake()
        {
            base.Awake();

            Validate();

            if (AllowRefresh())
                Refresh();
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (AllowRefresh())
                Refresh();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (material != null)
                Destroy(material);
        }

        private void OnRectTransformDimensionsChange()
        {
            if (enabled && AllowRefresh())
                Refresh();
        }

        public virtual void Validate()
        {
            if (disableRoundedCorner)
                return;

            if (image == null)
                image = GetComponent<Image>();

            if (material == null && image != null)
            {
                image.material = Instantiate(Resources.Load(MaterialPath) as Material);
                material = image.material;
                image.sprite = null;
            }

            if (graphic == null)
                graphic = GetComponent<Graphic>();

            if (mask == null)
                mask = GetComponent<Mask>();

            if (mask == null)
                mask = gameObject.AddComponent<Mask>();
        }

        protected bool AllowRefresh()
        {
            if (disableRoundedCorner)
                return false;

            if (graphic == null || material == null || !roundedEnabled)
                return false;

            return true;
        }

        public virtual void Refresh()
        {
            if (mask != null)
                mask.MaskEnabled();
        }

        protected override void ApplyTheme()
        {
            if (AllowRefresh())
                Refresh();
        }

        public void EnableRoundedCorner()
        {
            if (disableRoundedCorner)
                return;

            roundedEnabled = true;

            if (image != null)
                image.material = material;
        }

        public void DisableRoundedCorner()
        {
            if (disableRoundedCorner)
                return;

            roundedEnabled = false;

            if (image != null)
                image.material = null;
        }

        public void SetDisableRoundedCorner()
        {
            disableRoundedCorner = true;
        }
    }
}

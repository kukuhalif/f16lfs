using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI
{
    public class RoundedCornerImage : RoundedCorner
    {
        private static readonly int Props = Shader.PropertyToID("_WidthHeightRadius");

        protected override string MaterialPath => "Rounded Image";

        public override void Refresh()
        {
            var rect = ((RectTransform)transform).rect;
            // pake graphic.materialForRendering supaya pake material yg dipake untuk rendering, karena component mask, mungkin nge duplikat material sehingga ngga valid lagi
            graphic.materialForRendering.SetVector(Props, new Vector4(rect.width, rect.height, theme.roundedPanelRadius, 0));

            base.Refresh();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    public class CornerImageGradient : BaseMeshEffect
    {
        [SerializeField] Color topLeftColor = Color.white;
        [SerializeField] Color topRightColor = Color.white;
        [SerializeField] Color bottomRightColor = Color.white;
        [SerializeField] Color bottomLeftColor = Color.white;

        public override void ModifyMesh(VertexHelper vh)
        {
            if (enabled)
            {
                Rect rect = graphic.rectTransform.rect;
                UIGradientUtils.Matrix2x3 localPositionMatrix = UIGradientUtils.LocalPositionMatrix(rect, Vector2.right);

                UIVertex vertex = default(UIVertex);
                for (int i = 0; i < vh.currentVertCount; i++)
                {
                    vh.PopulateUIVertex(ref vertex, i);
                    Vector2 normalizedPosition = localPositionMatrix * vertex.position;
                    vertex.color *= UIGradientUtils.Bilerp(bottomLeftColor, bottomRightColor, topLeftColor, topRightColor, normalizedPosition);
                    vh.SetUIVertex(vertex, i);
                }
            }
        }
    }
}

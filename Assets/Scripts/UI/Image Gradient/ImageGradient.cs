using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    public class ImageGradient : BaseMeshEffect
    {
        [SerializeField] Color color1 = Color.white;
        [SerializeField] Color color2 = Color.white;
        [Range(-180f, 180f)] [SerializeField] float angle = 0f;
        [SerializeField] bool ignoreRatio = true;

        public override void ModifyMesh(VertexHelper vh)
        {
            if (enabled)
            {
                Rect rect = graphic.rectTransform.rect;
                Vector2 dir = UIGradientUtils.RotationDir(angle);

                if (!ignoreRatio)
                    dir = UIGradientUtils.CompensateAspectRatio(rect, dir);

                UIGradientUtils.Matrix2x3 localPositionMatrix = UIGradientUtils.LocalPositionMatrix(rect, dir);

                UIVertex vertex = default(UIVertex);
                for (int i = 0; i < vh.currentVertCount; i++)
                {
                    vh.PopulateUIVertex(ref vertex, i);
                    Vector2 localPosition = localPositionMatrix * vertex.position;
                    vertex.color *= Color.Lerp(color2, color1, localPosition.y);
                    vh.SetUIVertex(vertex, i);
                }
            }
        }
    }
}

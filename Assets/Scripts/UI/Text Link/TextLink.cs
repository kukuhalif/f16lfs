﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TextLink : TEXLink
    {
        [SerializeField] MateriElementType contentType;

        object contentData;
        bool isBlocked = false;
        GameObject player;
        GameObject cameraVR;
        Vector2 positionVRCamera;
        private void Start()
        {
            onClick.AddListener(RunLink);
            EventManager.AddListener<ApplyUIThemesEvent>(ApplyThemeListener);
            VirtualTrainingInputSystem.RaycastedUIUpdated += RaycastedUIChangedListener;
            Reload();
            isVR = VirtualTrainingCamera.IsVR;
            //player = VirtualTrainingCamera.CurrentCamera.gameObject;
            //positionVRCamera = new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);
            //if (player != null)
            //{
            //    cameraVR = player.transform.Find("HandInputModuleDispay").gameObject;
            //    triggerCamera = cameraVR.GetComponent<Camera>();
            //}
            if (isVR && VirtualTrainingCamera.handVR != null)
            {
                positionVRCamera = new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);
                cameraVR = VirtualTrainingCamera.handVR.gameObject;
                if (cameraVR != null)
                {
                    triggerCamera = cameraVR.GetComponent<Camera>();
                }
            }
        }

        private void OnDestroy()
        {
            onClick.RemoveListener(RunLink);
            EventManager.RemoveListener<ApplyUIThemesEvent>(ApplyThemeListener);
            VirtualTrainingInputSystem.RaycastedUIUpdated -= RaycastedUIChangedListener;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            TEXDraw td = GetComponent<TEXDraw>();
            td.raycastTarget = true;
        }

        public void SetContentData(object contentData)
        {
            this.contentData = contentData;
        }

        public void SetColor(Color color)
        {
            if (target == null)
                target = GetComponent<TEXDraw>();

            Target.color = color;
        }

        protected override void UpdateInput()
        {
            base.UpdateInput();

            if (isVR)
            {
                if (cameraVR != null)
                {
                    input_HoverPos = positionVRCamera;

                    if (VirtualTrainingCamera.IsClickVR)
                    {
                        Debug.Log("is click vr");
                        input_PressPos.Add(positionVRCamera);
                    }
                }
            }
        }

        public void ParseLink(string rawLink, ref string code, ref string content)
        {
            // link template
            // \link[pdf_nama pdf file]{link untuk pdf}
            // \link[schematic_schematic id]{link untuk skematik}
            // \link[figure_figure name]{link untuk figure}
            // \link[camerafigure_figure name/camera name]{link untuk kamera figur}
            // \link[camerapreset_camera preset name]{link untuk kamera preset}

            string[] splits = rawLink.Split('_');
            if (splits.Length > 0)
            {
                code = splits[0].ToLower();
                content = rawLink.Replace(code + "_", "");
            }
        }

        protected override bool IsLinkBlocked()
        {
            VirtualTrainingInputSystem.UpdateRaycastedUI();
            Debug.Log("is blocked " + isBlocked);
            return isBlocked;
        }

        private void RaycastedUIChangedListener(GameObject raycastedUI)
        {
            isBlocked = true;

            if (raycastedUI == null)
                return;

            var parents = raycastedUI.transform.GetAllParents();
            foreach (var parent in parents)
            {
                if (parent == transform)
                {
                    isBlocked = false;
                    return;
                }
            }
        }

        private void ApplyThemeListener(ApplyUIThemesEvent e)
        {
            Reload();
        }

        private void Reload()
        {
            UITheme theme = DatabaseManager.GetUiTheme();
            normal = theme.TextLinkNormalColor;
            hover = theme.TextLinkHoverColor;
            pressed = theme.TextLinkPressedColor;
            after = theme.TextLinkAfterColor;
        }

        protected override void DefaultCursor()
        {
            base.DefaultCursor();
            VirtualTrainingCursor.DefaultCursor();
        }

        protected override void LinkCursor()
        {
            if (isBlocked)
                return;

            base.LinkCursor();
            VirtualTrainingCursor.Link();
        }

        private void RunLink(string txt)
        {
            if (isBlocked)
                return;

            string code = "", content = "";
            ParseLink(txt, ref code, ref content);

            switch (code)
            {
                case "pdf":

                    EventManager.TriggerEvent(new ShowPdfLinkEvent(content));

                    break;

                case "schematic":

                    EventManager.TriggerEvent(new ShowSchematicLinkEvent(content));

                    break;

                case "figure":

                    EventManager.TriggerEvent(new PlayFigureLinkEvent(content, contentType, contentData));

                    break;

                case "camerafigure":

                    if (content.Contains("/"))
                    {
                        var names = content.Split('/');
                        if (names.Length >= 2)
                            EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(names[0], names[1], contentType, contentData));
                        else
                            Debug.LogWarning("camera figure link format is incorrect : " + content);
                    }
                    else
                        Debug.LogWarning("camera figure link format is incorrect : " + content);

                    break;

                case "camerapreset":

                    EventManager.TriggerEvent(new CameraPresetEvent(content));

                    break;
            }

            Debug.Log(txt);
        }
    }
}

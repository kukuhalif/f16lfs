using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class VideoController : MonoBehaviour
    {
        [SerializeField] VideoPlayer videoPlayer;
        [SerializeField] InteractionButton playPauseButton;
        [SerializeField] InteractionButton beginButton;
        [SerializeField] InteractionButton lastButton;
        [SerializeField] Slider slider;

        UIInteraction sliderHandleInteraction;
        bool IS_PLAYING, clickDown, isLoop;

        public bool isPlaying { get => IS_PLAYING; }

        private void Start()
        {
            playPauseButton.OnClickEvent += PlayPauseListener;
            if (beginButton != null)
                beginButton.OnClickEvent += Begin;
            if (lastButton != null)
                lastButton.OnClickEvent += Last;

            sliderHandleInteraction = slider.gameObject.AddComponent<UIInteraction>();
            sliderHandleInteraction.OnClickDown += OnClickDownHandle;
            sliderHandleInteraction.OnClickUp += OnClickUpHandle;

            videoPlayer.loopPointReached += OnFinishVideo;

            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            playPauseButton.OnClickEvent -= PlayPauseListener;
            if (beginButton != null)
                beginButton.OnClickEvent -= Begin;
            if (lastButton != null)
                lastButton.OnClickEvent -= Last;
            videoPlayer.loopPointReached -= OnFinishVideo;

            if (sliderHandleInteraction != null)
            {
                sliderHandleInteraction.OnClickDown -= OnClickDownHandle;
                sliderHandleInteraction.OnClickUp -= OnClickUpHandle;
            }

            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        void OnFinishVideo(UnityEngine.Video.VideoPlayer vp)
        {
            if (isLoop == false)
            {
                IS_PLAYING = false;
                playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
            }
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            videoPlayer.SetDirectAudioVolume(0, e.data.voiceVolume);
        }

        private void OnClickDownHandle()
        {
            clickDown = true;
            IS_PLAYING = videoPlayer.isPlaying;
            videoPlayer.Pause();
        }

        private void OnClickUpHandle()
        {
            clickDown = false;
            if (IS_PLAYING)
            {
                videoPlayer.Play();
            }
        }

        private void Update()
        {
            if (clickDown)
                videoPlayer.frame = (long)(slider.value * videoPlayer.frameCount);
            else
                slider.value = (float)videoPlayer.frame / (float)videoPlayer.frameCount;
        }

        private void PlayPauseListener()
        {
            if (videoPlayer.isPlaying)
            {
                Pause();
            }
            else
            {
                EventManager.TriggerEvent(new VoiceSoundEvent(this.gameObject));
                playPauseButton.SetIcon(DatabaseManager.GetUITexture().pause);
                videoPlayer.Play();
                IS_PLAYING = true;
            }
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void SetRenderTexture(RenderTexture rt)
        {
            videoPlayer.targetTexture = rt;
        }

        public void Play(VideoClip clip, bool looping)
        {
            isLoop = looping;
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().pause);
            videoPlayer.clip = clip;
            videoPlayer.isLooping = looping;
            videoPlayer.frame = 0;
            videoPlayer.Prepare();

            StartCoroutine(WaitVideoReady());
        }

        public void Stop()
        {
            videoPlayer.Stop();
        }

        public void Pause()
        {
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
            videoPlayer.Pause();
            IS_PLAYING = false;
        }

        public void ReleaseTexture()
        {
            videoPlayer.targetTexture.Release();
        }

        public void Begin()
        {
            videoPlayer.frame = 0;
            videoPlayer.Pause();
            slider.value = 0;
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
        }

        public void Last()
        {
            videoPlayer.frame = (long)videoPlayer.frameCount;
            videoPlayer.Pause();
            slider.value = 1;
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
        }

        private IEnumerator WaitVideoReady()
        {
            while (!videoPlayer.isPrepared)
                yield return null;

            videoPlayer.Play();
            IS_PLAYING = true;
            EventManager.TriggerEvent(new VoiceSoundEvent(this.gameObject));
        }
    }
}

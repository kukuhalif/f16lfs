using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.UI
{
    public abstract class TreeViewUI : MonoBehaviour
    {
        [SerializeField] Transform elementParent;
        [SerializeField] int indentation;
        [SerializeField] ScrollRect scrollRect;
        [SerializeField] LayoutGroup verticalLayout;
        [SerializeField] TMP_InputField searchInput;
        [SerializeField] bool alwaysScrollFirstElement;
        [SerializeField] CanvasGroup contentCanvasGroup;

        const float REFRESH_UI_DURATION = 0.8f;

        TreeViewUIElement elementTemplate;
        TreeViewUIElement selectedElement;
        bool updateUI;
        bool ensureVisibility;
        float lastVerticalPos = 1f;

        List<TreeViewUIElement> uiElements = new List<TreeViewUIElement>();
        Dictionary<int, TreeViewUIElement> uiElementLookup = new Dictionary<int, TreeViewUIElement>();

        protected List<TreeViewUIElement> UiElements { get => uiElements; }
        public Dictionary<int, TreeViewUIElement> UiElementLookup { get => uiElementLookup; }

        delegate void Search(string keyword);
        event Search SearchEvent;

        protected abstract TreeViewUIElement GetElementTemplate();
        protected abstract void ClickedElementCallback(TreeElement element);

        protected virtual void Start()
        {
            elementTemplate = GetElementTemplate();
            searchInput.onValueChanged.AddListener(SearchElement);
        }

        protected void GenerateElements(TreeElement root)
        {
            ClearElements();

            if (root.hasChildren)
                for (int i = 0; i < root.children.Count; i++)
                {
                    GenerateUI(root.children[i], true);
                }
        }

        public void ClearElements()
        {
            if (uiElements.Count == 0)
                return;

            for (int i = 0; i < uiElements.Count; i++)
            {
                SearchEvent -= uiElements[i].SearchCallback;
                Destroy(uiElements[i].gameObject);
            }

            uiElements.Clear();
            uiElementLookup.Clear();
            selectedElement = null;
        }

        protected virtual void OnEnable()
        {
            scrollRect.verticalScrollbar.value = lastVerticalPos;

            if (selectedElement == null || alwaysScrollFirstElement)
            {
                if (uiElements.Count > 0)
                    selectedElement = uiElements[0];
            }

            contentCanvasGroup.DOFade(1f, 1f);

            StartCoroutine(EnsureVisibility(REFRESH_UI_DURATION));
        }

        protected virtual void OnDisable()
        {
            contentCanvasGroup.alpha = 0f;
            lastVerticalPos = scrollRect.verticalScrollbar.value;
        }

        private void OnDestroy()
        {
            searchInput.onValueChanged.RemoveListener(SearchElement);

            for (int i = 0; i < uiElements.Count; i++)
            {
                SearchEvent += uiElements[i].SearchCallback;
            }
        }

        protected virtual void SetupElementCallback(TreeViewUIElement uiElement, TreeElement element)
        {
            uiElement.SetText(element.name);
        }

        private void GenerateUI(TreeElement element, bool show)
        {
            // ui element setup
            TreeViewUIElement newElement = Instantiate(elementTemplate);
            newElement.transform.SetParent(elementParent);
            newElement.transform.localScale = Vector3.one;
            newElement.transform.position = Vector3.zero;
            newElement.Setup(element, ToggleCallback, ClickedCallback, indentation, show);

            // set search event listener
            SearchEvent += newElement.SearchCallback;

            // custom ui element callback
            SetupElementCallback(newElement, element);

            // set ui element list
            uiElements.Add(newElement);

            // set ui element lookup
            uiElementLookup.Add(element.id, newElement);

            if (element.hasChildren)
            {
                for (int i = 0; i < element.children.Count; i++)
                {
                    GenerateUI(element.children[i], false);
                }
            }
        }

        private void SearchElement(string keyword)
        {
            keyword = keyword.ToLower();
            SearchEvent.Invoke(keyword);
        }

        private void ToggleCallback(TreeElement element, bool expand)
        {
            for (int i = 0; i < element.children.Count; i++)
            {
                if (expand)
                    EnableElement(element.children[i]);
                else
                {
                    DisableElement(element.children[i]);
                    updateUI = true;
                    StartCoroutine(StopRefreshUI());
                }
            }
        }

        private void Update()
        {
            // hack untuk refresh ui ketika collapsing tree element, biar nge scale down nya smooth
            // anehnya kalo pas expanding ngga ada masalah ckck
            if (updateUI)
                verticalLayout.SetLayoutVertical();

            if (ensureVisibility && selectedElement != null)
                scrollRect.EnsureVerticalVisibility(selectedElement.RectTransform);
        }

        private IEnumerator StopRefreshUI()
        {
            yield return new WaitForSeconds(REFRESH_UI_DURATION);
            updateUI = false;
        }

        private void EnableElement(TreeElement element)
        {
            TreeViewUIElement ui = uiElementLookup[element.id];

            ui.Show(true);

            if (element.hasChildren && ui.IsExpanded)
            {
                for (int i = 0; i < element.children.Count; i++)
                {
                    EnableElement(element.children[i]);
                }
            }
        }

        private void DisableElement(TreeElement element)
        {
            TreeViewUIElement ui = uiElementLookup[element.id];

            ui.Show(false);

            if (element.hasChildren)
            {
                for (int i = 0; i < element.children.Count; i++)
                {
                    DisableElement(element.children[i]);
                }
            }
        }

        private void ClickedCallback(TreeElement element)
        {
            if (element.isFolder)
                return;

            if (selectedElement != null)
                selectedElement.Deselect();

            selectedElement = uiElementLookup[element.id];
            selectedElement.Select();

            ClickedElementCallback(element);
        }

        private IEnumerator EnsureVisibility(float duration)
        {
            scrollRect.movementType = ScrollRect.MovementType.Clamped;
            ensureVisibility = true;
            yield return new WaitForSeconds(duration);
            ensureVisibility = false;
            yield return null;
            scrollRect.movementType = ScrollRect.MovementType.Elastic;
        }

        protected void ClearSelected()
        {
            if (selectedElement != null)
                selectedElement.Deselect();
        }

        public void FocusAndClickElementId(int treeElementId)
        {
            foreach (var uiElement in UiElements)
            {
                if (uiElement.Data.id == treeElementId)
                {
                    FocusAndClickElement(uiElement.Data);
                    break;
                }
            }
        }

        protected void FocusAndClickElement(TreeElement element)
        {
            // expand to element
            List<TreeElement> parents = element.Parents();
            for (int i = 0; i < parents.Count; i++)
            {
                if (uiElementLookup.ContainsKey(parents[i].id))
                {
                    TreeViewUIElement parentUI = uiElementLookup[parents[i].id];
                    if (!parentUI.IsExpanded)
                        parentUI.IsExpanded = true;
                }
            }

            // click element
            ClickedCallback(element);

            // focus to element
            if (gameObject.activeInHierarchy)
                StartCoroutine(EnsureVisibility(REFRESH_UI_DURATION));
        }

        protected void FocusAndClickElement(int index)
        {
            // expand to element
            TreeElement element = uiElements[index].Data;
            List<TreeElement> parents = element.Parents();
            for (int i = 0; i < parents.Count; i++)
            {
                if (uiElementLookup.ContainsKey(parents[i].id))
                {
                    TreeViewUIElement parentUI = uiElementLookup[parents[i].id];
                    if (!parentUI.IsExpanded)
                        parentUI.IsExpanded = true;
                }
            }

            // click element
            ClickedCallback(element);

            // focus to element
            if (gameObject.activeInHierarchy)
                StartCoroutine(EnsureVisibility(REFRESH_UI_DURATION));
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;


namespace VirtualTraining.UI
{
    public class TreeElementButton : InteractionButton
    {
        private bool isFolder;

        public bool IsFolder { set => isFolder = value; }

        protected override ButtonColorTheme ButtonColor => isFolder ? theme.treeElementFolderButton : theme.button;

    }
}
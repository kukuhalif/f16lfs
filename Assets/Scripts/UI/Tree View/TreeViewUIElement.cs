using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.UI
{
    public class TreeViewUIElement : MonoBehaviour
    {
        [SerializeField] TreeElementButton button;
        [SerializeField] UIInteraction toggle;
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] CanvasGroup canvasGroup;

        protected const float EXPAND_DURATION = 0.5f;
        private TreeElement data;
        private RectTransform rectTransform;
        private Action<TreeElement, bool> toggleCallback;
        private Action<TreeElement> clickCallback;
        private bool isExpanded = false;
        private bool isLastStateActive = true;
        private bool isRotating = false;

        public TreeElement Data { get => data; }
        public RectTransform RectTransform { get => rectTransform; }
        public InteractionButton Button { get => button; }

        public bool IsExpanded
        {
            get => isExpanded;
            set
            {
                toggle.transform.DOLocalRotate(new Vector3(0, 0, isExpanded ? 0 : -90), EXPAND_DURATION);
                isExpanded = value;
                toggleCallback.Invoke(data, isExpanded);
                ExpandCallback(isExpanded);
            }
        }

        private void OnEnable()
        {
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;
        }

        public void Setup(TreeElement data, Action<TreeElement, bool> toggleCallback, Action<TreeElement> clickCallback, int indentation, bool show)
        {
            // set variables
            this.data = data;
            this.clickCallback = clickCallback;
            this.toggleCallback = toggleCallback;
            rectTransform = GetComponent<RectTransform>();

            // set toggle
            if (data.hasChildren)
            {
                // element has child
                toggle.OnClickDown += ToggleListener;
            }
            else
            {
                // "disable" toggle
                EnableToggle(false);
            }

            // set padding
            LayoutGroup le = GetComponent<LayoutGroup>();
            le.padding.left = data.Parents().Count * indentation;

            // set button status
            button.IsFolder = data.isFolder;

            // add element listener
            button.OnClickEvent += ClickListener;

            // set alpha to show
            if (show)
            {
                canvasGroup.alpha = 1f;
            }

            // disable at start
            else
            {
                canvasGroup.alpha = 0f;
                transform.localScale = new Vector3(1, 0, 1);
                gameObject.SetActive(false);
                isLastStateActive = false;
            }

            // toggle pointer enter & exit
            toggle.OnCursorEnter += OnTogglePointerEnter;
            toggle.OnCursorExit += OnTogglePointerExit;
        }

        protected virtual void OnDestroy()
        {
            button.OnClickEvent -= ClickListener;

            if (data.hasChildren)
            {
                toggle.OnClickDown -= ToggleListener;
            }

            toggle.OnCursorEnter -= OnTogglePointerEnter;
            toggle.OnCursorExit -= OnTogglePointerExit;
        }

        private void OnTogglePointerEnter()
        {
            button.SetActive(false);
        }

        private void OnTogglePointerExit()
        {
            button.SetActive(true);
        }

        private void ClickListener()
        {
            clickCallback.Invoke(data);

            if (data.hasChildren && !isExpanded)
                ToggleListener();
        }

        private void ToggleListener()
        {
            if (isRotating)
                return;

            isRotating = true;
            toggle.transform.DOLocalRotate(new Vector3(0, 0, isExpanded ? 0 : -90), EXPAND_DURATION).OnComplete(RotateCompleteCallback);
            isExpanded = !isExpanded;
            toggleCallback.Invoke(data, isExpanded);
            AudioPlayer.PlaySFX(isExpanded ? SFX.ExpandTreeElement : SFX.CollapseTreeElement);
            ExpandCallback(isExpanded);
        }

        private void RotateCompleteCallback()
        {
            isRotating = false;
        }

        private void EnableToggle(bool enable)
        {
            Image icon = toggle.GetComponent<Image>();
            if (enable)
            {
                icon.color = Color.white;
                toggle.enabled = true;
            }
            else
            {
                icon.color = Color.clear;
                toggle.enabled = false;
            }
        }

        public void Show(bool show, bool setLastActiveState = true)
        {
            if (show && !gameObject.activeSelf)
            {
                if (setLastActiveState)
                    isLastStateActive = true;
                gameObject.SetActive(true);
                transform.DOScaleY(1f, EXPAND_DURATION).OnComplete(OnShowElement);
                canvasGroup.DOFade(1f, EXPAND_DURATION);
            }
            else if (!show && gameObject.activeSelf)
            {
                if (setLastActiveState)
                    isLastStateActive = false;
                transform.DOScaleY(0f, EXPAND_DURATION);
                canvasGroup.DOFade(0f, EXPAND_DURATION).OnComplete(Disable);
            }
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        public virtual void SetText(string text)
        {
            this.text.text = text;
        }

        public virtual string GetText()
        {
            return text.text;
        }

        protected virtual void ExpandCallback(bool isExpanded)
        {

        }

        protected virtual void OnShowElement()
        {

        }

        public void Select()
        {
            button.ForcePressedColor(true);
        }

        public void Deselect()
        {
            button.ForcePressedColor(false);
        }

        public void SearchCallback(string keyword)
        {
            // reset element
            if (string.IsNullOrEmpty(keyword))
            {
                if (data.hasChildren)
                    EnableToggle(true);

                Show(isLastStateActive, false);

                return;
            }

            // search keyword match
            if (GetText().ToLower().Contains(keyword))
            {
                Show(true, false);
                EnableToggle(false);
            }
            else
            {
                Show(false, false);
            }
        }
    }
}

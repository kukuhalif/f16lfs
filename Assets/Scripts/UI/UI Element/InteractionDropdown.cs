using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(TMP_Dropdown))]
    public class InteractionDropdown : UIElement
    {
        protected override void Start()
        {
            base.Start();

            TMP_Dropdown dropdown = GetComponent<TMP_Dropdown>();
            dropdown.transition = Selectable.Transition.ColorTint;
        }

        private void Update()
        {
            // todo : hacky stuff, karena tmp dropdown sorting ordernya di hardcode sama tmp
            Canvas[] c = transform.GetComponentsInChildren<Canvas>();
            foreach (Canvas c2 in c)
            {
                if (c2.gameObject.activeSelf)
                {
                    c2.overrideSorting = true;
                    c2.sortingOrder = 32700; // max value = 32767
                }
            }

            Canvas canvas = transform.GetComponentsInParentRecursive<Canvas>();
            if (canvas != null)
                for (int i = 0; i < canvas.transform.childCount; i++)
                {
                    if (canvas.transform.GetChild(i).gameObject.name == "Blocker")
                    {
                        Canvas bc = canvas.transform.GetChild(i).gameObject.GetComponent<Canvas>();
                        if (bc != null)
                            bc.sortingOrder = 32650; // dibawah dropdown options
                    }
                }
        }

        protected override void ApplyTheme()
        {
            TMP_Dropdown dropdown = GetComponent<TMP_Dropdown>();

            Image bgImg = GetComponent<Image>();
            bgImg.color = Color.white;

            ColorBlock inputFieldColor = dropdown.colors;

            inputFieldColor.normalColor = theme.dropdownTextNormalColor;
            inputFieldColor.highlightedColor = theme.dropdownTextHighlightColor;
            inputFieldColor.pressedColor = theme.dropdownTextPressedColor;
            inputFieldColor.selectedColor = theme.dropdownTextSelectedColor;
            inputFieldColor.disabledColor = theme.dropdownTextDisabledColor;

            dropdown.colors = inputFieldColor;

            TextMeshProUGUI[] txts = transform.GetAllComponentsInChildsExcludeThis<TextMeshProUGUI>();
            for (int i = 0; i < txts.Length; i++)
            {
                txts[i].color = theme.genericTextColor;
            }

            Toggle[] toggles = transform.GetAllComponentsInChildsExcludeThis<Toggle>();
            for (int i = 0; i < toggles.Length; i++)
            {
                InteractionDropdownElement itg = toggles[i].gameObject.GetComponent<InteractionDropdownElement>();
                if (itg == null)
                    toggles[i].gameObject.AddComponent<InteractionDropdownElement>();
            }

            Scrollbar[] scrollbars = transform.GetAllComponentsInChildsExcludeThis<Scrollbar>();
            for (int i = 0; i < scrollbars.Length; i++)
            {
                InteractionScrollbar isc = scrollbars[i].gameObject.GetComponent<InteractionScrollbar>();
                if (isc == null)
                    scrollbars[i].gameObject.AddComponent<InteractionScrollbar>();
            }

            Image[] imgs = transform.GetAllComponentsInChildsExcludeThis<Image>();
            for (int i = 0; i < imgs.Length; i++)
            {
                if (imgs[i].gameObject.name == "Dropdown List" || imgs[i].gameObject.name == "Template")
                {
                    imgs[i].color = Color.clear;
                    imgs[i].sprite = null;
                }
                else if (imgs[i].gameObject.name == "Item Checkmark")
                {
                    imgs[i].color = Color.white;
                    imgs[i].sprite = DatabaseManager.GetUITexture().checkmark;
                }
                else if (imgs[i].gameObject.name == "Item Background")
                    imgs[i].color = Color.white;
                else if (imgs[i].gameObject.name == "Arrow")
                {
                    imgs[i].color = Color.white;
                    imgs[i].sprite = DatabaseManager.GetUITexture().dropdownArrow;
                }
                else
                    imgs[i].color = theme.panelContentColor;
            }
        }
    }
}

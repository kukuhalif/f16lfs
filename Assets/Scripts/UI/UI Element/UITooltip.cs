using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.UI
{
    public class UITooltip : UIElement
    {
        static UITooltip INSTANCE;
        [SerializeField] float yOffsetPosition;
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] RectTransform background;
        RectTransform rectTransform;

        public static void Show(RectTransform source, string tooltip)
        {
            if (INSTANCE == null)
            {
                GameObject go = (GameObject)Resources.Load("UI/Tooltip");
                GameObject instantiated = Instantiate(go);
                INSTANCE = instantiated.GetComponent<UITooltip>();
                INSTANCE.rectTransform = instantiated.GetComponent<RectTransform>();
            }

            INSTANCE.Setup(source, tooltip);
        }

        public static void Hide()
        {
            if (INSTANCE == null)
                return;

            INSTANCE.gameObject.SetActive(false);
        }

        private void Setup(RectTransform source, string tooltip)
        {
            rectTransform.SetParent(source);
            rectTransform.SetAsLastSibling();

            rectTransform.anchorMin = new Vector2(0, 1);
            rectTransform.anchorMax = new Vector2(1, 1);

            rectTransform.anchoredPosition = new Vector2(0, yOffsetPosition);

            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, 1);
            gameObject.SetActive(true);
            rectTransform.localScale = Vector3.one;

            rectTransform.localEulerAngles = Vector3.zero;

            text.text = tooltip;
        }

        private void Update()
        {
            background.anchoredPosition = text.rectTransform.anchoredPosition;
            background.sizeDelta = text.rectTransform.sizeDelta;
        }

        protected override void ApplyTheme()
        {
            text.color = theme.tooltipTextColor;
            background.GetComponent<Image>().color = theme.tooltipBackgroundColor;
        }
    }
}

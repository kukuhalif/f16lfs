using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class UIElement : MonoBehaviour
    {
        [SerializeField] string tooltip;
        [SerializeField] bool ignoreDynamicUiSize;

        protected UITheme theme
        {
            get => DatabaseManager.GetUiTheme();
        }
        public bool IgnoreDynamicUiSize { get => ignoreDynamicUiSize; }

        protected virtual void Awake()
        {

        }

        protected virtual void Start()
        {
            EventManager.AddListener<ApplyUIThemesEvent>(ApplyThemeListener);
            ApplyTheme();
        }

        protected virtual void OnDestroy()
        {
            EventManager.RemoveListener<ApplyUIThemesEvent>(ApplyThemeListener);
        }

        protected virtual void OnEnable()
        {

        }

        private void ApplyThemeListener(ApplyUIThemesEvent e)
        {
            ApplyTheme();
        }

        protected virtual void ApplyTheme()
        {

        }

        public void ShowTooltip(RectTransform rectTransform)
        {
            if (string.IsNullOrEmpty(tooltip))
                return;

            UITooltip.Show(rectTransform, tooltip);
        }

        public void HideTooltip()
        {
            UITooltip.Hide();
        }

        public void SetTooltip(string tooltip)
        {
            this.tooltip = tooltip;
        }

        /// <summary>
        /// borders -> left right top bottom
        /// </summary>
        protected RectTransform[] GenerateCornerRect(RectTransform panel)
        {
            // left
            GameObject leftResizeObj = new GameObject("left", typeof(RectTransform));
            leftResizeObj.transform.SetParent(panel);
            leftResizeObj.transform.Reset();

            RectTransform leftRect = leftResizeObj.GetComponent<RectTransform>();
            leftRect.pivot = new Vector2(0, 0.5f);
            leftRect.anchorMin = new Vector2(0, 0);
            leftRect.anchorMax = new Vector2(0, 1);
            leftRect.anchoredPosition = Vector2.zero;
            leftRect.sizeDelta = Vector2.zero;

            // right
            GameObject rightResizeObj = new GameObject("right", typeof(RectTransform));
            rightResizeObj.transform.SetParent(panel);
            rightResizeObj.transform.Reset();

            RectTransform rightRect = rightResizeObj.GetComponent<RectTransform>();
            rightRect.pivot = new Vector2(1, 0.5f);
            rightRect.anchorMin = new Vector2(1, 0);
            rightRect.anchorMax = new Vector2(1, 1);
            rightRect.anchoredPosition = Vector2.zero;
            rightRect.sizeDelta = Vector2.zero;

            // top
            GameObject topResizeObj = new GameObject("top", typeof(RectTransform));
            topResizeObj.transform.SetParent(panel);
            topResizeObj.transform.Reset();

            RectTransform topRect = topResizeObj.GetComponent<RectTransform>();
            topRect.pivot = new Vector2(0.5f, 1);
            topRect.anchorMin = new Vector2(0, 1);
            topRect.anchorMax = new Vector2(1, 1);
            topRect.anchoredPosition = Vector2.zero;
            topRect.sizeDelta = Vector2.zero;

            // bottom
            GameObject bottomResizeObj = new GameObject("bottom", typeof(RectTransform));
            bottomResizeObj.transform.SetParent(panel);
            bottomResizeObj.transform.Reset();

            RectTransform bottomRect = bottomResizeObj.GetComponent<RectTransform>();
            bottomRect.pivot = new Vector2(0.5f, 0);
            bottomRect.anchorMin = new Vector2(0, 0);
            bottomRect.anchorMax = new Vector2(1, 0);
            bottomRect.anchoredPosition = Vector2.zero;
            bottomRect.sizeDelta = Vector2.zero;

            RectTransform[] corners = new RectTransform[4];
            corners[0] = leftRect;
            corners[1] = rightRect;
            corners[2] = topRect;
            corners[3] = bottomRect;

            return corners;
        }
    }
}

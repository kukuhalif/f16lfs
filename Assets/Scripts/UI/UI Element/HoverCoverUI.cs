using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    public class HoverCoverUI : HoverCoverBase
    {
        public enum Direction { Bottom, Top, Left, Right, BottomRight, None };

        static HoverCoverUI INSTANCE;

        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] RectTransform underline;
        [SerializeField] RectTransform gradientBottom;
        [SerializeField] RectTransform gradientTop;
        [SerializeField] RectTransform gradientLeft;
        [SerializeField] RectTransform gradientRight;
        [SerializeField] RectTransform gradientBottomRight;
        [SerializeField] float idleValue;
        [SerializeField] float pressedValue;
        [SerializeField] float idleSpeed;
        [SerializeField] float pressedSpeed;
        [SerializeField] float alphaSpeed;
        [SerializeField] float underlineSpeed;

        RectTransform source;
        Direction direction;
        Vector2 startPos = new Vector2(), endPos = new Vector2();
        float currentPos, currentSpeed;
        float currentAlpha;

        bool useUnderline;
        float currentUnderlineScale;

        public static void Show(RectTransform source, Direction direction, bool useUnderline)
        {
            if (direction == Direction.None && !useUnderline)
            {
                Hide();
                return;
            }

            if (INSTANCE == null)
            {
                INSTANCE = LoadPrefab<HoverCoverUI>("UI/Hover Cover UI");
                Hide();
            }

            INSTANCE.source = source;
            INSTANCE.direction = direction;
            INSTANCE.useUnderline = useUnderline;
            INSTANCE.Setup(source);
            INSTANCE.OnPointerEnter();
        }

        public static void Hide()
        {
            if (INSTANCE == null)
                return;

            INSTANCE.underline.gameObject.SetActive(false);
            INSTANCE.gradientBottom.gameObject.SetActive(false);
            INSTANCE.gradientTop.gameObject.SetActive(false);
            INSTANCE.gradientLeft.gameObject.SetActive(false);
            INSTANCE.gradientRight.gameObject.SetActive(false);
            INSTANCE.gradientBottomRight.gameObject.SetActive(false);
            INSTANCE.gameObject.SetActive(false);
            INSTANCE.currentUnderlineScale = 0f;
            INSTANCE.canvasGroup.alpha = 0f;
            INSTANCE.currentAlpha = 0f;
        }

        public static void PointerDown()
        {
            if (INSTANCE == null)
                return;

            INSTANCE.OnPointerDown();
        }

        public static void PointerUp()
        {
            if (INSTANCE == null)
                return;

            INSTANCE.OnPointerUp();
        }

        protected override void ApplyTheme()
        {
            var image = underline.GetComponent<Image>();
            image.sprite = theme.hoverCoverTheme.underlineSprite;
            image.color = theme.hoverCoverTheme.underlineColor;

            image = gradientBottom.GetComponent<Image>();
            image.sprite = theme.hoverCoverTheme.bottom;
            image.color = theme.hoverCoverTheme.color;

            image = gradientLeft.GetComponent<Image>();
            image.sprite = theme.hoverCoverTheme.left;
            image.color = theme.hoverCoverTheme.color;

            image = gradientRight.GetComponent<Image>();
            image.sprite = theme.hoverCoverTheme.right;
            image.color = theme.hoverCoverTheme.color;

            image = gradientTop.GetComponent<Image>();
            image.sprite = theme.hoverCoverTheme.top;
            image.color = theme.hoverCoverTheme.color;

            image = gradientBottomRight.GetComponent<Image>();
            image.sprite = theme.hoverCoverTheme.bottomRight;
            image.color = theme.hoverCoverTheme.color;
        }

        private void Update()
        {
            if (useUnderline && currentUnderlineScale < 1f)
            {
                float xScale = Mathf.Lerp(0f, 1f, currentUnderlineScale);
                underline.localScale = new Vector3(xScale, 1, 1);

                currentUnderlineScale += Time.deltaTime * underlineSpeed;

                if (currentUnderlineScale >= 1f)
                    currentUnderlineScale = 1f;
            }

            if (direction != Direction.None)
            {
                switch (direction)
                {
                    case Direction.Bottom:

                        gradientBottom.SetTop(Mathf.Lerp(startPos.x, endPos.x, currentPos));

                        break;
                    case Direction.Top:

                        gradientTop.SetBottom(Mathf.Lerp(startPos.x, endPos.x, currentPos));

                        break;
                    case Direction.Left:

                        gradientLeft.SetRight(Mathf.Lerp(startPos.x, endPos.x, currentPos));

                        break;
                    case Direction.Right:

                        gradientRight.SetLeft(Mathf.Lerp(startPos.x, endPos.x, currentPos));

                        break;
                    case Direction.BottomRight:

                        gradientBottomRight.SetTop(Mathf.Lerp(startPos.y, endPos.y, currentPos));
                        gradientBottomRight.SetLeft(Mathf.Lerp(startPos.x, endPos.x, currentPos));

                        break;
                }

                currentPos += Time.deltaTime * currentSpeed;

                if (currentPos >= 1f)
                    currentPos = 1f;
            }

            if (currentAlpha < 1f)
            {
                canvasGroup.alpha = Mathf.Lerp(0f, 1f, currentAlpha);

                currentAlpha += Time.deltaTime * alphaSpeed;

                if (currentAlpha >= 1f)
                    currentAlpha = 1f;
            }
        }

        public override void OnPointerDown()
        {
            currentSpeed = pressedSpeed;

            switch (direction)
            {
                case Direction.Bottom:

                    gradientBottom.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = pressedValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.Top:

                    gradientTop.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = pressedValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.Left:

                    gradientLeft.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = pressedValue * source.sizeDelta.x;
                    currentPos = 0f;

                    break;
                case Direction.Right:

                    gradientRight.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = pressedValue * source.sizeDelta.x;
                    currentPos = 0f;

                    break;
                case Direction.BottomRight:

                    gradientBottomRight.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    startPos.y = endPos.y;
                    endPos.x = pressedValue * source.sizeDelta.x;
                    endPos.y = pressedValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.None:
                    break;
            }
        }

        public override void OnPointerUp()
        {
            switch (direction)
            {
                case Direction.Bottom:

                    gradientBottom.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = idleValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.Top:

                    gradientTop.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = idleValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.Left:

                    gradientLeft.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = idleValue * source.sizeDelta.x;
                    currentPos = 0f;

                    break;
                case Direction.Right:

                    gradientRight.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    endPos.x = idleValue * source.sizeDelta.x;
                    currentPos = 0f;

                    break;
                case Direction.BottomRight:

                    gradientBottomRight.gameObject.SetActive(true);
                    startPos.x = endPos.x;
                    startPos.y = endPos.y;
                    endPos.x = idleValue * source.sizeDelta.x;
                    endPos.y = idleValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.None:
                    break;
            }
        }

        public override void OnPointerEnter()
        {
            currentSpeed = idleSpeed;

            gradientBottom.gameObject.SetActive(false);
            gradientTop.gameObject.SetActive(false);
            gradientLeft.gameObject.SetActive(false);
            gradientRight.gameObject.SetActive(false);
            gradientBottomRight.gameObject.SetActive(false);

            underline.gameObject.SetActive(useUnderline);
            if (useUnderline)
                currentUnderlineScale = 0f;

            switch (direction)
            {
                case Direction.Bottom:

                    gradientBottom.gameObject.SetActive(true);
                    startPos.x = source.sizeDelta.y;
                    endPos.x = idleValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.Top:

                    gradientTop.gameObject.SetActive(true);
                    startPos.x = source.sizeDelta.y;
                    endPos.x = idleValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.Left:

                    gradientLeft.gameObject.SetActive(true);
                    startPos.x = source.sizeDelta.x;
                    endPos.x = idleValue * source.sizeDelta.x;
                    currentPos = 0f;

                    break;
                case Direction.Right:

                    gradientRight.gameObject.SetActive(true);
                    startPos.x = source.sizeDelta.x;
                    endPos.x = idleValue * source.sizeDelta.x;
                    currentPos = 0f;

                    break;
                case Direction.BottomRight:

                    gradientBottomRight.gameObject.SetActive(true);
                    startPos.x = source.sizeDelta.x;
                    startPos.y = source.sizeDelta.y;
                    endPos.x = idleValue * source.sizeDelta.x;
                    endPos.y = idleValue * source.sizeDelta.y;
                    currentPos = 0f;

                    break;
                case Direction.None:
                    break;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Scrollbar))]
    public class InteractionScrollbar : UIElement
    {
        protected override void Start()
        {
            base.Start();

            Scrollbar scrollbar = GetComponent<Scrollbar>();
            scrollbar.transition = Selectable.Transition.ColorTint;
        }

        protected override void ApplyTheme()
        {
            Scrollbar scrollbar = GetComponent<Scrollbar>();

            ColorBlock sliderColor = scrollbar.colors;

            sliderColor.normalColor = theme.scrollbarHandleDefaultColor;
            sliderColor.highlightedColor = theme.scrollbarHandleHighlightedColor;
            sliderColor.pressedColor = theme.scrollbarHandlePressedColor;
            sliderColor.selectedColor = theme.scrollbarHandleSelectedColor;
            sliderColor.disabledColor = theme.scrollbarHandleDisabledColor;

            scrollbar.colors = sliderColor;

            Image bg = scrollbar.GetComponent<Image>();
            bg.color = theme.scrollbarBgColor;
        }
    }
}

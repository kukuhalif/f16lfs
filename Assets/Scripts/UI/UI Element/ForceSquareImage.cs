using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ForceSquareImage : MonoBehaviour
{
    [SerializeField] float size;

    private void Start()
    {
        LayoutElement le = GetComponent<LayoutElement>();
        le.minWidth = le.minHeight = le.preferredHeight = le.preferredWidth = size;

        RectTransform rt = GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(size, size);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
namespace VirtualTraining.UI
{
    public class UISpawner : MonoBehaviour
    {
        List<CanvasScaler> canvasScalers = new List<CanvasScaler>();
        List<CanvasScaler> ignoreScaleCanvasScalers = new List<CanvasScaler>();

        bool first = true;
        SceneMode lastMode;

        private void Start()
        {
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.AddListener<SceneReadyEvent>(SwitchSceneListener);

            var startMode = DatabaseManager.GetStartingSceneConfig();
            lastMode = startMode.mode;
            SpawnUI(startMode);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<SceneReadyEvent>(SwitchSceneListener);
        }

        private void SwitchSceneListener(SceneReadyEvent e)
        {
            if (!first)
            {
                if (e.sceneConfig.mode == lastMode)
                    return;
            }

            StartCoroutine(SpawnUIDelay(e.sceneConfig));
        }

        IEnumerator SpawnUIDelay(SceneConfig mode)
        {
            yield return null;

            first = false;
            lastMode = mode.mode;

            Debug.Log("respawn ui");

            canvasScalers.Clear();
            ignoreScaleCanvasScalers.Clear();

            yield return null;

            // destroy previous ui
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }

            yield return null;

            SpawnUI(mode);
        }

        private void SpawnUI(SceneConfig mode)
        {
            first = false;

            foreach (var uiPrefab in mode.persistentUiPrefabs)
            {
                GameObject instantiatedUI = (GameObject)Instantiate(uiPrefab);
                instantiatedUI.transform.SetParent(transform);
                RegisterUI(instantiatedUI);
            }

            SetCanvasResolution(SettingUtility.LastResolution);
        }

        private void RegisterUI(GameObject obj)
        {
            UIElement[] uiElements = obj.transform.GetAllComponentsInChilds<UIElement>();

            for (int e = 0; e < uiElements.Length; e++)
            {
                if (uiElements[e] != null)
                {
                    if (uiElements[e].IgnoreDynamicUiSize)
                    {
                        var ue = uiElements[e].transform.GetComponentsInParentRecursive<CanvasScaler>();
                        if (ue != null && !ignoreScaleCanvasScalers.Contains(ue))
                            ignoreScaleCanvasScalers.Add(ue);
                    }
                    else
                    {
                        var cs = uiElements[e].transform.GetComponentsInParentRecursive<CanvasScaler>();
                        if (cs != null && !canvasScalers.Contains(cs))
                            canvasScalers.Add(cs);
                    }
                }
            }
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            SetCanvasResolution(e.resolution);
        }

        private void SetCanvasResolution(Vector2 resolution)
        {
            Debug.Log("set canvas scaler");
            for (int i = 0; i < canvasScalers.Count; i++)
            {
                if (SettingUtility.LastSetting.dynamicUiSize)
                    canvasScalers[i].referenceResolution = resolution;
                else
                    canvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
            }

            for (int i = 0; i < ignoreScaleCanvasScalers.Count; i++)
            {
                ignoreScaleCanvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
            }
        }
    }
}

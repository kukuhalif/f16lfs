using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public abstract class UIElementAnimation : RoundedCornerImage
    {
        [SerializeField] private bool ignoreAnimation;
        [SerializeField] protected HoverCoverUI.Direction hoverCoverDirection;
        [SerializeField] protected bool useUnderline;
        [SerializeField] List<Transform> animatedTransforms = new List<Transform>();

        List<RectTransform> animatedRectTransforms = new List<RectTransform>();
        List<Vector2> originalPivots = new List<Vector2>();
        protected RectTransform rectTransform;
        Vector2 lastCursorPosition;

        const float ANIMATION_DURATION = 0.15f;
        const float HOVER_IN_SCALE = 1.08f;
        const float PRESSED_SCALE = 0.9f;
        const float MIN_DISTANCE = 0.01f;

        protected override void Start()
        {
            rectTransform = GetComponent<RectTransform>();

            base.Start();

            if (ignoreAnimation)
                return;

            if (animatedTransforms.Count == 0)
                animatedTransforms = GetAnimatedObject();

            foreach (var at in animatedTransforms)
            {
                RectTransform rt = at.GetComponent<RectTransform>();
                animatedRectTransforms.Add(rt);
                originalPivots.Add(rt.pivot);
            }
        }

        protected abstract List<Transform> GetAnimatedObject();

        private void SetDefault()
        {
            for (int i = 0; i < animatedTransforms.Count; i++)
            {
                animatedTransforms[i].localScale = Vector3.one;
                animatedRectTransforms[i].SetPivot(originalPivots[i]);
            }
        }

        protected void HoverIn()
        {
            if (ignoreAnimation)
                return;

            lastCursorPosition = VirtualTrainingInputSystem.PointerPosition;

            for (int i = 0; i < animatedTransforms.Count; i++)
            {
                animatedRectTransforms[i].SetPivot(new Vector2(0.5f, 0.5f));
                animatedTransforms[i].DOScale(HOVER_IN_SCALE, ANIMATION_DURATION);
            }

            StartCoroutine(CheckCursor());
        }

        IEnumerator CheckCursor()
        {
            yield return new WaitForSeconds(1f);
            if (!rectTransform.RectContains(VirtualTrainingInputSystem.PointerPosition))
            {
                // force hover out
                for (int i = 0; i < animatedTransforms.Count; i++)
                {
                    animatedRectTransforms[i].SetPivot(new Vector2(0.5f, 0.5f));
                    animatedTransforms[i].DOScale(1f, ANIMATION_DURATION).OnComplete(SetDefault);
                }
            }
        }

        protected void HoverOut()
        {
            if (ignoreAnimation)
                return;

            if (lastCursorPosition == Vector2.zero || Vector2.Distance(lastCursorPosition, VirtualTrainingInputSystem.PointerPosition) < MIN_DISTANCE)
                return;

            for (int i = 0; i < animatedTransforms.Count; i++)
            {
                animatedRectTransforms[i].SetPivot(new Vector2(0.5f, 0.5f));
                if (gameObject.activeInHierarchy)
                    animatedTransforms[i].DOScale(1f, ANIMATION_DURATION).OnComplete(SetDefault);
                else
                    SetDefault();
            }
        }

        protected void PointerDown()
        {
            if (ignoreAnimation)
                return;

            for (int i = 0; i < animatedTransforms.Count; i++)
            {
                animatedRectTransforms[i].SetPivot(new Vector2(0.5f, 0.5f));
                animatedTransforms[i].DOScale(PRESSED_SCALE, ANIMATION_DURATION);
            }
        }

        protected void PointerUp()
        {
            if (ignoreAnimation)
                return;

            for (int i = 0; i < animatedTransforms.Count; i++)
            {
                animatedRectTransforms[i].SetPivot(new Vector2(0.5f, 0.5f));
                animatedTransforms[i].DOScale(HOVER_IN_SCALE, ANIMATION_DURATION);
            }
        }
    }
}

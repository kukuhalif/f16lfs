using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    public class InteractionDropdownElement : UIElement
    {
        protected override void Start()
        {
            base.Start();

            Toggle toggle = GetComponent<Toggle>();
            toggle.transition = Selectable.Transition.ColorTint;
        }

        protected override void ApplyTheme()
        {
            Toggle toggle = GetComponent<Toggle>();

            ColorBlock inputFieldColor = toggle.colors;

            inputFieldColor.normalColor = theme.dropdownTextNormalColor;
            inputFieldColor.highlightedColor = theme.dropdownTextHighlightColor;
            inputFieldColor.pressedColor = theme.dropdownTextPressedColor;
            inputFieldColor.selectedColor = theme.dropdownTextSelectedColor;
            inputFieldColor.disabledColor = theme.dropdownTextDisabledColor;

            toggle.colors = inputFieldColor;

            TextMeshProUGUI[] txts = transform.GetAllComponentsInChildsExcludeThis<TextMeshProUGUI>();
            for (int i = 0; i < txts.Length; i++)
            {
                txts[i].color = theme.genericTextColor;
            }
        }
    }
}

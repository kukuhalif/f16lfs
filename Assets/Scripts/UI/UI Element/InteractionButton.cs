using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Image))]
    [DisallowMultipleComponent]
    public class InteractionButton : UIElementInteraction
    {
        #region state & cache

        enum State { Normal, Hover, Pressed }
        State currentState;

        [SerializeField] bool ignoreIcon = false;

        Image background;
        Image icon;
        TextMeshProUGUI tmpro;
        TEXDraw texDraw;

        bool forcePressedColor;

        #endregion

        #region virtual

        protected virtual ButtonColorTheme ButtonColor => theme.button;

        #endregion

        #region initialization

        protected override void Awake()
        {
            currentState = State.Normal;

            base.Awake();

            background = GetComponent<Image>();

            if (!ignoreIcon)
                icon = transform.GetComponentInChildsExcludeThis<Image>();

            tmpro = transform.GetComponentInChildsExcludeThis<TextMeshProUGUI>();
            texDraw = transform.GetComponentInChildsExcludeThis<TEXDraw>();
        }

        protected override void Start()
        {
            base.Start();

            if (icon != null)
                icon.raycastTarget = false;

            if (tmpro != null)
                tmpro.raycastTarget = false;
        }

        protected override List<Transform> GetAnimatedObject()
        {
            if (texDraw != null)
                return new List<Transform>();

            if (icon != null)
            {
                if (tmpro != null)
                    return new List<Transform>() { icon.transform, tmpro.transform };
                else
                    return new List<Transform>() { icon.transform };
            }

            if (tmpro != null)
                return new List<Transform>() { tmpro.transform };

            return new List<Transform>() { transform };
        }

        #endregion

        #region callback

        protected override void OnPointerClick()
        {

        }

        protected override void OnPointerEnter()
        {
            SetColor(State.Hover);
            HoverIn();
            HoverCoverUI.Show(rectTransform, hoverCoverDirection, useUnderline);
            StartHoverImageSequence();
            ShowTooltip(rectTransform);
        }

        protected override void OnPointerExit()
        {
            SetColor(State.Normal);
            HoverOut();
            HoverCoverUI.Hide();
            StopHoverImageSequence();
            HideTooltip();
        }

        protected override void OnPointerDown()
        {
            if (!InputActive)
                return;

            SetColor(State.Pressed);
            PointerDown();
            HoverCoverUI.PointerDown();
        }

        protected override void OnPointerUp()
        {
            SetColor(State.Hover);
            PointerUp();
            HoverCoverUI.PointerUp();
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            SetColor(currentState);
        }

        protected override Image GetIconImage()
        {
            return icon;
        }

        #endregion

        #region private function

        private void SetColor(State nextState)
        {
            if (forcePressedColor)
                nextState = State.Pressed;

            currentState = nextState;

            switch (nextState)
            {
                case State.Normal:

                    background.color = ButtonColor.normal.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ButtonColor.normal.contentColor;
                    if (tmpro != null)
                        tmpro.color = ButtonColor.normal.contentColor;
                    if (texDraw != null)
                        texDraw.color = ButtonColor.normal.contentColor;

                    break;
                case State.Hover:

                    background.color = ButtonColor.hover.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ButtonColor.hover.contentColor;
                    if (tmpro != null)
                        tmpro.color = ButtonColor.hover.contentColor;
                    if (texDraw != null)
                        texDraw.color = ButtonColor.hover.contentColor;

                    break;
                case State.Pressed:

                    background.color = ButtonColor.pressed.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ButtonColor.pressed.contentColor;
                    if (tmpro != null)
                        tmpro.color = ButtonColor.pressed.contentColor;
                    if (texDraw != null)
                        texDraw.color = ButtonColor.pressed.contentColor;

                    break;
            }
        }

        #endregion

        #region public function

        public void SetText(string text)
        {
            if (tmpro == null)
                tmpro = transform.GetComponentInChildsExcludeThis<TextMeshProUGUI>();

            if (texDraw == null)
                texDraw = transform.GetComponentInChildsExcludeThis<TEXDraw>();

            if (tmpro != null)
                tmpro.text = text;

            if (texDraw != null)
                texDraw.text = text;
        }

        public void SetIcon(Sprite sprite)
        {
            if (icon == null)
                icon = transform.GetComponentInChildsExcludeThis<Image>();

            if (icon != null)
                icon.sprite = sprite;
        }

        public void SetActive(bool active)
        {
            SetInternalActiveState(active);
            SetColor(active ? State.Hover : State.Normal);
        }

        public Sprite GetBackground()
        {
            if (background == null)
                background = transform.GetComponent<Image>();

            if (background != null)
                return background.sprite;

            return null;
        }

        public void SetBackground(Sprite sprite)
        {
            if (background == null)
                background = transform.GetComponent<Image>();

            if (background != null)
                background.sprite = sprite;
        }

        public void ForcePressedColor(bool on)
        {
            if (on)
            {
                SetColor(State.Pressed);
                forcePressedColor = true;
            }
            else
            {
                forcePressedColor = false;
                SetColor(State.Normal);
            }
        }

        #endregion
    }
}


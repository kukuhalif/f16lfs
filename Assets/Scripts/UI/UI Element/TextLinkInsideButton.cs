using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI
{
    public class TextLinkInsideButton : MonoBehaviour
    {
        [SerializeField] InteractionButton button;
        [SerializeField] TEXLink link;

        private void Start()
        {
            link.HoveringCallback += HoveringCallback;
        }

        private void OnDestroy()
        {
            link.HoveringCallback -= HoveringCallback;
        }

        private void HoveringCallback(bool hovering)
        {
            Debug.Log("hovering " + hovering);
            button.SetInternalActiveState(!hovering);
        }
    }
}

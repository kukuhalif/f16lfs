using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI
{
    public class InteractionTab : MonoBehaviour
    {
        public delegate void OnChangeTab(int selected);
        public event OnChangeTab OnChangeTabEvent;

        [SerializeField] InteractionToggle[] toggles;

        private void Start()
        {
            InteractionToggleGroup toggleGroup = gameObject.AddComponent<InteractionToggleGroup>();
            toggleGroup.Setup(toggles);

            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].OnChangeToggleEvent += OnChangeToggleListener;
            }
        }

        private void OnDestroy()
        {
            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].OnChangeToggleEvent -= OnChangeToggleListener;
            }
        }

        private void OnChangeToggleListener(InteractionToggle toggle)
        {
            if (toggle.IsOn)
            {
                for (int i = 0; i < toggles.Length; i++)
                {
                    if (toggles[i] == toggle)
                    {
                        OnChangeTabEvent?.Invoke(i);
                        break;
                    }
                }
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Slider))]
    public class InteractionSlider : UIElement, IPointerUpHandler
    {
        public delegate void PointerUpDelegate(float value);
        public event PointerUpDelegate OnPointerUpCallback;

        Slider slider;

        protected override void Start()
        {
            base.Start();

            slider = GetComponent<Slider>();
            slider.transition = Selectable.Transition.ColorTint;
        }

        protected override void ApplyTheme()
        {
            if (slider == null)
                slider = GetComponent<Slider>();

            ColorBlock sliderColor = slider.colors;

            sliderColor.normalColor = theme.sliderHandleDefaultColor;
            sliderColor.highlightedColor = theme.sliderHandleHighlightedColor;
            sliderColor.pressedColor = theme.sliderHandlePressedColor;
            sliderColor.selectedColor = theme.sliderHandleSelectedColor;
            sliderColor.disabledColor = theme.sliderHandleDisabledColor;

            slider.colors = sliderColor;

            Image fill = slider.fillRect.GetComponent<Image>();
            fill.color = theme.sliderFillColor;

            Image bg = slider.transform.GetComponentInFirstChild<Image>();
            bg.color = theme.sliderBgColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnPointerUpCallback?.Invoke(slider.value);
        }
    }
}


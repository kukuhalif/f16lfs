using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Image))]
    [DisallowMultipleComponent]
    public class InteractionToggle : UIElementInteraction
    {
        #region event delegate

        public delegate void OnStateChanged(bool on);
        public event OnStateChanged OnStateChangedEvent;

        public delegate void OnChangeToggle(InteractionToggle toggle);
        public event OnChangeToggle OnChangeToggleEvent;

        #endregion

        #region state & cache

        enum State { normalOff, hoverOff, pressedOff, normalOn, hoverOn, pressedOn }

        [SerializeField] bool defaultState;
        [SerializeField] Sprite onIcon;
        [SerializeField] Sprite offIcon;

        Image background;
        Image icon;
        TextMeshProUGUI tmpro;
        bool isOn;
        bool allowSwitchOff = true; // allow switch off by pointer click
        public bool AllowSwitchOff { set => allowSwitchOff = value; }
        public bool IsOn
        {
            get => isOn;
            set
            {
                ChangeState(value);
            }
        }

        #endregion

        #region virtual

        protected virtual ToggleColorTheme ToggleColor => theme.toggle;

        #endregion

        #region initialization

        protected override void Awake()
        {
            base.Awake();

            background = GetComponent<Image>();
            icon = transform.GetComponentInChildsExcludeThis<Image>();
            tmpro = transform.GetComponentInChildsExcludeThis<TextMeshProUGUI>();
        }

        protected override void Start()
        {
            base.Start();

            if (icon != null)
                icon.raycastTarget = false;

            if (tmpro != null)
                tmpro.raycastTarget = false;

            isOn = defaultState;
            SetColor(isOn ? State.normalOn : State.normalOff);
            SetIcon();
        }

        protected override List<Transform> GetAnimatedObject()
        {
            if (icon != null)
            {
                if (tmpro != null)
                    return new List<Transform>() { icon.transform, tmpro.transform };
                else
                    return new List<Transform>() { icon.transform };
            }

            if (tmpro != null)
                return new List<Transform>() { tmpro.transform };

            return new List<Transform>() { transform };
        }

        #endregion

        #region callback

        protected override Image GetIconImage()
        {
            return icon;
        }

        protected override void OnPointerClick()
        {
            if (!allowSwitchOff && isOn)
                return;

            ChangeState();
        }

        protected override void OnPointerEnter()
        {
            SetColor(isOn ? State.hoverOn : State.hoverOff);
            StartHoverImageSequence();
            HoverIn();
            HoverCoverUI.Show(rectTransform, hoverCoverDirection, useUnderline);
            ShowTooltip(rectTransform);
        }

        protected override void OnPointerExit()
        {
            SetColor(isOn ? State.normalOn : State.normalOff);
            StopHoverImageSequence();
            HoverOut();
            HoverCoverUI.Hide();
            HideTooltip();
        }

        protected override void OnPointerDown()
        {
            SetColor(isOn ? State.pressedOn : State.pressedOff);
            PointerDown();
            HoverCoverUI.PointerDown();
        }

        protected override void OnPointerUp()
        {
            SetColor(isOn ? State.hoverOn : State.hoverOff);
            PointerUp();
            HoverCoverUI.PointerUp();
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            SetColor(isOn ? State.normalOn : State.normalOff);
        }

        #endregion

        #region private function

        private void ChangeState()
        {
            isOn = !isOn;

            bool pointerInside = rectTransform.RectContains(VirtualTrainingInputSystem.PointerPosition);

            if (pointerInside)
                SetColor(isOn ? State.hoverOn : State.hoverOff);
            else
                SetColor(isOn ? State.normalOn : State.normalOff);

            OnStateChangedEvent?.Invoke(isOn);
            OnChangeToggleEvent?.Invoke(this);

            SetIcon();
        }

        private void SetIcon()
        {
            if (onIcon != null && offIcon != null && icon != null)
            {
                icon.sprite = isOn ? onIcon : offIcon;
            }
        }

        private void ChangeState(bool newState)
        {
            if (isOn == newState)
                return;

            isOn = newState;

            bool pointerInside = rectTransform.RectContains(VirtualTrainingInputSystem.PointerPosition);

            if (pointerInside)
                SetColor(isOn ? State.hoverOn : State.hoverOff);
            else
                SetColor(isOn ? State.normalOn : State.normalOff);

            OnStateChangedEvent?.Invoke(isOn);
            OnChangeToggleEvent?.Invoke(this);
        }

        private void SetColor(State nextState)
        {
            switch (nextState)
            {
                case State.normalOff:

                    background.color = ToggleColor.normalOff.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ToggleColor.normalOff.contentColor;
                    if (tmpro != null)
                        tmpro.color = ToggleColor.normalOff.contentColor;

                    break;
                case State.hoverOff:

                    background.color = ToggleColor.hoverOff.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ToggleColor.hoverOff.contentColor;
                    if (tmpro != null)
                        tmpro.color = ToggleColor.hoverOff.contentColor;

                    break;
                case State.pressedOff:

                    background.color = ToggleColor.pressedOff.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ToggleColor.pressedOff.contentColor;
                    if (tmpro != null)
                        tmpro.color = ToggleColor.pressedOff.contentColor;

                    break;
                case State.normalOn:

                    background.color = ToggleColor.normalOn.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ToggleColor.normalOn.contentColor;
                    if (tmpro != null)
                        tmpro.color = ToggleColor.normalOn.contentColor;

                    break;
                case State.hoverOn:

                    background.color = ToggleColor.hoverOn.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ToggleColor.hoverOn.contentColor;
                    if (tmpro != null)
                        tmpro.color = ToggleColor.hoverOn.contentColor;

                    break;
                case State.pressedOn:

                    background.color = ToggleColor.pressedOn.backgroundColor;
                    if (icon != null && !hoverIconAnimated)
                        icon.color = ToggleColor.pressedOn.contentColor;
                    if (tmpro != null)
                        tmpro.color = ToggleColor.pressedOn.contentColor;

                    break;
            }
        }

        #endregion
    }
}

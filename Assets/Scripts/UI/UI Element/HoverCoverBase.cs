using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace VirtualTraining.UI
{
    public abstract class HoverCoverBase : UIElement
    {
        [SerializeField] protected RectTransform rectTransform;

        protected static T LoadPrefab<T>(string resourcePath) where T : HoverCoverBase
        {
            GameObject go = (GameObject)Resources.Load(resourcePath);
            GameObject instantiated = Instantiate(go);
            T newHc = instantiated.GetComponent<T>();
            return newHc;
        }

        protected void Setup(RectTransform source)
        {
            rectTransform.SetParent(source);
            rectTransform.SetAsFirstSibling();
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;

            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, 1);
            gameObject.SetActive(true);
            rectTransform.localScale = Vector3.one;

            rectTransform.localEulerAngles = Vector3.zero;
        }

        public abstract void OnPointerDown();
        public abstract void OnPointerUp();
        public abstract void OnPointerEnter();
    }
}

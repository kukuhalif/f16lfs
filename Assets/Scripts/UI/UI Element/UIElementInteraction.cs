using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public abstract class UIElementInteraction : UIElementAnimation, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        #region event delegate

        public delegate void InteractionDelegate();
        public event InteractionDelegate OnClickEvent;
        public event InteractionDelegate OnPointerDownEvent;
        public event InteractionDelegate OnPointerUpEvent;
        public event InteractionDelegate OnPointerEnterEvent;
        public event InteractionDelegate OnPointerExitEvent;

        #endregion

        #region state

        // supaya ga dianggap keluar dari ui ini ketika masuk ke element ui yg ada di dalamnya ( contoh texdraw link di dalam tree element ui )
        [SerializeField] bool checkPointerPositionOnPointerExit;

        bool inputActive = true;
        bool disableSFX = false;

        public bool InputActive { get => inputActive; }

        #endregion

        #region hover sequence

        const float HOVER_SEQUENCE_DELAY = 1.5f;

        [SerializeField] int hoverSequenceFps = 40;
        [SerializeField] Sprite[] hoverImageSequences;

        Sprite defaultIcon;

        int frameIndex;

        private float frameTime { get => 1f / (float)hoverSequenceFps; }
        protected bool hoverIconAnimated => hoverImageSequences == null ? false : hoverImageSequences.Length > 0;

        private void HoverIconAnimation()
        {
            GetIconImage().sprite = hoverImageSequences[frameIndex];
            frameIndex++;
            if (frameIndex > hoverImageSequences.Length - 1)
            {
                frameIndex = 0;
                CancelInvoke(nameof(HoverIconAnimation));
                InvokeRepeating(nameof(HoverIconAnimation), HOVER_SEQUENCE_DELAY, frameTime);
            }
        }

        protected void StartHoverImageSequence()
        {
            if (GetIconImage() == null || !hoverIconAnimated)
                return;

            defaultIcon = GetIconImage().sprite;
            frameIndex = 0;
            CancelInvoke(nameof(HoverIconAnimation));
            InvokeRepeating(nameof(HoverIconAnimation), frameTime, frameTime);
        }

        protected void StopHoverImageSequence()
        {
            if (GetIconImage() == null || !hoverIconAnimated)
                return;

            CancelInvoke(nameof(HoverIconAnimation));

            if (defaultIcon != null)
                GetIconImage().sprite = defaultIcon;
        }

        #endregion

        #region on enable and disable

        protected virtual void OnDisable()
        {
            OnPointerExit();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            OnPointerExit();
        }

        #endregion

        #region abstract & virtual

        protected abstract void OnPointerClick();
        protected abstract Image GetIconImage();
        protected virtual void OnPointerDown() { }
        protected virtual void OnPointerEnter() { }
        protected virtual void OnPointerExit() { }
        protected virtual void OnPointerUp() { }

        #endregion

        #region callback

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.ButtonClick);

            if (inputActive)
            {
                OnPointerClick();
                OnClickEvent?.Invoke();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointerDown();
            OnPointerDownEvent?.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnPointerEnter();
            OnPointerEnterEvent?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (checkPointerPositionOnPointerExit)
            {
                bool stillHere = rectTransform.RectContains(VirtualTrainingInputSystem.PointerPosition);
                if (stillHere)
                    return;
            }

            OnPointerExit();
            OnPointerExitEvent?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnPointerUp();
            OnPointerUpEvent?.Invoke();
        }

        #endregion

        #region public function

        public void DisableSFX()
        {
            disableSFX = true;
        }

        public void SetInternalActiveState(bool active)
        {
            inputActive = active;
        }

        public void DisableHoverCover()
        {
            hoverCoverDirection = HoverCoverUI.Direction.None;
        }

        #endregion
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(TMP_InputField))]
    public class InteractionInputField : UIElement
    {
        protected override void Start()
        {
            base.Start();

            TMP_InputField inputField = GetComponent<TMP_InputField>();
            inputField.transition = Selectable.Transition.ColorTint;
        }

        protected override void ApplyTheme()
        {
            TMP_InputField inputField = GetComponent<TMP_InputField>();

            ColorBlock inputFieldColor = inputField.colors;

            inputFieldColor.normalColor = theme.inputFieldTextNormalColor;
            inputFieldColor.highlightedColor = theme.inputFieldTextHighlightColor;
            inputFieldColor.pressedColor = theme.inputFieldTextPressedColor;
            inputFieldColor.selectedColor = theme.inputFieldTextSelectedColor;
            inputFieldColor.disabledColor = theme.inputFieldTextDisabledColor;

            inputField.colors = inputFieldColor;

            Image bg = GetComponentInChildren<Image>();
            bg.color = theme.inputFieldBgColor;

            TextMeshProUGUI[] txts = transform.GetAllComponentsInChildsExcludeThis<TextMeshProUGUI>();
            for (int i = 0; i < txts.Length; i++)
            {
                txts[i].color = theme.genericTextColor;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public static class VirtualTrainingUiUtility
    {
        private static RectTransform FULLSCREEN_PANEL_IGNORE_TASKBAR;
        private static RectTransform FULLSCREEN_PANEL_CALCULATE_TASKBAR;
        private static RectTransform MINIMIZE_PANEL_AREA;

        private static bool DYNAMIC_UI_SIZE;
        private static Vector2 DEFAULT_UI_RESOLUTION;
        private static RectTransform FULLSCREEN_LEFT;
        private static RectTransform FULLSCREEN_RIGHT;
        private static RectTransform FULLSCREEN_TOP;
        private static RectTransform FULLSCREEN_BOTTOM_TASKBAR;
        private static RectTransform FULLSCREEN_BOTTOM;

        public static bool DynamicUiSize { get => DYNAMIC_UI_SIZE; set => DYNAMIC_UI_SIZE = value; }
        public static Vector2 DefaultUiResolution { get => DEFAULT_UI_RESOLUTION; set => DEFAULT_UI_RESOLUTION = value; }
        public static RectTransform FullscreenPanelIgnoreTaskbar { get => FULLSCREEN_PANEL_IGNORE_TASKBAR; }
        public static RectTransform FullscreenPanelCalculateTaskbar { get => FULLSCREEN_PANEL_CALCULATE_TASKBAR; }
        public static RectTransform MinimizePanelArea { get => MINIMIZE_PANEL_AREA; }
        public static RectTransform FullscreenLeft { get => FULLSCREEN_LEFT; }
        public static RectTransform FullscreenRight { get => FULLSCREEN_RIGHT; }
        public static RectTransform FullscreenTop { get => FULLSCREEN_TOP; }
        public static RectTransform FullscreenBottomTaskbar { get => FULLSCREEN_BOTTOM_TASKBAR; }
        public static RectTransform FullscreenBottom { get => FULLSCREEN_BOTTOM; }

        public static void SetupFullscreenPanel(RectTransform ignoreTaskbar, RectTransform calculateTaskbar, RectTransform minimizePanelArea, RectTransform fullscreenLeft, RectTransform fullscreenRight, RectTransform fullscreenTop, RectTransform fullscreenBottomTaskbar, RectTransform fullscreenBottom, bool dynamicUISize)
        {
            FULLSCREEN_PANEL_IGNORE_TASKBAR = ignoreTaskbar;
            FULLSCREEN_PANEL_CALCULATE_TASKBAR = calculateTaskbar;
            MINIMIZE_PANEL_AREA = minimizePanelArea;

            FULLSCREEN_LEFT = fullscreenLeft;
            FULLSCREEN_RIGHT = fullscreenRight;
            FULLSCREEN_TOP = fullscreenTop;
            FULLSCREEN_BOTTOM_TASKBAR = fullscreenBottomTaskbar;
            FULLSCREEN_BOTTOM = fullscreenBottom;

            DYNAMIC_UI_SIZE = dynamicUISize;
        }

        public static RectTransform GetFullscreenPanel(bool calculateTaskbar)
        {
            return calculateTaskbar ? FullscreenPanelCalculateTaskbar : FullscreenPanelIgnoreTaskbar;
        }

        public static Rect GetScreenBounds(bool calculateTaskbar)
        {
            if (VirtualTrainingCamera.CurrentCamera == null)
                return new Rect();

            Rect fullRect = new Rect();

            if (calculateTaskbar)
            {
                fullRect.xMin = FullscreenLeft.position.x;
                fullRect.xMax = FullscreenRight.position.x;
                fullRect.yMin = FullscreenBottomTaskbar.position.y; // todo : taskbar dibawah
                fullRect.yMax = FullscreenTop.position.y;

                return fullRect;
            }

            fullRect.xMin = FullscreenLeft.position.x;
            fullRect.xMax = FullscreenRight.position.x;
            fullRect.yMin = FullscreenBottom.position.y;
            fullRect.yMax = FullscreenTop.position.y;

            return fullRect;
        }

        public static float GetTaskbarTopOffset()
        {
            return 0f;
        }

        public static float GetTaskbarBottomOffset()
        {
            return FullscreenBottomTaskbar.position.y; // todo : taskbar dibawah
        }

        public static float GetTaskbarLeftOffset()
        {
            return 0f;
        }

        public static float GetTaskbarRightOffset()
        {
            return 0f;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public static class UIExtensionMethod
    {
        public static bool RectContains(this RectTransform container, RectTransform item)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(container, item.position);
        }

        public static bool RectContains(this RectTransform container, Vector2 position)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(container, position);
        }

        public static void RefreshLayout(this RectTransform rect)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        }

        /// <summary>
        /// borders -> left right top bottom
        /// </summary>
        public static void ConfinePanel(this RectTransform rect, bool calculateTaskbar, RectTransform[] corners, bool calculateResolution)
        {
            if (corners.Length < 4)
                return;

            Rect screenBounds = VirtualTrainingUiUtility.GetScreenBounds(calculateTaskbar);

            Vector2 offset = new Vector2();

            for (int i = 0; i < corners.Length; i++)
            {
                if (corners[i].position.x < screenBounds.xMin)
                {
                    rect.pivot = new Vector2(0, rect.pivot.y);
                    rect.anchorMin = new Vector2(0, rect.anchorMin.y);
                    rect.anchorMax = new Vector2(0, rect.anchorMax.y);
                    rect.anchoredPosition = new Vector2(0, rect.anchoredPosition.y);

                    if (calculateTaskbar)
                        offset.x = VirtualTrainingUiUtility.GetTaskbarLeftOffset();
                }
                else if (corners[i].position.x > screenBounds.xMax)
                {
                    rect.pivot = new Vector2(1, rect.pivot.y);
                    rect.anchorMin = new Vector2(1, rect.anchorMin.y);
                    rect.anchorMax = new Vector2(1, rect.anchorMax.y);
                    rect.anchoredPosition = new Vector2(0, rect.anchoredPosition.y);

                    if (calculateTaskbar)
                        offset.x = VirtualTrainingUiUtility.GetTaskbarRightOffset();
                }

                if (corners[i].position.y < screenBounds.yMin) // bottom
                {
                    rect.pivot = new Vector2(rect.pivot.x, 0);
                    rect.anchorMin = new Vector2(rect.anchorMin.x, 0);
                    rect.anchorMax = new Vector2(rect.anchorMax.x, 0);
                    rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

                    if (calculateTaskbar)
                        offset.y = VirtualTrainingUiUtility.GetTaskbarBottomOffset();
                }
                else if (corners[i].position.y > screenBounds.yMax) // top
                {
                    rect.pivot = new Vector2(rect.pivot.x, 1);
                    rect.anchorMin = new Vector2(rect.anchorMin.x, 1);
                    rect.anchorMax = new Vector2(rect.anchorMax.x, 1);
                    rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 1);

                    if (calculateTaskbar)
                        offset.y = VirtualTrainingUiUtility.GetTaskbarTopOffset();
                }
            }

            if (calculateResolution && !VirtualTrainingUiUtility.DynamicUiSize)
                rect.anchoredPosition += (offset * (VirtualTrainingUiUtility.DefaultUiResolution / SettingUtility.LastResolution));
            else
                rect.anchoredPosition += offset;
        }

        public static void SetPivot(this RectTransform rectTransform, Vector2 pivot)
        {
            if (rectTransform == null) return;

            Vector2 size = rectTransform.rect.size;
            Vector2 deltaPivot = rectTransform.pivot - pivot;
            Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);
            rectTransform.pivot = pivot;
            rectTransform.localPosition -= deltaPosition;
        }

        public static void SetAnchors(this RectTransform This, Vector2 AnchorMin, Vector2 AnchorMax)
        {
            var Parent = This.parent;
            if (Parent) This.SetParent(null);
            This.anchorMin = AnchorMin;
            This.anchorMax = AnchorMax;
            if (Parent) This.SetParent(Parent);
        }

        public static void EnsureVerticalVisibility(this ScrollRect scrollRect, RectTransform child)
        {
            Debug.Assert(child.parent == scrollRect.content,
                "EnsureVisibility assumes that 'child' is directly nested in the content of 'scrollRect'");

            float viewportHeight = scrollRect.viewport.rect.height;
            Vector2 scrollPosition = scrollRect.content.anchoredPosition;

            float elementTop = child.anchoredPosition.y;
            float elementBottom = elementTop - child.rect.height;

            float visibleContentTop = -scrollPosition.y - child.rect.height;
            float visibleContentBottom = -scrollPosition.y - viewportHeight - child.rect.height;

            float scrollDelta =
                elementTop > visibleContentTop ? visibleContentTop - elementTop :
                elementBottom < visibleContentBottom ? visibleContentBottom - elementBottom :
                0f;

            scrollPosition.y += scrollDelta;
            scrollRect.content.anchoredPosition = scrollPosition;
        }

        public static void EnsureHorizontalVisibility(this ScrollRect scrollRect, RectTransform child)
        {
            Debug.Assert(child.parent == scrollRect.content,
                "EnsureVisibility assumes that 'child' is directly nested in the content of 'scrollRect'");

            float viewportWidth = scrollRect.viewport.rect.width;
            Vector2 scrollPosition = scrollRect.content.anchoredPosition;

            float elementLeft = child.anchoredPosition.x;
            float elementRight = elementLeft - child.rect.width;

            float visibleContentLeft = -scrollPosition.x - child.rect.width;
            float visibleContentRight = -scrollPosition.x - viewportWidth - child.rect.width;

            float scrollDelta =
                elementLeft > visibleContentLeft ? visibleContentLeft - elementLeft :
                elementRight < visibleContentRight ? visibleContentRight - elementRight :
                0f;

            scrollPosition.x += scrollDelta;
            scrollRect.content.anchoredPosition = scrollPosition;
        }

        public static void SetLeft(this RectTransform rt, float left)
        {
            rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        }

        public static void SetRight(this RectTransform rt, float right)
        {
            rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        }

        public static void SetTop(this RectTransform rt, float top)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        }

        public static void SetBottom(this RectTransform rt, float bottom)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        }

        public static float GetLeft(this RectTransform rt)
        {
            return rt.offsetMin.x;
        }

        public static float GetRight(this RectTransform rt)
        {
            return -rt.offsetMin.x;
        }

        public static float GetTop(this RectTransform rt)
        {
            return -rt.offsetMin.y;
        }

        public static float GetBottom(this RectTransform rt)
        {
            return rt.offsetMin.y;
        }
    }
}

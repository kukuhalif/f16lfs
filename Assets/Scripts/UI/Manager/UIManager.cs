using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] bool initOnStart = false;
        [SerializeField] GameObject[] toEnabledUIs;
        [SerializeField] CanvasScaler[] nonPanelCanvasScalers;

        List<CanvasScaler> canvasScalers = new List<CanvasScaler>();
        List<CanvasScaler> ignoreScaleCanvasScalers = new List<CanvasScaler>();

        private void Start()
        {
            if (initOnStart)
                StartCoroutine(EnableUI());
            else
                EventManager.AddListener<InitializeUIEvent>(InitializeUIListener);
        }


        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void InitializeUIListener(InitializeUIEvent e)
        {
            EventManager.RemoveListener<InitializeUIEvent>(InitializeUIListener);
            StartCoroutine(EnableUI());
        }

        private IEnumerator EnableUI()
        {
            for (int i = 0; i < toEnabledUIs.Length; i++)
            {
                yield return null;
                toEnabledUIs[i].SetActive(true);
                UIElement[] uiElements = toEnabledUIs[i].transform.GetAllComponentsInChilds<UIElement>();

                for (int e = 0; e < uiElements.Length; e++)
                {
                    if (uiElements[e] != null)
                    {
                        if (uiElements[e].IgnoreDynamicUiSize)
                        {
                            var isc = uiElements[e].transform.GetComponentsInParentRecursive<CanvasScaler>();
                            if (!ignoreScaleCanvasScalers.Contains(isc))
                                ignoreScaleCanvasScalers.Add(isc);
                        }
                        else
                        {
                            var cs = uiElements[e].transform.GetComponentsInParentRecursive<CanvasScaler>();
                            if (!canvasScalers.Contains(cs))
                                canvasScalers.Add(cs);
                        }
                    }
                }
            }

            Debug.Log("UI initialization finished");
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            yield return null;
            EventManager.TriggerEvent(new InitializationUIDoneEvent());

            SetCanvasResolution(SettingUtility.LastResolution);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            SetCanvasResolution(e.resolution);
        }

        private void SetCanvasResolution(Vector2 resolution)
        {
            Debug.Log("set canvas scaler");
            for (int i = 0; i < canvasScalers.Count; i++)
            {
                if (SettingUtility.LastSetting.dynamicUiSize)
                    canvasScalers[i].referenceResolution = resolution;
                else
                    canvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
            }

            if (ignoreScaleCanvasScalers != null)
                for (int i = 0; i < ignoreScaleCanvasScalers.Count; i++)
                {
                    ignoreScaleCanvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
                }

            if (nonPanelCanvasScalers != null)
                for (int i = 0; i < nonPanelCanvasScalers.Length; i++)
                {
                    if (SettingUtility.LastSetting.dynamicUiSize)
                        nonPanelCanvasScalers[i].referenceResolution = resolution;
                    else
                        nonPanelCanvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
                }
        }
    }
}

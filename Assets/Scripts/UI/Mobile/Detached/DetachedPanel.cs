using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class DetachedPanel : UIPanel
    {
        [SerializeField] Transform contentParent;

        public void Detach(BaseContentPanel contentPanel)
        {
            ShowPanel();

            RectTransform content = contentPanel.GetContentPanel();
            content.SetParent(contentParent);
            content.transform.SetAsFirstSibling();
            content.SetLeft(0);
            content.SetTop(0);
            content.SetRight(0);
            content.SetBottom(0);
            content.localScale = Vector3.one;
        }
    }
}

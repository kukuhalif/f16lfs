using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.UI.Mobile
{
    public class AnimationPlayerPanel : MonoBehaviour
    {
        [SerializeField] Slider sliderAnimation;
        [SerializeField] GameObject animationSection;
        [SerializeField] InteractionButton playPauseAnimation;
        [SerializeField] GameObject animationButtonController;

        AnimationManager animationManager;
        bool animationIsPlaying;

        private void Start()
        {
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
            EventManager.AddListener<AnimationPlayEvent>(PlayAnimationListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            playPauseAnimation.OnClickEvent += PlayPauseAnimationListener;
            sliderAnimation.onValueChanged.AddListener(delegate { AnimationSliderChanged(); });
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<AnimationPlayEvent>(PlayAnimationListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            playPauseAnimation.OnClickEvent -= PlayPauseAnimationListener;
            sliderAnimation.onValueChanged.RemoveListener(delegate { AnimationSliderChanged(); });
            animationManager.SetSlider(sliderAnimation, null, null);
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
            animationManager = FindObjectOfType<AnimationManager>();
            animationManager.SetSlider(sliderAnimation, ResetAnimationDoneListener, DoneAnimationListener);
            animationManager.ResetAnimation();
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            animationSection.SetActive(false);
        }

        void PlayAnimationListener(AnimationPlayEvent e)
        {
            if (e.animationData == null)
            {
                animationSection.SetActive(false);
                return;
            }

            if (animationSection != null)
                animationSection.SetActive(true);

            animationIsPlaying = !animationIsPlaying;
            animationIsPlaying = true;
            playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);

            if (animationIsPlaying)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);
            else
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);

            if (e.animationData.isShowAnimationUI == false)
            {
                if (animationButtonController != null)
                    animationButtonController.SetActive(false);
            }
            else
            {
                if (animationButtonController != null)
                    animationButtonController.SetActive(true);
            }
        }


        void ResetAnimationDoneListener()
        {
            if (animationSection != null)
                animationSection.SetActive(false);
            animationIsPlaying = false;
        }

        void DoneAnimationListener()
        {
            animationIsPlaying = false;
            if (playPauseAnimation != null)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);
        }

        void PlayPauseAnimationListener()
        {
            if (animationManager == null)
                return;

            animationManager.PlayAnimation();

            animationIsPlaying = !animationIsPlaying;
            if (animationIsPlaying)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);
            else
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);
        }

        public void AnimationSliderChanged()
        {
            if (animationManager == null)
                return;

            animationManager.ValueChanged();
        }
    }
}

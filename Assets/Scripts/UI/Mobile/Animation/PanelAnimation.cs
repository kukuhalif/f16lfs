using System;
using System.Collections;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform), typeof(CanvasGroup))]
    public class PanelAnimation : MonoBehaviour
    {
        [SerializeField] Vector2 startAnchoredPosition;
        [SerializeField] Vector2 endAnchoredPosition;

        RectTransform rectTransform;
        CanvasGroup canvasGroup;

        float progress;

        Vector2 lastAnchoredPosition;
        Vector2 destinationAnchoredPosition;

        float lastAlpha;
        float destinationAlpha;

        AnimationCurve animationCurve;

        Action onComplete;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            rectTransform = GetComponent<RectTransform>();
        }

        public void FadeIn(Action onComplete = null)
        {
            progress = 0;
            lastAnchoredPosition = startAnchoredPosition;
            destinationAnchoredPosition = endAnchoredPosition;
            lastAlpha = 0;
            destinationAlpha = 1;
            animationCurve = DatabaseManager.GetUIFadeInCurve();

            this.onComplete = onComplete;

            StartCoroutine(Animate(DatabaseManager.GetUITransitionSpeed()));
        }

        public void FadeOut(Action onComplete = null)
        {
            progress = 0;
            lastAnchoredPosition = endAnchoredPosition;
            destinationAnchoredPosition = startAnchoredPosition;
            lastAlpha = 1;
            destinationAlpha = 0;
            animationCurve = DatabaseManager.GetUIFadeOutCurve();

            this.onComplete = onComplete;

            StartCoroutine(Animate(DatabaseManager.GetUITransitionSpeed()));
        }

        private IEnumerator Animate(float transitionSpeed)
        {
            while (progress < 1f)
            {
                progress += Time.deltaTime * transitionSpeed;
                if (progress > 1f)
                    progress = 1f;

                float value = animationCurve.Evaluate(progress);
                rectTransform.anchoredPosition = Vector2.LerpUnclamped(lastAnchoredPosition, destinationAnchoredPosition, value);
                canvasGroup.alpha = Mathf.Lerp(lastAlpha, destinationAlpha, value);

                yield return null;
            }

            if (onComplete != null)
            {
                yield return null;
                onComplete.Invoke();
                onComplete = null;
            }
        }
    }
}

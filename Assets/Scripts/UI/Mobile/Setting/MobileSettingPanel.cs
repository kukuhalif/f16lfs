using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class MobileSettingPanel : MonoBehaviour
    {
        [SerializeField] PanelAnimation panelAnimation;

        [SerializeField] TextMeshProUGUI qualityLabel;
        [SerializeField] InteractionButton previousQualityButton;
        [SerializeField] InteractionButton nextQualityButton;

        [SerializeField] Slider volumeSlider;
        [SerializeField] Slider rotateSpeedSlider;
        [SerializeField] Slider zoomSpeedSlider;
        [SerializeField] Slider dragSpeedSlider;

        [SerializeField] InteractionButton defaultButton;
        [SerializeField] InteractionButton applyButton;
        [SerializeField] InteractionButton closeButton;

        List<string> qualityOptions = new List<string>();

        SettingData defaultSetting;
        SettingData modifiedSetting;
        SettingData lastSetting;

        bool initialized = false;

        Canvas canvas;

        public void Initialize()
        {
            if (initialized)
                return;

            canvas = transform.GetComponentsInParentRecursive<Canvas>();

            defaultSetting = JsonUtility.FromJson<SettingData>(JsonUtility.ToJson(DatabaseManager.GetDefaultSetting()));
            modifiedSetting = JsonUtility.FromJson<SettingData>(JsonUtility.ToJson(SettingUtility.GetSavedSetting()));

            SetMinMaxSetting();
            QualityOptionInit();

            previousQualityButton.OnClickEvent += PreviousQuality;
            nextQualityButton.OnClickEvent += NextQuality;

            volumeSlider.onValueChanged.AddListener(VolumeSliderListener);
            rotateSpeedSlider.onValueChanged.AddListener(RotateSliderListener);
            zoomSpeedSlider.onValueChanged.AddListener(ZoomSliderListener);
            dragSpeedSlider.onValueChanged.AddListener(DragSliderListener);

            defaultButton.OnClickEvent += DefaultSettingListener;
            applyButton.OnClickEvent += ApplySettingListener;
            closeButton.OnClickEvent += CloseButton_OnClickEvent;

            SetUI();
            ApplySetting();

            initialized = true;
        }

        private void OnDestroy()
        {
            previousQualityButton.OnClickEvent -= PreviousQuality;
            nextQualityButton.OnClickEvent -= NextQuality;
            defaultButton.OnClickEvent -= DefaultSettingListener;
            applyButton.OnClickEvent -= ApplySettingListener;
            closeButton.OnClickEvent -= CloseButton_OnClickEvent;

            volumeSlider.onValueChanged.RemoveListener(VolumeSliderListener);
            rotateSpeedSlider.onValueChanged.RemoveListener(RotateSliderListener);
            zoomSpeedSlider.onValueChanged.RemoveListener(ZoomSliderListener);
            dragSpeedSlider.onValueChanged.RemoveListener(DragSliderListener);
        }

        private void OnEnable()
        {
            Initialize();

            modifiedSetting = JsonUtility.FromJson<SettingData>(JsonUtility.ToJson(SettingUtility.GetSavedSetting()));
            lastSetting = JsonUtility.FromJson<SettingData>(JsonUtility.ToJson(modifiedSetting));

            SetUI();
        }

        private void VolumeSliderListener(float value)
        {
            modifiedSetting.sfxVolume = value;
            modifiedSetting.voiceVolume = value;
        }

        private void RotateSliderListener(float value)
        {
            modifiedSetting.rotateSpeed = value;
        }

        private void ZoomSliderListener(float value)
        {
            modifiedSetting.zoomSpeed = value;
        }

        private void DragSliderListener(float value)
        {
            modifiedSetting.dragSpeed = value;
        }

        private void SetMinMaxSetting()
        {
            var minMax = DatabaseManager.GetMinMaxSetting();

            rotateSpeedSlider.minValue = minMax.rotateSpeed.x;
            rotateSpeedSlider.maxValue = minMax.rotateSpeed.y;

            zoomSpeedSlider.minValue = minMax.zoomSpeed.x;
            zoomSpeedSlider.maxValue = minMax.zoomSpeed.y;

            dragSpeedSlider.minValue = minMax.dragSpeed.x;
            dragSpeedSlider.maxValue = minMax.dragSpeed.y;

            volumeSlider.minValue = minMax.sfxVolume.x;
            volumeSlider.maxValue = minMax.sfxVolume.y;
        }

        public void ShowPanel()
        {
            if (canvas != null && canvas.sortingOrder < UIPanel.SelectedOrder)
                canvas.sortingOrder = UIPanel.SelectedOrder;

            transform.parent.gameObject.SetActive(true);
            panelAnimation.FadeIn();
        }

        public void HidePanel()
        {
            panelAnimation.FadeOut(() =>
            {
                transform.parent.gameObject.SetActive(false);
            });
        }

        private void DefaultSettingListener()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Default setting ?", () =>
            {
                ResetSetting();
            }, null));
        }

        private void ApplySettingListener()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Apply setting ?", () =>
            {
                ApplySetting();
                HidePanel();
            }, null));
        }

        private void ResetSetting()
        {
            Debug.Log("reset setting");
            string defaultData = JsonUtility.ToJson(defaultSetting);
            modifiedSetting = JsonUtility.FromJson<SettingData>(defaultData);

            SetUI();
            ApplySetting();
            SettingUtility.SetSettingData(modifiedSetting);
        }

        private void CloseButton_OnClickEvent()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Cancel setting ?", () =>
            {
                modifiedSetting = JsonUtility.FromJson<SettingData>(JsonUtility.ToJson(lastSetting));
                SetUI();
                ApplySetting();
                HidePanel();
            }, null));
        }

        private void SetUI()
        {
            qualityLabel.text = qualityOptions[modifiedSetting.quality];
            volumeSlider.value = modifiedSetting.sfxVolume;
            rotateSpeedSlider.value = modifiedSetting.rotateSpeed;
            zoomSpeedSlider.value = modifiedSetting.zoomSpeed;
            dragSpeedSlider.value = modifiedSetting.dragSpeed;
        }

        private void GetSliderValue()
        {
            modifiedSetting.sfxVolume = volumeSlider.value;
            modifiedSetting.voiceVolume = volumeSlider.value;
            modifiedSetting.rotateSpeed = rotateSpeedSlider.value;
            modifiedSetting.zoomSpeed = zoomSpeedSlider.value;
            modifiedSetting.dragSpeed = dragSpeedSlider.value;
        }

        private void QualityOptionInit()
        {
            qualityOptions.Clear();
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                qualityOptions.Add(QualitySettings.names[i]);
            }

            // quality default
            defaultSetting.quality = qualityOptions.Count - 1;
            if (modifiedSetting.quality >= qualityOptions.Count)
                modifiedSetting.quality = qualityOptions.Count - 1;
        }

        private void PreviousQuality()
        {
            modifiedSetting.quality--;

            if (modifiedSetting.quality < 0)
                modifiedSetting.quality = qualityOptions.Count - 1;

            qualityLabel.text = qualityOptions[modifiedSetting.quality];
        }

        private void NextQuality()
        {
            modifiedSetting.quality++;

            if (modifiedSetting.quality >= qualityOptions.Count)
                modifiedSetting.quality = 0;

            qualityLabel.text = qualityOptions[modifiedSetting.quality];
        }

        private void ApplySetting()
        {
            GetSliderValue();

            QualitySettings.SetQualityLevel(modifiedSetting.quality);
            Debug.Log("set quality to " + qualityOptions[QualitySettings.GetQualityLevel()]);

            SettingUtility.SetLastSetting(modifiedSetting);

            Vector2 res = new Vector2(Screen.width, Screen.height);
            SettingUtility.SetLastResolution(res);

            EventManager.TriggerEvent(new ApplySettingEvent(modifiedSetting, res));
            EventManager.TriggerEvent(new SwitchUIThemeEvent(modifiedSetting.uiTheme));

            SettingUtility.SetSettingData(modifiedSetting);
        }
    }
}


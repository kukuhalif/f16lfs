using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class MainToolbar : BaseToolbar
    {
        [SerializeField] NavigationToolbar navigationToolbar;
        [SerializeField] ObjectInteractionToolbar objectInteractionToolbar;
        [SerializeField] CutawayToolbar cutawayToolbar;

        [SerializeField] InteractionButton navigationButton;
        [SerializeField] InteractionButton objectInteractionButton;
        [SerializeField] InteractionButton cutawayButton;
        [SerializeField] InteractionToggle objectListToggle;
        [SerializeField] InteractionButton partNumberButton;
        [SerializeField] InteractionButton resetGlobalButton;

        protected override void Start()
        {
            base.Start();

            navigationButton.OnClickEvent += NavigationButton_OnClickEvent;
            objectInteractionButton.OnClickEvent += ObjectInteractionButton_OnClickEvent;
            cutawayButton.OnClickEvent += CutawayButton_OnClickEvent;
            objectListToggle.OnStateChangedEvent += ObjectListToggle_OnStateChangedEvent;
            partNumberButton.OnClickEvent += PartNumberButton_OnClickEvent;
            resetGlobalButton.OnClickEvent += ResetGlobalButton_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            navigationButton.OnClickEvent -= NavigationButton_OnClickEvent;
            objectInteractionButton.OnClickEvent -= ObjectInteractionButton_OnClickEvent;
            cutawayButton.OnClickEvent -= CutawayButton_OnClickEvent;
            objectListToggle.OnStateChangedEvent -= ObjectListToggle_OnStateChangedEvent;
            partNumberButton.OnClickEvent -= PartNumberButton_OnClickEvent;
            resetGlobalButton.OnClickEvent -= ResetGlobalButton_OnClickEvent;
        }

        private void ResetGlobalButton_OnClickEvent()
        {
            Debug.Log("reset global");
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        private void PartNumberButton_OnClickEvent()
        {
            Debug.Log("part number");
        }

        private void ObjectListToggle_OnStateChangedEvent(bool on)
        {
            Debug.Log("object list");
            EventManager.TriggerEvent(new ObjectInteractionShowObjectListEvent(on));
        }

        private void CutawayButton_OnClickEvent()
        {
            Hide(() =>
            {
                cutawayToolbar.Show();
            });
        }

        private void ObjectInteractionButton_OnClickEvent()
        {
            Hide(() =>
            {
                objectInteractionToolbar.Show();
            });
        }

        private void NavigationButton_OnClickEvent()
        {
            Hide(() =>
            {
                navigationToolbar.Show();
            });
        }
    }
}

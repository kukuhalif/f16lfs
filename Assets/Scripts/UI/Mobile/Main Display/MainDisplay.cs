using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.Mobile
{
    public class MainDisplay : MonoBehaviour
    {
        [SerializeField] private AccessPanel accessPanel;
        [SerializeField] private PropertiesPanel propertiesPanel;
        [SerializeField] private Toolbar toolbarPanel;

        public void ShowPanel()
        {
            accessPanel.ShowPanel();
            propertiesPanel.ShowPanel();

            if (toolbarPanel.IsToolbarEnabledFromPropertiesPanel)
                toolbarPanel.ShowPanel();
        }

        public void HidePanel(Action onComplete)
        {
            accessPanel.HidePanel(onComplete);
            propertiesPanel.HidePanel();

            if (toolbarPanel.IsToolbarEnabledFromPropertiesPanel)
                toolbarPanel.HidePanel();
        }
    }
}

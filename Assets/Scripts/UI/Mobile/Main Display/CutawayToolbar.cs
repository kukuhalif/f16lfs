using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class CutawayToolbar : BaseToolbar
    {
        [SerializeField] MainToolbar mainToolbar;
        [SerializeField] InteractionButton backButton;

        [SerializeField] InteractionButton resetButton;
        [SerializeField] InteractionToggle translateToggle;
        [SerializeField] InteractionToggle rotateToggle;
        [SerializeField] InteractionToggle planeToggle;
        [SerializeField] InteractionToggle boxToggle;
        [SerializeField] InteractionToggle cornerToggle;
        [SerializeField] InteractionToggleGroup cutawayModeToggleGroup;

        bool cutawayOn;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<CutawayEvent>(CutawayListener);

            backButton.OnClickEvent += BackButton_OnClickEvent;
            translateToggle.OnStateChangedEvent += TranslateToggle_OnStateChangedEvent;
            planeToggle.OnStateChangedEvent += PlaneToggle_OnStateChangedEvent;
            boxToggle.OnStateChangedEvent += BoxToggle_OnStateChangedEvent;
            cornerToggle.OnStateChangedEvent += CornerToggle_OnStateChangedEvent;
            resetButton.OnClickEvent += ResetButton_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<CutawayEvent>(CutawayListener);

            backButton.OnClickEvent -= BackButton_OnClickEvent;
            translateToggle.OnStateChangedEvent -= TranslateToggle_OnStateChangedEvent;
            planeToggle.OnStateChangedEvent -= PlaneToggle_OnStateChangedEvent;
            boxToggle.OnStateChangedEvent -= BoxToggle_OnStateChangedEvent;
            cornerToggle.OnStateChangedEvent -= CornerToggle_OnStateChangedEvent;
            resetButton.OnClickEvent -= ResetButton_OnClickEvent;
        }

        private void ResetButton_OnClickEvent()
        {
            EventManager.TriggerEvent(new ResetCutawayEvent());
        }

        private void CornerToggle_OnStateChangedEvent(bool on)
        {
            if (on)
            {
                Debug.Log("cutaway corner");
                cutawayOn = true;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Corner));
            }
            else
                AllOff();
        }

        private void BoxToggle_OnStateChangedEvent(bool on)
        {
            if (on)
            {
                Debug.Log("cutaway box");
                cutawayOn = true;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Box));
            }
            else
                AllOff();
        }

        private void PlaneToggle_OnStateChangedEvent(bool on)
        {
            if (on)
            {
                Debug.Log("cutaway plane");
                cutawayOn = true;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Plane));
            }
            else
                AllOff();
        }

        private void AllOff()
        {
            if (cutawayOn && cutawayModeToggleGroup.GetSelectedToggle() == null)
            {
                Debug.Log("cutaway off");
                cutawayOn = false;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.None));
            }
        }

        private void TranslateToggle_OnStateChangedEvent(bool on)
        {
            EventManager.TriggerEvent(new SwitchCutawayGizmoModeEvent(on));
        }

        private void BackButton_OnClickEvent()
        {
            Hide(() =>
            {
                mainToolbar.Show();
            });
        }

        private void CutawayListener(CutawayEvent e)
        {
            planeToggle.OnStateChangedEvent -= PlaneToggle_OnStateChangedEvent;
            boxToggle.OnStateChangedEvent -= BoxToggle_OnStateChangedEvent;
            cornerToggle.OnStateChangedEvent -= CornerToggle_OnStateChangedEvent;

            if (e.cutaway != null)
            {
                switch (e.cutaway.state)
                {
                    case CutawayState.None:

                        planeToggle.IsOn = false;
                        boxToggle.IsOn = false;
                        cornerToggle.IsOn = false;

                        break;
                    case CutawayState.Plane:

                        planeToggle.IsOn = true;

                        break;
                    case CutawayState.Box:

                        boxToggle.IsOn = true;

                        break;
                    case CutawayState.Corner:

                        cornerToggle.IsOn = true;

                        break;
                }
            }
            else
            {
                planeToggle.IsOn = false;
                boxToggle.IsOn = false;
                cornerToggle.IsOn = false;
            }

            planeToggle.OnStateChangedEvent += PlaneToggle_OnStateChangedEvent;
            boxToggle.OnStateChangedEvent += BoxToggle_OnStateChangedEvent;
            cornerToggle.OnStateChangedEvent += CornerToggle_OnStateChangedEvent;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class PropertiesPanel : MonoBehaviour
    {
        [SerializeField] PanelAnimation panelAnimation;
        [SerializeField] InteractionButton homeButton;
        [SerializeField] InteractionButton toolbarButton;
        [SerializeField] InteractionButton settingButton;
        [SerializeField] MainDisplay mainDisplay;
        [SerializeField] MobileLandingPagePanel landingPage;
        [SerializeField] Toolbar toolbar;
        [SerializeField] MobileSettingPanel settingPanel;

        private void Start()
        {
            homeButton.OnClickEvent += HomeButton_OnClickEvent;
            toolbarButton.OnClickEvent += ToolbarButton_OnClickEvent;
            settingButton.OnClickEvent += SettingButton_OnClickEvent;
        }

        private void OnDestroy()
        {
            homeButton.OnClickEvent -= HomeButton_OnClickEvent;
            toolbarButton.OnClickEvent -= ToolbarButton_OnClickEvent;
            settingButton.OnClickEvent -= SettingButton_OnClickEvent;
        }

        private void HomeButton_OnClickEvent()
        {
            EventManager.TriggerEvent(new FreePlayEvent(true));

            mainDisplay.HidePanel(() =>
            {
                landingPage.ShowPanel();
            });
        }

        private void SettingButton_OnClickEvent()
        {
            settingPanel.ShowPanel();
        }

        private void ToolbarButton_OnClickEvent()
        {
            if (toolbar.IsToolbarEnabledFromPropertiesPanel)
                toolbar.HideFromPropertiesPanel();
            else
                toolbar.ShowFromPropertiesPanel();
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);
            panelAnimation.FadeIn();
        }

        public void HidePanel()
        {
            panelAnimation.FadeOut(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}

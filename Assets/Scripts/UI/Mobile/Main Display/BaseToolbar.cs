using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.Mobile
{
    public abstract class BaseToolbar : MonoBehaviour
    {
        [SerializeField] Toolbar toolbar;
        PanelAnimation panelAnimation;

        protected virtual void Start()
        {
            panelAnimation = GetComponent<PanelAnimation>();
        }

        protected virtual void OnDestroy()
        {

        }

        public void Show()
        {
            gameObject.SetActive(true);
            toolbar.SetCurrentToolbar(this);
            panelAnimation.FadeIn();
        }

        public void Hide(Action onComplete = null)
        {
            panelAnimation.FadeOut(() =>
            {
                gameObject.SetActive(false);
                onComplete?.Invoke();
            });
        }
    }
}

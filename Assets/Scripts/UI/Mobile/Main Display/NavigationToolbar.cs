using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class NavigationToolbar : BaseToolbar
    {
        [SerializeField] MainToolbar mainToolbar;
        [SerializeField] InteractionButton backButton;
        [SerializeField] InteractionToggle rotateToggle;
        [SerializeField] InteractionToggle dragToggle;
        [SerializeField] InteractionToggle cameraLockToggle;
        [SerializeField] InteractionButton resetCameraButton;

        protected override void Start()
        {
            base.Start();

            backButton.OnClickEvent += BackButton_OnClickEvent;
            rotateToggle.OnStateChangedEvent += RotateToggle_OnStateChangedEvent;
            cameraLockToggle.OnStateChangedEvent += CameraLockToggle_OnStateChangedEvent;
            resetCameraButton.OnClickEvent += ResetCameraButton_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            backButton.OnClickEvent -= BackButton_OnClickEvent;
            rotateToggle.OnStateChangedEvent -= RotateToggle_OnStateChangedEvent;
            cameraLockToggle.OnStateChangedEvent -= CameraLockToggle_OnStateChangedEvent;
            resetCameraButton.OnClickEvent -= ResetCameraButton_OnClickEvent;
        }

        private void CameraLockToggle_OnStateChangedEvent(bool on)
        {
            Debug.Log("cam lock " + on);
            EventManager.TriggerEvent(new ObjectInteractionLockCameraEvent(on));
        }

        private void ResetCameraButton_OnClickEvent()
        {
            Debug.Log("reset camera");
            EventManager.TriggerEvent(new ObjectInteractionResetCameraEvent());
        }

        private void RotateToggle_OnStateChangedEvent(bool on)
        {
            Debug.Log("rotate " + on);
            EventManager.TriggerEvent(new ObjectInteractionRotateEvent(on));
        }

        private void BackButton_OnClickEvent()
        {
            Hide(() =>
            {
                mainToolbar.Show();
            });
        }
    }
}

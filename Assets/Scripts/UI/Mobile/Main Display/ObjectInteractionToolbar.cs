using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class ObjectInteractionToolbar : BaseToolbar
    {
        [SerializeField] MainToolbar mainToolbar;
        [SerializeField] InteractionButton backButton;
        [SerializeField] InteractionToggle selectToggle;
        [SerializeField] InteractionButton hideButton;
        [SerializeField] InteractionToggle showButton;
        [SerializeField] InteractionToggle pullApartToggle;
        [SerializeField] InteractionButton xRayToggle;
        [SerializeField] InteractionButton solidToggle;
        [SerializeField] InteractionButton resetButton;

        protected override void Start()
        {
            base.Start();

            backButton.OnClickEvent += BackButton_OnClickEvent;
            selectToggle.OnStateChangedEvent += SelectToggle_OnStateChangedEvent;
            hideButton.OnClickEvent += HideButton_OnClickEvent;
            showButton.OnStateChangedEvent += ShowButton_OnStateChangedEvent;
            pullApartToggle.OnStateChangedEvent += PullApartToggle_OnStateChangedEvent;
            resetButton.OnClickEvent += ResetButton_OnClickEvent;
            xRayToggle.OnClickEvent += XRayToggle_OnClickEvent;
            solidToggle.OnClickEvent += SolidToggle_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            backButton.OnClickEvent -= BackButton_OnClickEvent;
            selectToggle.OnStateChangedEvent -= SelectToggle_OnStateChangedEvent;
            hideButton.OnClickEvent -= HideButton_OnClickEvent;
            pullApartToggle.OnStateChangedEvent -= PullApartToggle_OnStateChangedEvent;
            resetButton.OnClickEvent -= ResetButton_OnClickEvent;
            xRayToggle.OnClickEvent -= XRayToggle_OnClickEvent;
            solidToggle.OnClickEvent -= SolidToggle_OnClickEvent;
            showButton.OnStateChangedEvent -= ShowButton_OnStateChangedEvent;
        }

        private void ResetButton_OnClickEvent()
        {
            Debug.Log("reset");
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
        }

        private void PullApartToggle_OnStateChangedEvent(bool on)
        {
            Debug.Log("pull apart" + on);
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(on));
        }

        private void ShowButton_OnStateChangedEvent(bool on)
        {
            Debug.Log("show");
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
            if (on)
            {
                showButton.OnStateChangedEvent -= ShowButton_OnStateChangedEvent;
                showButton.IsOn = false;
                showButton.OnStateChangedEvent += ShowButton_OnStateChangedEvent;
            }
        }

        private void SolidToggle_OnClickEvent()
        {
            Debug.Log("solid");
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(false));
        }

        private void XRayToggle_OnClickEvent()
        {
            Debug.Log("x ray");
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(true));
        }

        private void HideButton_OnClickEvent()
        {
            Debug.Log("hide");
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(false));
        }

        private void SelectToggle_OnStateChangedEvent(bool on)
        {
            Debug.Log("select object " + on);
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(on));
        }

        private void BackButton_OnClickEvent()
        {
            Hide(() =>
            {
                mainToolbar.Show();
            });
        }
    }
}

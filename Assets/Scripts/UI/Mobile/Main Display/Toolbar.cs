using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.Mobile
{
    public class Toolbar : MonoBehaviour
    {
        [SerializeField] MainToolbar mainToolbar;
        [SerializeField] NavigationToolbar navigationToolbar;
        [SerializeField] ObjectInteractionToolbar objectInteractionToolbar;
        [SerializeField] CutawayToolbar cutawayToolbar;

        bool isToolbarEnabledFromPropertiesPanel;
        BaseToolbar currentlyActive;

        public bool IsToolbarEnabledFromPropertiesPanel { get => isToolbarEnabledFromPropertiesPanel; }

        private void Start()
        {
            currentlyActive = mainToolbar;
        }

        public void ShowFromPropertiesPanel()
        {
            isToolbarEnabledFromPropertiesPanel = true;
            ShowPanel();
        }

        public void HideFromPropertiesPanel()
        {
            isToolbarEnabledFromPropertiesPanel = false;
            HidePanel();
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);
            currentlyActive.Show();
        }

        public void HidePanel()
        {
            currentlyActive.Hide();
        }

        public void SetCurrentToolbar(BaseToolbar toolbar)
        {
            currentlyActive = toolbar;
        }
    }
}

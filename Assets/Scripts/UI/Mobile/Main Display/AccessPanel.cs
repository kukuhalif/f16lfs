using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class AccessPanel : MonoBehaviour
    {
        [SerializeField] InteractionToggle[] alwaysEnabledToggles;

        [SerializeField] PanelAnimation panelAnimation;

        [SerializeField] InteractionToggle[] toggles;

        [SerializeField] InteractionToggle hideUnhideToggle;

        [SerializeField] BaseContentPanel[] contents;

        InteractionToggle activeContent;

        List<InteractionToggle> detachedToggles = new List<InteractionToggle>();

        private void Start()
        {
            foreach (var toggle in toggles)
            {
                toggle.OnChangeToggleEvent += Toggle_OnChangeToggleEvent;
            }

            hideUnhideToggle.OnStateChangedEvent += HideUnhideButton_StateChangedEvent;

            // enable toggle from content callback
            for (int i = 0; i < toggles.Length; i++)
            {
                SetupEnableDisableContentCallback(contents[i], toggles[i]);
            }

            hideUnhideToggle.gameObject.SetActive(false);
            EventManager.AddListener<SetSchematicDataEvent>(MateriEventListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        private void OnDestroy()
        {
            foreach (var toggle in toggles)
            {
                toggle.OnChangeToggleEvent -= Toggle_OnChangeToggleEvent;
            }

            hideUnhideToggle.OnStateChangedEvent -= HideUnhideButton_StateChangedEvent;

            EventManager.RemoveListener<SetSchematicDataEvent>(MateriEventListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void OnDisable()
        {
            hideUnhideToggle.gameObject.SetActive(false);
        }

        private void EnableHideUnhide()
        {
            hideUnhideToggle.OnStateChangedEvent -= HideUnhideButton_StateChangedEvent;
            hideUnhideToggle.IsOn = true;
            hideUnhideToggle.OnStateChangedEvent += HideUnhideButton_StateChangedEvent;
        }

        private void DisableHideUnhide()
        {
            hideUnhideToggle.OnStateChangedEvent -= HideUnhideButton_StateChangedEvent;
            hideUnhideToggle.IsOn = false;
            hideUnhideToggle.OnStateChangedEvent += HideUnhideButton_StateChangedEvent;
        }

        private void MateriEventListener(SetSchematicDataEvent e)
        {
            if (activeContent != null && e.schematics == null)
            {
                bool disable = true;
                foreach (var always in alwaysEnabledToggles)
                {
                    if (always == activeContent)
                        disable = false;
                }

                if (disable)
                {
                    hideUnhideToggle.gameObject.SetActive(false);
                    DisableHideUnhide();
                }
            }
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            if (activeContent != null)
            {
                bool disable = true;
                foreach (var always in alwaysEnabledToggles)
                {
                    if (always == activeContent)
                        disable = false;
                }

                if (disable)
                {
                    hideUnhideToggle.gameObject.SetActive(false);
                    DisableHideUnhide();
                }
            }
        }

        IEnumerator SetToggleActiveDelayed(InteractionToggle toggle)
        {
            yield return null;
            toggle.IsOn = true;
        }

        private void SetupEnableDisableContentCallback(BaseContentPanel contentPanel, InteractionToggle toggle)
        {
            contentPanel.SetupEnableDisableCallback((bool showImmediately) =>
            {
                if (!toggle.gameObject.activeInHierarchy)
                {
                    // enable panel callback
                    if (showImmediately)
                    {
                        toggle.gameObject.SetActive(true);
                        StartCoroutine(SetToggleActiveDelayed(toggle));
                    }
                    else
                    {
                        toggle.gameObject.SetActive(true);
                    }
                }

            }, () =>
            {
                if (toggle.gameObject.activeInHierarchy)
                {
                    // disable panel callback
                    if (contentPanel.gameObject.activeInHierarchy)
                    {
                        contentPanel.HidePanel(() =>
                        {
                            toggle.gameObject.SetActive(false);
                        });
                    }
                    else
                    {
                        toggle.gameObject.SetActive(false);
                    }
                }
                else
                {
                    if (toggle.IsOn)
                        toggle.IsOn = false;

                    toggle.gameObject.SetActive(false);
                }
            }, (bool detached) =>
            {
                // detached panel callback, true = detached, false = attached
                if (detached)
                {
                    detachedToggles.Add(toggle);
                }
                else
                {
                    detachedToggles.Remove(toggle);

                    toggle.IsOn = false;

                    //bool activeContentIsDetached = false;
                    //for (int i = 0; i < detachedToggles.Count; i++)
                    //{
                    //    if (activeContent == detachedToggles[i])
                    //    {
                    //        activeContentIsDetached = true;
                    //    }
                    //}

                    //if (activeContent == toggle)
                    //{
                    //    activeContent.IsOn = true;
                    //}
                    //else if (activeContent.IsOn && !activeContentIsDetached)
                    //{
                    //   activeContent.IsOn = false;
                    //}

                    //activeContent = toggle;
                }
            });
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);
            panelAnimation.FadeIn();
        }

        private void Toggle_OnChangeToggleEvent(InteractionToggle toggle)
        {
            if (toggle.IsOn)
            {
                hideUnhideToggle.gameObject.SetActive(true);
                EnableHideUnhide();

                bool toggleDetached = true;

                // is panel always detached ignore previously active panel
                bool alwaysDetached = false;
                for (int i = 0; i < toggles.Length; i++)
                {
                    if (toggle == toggles[i])
                    {
                        if (contents[i].AlwaysDetached)
                        {
                            alwaysDetached = true;
                            contents[i].ShowPanel();

                            if (activeContent == null)
                                activeContent = toggle;
                        }

                        break;
                    }
                }

                if (!alwaysDetached)
                {
                    if (activeContent != null && activeContent.IsOn && activeContent != toggle)
                    {
                        for (int i = 0; i < detachedToggles.Count; i++)
                        {
                            if (activeContent == detachedToggles[i])
                                toggleDetached = false;
                        }

                        if (toggleDetached)
                            activeContent.IsOn = false;
                    }

                    for (int i = 0; i < toggles.Length; i++)
                    {
                        if (toggle == toggles[i])
                        {
                            activeContent = toggles[i];
                            contents[i].ShowPanel();
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < toggles.Length; i++)
                {
                    if (toggle == toggles[i])
                    {
                        contents[i].HidePanel();
                    }
                }
            }
        }

        public void HidePanel(Action onComplete)
        {
            panelAnimation.FadeOut(() =>
            {
                gameObject.SetActive(false);
                onComplete.Invoke();

                foreach (var toggle in toggles)
                {
                    bool disableToggle = true;
                    foreach (var alwaysEnabledToggle in alwaysEnabledToggles)
                    {
                        if (alwaysEnabledToggle == toggle)
                            disableToggle = false;
                    }

                    if (disableToggle)
                        toggle.gameObject.SetActive(false);
                }
            });

            if (activeContent != null)
            {
                activeContent.IsOn = false;
            }

            while (detachedToggles.Count > 0)
            {
                detachedToggles[0].IsOn = false;
            }
        }

        private void HideUnhideButton_StateChangedEvent(bool on)
        {
            if (activeContent == null)
                return;

            activeContent.IsOn = on;
        }
    }
}

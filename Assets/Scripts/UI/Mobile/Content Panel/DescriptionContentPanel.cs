using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class DescriptionContentPanel : BaseContentPanel
    {
        [SerializeField] MobileUIManager mobileUIManager;
        [SerializeField] TEXDraw title;
        [SerializeField] TextLink textLink;
        [SerializeField] TEXDraw description;
        [SerializeField] ScrollRect scrollRect;
        [SerializeField] InteractionButton previousMateriButton;
        [SerializeField] InteractionButton nextMateriButton;

        // cache
        private MateriTreeElement currentMateriTreeElement;
        private List<MateriTreeElement> materies = new List<MateriTreeElement>();

        protected override void Start()
        {
            base.Start();

            // cache materi list
            var materiTree = DatabaseManager.GetMateriTree();
            TreeElementUtility.TreeToList(materiTree, materies);

            // remove empty materi data
            List<MateriTreeElement> toRemove = new List<MateriTreeElement>();
            for (int i = 0; i < materies.Count; i++)
            {
                if (materies[i].depth < 0)
                    toRemove.Add(materies[i]);
                else if (materies[i].isFolder)
                    toRemove.Add(materies[i]);

                if (materies[i].MateriData.deviceMode == DeviceMode.VrOnly)
                    toRemove.Add(materies[i]);
            }
            for (int i = 0; i < toRemove.Count; i++)
            {
                materies.Remove(toRemove[i]);
            }
            // ---------------------------------

            EventManager.AddListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            previousMateriButton.OnClickEvent += PrevMateri;
            nextMateriButton.OnClickEvent += NextMateri;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            previousMateriButton.OnClickEvent -= PrevMateri;
            nextMateriButton.OnClickEvent -= NextMateri;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClearUI();
            DisableAccessPanelTab();
        }

        private void DescriptionEventListener(DescriptionPanelEvent e)
        {
            title.text = e.materiTreeElement.MateriData.name;
            description.text = e.materiTreeElement.MateriData.description;

            textLink.SetContentData(e.materiTreeElement);

            currentMateriTreeElement = e.materiTreeElement;

            scrollRect.verticalNormalizedPosition = 0f;

            EnableAccessPanelTab(true);

            mobileUIManager.StartCoroutine(TriggerMateriUIEvent(e));
        }

        IEnumerator TriggerMateriUIEvent(DescriptionPanelEvent e)
        {
            yield return null;

            switch (e.materiTreeElement.MateriData.elementType)
            {
                case MateriElementType.Materi:

                    break;

                case MateriElementType.Maintenance:

                    EventManager.TriggerEvent(new MaintenanceUIEvent(currentMateriTreeElement.MateriData));

                    break;

                case MateriElementType.Troubleshoot:

                    EventManager.TriggerEvent(new TroubleshootUIEvent(currentMateriTreeElement.MateriData));

                    break;

                case MateriElementType.RemoveInstall:

                    EventManager.TriggerEvent(new RemoveInstallUIEvent(currentMateriTreeElement.MateriData));

                    break;

                case MateriElementType.FlightScenario:

                    EventManager.TriggerEvent(new FlightScenarioUIEvent(currentMateriTreeElement.MateriData));

                    break;
            }

            if (e.materiTreeElement.MateriData.schematics.Count > 0)
                EventManager.TriggerEvent(new SetSchematicDataEvent(e.materiTreeElement.MateriData.schematics));

            while (scrollRect.verticalNormalizedPosition < 1f)
            {
                yield return null;
                scrollRect.verticalNormalizedPosition = 1f;
            }

            yield return null;
            description.gameObject.SetActive(false);
            yield return null;
            description.gameObject.SetActive(true);

            yield return null;
            scrollRect.CalculateLayoutInputHorizontal();
            scrollRect.CalculateLayoutInputVertical();
        }

        protected override void AfterAttached()
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(RefreshScrollRect());
        }

        protected override void AfterDetached()
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(RefreshScrollRect());
        }

        IEnumerator RefreshScrollRect()
        {
            yield return null;

            scrollRect.CalculateLayoutInputHorizontal();
            scrollRect.CalculateLayoutInputVertical();

            description.gameObject.SetActive(false);
            yield return null;
            description.gameObject.SetActive(true);
        }

        private void ClearUI()
        {
            currentMateriTreeElement = null;
            title.text = "";
            description.text = "";
        }

        private int GetCurrentMateriIndex()
        {
            for (int i = 0; i < materies.Count; i++)
            {
                if (currentMateriTreeElement.id == materies[i].id)
                    return i;
            }

            return -1;
        }

        private void PrevMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index--;

            if (index > -1)
            {
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[index]));
            }
        }

        private void NextMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index++;

            if (index < materies.Count)
            {
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[index]));
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using System.Linq;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class FlightScenarioContentPanel : BaseContentPanel
    {
        [SerializeField] TreeViewUIFlightScenario treeViewFlightScenario;

        [SerializeField] RectTransform itemContentRect;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<FlightScenarioUIEvent>(FlightScenarioUIEventListener);

            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<FlightScenarioUIEvent>(FlightScenarioUIEventListener);
        }

        private void OnEnable()
        {
            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            treeViewFlightScenario.ClearElements();
        }

        private void FlightScenarioUIEventListener(FlightScenarioUIEvent e)
        {
            if (e.materi == null)
            {
                treeViewFlightScenario.ClearElements();
                DisableAccessPanelTab();
                return;
            }

            if (e.materi.flightScenarioTree == null)
            {
                treeViewFlightScenario.ClearElements();
                Debug.LogError("flight scenario database missing");
                DisableAccessPanelTab();
                return;
            }

            // get flight scenario tree root from flight scenario tree element list
            FlightScenarioTreeElement root = TreeElementUtility.ListToTree(e.materi.flightScenarioTree.GetData().treeElements);

            // generate tree view
            treeViewFlightScenario.GenerateTree(root);

            // show flight scenario panel
            EnableAccessPanelTab();
        }
    }
}

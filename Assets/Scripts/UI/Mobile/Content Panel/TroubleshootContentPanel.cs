using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class TroubleshootContentPanel : BaseContentPanel
    {
        [SerializeField] GameObject prefabCanvastTroubleshoot;
        [SerializeField] Transform troubleshootPrefabParent;

        GameObject currentCanvasT;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<TroubleshootUIEvent>(TroubleshootListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<TroubleshootUIEvent>(TroubleshootListener);
        }

        private void TroubleshootListener(TroubleshootUIEvent e)
        {
            if (e.materi != null)
            {
                if (e.materi.troubleshootData.troubleshootGraph != null && currentCanvasT == null)
                {
                    currentCanvasT = Instantiate(prefabCanvastTroubleshoot, troubleshootPrefabParent);
                    currentCanvasT.transform.SetSiblingIndex(1);
                    var tg = currentCanvasT.GetComponent<TroubleshootGenerator>();
                    tg.troubleshootData = e.materi.troubleshootData;
                    tg.Initialize();
                    currentCanvasT.SetActive(true);
                }

                if (e.materi.troubleshootData.troubleshootGraph == null)
                {
                    Debug.LogError("troubleshoot database missing");
                    DisableAccessPanelTab();
                    return;
                }

                EnableAccessPanelTab();
            }
            else
            {
                if (currentCanvasT != null)
                {
                    Destroy(currentCanvasT);
                }

                DisableAccessPanelTab();
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.Mobile
{
    public abstract class BaseContentPanel : MonoBehaviour
    {
        [SerializeField] InteractionButton detachButton;
        [SerializeField] GameObject detachedControlButtons;
        [SerializeField] InteractionButton minimizeButton;
        [SerializeField] DetachedPanel detachedPanel;
        [SerializeField] Transform contentParent;
        [SerializeField] RectTransform titlePanel;
        [SerializeField] RectTransform contentPanel;
        [SerializeField] bool alwaysDetached;

        PanelAnimation panelAnimation;
        Action<bool> enablePanelCallback;
        Action disablePanelCallback;
        Action<bool> detachedCallback;

        bool detachedPanelInitialized = false;
        bool isPanelDetached = false;

        public bool AlwaysDetached => alwaysDetached;

        protected virtual void Start()
        {
            panelAnimation = GetComponent<PanelAnimation>();

            if (detachButton != null)
                detachButton.OnClickEvent += DetachButton_OnClickEvent;
            if (minimizeButton != null)
                minimizeButton.OnClickEvent += AttachButton_OnClickEvent;

            if (detachedPanel != null && !detachedPanelInitialized)
            {
                StartCoroutine(InitializeDetachedPanel());
            }
            else if (detachedPanel == null)
            {
                StartCoroutine(DelayedDisable());
            }
        }

        IEnumerator InitializeDetachedPanel()
        {
            detachedPanel.Detach(this);
            detachedPanel.SetTitlePanel(titlePanel);
            detachedPanel.transform.parent.gameObject.SetActive(true);

            yield return null;
            detachedPanel.ClosePanel();
            RectTransform content = contentPanel;
            content.SetParent(contentParent);
            content.SetLeft(0);
            content.SetTop(0);
            content.SetRight(0);
            content.SetBottom(0);
            content.localScale = Vector3.one;

            yield return null;
            gameObject.SetActive(false);
        }

        IEnumerator DelayedDisable()
        {
            yield return null;
            gameObject.SetActive(false);
        }

        protected virtual void OnDestroy()
        {
            if (detachButton != null)
                detachButton.OnClickEvent -= DetachButton_OnClickEvent;
            if (minimizeButton != null)
                minimizeButton.OnClickEvent -= AttachButton_OnClickEvent;
        }

        private void DetachButton_OnClickEvent()
        {
            panelAnimation.FadeOut();
            detachedPanel.Detach(this);

            detachedControlButtons.SetActive(true);
            detachButton.gameObject.SetActive(false);

            isPanelDetached = true;
            detachedCallback.Invoke(true);

            AfterDetached();
        }

        protected virtual void AfterDetached()
        {

        }

        private void AttachButton_OnClickEvent()
        {
            detachedControlButtons.SetActive(false);
            detachButton.gameObject.SetActive(true);

            detachedPanel.ClosePanel();

            RectTransform content = contentPanel;
            content.SetParent(contentParent);
            content.transform.SetAsFirstSibling();
            content.SetLeft(0);
            content.SetTop(0);
            content.SetRight(0);
            content.SetBottom(0);
            content.localScale = Vector3.one;

            //panelAnimation.FadeIn();

            isPanelDetached = false;
            detachedCallback.Invoke(false);

            AfterAttached();
        }

        protected virtual void AfterAttached()
        {

        }

        public RectTransform GetContentPanel()
        {
            return contentPanel;
        }

        public void SetupEnableDisableCallback(Action<bool> enablePanelCallback, Action disablePanelCallback, Action<bool> detachedCallback)
        {
            this.enablePanelCallback = enablePanelCallback;
            this.disablePanelCallback = disablePanelCallback;
            this.detachedCallback = detachedCallback;
        }

        public void ShowPanel(Action onComplete = null)
        {
            if (isPanelDetached)
                return;

            if (alwaysDetached)
            {
                detachedPanel.Detach(this);

                detachedControlButtons.SetActive(true);
                detachButton.gameObject.SetActive(false);

                isPanelDetached = true;
                detachedCallback.Invoke(true);

                AfterDetached();

                return;
            }

            gameObject.SetActive(true);
            panelAnimation.FadeIn(onComplete);
        }

        public void HidePanel(Action onComplete = null)
        {
            if (alwaysDetached)
            {
                detachedControlButtons.SetActive(false);
                detachButton.gameObject.SetActive(true);

                detachedPanel.ClosePanel();

                isPanelDetached = false;
                detachedCallback.Invoke(false);

                AfterAttached();

                return;
            }

            if (isPanelDetached)
                AttachButton_OnClickEvent();

            if (gameObject.activeInHierarchy)
            {
                panelAnimation.FadeOut(() =>
                {
                    gameObject.SetActive(false);
                    onComplete?.Invoke();
                });
            }
            else
                onComplete?.Invoke();
        }

        protected void EnableAccessPanelTab(bool showImmediately = false)
        {
            enablePanelCallback?.Invoke(showImmediately);
        }

        protected void DisableAccessPanelTab()
        {
            disablePanelCallback?.Invoke();
        }
    }
}

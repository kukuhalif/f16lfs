using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class SchematicContentPanel : BaseContentPanel
    {
        [SerializeField] TreeViewUISchematic treeView;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<SetSchematicDataEvent>(SetSchematicDataListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<SetSchematicDataEvent>(SetSchematicDataListener);
        }

        private void SetSchematicDataListener(SetSchematicDataEvent e)
        {
            treeView.SetSchematicData(e.schematics);

            if (e.schematics != null && e.schematics.Count > 0)
                EnableAccessPanelTab();
            else
                DisableAccessPanelTab();
        }
    }
}


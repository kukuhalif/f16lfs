using System.Collections;
using System.Collections.Generic;
using TMPro;
using System.Linq;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class MaintenanceContentPanel : BaseContentPanel
    {
        [SerializeField] TreeViewUIMaintenance treeViewMaintenance;

        [SerializeField] RectTransform itemContentRect;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<MaintenanceUIEvent>(MaintenanceUIEventListener);

            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
        }

        private void OnEnable()
        {
            EventManager.TriggerEvent(new MaintenancePanelUpdatedEvent());
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            treeViewMaintenance.ClearElements();
        }

        private void MaintenanceUIEventListener(MaintenanceUIEvent e)
        {
            if (e.materi == null)
            {
                treeViewMaintenance.ClearElements();
                DisableAccessPanelTab();
                return;
            }

            if (e.materi.maintenanceTree == null)
            {
                treeViewMaintenance.ClearElements();
                Debug.LogError("maintenance database missing");
                DisableAccessPanelTab();
                return;
            }

            // get maintenance tree root from maintenance tree element list
            MaintenanceTreeElement root = TreeElementUtility.ListToTree(e.materi.maintenanceTree.GetData().treeElements);

            // generate tree view
            treeViewMaintenance.GenerateTree(root);

            // show maintenance panel
            EnableAccessPanelTab();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class PDFMateriContentPanel : BaseContentPanel
    {
        [SerializeField] TreeViewUIPDFMateri pdfTreeView;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<MateriEvent>(MateriListener);
            EventManager.AddListener<FreePlayEvent>(FreeplayListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<MateriEvent>(MateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreeplayListener);
        }

        private void MateriListener(MateriEvent e)
        {
            if (e.materiTreeElement.MateriData.pdfs != null && e.materiTreeElement.MateriData.pdfs.Count > 0)
            {
                pdfTreeView.GeneratePdf(e.materiTreeElement.MateriData.pdfs);
                EnableAccessPanelTab();
            }
            else
            {
                DisableAccessPanelTab();
            }
        }

        private void FreeplayListener(FreePlayEvent e)
        {
            DisableAccessPanelTab();
        }
    }
}


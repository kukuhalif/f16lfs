using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.UI.Mobile
{
    public class ObjectListContentPanel : BaseContentPanel
    {
        [SerializeField] TreeViewUIObjectList treeView;
        [SerializeField] InteractionButton resetToggleButton;
        //[SerializeField] TextMeshProUGUI[] titles;

        bool firstStart = true;
        bool resetToggles = false;
        bool firstClose = true;

        protected override void Start()
        {
            base.Start();

            //EventManager.AddListener<ObjectInteractionShowPartObjectListEvent>(ObjectInteractionListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            resetToggleButton.OnClickEvent += treeView.ResetAllToggles;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            //EventManager.RemoveListener<ObjectInteractionShowPartObjectListEvent>(ObjectInteractionListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);

            resetToggleButton.OnClickEvent -= treeView.ResetAllToggles;
        }

        //protected override void ApplyTheme()
        //{
        //    base.ApplyTheme();

        //    for (int i = 0; i < titles.Length; i++)
        //    {
        //        titles[i].color = theme.genericTextColor;
        //    }
        //}

        //protected override void PanelSizeUpdatedCallback()
        //{
        //    EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        //}

        private void PlayMateriListener(MateriEvent e)
        {
            resetToggles = true;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            resetToggles = true;
        }

        //private void ObjectInteractionListener(ObjectInteractionShowPartObjectListEvent e)
        //{
        //    if (e.on)
        //    {
        //        resetToggles = true;
        //        EventManager.TriggerEvent(new ResetPartObjectEvent());
        //        ShowPanel();
        //    }
        //    else
        //        ClosePanel();
        //}

        private void OnEnable()
        {
            EventManager.TriggerEvent(new ObjectInteractionShowObjectListEvent(true));

            if (resetToggles)
            {
                treeView.ResetAllToggles();
                EventManager.TriggerEvent(new ResetPartObjectEvent());
                resetToggles = false;
            }

            EventManager.TriggerEvent(new ObjectListPanelEvent(true));

            if (firstStart)
            {
                firstStart = false;
                StartCoroutine(treeView.RefreshAtStartUiElement());
            }

            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        private void OnDisable()
        {
            EventManager.TriggerEvent(new ObjectListPanelEvent(false));

            resetToggles = true;

            if (!firstClose)
                EventManager.TriggerEvent(new ResetPartObjectEvent());

            firstClose = false;
        }
    }
}

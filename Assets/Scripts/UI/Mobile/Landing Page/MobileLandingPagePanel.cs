using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.UI.Mobile
{
    public class MobileLandingPagePanel : UIElement
    {
        [SerializeField] private TextMeshProUGUI projectTitleText;
        [SerializeField] private TextMeshProUGUI projectDescriptionText;
        [SerializeField] private InteractionButton playButton;
        [SerializeField] private InteractionButton VrButton;
        [SerializeField] private InteractionButton settingButton;
        [SerializeField] private InteractionButton helpButton;
        [SerializeField] private InteractionButton aboutButton;
        [SerializeField] private InteractionButton exitButton;
        [SerializeField] private InteractionButton networkingButton;
        [SerializeField] private PanelAnimation panelAnimation;
        [SerializeField] private MainDisplay mainDisplay;
        [SerializeField] private MobileAboutPanel about;
        [SerializeField] private MobileHelpPanel help;
        [SerializeField] private MobileSettingPanel settingPanel;
        [SerializeField] private RenderTexture showcaseRenderTexture;

        bool showFirstLandingPage = true;

        protected override void Start()
        {
            base.Start();

            if (projectTitleText != null)
                projectTitleText.text = Application.productName;
            if (projectDescriptionText != null)
                projectDescriptionText.text = DatabaseManager.GetProjectDetails().projectDescription;

            playButton.OnClickEvent += PlayButton_OnClickEvent;
            settingButton.OnClickEvent += SettingButton_OnClickEvent;
            helpButton.OnClickEvent += HelpButton_OnClickEvent;
            aboutButton.OnClickEvent += AboutButton_OnClickEvent;
            exitButton.OnClickEvent += ExitButton_OnClickEvent;
            networkingButton.OnClickEvent += NetworkingButton_OnClickEvent;
            if (VrButton != null)
                VrButton.OnClickEvent += VrButton_OnClickEvent;

            EventManager.AddListener<UnloadNetworkingSceneEvent>(UnloadNetworkingListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            playButton.OnClickEvent -= PlayButton_OnClickEvent;
            settingButton.OnClickEvent -= SettingButton_OnClickEvent;
            helpButton.OnClickEvent -= HelpButton_OnClickEvent;
            aboutButton.OnClickEvent -= AboutButton_OnClickEvent;
            exitButton.OnClickEvent -= ExitButton_OnClickEvent;
            networkingButton.OnClickEvent -= NetworkingButton_OnClickEvent;
            if (VrButton != null)
                VrButton.OnClickEvent -= VrButton_OnClickEvent;

            EventManager.RemoveListener<UnloadNetworkingSceneEvent>(UnloadNetworkingListener);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (showFirstLandingPage)
            {
                EventManager.TriggerEvent(new LandingPageEnabledEvent(true));

                showFirstLandingPage = false;
            }
            else
            {
                EventManager.AddListener<CameraArriveEvent>(OnArriveCameraListener);
            }
        }

        private void OnDisable()
        {
            if (showcaseRenderTexture != null)
                showcaseRenderTexture.Release();

            if (!showFirstLandingPage)
                EventManager.RemoveListener<CameraArriveEvent>(OnArriveCameraListener);

            EventManager.TriggerEvent(new LandingPageEnabledEvent(false));
        }

        private void UnloadNetworkingListener(UnloadNetworkingSceneEvent e)
        {
            mainDisplay.HidePanel(() =>
            {
                ShowPanel();
                EventManager.TriggerEvent(new LandingPageEnabledEvent(true));
            });
        }

        private void OnArriveCameraListener(CameraArriveEvent e)
        {
            EventManager.TriggerEvent(new LandingPageEnabledEvent(true));
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            if (projectTitleText != null)
                projectTitleText.color = theme.panelTitleTextColor;
            if (projectDescriptionText != null)
                projectDescriptionText.color = theme.panelTitleTextColor;
        }

        public void ShowPanel()
        {
            transform.parent.gameObject.SetActive(true);
            panelAnimation.FadeIn();
        }

        public void HidePanel(Action onComplete)
        {
            VirtualTrainingCamera.ResetCameraPosition(null);

            panelAnimation.FadeOut(() =>
            {
                transform.parent.gameObject.SetActive(false);
                onComplete.Invoke();
            });
        }

        private void ExitButton_OnClickEvent()
        {
            Debug.Log("exit");
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to exit?", () => { EventManager.TriggerEvent(new QuitAppEvent()); }, null));
        }

        private void AboutButton_OnClickEvent()
        {
            Debug.Log("about");
            about.ShowPanel();
        }

        private void HelpButton_OnClickEvent()
        {
            Debug.Log("help");
            //help.ShowPanel();
            EventManager.TriggerEvent(new ShowPdfEvent(DatabaseManager.GetProjectDetails().helpPDF));
        }

        private void SettingButton_OnClickEvent()
        {
            Debug.Log("setting");
            settingPanel.ShowPanel();
        }

        private void PlayButton_OnClickEvent()
        {
            Debug.Log("play");
            HidePanel(() =>
            {
                mainDisplay.ShowPanel();
            });
        }

        private void NetworkingButton_OnClickEvent()
        {
            Debug.Log("networking");
            HidePanel(() =>
            {
                mainDisplay.ShowPanel();
                VirtualTrainingSceneManager.OpenNetworking();
            });
        }

        private void VrButton_OnClickEvent()
        {
            Debug.Log("vr");
            VirtualTrainingSceneManager.LoadVRScene();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class MobileAboutPanel : MonoBehaviour
    {
        [SerializeField] PanelAnimation panelAnimation;
        [SerializeField] InteractionButton closeButton;
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] Image textBackground;
        [SerializeField] Image aboutIcon;

        private void Start()
        {
            text.text = DatabaseManager.GetProjectDetails().about;

            if (DatabaseManager.GetProjectDetails().aboutIcon != null)
            {
                aboutIcon.sprite = DatabaseManager.GetProjectDetails().aboutIcon;
                AspectRatioFitter aspectRatioFitter = aboutIcon.GetComponent<AspectRatioFitter>();
                aspectRatioFitter.aspectRatio = (float)aboutIcon.sprite.texture.width / (float)aboutIcon.sprite.texture.height;
            }
            else
                aboutIcon.gameObject.SetActive(false);  

            closeButton.OnClickEvent += CloseButton_OnClickEvent;
        }

        private void OnDestroy()
        {
            closeButton.OnClickEvent -= CloseButton_OnClickEvent;
        }

        private void CloseButton_OnClickEvent()
        {
            HidePanel();
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);
            panelAnimation.FadeIn();
        }

        public void HidePanel()
        {
            panelAnimation.FadeOut(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}
using Paroxe.PdfRenderer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class MobileHelpPanel : UIElement
    {
        [SerializeField] PanelAnimation panelAnimation;
        [SerializeField] InteractionButton closeButton;
        [SerializeField] PDFViewer pdfViewer;

        [SerializeField] Text[] titleTexts;
        [SerializeField] Text[] genericText;
        [SerializeField] Image[] contentTitle;
        [SerializeField] Image[] contentBackground;
        [SerializeField] Image sideBar;
        [SerializeField] Image title;
        [SerializeField] Image body;

        protected override void Start()
        {
            base.Start();

            pdfViewer.LoadDocumentFromAsset(DatabaseManager.GetProjectDetails().helpPDF, 0);

            closeButton.OnClickEvent += CloseButton_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            closeButton.OnClickEvent -= CloseButton_OnClickEvent;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            for (int i = 0; i < titleTexts.Length; i++)
            {
                titleTexts[i].color = theme.panelTitleTextColor;
            }

            for (int i = 0; i < genericText.Length; i++)
            {
                genericText[i].color = theme.genericTextColor;
            }

            for (int i = 0; i < contentTitle.Length; i++)
            {
                contentTitle[i].color = theme.panelContentColor;
            }

            for (int i = 0; i < contentBackground.Length; i++)
            {
                contentBackground[i].color = theme.panelContentColor;
            }

            sideBar.color = theme.panelContentColor;

            title.color = theme.panelTitleColor;
            body.color = theme.panelContentColor;
        }

        private void CloseButton_OnClickEvent()
        {
            HidePanel();
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);
            panelAnimation.FadeIn();
        }

        public void HidePanel()
        {
            panelAnimation.FadeOut(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}

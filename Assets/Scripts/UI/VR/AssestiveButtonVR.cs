using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System;
using UnityEngine.UI;

namespace VirtualTraining.UI.VR
{
    public class AssestiveButtonVR : MonoBehaviour
    {
        [SerializeField] InteractionButton btnPullAPart;
        [SerializeField] InteractionButton btnMultipleSelection;
        [SerializeField] InteractionButton btnResetObject;
        [SerializeField] InteractionButton btnCutaway;
        [SerializeField] InteractionButton btnHide;
        [SerializeField] InteractionButton btnShow;
        [SerializeField] InteractionButton btnXray;

        [SerializeField] InteractionToggle tglPullApartInt;
        [SerializeField] InteractionToggle tglMultipleSelectionInt;
        [SerializeField] InteractionToggle tglCutaway;
        [SerializeField] InteractionToggle tglXray;

        [SerializeField] CutawayPanelVR cutawayPanel;

        bool isRight;
        bool isLeft;
        float speedScrollbar;

        // Start is called before the first frame update
        void Start()
        {
            btnPullAPart.OnPointerDownEvent += PullaPartPressed;
            btnMultipleSelection.OnPointerDownEvent += MultiplePressed;
            btnResetObject.OnPointerDownEvent += ResetPressed;
            btnCutaway.OnPointerDownEvent += CutawayPressed;
            btnHide.OnPointerDownEvent += HidePressed;
            btnShow.OnPointerDownEvent += ShowPressed;
            btnXray.OnPointerDownEvent += XrayPressed;

            tglCutaway.OnStateChangedEvent += CutawayToggleInteraction;
            tglPullApartInt.OnStateChangedEvent += PullapartToggleInteraction;
            tglMultipleSelectionInt.OnStateChangedEvent += MultipleToggleInteraction;
            tglXray.OnStateChangedEvent += XrayToggleInteraction;
        }

        private void OnDestroy()
        {
            btnPullAPart.OnPointerDownEvent -= PullaPartPressed;
            btnMultipleSelection.OnPointerDownEvent -= MultiplePressed;
            btnResetObject.OnPointerDownEvent -= ResetPressed;
            btnCutaway.OnPointerDownEvent -= CutawayPressed;
            btnHide.OnPointerDownEvent -= HidePressed;
            btnShow.OnPointerDownEvent -= ShowPressed;
            btnXray.OnPointerDownEvent -= XrayPressed;

            tglCutaway.OnStateChangedEvent -= CutawayToggleInteraction;
            tglPullApartInt.OnStateChangedEvent -= PullapartToggleInteraction;
            tglMultipleSelectionInt.OnStateChangedEvent -= MultipleToggleInteraction;
            tglXray.OnStateChangedEvent -= XrayToggleInteraction;
        }

        void PullaPartPressed()
        {
            tglPullApartInt.IsOn = !tglPullApartInt.IsOn;
        }
        void MultiplePressed()
        {
            tglMultipleSelectionInt.IsOn = !tglMultipleSelectionInt.IsOn;
        }
        void ResetPressed()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
        }

        void CutawayPressed()
        {
            tglCutaway.IsOn = !tglCutaway.IsOn;
        }

        void HidePressed()
        {
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(false));
        }
        void ShowPressed()
        {
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
        }
        void XrayPressed()
        {
            tglXray.IsOn = !tglXray.IsOn;
        }

        void PullapartToggleInteraction(bool isOn)
        {
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(isOn));
        }
        void MultipleToggleInteraction(bool isOn)
        {
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(isOn));
        }

        void CutawayToggleInteraction(bool isOn)
        {
            cutawayPanel.SetActivePanel(isOn);

            if (isOn == false)
            {
                //EventManager.TriggerEvent(new ResetCutawayEvent());
                EventManager.TriggerEvent(new ObjectInteractionCutawayEvent(isOn));
                Debug.Log("cutaway is off");
            }
            else
            {
                cutawayPanel.ResetPositionSlider();
            }
        }
        void XrayToggleInteraction(bool isOn)
        {
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(isOn));
        }

        public void AllResetToggle()
        {
            tglCutaway.IsOn = false;
            tglMultipleSelectionInt.IsOn = false;
            tglPullApartInt.IsOn = false;
            tglXray.IsOn = false;
        }
    }
}

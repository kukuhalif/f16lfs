using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.VR
{
    public class HeaderPanelVR : MonoBehaviour
    {
        public WorldSpacePanel worldSpacePanel;

        private void Start()
        {
            SetCollider();
        }
        public void SetCollider(RectTransform rect = null)
        {
            if (rect == null)
                rect = GetComponent<RectTransform>();

            BoxCollider boxColl = GetComponent<BoxCollider>();

            if (boxColl != null)
                boxColl.size = new Vector3(rect.rect.width, boxColl.size.y, 5);
            RectTransform rectt = GetComponent<RectTransform>();
            rectt.sizeDelta = new Vector2(rect.rect.width, rectt.sizeDelta.y);
        }

    }
}

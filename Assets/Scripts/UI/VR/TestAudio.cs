using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public class TestAudio : MonoBehaviour
{
    public AudioSourcePlayer audios;
    public AudioClip clip;

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.P)) ;
        {
            audios.PlayNaration(clip);
        }
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.O))
        {
            audios.PauseNaration();
        }

        if (Input.GetKey(KeyCode.I))
        {
            transform.Rotate(Vector3.down * Time.deltaTime * 14, Space.World);
        }

        if (Input.GetKey(KeyCode.U))
        {
            transform.Rotate(Vector3.up * Time.deltaTime * 14, Space.World);
        }
    }
}

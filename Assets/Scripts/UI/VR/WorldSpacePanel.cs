using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using DG.Tweening;
using UnityEditor;
using System;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.UI.VR
{
    public class WorldSpacePanel : UIElement
    {
        [SerializeField] bool isSystemPanel;
        [SerializeField] Vector3 position;
        public Vector3 getPosition { get => position; }
        Canvas canvas;
        public Transform cameraOrbit;
        public float positionZ = 1;
        RectTransform bodyPanel;
        CanvasGroup canvasGroup;
        float duration = 1f;
        GameObject fiturCont;
        Vector3 ueulerContainer;
        float ueulerDeltaPos;
        Vector3 cameraPosTemp;
        protected bool forMonitorCamera;
        public Canvas Canvas { get => canvas; }

        [SerializeField] Image panelHeader;
        [SerializeField] TextMeshProUGUI titleHeader;
        [SerializeField] Image panelContent;
        [SerializeField] bool itsConfirmationPanel;

        [SerializeField] public Vector3 defaultPosition = new Vector3(0, 1, 1);
        public bool isReseting;

        bool isActiveLast;
        protected override void Awake()
        {
            base.Awake();

            Init();
        }
        protected override void ApplyTheme()
        {
            if (panelHeader != null)
                panelHeader.color = theme.panelTitleColor;
            if (titleHeader != null)
                titleHeader.color = theme.panelTitleTextColor;
            if (panelContent != null)
                panelContent.color = theme.panelContentColor;
        }

        protected override void Start()
        {
            base.Start();
            ApplyTheme();
            EventManager.AddListener<InitializationUIDoneEvent>(InitUI);
            SetCollider();
        }

        void SetCollider()
        {
            if (panelHeader == null)
                return;

            RectTransform rect = GetComponent<RectTransform>();
            HeaderPanelVR headerPanelVR = panelHeader.GetComponent<HeaderPanelVR>();
            if (headerPanelVR != null)
            {
                headerPanelVR.SetCollider(rect);
            }
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<InitializationUIDoneEvent>(InitUI);
        }

        void InitUI(InitializationUIDoneEvent e)
        {
            SetActivePanel(false);
        }

        public void SetObjectCondiition()
        {
            if (this.gameObject.activeInHierarchy)
                isActiveLast = true;
            else
                isActiveLast = false;

            if (itsConfirmationPanel == false)
                SetActivePanel(false);
        }

        public void ResetToLastCondition()
        {
            if (itsConfirmationPanel == false)
                SetActivePanel(isActiveLast);
        }

        protected override void OnEnable()
        {
            StartCoroutine(DelaySetPosition(isReseting));
            isReseting = false;
        }

        void Init()
        {
            canvasGroup = transform.parent.GetComponentInParent<CanvasGroup>();
            cameraOrbit = (new GameObject()).transform;
            cameraOrbit.gameObject.name = "UI " + gameObject.name;

            if (canvasGroup != null)
                canvasGroup.alpha = 0;


            bodyPanel = transform.parent.GetComponent<RectTransform>();
            if (bodyPanel == null)
                bodyPanel = GetComponent<RectTransform>();

            UIControllerVR uIManager = FindObjectOfType<UIControllerVR>();
            uIManager.uiPanels.Add(cameraOrbit.gameObject);
            uIManager.worldSpacePanels.Add(this);
        }

        public void SettranformToPlayer(GameObject fiturContainer, GameObject systemPanelContainer)
        {
            if (VirtualTrainingCamera.IsVR)
                cameraOrbit.SetParent(VirtualTrainingCamera.CameraTransform.parent);

            cameraOrbit.localPosition = defaultPosition;
            cameraOrbit.rotation = Quaternion.identity;
            cameraOrbit.gameObject.name = "UI " + gameObject.name;

            canvas = transform.parent.GetComponentInParent<Canvas>();
            canvas.transform.SetParent(cameraOrbit);
            canvas.transform.localPosition = Vector3.zero;

            if (isSystemPanel)
                fiturCont = systemPanelContainer;
            else
                fiturCont = fiturContainer;

            if (fiturCont != null)
                cameraOrbit.transform.SetParent(fiturCont.transform);

            if (isSystemPanel)
                fiturCont = fiturContainer;
        }

        public void SetActivePanel(bool isActive, Action callback = null)
        {
            if (isActive)
            {
                this.transform.parent.parent.gameObject.SetActive(true);
                this.gameObject.SetActive(true);

                canvasGroup.DOFade(1, duration / 1f);
                bodyPanel.DOScale(1, duration).OnComplete(() =>
                {
                    this.transform.parent.parent.gameObject.SetActive(true);
                    PanelLookPlayer();
                });
                PanelLookPlayer();
                Debug.Log(GetComponentInParent<Transform>().gameObject.name + this.gameObject.name + " active");
            }
            else
            {
                canvasGroup.DOFade(0, duration / 2f);
                bodyPanel.DOScale(0, duration / 2).OnComplete(() =>
                 {
                     this.transform.parent.parent.gameObject.SetActive(false);

                     if (callback != null)
                         callback();
                     Debug.Log(GetComponentInParent<Transform>().gameObject.name + this.gameObject.name + " unactive");
                 });
            }
        }

        public void PanelLookPlayer()
        {
            if (cameraOrbit != null && VirtualTrainingCamera.CurrentCamera != null)
                cameraOrbit.LookAt(VirtualTrainingCamera.CurrentCamera.transform);
        }

        IEnumerator DelaySetPosition(bool isReset = false)
        {
            cameraPosTemp = Vector3.zero;
            if (fiturCont == null)
                yield break;

            ueulerContainer = fiturCont.transform.localEulerAngles;
            Debug.Log(ueulerDeltaPos + " ueulerDeltaPos " + gameObject.name);
            if (isReset)
            {
                ueulerDeltaPos = 0;
                cameraOrbit.localPosition = defaultPosition;
            }

            //if (forMonitorCamera)
            //    yield return new WaitForSeconds(1);

            //forMonitorCamera = false;


            fiturCont.transform.localEulerAngles = new Vector3(fiturCont.transform.localEulerAngles.x,
            VirtualTrainingCamera.CameraTransform.localEulerAngles.y - ueulerDeltaPos,
            fiturCont.transform.localEulerAngles.z);


            cameraPosTemp = cameraOrbit.position;

            ueulerDeltaPos = VirtualTrainingCamera.CameraTransform.localEulerAngles.y;
            fiturCont.transform.localEulerAngles = ueulerContainer;
            yield return new WaitForSeconds(.1f);

            cameraOrbit.position = cameraPosTemp;
            PanelLookPlayer();
        }

        public void DragUI(Vector3 deltaPosition)
        {
            cameraOrbit.position = new Vector3(deltaPosition.x, deltaPosition.y, deltaPosition.z);
            PanelLookPlayer();
        }

        public void ResetPosition()
        {
            isReseting = true;
            if (this.gameObject.activeInHierarchy)
            {
                StartCoroutine(DelaySetPosition(true));
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.SceneManagement;

namespace VirtualTraining.UI.VR
{
    public class UIControllerVR : MonoBehaviour
    {
        public List<WorldSpacePanel> worldSpacePanels = new List<WorldSpacePanel>();
        public List<GameObject> uiPanels = new List<GameObject>();

        public SystemPanelVR systemPanelVR;
        public DescriptionPanelVR descriptionPanelVR;
        public HelpPanelVR helpPanelVR;
        public SettingPanelVR settingPanelVR;
        public ConfirmationPanelVR confirmationPanelVR;

        public GameObject mainMenuVR;
        [SerializeField] float distanceToPlayer;

        [SerializeField] GameObject canvasContainer;
        private void Start()
        {
            EventManager.AddListener<OpenPanelVREvent>(OpenHideCanvas);
            EventManager.AddListener<ResetPanelVREvent>(ResetPositionDo);
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.AddListener<ConfirmationPanelEvent>(HideAllPanel);
            EventManager.AddListener<CloseConfirmationPanelEvent>(BackToLast);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<OpenPanelVREvent>(OpenHideCanvas);
            EventManager.RemoveListener<ResetPanelVREvent>(ResetPositionDo);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<ConfirmationPanelEvent>(HideAllPanel);
            EventManager.RemoveListener<CloseConfirmationPanelEvent>(BackToLast);

            SetBackPanel();
        }

        void SetBackPanel()
        {
            for (int i = 0; i < worldSpacePanels.Count; i++)
            {
                if (worldSpacePanels[i] != null)
                {
                    Transform canvasTemp = worldSpacePanels[i].transform.parent;
                    if (canvasContainer != null)
                        worldSpacePanels[i].transform.SetParent(canvasContainer.transform);
                    if (canvasTemp != null)
                        Destroy(canvasTemp.gameObject);
                }
            }
        }
        private void ApplySettingListener(ApplySettingEvent e)
        {
            distanceToPlayer = (e.data.vrMenuHeight / 10);
            if (mainMenuVR != null)
                mainMenuVR.transform.localPosition = new Vector3(0, 0, distanceToPlayer);
        }

        public void OpenHideCanvas(OpenPanelVREvent e)
        {
            if (e.enabled)
            {
                //EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to Open?", OpenConfirmation, null));
                OpenConfirmation();
            }
            else
            {
                //EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to Hide?", HideConfirmation, null));
                HideConfirmation();
            }
        }

        void OpenConfirmation()
        {
            for (int i = 0; i < uiPanels.Count; i++)
            {
                uiPanels[i].gameObject.SetActive(true);
            }
        }

        void HideConfirmation()
        {
            for (int i = 0; i < uiPanels.Count; i++)
            {
                uiPanels[i].gameObject.SetActive(false);
            }
        }

        public void SetAllPanel(GameObject fiturContainer, GameObject systemPanelContainer, GameObject mainMenuContainer)
        {
            for (int i = 0; i < worldSpacePanels.Count; i++)
            {
                worldSpacePanels[i].SettranformToPlayer(fiturContainer, systemPanelContainer);
            }

            mainMenuVR.transform.SetParent(mainMenuContainer.transform);
            mainMenuVR.transform.localPosition = new Vector3(0, 0, distanceToPlayer);
            MainMenuVR mainMenuVRs = mainMenuVR.GetComponentInChildren<MainMenuVR>();

            if(mainMenuVRs != null)
            {
                mainMenuVRs.allSystemPanel = systemPanelContainer;
            }
        }

        public void ResetPositionDo(ResetPanelVREvent e)
        {
            //featureInit.transform.localEulerAngles = new Vector3(featureInit.transform.localEulerAngles.x, VirtualTrainingCamera.CameraTransform.localEulerAngles.y, featureInit.transform.localEulerAngles.z);
            if (worldSpacePanels.Count > 0)
            {
                for (int i = 0; i < worldSpacePanels.Count; i++)
                {
                    worldSpacePanels[i].ResetPosition();
                }
            }
        }

        void HideAllPanel(ConfirmationPanelEvent e)
        {
            for (int i = 0; i < worldSpacePanels.Count; i++)
            {
                worldSpacePanels[i].SetObjectCondiition();
            }
        }

        void BackToLast(CloseConfirmationPanelEvent e)
        {
            for (int i = 0; i < worldSpacePanels.Count; i++)
            {
                worldSpacePanels[i].ResetToLastCondition();
            }
        }

    }
}

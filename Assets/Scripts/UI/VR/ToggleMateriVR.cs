using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI.VR
{

    public class ToggleMateriVR : MonoBehaviour
    {
        MateriTreeElement materiTreeElement;
        MateriTreeElement materiTreeElementParent;
        TestDummyVR testDummyVR;
        TextMeshProUGUI textTitle;
        InteractionToggle interactionToggle;
        ListUIVR listUIVR;

        [SerializeField] bool isMateri;
        [SerializeField] bool isSubMateri;
        [SerializeField] bool isSubSubMateri;

        [SerializeField] bool panel1;
        // Start is called before the first frame update

        private void Awake()
        {
            interactionToggle = GetComponent<InteractionToggle>();
            textTitle = GetComponentInChildren<TextMeshProUGUI>();
            testDummyVR = FindObjectOfType<TestDummyVR>();

            interactionToggle.OnStateChangedEvent += MateriClicked;
        }

        public void ResetToggle()
        {
            interactionToggle.IsOn = false;
        }

        public void SetMateri(MateriTreeElement materi, ListUIVR list, MateriTreeElement materiParent,bool isPanel1)
        {
            if (textTitle == null)
                textTitle = GetComponentInChildren<TextMeshProUGUI>();

            listUIVR = list;
            materiTreeElementParent = materiParent;
            materiTreeElement = materi;
            textTitle.text = materi.CompleteName;
            panel1 = isPanel1;
        }

        private void OnDestroy()
        {
            interactionToggle.OnStateChangedEvent -= MateriClicked;
        }

        void MateriClicked(bool Condiion)
        {
            if (Condiion)
            {
                //if (isMateri)
                //{
                //    testDummyVR.GoToPanel(textTitle.text, 0);
                //}
                //else if (isSubMateri)
                //{
                //    testDummyVR.GoToPanel(textTitle.text, 1);
                //}
                //else
                //{
                //    testDummyVR.GoToPanel(textTitle.text, 2);
                //}

                if (listUIVR != null)
                {
                    if (panel1)
                    {
                        listUIVR.currentMateri = materiTreeElement;
                        listUIVR.GeneratePanel2(materiTreeElement);
                        Debug.Log(materiTreeElement.name);
                    }
                    else
                    {
                        listUIVR.currentMateri = materiTreeElement;
                        listUIVR.GeneratePanel1(materiTreeElementParent);
                        listUIVR.GeneratePanel2(materiTreeElement);
                    }
                }
            }
        }
    }
}


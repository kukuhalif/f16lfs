using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.VR
{
    public class ListUIVR : MonoBehaviour
    {
        [SerializeField] ToggleMateriVR buttonMateri1Prefab;
        [SerializeField] ToggleMateriVR buttonMateri2Prefab;
        [SerializeField] Transform panelParent1;
        [SerializeField] Transform panelParent2;
        [SerializeField] InteractionToggleGroup toggleGroup1;
        [SerializeField] InteractionToggleGroup toggleGroup2;

        public MateriTreeElement currentMateri;

        

        private void Start()
        {
            GeneratePanel1(DatabaseManager.GetMateriTree());
        }

        public void GeneratePanel1(MateriTreeElement materiTree)
        {
            GeneratePanelBase(materiTree, panelParent1, buttonMateri1Prefab, toggleGroup1,true);
        }

        public void GeneratePanel2(MateriTreeElement materiTree)
        {
            GeneratePanelBase(materiTree, panelParent2, buttonMateri2Prefab, toggleGroup2,false);
        }

        void GeneratePanelBase(MateriTreeElement materiTree, Transform parents, ToggleMateriVR buttonPrefab, InteractionToggleGroup toggleGroup,bool isPanel1)
        {
            List<InteractionToggle> toggles = new List<InteractionToggle>();
            for (int i = 0; i < parents.childCount; i++)
            {
                parents.GetChild(i).gameObject.SetActive(false);
            }

            if (materiTree.hasChildren == true)
            {
                for (int i = 0; i < materiTree.children.Count; i++)
                {
                    ToggleMateriVR toggleMateriVRTemp; ;
                    if (parents.childCount > materiTree.children.Count)
                    {
                        toggleMateriVRTemp = parents.GetChild(i).GetComponent<ToggleMateriVR>();
                        InteractionToggle toggleTemps = toggleMateriVRTemp.GetComponent<InteractionToggle>();
                        toggleTemps.IsOn = false;

                    }
                    else
                    {
                        toggleMateriVRTemp = Instantiate(buttonPrefab.gameObject, parents.transform).GetComponent<ToggleMateriVR>();
                    }

                    MateriTreeElement materiTemp = materiTree.children[i] as MateriTreeElement;
                    toggleMateriVRTemp.SetMateri(materiTemp, this, materiTree, isPanel1);

                    InteractionToggle toggleTemp = toggleMateriVRTemp.GetComponent<InteractionToggle>();
                    toggles.Add(toggleTemp);

                    toggleGroup.Setup(toggles.ToArray());

                    if (currentMateri.id != 0)
                    {
                        if (currentMateri.id == materiTemp.id)
                        {
                            toggleTemp.IsOn = true;
                        }
                    }
                    else
                    {
                        if (i == 0 && isPanel1 == true)
                        {
                            Debug.Log(materiTemp.name);
                            toggleTemp.IsOn = true;
                        }
                    }

                    toggleMateriVRTemp.gameObject.SetActive(true);
                }
            }

            parents.gameObject.SetActive(false);
            parents.gameObject.SetActive(true);
        }
    }
}

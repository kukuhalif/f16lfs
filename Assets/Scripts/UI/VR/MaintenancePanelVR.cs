using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;
using UnityEditor;

namespace VirtualTraining.UI.VR
{
    public class MaintenancePanelVR : WorldSpacePanel
    {
        [SerializeField] TreeViewUIMaintenance treeViewMaintenance;
        bool isMaintenanceDataExist = false;
        [SerializeField] GameObject treeViewUI;

        protected override void Start()
        {
            defaultPosition = DatabaseManager.GetVRData().maintenancePosition.defaultPosition;
            base.Start();
            EventManager.AddListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
            StartCoroutine(DelayStart());
        }

        IEnumerator DelayStart()
        {
            yield return new WaitForSeconds(0.5f);
            treeViewUI.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            treeViewUI.SetActive(true);
            SetActivePanel(false);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            EventManager.TriggerEvent(new MaintenancePanelUpdatedEvent());
        }

        private void MaintenanceUIEventListener(MaintenanceUIEvent e)
        {
            if (e.materi == null)
            {
                isMaintenanceDataExist = false;
                SetActivePanel(false);
                treeViewMaintenance.ClearElements();
                return;
            }

            if (e.materi.maintenanceTree == null)
            {
                isMaintenanceDataExist = false;
                SetActivePanel(false);
                treeViewMaintenance.ClearElements();

                Debug.LogError("maintenance database missing");

                return;
            }

            isMaintenanceDataExist = true;

            // get maintenance tree root from maintenance tree element list
            MaintenanceTreeElement root = TreeElementUtility.ListToTree(e.materi.maintenanceTree.GetData().treeElements);

            // generate tree view
            treeViewMaintenance.GenerateTree(root);

            // show maintenance panel
            SetActivePanel(true);
        }

        public void DeactivePanel()
        {
            SetActivePanel(false);
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(MaintenancePanelVR))]
    public class OverrideMaintenance : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    MaintenancePanelVR panel = (MaintenancePanelVR)target;

                    vrData.maintenancePosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.maintenancePosition.defaultPosition;
                    DatabaseManager.SetVRData(vrData); ;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }

            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif
}

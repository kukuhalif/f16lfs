using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.UI
{
    public class InteractionButtonConfirmation : UIElement
    {
        public delegate void OnClick();
        static InteractionButtonConfirmation INSTANCE;
        [SerializeField] Image background;
        Action actions;
        RectTransform rectTransform;
        float timeDuration = 1f;
        float t;
        bool isController;

        bool isProgressing;
        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            if (isController == false)
                timeDuration = DatabaseManager.GetDefaultSetting().durationConfirmationButton;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            if (isController)
                timeDuration = e.data.durationConfirmationButton;
            Debug.Log(timeDuration + " times");
        }
        public static void Show(RectTransform source, Action actions)
        {
            if (INSTANCE == null)
            {
                GameObject go = (GameObject)Resources.Load("UI/Interaction Button Confirmation");
                GameObject instantiated = Instantiate(go);
                INSTANCE = instantiated.GetComponent<InteractionButtonConfirmation>();
                INSTANCE.rectTransform = instantiated.GetComponent<RectTransform>();
            }
            INSTANCE.Progress();
            INSTANCE.actions = actions;
            INSTANCE.Setup(source);
        }
        private void Update()
        {
            if (isProgressing)
            {
                //background.fillAmount += speed * Time.deltaTime;
                t += Time.deltaTime / timeDuration;
                background.fillAmount = Mathf.Lerp(0, 1, t);

                if (background.fillAmount >= 1)
                {
                    isProgressing = false;
                    t = 0;
                    ResetProgress();
                    if (actions != null)
                        actions?.Invoke();
                }
            }
        }

        void Progress()
        {
            t = 0;
            background.fillAmount = 0;
            isProgressing = true;
        }

        public static void ResetProgress()
        {
            if (INSTANCE == null)
                return;

            INSTANCE.isProgressing = false;
            INSTANCE.t = 0;
            Hide();
            if (INSTANCE.background != null)
                INSTANCE.background.fillAmount = 0;
        }

        public static void SetConfirmationDuration(float time)
        {
            if (INSTANCE == null)
            {
                GameObject go = (GameObject)Resources.Load("UI/Interaction Button Confirmation");
                GameObject instantiated = Instantiate(go);
                INSTANCE = instantiated.GetComponent<InteractionButtonConfirmation>();
                INSTANCE.rectTransform = instantiated.GetComponent<RectTransform>();
                INSTANCE.gameObject.SetActive(true);
                INSTANCE.gameObject.SetActive(false);
            }
            INSTANCE.isController = true;
            INSTANCE.timeDuration = time;
            Debug.Log(time + " time");
        }

        public static void Hide()
        {
            if (INSTANCE == null)
                return;

            INSTANCE.gameObject.SetActive(false);
        }

        private void Setup(RectTransform source)
        {
            rectTransform.SetParent(source);
            rectTransform.SetLeft(0f);
            rectTransform.SetRight(0f);
            rectTransform.SetTop(0f);
            rectTransform.SetBottom(0f);
            rectTransform.transform.localPosition = Vector3.zero;
            rectTransform.transform.localRotation = Quaternion.identity;
            gameObject.SetActive(true);
            rectTransform.localScale = Vector3.one;
        }

        protected override void ApplyTheme()
        {
            //background.color = theme.hoverCover;
        }
    }
}

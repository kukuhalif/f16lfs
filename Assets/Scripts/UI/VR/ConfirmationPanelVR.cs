using UnityEngine;
using TMPro;
using VirtualTraining.Core;
using System;
using DG.Tweening;
using UnityEditor;

namespace VirtualTraining.UI.VR
{
    public class ConfirmationPanelVR : WorldSpacePanel
    {
        [SerializeField] TextMeshProUGUI label;
        [SerializeField] InteractionButton yesButton;
        [SerializeField] InteractionButton noButton;

        Action onYesCallback;
        Action onNoCallback;

        [SerializeField] MainMenuVR mainMenuVR;

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            label.color= theme.panelTitleTextColor;
        }

        protected override void Start()
        {
            defaultPosition = DatabaseManager.GetVRData().confirmationPosition.defaultPosition;
            base.Start();
            yesButton.OnClickEvent += OnYes;
            noButton.OnClickEvent += OnNo;

            EventManager.AddListener<ConfirmationPanelEvent>(ConfirmationEventListener);
            //Init();
            GetComponentInParent<Transform>().gameObject.SetActive(false);
        }

        protected override void OnDestroy()
        {
            yesButton.OnClickEvent += OnYes;
            noButton.OnClickEvent += OnNo;

            EventManager.RemoveListener<ConfirmationPanelEvent>(ConfirmationEventListener);
        }

        private void OnNo()
        {
            onNoCallback?.Invoke();
            OnClosePanel();
        }

        private void OnYes()
        {
            onYesCallback?.Invoke();
            OnClosePanel();
        }

        private void OnClosePanel()
        {
            //this.gameObject.SetActive(false);
            SetActivePanel(false);
            if (mainMenuVR != null)
            {
                mainMenuVR.CloseConfirmationPanel();
                mainMenuVR.SetaActiveSystemPanel();
            }

            EventManager.TriggerEvent(new CloseConfirmationPanelEvent());
        }

        private void ConfirmationEventListener(ConfirmationPanelEvent e)
        {
            label.text = e.text;

            string yesText, noText;

            if (e.onYesCallback == null && e.onNoCallback == null)
            {
                yesText = "ok";
                noText = "";
            }
            else
            {
                yesText = "yes";
                noText = "no";
            }

            yesButton.SetText(yesText);
            onYesCallback = e.onYesCallback;

            if (e.onYesCallback == null && e.onNoCallback == null)
            {
                noButton.gameObject.SetActive(false);
            }
            else
            {
                noButton.gameObject.SetActive(true);
                onNoCallback = e.onNoCallback;
                noButton.SetText(noText);
            }
            this.gameObject.SetActive(true);
            SetActivePanel(true);
            //if (mainMenuVR != null)
                //mainMenuVR.SetaUnactiveSystemPanel();
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(ConfirmationPanelVR))]
    public class OverrideConfirmation: Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    ConfirmationPanelVR panel = (ConfirmationPanelVR)target;

                    vrData.confirmationPosition.defaultPosition = panel.gameObject.transform.parent.localPosition;
                    DatabaseManager.SetVRData(vrData); ;
                    Debug.Log("Save Position" + panel.gameObject.name+ " Success");
                }
            }

            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif
}

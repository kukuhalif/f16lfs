using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using VirtualTraining.Core;
using UnityEngine.EventSystems;

namespace VirtualTraining.UI.VR
{
    public class ToolbarPanelVR : MonoBehaviour
    {

        [SerializeField] InteractionButton resetPositionButton;
        [SerializeField] InteractionButton resetPanelButton;
        [SerializeField] InteractionButton hidePanelButton;
        [SerializeField] InteractionButton unhidePanelButton;

        // Start is called before the first frame update
        void Start()
        {
            resetPositionButton.OnPointerDownEvent += ResetPositionButton;
            resetPanelButton.OnPointerDownEvent += ResetPanelButton;

            hidePanelButton.OnPointerDownEvent += HideInteracted;
            unhidePanelButton.OnPointerDownEvent += UnhideInteracted;
        }

        private void OnDestroy()
        {
            resetPositionButton.OnPointerDownEvent -= ResetPositionButton;
            resetPanelButton.OnPointerDownEvent -= ResetPanelButton;

            hidePanelButton.OnPointerDownEvent -= HideInteracted;
            unhidePanelButton.OnPointerDownEvent -= UnhideInteracted;
        }
        void ResetPositionButton()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to Reset Position?", ResetPosition, null));
        }
        void ResetPosition()
        {
            VirtualTrainingCamera.ResetCameraPosition(RestPositionCallback);
        }

        void RestPositionCallback()
        {
            Debug.Log("Reset Position");
        }
        void ResetPanelButton()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to Reset All Panel Position?", ResetPanel, null));
            //ResetPanel();
        }

        void ResetPanel()
        {
            EventManager.TriggerEvent(new ResetPanelVREvent());
        }

        void HideInteracted()
        {
            EventManager.TriggerEvent(new OpenPanelVREvent(false));
        }

        void UnhideInteracted()
        {
            EventManager.TriggerEvent(new OpenPanelVREvent(true));
        }
    }
}

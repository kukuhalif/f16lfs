using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;


namespace VirtualTraining.UI.VR
{
    public class SystemPanelVR : WorldSpacePanel
    {
        [SerializeField] GameObject treeViewUI;
        // Start is called before the first frame update
        protected override void Start()
        {
            defaultPosition = DatabaseManager.GetVRData().systemPosition.defaultPosition;
            base.Start();
            StartCoroutine(DelayStart());
        }

        IEnumerator DelayStart()
        {
            yield return new WaitForSeconds(0.5f);
            treeViewUI.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            treeViewUI.SetActive(true);
            SetActivePanel(false);
        }

        public void ActivateTreeViewUI()
        {
            treeViewUI.SetActive(true);
        }

        // Update is called once per frame
        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(SystemPanelVR))]
    public class OverrideSystem : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    SystemPanelVR panel = (SystemPanelVR)target;

                    vrData.systemPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.systemPosition.defaultPosition;
                    DatabaseManager.SetVRData(vrData); ;
                    Debug.Log("Save Position" + panel.gameObject.name+ " Success");
                }
            }

            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif
}

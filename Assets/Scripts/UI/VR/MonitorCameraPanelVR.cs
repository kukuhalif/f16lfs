using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;
using UnityEditor;

namespace VirtualTraining.UI.VR
{
    public class MonitorCameraPanelVR : WorldSpacePanel
    {
        [SerializeField] RectTransform monitorPanel;
        [SerializeField] InteractionButton prevButton;
        [SerializeField] InteractionButton nextButton;
        [SerializeField] TextMeshProUGUI cameraName;
        [SerializeField] LayerMask raycastLayer;

        int currentIndex;
        List<MonitorCamera> monitors;
        UIInteraction panelInteraction;

        RaycastHit hitInfo = new RaycastHit();
        bool isDefaultCursor;
        bool isMovePointerListenerAdded;
        GameObject currentRaycastTarget;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<MonitorCameraEvent>(MonitorCameraListener);
            prevButton.OnClickEvent += Prev;
            nextButton.OnClickEvent += Next;

            panelInteraction = monitorPanel.gameObject.AddComponent<UIInteraction>();
            panelInteraction.OnClickDown += OnLeftClickDown;
            panelInteraction.OnCursorEnter += OnCursorEnter;
            panelInteraction.OnCursorExit += OnCursorExit;

            AspectRatioFitter aspectRatio = monitorPanel.gameObject.AddComponent<AspectRatioFitter>();
            aspectRatio.aspectRatio = monitorPanel.rect.width / monitorPanel.rect.height;
            aspectRatio.aspectMode = AspectRatioFitter.AspectMode.FitInParent;

            defaultPosition = DatabaseManager.GetVRData().monitorCameraPosition.defaultPosition;
            Debug.Log("default Position" + defaultPosition);
            SetActivePanel(false);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MonitorCameraEvent>(MonitorCameraListener);
            prevButton.OnClickEvent -= Prev;
            nextButton.OnClickEvent -= Next;
            panelInteraction.OnClickDown -= OnLeftClickDown;
            panelInteraction.OnCursorEnter -= OnCursorEnter;
            panelInteraction.OnCursorExit -= OnCursorExit;
        }

        private void Update()
        {
            //if (VirtualTrainingCamera.RaycastFromMonitorCameraVR(ref hitInfo, monitorPanel, raycastLayer))
            //{
            //    Debug.Log("kena " + hitInfo.transform.name);
            //}

            //if (Input.GetKeyDown(KeyCode.G))
            //{
            //    SetActivePanel(false);
            //}
            //if (Input.GetKeyDown(KeyCode.O))
            //{
            //    SetActivePanel(true);
            //}
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            cameraName.color = theme.genericTextColor;
        }
        protected override void OnEnable()
        {
            base.OnEnable();
        }

        private void OnLeftClickDown()
        {
            // trigger panel selection because body panel has 2 ui element (body & monitor panel)
            //SetOnTop();
            Debug.Log("masuk monitor player");

            if (VirtualTrainingCamera.RaycastFromMonitorCameraVR(ref hitInfo, monitorPanel, raycastLayer))
            {
                EventManager.TriggerEvent(new MonitorCameraInteractionEvent(hitInfo.collider.gameObject));
                Debug.Log("klik sesuatu " + hitInfo.collider.gameObject.name);
            }
        }

        private void OnCursorEnter()
        {
            EventManager.TriggerEvent(new CursorEnterMonitorPanelEvent(true));
            VirtualTrainingInputSystem.OnMovePointer += OnMovePointer;
            isMovePointerListenerAdded = true;
        }

        private void OnCursorExit()
        {
            EventManager.TriggerEvent(new CursorEnterMonitorPanelEvent(false));
            if (isMovePointerListenerAdded)
            {
                VirtualTrainingInputSystem.OnMovePointer -= OnMovePointer;
                isMovePointerListenerAdded = false;
            }
        }

        private void OnMovePointer(Vector2 deltaPosition)
        {
            if (VirtualTrainingCamera.RaycastFromMonitorCameraVR(ref hitInfo, monitorPanel, raycastLayer))
            {
                if (currentRaycastTarget != hitInfo.collider.gameObject)
                {
                    VirtualTrainingCursor.DefaultCursor();
                    EventManager.TriggerEvent(new RaycastObjectFromMonitorCameraEvent(hitInfo.collider.gameObject));
                    currentRaycastTarget = hitInfo.collider.gameObject;
                    isDefaultCursor = false;
                }
            }
            else
            {
                if (!isDefaultCursor)
                {
                    VirtualTrainingCursor.DefaultCursor();
                    isDefaultCursor = true;
                }
                currentRaycastTarget = null;
            }
        }

        public void TestingTest()
        {
            SetActivePanel(true);
        }

        private void MonitorCameraListener(MonitorCameraEvent e)
        {
            if (e.monitors == null || e.monitors.Count == 0)
            {
                monitors = null;
                SetActivePanel(false);
                return;
            }

            monitors = e.monitors;
            currentIndex = 0;

            // new monitor camera is default camera monitor name (check monitor camera constructor)
            string camName = string.Equals(monitors[0].displayName, "new monitor camera") ? "" : monitors[0].displayName;

            if (e.monitors.Count <= 1)
            {
                prevButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(false);
                cameraName.text = camName;
            }
            else
            {
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
                cameraName.text = camName + " (1/" + e.monitors.Count + ")";
            }
            //StartCoroutine(DelayOpenPanel()); 
            forMonitorCamera = true;
            SetActivePanel(true);


            EventManager.TriggerEvent(new PlayMonitorCameraEvent(0, monitors[0]));
        }

        IEnumerator DelayOpenPanel()
        {
            yield return new WaitForSeconds(2f);
            SetActivePanel(true);
        }


        private void Next()
        {
            currentIndex++;

            if (currentIndex >= monitors.Count)
                currentIndex = 0;

            string camName = string.Equals(monitors[currentIndex].displayName, "new monitor camera") ? "" : monitors[currentIndex].displayName;
            cameraName.text = camName + " (" + (currentIndex + 1) + "/" + monitors.Count + ")";

            EventManager.TriggerEvent(new PlayMonitorCameraEvent(currentIndex, monitors[currentIndex]));
        }

        private void Prev()
        {
            currentIndex--;

            if (currentIndex < 0)
                currentIndex = monitors.Count - 1;

            string camName = string.Equals(monitors[currentIndex].displayName, "new monitor camera") ? "" : monitors[currentIndex].displayName;
            cameraName.text = camName + " (" + (currentIndex + 1) + "/" + monitors.Count + ")";

            EventManager.TriggerEvent(new PlayMonitorCameraEvent(currentIndex, monitors[currentIndex]));
        }

        //protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        //{
        //    if (monitors != null)
        //        base.ShowAllPanelListener(e);
        //}
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(MonitorCameraPanelVR))]
    public class OverrideMonitorCamera : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    MonitorCameraPanelVR panel = (MonitorCameraPanelVR)target;

                    vrData.monitorCameraPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.monitorCameraPosition.defaultPosition;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }
            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif

}

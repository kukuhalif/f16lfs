using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.UI;
using UnityEngine.Video;

namespace VirtualTraining.UI.VR
{
    public class VideoPlayerHelp : MonoBehaviour
    {
        [SerializeField] VideoPlayer videoPlayer;
        [SerializeField] InteractionButton play;
        [SerializeField] InteractionButton pause;

        private void Start()
        {
            play.OnClickEvent += PlayVideo;
            pause.OnClickEvent += PauseVideo;
        }

        private void OnEnable()
        {
            videoPlayer.Stop();
            play.gameObject.SetActive(true);
        }

        private void OnDestroy()
        {
            play.OnClickEvent -= PlayVideo;
            pause.OnClickEvent -= PauseVideo;
        }

        void PlayVideo()
        {
            videoPlayer.Play();
            play.gameObject.SetActive(false);
        }

        void PauseVideo()
        {
            videoPlayer.Pause();
            play.gameObject.SetActive(true);
        }
    }
}


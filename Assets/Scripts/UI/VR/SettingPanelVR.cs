using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;
using UnityEditor;

namespace VirtualTraining.UI.VR
{
    public class SettingPanelVR : WorldSpacePanel
    {
        [SerializeField] TMP_Dropdown qualityDropdown;

        [SerializeField] Slider sfxVolume;
        [SerializeField] Slider voiceVolume;
        [SerializeField] Slider rotateSpeedPlayer;
        [SerializeField] Slider teleportSpeedPlayer;
        [SerializeField] Slider speedLerpMenuVR;
        [SerializeField] Slider durationConfirmationButton;
        [SerializeField] Slider heightMenuDistance;
        [SerializeField] Slider widthMenuDistance;

        [SerializeField] TextMeshProUGUI sfxVolumeText;
        [SerializeField] TextMeshProUGUI voiceVolumesText;
        [SerializeField] TextMeshProUGUI rotateSpeedText;
        [SerializeField] TextMeshProUGUI speedLerpMenuVRText;
        [SerializeField] TextMeshProUGUI durationConfirmationButtonText;
        [SerializeField] TextMeshProUGUI heightMenuDistanceText;
        [SerializeField] TextMeshProUGUI widthMenuText;

        [SerializeField] InteractionButton resetSetting;
        [SerializeField] InteractionButton applySetting;

        [SerializeField] InteractionButton sfxVolumePlus;
        [SerializeField] InteractionButton sfxVolumeMinus;

        [SerializeField] InteractionButton voiceVolumePlus;
        [SerializeField] InteractionButton voiceVolumeMinus;

        [SerializeField] InteractionButton rotateSpeedPlus;
        [SerializeField] InteractionButton rotateSpeedMinus;

        [SerializeField] InteractionButton speedLerpMenuVRPlus;
        [SerializeField] InteractionButton speedLerpMenuVRMinus;

        [SerializeField] InteractionButton durationConfirmationButtonPlus;
        [SerializeField] InteractionButton durationConfirmationButtonMinus;

        [SerializeField] InteractionButton heightMenuDistancePlus;
        [SerializeField] InteractionButton heightMenuDistanceMinus;

        [SerializeField] InteractionButton widthMenuDistancePlus;
        [SerializeField] InteractionButton widthMenuDistanceMinus;

        [SerializeField] TextMeshProUGUI[] allGenericText;

        SettingData defaultSetting;
        SettingData currentSetting;
        List<InteractionSlider> sliders = new List<InteractionSlider>();
        List<Resolution> resolutions = new List<Resolution>();
        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();

            SetMinMaxSetting();
            defaultSetting = DatabaseManager.GetDefaultSetting();
            currentSetting = new SettingData();
            defaultPosition = DatabaseManager.GetVRData().settingPosition.defaultPosition;
            ResolutionOptionInit();

            QualityOptionInit();
            AddSliders();
            SettingUtility.SetSettingData(SettingUtility.LastSetting);
            applySetting.OnClickEvent += ApplyButtonCallback;
            resetSetting.OnClickEvent += ResetButtonCallback;
            EventManager.AddListener<InitializationUIDoneEvent>(UIInidDone);
            SetActivePanel(false);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            RemoveUIListener();

            applySetting.OnClickEvent -= ApplyButtonCallback;
            resetSetting.OnClickEvent -= ResetButtonCallback;
        }
        private void UIInidDone(InitializationUIDoneEvent e)
        {
            Load();
            AddUIListener();
            EventManager.RemoveListener<InitializationUIDoneEvent>(UIInidDone);
        }
        private void AddUIListener()
        {
            sfxVolume.onValueChanged.AddListener(ChangedSFXValue);
            voiceVolume.onValueChanged.AddListener(ChangedBGMValue);
            rotateSpeedPlayer.onValueChanged.AddListener(ChangedRotatePlayerValue);
            teleportSpeedPlayer.onValueChanged.AddListener(ChangedMoveCameraValue);
            speedLerpMenuVR.onValueChanged.AddListener(ChangedSpeedLerpMenuValue);
            durationConfirmationButton.onValueChanged.AddListener(ChangedDurationConfirmationValue);
            heightMenuDistance.onValueChanged.AddListener(ChangedHeightValue);
            widthMenuDistance.onValueChanged.AddListener(ChangedWidthValue);
            qualityDropdown.onValueChanged.AddListener(ChangeQualityValue);

            sfxVolumePlus.OnClickEvent += SfxPlus;
            sfxVolumeMinus.OnClickEvent += SfxMinus;
            voiceVolumePlus.OnClickEvent += VoicePlus;
            voiceVolumeMinus.OnClickEvent += VoiceMinus;
            rotateSpeedPlus.OnClickEvent += RotatePlus;
            rotateSpeedMinus.OnClickEvent += RotateMinus;
            speedLerpMenuVRPlus.OnClickEvent += SpeedLerpPlus;
            speedLerpMenuVRMinus.OnClickEvent += SpeedLerpMinus;
            durationConfirmationButtonPlus.OnClickEvent += DurationCBPlus;
            durationConfirmationButtonMinus.OnClickEvent += DurationCBMinus;
            heightMenuDistancePlus.OnClickEvent += HeightMenuPlus;
            heightMenuDistanceMinus.OnClickEvent += HeightMenuMinus;
            widthMenuDistancePlus.OnClickEvent += WidthMenuPlus;
            widthMenuDistanceMinus.OnClickEvent += WidthMenuMinus;

            for (int i = 0; i < sliders.Count; i++)
            {
                sliders[i].OnPointerUpCallback += OnSliderPointerUpCallback;
            }
        }
        private void RemoveUIListener()
        {
            sfxVolume.onValueChanged.RemoveListener(ChangedSFXValue);
            voiceVolume.onValueChanged.RemoveListener(ChangedBGMValue);
            rotateSpeedPlayer.onValueChanged.RemoveListener(ChangedRotatePlayerValue);
            teleportSpeedPlayer.onValueChanged.RemoveListener(ChangedMoveCameraValue);
            speedLerpMenuVR.onValueChanged.RemoveListener(ChangedSpeedLerpMenuValue);
            durationConfirmationButton.onValueChanged.RemoveListener(ChangedDurationConfirmationValue);
            heightMenuDistance.onValueChanged.RemoveListener(ChangedHeightValue);
            widthMenuDistance.onValueChanged.RemoveListener(ChangedWidthValue);
            qualityDropdown.onValueChanged.RemoveListener(ChangeQualityValue);

            sfxVolumePlus.OnClickEvent -= SfxPlus;
            sfxVolumeMinus.OnClickEvent -= SfxMinus;
            voiceVolumePlus.OnClickEvent -= VoicePlus;
            voiceVolumeMinus.OnClickEvent -= VoiceMinus;
            rotateSpeedPlus.OnClickEvent -= RotatePlus;
            rotateSpeedMinus.OnClickEvent -= RotateMinus;
            speedLerpMenuVRPlus.OnClickEvent -= SpeedLerpPlus;
            speedLerpMenuVRMinus.OnClickEvent -= SpeedLerpMinus;
            durationConfirmationButtonPlus.OnClickEvent -= DurationCBPlus;
            durationConfirmationButtonMinus.OnClickEvent -= DurationCBMinus;
            heightMenuDistancePlus.OnClickEvent -= HeightMenuPlus;
            heightMenuDistanceMinus.OnClickEvent -= HeightMenuMinus;
            widthMenuDistancePlus.OnClickEvent -= WidthMenuPlus;
            widthMenuDistanceMinus.OnClickEvent -= WidthMenuMinus;

            for (int i = 0; i < sliders.Count; i++)
            {
                sliders[i].OnPointerUpCallback -= OnSliderPointerUpCallback;
            }
        }

        private void AddSliders()
        {
            sliders.Add(sfxVolume.GetComponent<InteractionSlider>());
            sliders.Add(voiceVolume.GetComponent<InteractionSlider>());
            sliders.Add(rotateSpeedPlayer.GetComponent<InteractionSlider>());
            sliders.Add(teleportSpeedPlayer.GetComponent<InteractionSlider>());
            sliders.Add(speedLerpMenuVR.GetComponent<InteractionSlider>());
            sliders.Add(durationConfirmationButton.GetComponent<InteractionSlider>());
            sliders.Add(heightMenuDistance.GetComponent<InteractionSlider>());
            sliders.Add(widthMenuDistance.GetComponent<InteractionSlider>());
        }
        private void OnSliderPointerUpCallback(float value)
        {
            Apply();
            AudioPlayer.PlaySFX(SFX.ButtonClick);
        }
        private void ResetButtonCallback()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Reset setting ?", ResetSetting, null));
        }

        private void ApplyButtonCallback()
        {
            SettingData saved = SettingUtility.GetSavedSetting();
            if (string.Equals(JsonUtility.ToJson(saved), JsonUtility.ToJson(currentSetting)))
                KeepSettingCallback();
            else
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Keep setting ?", KeepSettingCallback, DiscardSettingCallback));
        }
        private void QualityOptionInit()
        {
            List<string> qualities = new List<string>();
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                qualities.Add(QualitySettings.names[i]);
            }

            qualityDropdown.ClearOptions();
            qualityDropdown.AddOptions(qualities);

            // quality default to medium
            defaultSetting.quality = 1;
        }

        private void KeepSettingCallback()
        {
            Debug.Log("save setting");
            SettingUtility.SetSettingData(currentSetting);
            SetActivePanel(false);
        }
        private void DiscardSettingCallback()
        {
            Debug.Log("discard setting");
            currentSetting = SettingUtility.GetSavedSetting();

            SetUI();
            Apply();
            //SetActivePanel(false);
        }
        private void ResetSetting()
        {
            Debug.Log("reset setting");
            string defaultData = JsonUtility.ToJson(defaultSetting);
            currentSetting = JsonUtility.FromJson<SettingData>(defaultData);

            SetUI();
            Apply();
            SettingUtility.SetSettingData(currentSetting);
        }
        private void Load()
        {
            Debug.Log("load setting");
            currentSetting = SettingUtility.GetSavedSetting();

            SetUI();
            Apply();
        }
        private void SetMinMaxSetting()
        {
            var minMax = DatabaseManager.GetMinMaxSetting();

            rotateSpeedPlayer.minValue = minMax.speedRotationVR.x;
            rotateSpeedPlayer.maxValue = minMax.speedRotationVR.y;

            teleportSpeedPlayer.minValue = minMax.zoomSpeed.x;
            teleportSpeedPlayer.maxValue = minMax.zoomSpeed.y;

            speedLerpMenuVR.minValue = minMax.dragSpeed.x;
            speedLerpMenuVR.maxValue = minMax.dragSpeed.y;

            durationConfirmationButton.minValue = minMax.moveCameraSpeed.x;
            durationConfirmationButton.maxValue = minMax.moveCameraSpeed.y;

            voiceVolume.minValue = minMax.bgmVolume.x;
            voiceVolume.maxValue = minMax.bgmVolume.y;

            sfxVolume.minValue = minMax.sfxVolume.x;
            sfxVolume.maxValue = minMax.sfxVolume.y;

            heightMenuDistance.minValue = minMax.vrMenuHeight.x;
            heightMenuDistance.maxValue = minMax.vrMenuHeight.y;

            widthMenuDistance.maxValue = minMax.vrCanvasDistance.y;
            widthMenuDistance.maxValue = minMax.vrCanvasDistance.y;
        }
        private void SetUI()
        {
            Debug.Log("set ui setting");
            rotateSpeedPlayer.value = currentSetting.speedRotationVR;
            teleportSpeedPlayer.value = currentSetting.cameraTransitionSpeed;
            speedLerpMenuVR.value = currentSetting.speedLerpMenuVR;
            durationConfirmationButton.value = currentSetting.durationConfirmationButton;
            sfxVolume.value = currentSetting.sfxVolume;
            voiceVolume.value = currentSetting.voiceVolume;
            qualityDropdown.value = currentSetting.quality;
            heightMenuDistance.value = currentSetting.vrMenuHeight;
            widthMenuDistance.value = currentSetting.vrCanvasDistance;
            qualityDropdown.value = currentSetting.quality;
            Debug.Log("set quality = " + currentSetting.quality);

            sfxVolumeText.text = ConvertValue(sfxVolume.value).ToString();
            voiceVolumesText.text = ConvertValue(voiceVolume.value).ToString();
            speedLerpMenuVRText.text = ConvertValue(speedLerpMenuVR.value).ToString();
            rotateSpeedText.text = ConvertValue(rotateSpeedPlayer.value).ToString();
            durationConfirmationButtonText.text = ConvertValue(durationConfirmationButton.value).ToString();
            heightMenuDistanceText.text = ConvertValue(heightMenuDistance.value).ToString();
            widthMenuText.text = ConvertValue(widthMenuDistance.value).ToString();

            for (int i = 0; i < allGenericText.Length; i++)
            {
                allGenericText[i].color = theme.genericTextColor;
            }
        }

        private void Apply()
        {
            Debug.Log("apply setting");

            int width;
            int height;

            if (currentSetting.resolution <= resolutions.Count - 1)
            {
                width = resolutions[currentSetting.resolution].width;
                height = resolutions[currentSetting.resolution].height;
            }
            else
            {
                width = resolutions[resolutions.Count - 1].width;
                height = resolutions[resolutions.Count - 1].height;
            }

            Screen.SetResolution(width, height, currentSetting.fullscreen);
            QualitySettings.SetQualityLevel(currentSetting.quality);

            Vector2 resolution = new Vector2(width, height);

            SettingUtility.SetLastSetting(currentSetting);
            SettingUtility.SetLastResolution(resolution);
            EventManager.TriggerEvent(new ApplySettingEvent(currentSetting, resolution));
            EventManager.TriggerEvent(new SwitchUIThemeEvent(currentSetting.uiTheme));
        }

        private void ResolutionOptionInit()
        {
            List<string> resOpt = new List<string>();
            Resolution[] res = Screen.resolutions;

            resolutions.Clear();

#if UNITY_EDITOR
            Resolution forka = new Resolution();
            forka.width = 3840;
            forka.height = 2160;
            resolutions.Add(forka);
            resOpt.Add("4k test");
#endif

            for (int i = 0; i < res.Length; i++)
            {
                float ar = (float)Screen.resolutions[i].width / (float)Screen.resolutions[i].height;
                if (ar >= 1.7 && ar < 1.8)
                {
                    if (!resOpt.Contains(res[i].width + " x " + res[i].height))
                    {
                        resolutions.Add(res[i]);
                        resOpt.Add(res[i].width + " x " + res[i].height);
                    }
                }
            }

            Debug.Log(resOpt.Count);
            // resolution default
            if (resOpt != null && resOpt.Count > 0)
                defaultSetting.resolution = resOpt.Count - 1;
        }

        private void ChangedBGMValue(float value)
        {
            currentSetting.voiceVolume = value;
            voiceVolumesText.text = ConvertValue(value).ToString();
        }
        private void ChangedSFXValue(float value)
        {
            currentSetting.sfxVolume = value;
            sfxVolumeText.text = ConvertValue(value).ToString();
        }
        private void ChangedRotatePlayerValue(float value)
        {
            currentSetting.speedRotationVR = value;
            rotateSpeedText.text = ConvertValue(value).ToString();
        }
        private void ChangedMoveCameraValue(float value)
        {
            currentSetting.cameraTransitionSpeed = value;
        }
        private void ChangedSpeedLerpMenuValue(float value)
        {
            currentSetting.speedLerpMenuVR = value;
            speedLerpMenuVRText.text = ConvertValue(value).ToString();
        }
        private void ChangedDurationConfirmationValue(float value)
        {
            currentSetting.durationConfirmationButton = value;
            durationConfirmationButtonText.text = ConvertValue(value).ToString();
        }
        private void ChangedHeightValue(float value)
        {
            currentSetting.vrMenuHeight = value;
            heightMenuDistanceText.text = ConvertValue(value).ToString();
        }
        private void ChangedWidthValue(float value)
        {
            currentSetting.vrCanvasDistance = value;
            widthMenuText.text = ConvertValue(value).ToString();
        }

        private void ChangeQualityValue(int quality)
        {
            currentSetting.quality = quality;
            QualitySettings.SetQualityLevel(currentSetting.quality);
        }

        float ConvertValue(float value)
        {
            value = Mathf.Round(value * 10.0f) * 0.1f;
            return value;
        }

        void SliderPlus(Slider slider)
        {
            float rangeSlider = slider.maxValue - slider.minValue;

            if (slider.value < slider.maxValue - (rangeSlider / 10))
                slider.value += rangeSlider / 10;
            else
                slider.value = slider.maxValue;
        }

        void SliderMinus(Slider slider)
        {
            float rangeSlider = slider.maxValue - slider.minValue;

            if (slider.value > slider.minValue + (rangeSlider / 10))
                slider.value -= rangeSlider / 10;
            else
                slider.value = slider.minValue;
        }

        void SfxPlus()
        {
            SliderPlus(sfxVolume);
        }

        void SfxMinus()
        {
            SliderMinus(sfxVolume);
        }

        void VoicePlus()
        {
            SliderPlus(voiceVolume);
        }

        void VoiceMinus()
        {
            SliderMinus(voiceVolume);
        }

        void RotatePlus()
        {
            SliderPlus(rotateSpeedPlayer);
        }

        void RotateMinus()
        {
            SliderMinus(rotateSpeedPlayer);
        }

        void SpeedLerpPlus()
        {
            SliderPlus(speedLerpMenuVR);
        }

        void SpeedLerpMinus()
        {
            SliderMinus(speedLerpMenuVR);
        }

        void DurationCBPlus()
        {
            SliderPlus(durationConfirmationButton);
        }

        void DurationCBMinus()
        {
            SliderMinus(durationConfirmationButton);
        }
        void HeightMenuPlus()
        {
            SliderPlus(heightMenuDistance);
        }

        void HeightMenuMinus()
        {
            SliderMinus(heightMenuDistance);
        }

        void WidthMenuPlus()
        {
            SliderPlus(widthMenuDistance);
        }

        void WidthMenuMinus()
        {
            SliderMinus(widthMenuDistance);
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(SettingPanelVR))]
    public class OverrideSetting : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    SettingPanelVR panel = (SettingPanelVR)target;

                    vrData.settingPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.settingPosition.defaultPosition;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }
            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif
}

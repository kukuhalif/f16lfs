using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI.Desktop;
using UnityEditor;

namespace VirtualTraining.UI.VR
{
    public class TroubleshootPanelVR : WorldSpacePanel
    {
        public GameObject prefabCanvastTroubleshoot;
        GameObject currentCanvasT;
        bool isTroubleshootDataExist = false;
        // Start is called before the first frame update
        protected override void Start()
        {
            defaultPosition = DatabaseManager.GetVRData().troubleshootPosition.defaultPosition;
            base.Start();
            EventManager.AddListener<TroubleshootUIEvent>(TroubleshootListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
            SetActivePanel(false);
        }

        // Update is called once per frame
        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<TroubleshootUIEvent>(TroubleshootListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
        }

        void TroubleshootListener(TroubleshootUIEvent e)
        {
            Debug.Log("instantiate sssss");
            if (e.materi != null)
            {
                if (e.materi.troubleshootData.troubleshootGraph != null && currentCanvasT == null)
                {
                    currentCanvasT = Instantiate(prefabCanvastTroubleshoot, transform);
                    currentCanvasT.transform.SetSiblingIndex(1);
                    var tg = currentCanvasT.GetComponent<TroubleshootGenerator>();
                    tg.troubleshootData = e.materi.troubleshootData;
                    tg.Initialize();
                    currentCanvasT.SetActive(true);

                    isTroubleshootDataExist = true;
                }

                if (e.materi.troubleshootData.troubleshootGraph == null)
                {
                    Debug.LogError("troubleshoot database missing");
                    return;
                }

                Debug.Log("instantiate troubleshoot");
                SetActivePanel(true);
            }
            else
            {
                ResetTroubleshoot();
            }
        }
        private void PlayMateriListener(MateriEvent e)
        {
            SetActivePanel(false);
        }
        public void ResetTroubleshoot()
        {
            if (currentCanvasT != null)
                Destroy(currentCanvasT);

            isTroubleshootDataExist = false;
        }

        public void DeactivePanel()
        {
            SetActivePanel(false);
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(TroubleshootPanelVR))]
    public class OverrideTroubleshoot : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    TroubleshootPanelVR panel = (TroubleshootPanelVR)target;

                    vrData.troubleshootPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.troubleshootPosition.defaultPosition;
                    DatabaseManager.SetVRData(vrData); ;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }

            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif
}


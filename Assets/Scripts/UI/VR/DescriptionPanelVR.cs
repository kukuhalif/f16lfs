using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;
using VirtualTraining.UI.Desktop;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using VirtualTraining.Feature;

namespace VirtualTraining.UI.VR
{
    public class DescriptionPanelVR : WorldSpacePanel
    {
        [SerializeField] TextMeshProUGUI title;

        [SerializeField] InteractionButton schematicButton;
        [SerializeField] InteractionButton maintenanceButton;
        [SerializeField] InteractionButton troubleshootButton;
        [SerializeField] InteractionButton resetButton;
        [SerializeField] InteractionButton previousMateriButton;
        [SerializeField] InteractionButton nextMateriButton;
        [SerializeField] InteractionButton nextFigureButton;
        [SerializeField] InteractionButton previousFigureButton;
        [SerializeField] InteractionButton previousPositionButton;
        [SerializeField] InteractionButton nextPositionButton;
        [SerializeField] InteractionToggle soundToggle;
        [SerializeField] InteractionButton playSoundButton;
        [SerializeField] InteractionButton pauseSoundButton;
        [SerializeField] InteractionButton resetSoundButton;
        [SerializeField] InteractionButton playPauseAnimation;
        [SerializeField] InteractionButton beginAnimation;
        [SerializeField] InteractionButton lastAnimation;

        [SerializeField] GameObject figureContainer;
        [SerializeField] GameObject PositionContainer;
        [SerializeField] GameObject animationPlayBar;
        [SerializeField] GameObject materiPositionContainer;
        [SerializeField] GameObject audioControll;

        [SerializeField] TMP_Text cameraName;
        [SerializeField] TMP_Text figureName;

        [SerializeField] AnimationManager animationManager;
        [SerializeField] Slider sliderAnimation;
        [SerializeField] MaintenancePanelVR maintenancePanel;
        [SerializeField] TroubleshootPanelVR troubleshootPanel;
        [SerializeField] HeaderPanelVR headerPanelVR;
        int cameraIndex;
        List<FigureCamera> cameras;
        AudioClip audioMateri;

        bool animationIsPlaying;
        bool isMaintenanceOpen;
        bool isTroubleshootOpen;

        // cache
        private MateriTreeElement currentMateriTreeElement;
        private List<MateriTreeElement> materies = new List<MateriTreeElement>();
        [SerializeField] List<GameObject> animationButtons = new List<GameObject>();

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            var materiTree = DatabaseManager.GetMateriTree();
            TreeElementUtility.TreeToList(materiTree, materies);

            // remove empty materi data
            List<MateriTreeElement> toRemove = new List<MateriTreeElement>();
            for (int i = 0; i < materies.Count; i++)
            {
                if (materies[i].depth < 0)
                    toRemove.Add(materies[i]);
                else if (materies[i].isFolder)
                    toRemove.Add(materies[i]);

                if (materies[i].MateriData.deviceMode == DeviceMode.DesktopOnly)
                    toRemove.Add(materies[i]);
            }
            for (int i = 0; i < toRemove.Count; i++)
            {
                materies.Remove(toRemove[i]);
            }

            defaultPosition = DatabaseManager.GetVRData().descriptionPosition.defaultPosition;


            schematicButton.OnClickEvent += OpenSchematic;
            maintenanceButton.OnClickEvent += OpenMaintenance;
            troubleshootButton.OnClickEvent += OpenTroubleshoot;
            //resetButton.OnClickEvent += ResetMateri;
            previousMateriButton.OnClickEvent += PrevMateri;
            nextMateriButton.OnClickEvent += NextMateri;
            nextFigureButton.OnClickEvent += NextFigure;
            previousFigureButton.OnClickEvent += PreviousFigure;
            nextPositionButton.OnClickEvent += NextPosition;
            previousPositionButton.OnClickEvent += PreviousPosition;
            //soundToggle.OnChangeStateEvent += SoundToggleInteracted;
            playSoundButton.OnClickEvent += PlaySoundButton;
            pauseSoundButton.OnClickEvent += PauseSoundButton;
            resetSoundButton.OnClickEvent += ResetSoundButton;

            playPauseAnimation.OnClickEvent += PlayPauseAnimationListener;
            //beginAnimation.OnClickEvent += BeginAnimationListener;
            //lastAnimation.OnClickEvent += LastAnimationListener;

            EventManager.AddListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.AddListener<UpdateFigureCameraPanelEvent>(FigureCameraListListener);
            EventManager.AddListener<UpdateFigurePanelEvent>(PlayFigureListener);
            EventManager.AddListener<AnimationPlayEvent>(PlayAnimationListener);
            EventManager.AddListener<VoiceSoundEvent>(NarationSoundListener);
            EventManager.AddListener<SceneReadyEvent>(SceneReadyEventListener);

            sliderAnimation.onValueChanged.AddListener(delegate { AnimationSliderChanged(); });
            SetActivePanel(false);
        }

        // Update is called once per frame

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.RemoveListener<UpdateFigureCameraPanelEvent>(FigureCameraListListener);
            EventManager.RemoveListener<UpdateFigurePanelEvent>(PlayFigureListener);
            EventManager.RemoveListener<AnimationPlayEvent>(PlayAnimationListener);
            EventManager.RemoveListener<VoiceSoundEvent>(NarationSoundListener);

            schematicButton.OnClickEvent -= OpenSchematic;
            maintenanceButton.OnClickEvent -= OpenMaintenance;
            troubleshootButton.OnClickEvent -= OpenTroubleshoot;
            //resetButton.OnClickEvent -= ResetMateri;
            previousMateriButton.OnClickEvent -= PrevMateri;
            nextMateriButton.OnClickEvent -= NextMateri;
            nextFigureButton.OnClickEvent -= NextFigure;
            previousFigureButton.OnClickEvent -= PreviousFigure;
            nextPositionButton.OnClickEvent -= NextPosition;
            previousPositionButton.OnClickEvent -= PreviousPosition;
            //soundToggle.OnChangeStateEvent -= SoundToggleInteracted;
            playSoundButton.OnClickEvent -= PlaySoundButton;
            pauseSoundButton.OnClickEvent -= PauseSoundButton;
            resetSoundButton.OnClickEvent -= ResetSoundButton;

            playPauseAnimation.OnClickEvent -= PlayPauseAnimationListener;
            //beginAnimation.OnClickEvent -= BeginAnimationListener;
            //lastAnimation.OnClickEvent -= LastAnimationListener;

            sliderAnimation.onValueChanged.RemoveListener(delegate { AnimationSliderChanged(); });
            animationManager.SetSlider(sliderAnimation, null, null);
        }

        private void DescriptionEventListener(DescriptionPanelEvent e)
        {
            isMaintenanceOpen = false;
            isTroubleshootOpen = false;
            if (title != null)
                title.text = e.materiTreeElement.MateriData.name;

            currentMateriTreeElement = e.materiTreeElement;

            maintenanceButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.Maintenance);
            troubleshootButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.Troubleshoot);

            if (currentMateriTreeElement.MateriData.schematics != null && currentMateriTreeElement.MateriData.schematics.Count > 0)
            {
                int contextMenuSchematicCount = 0;

                for (int i = 0; i < currentMateriTreeElement.MateriData.schematics.Count; i++)
                {
                    if (string.IsNullOrEmpty(currentMateriTreeElement.MateriData.schematics[i].descriptionId))
                    {
                        contextMenuSchematicCount++;
                    }
                }

                schematicButton.gameObject.SetActive(contextMenuSchematicCount > 0);
            }
            else
                schematicButton.gameObject.SetActive(false);

            SetActivePanel(true);
        }
        void PlayAnimationListener(AnimationPlayEvent e)
        {
            if (e.animationData == null)
            {
                for (int i = 0; i < animationButtons.Count; i++)
                {
                    animationButtons[i].SetActive(false);
                }
                return;
            }
            animationIsPlaying = !animationIsPlaying;
            animationIsPlaying = true;
            playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);

            for (int i = 0; i < animationButtons.Count; i++)
            {
                animationButtons[i].SetActive(true);
            }

            if (e.animationData.isShowAnimationUI == false)
            {
                if (animationPlayBar != null)
                    animationPlayBar.SetActive(false);
            }
            else
            {
                if (animationPlayBar != null)
                    animationPlayBar.SetActive(true);
            }
        }

        void SceneReadyEventListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyEventListener);
            animationManager = FindObjectOfType<AnimationManager>();
            animationManager.SetSlider(sliderAnimation, ResetAnimationDoneListener, DoneAnimationListener);
            animationManager.ResetAnimation();
        }

        private void OpenMaintenance()
        {
            if (isMaintenanceOpen)
            {
                maintenancePanel.DeactivePanel();
                isMaintenanceOpen = false;
            }
            else
            {
                isMaintenanceOpen = true;
                EventManager.TriggerEvent(new MaintenanceUIEvent(currentMateriTreeElement.MateriData));
            }
        }
        private void OpenTroubleshoot()
        {
            if (isTroubleshootOpen)
            {
                troubleshootPanel.DeactivePanel();
                isTroubleshootOpen = true;
            }
            else
            {
                EventManager.TriggerEvent(new TroubleshootUIEvent(currentMateriTreeElement.MateriData));
                isTroubleshootOpen = false;
            }
        }
        private void PrevMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index--;

            if (index > -1)
            {
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[index]));
            }
        }
        private void OpenSchematic()
        {
            List<Schematic> contextMenuSchematic = new List<Schematic>();

            for (int i = 0; i < currentMateriTreeElement.MateriData.schematics.Count; i++)
            {
                if (string.IsNullOrEmpty(currentMateriTreeElement.MateriData.schematics[i].descriptionId))
                {
                    contextMenuSchematic.Add(currentMateriTreeElement.MateriData.schematics[i]);
                }
            }


            //if (contextMenuSchematic.Count == 1)
            if (contextMenuSchematic.Count >= 1)
                EventManager.TriggerEvent(new ShowSchematicEvent(contextMenuSchematic[0], true));
            //else if (contextMenuSchematic.Count > 1)
            //{
            //    VirtualTraining.UI.Desktop.ContextMenu.Load();

            //    for (int i = 0; i < contextMenuSchematic.Count; i++)
            //    {
            //        string menuItem = contextMenuSchematic[i].image == null ? contextMenuSchematic[i].video.name : contextMenuSchematic[i].image.name;
            //        Schematic schematic = contextMenuSchematic[i];
            //        VirtualTraining.UI.Desktop.ContextMenu.AddItem(menuItem, delegate { EventManager.TriggerEvent(new ShowSchematicEvent(schematic, true)); });
            //    }

            //    //VirtualTraining.UI.Desktop.ContextMenu.Show(Canvas, this);
            //}
        }

        private void NextMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index++;

            if (index < materies.Count)
            {
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[index]));
            }
        }

        void NextFigure()
        {
            EventManager.TriggerEvent(new NextFigureEvent(MateriElementType.Materi));
        }

        void PreviousFigure()
        {
            EventManager.TriggerEvent(new PrevFigureEvent(MateriElementType.Materi));
        }

        void SoundToggleInteracted(bool isOn)
        {
            if (isOn)
            {
                if (audioMateri != null)
                    AudioPlayer.PlayMateri(audioMateri);
                else
                    Debug.Log("audio clip null");
            }
            else
                PauseSoundButton();
        }

        void NarationSoundListener(VoiceSoundEvent e)
        {
            if (e.goAudio != this.gameObject)
            {
                PauseSoundButton();
            }
        }

        void PlaySoundButton()
        {
            if (audioMateri != null)
            {
                AudioPlayer.PlayMateri(audioMateri);
                EventManager.TriggerEvent(new VoiceSoundEvent(this.gameObject));
            }
            else
                Debug.Log("audio clip null");
        }

        void PauseSoundButton()
        {
            AudioPlayer.PauseMateri();
        }
        void ResetSoundButton()
        {
            if (audioMateri != null)
                AudioPlayer.ResetMateri(audioMateri);
        }

        private int GetCurrentMateriIndex()
        {
            for (int i = 0; i < materies.Count; i++)
            {
                if (currentMateriTreeElement.id == materies[i].id)
                    return i;
            }

            return -1;
        }

        private void ResetMateri()
        {
            EventManager.TriggerEvent(new MateriEvent(currentMateriTreeElement));
        }

        public bool IsMateriDataExist()
        {
            return currentMateriTreeElement != null;
        }

        void NextPosition()
        {
            if (cameras.Count <= 0)
                return;

            cameraIndex++;

            if (cameraIndex >= cameras.Count)
                cameraIndex = 0;

            VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
            cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
        }

        void PreviousPosition()
        {
            if (cameras.Count <= 1)
                return;

            cameraIndex--;

            if (cameraIndex < 0)
                cameraIndex = cameras.Count - 1;

            VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
            cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
        }
        private void PlayFigureListener(UpdateFigurePanelEvent e)
        {
            audioControll.SetActive(false);
            if (e.figureCount == 0)
            {
                figureContainer.SetActive(false);
                if (e.figure.clip == null)
                    AudioPlayer.StopMateri();
                else
                {
                    audioMateri = e.figure.clip;
                    AudioPlayer.ResetMateri(e.figure.clip);
                    //soundToggle.IsOn = true;
                }
            }
            else if (e.figureCount == 1 && (string.Equals(e.name, "new figure") || string.IsNullOrEmpty(e.name))) // "new figure is default figure name, check figure constructor"
            {
                figureName.text = e.name;
                figureContainer.SetActive(false);
                if (e.figure.clip == null)
                    PauseSoundButton();
                else
                {
                    audioControll.SetActive(true);
                    audioMateri = e.figure.clip;
                    AudioPlayer.ResetMateri(e.figure.clip);
                    //soundToggle.IsOn = true;
                }
            }
            else
            {
                if (e.figureCount <= 1)
                {
                    previousFigureButton.gameObject.SetActive(false);
                    nextFigureButton.gameObject.SetActive(false);
                    figureName.text = string.Equals(e.name, "new figure") ? "" : e.name;
                }
                else
                {
                    previousFigureButton.gameObject.SetActive(true);
                    nextFigureButton.gameObject.SetActive(true);
                    figureName.text = (string.Equals(e.name, "new figure") ? "" : e.name) + " (" + (e.currentFigure + 1).ToString() + " / " + e.figureCount.ToString() + ")";
                }

                if (e.figure.clip == null)
                    PauseSoundButton();
                else
                {
                    audioMateri = e.figure.clip;
                    AudioPlayer.ResetMateri(e.figure.clip);
                    audioControll.SetActive(true);
                }

                figureContainer.SetActive(true);
            }
            StartCoroutine(SetCollider());

        }

        private void FigureCameraListListener(UpdateFigureCameraPanelEvent e)
        {
            if (e.type != MateriElementType.Materi)
                return;

            PositionContainer.SetActive(false);
            materiPositionContainer.SetActive(true);
            // filter cameras
            cameras = (from camera in e.figureCameras
                       where camera.mode == DeviceMode.AllDevice || camera.mode == DeviceMode.VrOnly
                       select camera).ToList();

            // set current camera
            cameraIndex = e.cameraIndex;

            if (cameras.Count == 0)
            {
                nextPositionButton.gameObject.SetActive(false);
                previousPositionButton.gameObject.SetActive(false);
            }
            else if (cameras.Count == 1 && (string.Equals(cameras[cameraIndex].displayName, "new figure camera") || string.IsNullOrEmpty(cameras[cameraIndex].displayName))) //  "new figure camera is default camera figure name, check figure camera constructor"
            {
                nextPositionButton.gameObject.SetActive(false);
                previousPositionButton.gameObject.SetActive(false);
            }
            else
            {
                if (cameras.Count <= 1)
                {
                    nextPositionButton.gameObject.SetActive(false);
                    previousPositionButton.gameObject.SetActive(false);
                    cameraName.text = string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName;
                }
                else
                {
                    PositionContainer.SetActive(true);
                    nextPositionButton.gameObject.SetActive(true);
                    previousPositionButton.gameObject.SetActive(true);
                    cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
                }
            }

            if (!figureContainer.activeInHierarchy && !PositionContainer.activeInHierarchy)
                materiPositionContainer.SetActive(false);
            else
                materiPositionContainer.SetActive(true);

            StartCoroutine(SetCollider());
        }

        IEnumerator SetCollider()
        {
            yield return new WaitForSeconds(0.2f);
            RectTransform rect = GetComponent<RectTransform>();
            BoxCollider boxColl = GetComponent<BoxCollider>();

            if (boxColl != null)
                boxColl.size = new Vector2(rect.sizeDelta.x, boxColl.size.y);

            if (headerPanelVR != null)
            {
                headerPanelVR.SetCollider(rect);
            }
        }
        void ResetAnimationDoneListener()
        {
            for (int i = 0; i < animationButtons.Count; i++)
            {
                animationButtons[i].SetActive(false);
            }
            animationIsPlaying = false;
        }

        void DoneAnimationListener()
        {
            animationIsPlaying = false;
            playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);
        }

        void PlayPauseAnimationListener()
        {
            //EventManager.TriggerEvent(new PlayPauseAnimationEvent());
            if (animationManager == null)
                return;

            animationManager.PlayAnimation();
            animationIsPlaying = !animationIsPlaying;
            if (animationIsPlaying)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);
            else
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);
        }

        void BeginAnimationListener()
        {
            if (animationManager == null)
                return;

            animationManager.BeginAnimation();
        }

        void LastAnimationListener()
        {
            if (animationManager == null)
                return;

            animationManager.LastAnimation();
        }
        public void AnimationSliderChanged()
        {
            if (animationManager == null)
                return;

            animationManager.ValueChanged();
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(DescriptionPanelVR))]
    public class OverrideDescription : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    DescriptionPanelVR panel = (DescriptionPanelVR)target;

                    vrData.descriptionPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.descriptionPosition.defaultPosition;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }
            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif
}

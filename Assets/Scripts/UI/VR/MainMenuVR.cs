using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.UI.VR
{
    public class MainMenuVR : MonoBehaviour
    {
        protected UITheme theme
        {
            get => DatabaseManager.GetUiTheme();
        }
        [SerializeField] InteractionButton systemButton;
        [SerializeField] InteractionButton assestiveButton;
        [SerializeField] InteractionButton toolbar2Button;
        [SerializeField] InteractionButton settingButton;
        [SerializeField] InteractionButton helpButton;
        [SerializeField] InteractionButton exitButton;

        [SerializeField] InteractionToggle systemToggle;
        [SerializeField] InteractionToggle assestiveToggle;
        [SerializeField] InteractionToggle toolbar2Toggle;
        [SerializeField] InteractionToggle settingToggle;
        [SerializeField] InteractionToggle helpToggle;
        [SerializeField] InteractionToggle exitToggle;

        [SerializeField] SystemPanelVR systemPanelVR;
        [SerializeField] DescriptionPanelVR descriptionPanelVR;
        [SerializeField] AssestiveButtonVR assestivePanelVR;
        [SerializeField] GameObject toolbar2PanelVR;
        [SerializeField] HelpPanelVR helpPanelVR;
        [SerializeField] SettingPanelVR settingPanelVR;
        [SerializeField] ConfirmationPanelVR confirmationPanelVR;

        [SerializeField] TextMeshProUGUI[] allGenericText;

        public GameObject allSystemPanel;

        // Start is called before the first frame update
        void Start()
        {
            systemButton.OnPointerDownEvent += SystemButtonClicked;
            assestiveButton.OnPointerDownEvent += ToolbarButtonClicked;
            settingButton.OnPointerDownEvent += SettingButtonClicked;
            toolbar2Button.OnPointerDownEvent += Toolbar2ButtonClicked;
            helpButton.OnPointerDownEvent += HelpButtonClicked;
            exitButton.OnPointerDownEvent += ExitButtonClicked;

            systemToggle.OnStateChangedEvent += SystemToggleInteracted;
            assestiveToggle.OnStateChangedEvent += ToolbarToggleInteracted;
            toolbar2Toggle.OnStateChangedEvent += Toolbar2ToggleInteracted;
            settingToggle.OnStateChangedEvent += SettingToggleInteracted;
            helpToggle.OnStateChangedEvent += HelpToggleInteracted;
            exitToggle.OnStateChangedEvent += ExitToggleInteracted;

            EventManager.AddListener<ResetObjectInteractionModeEvent>(DefaultModeListener);

            SetTextGeneric();
            DefaultState();

            StartCoroutine(InitExitToggle());
        }

        IEnumerator InitExitToggle()
        {
            exitToggle.gameObject.SetActive(true);
            yield return null;
            exitToggle.gameObject.SetActive(false);
        }

        public void SetupAllPanel(SystemPanelVR systemPanelVR, DescriptionPanelVR descriptionPanelVR, HelpPanelVR helpPanelVR, SettingPanelVR settingPanelVR,
            ConfirmationPanelVR confirmationPanelVR)
        {
            this.systemPanelVR = systemPanelVR;
            this.descriptionPanelVR = descriptionPanelVR;
            this.helpPanelVR = helpPanelVR;
            this.settingPanelVR = settingPanelVR;
            this.confirmationPanelVR = confirmationPanelVR;
        }

        private void OnDestroy()
        {
            systemButton.OnPointerDownEvent -= SystemButtonClicked;
            assestiveButton.OnPointerDownEvent -= ToolbarButtonClicked;
            settingButton.OnPointerDownEvent -= SettingButtonClicked;
            helpButton.OnPointerDownEvent -= HelpButtonClicked;
            exitButton.OnPointerDownEvent -= ExitButtonClicked;

            systemToggle.OnStateChangedEvent -= SystemToggleInteracted;
            assestiveToggle.OnStateChangedEvent -= ToolbarToggleInteracted;
            settingToggle.OnStateChangedEvent -= SettingToggleInteracted;
            helpToggle.OnStateChangedEvent -= HelpToggleInteracted;
            exitToggle.OnStateChangedEvent -= ExitToggleInteracted;

            EventManager.RemoveListener<ResetObjectInteractionModeEvent>(DefaultModeListener);
        }

        private void DefaultModeListener(ResetObjectInteractionModeEvent e)
        {
            DefaultState();
        }

        public void CloseConfirmationPanel()
        {
            exitToggle.IsOn = false;
        }

        void SetTextGeneric()
        {
            for (int i = 0; i < allGenericText.Length; i++)
            {
                allGenericText[i].color = theme.genericTextColor;
            }
        }
        void SystemButtonClicked()
        {
            systemToggle.IsOn = !systemToggle.IsOn;
        }
        void ToolbarButtonClicked()
        {
            //assestiveToggle.IsOn = true;
            assestiveToggle.IsOn = !assestiveToggle.IsOn;
        }
        void SettingButtonClicked()
        {
            settingToggle.IsOn = !settingToggle.IsOn;

        }
        void Toolbar2ButtonClicked()
        {
            //toolbar2Toggle.IsOn = true;
            toolbar2Toggle.IsOn = !toolbar2Toggle.IsOn;
        }
        void HelpButtonClicked()
        {
            helpToggle.IsOn = !helpToggle.IsOn;
        }
        void ExitButtonClicked()
        {
            exitToggle.IsOn = !exitToggle.IsOn;
        }

        void SystemToggleInteracted(bool condition)
        {
            if (condition)
            {
                ToolbarToggleInteracted(false);
                Toolbar2ToggleInteracted(false);
                SetaActiveSystemPanel();
                systemPanelVR.SetActivePanel(condition);
                systemPanelVR.ActivateTreeViewUI();
            }
            else
                systemPanelVR.SetActivePanel(condition, SetaUnactiveSystemPanel);
        }
        public void SetaActiveSystemPanel()
        {
            allSystemPanel.SetActive(true);
        }

        public void SetaUnactiveSystemPanel()
        {
            allSystemPanel.SetActive(false);
        }
        void ToolbarToggleInteracted(bool condition)
        {
            assestivePanelVR.gameObject.SetActive(condition);
            //assestivePanelVR.CutawayToggleInteraction(false);
            assestivePanelVR.AllResetToggle();
        }
        void SettingToggleInteracted(bool condition)
        {
            settingPanelVR.SetActivePanel(condition);
        }
        void Toolbar2ToggleInteracted(bool condition)
        {
            toolbar2PanelVR.SetActive(condition);
        }
        void HelpToggleInteracted(bool condition)
        {
            helpPanelVR.SetActivePanel(condition);
        }
        void ExitToggleInteracted(bool condition)
        {
            if (condition)
            {
                Debug.Log("quit vr");
                EventManager.AddListener<CameraArriveEvent>(CameraArriveListener);
                EventManager.TriggerEvent(new FreePlayEvent(true));

            }
            else
            {
                confirmationPanelVR.SetActivePanel(false);
                EventManager.TriggerEvent(new CloseConfirmationPanelEvent());
            }
        }

        private void CameraArriveListener(CameraArriveEvent e)
        {
            Debug.Log("camera reset arrive");
            EventManager.RemoveListener<CameraArriveEvent>(CameraArriveListener);
            VirtualTrainingSceneManager.UnloadVRScene();
        }

        private void DefaultState()
        {
            EventManager.TriggerEvent(new ObjectInteractionRotateEvent(true));
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionLockCameraEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionCutawayEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
        }

    }
}

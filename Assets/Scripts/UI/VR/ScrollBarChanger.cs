using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    public class ScrollBarChanger : MonoBehaviour
    {
        [SerializeField] Scrollbar scrollbar;
        [SerializeField] InteractionButton plusButton;
        [SerializeField] InteractionButton minusButton;

        [SerializeField] float speedScrollbar = 0.15f;

        bool isPlusPress;
        bool isMinusPress;

        private void Start()
        {
            plusButton.OnPointerDownEvent += PlusButtonPressed;
            plusButton.OnPointerUpEvent += PlusButtonReleased;
            minusButton.OnPointerDownEvent += MinusButtonPressed;
            minusButton.OnPointerUpEvent += MinusButtonReleased;

            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            plusButton.OnPointerDownEvent -= PlusButtonPressed;
            plusButton.OnPointerUpEvent -= PlusButtonReleased;
            minusButton.OnPointerDownEvent -= MinusButtonPressed;
            minusButton.OnPointerUpEvent -= MinusButtonReleased;

            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }
        private void ApplySettingListener(ApplySettingEvent e)
        {
            speedScrollbar = e.data.speedScrollbar/10;
        }

        private void Update()
        {
            if (isPlusPress)
                if (scrollbar.value < 1)
                    scrollbar.value += speedScrollbar * Time.deltaTime;
            if (isMinusPress)
                if (scrollbar.value > 0)
                    scrollbar.value -= speedScrollbar * Time.deltaTime;
        }

        void PlusButtonPressed()
        {
            isPlusPress = true;
        }

        void PlusButtonReleased()
        {
            isPlusPress = false;
        }

        void MinusButtonPressed()
        {
            isMinusPress = true;
        }

        void MinusButtonReleased()
        {
            isMinusPress = false;
        }
    }
}


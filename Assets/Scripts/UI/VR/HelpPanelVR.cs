using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;

namespace VirtualTraining.UI.VR
{
    public class HelpPanelVR : WorldSpacePanel
    {
        [SerializeField] InteractionButton closeButton;
        [SerializeField] InteractionButton controllerButton;
        [SerializeField] InteractionButton interactionUIButton;
        [SerializeField] InteractionButton interactionObjectButton;
        [SerializeField] InteractionButton locomotionButton;
        [SerializeField] GameObject panelController;
        [SerializeField] GameObject panelInteractionUI;
        [SerializeField] GameObject panelInteractionObject;
        [SerializeField] GameObject panelLocomotion;
        [SerializeField] GameObject panelMenu;

        [SerializeField] InteractionButton backButton;
        // Start is called before the first frame update
        protected override void Start()
        {
            defaultPosition = DatabaseManager.GetVRData().helpPosition.defaultPosition;
            base.Start();
            closeButton.OnClickEvent += CloseHelpPanel;
            controllerButton.OnClickEvent += OpenControllerPanel;
            interactionUIButton.OnClickEvent += OpenInteractionUIPanel;
            interactionObjectButton.OnClickEvent += OpenInteractionObjectPanel;
            locomotionButton.OnClickEvent += OpenLocomotionPanel;
            backButton.OnClickEvent += OpenMainMenu;
            gameObject.SetActive(false);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            closeButton.OnClickEvent -= CloseHelpPanel;
            controllerButton.OnClickEvent -= OpenControllerPanel;
            interactionUIButton.OnClickEvent -= OpenInteractionUIPanel;
            interactionObjectButton.OnClickEvent -= OpenInteractionObjectPanel;
            locomotionButton.OnClickEvent -= OpenLocomotionPanel;
            backButton.OnClickEvent -= OpenMainMenu;
            backButton.gameObject.SetActive(false);
        }

        void CloseHelpPanel()
        {
            SetActivePanel(false);
        }

        void OpenControllerPanel()
        {
            OpenSubPanel(panelController);
        }

        void OpenInteractionUIPanel()
        {
            OpenSubPanel(panelInteractionUI);
        }

        void OpenInteractionObjectPanel()
        {
            OpenSubPanel(panelInteractionObject);
        }

        void OpenLocomotionPanel()
        {
            OpenSubPanel(panelLocomotion);
        }

        void OpenSubPanel(GameObject panel)
        {
            panelMenu.SetActive(false);
            backButton.SetActive(true);
            panel.SetActive(true);
        }

        void OpenMainMenu()
        {
            backButton.SetActive(false);
            panelMenu.SetActive(true);
            panelController.SetActive(false);
            panelInteractionObject.SetActive(false);
            panelInteractionUI.SetActive(false);
            panelLocomotion.SetActive(false);
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(HelpPanelVR))]
    public class OverrideHelp : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    HelpPanelVR panel = (HelpPanelVR)target;

                    vrData.helpPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.helpPosition.defaultPosition;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }

            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }
#endif
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System;
using TMPro;

namespace VirtualTraining.UI.VR
{
    public class TestDummyVR : MonoBehaviour
    {
        public GameObject canvasSystem;

        public GameObject panel1;
        public GameObject panel2;
        public GameObject panel3;

        public InteractionButton backPanel2;
        public InteractionButton backPanel3;

        public TextMeshProUGUI HeaderMateri;
        public TextMeshProUGUI HeaderMater2;
        public TextMeshProUGUI HeaderSubMateri;

        private void Start()
        {
            backPanel2.OnClickEvent += GoToPanel1;
            backPanel3.OnClickEvent += GoToPanel2;

        }

        private void OnDestroy()
        {
            backPanel2.OnClickEvent -= GoToPanel1;
            backPanel3.OnClickEvent -= GoToPanel2;

        }

        public void GoToPanel(string textTitle, int kedalaman)
        {
            if (kedalaman == 0)
            {
                Debug.Log("materi clicked");
                HeaderMateri.text = textTitle;
                HeaderMater2.text = textTitle;
                GoToPanel2();
            }
            else if(kedalaman == 1)
            {
                Debug.Log("submateri clicked");
                HeaderSubMateri.text = textTitle;
                GoToPanel3();
            }
            else
            {
                Debug.Log("close clicked");
                ClosePanelSystem();
            }
        }

        void GoToPanel1()
        {
            panel1.SetActive(true);
            panel2.SetActive(false);
            panel3.SetActive(false);
        }

        void GoToPanel2()
        {
            panel2.SetActive(true);
            panel1.SetActive(false);
            panel3.SetActive(false);
        }

        void GoToPanel3()
        {
            panel3.SetActive(true);
            panel2.SetActive(false);
            panel1.SetActive(false);
        }

        void ClosePanelSystem()
        {
            canvasSystem.SetActive(false);
        }
    }
}

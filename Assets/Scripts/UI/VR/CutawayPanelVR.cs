using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.UI.VR
{
    public class CutawayPanelVR : WorldSpacePanel
    {
        [SerializeField] InteractionButton resetButton;

        [SerializeField] InteractionToggle planeToggle;
        [SerializeField] InteractionToggle boxToggle;
        [SerializeField] InteractionToggle cornerToggle;

        [SerializeField] InteractionToggle positionButton;
        [SerializeField] InteractionToggle rotationButton;

        [SerializeField] Slider sliderCutawayX;
        [SerializeField] Slider sliderCutawayY;
        [SerializeField] Slider sliderCutawayZ;

        [SerializeField] GameObject panel1;
        [SerializeField] GameObject panel2;

        float minXPosition;
        float maxXPosition;
        float minYPosition;
        float maxYPosition;
        float minZPosition;
        float maxZPosition;

        float minXRotation;
        float maxXRotation;
        float minYRotation;
        float maxYRotation;
        float minZRotation;
        float maxZRotation;

        Vector3 slidervaluePosition;
        Vector3 slidervalueRotation;
        Transform gizmoTransform;

        Vector3 defaultPositionCutaway;
        bool isAllowUpdate;

        [SerializeField] TextMeshProUGUI[] allGenericText;

        protected override void OnEnable()
        {
            base.OnEnable();

            EventManager.TriggerEvent(new GetGizmoTransformEvent((transform) =>
            {
                gizmoTransform = transform;

                if (gizmoTransform != null)
                {
                    GetDefaultPostion();
                    slidervaluePosition = defaultPositionCutaway;
                    slidervalueRotation = gizmoTransform.eulerAngles;
                }

            }));
        }
        void GetDefaultPostion()
        {
            defaultPositionCutaway = (DatabaseManager.GetCutawayConfig().maxPos + DatabaseManager.GetCutawayConfig().minPos) / 2;
        }
        private void ResetCallback()
        {
            EventManager.TriggerEvent(new ResetCutawayEvent());
            if (gizmoTransform != null)
            {
                isAllowUpdate = false;
                GetDefaultPostion();
                slidervaluePosition = defaultPositionCutaway;
                slidervalueRotation = Vector3.zero;

                if (rotationButton.IsOn)
                {
                    sliderCutawayX.value = 0;
                    sliderCutawayY.value = 0;
                    sliderCutawayZ.value = 0;
                }
                else
                {
                    sliderCutawayX.value = slidervaluePosition.x;
                    sliderCutawayY.value = slidervaluePosition.y;
                    sliderCutawayZ.value = slidervaluePosition.z;
                }
                isAllowUpdate = true;
            }
        }

        protected override void Start()
        {
            EventManager.AddListener<CutawayEvent>(CutawayListener);
            base.Start();
            defaultPosition = DatabaseManager.GetVRData().cutawayPosition.defaultPosition;

            resetButton.OnClickEvent += ResetCallback;
            planeToggle.OnStateChangedEvent += PlaneToggleCallback;
            boxToggle.OnStateChangedEvent += BoxToggleCallback;
            cornerToggle.OnStateChangedEvent += CornerToggleCallback;
            positionButton.OnStateChangedEvent += PositionToggleCallback;
            rotationButton.OnStateChangedEvent += RotationToggleCallback;

            GetDefaultPostion();
            SetActivePanel(false);

            for (int i = 0; i < allGenericText.Length; i++)
            {
                allGenericText[i].color = theme.genericTextColor;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<CutawayEvent>(CutawayListener);

            resetButton.OnClickEvent -= ResetCallback;
            planeToggle.OnStateChangedEvent -= PlaneToggleCallback;
            boxToggle.OnStateChangedEvent -= BoxToggleCallback;
            cornerToggle.OnStateChangedEvent -= CornerToggleCallback;
            positionButton.OnStateChangedEvent -= PositionToggleCallback;
            rotationButton.OnStateChangedEvent -= RotationToggleCallback;

        }

        private void CornerToggleCallback(bool on)
        {
            if (on)
            {
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Corner));
                SetDefaultPosition(DatabaseManager.GetCutawayConfig().minPos, DatabaseManager.GetCutawayConfig().maxPos);
                SetDefaultRotation();
                OpenPanel2();
            }
        }

        private void BoxToggleCallback(bool on)
        {
            if (on)
            {
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Box));
                SetDefaultPosition(DatabaseManager.GetCutawayConfig().minPos, DatabaseManager.GetCutawayConfig().maxPos);
                SetDefaultRotation();
                OpenPanel2();
            }
        }

        private void PlaneToggleCallback(bool on)
        {
            if (on)
            {
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Plane));
                SetDefaultPosition(DatabaseManager.GetCutawayConfig().minPos, DatabaseManager.GetCutawayConfig().maxPos);
                SetDefaultRotation();
                OpenPanel2();
            }
        }

        void OpenPanel2()
        {
            //panel1.SetActive(false);
            panel2.SetActive(true);
            positionButton.IsOn = true;
        }

        private void CutawayListener(CutawayEvent e)
        {
            planeToggle.OnStateChangedEvent -= PlaneToggleCallback;
            boxToggle.OnStateChangedEvent -= BoxToggleCallback;
            cornerToggle.OnStateChangedEvent -= CornerToggleCallback;

            if (e.cutaway != null)
            {
                switch (e.cutaway.state)
                {
                    case CutawayState.None:

                        SetActivePanel(false);
                        panel1.SetActive(true);
                        panel2.SetActive(false);

                        planeToggle.IsOn = false;
                        boxToggle.IsOn = false;
                        cornerToggle.IsOn = false;

                        break;
                    case CutawayState.Plane:

                        SetActivePanel(true);
                        panel1.SetActive(true);
                        panel2.SetActive(false);
                        slidervaluePosition = e.cutaway.position;
                        planeToggle.IsOn = true;

                        break;
                    case CutawayState.Box:

                        SetActivePanel(true);
                        panel1.SetActive(true);
                        panel2.SetActive(false);
                        slidervaluePosition = e.cutaway.position;
                        boxToggle.IsOn = true;

                        break;
                    case CutawayState.Corner:

                        SetActivePanel(true);
                        panel1.SetActive(true);
                        panel2.SetActive(false);
                        slidervaluePosition = e.cutaway.position;
                        cornerToggle.IsOn = true;

                        break;
                }
            }
            else
            {
                SetActivePanel(false);
                panel1.SetActive(true);
                panel2.SetActive(false);

                planeToggle.IsOn = false;
                boxToggle.IsOn = false;
                cornerToggle.IsOn = false;

                slidervaluePosition = defaultPositionCutaway;
                slidervalueRotation = Vector3.zero;
            }

            planeToggle.OnStateChangedEvent += PlaneToggleCallback;
            boxToggle.OnStateChangedEvent += BoxToggleCallback;
            cornerToggle.OnStateChangedEvent += CornerToggleCallback;
        }

        public void ResetPositionSlider()
        {
            panel1.SetActive(true);
            panel2.SetActive(false);

            slidervaluePosition = defaultPositionCutaway;
            slidervalueRotation = Vector3.zero;
            if (gizmoTransform != null)
            {
                gizmoTransform.transform.localPosition = slidervaluePosition;
                gizmoTransform.transform.localEulerAngles = slidervaluePosition;
            }
            planeToggle.IsOn = false;
            boxToggle.IsOn = false;
            cornerToggle.IsOn = false;
        }

        void PositionToggleCallback(bool isOn)
        {
            if (isOn)
            {
                //xButton.IsOn = true;
                isAllowUpdate = false;

                sliderCutawayX.minValue = minXPosition;
                sliderCutawayX.maxValue = maxXPosition;
                sliderCutawayX.value = slidervaluePosition.x;

                sliderCutawayY.minValue = minYPosition;
                sliderCutawayY.maxValue = maxYPosition;
                sliderCutawayY.value = slidervaluePosition.y;

                sliderCutawayZ.minValue = minZPosition;
                sliderCutawayZ.maxValue = maxZPosition;
                sliderCutawayZ.value = slidervaluePosition.z;

                isAllowUpdate = true;
            }
        }

        void RotationToggleCallback(bool isOn)
        {
            if (isOn)
            {
                isAllowUpdate = false;

                //xButton.IsOn = true;
                sliderCutawayX.minValue = minXRotation;
                sliderCutawayX.maxValue = maxXRotation;
                sliderCutawayX.value = slidervalueRotation.x;

                sliderCutawayY.minValue = minYRotation;
                sliderCutawayY.maxValue = maxYRotation;
                sliderCutawayY.value = slidervalueRotation.y;

                sliderCutawayZ.minValue = minZRotation;
                sliderCutawayZ.maxValue = maxZRotation;
                sliderCutawayZ.value = slidervalueRotation.z;

                isAllowUpdate = true;
            }
        }

        void SetDefaultPosition(Vector3 minPos, Vector3 maxPos)
        {
            minXPosition = minPos.x;
            minYPosition = minPos.y;
            minZPosition = minPos.z;

            maxXPosition = maxPos.x;
            maxYPosition = maxPos.y;
            maxZPosition = maxPos.z;
        }
        void SetDefaultRotation()
        {
            minXRotation = 0;
            minYRotation = 0;
            minZRotation = 0;

            maxXRotation = 360;
            maxYRotation = 360;
            maxZRotation = 360;
        }

        public void OnChangedSliderX()
        {
            if (isAllowUpdate == false)
                return;

            if (positionButton.IsOn)
            {
                slidervaluePosition.x = sliderCutawayX.value;
            }
            else
            {
                slidervalueRotation.x = sliderCutawayX.value;
            }

            if (gizmoTransform == null)
            {
                EventManager.TriggerEvent(new GetGizmoTransformEvent((transform) =>
                {
                    gizmoTransform = transform;
                    if (gizmoTransform != null)
                    {
                        slidervaluePosition = gizmoTransform.position;
                        slidervalueRotation = gizmoTransform.eulerAngles;
                    }
                }));
            }
            else
            {
                gizmoTransform.transform.localPosition = slidervaluePosition;
                gizmoTransform.transform.localEulerAngles = slidervalueRotation;
            }
        }

        public void OnChangedSliderY()
        {
            if (isAllowUpdate == false)
                return;

            if (positionButton.IsOn)
            {
                slidervaluePosition.y = sliderCutawayY.value;
            }
            else
            {
                slidervalueRotation.y = sliderCutawayY.value;
            }

            if (gizmoTransform == null)
            {
                EventManager.TriggerEvent(new GetGizmoTransformEvent((transform) =>
                {
                    gizmoTransform = transform;
                    if (gizmoTransform != null)
                    {
                        slidervaluePosition = gizmoTransform.position;
                        slidervalueRotation = gizmoTransform.eulerAngles;
                    }
                }));
            }
            else
            {
                gizmoTransform.transform.localPosition = slidervaluePosition;
                gizmoTransform.transform.localEulerAngles = slidervalueRotation;
            }
        }

        public void OnChangedSliderZ()
        {
            if (isAllowUpdate == false)
                return;

            if (positionButton.IsOn)
            {
                slidervaluePosition.z = sliderCutawayZ.value;
            }
            else
            {
                slidervalueRotation.z = sliderCutawayZ.value;
            }

            if (gizmoTransform == null)
            {
                EventManager.TriggerEvent(new GetGizmoTransformEvent((transform) =>
                {
                    gizmoTransform = transform;
                    if (gizmoTransform != null)
                    {
                        slidervaluePosition = gizmoTransform.position;
                        slidervalueRotation = gizmoTransform.eulerAngles;
                    }
                }));
            }
            else
            {
                gizmoTransform.transform.localPosition = slidervaluePosition;
                gizmoTransform.transform.localEulerAngles = slidervalueRotation;
            }
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(CutawayPanelVR))]
    public class OverrideCutaway : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    CutawayPanelVR panel = (CutawayPanelVR)target;

                    vrData.cutawayPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.cutawayPosition.defaultPosition;
                    DatabaseManager.SetVRData(vrData); ;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }

            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }

#endif

}

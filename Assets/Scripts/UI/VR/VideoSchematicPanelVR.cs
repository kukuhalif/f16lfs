using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;
using UnityEngine.UI;
using VirtualTraining.UI.Desktop;
using TMPro;
namespace VirtualTraining.UI.VR
{
    public class VideoSchematicPanelVR : WorldSpacePanel
    {
        [SerializeField] InteractionButton closeButton;

        [SerializeField] RawImage ImageTexture;
        [SerializeField] RawImage VideoRawTexture;
        [SerializeField] VideoController videoController;
        [SerializeField] InteractionButton showTreeViewButton;
        [SerializeField] InteractionButton hideTreeViewButton;
        [SerializeField] TreeViewUISchematic treeViewSchematic;
        [SerializeField] Image treeViewBackground;
        [SerializeField] RenderTexture defaultRenderTexture;
        [SerializeField] float defaultWidth;
        [SerializeField] AspectRatioFitter imageAspectRatio;
        [SerializeField] AspectRatioFitter videoAspectRatio;
        [SerializeField] RectTransform headerRect;
        [SerializeField] RectTransform bottomRect;
        private Vector2 targetSize;

        private List<Schematic> schematics;
        private Schematic currentSchematic;
        private Transform schematicTarget;

        protected float AnimationDuration = 0.5f;
        [SerializeField] private TextMeshProUGUI titleText;

        protected override void Start()
        {
            defaultPosition = DatabaseManager.GetVRData().videoSchematicPosition.defaultPosition;
            base.Start();
            EventManager.AddListener<SetSchematicDataEvent>(SetSchematicDataListener);
            EventManager.AddListener<ShowSchematicEvent>(ShowSchematicListener);
            EventManager.AddListener<ShowSchematicLinkEvent>(SchematicLinkListener);
            EventManager.AddListener<VoiceSoundEvent>(NarationSoundListener);

            showTreeViewButton.OnClickEvent += ShowTreeViewButtonListener;
            hideTreeViewButton.OnClickEvent += HideTreeViewButtonListener;
            closeButton.OnClickEvent += CloseButtonListener;
            SetActivePanel(false);
        }

        protected override void OnDestroy()
        {
            EventManager.RemoveListener<SetSchematicDataEvent>(SetSchematicDataListener);
            EventManager.RemoveListener<ShowSchematicEvent>(ShowSchematicListener);
            EventManager.RemoveListener<ShowSchematicLinkEvent>(SchematicLinkListener);
            EventManager.RemoveListener<VoiceSoundEvent>(NarationSoundListener);

            showTreeViewButton.OnClickEvent -= ShowTreeViewButtonListener;
            hideTreeViewButton.OnClickEvent -= HideTreeViewButtonListener;
            closeButton.OnClickEvent -= CloseButtonListener;
            base.OnDestroy();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (schematicTarget != null)
                schematicTarget.localScale = Vector3.one;
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
        }

        private void OnDisable()
        {
            videoController.ReleaseTexture();
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
            //treeViewSchematic.Show(false, 0f);
        }

        private void ShowTreeViewButtonListener()
        {
            treeViewSchematic.Show(true, AnimationDuration);
        }

        private void HideTreeViewButtonListener()
        {
            treeViewSchematic.Show(false, AnimationDuration);
        }

        private void PlayMateriListener(MateriEvent e)
        {
            DisableOldSchematic();
            SetActivePanel(false);
        }

        void CloseButtonListener()
        {
            DisableOldSchematic();
            SetActivePanel(false);
        }

        private void DisableOldSchematic()
        {
            videoController.SetActive(false);
            ImageTexture.gameObject.SetActive(false);
            VideoRawTexture.gameObject.SetActive(false);
        }

        void NarationSoundListener(VoiceSoundEvent e)
        {
            if (e.goAudio != this.gameObject)
            {
                videoController.Pause();
            }
        }

        private void SetSchematicDataListener(SetSchematicDataEvent e)
        {
            currentSchematic = null;
            schematics = e.schematics;

            treeViewSchematic.Show(false, AnimationDuration);
            treeViewSchematic.SetSchematicData(e.schematics);

            if (schematics == null || schematics.Count == 0)
            {
                SetActivePanel(false);
                return;
            }

            for (int i = 0; i < schematics.Count; i++)
            {
                if (schematics[i].showImmediately)
                {
                    DisableOldSchematic();
                    SetActivePanel(true);
                    Show(schematics[i], true);
                    return;
                }
            }
        }
        private void ShowSchematicListener(ShowSchematicEvent e)
        {
            DisableOldSchematic();
            SetActivePanel(true);
            Show(e.schematic, e.restorePanel);
        }

        private void SchematicLinkListener(ShowSchematicLinkEvent e)
        {
            for (int i = 0; i < schematics.Count; i++)
            {
                if (e.id == schematics[i].descriptionId)
                {
                    DisableOldSchematic();
                    SetActivePanel(true);
                    Show(schematics[i], true);
                    return;
                }
            }

            Debug.LogError("schematic link not found");
        }

        private void Show(Schematic schematic, bool restorePanel)
        {
            videoController.Stop();

            if (schematic.image != null)
            {
                SetTargetHeight(schematic.image.width, schematic.image.height, false);
                imageAspectRatio.aspectRatio = (float)schematic.image.width / (float)schematic.image.height;
                SetTitleText(schematic.image.name);
                videoController.SetActive(false);
                ImageTexture.gameObject.SetActive(true);
                VideoRawTexture.gameObject.SetActive(false);
                ImageTexture.texture = schematic.image;
                currentSchematic = schematic;
                schematicTarget = ImageTexture.transform;

            }
            else if (schematic.video != null)
            {
                int sWidth = (int)schematic.video.width;
                int sHeight = (int)schematic.video.height;

                SetTargetHeight(sWidth, sHeight, true);
                videoAspectRatio.aspectRatio = (float)sWidth / (float)sHeight;

                if (VideoRawTexture.texture.height != sHeight || VideoRawTexture.texture.width != sWidth)
                {
                    RenderTexture newRT = new RenderTexture(sWidth, sHeight, defaultRenderTexture.depth);
                    VideoRawTexture.texture = newRT;
                    videoController.SetRenderTexture(newRT);
                }

                SetTitleText(schematic.video.name);
                videoController.SetActive(true);
                ImageTexture.gameObject.SetActive(false);
                VideoRawTexture.gameObject.SetActive(true);
                videoController.Play(schematic.video, schematic.isLooping);
                currentSchematic = schematic;
                schematicTarget = VideoRawTexture.transform;
            }

            schematicTarget.localScale = Vector3.one;
        }

        private void SetTargetHeight(float width, float height, bool video)
        {
            float additionalHeight = headerRect.rect.height;
            if (video)
                additionalHeight += bottomRect.rect.height;

            targetSize.x = defaultWidth;
            float aspectRatio = width / height;
            targetSize.y = (targetSize.x / aspectRatio) + additionalHeight;
        }

        void SetTitleText(string text)
        {
            if (titleText != null)
                titleText.text = text;
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(VideoSchematicPanelVR))]
    public class OverrideSchematic : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Save Position"))
            {
                if (EditorUtility.DisplayDialog("Saving", "save Position ?", "yes", "no"))
                {
                    VRData vrData = DatabaseManager.GetVRData();
                    VideoSchematicPanelVR panel = (VideoSchematicPanelVR)target;

                    vrData.videoSchematicPosition.defaultPosition = panel.gameObject.transform.parent.parent.parent.localPosition;
                    panel.defaultPosition = vrData.videoSchematicPosition.defaultPosition;
                    DatabaseManager.SetVRData(vrData); ;
                    Debug.Log("Save Position" + panel.gameObject.name + " Success");
                }
            }
            // will enable the default inpector UI 
            base.OnInspectorGUI();
        }
    }
#endif
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.Networking;
using TMPro;
using Models;
using Proyecto26;

public class ParseRestLogin : MonoBehaviour
{
    public const string APP_ID = "centos_parse_app_id";
    public const string MASTER_KEY = "centos_parse_master_key";
    public const string BASE_URL = "http://192.168.55.104:1337/parse";

    private RequestHelper currentRequest;

    public TMP_InputField usernameText;
    public TMP_InputField passwordText;
    public TMP_Text messageText;

    //private string username, password;
    [Serializable]
    private class LoginData
    {
        public string username;
        public string password;
    }
    LoginData loginData = new LoginData();

    [Serializable]
    private class LoginResponse
    {
        public string objectId;
        public string username;
        public string email;
        public string emailVerified;
        public string sessionToken;
        public string createdAt;
        public string updatedAt;  
    }
    LoginResponse loginResponse = new LoginResponse(); 

    private bool isLoginSuccess = false;

    // Start is called before the first frame update
    void Start()
    {
        RestClient.DefaultRequestHeaders["X-Parse-Application-Id"] = APP_ID;
        RestClient.DefaultRequestHeaders["X-Parse-REST-API-Key"] = MASTER_KEY;
    }

    private void OnDestroy()
    {
    }

    public void SignIn()
    {
        if (usernameText != null)
            loginData.username = usernameText.text;
        if (passwordText != null)
            loginData.password = passwordText.text;

        Debug.Log("email input: " + loginData.username + ", password input: " + loginData.password);

        /*byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(loginData));
        //var request = UnityWebRequest.Post("http://localhost:1337/parse/login", jsonLoginData);
        var request = new UnityWebRequest("http://localhost:1337/parse/login", "PUT");
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("X-Parse-Application-Id", APP_ID);
        request.SetRequestHeader("X-Parse-REST-API-Key", MASTER_KEY);
        request.SetRequestHeader("X-Parse-Revocable-Session", "1");
        request.SetRequestHeader("Content-Type", "application/json");
        StartCoroutine(SendLoginRequest(request));*/

        currentRequest = new RequestHelper
        {
            Uri = BASE_URL + "/login",
            Headers = new Dictionary<string, string> {
                { "X-Parse-Application-Id", APP_ID },
                { "X-Parse-REST-API-Key", MASTER_KEY },
                { "X-Parse-Revocable-Session", "1" },
                { "Content-Type", "application/json" }
            },
            Body = loginData,
            EnableDebug = true
        };
        RestClient.Post<LoginResponse>(currentRequest, (error, result, response) =>
        {
            if (error != null)
            {
                Debug.Log("Network error has occured: " + error.Message);
                messageText.text = "Network error has occured: " + error.Message;
            }
            else
            {
                //loginResponse = JsonUtility.FromJson<LoginResponse>(response.);
                Debug.LogFormat("User signed in successfully: {0} ({1})", response.username, response.objectId);
                messageText.text = "User signed in successfully: " + response.username + " (" + response.objectId + ")\n ** LOADING SCENE ** ";
                isLoginSuccess = true;
            }
        });

        //StartCoroutine(HideMessage());
    }

    /*IEnumerator SendLoginRequest(UnityWebRequest req)
    {
        yield return req.SendWebRequest();
        if (req.result == UnityWebRequest.Result.Success)
        {
            string result = req.downloadHandler.text;
            loginResponse = JsonUtility.FromJson<LoginResponse>(result);
            Debug.LogFormat("User signed in successfully: {0} ({1})", loginResponse.username, loginResponse.objectId);
            messageText.text = "User signed in successfully: " + loginResponse.username + " (" + loginResponse.objectId + ")";
            isLoginSuccess = true;
        }
        else
        {
            Debug.Log("Network error has occured: " + req.GetResponseHeader(""));
            messageText.text = "Network error has occured: " + req.GetResponseHeader("");
        }
    }*/

    public void SignOut()
    {
        if (loginResponse != null)
        {
            /*try
            {
                var request = UnityWebRequest.Post("http://localhost:1337/parse/logout", "");
                request.SetRequestHeader("X-Parse-Application-Id", APP_ID);
                request.SetRequestHeader("X-Parse-REST-API-Key", MASTER_KEY);
                request.SetRequestHeader("X-Parse-Session-Token", loginResponse.sessionToken);
                StartCoroutine(SendLogoutRequest(request));
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                messageText.text = e.Message;
            }*/
            currentRequest = new RequestHelper
            {
                Uri = BASE_URL + "/logout",
                Headers = new Dictionary<string, string> {
                    { "X-Parse-Application-Id", APP_ID },
                    { "X-Parse-REST-API-Key", MASTER_KEY },
                    { "X-Parse-Session-Token", loginResponse.sessionToken }
                },
                EnableDebug = true
            };
            RestClient.Post(currentRequest, (error, result) =>
            {
                if (error != null)
                {
                    Debug.Log("Network error has occured: " + error.Message);
                    messageText.text = "Network error has occured: " + error.Message;
                }
                else
                {
                    Debug.Log("User logged out successfully!");
                    messageText.text = "User logged out successfully!";
                    isLoginSuccess = false;
                    loginResponse = new LoginResponse();
                }
                StartCoroutine(HideMessage());
            });
        }
        else
        {
            Debug.Log("User not logged in!");
            messageText.text = "User not logged in!";
        }
    }

    /*IEnumerator SendLogoutRequest(UnityWebRequest req)
    {
        yield return req.SendWebRequest();
        if (req.result == UnityWebRequest.Result.Success)
        {
            Debug.Log("User logged out successfully!");
            messageText.text = "User logged out successfully!";
            isLoginSuccess = false;
            loginResponse = new LoginResponse();
            StartCoroutine(HideMessage());
        }
        else
        {
            Debug.Log("Network error has occured: " + req.GetResponseHeader(""));
            messageText.text = "Network error has occured: " + req.GetResponseHeader("");
        }
    }*/

    public void GetCurrentUser()
    {
        if (isLoginSuccess && loginResponse != null)
        {
            Debug.LogFormat("User {0} is logged in!", loginResponse.username);
            messageText.text = String.Format("User {0} is logged in!", loginResponse.username);
        }
        else
        {
            Debug.Log("User is not logged in!");
            messageText.text = "User is not logged in!";
        }
        StartCoroutine(HideMessage());
    }

    public void GetUser()
    {
        currentRequest = new RequestHelper
        {
            Uri = BASE_URL + "/users/xKUu6GVYOU",
            Headers = new Dictionary<string, string> {
                    { "X-Parse-Application-Id", APP_ID },
                    { "X-Parse-REST-API-Key", MASTER_KEY }
                },
            EnableDebug = true
        };
        RestClient.Get(currentRequest, (error, result) =>
        {
            if (error != null)
            {
                Debug.Log("Network error has occured: " + error.Message);
                messageText.text = "Network error has occured: " + error.Message;
            }
            else
            {
                Debug.Log("Response: " + result.Text);
                messageText.text = "Response: " + result.Text;
            }
            StartCoroutine(HideMessage());
        });
    }

    public void TestRequest()
    {
        currentRequest = new RequestHelper
        {
            Uri = "https://jsonplaceholder.typicode.com/todos/1",
            EnableDebug = true
        };
        RestClient.Get(currentRequest, (error, result) =>
        {
            if (error != null)
            {
                Debug.Log("Network error has occured: " + error.Message);
                messageText.text = "Network error has occured: " + error.Message;
            }
            else
            {
                Debug.Log("Response: " + result.Text);
                messageText.text = "Response: " + result.Text;
            }
            StartCoroutine(HideMessage());
        });
    }

    IEnumerator HideMessage()
    {
        yield return new WaitForSeconds(3);
        if (messageText != null)
            messageText.text = "";
        if (isLoginSuccess)
        {
            messageText.text = "Loading scene...";
            SceneManager.LoadScene("HelicopterSim");
        }
    }
}

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Auth;
using Firebase.Extensions;
using TMPro;

public class FirebaseLogin : MonoBehaviour
{
    FirebaseApp app;
    FirebaseAuth auth;

    public TMP_InputField emailText;
    public TMP_InputField passwordText;
    public TMP_Text messageText;

    private string email, password;

    private bool isLoginSuccess = false;

    // Start is called before the first frame update
    void Start()
    {
        // create app
        AppOptions options = new AppOptions();
        options.ApiKey = "AIzaSyBDuBmH8x8-laz6F2M3QIkiKLT4bTeR5kA";
        options.AppId = "1:791032978892:android:5fae7c7c25a186369c5169";
        options.ProjectId = "segaris-api";

        app = FirebaseApp.Create(options, "virtual training");

        // get auth
        //FirebaseAuth auth = FirebaseAuth.DefaultInstance;
        auth = FirebaseAuth.GetAuth(app);
    }

    private void OnDestroy()
    {
        app.Dispose();
        auth.Dispose();
    }

    public void SignIn()
    {
        if (emailText != null)
            email = emailText.text;
        if (passwordText != null)
            password = passwordText.text;

        Debug.Log("email input: " + email + ", password input: " + password);

        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                messageText.text = "Sign in was cancelled!";
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception.Message);
                messageText.text = "Sign in encountered an error: " + task.Exception.Message;
                return;
            }
            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.Email, newUser.UserId);
            messageText.text = "User signed in successfully: " + newUser.Email + " (" + newUser.UserId + ")";
            isLoginSuccess = true;
        });
        StartCoroutine(HideMessage());
    }

    public void SignOut()
    {
        auth.SignOut();
        Debug.Log("User logged out!");
        messageText.text = "User logged out!";
        StartCoroutine(HideMessage());
    }

    public void GetCurrentUser()
    {
        FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            //string name = user.DisplayName;
            string email = user.Email;
            System.Uri photo_url = user.PhotoUrl;
            // The user's Id, unique to the Firebase project.
            // Do NOT use this value to authenticate with your backend server, if you
            // have one; use User.TokenAsync() instead.
            string uid = user.UserId;
            Debug.LogFormat("User {0} is logged in!", email);
            messageText.text = String.Format("User {0} is logged in!", email);
        }
        else
        {
            Debug.Log("User is not logged in!");
            messageText.text = "User is not logged in!";
        }
        StartCoroutine(HideMessage());
    }

    IEnumerator HideMessage()
    {
        yield return new WaitForSeconds(3);
        if (messageText != null)
            messageText.text = "";
        if (isLoginSuccess)
        {
            messageText.text = "Loading scene...";
            SceneManager.LoadScene("HelicopterSim");
        }
    }
}

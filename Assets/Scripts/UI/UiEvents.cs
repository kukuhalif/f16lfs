using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class UpdateCameraPanelEvent : VirtualTrainingEvent
    {
        public int index;

        public UpdateCameraPanelEvent(int index)
        {
            this.index = index;
        }
    }

    public class MinimizeUIPanelEvent : VirtualTrainingEvent
    {
        public string panelName;
        public Action maximizeCallback;

        public MinimizeUIPanelEvent(string panelName, Action maximizeCallback)
        {
            this.panelName = panelName;
            this.maximizeCallback = maximizeCallback;
        }
    }

    public class TriggerMateriInsideTreeViewUIEvent : VirtualTrainingEvent
    {
        public MateriTreeElement materiTreeElement;

        public TriggerMateriInsideTreeViewUIEvent(MateriTreeElement materiTreeElement)
        {
            this.materiTreeElement = materiTreeElement;
        }
    }

    public class TriggerMaintenanceInsidePanelEvent : VirtualTrainingEvent
    {
        public MaintenanceTreeElement maintenanceTreeElement;

        public TriggerMaintenanceInsidePanelEvent(MaintenanceTreeElement maintenanceTreeElement)
        {
            this.maintenanceTreeElement = maintenanceTreeElement;
        }
    }

    public class TriggerTroubleshootInsidePanelEvent : VirtualTrainingEvent
    {
        public TroubleshootNode troubleshootNode;

        public TriggerTroubleshootInsidePanelEvent(TroubleshootNode troubleshootNode)
        {
            this.troubleshootNode = troubleshootNode;
        }
    }

    public class OnClosedPdfPanelEvent : VirtualTrainingEvent
    {

    }

    public class ShowPdfLinkEvent : VirtualTrainingEvent
    {
        public string id;

        public ShowPdfLinkEvent(string id)
        {
            this.id = id;
        }
    }

    public class CloseUIPanelEvent : VirtualTrainingEvent
    {
        public string name;

        public CloseUIPanelEvent(string name)
        {
            this.name = name;
        }
    }

    public class ShowUIPanelEvent : VirtualTrainingEvent
    {
        public GameObject panelObj;

        public ShowUIPanelEvent(GameObject panelObj)
        {
            this.panelObj = panelObj;
        }
    }

    public class ShowSchematicEvent : VirtualTrainingEvent
    {
        public Schematic schematic;
        public bool restorePanel;

        public ShowSchematicEvent(Schematic schematic, bool restorePanel)
        {
            this.schematic = schematic;
            this.restorePanel = restorePanel;
        }
    }

    public class ShowSchematicLinkEvent : VirtualTrainingEvent
    {
        public string id;

        public ShowSchematicLinkEvent(string id)
        {
            this.id = id;
        }
    }

    public class ObjectListPanelEvent : VirtualTrainingEvent
    {
        public bool open;

        public ObjectListPanelEvent(bool open)
        {
            this.open = open;
        }
    }

    public class SwitchUIThemeEvent : VirtualTrainingEvent
    {
        public int index;

        public SwitchUIThemeEvent(int index)
        {
            this.index = index;
        }
    }

    public class ShowAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class MinimizeAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class CloseAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class ResetAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class CloseConfirmationPanelEvent : VirtualTrainingEvent
    {

    }

    public class ObjectListToggleStateChangedEvent : VirtualTrainingEvent
    {
        public int sourceState;
        public int newState;
        public TreeElement element;

        public ObjectListToggleStateChangedEvent(int sourceState, int newState, TreeElement element)
        {
            this.sourceState = sourceState;
            this.newState = newState;
            this.element = element;
        }
    }

    public class MaintenancePanelUpdatedEvent : VirtualTrainingEvent
    {

    }

    public class FlightScenarioPanelUpdatedEvent : VirtualTrainingEvent
    {

    }

    public class ObjectListPanelUpdatedEvent : VirtualTrainingEvent
    {

    }

    public class LandingPageEnabledEvent : VirtualTrainingEvent
    {
        public bool enabled;

        public LandingPageEnabledEvent(bool enabled)
        {
            this.enabled = enabled;
        }
    }

    public class FullscreenPanelEvent : VirtualTrainingEvent
    {
        public bool fullscreen;
        public GameObject panel;

        public FullscreenPanelEvent(bool fullscreen, GameObject panel)
        {
            this.fullscreen = fullscreen;
            this.panel = panel;
        }
    }

    public class ShowAboutPanelEvent : VirtualTrainingEvent
    {
        public Action onClosePanelCallback;

        public ShowAboutPanelEvent(Action onClosePanelCallback)
        {
            this.onClosePanelCallback = onClosePanelCallback;
        }
    }

    public class VoiceSoundEvent : VirtualTrainingEvent
    {
        public GameObject goAudio;

        public VoiceSoundEvent(GameObject goAudio)
        {
            this.goAudio = goAudio;
        }
    }

    public class OpenPanelVREvent : VirtualTrainingEvent
    {
        public bool enabled;

        public OpenPanelVREvent(bool enabled)
        {
            this.enabled = enabled;
        }
    }

    public class ResetPanelVREvent : VirtualTrainingEvent
    {

    }

    public class ObjectInteractionPartNumberPanelEvent : VirtualTrainingEvent
    {
        public bool show;

        public ObjectInteractionPartNumberPanelEvent(bool show)
        {
            this.show = show;
        }
    }

    public class PartNumberPanelEvent : VirtualTrainingEvent
    {
        public bool open;

        public PartNumberPanelEvent(bool open)
        {
            this.open = open;
        }
    }
}
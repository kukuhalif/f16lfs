using Paroxe.PdfRenderer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIPDFMateri : TreeViewUI
    {
        class ManualTreeElement : TreeElement
        {
            public PDFAsset pdf;

            public override string GetDuplicateJsonData()
            {
                return "";
            }

            public override void SetDuplicatedJsonData(string data)
            {

            }

            public ManualTreeElement(PDFAsset pdf, string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
            {
                this.pdf = pdf;
            }
        }

        List<PdfData> currentPdfs = new List<PdfData>();

        protected override void Start()
        {
            base.Start();
        }

        public void GeneratePdf(List<PdfData> pdfs)
        {
            ClearElements();

            currentPdfs = pdfs;

            ManualTreeElement root = new ManualTreeElement(null, "root", -1, 0, true);
            root.children = new List<TreeElement>();

            for (int i = 0; i < pdfs.Count; i++)
            {
                ManualTreeElement element = new ManualTreeElement(pdfs[i].pdfAsset, pdfs[i].pdfAsset.name, 1, i + 1, false);
                root.children.Add(element);
            }

            GenerateElements(root);
        }


        protected override void ClickedElementCallback(TreeElement element)
        {
            ManualTreeElement pdfElement = element as ManualTreeElement;

            for (int i = 0; i < currentPdfs.Count; i++)
            {
                if (currentPdfs[i].pdfAsset == pdfElement.pdf)
                {
                    EventManager.TriggerEvent(new ShowPdfEvent(currentPdfs[i]));
                    break;
                }
            }
        }

        protected override TreeViewUIElement GetElementTemplate()
        {
            GameObject template = Resources.Load("UI/Tree View Element Template") as GameObject;
            return template.GetComponent<TreeViewUIElement>();
        }
    }
}

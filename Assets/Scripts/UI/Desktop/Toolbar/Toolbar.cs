using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DanielLochner.Assets.SimpleScrollSnap;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.UI.Desktop
{
    public class Toolbar : UIElement
    {
        [SerializeField] SimpleScrollSnap simpleScrollSnap;
        [SerializeField] InteractionButton prevButton;
        [SerializeField] InteractionButton nextButton;
        [SerializeField] Image background;

        #region toolbar button

        [SerializeField] InteractionToggle rotateMode;
        [SerializeField] InteractionToggle multipleSelectionMode;
        [SerializeField] InteractionToggle pullApartMode;
        [SerializeField] InteractionButton resetCamera;
        [SerializeField] InteractionButton resetObject;
        [SerializeField] InteractionToggle lockCameraMode;
        [SerializeField] InteractionToggle xRayMode;
        [SerializeField] InteractionToggle cutawayMode;
        [SerializeField] InteractionToggle visible;
        [SerializeField] InteractionButton hide;
        [SerializeField] InteractionToggle partObjectList;
        [SerializeField] CutawayPanel cutawayPanel;
        [SerializeField] InteractionToggle partNumber;

        #endregion

        protected override void Start()
        {
            base.Start();

            prevButton.OnClickEvent += PrevListener;
            nextButton.OnClickEvent += NextListener;

            rotateMode.OnStateChangedEvent += RotateMode_OnChangeStateEvent;
            multipleSelectionMode.OnStateChangedEvent += MultipleSelectionMode_OnChangeStateEvent;
            pullApartMode.OnStateChangedEvent += PullApartMode_OnChangeStateEvent;
            resetCamera.OnClickEvent += ResetCamera_OnChangeStateEvent;
            resetObject.OnClickEvent += ResetObject_OnChangeStateEvent;
            lockCameraMode.OnStateChangedEvent += LockCamera_OnChangeStateEvent;
            xRayMode.OnStateChangedEvent += XRayMode_OnChangeStateEvent;
            cutawayMode.OnStateChangedEvent += CutawayMode_OnChangeStateEvent;
            visible.OnStateChangedEvent += Visible_OnChangeStateEvent;
            hide.OnClickEvent += Hide_OnChangeStateEvent;
            partObjectList.OnStateChangedEvent += PartObjectList_OnChangeStateEvent;
            partNumber.OnStateChangedEvent += PartNumber_OnStateChangedEvent;

            EventManager.AddListener<HideObjectEvent>(HideObjectListener);
            EventManager.AddListener<CutawayEvent>(CutawayListener);
            EventManager.AddListener<ObjectListPanelEvent>(ObjectListPanelListener);
            EventManager.AddListener<DisableCameraMovementEvent>(DisableCameraMovementListener);
            EventManager.AddListener<SceneReadyEvent>(InitDoneListener);
            EventManager.AddListener<ResetObjectInteractionModeEvent>(DefaultModeListener);
            EventManager.AddListener<PartNumberPanelEvent>(PartNumberPanelListener);

            DefaultState();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            prevButton.OnClickEvent -= PrevListener;
            nextButton.OnClickEvent -= NextListener;

            rotateMode.OnStateChangedEvent -= RotateMode_OnChangeStateEvent;
            multipleSelectionMode.OnStateChangedEvent -= MultipleSelectionMode_OnChangeStateEvent;
            pullApartMode.OnStateChangedEvent -= PullApartMode_OnChangeStateEvent;
            resetCamera.OnClickEvent -= ResetCamera_OnChangeStateEvent;
            resetObject.OnClickEvent -= ResetObject_OnChangeStateEvent;
            lockCameraMode.OnStateChangedEvent -= LockCamera_OnChangeStateEvent;
            xRayMode.OnStateChangedEvent -= XRayMode_OnChangeStateEvent;
            cutawayMode.OnStateChangedEvent -= CutawayMode_OnChangeStateEvent;
            visible.OnStateChangedEvent -= Visible_OnChangeStateEvent;
            hide.OnClickEvent -= Hide_OnChangeStateEvent;
            partObjectList.OnStateChangedEvent -= PartObjectList_OnChangeStateEvent;
            partNumber.OnStateChangedEvent -= PartNumber_OnStateChangedEvent;

            EventManager.RemoveListener<HideObjectEvent>(HideObjectListener);
            EventManager.RemoveListener<CutawayEvent>(CutawayListener);
            EventManager.RemoveListener<ObjectListPanelEvent>(ObjectListPanelListener);
            EventManager.RemoveListener<DisableCameraMovementEvent>(DisableCameraMovementListener);
            EventManager.RemoveListener<ResetObjectInteractionModeEvent>(DefaultModeListener);
            EventManager.RemoveListener<PartNumberPanelEvent>(PartNumberPanelListener);
        }

        private void PrevListener()
        {
            simpleScrollSnap.GoToPreviousPanel();
        }

        private void NextListener()
        {
            simpleScrollSnap.GoToNextPanel();
        }

        protected override void ApplyTheme()
        {
            background.color = theme.panelTitleColor;
        }

        #region event listener

        private void HideObjectListener(HideObjectEvent e)
        {
            visible.OnStateChangedEvent -= Visible_OnChangeStateEvent;
            visible.IsOn = true;
            visible.OnStateChangedEvent += Visible_OnChangeStateEvent;
        }

        private void CutawayListener(CutawayEvent e)
        {
            if (e.cutaway == null)
            {
                cutawayMode.IsOn = false;
            }
            else
            {
                if (e.cutaway.state == CutawayState.None)
                    cutawayMode.IsOn = false;
                else
                    cutawayMode.IsOn = true;
            }
        }

        private void ObjectListPanelListener(ObjectListPanelEvent e)
        {
            partObjectList.OnStateChangedEvent -= PartObjectList_OnChangeStateEvent;
            partObjectList.IsOn = e.open;
            partObjectList.OnStateChangedEvent += PartObjectList_OnChangeStateEvent;
        }

        private void PartNumberPanelListener(PartNumberPanelEvent e)
        {
            partNumber.OnStateChangedEvent -= PartNumber_OnStateChangedEvent;
            partNumber.IsOn = e.open;
            partNumber.OnStateChangedEvent += PartNumber_OnStateChangedEvent;
        }

        private void DisableCameraMovementListener(DisableCameraMovementEvent e)
        {
            lockCameraMode.IsOn = e.disable;
        }

        private void DefaultModeListener(ResetObjectInteractionModeEvent e)
        {
            DefaultState();
        }

        private void InitDoneListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(InitDoneListener);
            DefaultState();
        }

        #endregion

        private void DefaultState()
        {
            EventManager.TriggerEvent(new ObjectInteractionRotateEvent(true));
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionLockCameraEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionCutawayEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
        }

        #region toolbar method

        private void PartObjectList_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionShowObjectListEvent(on));
        }

        private void Hide_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(false));
        }

        private void Visible_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));

            if (on)
            {
                visible.OnStateChangedEvent -= Visible_OnChangeStateEvent;
                visible.IsOn = false;
                visible.OnStateChangedEvent += Visible_OnChangeStateEvent;
            }
        }

        private void CutawayMode_OnChangeStateEvent(bool on)
        {
            if (on)
                cutawayPanel.ShowPanel();
            else
                cutawayPanel.ClosePanel();
        }

        private void XRayMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(on));
        }

        private void LockCamera_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionLockCameraEvent(on));
        }

        private void ResetObject_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
        }

        private void ResetCamera_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetCameraEvent());
        }

        private void PullApartMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(on));
        }

        private void MultipleSelectionMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(on));
        }

        private void RotateMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionRotateEvent(on));
        }

        private void PartNumber_OnStateChangedEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionPartNumberPanelEvent(on));
        }

        #endregion
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.UI.Desktop
{
    public class FlightScenarioPanel : UIPanel
    {
        [SerializeField] TreeViewUIFlightScenario treeViewFlightScenario;
        [SerializeField] InteractionButton backButton;

        bool isFlightScenarioDataExist = false;

        protected override void Awake()
        {
            base.Awake();
            if (backButton != null)
            {
                backButton.OnClickEvent += GoBack;
            }
        }

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<FlightScenarioUIEvent>(FlightScenarioUIEventListener);
            //EventManager.AddListener<CloseUIPanelEvent>(CloseFlightScenarioPanelListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<FlightScenarioUIEvent>(FlightScenarioUIEventListener);
            //EventManager.RemoveListener<CloseUIPanelEvent>(CloseFlightScenarioPanelListener);
            if (backButton != null)
            {
                backButton.OnClickEvent -= GoBack;
            }
        }

        private void GoBack()
        {
            ClosePanel();
            //EventManager.TriggerEvent(new DescriptionPanelEvent(VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT));
            //var scenarioName = VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.MateriData.name;
            //EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => VirtualTrainingSceneManager.LoadSceneProgress, () => "unloading " + scenarioName, () => VirtualTrainingSceneManager.SwitchSceneMode(SceneMode.Desktop), null, false));
            //VirtualTrainingSceneManager.CloseFlightScenario(VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.MateriData.name);
        }

        /*private void CloseFlightScenarioPanelListener(CloseUIPanelEvent e)
        {
            if (e.name == "Flight Scenario")
            {
                VirtualTrainingSceneManager.CloseFlightScenario(VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.MateriData.name);
                //EventManager.AddListener<InitializationUIDoneEvent>(FlightScenarioFinalDoneListener);
                //var scenarioName = VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.MateriData.name;
                //EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => VirtualTrainingSceneManager.LoadSceneProgress, () => "unloading " + scenarioName, () => VirtualTrainingSceneManager.SwitchSceneMode(SceneMode.Desktop), null, false));
            }
        }

        private static void FlightScenarioFinalDoneListener(InitializationUIDoneEvent e)
        {
            EventManager.RemoveListener<InitializationUIDoneEvent>(FlightScenarioFinalDoneListener);
            EventManager.TriggerEvent(new SceneReadyEvent(CURRENT_SCENE_CONFIG));
            EventManager.TriggerEvent(new FreePlayEvent());
            //SCENE_LOADER.CompleteProgress();
            Resources.UnloadUnusedAssets();
            EventManager.TriggerEvent(new DescriptionPanelEvent(LAST_MATERI_TREE_ELEMENT));
        }*/

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            isFlightScenarioDataExist = false;
            ClosePanel();
            treeViewFlightScenario.ClearElements();
        }

        protected override void PanelSizeUpdatedCallback()
        {
            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (isFlightScenarioDataExist)
                base.ShowAllPanelListener(e);
        }

        private void FlightScenarioUIEventListener(FlightScenarioUIEvent e)
        {
            if (e.materi == null)
            {
                isFlightScenarioDataExist = false;
                ClosePanel();
                treeViewFlightScenario.ClearElements();
                return;
            }

            if (e.materi.flightScenarioTree == null)
            {
                isFlightScenarioDataExist = false;
                ClosePanel();
                treeViewFlightScenario.ClearElements();

                Debug.LogError("flight scenario database missing");

                return;
            }

            isFlightScenarioDataExist = true;

            // get flight scenario tree root from flight scenario tree element list
            FlightScenarioTreeElement root = TreeElementUtility.ListToTree(e.materi.flightScenarioTree.GetData().treeElements);

            // generate tree view
            treeViewFlightScenario.GenerateTree(root);

            // show flight scenario panel
            ShowPanel(null, false, false, false, false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIMaintenance : TreeViewUI
    {
        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            EventManager.AddListener<TriggerMaintenanceInsidePanelEvent>(TriggerMaintenanceListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            EventManager.RemoveListener<TriggerMaintenanceInsidePanelEvent>(TriggerMaintenanceListener);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClearElements();
        }

        protected override TreeViewUIElement GetElementTemplate()
        {
            GameObject template = Resources.Load("UI/Tree View Element Maintenance Template") as GameObject;
            template.transform.localEulerAngles = Vector3.zero;
            return template.GetComponent<TreeViewUIElementMaintenance>();
        }

        protected override void ClickedElementCallback(TreeElement element)
        {
            MaintenanceTreeElement maintenance = element as MaintenanceTreeElement;
            EventManager.TriggerEvent(new MaintenancePlayEvent(maintenance));
        }

        private TreeViewUIElementMaintenance GetMaintenanceElement(TreeElement element)
        {
            return UiElementLookup[element.id] as TreeViewUIElementMaintenance;
        }

        protected override void SetupElementCallback(TreeViewUIElement uiElement, TreeElement element)
        {
            MaintenanceTreeElement maintenance = element as MaintenanceTreeElement;
            string text = string.IsNullOrEmpty(maintenance.data.description) ?
                maintenance.data.title + System.Environment.NewLine :
                maintenance.data.description + System.Environment.NewLine;
            uiElement.SetText(text);
            TreeViewUIElementMaintenance maintenanceElementUI = uiElement as TreeViewUIElementMaintenance;
            maintenanceElementUI.SetGetElementCallback(GetMaintenanceElement);
            maintenanceElementUI.SetMaintenanceData(maintenance.data);

            uiElement.gameObject.transform.localEulerAngles = Vector3.zero;
            uiElement.gameObject.transform.localPosition = new Vector3(uiElement.gameObject.transform.localPosition.x, uiElement.gameObject.transform.localPosition.y,
                0);


        }

        public void GenerateTree(MaintenanceTreeElement root)
        {
            GenerateElements(root);
        }

        private void TriggerMaintenanceListener(TriggerMaintenanceInsidePanelEvent e)
        {
            FocusAndClickElement(e.maintenanceTreeElement);
        }
    }
}

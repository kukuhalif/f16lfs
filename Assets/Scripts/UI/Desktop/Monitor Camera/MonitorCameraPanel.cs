﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;
using UnityEngine.UI;

namespace VirtualTraining.UI.Desktop
{
    public class MonitorCameraPanel : UIPanel
    {
        [SerializeField] RectTransform monitorPanel;
        [SerializeField] InteractionButton prevButton;
        [SerializeField] InteractionButton nextButton;
        [SerializeField] TextMeshProUGUI cameraName;

        int currentIndex;
        List<MonitorCamera> monitors;
        UIInteraction panelInteraction;

        RaycastHit hitInfo = new RaycastHit();
        bool isDefaultCursor;
        bool isMovePointerListenerAdded;
        GameObject currentRaycastTarget;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<MonitorCameraEvent>(MonitorCameraListener);
            prevButton.OnClickEvent += Prev;
            nextButton.OnClickEvent += Next;

            panelInteraction = monitorPanel.gameObject.AddComponent<UIInteraction>();
            panelInteraction.OnClickDown += OnLeftClickDown;
            panelInteraction.OnCursorEnter += OnCursorEnter;
            panelInteraction.OnCursorExit += OnCursorExit;

            AspectRatioFitter aspectRatio = monitorPanel.gameObject.AddComponent<AspectRatioFitter>();
            aspectRatio.aspectRatio = monitorPanel.rect.width / monitorPanel.rect.height;
            aspectRatio.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MonitorCameraEvent>(MonitorCameraListener);
            prevButton.OnClickEvent -= Prev;
            nextButton.OnClickEvent -= Next;
            panelInteraction.OnClickDown -= OnLeftClickDown;
            panelInteraction.OnCursorEnter -= OnCursorEnter;
            panelInteraction.OnCursorExit -= OnCursorExit;
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);

            if (isMovePointerListenerAdded)
            {
                VirtualTrainingInputSystem.OnMovePointer -= OnMovePointer;
                isMovePointerListenerAdded = false;
            }
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            cameraName.color = theme.genericTextColor;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            monitors = null;
            ClosePanel();
        }

        private void OnLeftClickDown()
        {
            // trigger panel selection because body panel has 2 ui element (body & monitor panel)
            SetOnTop();

            if (VirtualTrainingCamera.RaycastFromMonitorCamera(ref hitInfo, VirtualTrainingInputSystem.PointerPosition, monitorPanel))
            {
                EventManager.TriggerEvent(new MonitorCameraInteractionEvent(hitInfo.collider.gameObject));
            }
        }

        private void OnCursorEnter()
        {
            EventManager.TriggerEvent(new CursorEnterMonitorPanelEvent(true));
            VirtualTrainingInputSystem.OnMovePointer += OnMovePointer;
            isMovePointerListenerAdded = true;
        }

        private void OnCursorExit()
        {
            EventManager.TriggerEvent(new CursorEnterMonitorPanelEvent(false));
            if (isMovePointerListenerAdded)
            {
                VirtualTrainingInputSystem.OnMovePointer -= OnMovePointer;
                isMovePointerListenerAdded = false;
            }
        }

        private void OnMovePointer(Vector2 deltaPosition)
        {
            if (VirtualTrainingCamera.RaycastFromMonitorCamera(ref hitInfo, VirtualTrainingInputSystem.PointerPosition, monitorPanel))
            {
                if (currentRaycastTarget != hitInfo.collider.gameObject)
                {
                    VirtualTrainingCursor.DefaultCursor();
                    EventManager.TriggerEvent(new RaycastObjectFromMonitorCameraEvent(hitInfo.collider.gameObject));
                    currentRaycastTarget = hitInfo.collider.gameObject;
                    isDefaultCursor = false;
                }
            }
            else
            {
                if (!isDefaultCursor)
                {
                    VirtualTrainingCursor.DefaultCursor();
                    isDefaultCursor = true;
                }
                currentRaycastTarget = null;
            }

            VirtualTrainingCamera.OrbitMonitorCamera(deltaPosition);
        }

        private void MonitorCameraListener(MonitorCameraEvent e)
        {
            if (e.monitors == null || e.monitors.Count == 0)
            {
                monitors = null;
                ClosePanel();
                return;
            }

            monitors = e.monitors;
            currentIndex = 0;

            // new monitor camera is default camera monitor name (check monitor camera constructor)
            string camName = string.Equals(monitors[0].displayName, "new monitor camera") ? "" : monitors[0].displayName;

            if (e.monitors.Count <= 1)
            {
                prevButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(false);
                cameraName.text = camName;
            }
            else
            {
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
                cameraName.text = camName + " (1/" + e.monitors.Count + ")";
            }

            ShowPanel();
            EventManager.TriggerEvent(new PlayMonitorCameraEvent(0, monitors[0]));
        }

        private void Next()
        {
            currentIndex++;

            if (currentIndex >= monitors.Count)
                currentIndex = 0;

            string camName = string.Equals(monitors[currentIndex].displayName, "new monitor camera") ? "" : monitors[currentIndex].displayName;
            cameraName.text = camName + " (" + (currentIndex + 1) + "/" + monitors.Count + ")";

            EventManager.TriggerEvent(new PlayMonitorCameraEvent(currentIndex, monitors[currentIndex]));
        }

        private void Prev()
        {
            currentIndex--;

            if (currentIndex < 0)
                currentIndex = monitors.Count - 1;

            string camName = string.Equals(monitors[currentIndex].displayName, "new monitor camera") ? "" : monitors[currentIndex].displayName;
            cameraName.text = camName + " (" + (currentIndex + 1) + "/" + monitors.Count + ")";

            EventManager.TriggerEvent(new PlayMonitorCameraEvent(currentIndex, monitors[currentIndex]));
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (monitors != null)
                base.ShowAllPanelListener(e);
        }
    }
}

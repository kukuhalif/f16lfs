using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class FullscreenPanel : MonoBehaviour
    {
        [SerializeField] CanvasScaler scaler;
        [SerializeField] RectTransform fullscreenPanel;
        [SerializeField] RectTransform fullscreenPanelCalculateTaskbar;
        [SerializeField] RectTransform minimizeAreaPanel;

        [SerializeField] RectTransform left, right, top, bottomTaskbar, bottom;

        bool dynamicUISize;

        private void Awake()
        {
            VirtualTrainingUiUtility.DefaultUiResolution = scaler.referenceResolution;
            VirtualTrainingUiUtility.SetupFullscreenPanel(fullscreenPanel, fullscreenPanelCalculateTaskbar, minimizeAreaPanel, left, right, top, bottomTaskbar, bottom, SettingUtility.LastSetting.dynamicUiSize);
        }

        private void Start()
        {
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            dynamicUISize = e.data.dynamicUiSize;
            VirtualTrainingUiUtility.DynamicUiSize = dynamicUISize;

            if (dynamicUISize)
            {
                scaler.referenceResolution = e.resolution;
                fullscreenPanelCalculateTaskbar.sizeDelta = new Vector2(fullscreenPanelCalculateTaskbar.sizeDelta.x, e.resolution.y - minimizeAreaPanel.sizeDelta.y);
            }
            else
            {
                scaler.referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
                fullscreenPanelCalculateTaskbar.sizeDelta = new Vector2(fullscreenPanelCalculateTaskbar.sizeDelta.x, VirtualTrainingUiUtility.DefaultUiResolution.y - minimizeAreaPanel.sizeDelta.y);
            }
        }
    }
}

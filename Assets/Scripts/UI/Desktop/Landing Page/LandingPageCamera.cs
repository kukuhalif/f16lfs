using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class LandingPageCamera : MonoBehaviour
    {
        [SerializeField] Camera cam;
        Transform rotateTarget;

        private void Start()
        {
            EventManager.AddListener<LandingPageEnabledEvent>(LandingPageEnabledListener);
            EventManager.AddListener<SceneReadyEvent>(InitDoneListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<LandingPageEnabledEvent>(LandingPageEnabledListener);
            EventManager.RemoveListener<SceneReadyEvent>(InitDoneListener);
        }

        private void InitDoneListener(SceneReadyEvent e)
        {
            InitData();
            EventManager.RemoveListener<SceneReadyEvent>(InitDoneListener);
        }

        private void InitData()
        {
            CameraDestination landingPageCamera = DatabaseManager.GetCameraData().landingPageCamera;

            transform.position = landingPageCamera.position;
            transform.rotation = landingPageCamera.rotation;

            if (cam != null)
            {
                cam.orthographic = landingPageCamera.isOrthographic;
                cam.orthographicSize = landingPageCamera.orthographicSize;
                cam.fieldOfView = landingPageCamera.fov;
            }

            if (landingPageCamera.target.gameObject != null)
                rotateTarget = landingPageCamera.target.gameObject.transform;
        }

        private void LandingPageEnabledListener(LandingPageEnabledEvent e)
        {
            Debug.Log("landing page enabled : " + e.enabled);
            if (cam == null)
            {
                enabled = e.enabled;

                InitData();

                VirtualTrainingCamera.CameraController.enabled = !e.enabled;
            }
            else
            {
                gameObject.SetActive(e.enabled);
                cam.enabled = e.enabled;
            }
        }

        private void Update()
        {
            // Spin the object around the target at 20 degrees/second.
            if (rotateTarget != null)
                transform.RotateAround(rotateTarget.transform.position, Vector3.up, 20 * Time.deltaTime);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class LandingPagePanel : UIPanel
    {
        [SerializeField] InteractionButton systemButton;
        [SerializeField] InteractionButton freePlayButton;
        [SerializeField] InteractionButton quizButton;
        [SerializeField] InteractionButton manualButton;
        [SerializeField] InteractionButton helpButton;
        [SerializeField] InteractionButton aboutButton;
        [SerializeField] InteractionButton quitButton;
        [SerializeField] InteractionButton vrButton;

        [SerializeField] Image bodyImage;
        [SerializeField] Image landingPageIcon;

        [SerializeField] TextMeshProUGUI projectNameText;
        [SerializeField] TextMeshProUGUI projectDetailText;

        [SerializeField] Taskbar taskbar;
        [SerializeField] SystemPanel systemPanel;
        [SerializeField] ManualPanel manualPanel;
        [SerializeField] HelpPanel helpPanel;

        [SerializeField] RenderTexture showcaseRenderTexture;

        protected override void Start()
        {
            base.Start();
            systemButton.OnClickEvent += SystemButtonPressed;
            freePlayButton.OnClickEvent += FreeplayButtonPressed;
            quizButton.OnClickEvent += QuizButtonPressed;
            manualButton.OnClickEvent += ManualButtonPressed;
            helpButton.OnClickEvent += HelpButtonPressed;
            aboutButton.OnClickEvent += AboutButtonPressed;
            quitButton.OnClickEvent += QuitButtonPressed;
            vrButton.OnClickEvent += VRButtonPressed;

            projectNameText.text = Application.productName;
            projectDetailText.text = DatabaseManager.GetProjectDetails().projectDescription;

            if (DatabaseManager.GetProjectDetails().landingPageIcon != null)
            {
                landingPageIcon.sprite = DatabaseManager.GetProjectDetails().landingPageIcon;
                AspectRatioFitter aspectRatioFitter = landingPageIcon.GetComponent<AspectRatioFitter>();
                aspectRatioFitter.aspectRatio = (float)landingPageIcon.sprite.texture.width / (float)landingPageIcon.sprite.texture.height;
            }
            else
                landingPageIcon.gameObject.SetActive(false);

            if (DatabaseManager.GetContentVRData().isVREnable == false)
            {
                vrButton.gameObject.SetActive(false);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            systemButton.OnClickEvent -= SystemButtonPressed;
            freePlayButton.OnClickEvent -= FreeplayButtonPressed;
            quizButton.OnClickEvent -= QuizButtonPressed;
            manualButton.OnClickEvent -= ManualButtonPressed;
            helpButton.OnClickEvent -= HelpButtonPressed;
            aboutButton.OnClickEvent -= AboutButtonPressed;
            quitButton.OnClickEvent -= QuitButtonPressed;
            vrButton.OnClickEvent -= VRButtonPressed;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            bodyImage.color = theme.landingPageBgColor;
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            EventManager.TriggerEvent(new LandingPageEnabledEvent(true));
            EventManager.TriggerEvent(new FullscreenPanelEvent(true, gameObject));
        }

        protected override void OnDisablePanel()
        {
            showcaseRenderTexture.Release();
            base.OnDisablePanel();
            EventManager.TriggerEvent(new LandingPageEnabledEvent(false));
        }

        private void SystemButtonPressed()
        {
            ClosePanel(() =>
            {
                taskbar.Show();
                systemPanel.ShowPanel();
            });
        }

        private void FreeplayButtonPressed()
        {
            ClosePanel(() =>
            {
                taskbar.Show();
                EventManager.TriggerEvent(new FreePlayEvent());
            });
        }

        private void QuizButtonPressed()
        {
            if (VirtualTrainingSceneManager.CheckDatabaseQuiz() == false)
                return;

            ClosePanel(() =>
            {
                VirtualTrainingSceneManager.OpenQuiz();
            }, true, true, false);
        }

        private void ManualButtonPressed()
        {
            ClosePanel(() =>
            {
                taskbar.Show();
                manualPanel.ShowPanel();
            });
        }

        private void HelpButtonPressed()
        {
            ClosePanel(() =>
            {
                taskbar.Show();
                //EventManager.TriggerEvent(new ShowPdfEvent(DatabaseManager.GetProjectDetails().helpPDF));
                helpPanel.ShowPanel();
            });
        }

        private void AboutButtonPressed()
        {
            ClosePanel(() =>
            {
                EventManager.TriggerEvent(new ShowAboutPanelEvent(() =>
                {
                    ShowPanel();
                }));
            });
        }

        private void QuitButtonPressed()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to exit?", QuitCallback, null));
        }

        private void VRButtonPressed()
        {
            VirtualTrainingSceneManager.LoadVRScene();
        }

        private void QuitCallback()
        {
            EventManager.TriggerEvent(new QuitAppEvent());
        }
    }
}


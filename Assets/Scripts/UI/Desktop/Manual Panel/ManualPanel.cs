using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class ManualPanel : UIPanel
    {
        [SerializeField] MainMenuPanel mainMenu;
        [SerializeField] InteractionButton back;
        [SerializeField] Taskbar taskbar;

        protected override void Start()
        {
            base.Start();
            back.OnClickEvent += Back;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            back.OnClickEvent -= Back;
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            VirtualTrainingInputSystem.OnStartLeftClick += StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick += StartClick;
            VirtualTrainingInputSystem.OnStartRightClick += StartClick;
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            VirtualTrainingInputSystem.OnStartLeftClick -= StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick -= StartClick;
            VirtualTrainingInputSystem.OnStartRightClick -= StartClick;
        }

        private void StartClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (!BodyRect.RectContains(pointerPosition) && !taskbar.RectContain(pointerPosition))
                ClosePanel();
        }

        private void Back()
        {
            ClosePanel(null, true);
            mainMenu.ShowPanel();
        }
    }
}

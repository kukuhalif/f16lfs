using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using VirtualTraining.Core;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class LoadingScreenPanel : UIPanel
    {
        [SerializeField] VideoPlayer videoPlayer;
        [SerializeField] Image loadingBar;
        [SerializeField] TextMeshProUGUI label;
        [SerializeField] AspectRatioFitter aspectRatioFitter;
        [SerializeField] RawImage videoImage;
        Func<float> GetProgress;
        Func<string> GetLabel;
        Action onShowPanelCallback;
        Action onEndLoadingCallback;

        bool panelEnabled;
        RenderTexture videoRenderTexture;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<ShowLoadingScreenEvent>(EventListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<ShowLoadingScreenEvent>(EventListener);
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            if (onShowPanelCallback != null)
            {
                onShowPanelCallback.Invoke();
                onShowPanelCallback = null;
            }
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            videoPlayer.Stop();
            videoPlayer.targetTexture.Release();

            if (onEndLoadingCallback != null)
            {
                onEndLoadingCallback.Invoke();
                onEndLoadingCallback = null;
            }

            VirtualTrainingCursor.DefaultCursor();
            loadingBar.fillAmount = 0f;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            // todo : sementara mobile
            //label.color = theme.genericTextColor;
            //progress.color = theme.genericTextColor;
            //loadingBar.color = theme.landingPageBgColor;
        }

        private void Update()
        {
            if (panelEnabled && GetProgress != null)
            {
                float progress = GetProgress();

                if (GetLabel != null)
                    label.text = GetLabel() + " " + (100f * progress).ToString("n2") + " %";

                loadingBar.fillAmount = Mathf.Lerp(loadingBar.fillAmount, progress, Time.deltaTime * 10f);
                if (progress >= 1f)
                {
                    videoPlayer.Stop();
                    ClosePanel();
                    GetProgress = null;
                    panelEnabled = false;
                }
                Canvas.ForceUpdateCanvases();
            }
        }

        private void EventListener(ShowLoadingScreenEvent e)
        {
            VirtualTrainingCursor.Loading();

            var data = SettingUtility.LastSetting;
            videoPlayer.SetDirectAudioVolume(0, data.voiceVolume);

            onShowPanelCallback = e.onShowPanelCallback;
            onEndLoadingCallback = e.endLoadingCallback;

            videoPlayer.Stop();
            videoPlayer.targetTexture.Release();

            loadingBar.fillAmount = 0f;
            label.text = "";
            GetProgress = e.progress;
            GetLabel = e.label;

            loadingBar.gameObject.SetActive(!e.hideUI);
            label.gameObject.SetActive(!e.hideUI);

            var videos = e.clips;
            int selected = UnityEngine.Random.Range(0, videos.Count);
            videoPlayer.clip = videos[selected];

            if (videoRenderTexture == null || (int)videoPlayer.clip.width != videoRenderTexture.width || (int)videoPlayer.clip.height != videoRenderTexture.height)
            {
                videoRenderTexture = new RenderTexture((int)videoPlayer.clip.width, (int)videoPlayer.clip.height, 24);

                videoPlayer.targetTexture = videoRenderTexture;
                videoImage.texture = videoRenderTexture;

                Debug.Log("create loading screen render texture : " + videoRenderTexture.width + " x " + videoRenderTexture.height);
            }

            aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            aspectRatioFitter.aspectRatio = (float)videoPlayer.clip.width / (float)videoPlayer.clip.height;

            ShowPanel(() => panelEnabled = true, false, false, true);
            videoPlayer.Play();
        }
    }
}

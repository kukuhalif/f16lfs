using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIObjectList : TreeViewUI
    {
        //[System.Serializable]
        //public class DataDebug
        //{
        //    public string name;
        //    public List<GameObject> objs;

        //    public DataDebug(string name, List<PartObject> parts)
        //    {
        //        this.name = name;
        //        for (int i = 0; i < parts.Count; i++)
        //        {
        //            objs = new List<GameObject>();
        //            var go = GameObjectReference.GetReference(parts[i].target.Id);
        //            objs.Add(go.gameObject);
        //        }
        //    }
        //}

        //public List<DataDebug> dataDebug = new List<DataDebug>();

        [SerializeField] TreeViewUIElement treeElementTemplate;

        Dictionary<TreeElement, List<PartObject>> objectIdsLookup = new Dictionary<TreeElement, List<PartObject>>();
        List<PartObject> partObjects = new List<PartObject>();

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<ObjectInteractionShowObjectListEvent>(Initialization);

            ObjectListTreeElement root = DatabaseManager.GetObjectListTree();

            GenerateElements(root);
        }

        private void Initialization(ObjectInteractionShowObjectListEvent e)
        {
            if (!e.on)
                return;

            ObjectListTreeElement root = DatabaseManager.GetObjectListTree();

            // cache all renderer as target object
            if (root.hasChildren)
                for (int i = 0; i < root.children.Count; i++)
                {
                    CacheTargetObject(root.children[i]);
                }

            // cleanup object id that is in child element
            for (int i = 0; i < UiElements.Count; i++)
            {
                TreeViewUIObjectListElement objUIElement = UiElements[i] as TreeViewUIObjectListElement;
                List<PartObject> partObjects = objectIdsLookup[objUIElement.Data];

                if (objUIElement.Data.hasChildren)
                    for (int c = 0; c < objUIElement.Data.children.Count; c++)
                    {
                        CleanupParentFromChildObjects(objUIElement.Data.children[c], partObjects);
                    }

                //dataDebug.Add(new DataDebug(UiElements[i].GetText(), partObjects));
            }

            EventManager.RemoveListener<ObjectInteractionShowObjectListEvent>(Initialization);
        }

        private void CleanupParentFromChildObjects(TreeElement childElement, List<PartObject> parentParts)
        {
            for (int i = 0; i < objectIdsLookup[childElement].Count; i++)
            {
                parentParts.RemoveAll(x => x.target.Id == objectIdsLookup[childElement][i].target.Id);
            }

            if (childElement.hasChildren)
                for (int i = 0; i < childElement.children.Count; i++)
                {
                    CleanupParentFromChildObjects(childElement.children[i], parentParts);
                }
        }

        private void CacheTargetObject(TreeElement element)
        {
            List<PartObject> ids = new List<PartObject>();

            ObjectListTreeElement objElement = element as ObjectListTreeElement;
            for (int i = 0; i < objElement.ObjectListData.objects.Count; i++)
            {
                // cache all renderer
                GameObjectType gameObjectType = objElement.ObjectListData.objects[i];
                Renderer[] renderers = gameObjectType.gameObject.transform.GetAllComponentsInChilds<Renderer>();
                for (int r = 0; r < renderers.Length; r++)
                {
                    if (renderers[r] != null)
                    {
                        GameObjectReference reference = renderers[r].GetComponent<GameObjectReference>();
                        PartObject newPart = new PartObject();
                        newPart.target.Id = reference.Id;
                        newPart.isRecursive = false;
                        ids.Add(newPart);
                    }
                }
            }

            objectIdsLookup.Add(element, ids);

            if (element.hasChildren)
                for (int i = 0; i < element.children.Count; i++)
                {
                    CacheTargetObject(element.children[i]);
                }
        }

        protected override void ClickedElementCallback(TreeElement element)
        {

        }

        protected override TreeViewUIElement GetElementTemplate()
        {
            return treeElementTemplate;
        }

        private TreeViewUIObjectListElement GetObjectListElement(TreeElement element)
        {
            return UiElementLookup[element.id] as TreeViewUIObjectListElement;
        }

        protected override void SetupElementCallback(TreeViewUIElement uiElement, TreeElement element)
        {
            base.SetupElementCallback(uiElement, element);
            TreeViewUIObjectListElement objectListElement = uiElement as TreeViewUIObjectListElement;
            objectListElement.SetGetElementCallback(GetObjectListElement);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            EventManager.AddListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            EventManager.RemoveListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);
        }

        public void ResetAllToggles()
        {
            StartCoroutine(ResetAllTogglesAction());
        }

        private IEnumerator ResetAllTogglesAction()
        {
            EventManager.RemoveListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);

            yield return null;

            for (int i = 0; i < UiElements.Count; i++)
            {
                TreeViewUIObjectListElement element = UiElements[i] as TreeViewUIObjectListElement;
                element.StartResetState();
                yield return null;
                element.State = -1;
                yield return null;
                element.EndResetState();
            }

            yield return null;

            EventManager.AddListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);
            EventManager.TriggerEvent(new ResetPartObjectEvent());
        }

        // to enabled toggle and setup some listener
        public IEnumerator RefreshAtStartUiElement()
        {
            List<GameObject> toDisabled = new List<GameObject>();

            yield return null;
            for (int i = 0; i < UiElements.Count; i++)
            {
                if (!UiElements[i].gameObject.activeSelf)
                    toDisabled.Add(UiElements[i].gameObject);

                UiElements[i].gameObject.SetActive(true);
            }

            yield return null;
            for (int i = 0; i < toDisabled.Count; i++)
            {
                toDisabled[i].SetActive(false);
            }
        }

        private void ToggleChangedListener(ObjectListToggleStateChangedEvent e)
        {
            StartCoroutine(SetElement(e.element, e.sourceState, e.newState));
        }

        private void SetChildToggleState(TreeElement element, int state, List<int> childIds)
        {
            TreeViewUIObjectListElement uiElement = UiElementLookup[element.id] as TreeViewUIObjectListElement;
            uiElement.State = state;

            childIds.Add(element.id);

            if (element.hasChildren)
                for (int i = 0; i < element.children.Count; i++)
                {
                    SetChildToggleState(element.children[i], state, childIds);
                }
        }

        private IEnumerator SetElement(TreeElement element, int sourceState, int newState)
        {
            // temporary remove toggle changed listener
            EventManager.RemoveListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);

            // reset part object
            EventManager.TriggerEvent(new ResetPartObjectEvent());

            yield return null;

            // calculate opposite state
            int oppositeState = sourceState + 2;
            if (oppositeState > 3)
                oppositeState -= 4;

            int destinationState = newState;

            // if switched off
            if (newState == -1)
            {
                // set this element to opposite state
                TreeViewUIObjectListElement uiElement = UiElementLookup[element.id] as TreeViewUIObjectListElement;
                uiElement.State = oppositeState;
                destinationState = uiElement.State;
            }

            // set element and child toggle state and get all child ids
            List<int> childIds = new List<int>();
            SetChildToggleState(element, destinationState, childIds);

            yield return null;

            // if next state is on
            if (newState > -1)
            {
                // set other element to opposite state
                for (int i = 0; i < UiElements.Count; i++)
                {
                    TreeViewUIObjectListElement partObjElement = UiElements[i] as TreeViewUIObjectListElement;

                    if (partObjElement.State != destinationState)
                    {
                        if (!childIds.Contains(partObjElement.Data.id))
                        {
                            partObjElement.State = oppositeState;
                        }
                    }
                }
            }

            yield return null;

            partObjects.Clear();

            // set part object, only trigger negative action (xray & hide)
            for (int i = 0; i < UiElements.Count; i++)
            {
                TreeViewUIObjectListElement partObjElement = UiElements[i] as TreeViewUIObjectListElement;

                if (partObjElement.State == 2 || partObjElement.State == 3)
                {
                    List<PartObject> cachedPartObjects = objectIdsLookup[partObjElement.Data];
                    for (int p = 0; p < cachedPartObjects.Count; p++)
                    {
                        if (partObjElement.State == 2)
                            cachedPartObjects[p].action = PartObjectAction.Hide;
                        else if (partObjElement.State == 3)
                            cachedPartObjects[p].action = PartObjectAction.Xray;

                        partObjects.Add(cachedPartObjects[p]);
                    }
                }
            }

            yield return null;

            // trigger part object
            EventManager.TriggerEvent(new PartObjectEvent(false, partObjects, () =>
            {
                // restore toggle changed listener
                EventManager.AddListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);

            }));
        }
    }
}
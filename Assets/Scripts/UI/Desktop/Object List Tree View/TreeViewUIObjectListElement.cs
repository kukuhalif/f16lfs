using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIObjectListElement : TreeViewUIElement
    {
        [SerializeField] UILineRenderer upperLine;
        [SerializeField] UILineRenderer childLine;
        [SerializeField] UILineRenderer siblingLine;

        [SerializeField] InteractionToggle[] toggles; // solo, highlight, hide, xRay

        int state = -1;
        bool isResetting;

        Func<TreeElement, TreeViewUIObjectListElement> getElementCallback;

        // todo : refactor line hierarchy to be more generic
        const float DEFAULT_UPPER_LINE_HEIGHT = 14f;
        bool refreshingUI;

        private void Start()
        {
            Button.SetInternalActiveState(false);

            toggles[0].OnStateChangedEvent += Solo;
            toggles[1].OnStateChangedEvent += Highlight;
            toggles[2].OnStateChangedEvent += Hide;
            toggles[3].OnStateChangedEvent += XRay;

            // enable / disable upper line
            if (Data.depth <= 0)
                upperLine.gameObject.SetActive(false);

            // enable / disable child line
            childLine.gameObject.SetActive(false);

            // enable / disable sibling line
            if (Data.depth > 0)
            {
                int childCount = Data.parent.children.Count;
                int childIndex = 0;
                for (int i = 0; i < Data.parent.children.Count; i++)
                {
                    if (Data.parent.children[i] == Data)
                    {
                        childIndex = i;
                        break;
                    }
                }
                if (childIndex < childCount - 1)
                {
                    siblingLine.gameObject.SetActive(true);
                }
            }

            EventManager.AddListener<ObjectListPanelUpdatedEvent>(PanelUpdatedListener);
            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        private void OnEnable()
        {
            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            toggles[0].OnStateChangedEvent -= Solo;
            toggles[1].OnStateChangedEvent -= Highlight;
            toggles[2].OnStateChangedEvent -= Hide;
            toggles[3].OnStateChangedEvent -= XRay;

            EventManager.RemoveListener<ObjectListPanelUpdatedEvent>(PanelUpdatedListener);
        }

        public int State
        {
            get => state;
            set
            {
                if (value > -1)
                    toggles[value].IsOn = true;
                else
                {
                    for (int i = 0; i < toggles.Length; i++)
                    {
                        toggles[i].IsOn = false;
                    }
                }

                state = value;
            }
        }

        public void StartResetState()
        {
            isResetting = true;
        }

        public void EndResetState()
        {
            isResetting = false;
        }

        private void Solo(bool on)
        {
            if (!isResetting)
                TriggerEvent(0, on);
        }

        private void Highlight(bool on)
        {
            if (!isResetting)
                TriggerEvent(1, on);
        }

        private void Hide(bool on)
        {
            if (!isResetting)
                TriggerEvent(2, on);
        }

        private void XRay(bool on)
        {
            if (!isResetting)
                TriggerEvent(3, on);
        }

        private void TriggerEvent(int nextState, bool on)
        {
            if (on && state != nextState)
            {
                state = nextState;
                EventManager.TriggerEvent(new ObjectListToggleStateChangedEvent(state, state, Data));
            }
            else if (!on && state == nextState)
            {
                state = -1;
                EventManager.TriggerEvent(new ObjectListToggleStateChangedEvent(nextState, state, Data));
            }
        }

        protected override void ExpandCallback(bool isExpanded)
        {
            if (Data.hasChildren)
                childLine.gameObject.SetActive(isExpanded);

            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        protected override void OnShowElement()
        {
            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        private void PanelUpdatedListener(ObjectListPanelUpdatedEvent e)
        {
            RefreshUI();
        }

        public void SetGetElementCallback(Func<TreeElement, TreeViewUIObjectListElement> getElementCallback)
        {
            this.getElementCallback = getElementCallback;
        }

        private void RefreshUI()
        {
            if (RectTransform == null || !gameObject.activeInHierarchy)
                return;

            StartCoroutine(RefreshUIAction());
        }

        private void SetDirty()
        {
            childLine.SetVerticesDirty();
            childLine.SetLayoutDirty();
            siblingLine.SetVerticesDirty();
            siblingLine.SetLayoutDirty();
        }

        IEnumerator RefreshUIAction()
        {
            refreshingUI = true;
            StartCoroutine(CalculateLittleSiblingUpperLineHeight());

            // set child line height
            yield return null;
            float height = RectTransform.rect.height;
            childLine.rectTransform.sizeDelta = new Vector2(childLine.rectTransform.sizeDelta.x, height);
            childLine.Points[1] = new Vector2(childLine.Points[1].x, -height);

            // set sibling line height
            siblingLine.rectTransform.sizeDelta = new Vector2(siblingLine.rectTransform.sizeDelta.x, height);
            siblingLine.Points[1] = new Vector2(siblingLine.Points[1].x, -height);

            SetDirty();
            yield return new WaitForSeconds(EXPAND_DURATION);
            yield return null;
            SetDirty();

            refreshingUI = false;
        }

        private IEnumerator CalculateLittleSiblingUpperLineHeight()
        {
            while (refreshingUI)
            {
                yield return null;
                if (Data.hasChildren)
                {
                    // set upper line height
                    // if has little sibling
                    int siblingCount = Data.parent.children.Count;
                    int siblingIndex = -1;
                    for (int i = 0; i < siblingCount; i++)
                    {
                        if (Data.parent.children[i] == Data)
                        {
                            siblingIndex = i;
                            break;
                        }
                    }
                    if (siblingCount > 1 && siblingIndex < siblingCount - 1)
                    {
                        float totalHeight = DEFAULT_UPPER_LINE_HEIGHT;

                        if (IsExpanded)
                        {
                            List<TreeElement> childs = Data.AllChilds();
                            // calculate line height

                            for (int i = 0; i < childs.Count; i++)
                            {
                                TreeViewUIObjectListElement descendants = getElementCallback(childs[i]);
                                if (descendants.transform.localScale.y >= 0.999f)
                                    totalHeight += descendants.RectTransform.rect.height;
                            }
                        }

                        TreeViewUIObjectListElement littleSibling = getElementCallback(Data.parent.children[siblingIndex + 1]);
                        littleSibling.upperLine.Points[0] = new Vector2(upperLine.Points[0].x, totalHeight);
                        littleSibling.upperLine.SetVerticesDirty();
                        littleSibling.upperLine.SetLayoutDirty();
                    }
                }
            }
        }
    }
}

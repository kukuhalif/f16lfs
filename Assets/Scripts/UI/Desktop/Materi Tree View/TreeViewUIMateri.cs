using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIMateri : TreeViewUI
    {
        protected override void Start()
        {
            base.Start();

            var root = DatabaseManager.GetMateriTree();
            MateriTreeElement rootTree = new MateriTreeElement("", -1, 0, MateriElementType.Materi, true);
            TreeElement newRoot = TreeElementUtility.Filter(root, rootTree, Include, SetData, CreateNewData);

            GenerateElements(newRoot);

            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            EventManager.AddListener<TriggerMateriInsideTreeViewUIEvent>(TriggerMateriListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            EventManager.RemoveListener<TriggerMateriInsideTreeViewUIEvent>(TriggerMateriListener);
        }

        protected override TreeViewUIElement GetElementTemplate()
        {
            GameObject template = Resources.Load("UI/Tree View Element Template") as GameObject;
            return template.GetComponent<TreeViewUIElement>();
        }

        protected override void ClickedElementCallback(TreeElement element)
        {
            MateriTreeElement materiElement = element as MateriTreeElement;
            EventManager.TriggerEvent(new MateriEvent(materiElement));
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClearSelected();
        }

        private void TriggerMateriListener(TriggerMateriInsideTreeViewUIEvent e)
        {
            FocusAndClickElement(e.materiTreeElement);
        }

        bool Include(TreeElement element)
        {
            MateriTreeElement m = element as MateriTreeElement;
            if ((m.MateriData.deviceMode == DeviceMode.AllDevice || m.MateriData.deviceMode == DeviceMode.DesktopOnly)
                && m.MateriData.figures.Count > 0)
                return true;

            return false;
        }

        void SetData(TreeElement source, TreeElement destination)
        {
            MateriTreeElement s = source as MateriTreeElement;
            MateriTreeElement d = destination as MateriTreeElement;

            d.MateriData = s.MateriData;
        }

        TreeElement CreateNewData(TreeElement source)
        {
            MateriTreeElement sourceTree = source as MateriTreeElement;
            MateriTreeElement newMateri = new MateriTreeElement(sourceTree.name, sourceTree.depth, sourceTree.id, sourceTree.MateriData.elementType, sourceTree.isFolder);

            newMateri.MateriData = sourceTree.MateriData;

            return newMateri;
        }
    }
}

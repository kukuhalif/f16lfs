﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class FigurePanel : UIElement
    {
        [SerializeField] TMP_Text figureName;
        [SerializeField] InteractionButton prevFigureButton;
        [SerializeField] InteractionButton nextFigureButton;
        [SerializeField] Image background;

        MateriElementType currentType;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<UpdateFigurePanelEvent>(PlayFigureListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            prevFigureButton.OnClickEvent += PreviousFigure;
            nextFigureButton.OnClickEvent += NextFigure;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<UpdateFigurePanelEvent>(PlayFigureListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            prevFigureButton.OnClickEvent -= PreviousFigure;
            nextFigureButton.OnClickEvent -= NextFigure;
        }

        private void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        private void ShowPanel()
        {
            gameObject.SetActive(true);
        }

        private void PlayFigureListener(UpdateFigurePanelEvent e)
        {
            Debug.Log("figure panel from " + e.type);

            currentType = e.type;

            ShowPanel();

            if (e.figureCount == 0)
                ClosePanel();
            else if (e.figureCount == 1 && (string.Equals(e.name, "new figure") || string.IsNullOrEmpty(e.name))) // "new figure is default figure name, check figure constructor"
                ClosePanel();
            else
            {
                if (e.figureCount <= 1)
                {
                    prevFigureButton.gameObject.SetActive(false);
                    nextFigureButton.gameObject.SetActive(false);
                    figureName.text = string.Equals(e.name, "new figure") ? "" : e.name;
                }
                else
                {
                    prevFigureButton.gameObject.SetActive(true);
                    nextFigureButton.gameObject.SetActive(true);
                    figureName.text = (string.Equals(e.name, "new figure") ? "" : e.name) + " (" + (e.currentFigure + 1).ToString() + " / " + e.figureCount.ToString() + ")";
                }

                ShowPanel();
            }
        }

        protected override void ApplyTheme()
        {
            background.color = theme.panelTitleColor;
            figureName.color = theme.panelTitleTextColor;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClosePanel();
        }

        private void NextFigure()
        {
            EventManager.TriggerEvent(new NextFigureEvent(currentType));
        }

        private void PreviousFigure()
        {
            EventManager.TriggerEvent(new PrevFigureEvent(currentType));
        }
    }
}

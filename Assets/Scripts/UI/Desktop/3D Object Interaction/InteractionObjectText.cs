using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

namespace VirtualTraining.UI.Desktop
{
    public class InteractionObjectText : MonoBehaviour, IPointerEnterHandler
    {
        [SerializeField] TextMeshProUGUI featureText;

        public void OnPointerEnter(PointerEventData eventData)
        {
            featureText.text = gameObject.name;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class CutawayPanel : UIPanel
    {
        [SerializeField] InteractionButton resetButton;
        [SerializeField] InteractionToggle translateToggle;
        [SerializeField] InteractionToggle planeToggle;
        [SerializeField] InteractionToggle boxToggle;
        [SerializeField] InteractionToggle cornerToggle;
        [SerializeField] InteractionToggleGroup cutawayModeToggleGroup;

        bool cutawayOn;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<CutawayEvent>(CutawayListener);

            resetButton.OnClickEvent += ResetCallback;
            translateToggle.OnStateChangedEvent += TranslateToggleCallback;
            planeToggle.OnStateChangedEvent += PlaneToggleCallback;
            boxToggle.OnStateChangedEvent += BoxToggleCallback;
            cornerToggle.OnStateChangedEvent += CornerToggleCallback;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<CutawayEvent>(CutawayListener);

            resetButton.OnClickEvent -= ResetCallback;
            translateToggle.OnStateChangedEvent -= TranslateToggleCallback;
            planeToggle.OnStateChangedEvent -= PlaneToggleCallback;
            boxToggle.OnStateChangedEvent -= BoxToggleCallback;
            cornerToggle.OnStateChangedEvent -= CornerToggleCallback;
        }

        protected override void CloseAllPanelListener(CloseAllUIPanelEvent e)
        {

        }

        protected override void MinimizeAllPanelListener(MinimizeAllUIPanelEvent e)
        {

        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {

        }

        private void ResetCallback()
        {
            EventManager.TriggerEvent(new ResetCutawayEvent());
        }

        private void TranslateToggleCallback(bool value)
        {
            EventManager.TriggerEvent(new SwitchCutawayGizmoModeEvent(value));
        }

        private void CornerToggleCallback(bool on)
        {
            if (on)
            {
                Debug.Log("cutaway corner");
                cutawayOn = true;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Corner));
            }
            else
                AllOff();
        }

        private void BoxToggleCallback(bool on)
        {
            if (on)
            {
                Debug.Log("cutaway box");
                cutawayOn = true;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Box));
            }
            else
                AllOff();
        }

        private void PlaneToggleCallback(bool on)
        {
            if (on)
            {
                Debug.Log("cutaway plane");
                cutawayOn = true;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.Plane));
            }
            else
                AllOff();
        }

        private void AllOff()
        {
            if (cutawayOn && cutawayModeToggleGroup.GetSelectedToggle() == null)
            {
                Debug.Log("cutaway off");
                cutawayOn = false;
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(CutawayState.None));
            }
        }

        private void CutawayListener(CutawayEvent e)
        {
            planeToggle.OnStateChangedEvent -= PlaneToggleCallback;
            boxToggle.OnStateChangedEvent -= BoxToggleCallback;
            cornerToggle.OnStateChangedEvent -= CornerToggleCallback;

            if (e.cutaway != null)
            {
                switch (e.cutaway.state)
                {
                    case CutawayState.None:

                        planeToggle.IsOn = false;
                        boxToggle.IsOn = false;
                        cornerToggle.IsOn = false;

                        break;
                    case CutawayState.Plane:

                        planeToggle.IsOn = true;

                        break;
                    case CutawayState.Box:

                        boxToggle.IsOn = true;

                        break;
                    case CutawayState.Corner:

                        cornerToggle.IsOn = true;

                        break;
                }
            }
            else
            {
                planeToggle.IsOn = false;
                boxToggle.IsOn = false;
                cornerToggle.IsOn = false;
            }

            planeToggle.OnStateChangedEvent += PlaneToggleCallback;
            boxToggle.OnStateChangedEvent += BoxToggleCallback;
            cornerToggle.OnStateChangedEvent += CornerToggleCallback;
        }
    }
}
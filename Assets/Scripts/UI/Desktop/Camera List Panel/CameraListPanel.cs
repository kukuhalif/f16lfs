using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;
using System.Linq;

namespace VirtualTraining.UI.Desktop
{
    public class CameraListPanel : UIElement
    {
        [SerializeField] TMP_Text cameraName;
        [SerializeField] InteractionButton prevCameraButton;
        [SerializeField] InteractionButton nextCameraButton;
        [SerializeField] Image background;

        int cameraIndex;
        List<FigureCamera> cameras = new List<FigureCamera>();

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<UpdateFigureCameraPanelEvent>(FigureCameraListListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            EventManager.AddListener<UpdateCameraPanelEvent>(UpdateNameListener);
            prevCameraButton.OnClickEvent += PreviousCamera;
            nextCameraButton.OnClickEvent += NextCamera;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<UpdateFigureCameraPanelEvent>(FigureCameraListListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            EventManager.RemoveListener<UpdateCameraPanelEvent>(UpdateNameListener);
            prevCameraButton.OnClickEvent -= PreviousCamera;
            nextCameraButton.OnClickEvent -= NextCamera;
        }

        private void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        private void ShowPanel()
        {
            gameObject.SetActive(true);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClosePanel();
        }

        private void UpdateNameListener(UpdateCameraPanelEvent e)
        {
            if (cameras.Count < 0)
                return;

            cameraIndex = e.index;

            if (cameraIndex < cameras.Count)
            {
                VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
                cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";

            }
        }

        private void NextCamera()
        {
            if (cameras.Count <= 0)
                return;

            cameraIndex++;

            if (cameraIndex >= cameras.Count)
                cameraIndex = 0;

            if (cameraIndex < cameras.Count)
            {
                VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
                cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
                EventManager.TriggerEvent(new UpdateCameraPanelEvent(cameraIndex));
            }
        }

        private void PreviousCamera()
        {
            if (cameras.Count <= 1)
                return;

            cameraIndex--;

            if (cameraIndex < 0)
                cameraIndex = cameras.Count - 1;

            if (cameraIndex < cameras.Count)
            {
                VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
                cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
                EventManager.TriggerEvent(new UpdateCameraPanelEvent(cameraIndex));
            }
        }

        private void FigureCameraListListener(UpdateFigureCameraPanelEvent e)
        {
            if (e.figureCameras == null)
            {
                ClosePanel();
                return;
            }

            Debug.Log("camera list panel from " + e.type);

            ShowPanel();

            // filter cameras
            cameras = (from camera in e.figureCameras
                       where camera.mode == DeviceMode.AllDevice || camera.mode == DeviceMode.DesktopOnly
                       select camera).ToList();

            // set current camera
            cameraIndex = e.cameraIndex;

            if (cameras.Count == 0)
                ClosePanel();
            else if (cameras.Count == 1 && (string.Equals(cameras[cameraIndex].displayName, "new figure camera") || string.IsNullOrEmpty(cameras[cameraIndex].displayName))) //  "new figure camera is default camera figure name, check figure camera constructor"
                ClosePanel();
            else
            {
                if (cameras.Count <= 1)
                {
                    prevCameraButton.gameObject.SetActive(false);
                    nextCameraButton.gameObject.SetActive(false);
                    cameraName.text = string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName;
                }
                else
                {
                    prevCameraButton.gameObject.SetActive(true);
                    nextCameraButton.gameObject.SetActive(true);
                    cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
                }

                ShowPanel();
            }
        }

        protected override void ApplyTheme()
        {
            background.color = theme.panelTitleColor;
            cameraName.color = theme.panelTitleTextColor;
        }
    }
}

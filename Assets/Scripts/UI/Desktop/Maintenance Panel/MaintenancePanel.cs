using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using System.Linq;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class MaintenancePanel : UIPanel
    {
        [SerializeField] TreeViewUIMaintenance treeViewMaintenance;

        [SerializeField] RectTransform itemContentRect;

        [SerializeField] RectTransform figureContainer;
        [SerializeField] TMP_Text figureName;
        [SerializeField] InteractionButton previousFigureButton;
        [SerializeField] InteractionButton nextFigureButton;

        [SerializeField] RectTransform cameraContainer;
        [SerializeField] TMP_Text cameraName;
        [SerializeField] InteractionButton previousCameraButton;
        [SerializeField] InteractionButton nextCameraButton;

        bool isMaintenanceDataExist = false;
        int cameraIndex;
        List<FigureCamera> cameras;
        float itemContentBottom;
        float figureContainerHeight;
        float cameraContainerHeight;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
            EventManager.AddListener<UpdateFigureCameraPanelEvent>(FigureCameraMaintenanceListListener);
            EventManager.AddListener<UpdateFigurePanelEvent>(PlayFigureListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            nextCameraButton.OnClickEvent += NextCamera;
            previousCameraButton.OnClickEvent += PreviousCamera;
            previousFigureButton.OnClickEvent += PreviousFigure;
            nextFigureButton.OnClickEvent += NextFigure;

            cameraContainerHeight = cameraContainer.rect.height;
            figureContainerHeight = figureContainer.rect.height;

            itemContentBottom = itemContentRect.GetBottom();
            itemContentBottom -= figureContainerHeight;
            itemContentBottom -= cameraContainerHeight;

            DisableFigureAndCameraContainer();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
            EventManager.RemoveListener<UpdateFigureCameraPanelEvent>(FigureCameraMaintenanceListListener);
            EventManager.RemoveListener<UpdateFigurePanelEvent>(PlayFigureListener);
            nextCameraButton.OnClickEvent -= NextCamera;
            previousFigureButton.OnClickEvent -= PreviousCamera;
            nextFigureButton.OnClickEvent -= NextFigure;
            previousFigureButton.OnClickEvent -= PreviousFigure;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            Image fig = figureContainer.GetComponent<Image>();
            fig.color = theme.panelTitleColor;
            Image cam = cameraContainer.GetComponent<Image>();
            cam.color = theme.panelTitleColor;
            figureName.color = theme.panelTitleTextColor;
            cameraName.color = theme.panelTitleTextColor;
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            EventManager.TriggerEvent(new MaintenancePanelUpdatedEvent());
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            DisableFigureAndCameraContainer();
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            isMaintenanceDataExist = false;
            ClosePanel();
            treeViewMaintenance.ClearElements();
        }

        protected override void PanelSizeUpdatedCallback()
        {
            EventManager.TriggerEvent(new MaintenancePanelUpdatedEvent());
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (isMaintenanceDataExist)
                base.ShowAllPanelListener(e);
        }

        private void FigureCameraMaintenanceListListener(UpdateFigureCameraPanelEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            // clear text
            cameraName.text = "";

            // enable / disable camera button container
            if (e.figureCameras.Count > 1)
            {
                cameraContainer.gameObject.SetActive(true);
                figureContainer.anchoredPosition = new Vector2(0, cameraContainerHeight);
            }
            else
            {
                cameraContainer.gameObject.SetActive(false);
                figureContainer.anchoredPosition = new Vector2(0, 0);
            }

            // filter cameras
            cameras = (from camera in e.figureCameras
                       where camera.mode == DeviceMode.AllDevice || camera.mode == DeviceMode.DesktopOnly
                       select camera).ToList();

            // set camera index
            cameraIndex = e.cameraIndex;

            if (cameras.Count > 1 && !string.IsNullOrEmpty(cameras[cameraIndex].displayName))
            {
                cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
            }

            // set bottom height
            SetContentHeight();
        }

        void NextCamera()
        {
            if (cameras.Count <= 0)
                return;

            cameraIndex++;

            if (cameraIndex >= cameras.Count)
                cameraIndex = 0;

            VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
            cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
        }

        void PreviousCamera()
        {
            if (cameras.Count <= 1)
                return;

            cameraIndex--;

            if (cameraIndex < 0)
                cameraIndex = cameras.Count - 1;

            VirtualTrainingCamera.MoveCamera(cameras[cameraIndex].destination, null);
            cameraName.text = (string.Equals(cameras[cameraIndex].displayName, "new figure camera") ? "" : cameras[cameraIndex].displayName) + " (" + (cameraIndex + 1).ToString() + " / " + cameras.Count.ToString() + ")";
        }

        private void MaintenanceUIEventListener(MaintenanceUIEvent e)
        {
            if (e.materi == null)
            {
                isMaintenanceDataExist = false;
                ClosePanel();
                treeViewMaintenance.ClearElements();
                DisableFigureAndCameraContainer();
                return;
            }

            if (e.materi.maintenanceTree == null)
            {
                isMaintenanceDataExist = false;
                ClosePanel();
                treeViewMaintenance.ClearElements();
                DisableFigureAndCameraContainer();
                Debug.LogError("maintenance database missing");
                return;
            }

            isMaintenanceDataExist = true;

            // get maintenance tree root from maintenance tree element list
            MaintenanceTreeElement root = TreeElementUtility.ListToTree(e.materi.maintenanceTree.GetData().treeElements);

            // generate tree view
            treeViewMaintenance.GenerateTree(root);

            // show maintenance panel
            ShowPanel();
        }

        private void PlayFigureListener(UpdateFigurePanelEvent e)
        {
            if (e.type != MateriElementType.Maintenance)
                return;

            // clear text
            figureName.text = "";

            if (e.figureCount > 1 && !string.IsNullOrEmpty(e.name))
            {
                figureName.text = (string.Equals(e.name, "new figure") ? "" : e.name) + " (" + (e.currentFigure + 1).ToString() + " / " + e.figureCount.ToString() + ")";
            }

            // enable / disable figure button container
            if (e.figureCount > 1)
                figureContainer.gameObject.SetActive(true);
            else
                figureContainer.gameObject.SetActive(false);

            SetContentHeight();
        }

        private void NextFigure()
        {
            EventManager.TriggerEvent(new NextFigureEvent(MateriElementType.Maintenance));
        }

        private void PreviousFigure()
        {
            EventManager.TriggerEvent(new PrevFigureEvent(MateriElementType.Maintenance));
        }

        private void DisableFigureAndCameraContainer()
        {
            figureContainer.gameObject.SetActive(false);
            cameraContainer.gameObject.SetActive(false);
            figureName.text = "";
            cameraName.text = "";
            SetContentHeight();
        }

        private void SetContentHeight()
        {
            float bottomValue = itemContentBottom;

            if (figureContainer.gameObject.activeSelf)
                bottomValue += figureContainerHeight;

            if (cameraContainer.gameObject.activeSelf)
                bottomValue += cameraContainerHeight;

            itemContentRect.SetBottom(bottomValue);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class UiRaycastBlocker : MonoBehaviour
    {
        [SerializeField] Canvas canvas;

        private void Start()
        {
            VirtualTrainingCursor.LoadingCursorEvent += LoadingCallback;
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            VirtualTrainingCursor.LoadingCursorEvent -= LoadingCallback;
        }

        private void OnEnable()
        {
            canvas.sortingOrder = UIPanel.SelectedOrder + 100;
        }

        private void LoadingCallback(bool loading)
        {
            gameObject.SetActive(loading);
        }
    }
}
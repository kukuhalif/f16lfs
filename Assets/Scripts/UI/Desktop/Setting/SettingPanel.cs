﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class SettingPanel : UIPanel
    {
        [SerializeField] InteractionTab tab;
        [SerializeField] GameObject[] tabContents;

        [SerializeField] Image panelBg1;
        [SerializeField] Image panelBg2;

        [SerializeField] InteractionButton backButton;
        [SerializeField] InteractionButton resetButton;

        [SerializeField] TextMeshProUGUI[] allGenericText;

        [SerializeField] Slider rotateSpeedSlider;
        [SerializeField] Slider zoomSpeedSlider;
        [SerializeField] Slider dragSpeedSlider;
        [SerializeField] Slider cameraTransitionSpeedSlider;
        [SerializeField] Slider voiceVolumeSlider;
        [SerializeField] Slider sfxVolumeSlider;
        [SerializeField] Slider vrCanvasDistanceSlider;
        [SerializeField] Slider vrMenuHeightSlider;

        [SerializeField] TMP_Dropdown resolutionDropdown;
        [SerializeField] TMP_Dropdown qualityDropdown;
        [SerializeField] TMP_Dropdown dynamicUiSizeDropdown;

        [SerializeField] TMP_Dropdown fullscreenDropdown;
        [SerializeField] TMP_Dropdown showEnvirontmentObjectDropdown;
        [SerializeField] TMP_Dropdown uiThemeDropdown;

        List<InteractionSlider> sliders = new List<InteractionSlider>();

        const int MIN_UI_HEIGHT = 900;

        SettingData defaultSetting;
        SettingData currentSetting;
        List<Resolution> resolutions = new List<Resolution>();

        protected override void Start()
        {
            base.Start();

            tab.OnChangeTabEvent += ChangeTabContentListener;

            resetButton.OnClickEvent += ResetButtonCallback;
            backButton.OnClickEvent += BackButtonCallback;

            defaultSetting = DatabaseManager.GetDefaultSetting();
            currentSetting = new SettingData();

            AddSliders();

            ResolutionOptionInit();
            QualityOptionInit();

            UIThemeDropdownInit();

            SetMinMaxSetting();

            EventManager.AddListener<InitializationUIDoneEvent>(UIInidDone);
            EventManager.AddListener<SceneReadyEvent>(FeatureInitDone);

            ChangeTabContentListener(0);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            tab.OnChangeTabEvent -= ChangeTabContentListener;
            resetButton.OnClickEvent -= ResetButtonCallback;
            backButton.OnClickEvent -= BackButtonCallback;
            RemoveUIListener();
        }

        private void FeatureInitDone(SceneReadyEvent e)
        {
            Load();

            EventManager.RemoveListener<SceneReadyEvent>(FeatureInitDone);
        }

        private void UIInidDone(InitializationUIDoneEvent e)
        {
            Load();
            AddUIListener();
            EventManager.RemoveListener<InitializationUIDoneEvent>(UIInidDone);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            panelBg1.color = theme.panelContentColor;
            panelBg2.color = theme.panelContentColor;

            for (int i = 0; i < allGenericText.Length; i++)
            {
                allGenericText[i].color = theme.genericTextColor;
            }
        }

        private void ChangeTabContentListener(int selected)
        {
            for (int i = 0; i < tabContents.Length; i++)
            {
                tabContents[i].SetActive(i == selected);
            }
        }

        private void ResetButtonCallback()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Reset setting ?", ResetSetting, null));
        }

        private void BackButtonCallback()
        {
            SettingData saved = SettingUtility.GetSavedSetting();
            if (string.Equals(JsonUtility.ToJson(saved), JsonUtility.ToJson(currentSetting)))
                KeepSettingCallback();
            else
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Keep setting ?", KeepSettingCallback, DiscardSettingCallback));
        }

        private void KeepSettingCallback()
        {
            Debug.Log("save setting");
            SettingUtility.SetSettingData(currentSetting);
            ClosePanel();
        }

        private void DiscardSettingCallback()
        {
            Debug.Log("discard setting");
            currentSetting = SettingUtility.GetSavedSetting();

            SetUI();
            Apply();
            ClosePanel();
        }

        private void Load()
        {
            Debug.Log("load setting");
            currentSetting = SettingUtility.GetSavedSetting();

            SetUI();
            Apply();
        }

        private void Apply()
        {
            Debug.Log("apply setting");

            int width;
            int height;

            if (currentSetting.resolution <= resolutions.Count - 1)
            {
                width = resolutions[currentSetting.resolution].width;
                height = resolutions[currentSetting.resolution].height;
            }
            else
            {
                width = resolutions[resolutions.Count - 1].width;
                height = resolutions[resolutions.Count - 1].height;
            }

            Screen.SetResolution(width, height, currentSetting.fullscreen);
            QualitySettings.SetQualityLevel(currentSetting.quality);

            Vector2 resolution = new Vector2(width, height);

            SettingUtility.SetLastSetting(currentSetting);
            SettingUtility.SetLastResolution(resolution);
            EventManager.TriggerEvent(new ApplySettingEvent(currentSetting, resolution));
            EventManager.TriggerEvent(new SwitchUIThemeEvent(currentSetting.uiTheme));
        }

        private void AddSliders()
        {
            sliders.Add(rotateSpeedSlider.GetComponent<InteractionSlider>());
            sliders.Add(zoomSpeedSlider.GetComponent<InteractionSlider>());
            sliders.Add(dragSpeedSlider.GetComponent<InteractionSlider>());
            sliders.Add(cameraTransitionSpeedSlider.GetComponent<InteractionSlider>());
            sliders.Add(voiceVolumeSlider.GetComponent<InteractionSlider>());
            sliders.Add(sfxVolumeSlider.GetComponent<InteractionSlider>());
            sliders.Add(vrCanvasDistanceSlider.GetComponent<InteractionSlider>());
            sliders.Add(vrMenuHeightSlider.GetComponent<InteractionSlider>());
        }

        private void SetUI()
        {
            Debug.Log("set ui setting");
            rotateSpeedSlider.value = currentSetting.rotateSpeed;
            zoomSpeedSlider.value = currentSetting.zoomSpeed;
            dragSpeedSlider.value = currentSetting.dragSpeed;
            cameraTransitionSpeedSlider.value = currentSetting.cameraTransitionSpeed;
            voiceVolumeSlider.value = currentSetting.voiceVolume;
            sfxVolumeSlider.value = currentSetting.sfxVolume;
            vrCanvasDistanceSlider.value = currentSetting.vrCanvasDistance;
            vrMenuHeightSlider.value = currentSetting.vrMenuHeight;
            resolutionDropdown.value = currentSetting.resolution;
            qualityDropdown.value = currentSetting.quality;
            dynamicUiSizeDropdown.value = currentSetting.dynamicUiSize ? 0 : 1;
            fullscreenDropdown.value = currentSetting.fullscreen ? 0 : 1;
            showEnvirontmentObjectDropdown.value = currentSetting.showEnvironmentObject ? 0 : 1;
            uiThemeDropdown.value = currentSetting.uiTheme;
        }

        private void ResetSetting()
        {
            Debug.Log("reset setting");
            string defaultData = JsonUtility.ToJson(defaultSetting);
            currentSetting = JsonUtility.FromJson<SettingData>(defaultData);

            SetUI();
            Apply();
            SettingUtility.SetSettingData(currentSetting);
        }

        private void UIThemeDropdownInit()
        {
            uiThemeDropdown.ClearOptions();
            List<string> themeOption = new List<string>();
            List<UITheme> themes = DatabaseManager.GetUIThemes();
            for (int i = 0; i < themes.Count; i++)
            {
                themeOption.Add(themes[i].name);
            }
            uiThemeDropdown.AddOptions(themeOption);

            if (themes.Count <= 1)
            {
                uiThemeDropdown.transform.parent.gameObject.SetActive(false);
            }
        }

        private void ResolutionOptionInit()
        {
            List<string> resOpt = new List<string>();
            Resolution[] res = Screen.resolutions;

            resolutions.Clear();

#if UNITY_EDITOR
            Resolution forka = new Resolution();
            forka.width = 3840;
            forka.height = 2160;
            resolutions.Add(forka);
            resOpt.Add("4k test");
#endif

            for (int i = 0; i < res.Length; i++)
            {
                float ar = (float)Screen.resolutions[i].width / (float)Screen.resolutions[i].height;
                if (ar >= 1.7 && ar < 1.8)
                {
                    if (!resOpt.Contains(res[i].width + " x " + res[i].height))
                    {
                        resolutions.Add(res[i]);
                        resOpt.Add(res[i].width + " x " + res[i].height);
                    }
                }
            }

            resolutionDropdown.ClearOptions();
            resolutionDropdown.AddOptions(resOpt);

            // resolution default
            defaultSetting.resolution = resOpt.Count - 1;
        }

        private void QualityOptionInit()
        {
            List<string> qualities = new List<string>();
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                qualities.Add(QualitySettings.names[i]);
            }

            qualityDropdown.ClearOptions();
            qualityDropdown.AddOptions(qualities);

            // quality default
            defaultSetting.quality = qualities.Count - 1;
        }

        private void OnSliderPointerUpCallback(float value)
        {
            Apply();
            AudioPlayer.PlaySFX(SFX.ButtonClick);
        }

        private void AddUIListener()
        {
            rotateSpeedSlider.onValueChanged.AddListener(ChangedRotateValue);
            zoomSpeedSlider.onValueChanged.AddListener(ChangedZoomValue);
            dragSpeedSlider.onValueChanged.AddListener(ChangedDragValue);
            cameraTransitionSpeedSlider.onValueChanged.AddListener(ChangedMoveCameraValue);
            voiceVolumeSlider.onValueChanged.AddListener(ChangedBGMValue);
            sfxVolumeSlider.onValueChanged.AddListener(ChangedSFXValue);
            vrCanvasDistanceSlider.onValueChanged.AddListener(ChangedVRCanvasDistanceValue);
            vrMenuHeightSlider.onValueChanged.AddListener(ChangedVRMenuHeightValue);
            resolutionDropdown.onValueChanged.AddListener(ChangedResolutionValue);
            dynamicUiSizeDropdown.onValueChanged.AddListener(ChangeDynamicUiSizeValue);
            qualityDropdown.onValueChanged.AddListener(ChangedQualityValue);
            fullscreenDropdown.onValueChanged.AddListener(ChangedFullscreenValue);
            showEnvirontmentObjectDropdown.onValueChanged.AddListener(ChangedShowBackgroundValue);
            uiThemeDropdown.onValueChanged.AddListener(ChangedUIThemeValue);

            for (int i = 0; i < sliders.Count; i++)
            {
                sliders[i].OnPointerUpCallback += OnSliderPointerUpCallback;
            }
        }

        private void RemoveUIListener()
        {
            rotateSpeedSlider.onValueChanged.RemoveListener(ChangedRotateValue);
            zoomSpeedSlider.onValueChanged.RemoveListener(ChangedZoomValue);
            dragSpeedSlider.onValueChanged.RemoveListener(ChangedDragValue);
            cameraTransitionSpeedSlider.onValueChanged.RemoveListener(ChangedMoveCameraValue);
            voiceVolumeSlider.onValueChanged.RemoveListener(ChangedBGMValue);
            sfxVolumeSlider.onValueChanged.RemoveListener(ChangedSFXValue);
            vrCanvasDistanceSlider.onValueChanged.RemoveListener(ChangedVRCanvasDistanceValue);
            vrMenuHeightSlider.onValueChanged.RemoveListener(ChangedVRMenuHeightValue);
            resolutionDropdown.onValueChanged.RemoveListener(ChangedResolutionValue);
            dynamicUiSizeDropdown.onValueChanged.RemoveListener(ChangeDynamicUiSizeValue);
            qualityDropdown.onValueChanged.RemoveListener(ChangedQualityValue);
            fullscreenDropdown.onValueChanged.RemoveListener(ChangedFullscreenValue);
            showEnvirontmentObjectDropdown.onValueChanged.RemoveListener(ChangedShowBackgroundValue);
            uiThemeDropdown.onValueChanged.RemoveListener(ChangedUIThemeValue);

            for (int i = 0; i < sliders.Count; i++)
            {
                sliders[i].OnPointerUpCallback -= OnSliderPointerUpCallback;
            }
        }

        private void SetMinMaxSetting()
        {
            var minMax = DatabaseManager.GetMinMaxSetting();

            rotateSpeedSlider.minValue = minMax.rotateSpeed.x;
            rotateSpeedSlider.maxValue = minMax.rotateSpeed.y;

            zoomSpeedSlider.minValue = minMax.zoomSpeed.x;
            zoomSpeedSlider.maxValue = minMax.zoomSpeed.y;

            dragSpeedSlider.minValue = minMax.dragSpeed.x;
            dragSpeedSlider.maxValue = minMax.dragSpeed.y;

            cameraTransitionSpeedSlider.minValue = minMax.moveCameraSpeed.x;
            cameraTransitionSpeedSlider.maxValue = minMax.moveCameraSpeed.y;

            voiceVolumeSlider.minValue = minMax.bgmVolume.x;
            voiceVolumeSlider.maxValue = minMax.bgmVolume.y;

            sfxVolumeSlider.minValue = minMax.sfxVolume.x;
            sfxVolumeSlider.maxValue = minMax.sfxVolume.y;

            vrCanvasDistanceSlider.minValue = minMax.vrCanvasDistance.x;
            vrCanvasDistanceSlider.maxValue = minMax.vrCanvasDistance.y;

            vrMenuHeightSlider.minValue = minMax.vrMenuHeight.x;
            vrMenuHeightSlider.maxValue = minMax.vrMenuHeight.y;
        }

        private void ChangedRotateValue(float value)
        {
            currentSetting.rotateSpeed = value;
        }

        private void ChangedZoomValue(float value)
        {
            currentSetting.zoomSpeed = value;
        }

        private void ChangedDragValue(float value)
        {
            currentSetting.dragSpeed = value;
        }

        private void ChangedMoveCameraValue(float value)
        {
            currentSetting.cameraTransitionSpeed = value;
        }

        private void ChangedBGMValue(float value)
        {
            currentSetting.voiceVolume = value;
        }

        private void ChangedSFXValue(float value)
        {
            currentSetting.sfxVolume = value;
        }

        private void ChangedVRMenuHeightValue(float value)
        {
            currentSetting.vrCanvasDistance = value;
        }

        private void ChangedVRCanvasDistanceValue(float value)
        {
            currentSetting.vrCanvasDistance = value;
        }

        private void ChangedResolutionValue(int value)
        {
            currentSetting.resolution = value;
            if (resolutions[value].height < MIN_UI_HEIGHT)
            {
                dynamicUiSizeDropdown.value = 1;
                dynamicUiSizeDropdown.interactable = false;
            }
            else
            {
                dynamicUiSizeDropdown.value = currentSetting.dynamicUiSize ? 0 : 1;
                dynamicUiSizeDropdown.interactable = true;
            }

            Apply();
        }

        private void ChangeDynamicUiSizeValue(int value)
        {
            currentSetting.dynamicUiSize = value == 0;
            Apply();
        }

        private void ChangedQualityValue(int value)
        {
            currentSetting.quality = value;
            Apply();
        }

        private void ChangedFullscreenValue(int value)
        {
            currentSetting.fullscreen = value == 0;
            Apply();
        }

        private void ChangedShowBackgroundValue(int value)
        {
            currentSetting.showEnvironmentObject = value == 0;
            Apply();
        }

        private void ChangedUIThemeValue(int value)
        {
            currentSetting.uiTheme = value;
            Apply();
        }
    }
}

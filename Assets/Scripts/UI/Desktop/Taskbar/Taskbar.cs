﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using DG.Tweening;
using VirtualTraining.SceneManagement;
using DanielLochner.Assets.SimpleScrollSnap;

namespace VirtualTraining.UI.Desktop
{
    public class Taskbar : UIElement
    {
        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] LandingPagePanel landingPagePanel;
        [SerializeField] MainMenuPanel mainMenuPanel;
        [SerializeField] SystemPanel systemPanel;
        [SerializeField] ManualPanel manualPanel;
        [SerializeField] SettingPanel settingPanel;
        [SerializeField] InteractionButton landingPageButton;
        [SerializeField] InteractionButton mainMenuButton;
        [SerializeField] InteractionButton freePlayButton;
        [SerializeField] InteractionButton settingButton;
        [SerializeField] InteractionButton expandButton;
        [SerializeField] InteractionButton vrViewButton;
        [SerializeField] InteractionButton desktopViewButton;
        [SerializeField] InteractionButton assistiveButton;
        [SerializeField] TaskbarExpandPanel expandPanel;
        [SerializeField] GameObject taskTemplate;
        [SerializeField] InteractionButton prevButton;
        [SerializeField] InteractionButton nextButton;
        [SerializeField] Transform taskParent;
        [SerializeField] SimpleScrollSnap simpleScrollSnap;
        [SerializeField] GameObject minimizedPanel;
        [SerializeField] InteractionButton networkingButton;
        [SerializeField] GameObject toolbarPanel;
        [SerializeField] Image menuPanelBackground;
        [SerializeField] Image expandPanelBackground;
        [SerializeField] Image minimizedPanelBackground;
        [SerializeField] UIPanel[] mainMenuPanels;

        const int MAX_TASK = 1;
        const float ANIMATION_DURATION = 0.5f;

        RectTransform rectTransform;

        UIPanel mainMenuLastOpenedPanel;

        int TaskCount
        {
            get
            {
                if (simpleScrollSnap.Panels == null)
                    return 0;

                int x = 0;
                for (int i = 0; i < simpleScrollSnap.Panels.Length; i++)
                {
                    if (simpleScrollSnap.Panels[i] != null)
                        x++;
                }
                return x;
            }
        }

        protected override void Start()
        {
            base.Start();
            rectTransform = GetComponent<RectTransform>();
            EventManager.AddListener<MinimizeUIPanelEvent>(MinimizeUIPanelListener);
            EventManager.AddListener<ShowUIPanelEvent>(ShowPanelListener);
            EventManager.AddListener<CloseUIPanelEvent>(ClosePanelListener);
            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(SessionStoppedListener);

            landingPageButton.OnClickEvent += ShowLandingPagePressed;
            mainMenuButton.OnClickEvent += MainMenuButtonListener;
            freePlayButton.OnClickEvent += FreePlayButtonListener;
            settingButton.OnClickEvent += SettingButtonListener;
            expandButton.OnClickEvent += ExpandTaskbarListener;
            vrViewButton.OnClickEvent += VRViewClicked;
            desktopViewButton.OnClickEvent += DesktopViewClicked;
            networkingButton.OnClickEvent += OpenNetworking;
            assistiveButton.OnClickEvent += AssistiveButtonListener;

            prevButton.OnClickEvent += Prev;
            nextButton.OnClickEvent += Next;

            CheckTaskCount();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MinimizeUIPanelEvent>(MinimizeUIPanelListener);
            EventManager.RemoveListener<ShowUIPanelEvent>(ShowPanelListener);
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(SessionStoppedListener);
            EventManager.RemoveListener<CloseUIPanelEvent>(ClosePanelListener);

            landingPageButton.OnClickEvent -= ShowLandingPagePressed;
            mainMenuButton.OnClickEvent -= MainMenuButtonListener;
            freePlayButton.OnClickEvent -= FreePlayButtonListener;
            settingButton.OnClickEvent -= SettingButtonListener;
            expandButton.OnClickEvent -= ExpandTaskbarListener;
            vrViewButton.OnClickEvent -= VRViewClicked;
            desktopViewButton.OnClickEvent -= DesktopViewClicked;
            networkingButton.OnClickEvent -= OpenNetworking;
            assistiveButton.OnClickEvent -= AssistiveButtonListener;

            prevButton.OnClickEvent -= Prev;
            nextButton.OnClickEvent -= Next;
        }

        protected override void ApplyTheme()
        {
            menuPanelBackground.color = theme.panelTitleColor;
            expandPanelBackground.color = theme.panelTitleColor;
            minimizedPanelBackground.color = theme.panelTitleColor;
        }

        private void OpenNetworking()
        {
            EventManager.TriggerEvent(new CloseAllUIPanelEvent());
            EventManager.TriggerEvent(new FreePlayEvent());
            VirtualTrainingSceneManager.OpenNetworking();
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            networkingButton.gameObject.SetActive(false);
        }

        private void SessionStoppedListener(OnClientStopEvent e)
        {
            networkingButton.gameObject.SetActive(true);
        }

        public void Show()
        {
            canvasGroup.DOFade(1f, ANIMATION_DURATION);
        }

        private void ShowLandingPagePressed()
        {
            EventManager.TriggerEvent(new FreePlayEvent());
            canvasGroup.DOFade(0f, ANIMATION_DURATION);
            landingPagePanel.ShowPanel();
        }

        private void AssistiveButtonListener()
        {
            toolbarPanel.SetActive(!toolbarPanel.activeSelf);
        }

        private void MinimizeUIPanelListener(MinimizeUIPanelEvent e)
        {
            // add panel
            simpleScrollSnap.AddToBack(taskTemplate);
            simpleScrollSnap.GoToPanel(simpleScrollSnap.Panels.Length - 1);

            Task newTask = simpleScrollSnap.Panels[simpleScrollSnap.Panels.Length - 1].gameObject.GetComponent<Task>();

            newTask.Setup(taskParent, e.panelName, (task) =>
            {
                e.maximizeCallback();
            });

            newTask.transform.DOScaleX(1f, ANIMATION_DURATION).OnComplete(() =>
            {
                CheckTaskCount();
            });
        }

        private void CheckTaskCount()
        {
            minimizedPanel.SetActive(TaskCount > 0);

            bool active = TaskCount > MAX_TASK ? true : false;
            prevButton.gameObject.SetActive(active);
            nextButton.gameObject.SetActive(active);
        }

        private void ShowPanelListener(ShowUIPanelEvent e)
        {
            // main menu panels
            for (int i = 0; i < mainMenuPanels.Length; i++)
            {
                if (mainMenuPanels[i].gameObject == e.panelObj)
                {
                    mainMenuLastOpenedPanel = mainMenuPanels[i];
                }
            }

            // handle restored panels
            if (simpleScrollSnap.Panels == null)
                return;

            for (int i = 0; i < simpleScrollSnap.Panels.Length; i++)
            {
                if (simpleScrollSnap.Panels[i] != null && simpleScrollSnap.Panels[i].gameObject.name == e.panelObj.name)
                {
                    simpleScrollSnap.Remove(i);
                    CheckTaskCount();
                    break;
                }
            }
        }

        private void ClosePanelListener(CloseUIPanelEvent e)
        {
            // handle closed panels
            if (simpleScrollSnap.Panels == null)
                return;

            for (int i = 0; i < simpleScrollSnap.Panels.Length; i++)
            {
                if (simpleScrollSnap.Panels[i] != null && simpleScrollSnap.Panels[i].gameObject.name == e.name)
                {
                    simpleScrollSnap.Remove(i);
                    break;
                }
            }
        }

        private void MainMenuButtonListener()
        {
            foreach (var panel in mainMenuPanels)
            {
                if (panel.IsPanelActive)
                    panel.ClosePanel();
            }

            if (mainMenuLastOpenedPanel == null)
            {
                if (mainMenuPanel.IsPanelActive)
                    mainMenuPanel.ClosePanel();
                else
                    mainMenuPanel.ShowPanel();
            }
            else
            {
                mainMenuLastOpenedPanel.ShowPanel();
            }
        }

        private void ExpandTaskbarListener()
        {
            if (expandPanel.IsPanelActive)
                expandPanel.ClosePanel();
            else
                expandPanel.ShowPanel();
        }

        private void FreePlayButtonListener()
        {
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        public void SettingButtonListener()
        {
            if (settingPanel.IsPanelActive)
                settingPanel.ClosePanel();
            else
                settingPanel.ShowPanel(null);
        }

        void VRViewClicked()
        {
            VirtualTrainingSceneManager.LoadHeadVRScene(VRviewCallback);
        }

        void VRviewCallback()
        {
            desktopViewButton.gameObject.SetActive(true);
            vrViewButton.gameObject.SetActive(false);
        }

        void DesktopViewClicked()
        {
            VirtualTrainingSceneManager.UnloadHeadVRScene();
            desktopViewButton.gameObject.SetActive(false);
            vrViewButton.gameObject.SetActive(true);
        }

        public bool RectContain(Vector2 position)
        {
            return rectTransform.RectContains(position);
        }

        private void Prev()
        {
            simpleScrollSnap.GoToPreviousPanel();
        }

        private void Next()
        {
            simpleScrollSnap.GoToNextPanel();
        }
    }
}
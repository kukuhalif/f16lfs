﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;
using VirtualTraining.UI;

namespace VirtualTraining.UI.Desktop
{
    public class Task : UIElement
    {
        [SerializeField] TMP_Text taskName;
        [SerializeField] InteractionButton button;

        Action<Task> showPanelCallback;

        protected override void Start()
        {
            base.Start();
            button.OnClickEvent += ShowPanel;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            button.OnClickEvent -= ShowPanel;
        }

        public void Setup(Transform parent, string name, Action<Task> showPanelCallback)
        {
            transform.SetParent(parent);
            transform.localScale = new Vector3(0, 1, 1);
            this.taskName.text = name;
            gameObject.name = name;
            gameObject.SetActive(true);
            this.showPanelCallback = showPanelCallback;
        }

        private void ShowPanel()
        {
            showPanelCallback.Invoke(this);
        }
    }
}

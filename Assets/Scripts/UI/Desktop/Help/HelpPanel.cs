using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.Desktop
{
    public class HelpPanel : UIPanel
    {
        [SerializeField] InteractionButton controlButton;
        [SerializeField] InteractionButton menuButton;
        [SerializeField] InteractionButton toolbarButton;
        [SerializeField] InteractionButton quizButton;

        protected override void Start()
        {
            base.Start();

            controlButton.OnClickEvent += ShowControlHelp;
            menuButton.OnClickEvent += ShowMenuHelp;
            toolbarButton.OnClickEvent += ShowToolbarHelp;
            quizButton.OnClickEvent += ShowQuizHelp;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            controlButton.OnClickEvent -= ShowControlHelp;
            menuButton.OnClickEvent -= ShowMenuHelp;
            toolbarButton.OnClickEvent -= ShowToolbarHelp;
            quizButton.OnClickEvent -= ShowQuizHelp;
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (IsPanelMinimized)
                ShowPanel();
        }

        private void ShowControlHelp()
        {
            Debug.Log("show control help");
        }

        private void ShowMenuHelp()
        {
            Debug.Log("show menu help");
        }

        private void ShowToolbarHelp()
        {
            Debug.Log("show toolbar help");
        }

        private void ShowQuizHelp()
        {
            Debug.Log("show quiz help");
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUISchematic : TreeViewUI
    {
        class SchematicTreeElement : TreeElement
        {
            public Schematic schematic;

            public override string GetDuplicateJsonData()
            {
                return "";
            }

            public override void SetDuplicatedJsonData(string data)
            {

            }

            public SchematicTreeElement(Schematic schematic, string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
            {
                this.schematic = schematic;
            }
        }

        [SerializeField] CanvasGroup canvasGroup;

        public void Show(bool show, float duration)
        {
            if (show)
                gameObject.SetActive(true);

            if (canvasGroup == null)
            {
                if (!show)
                    gameObject.SetActive(false);
            }
            else
            {
                canvasGroup.DOFade(show ? 1f : 0f, duration).OnComplete(() =>
                {
                    if (!show)
                        gameObject.SetActive(false);
                });
            }
        }

        public void SetSchematicData(List<Schematic> schematics)
        {
            if (schematics == null || schematics.Count == 0)
            {
                ClearElements();
                return;
            }

            List<TreeElement> treeElements = new List<TreeElement>();
            treeElements.Add(new SchematicTreeElement(null, "root", -1, -1, true));
            for (int i = 0; i < schematics.Count; i++)
            {
                string name = "";
                if (schematics[i] != null)
                {
                    if (schematics[i].video != null)
                        name = schematics[i].video.name;
                    else if (schematics[i].image != null)
                        name = schematics[i].image.name;
                }

                SchematicTreeElement newElement = new SchematicTreeElement(schematics[i], name, 0, i, false);
                treeElements.Add(newElement);
            }

            TreeElement root = TreeElementUtility.ListToTree(treeElements);
            GenerateElements(root);
        }

        protected override void ClickedElementCallback(TreeElement element)
        {
            SchematicTreeElement ste = element as SchematicTreeElement;
            EventManager.TriggerEvent(new ShowSchematicEvent(ste.schematic, false));
        }

        protected override TreeViewUIElement GetElementTemplate()
        {
            GameObject template = Resources.Load("UI/Tree View Element Template") as GameObject;
            return template.GetComponent<TreeViewUIElement>();
        }
    }
}


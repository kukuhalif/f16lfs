using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class SchematicPanel : UIPanel
    {
        [SerializeField] RawImage ImageTexture;
        [SerializeField] RawImage VideoRawTexture;
        [SerializeField] VideoController videoController;
        [SerializeField] RectTransform headerRect;
        [SerializeField] RectTransform bottomRect;
        [SerializeField] RenderTexture defaultRenderTexture;
        [SerializeField] float defaultWidth;
        [SerializeField] AspectRatioFitter imageAspectRatio;
        [SerializeField] AspectRatioFitter videoAspectRatio;
        [SerializeField] InteractionButton showTreeViewButton;
        [SerializeField] InteractionButton hideTreeViewButton;
        [SerializeField] TreeViewUISchematic treeViewSchematic;
        [SerializeField] Image treeViewBackground;

        private List<Schematic> schematics;
        private Schematic currentSchematic;
        private Vector2 targetSize;

        private float zoomSpeed;
        private bool isDraggingZoom;
        private float lastPinchDistance;
        private float scrollValue;
        private Vector2 pointerDeltaPosition = new Vector2();
        private Transform schematicTarget;
        private Canvas raycastedCanvas;

        private bool isGrabbing;
        private Vector2 dragSmooth;

        private float DragSpeed { get => SettingUtility.LastSetting.dragSpeed; }

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<SetSchematicDataEvent>(SetSchematicDataListener);
            EventManager.AddListener<ShowSchematicEvent>(ShowSchematicListener);
            EventManager.AddListener<ShowSchematicLinkEvent>(SchematicLinkListener);
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

            showTreeViewButton.OnClickEvent += ShowTreeViewButtonListener;
            hideTreeViewButton.OnClickEvent += HideTreeViewButtonListener;

            StartCoroutine(InitializeTree());
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<SetSchematicDataEvent>(SetSchematicDataListener);
            EventManager.RemoveListener<ShowSchematicEvent>(ShowSchematicListener);
            EventManager.RemoveListener<ShowSchematicLinkEvent>(SchematicLinkListener);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);

            showTreeViewButton.OnClickEvent -= ShowTreeViewButtonListener;
            hideTreeViewButton.OnClickEvent -= HideTreeViewButtonListener;
        }

        private IEnumerator InitializeTree()
        {
            treeViewSchematic.gameObject.SetActive(true);
            yield return null;
            treeViewSchematic.gameObject.SetActive(false);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            treeViewBackground.color = theme.panelContentColor;
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            isDraggingZoom = false;
            lastPinchDistance = 0f;
            scrollValue = 0f;
            pointerDeltaPosition = new Vector2();
            if (schematicTarget != null)
                schematicTarget.localScale = Vector3.one;

            EventManager.AddListener<MateriEvent>(PlayMateriListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            VirtualTrainingInputSystem.RaycastedUIUpdated += RaycastedUiUpdatedListener;
            VirtualTrainingInputSystem.OnMouseScroll += MouseScrollListener;
            VirtualTrainingInputSystem.OnStartRightClick += StartRightClickListener;
            VirtualTrainingInputSystem.OnEndRightClick += EndRightClickListener;
            VirtualTrainingInputSystem.OnMovePointer += OnMovePointerListener;
            VirtualTrainingInputSystem.OnStartLeftClick += StartLeftClickListener;
            VirtualTrainingInputSystem.OnEndLeftClick += EndLeftClickListener;

            VirtualTrainingInputSystem.OnTouchRemoved += TouchRemoved;

            var cg = showTreeViewButton.GetComponent<CanvasGroup>();
            cg.ignoreParentGroups = true;
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            videoController.ReleaseTexture();
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);

            VirtualTrainingInputSystem.RaycastedUIUpdated -= RaycastedUiUpdatedListener;
            VirtualTrainingInputSystem.OnMouseScroll -= MouseScrollListener;
            VirtualTrainingInputSystem.OnStartRightClick -= StartRightClickListener;
            VirtualTrainingInputSystem.OnEndRightClick -= EndRightClickListener;
            VirtualTrainingInputSystem.OnMovePointer -= OnMovePointerListener;
            VirtualTrainingInputSystem.OnStartLeftClick -= StartLeftClickListener;
            VirtualTrainingInputSystem.OnEndLeftClick -= EndLeftClickListener;
            VirtualTrainingInputSystem.OnTouchRemoved -= TouchRemoved;

            treeViewSchematic.Show(false, 0f);
        }

        private void TouchRemoved(bool isFingerOverUI)
        {
            isDraggingZoom = false;
        }

        private void ShowTreeViewButtonListener()
        {
            treeViewSchematic.Show(true, AnimationDuration);
        }

        private void HideTreeViewButtonListener()
        {
            treeViewSchematic.Show(false, AnimationDuration);
        }

        private void RaycastedUiUpdatedListener(GameObject raycastedUI)
        {
            if (raycastedUI != null)
                raycastedCanvas = raycastedUI.transform.GetComponentsInParentRecursive<Canvas>();
            else
                raycastedCanvas = null;
        }

        private void MouseScrollListener(float value, GameObject raycastedUI)
        {
            if (raycastedUI == null)
            {
                scrollValue = 0f;
                return;
            }

            TreeViewUISchematic tv = raycastedUI.transform.GetComponentsInParentRecursive<TreeViewUISchematic>();
            if (tv != null)
            {
                scrollValue = 0f;
                return;
            }

            VideoController vc = raycastedUI.transform.GetComponentsInParentRecursive<VideoController>();
            if (vc != null)
            {
                scrollValue = 0f;
                return;
            }

            raycastedCanvas = raycastedUI.transform.GetComponentsInParentRecursive<Canvas>();
            if (raycastedCanvas == null)
            {
                scrollValue = 0f;
                return;
            }

            if (raycastedCanvas != Canvas)
            {
                scrollValue = 0f;
                return;
            }

            scrollValue = value;
        }

        private void StartRightClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI == null)
            {
                isDraggingZoom = false;
                return;
            }

            TreeViewUISchematic tv = raycastedUI.transform.GetComponentsInParentRecursive<TreeViewUISchematic>();
            if (tv != null)
            {
                isDraggingZoom = false;
                return;
            }

            VideoController vc = raycastedUI.transform.GetComponentsInParentRecursive<VideoController>();
            if (vc != null)
            {
                isDraggingZoom = false;
                return;
            }

            raycastedCanvas = raycastedUI.transform.GetComponentsInParentRecursive<Canvas>();
            if (raycastedCanvas == null)
            {
                isDraggingZoom = false;
                return;
            }

            if (raycastedCanvas != Canvas)
            {
                isDraggingZoom = false;
                return;
            }

            isDraggingZoom = true;
        }

        private void EndRightClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isDraggingZoom = false;
        }

        private void OnMovePointerListener(Vector2 deltaPosition)
        {
            pointerDeltaPosition = deltaPosition;
        }

        private void StartLeftClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI == null)
            {
                isGrabbing = false;
                return;
            }

            TreeViewUISchematic tv = raycastedUI.transform.GetComponentsInParentRecursive<TreeViewUISchematic>();
            if (tv != null)
            {
                isGrabbing = false;
                return;
            }

            VideoController vc = raycastedUI.transform.GetComponentsInParentRecursive<VideoController>();
            if (vc != null)
            {
                isGrabbing = false;
                return;
            }

            raycastedCanvas = raycastedUI.transform.GetComponentsInParentRecursive<Canvas>();
            if (raycastedCanvas == null)
            {
                isGrabbing = false;
                return;
            }

            if (raycastedCanvas != Canvas)
            {
                isGrabbing = false;
                return;
            }

            if (VirtualTrainingInputSystem.TouchCount <= 1)
                isGrabbing = true;
            else
                isGrabbing = false;
        }

        private void EndLeftClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isGrabbing = false;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            DisableOldSchematic();
            ClosePanel();
        }

        private void PlayMateriListener(MateriEvent e)
        {
            DisableOldSchematic();
            ClosePanel();
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            zoomSpeed = e.data.zoomSpeed;
            DisableOldSchematic();
            ClosePanel();
        }

        private float CalculatePinchZoom(Vector2 touchZero, Vector2 touchOne, float zoomDistance)
        {
            float currentDistance = Vector2.Distance(touchZero, touchOne);

            float result = 0f;

            if (currentDistance > lastPinchDistance)
            {
                result = -Time.deltaTime * zoomDistance;
            }
            else if (currentDistance < lastPinchDistance)
            {
                result = Time.deltaTime * zoomDistance;
            }

            lastPinchDistance = currentDistance;

            return result;
        }

        private float CalculateZoomValue()
        {
            if (VirtualTrainingInputSystem.TouchCount == 2)
            {
                if (raycastedCanvas == null)
                    return 0f;

                if (raycastedCanvas != Canvas)
                    return 0f;

                return CalculatePinchZoom(VirtualTrainingInputSystem.GetTouchPosition(0), VirtualTrainingInputSystem.GetTouchPosition(1), 1f);
            }
            else if (isDraggingZoom)
            {
                return pointerDeltaPosition.y * Time.deltaTime;
            }
            else if (scrollValue != 0f)
            {
                if (scrollValue > 0)
                    return -Time.deltaTime;
                else if (scrollValue < 0)
                    return Time.deltaTime;
            }

            return 0f;
        }

        private void ZoomUpdate()
        {
            float zoomValue = CalculateZoomValue() * zoomSpeed;
            schematicTarget.localScale -= new Vector3(zoomValue, zoomValue, 1);
            if (schematicTarget.localScale.x < 0.5f)
                schematicTarget.localScale = new Vector3(0.5f, 0.5f, 1f);
            else if (schematicTarget.localScale.x > 5f)
                schematicTarget.localScale = new Vector3(5f, 5f, 1f);
        }

        private void DragUpdate()
        {
            if (isGrabbing)
            {
                dragSmooth = Vector2.Lerp(dragSmooth, pointerDeltaPosition, Time.deltaTime * 10);
                schematicTarget.Translate(dragSmooth * DragSpeed, Space.Self);
            }
            else
            {
                dragSmooth = Vector2.zero;
            }
        }

        private void Update()
        {
            if (schematicTarget == null || IsDragging || IsResizing)
                return;

            DragUpdate();
            ZoomUpdate();
        }

        private void DisableOldSchematic()
        {
            if (videoController != null)
                videoController.SetActive(false);

            if (ImageTexture != null)
                ImageTexture.gameObject.SetActive(false);

            if (VideoRawTexture != null)
                VideoRawTexture.gameObject.SetActive(false);
        }

        private void SetSchematicDataListener(SetSchematicDataEvent e)
        {
            currentSchematic = null;
            schematics = e.schematics;

            treeViewSchematic.Show(false, AnimationDuration);
            treeViewSchematic.SetSchematicData(e.schematics);

            if (schematics == null || schematics.Count == 0)
            {
                ClosePanel();
                return;
            }

            for (int i = 0; i < schematics.Count; i++)
            {
                if (schematics[i].showImmediately)
                {
                    DisableOldSchematic();
                    ShowPanel(() =>
                    {
                        Show(schematics[i], true);
                    });

                    return;
                }
            }
        }

        private void ShowSchematicListener(ShowSchematicEvent e)
        {
            DisableOldSchematic();
            ShowPanel(() =>
            {
                Show(e.schematic, e.restorePanel);
            }, e.restorePanel);
        }

        private void SchematicLinkListener(ShowSchematicLinkEvent e)
        {
            for (int i = 0; i < schematics.Count; i++)
            {
                if (e.id == schematics[i].descriptionId)
                {
                    DisableOldSchematic();
                    ShowPanel(() =>
                    {
                        Show(schematics[i], true);
                    });
                    return;
                }
            }

            Debug.LogError("schematic link not found");
        }

        private void SetTargetHeight(float width, float height, bool video)
        {
            float additionalHeight = headerRect.rect.height;
            if (video)
                additionalHeight += bottomRect.rect.height;

            targetSize.x = defaultWidth;
            float aspectRatio = width / height;
            targetSize.y = (targetSize.x / aspectRatio) + additionalHeight;
        }

        private void Show(Schematic schematic, bool restorePanel)
        {
            videoController.Stop();

            if (schematic.image != null)
            {
                SetTargetHeight(schematic.image.width, schematic.image.height, false);
                imageAspectRatio.aspectRatio = (float)schematic.image.width / (float)schematic.image.height;
                SetTitleText(schematic.image.name);
                videoController.SetActive(false);
                ImageTexture.gameObject.SetActive(true);
                VideoRawTexture.gameObject.SetActive(false);
                ImageTexture.texture = schematic.image;
                currentSchematic = schematic;
                schematicTarget = ImageTexture.transform;

            }
            else if (schematic.video != null)
            {
                int sWidth = (int)schematic.video.width;
                int sHeight = (int)schematic.video.height;

                SetTargetHeight(sWidth, sHeight, true);
                videoAspectRatio.aspectRatio = (float)sWidth / (float)sHeight;

                if (VideoRawTexture.texture.height != sHeight || VideoRawTexture.texture.width != sWidth)
                {
                    RenderTexture newRT = new RenderTexture(sWidth, sHeight, defaultRenderTexture.depth);
                    VideoRawTexture.texture = newRT;
                    videoController.SetRenderTexture(newRT);
                }

                SetTitleText(schematic.video.name);
                videoController.SetActive(true);
                ImageTexture.gameObject.SetActive(false);
                VideoRawTexture.gameObject.SetActive(true);
                videoController.Play(schematic.video, schematic.isLooping);
                currentSchematic = schematic;
                schematicTarget = VideoRawTexture.transform;
            }

            SetRestoredDefaultSize(targetSize, restorePanel);
            schematicTarget.localScale = Vector3.one;
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (currentSchematic != null)
                base.ShowAllPanelListener(e);
        }

        protected override void OnStartMinimizePanel()
        {
            if (showTreeViewButton == null)
                return;

            var cg = showTreeViewButton.GetComponent<CanvasGroup>();
            cg.ignoreParentGroups = false;
        }

        protected override void OnStartClosePanel()
        {
            if (showTreeViewButton == null)
                return;

            var cg = showTreeViewButton.GetComponent<CanvasGroup>();
            cg.ignoreParentGroups = false;
        }
    }
}


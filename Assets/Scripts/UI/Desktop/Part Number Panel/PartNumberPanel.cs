using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class PartNumberPanel : UIPanel
    {
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] VerticalLayoutGroup verticalLayoutGroup;

        protected override void Start()
        {
            base.Start();

            text.text = "";
            EventManager.AddListener<ObjectInteractionPartNumberPanelEvent>(PartNumberPanelListener);
            EventManager.AddListener<PartNumberInteractionEvent>(ShowPartNumberListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<ObjectInteractionPartNumberPanelEvent>(PartNumberPanelListener);
            EventManager.RemoveListener<PartNumberInteractionEvent>(ShowPartNumberListener);
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            EventManager.TriggerEvent(new PartNumberPanelEvent(true));
            StartCoroutine(RefreshText());
        }


        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            EventManager.TriggerEvent(new PartNumberPanelEvent(false));
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (IsPanelMinimized)
                ShowPanel();
        }

        private void PartNumberPanelListener(ObjectInteractionPartNumberPanelEvent e)
        {
            text.text = "";

            if (e.show)
                ShowPanel();
            else
                ClosePanel();
        }

        private void ShowPartNumberListener(PartNumberInteractionEvent e)
        {
            if (e.gameObject == null)
            {
                text.text = "";
            }
            else
            {
                string name = "object name : " + e.gameObject.name;
                string obj = "part number : " + e.partNumber;
                text.text = name + System.Environment.NewLine + obj;
            }

            if (gameObject.activeInHierarchy)
                StartCoroutine(RefreshText());
        }

        private IEnumerator RefreshText()
        {
            verticalLayoutGroup.enabled = false;
            yield return null;
            verticalLayoutGroup.enabled = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class MainMenuPanel : UIPanel
    {
        [SerializeField] SystemPanel systemPanel;
        [SerializeField] ManualPanel manualPanel;
        [SerializeField] HelpPanel helpPanel;
        [SerializeField] Taskbar taskbar;
        [SerializeField] Image projectIcon;
        [SerializeField] TextMeshProUGUI projectName;
        [SerializeField] TextMeshProUGUI projectSubName;
        [SerializeField] InteractionButton systemButton;
        [SerializeField] InteractionButton freeplayButton;
        [SerializeField] InteractionButton quizButton;
        [SerializeField] InteractionButton manualButton;
        [SerializeField] InteractionButton helpButton;
        [SerializeField] InteractionButton aboutButton;
        [SerializeField] InteractionButton quitButton;
        [SerializeField] InteractionButton vrButton;
        [SerializeField] TextMeshProUGUI[] titleTexts;


        protected override void Start()
        {
            base.Start();

            Sprite icon = DatabaseManager.GetProjectDetails().mainMenuIcon;
            if (icon == null)
                projectIcon.gameObject.SetActive(false);
            else
            {
                projectIcon.sprite = icon;
                AspectRatioFitter aspectRatioFitter = projectIcon.GetComponent<AspectRatioFitter>();
                aspectRatioFitter.aspectRatio = (float)projectIcon.sprite.texture.width / (float)projectIcon.sprite.texture.height;
            }

            projectName.text = Application.productName;
            projectSubName.text = DatabaseManager.GetProjectDetails().projectDescription;

            systemButton.OnClickEvent += ShowSystem;
            freeplayButton.OnClickEvent += FreePlay;
            quizButton.OnClickEvent += LoadQuiz;
            manualButton.OnClickEvent += ShowManual;
            helpButton.OnClickEvent += ShowHelp;
            aboutButton.OnClickEvent += ShowAbout;
            quitButton.OnClickEvent += Quit;
            vrButton.OnClickEvent += VRModeSwitch;

            LayoutElement[] layouts = transform.GetAllComponentsInChildsExcludeThis<LayoutElement>();
            for (int i = 0; i < layouts.Length; i++)
            {
                layouts[i].CalculateLayoutInputHorizontal();
                layouts[i].CalculateLayoutInputVertical();
            }

            if(DatabaseManager.GetContentVRData().isVREnable == false)
            {
                vrButton.gameObject.SetActive(false);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            systemButton.OnClickEvent -= ShowSystem;
            freeplayButton.OnClickEvent -= FreePlay;
            quizButton.OnClickEvent -= LoadQuiz;
            manualButton.OnClickEvent -= ShowManual;
            helpButton.OnClickEvent -= ShowHelp;
            aboutButton.OnClickEvent -= ShowAbout;
            quitButton.OnClickEvent -= Quit;
            vrButton.OnClickEvent -= VRModeSwitch;
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            VirtualTrainingInputSystem.OnStartLeftClick += StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick += StartClick;
            VirtualTrainingInputSystem.OnStartRightClick += StartClick;
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            VirtualTrainingInputSystem.OnStartLeftClick -= StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick -= StartClick;
            VirtualTrainingInputSystem.OnStartRightClick -= StartClick;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            for (int i = 0; i < titleTexts.Length; i++)
            {
                titleTexts[i].color = theme.panelTitleTextColor;
            }
        }

        private void StartClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (!BodyRect.RectContains(pointerPosition) && !taskbar.RectContain(pointerPosition))
                ClosePanel();
        }

        private void ShowSystem()
        {
            systemPanel.ShowPanel();
            ClosePanel();
        }

        private void FreePlay()
        {
            ClosePanel();
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        private void LoadQuiz()
        {
            if (VirtualTrainingSceneManager.CheckDatabaseQuiz() == false)
                return;

            ClosePanel(() =>
            {
                VirtualTrainingSceneManager.OpenQuiz();
            }, true, true, false);
        }

        private void ShowManual()
        {
            manualPanel.ShowPanel();
            ClosePanel();
        }

        private void ShowHelp()
        {
            //EventManager.TriggerEvent(new ShowPdfEvent(DatabaseManager.GetProjectDetails().helpPDF));
            helpPanel.ShowPanel();
            ClosePanel();
        }

        private void ShowAbout()
        {
            EventManager.TriggerEvent(new ShowAboutPanelEvent(null));
            ClosePanel();
        }

        private void Quit()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to exit?", QuitCallback, null));
        }

        void VRModeSwitch()
        {
            VirtualTrainingSceneManager.LoadVRScene();
        }

        private void QuitCallback()
        {
            EventManager.TriggerEvent(new QuitAppEvent());
        }
    }
}

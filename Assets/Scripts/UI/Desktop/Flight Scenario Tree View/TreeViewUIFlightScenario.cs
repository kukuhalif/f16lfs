using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIFlightScenario : TreeViewUI
    {
        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<MateriEvent>(MateriListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MateriEvent>(MateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void MateriListener(MateriEvent e)
        {
            ClearElements();
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            //ClearElements();
        }

        protected override TreeViewUIElement GetElementTemplate()
        {
            GameObject template = Resources.Load("UI/Tree View Element Flight Scenario Template") as GameObject;
            template.transform.localEulerAngles = Vector3.zero;
            return template.GetComponent<TreeViewUIElementFlightScenario>();
        }

        protected override void ClickedElementCallback(TreeElement element)
        {
            FlightScenarioTreeElement flightScenario = element as FlightScenarioTreeElement;
            EventManager.TriggerEvent(new FlightScenarioPlayEvent(flightScenario));
        }

        private TreeViewUIElementFlightScenario GetFlightScenarioElement(TreeElement element)
        {
            return UiElementLookup[element.id] as TreeViewUIElementFlightScenario;
        }

        protected override void SetupElementCallback(TreeViewUIElement uiElement, TreeElement element)
        {
            FlightScenarioTreeElement flightScenario = element as FlightScenarioTreeElement;
            string text = string.IsNullOrEmpty(flightScenario.data.description) ?
                flightScenario.data.title + System.Environment.NewLine :
                flightScenario.data.description + System.Environment.NewLine;
            uiElement.SetText(text);
            TreeViewUIElementFlightScenario flightScenarioElementUI = uiElement as TreeViewUIElementFlightScenario;
            flightScenarioElementUI.SetGetElementCallback(GetFlightScenarioElement);
            flightScenarioElementUI.SetFlightScenarioData(flightScenario.data);

            uiElement.gameObject.transform.localEulerAngles = Vector3.zero;
            uiElement.gameObject.transform.localPosition = new Vector3(uiElement.gameObject.transform.localPosition.x, uiElement.gameObject.transform.localPosition.y,
                0);


        }

        public void GenerateTree(FlightScenarioTreeElement root)
        {
            GenerateElements(root);
        }
    }
}

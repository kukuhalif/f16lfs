using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TreeViewUIElementFlightScenario : TreeViewUIElement
    {
        [SerializeField] TEXDraw texDraw;
        [SerializeField] TextLink textLink;
        [SerializeField] UILineRenderer upperLine;
        [SerializeField] UILineRenderer childLine;
        [SerializeField] UILineRenderer siblingLine;
        [SerializeField] ContentSizeFitter textFitter;
        [SerializeField] LayoutElement textLayoutElement;

        Func<TreeElement, TreeViewUIElementFlightScenario> getElementCallback;

        // todo : refactor line hierarchy to be more generic
        const float DEFAULT_UPPER_LINE_HEIGHT = 14f;
        bool refreshingUI;

        private void Start()
        {
            // enable / disable upper line
            if (Data.depth <= 0)
                upperLine.gameObject.SetActive(false);

            // enable / disable child line
            childLine.gameObject.SetActive(false);

            // enable / disable sibling line
            if (Data.depth > 0)
            {
                int childCount = Data.parent.children.Count;
                int childIndex = 0;
                for (int i = 0; i < Data.parent.children.Count; i++)
                {
                    if (Data.parent.children[i] == Data)
                    {
                        childIndex = i;
                        break;
                    }
                }
                if (childIndex < childCount - 1)
                {
                    siblingLine.gameObject.SetActive(true);
                }
            }

            EventManager.AddListener<FlightScenarioPanelUpdatedEvent>(PanelUpdatedListener);
            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<FlightScenarioPanelUpdatedEvent>(PanelUpdatedListener);
        }

        private void OnEnable()
        {
            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
        }

        private void PanelUpdatedListener(FlightScenarioPanelUpdatedEvent e)
        {
            RefreshUI();
        }

        public override void SetText(string text)
        {
            texDraw.text = text;
        }

        public override string GetText()
        {
            return texDraw.text;
        }

        public void SetGetElementCallback(Func<TreeElement, TreeViewUIElementFlightScenario> getElementCallback)
        {
            this.getElementCallback = getElementCallback;
        }

        public void SetFlightScenarioData(FlightScenarioDataModel flightScenario)
        {
            textLink.SetContentData(flightScenario);
        }

        private void RefreshUI()
        {
            if (RectTransform == null || !gameObject.activeInHierarchy)
                return;

            StartCoroutine(RefreshUIAction());
        }

        IEnumerator RefreshUIAction()
        {
            yield return null;
            textFitter.SetLayoutVertical();
            textLayoutElement.preferredWidth = RectTransform.rect.width;

            refreshingUI = true;
            StartCoroutine(CalculateLittleSiblingUpperLineHeight());

            // set child line height
            yield return null;
            float height = RectTransform.rect.height;
            childLine.rectTransform.sizeDelta = new Vector2(childLine.rectTransform.sizeDelta.x, height);
            childLine.Points[1] = new Vector2(childLine.Points[1].x, -height);

            // set sibling line height
            siblingLine.rectTransform.sizeDelta = new Vector2(siblingLine.rectTransform.sizeDelta.x, height);
            siblingLine.Points[1] = new Vector2(siblingLine.Points[1].x, -height);

            SetDirty();
            yield return new WaitForSeconds(EXPAND_DURATION);
            yield return null;
            SetDirty();

            refreshingUI = false;
        }

        private IEnumerator CalculateLittleSiblingUpperLineHeight()
        {
            while (refreshingUI)
            {
                yield return null;
                if (Data.hasChildren)
                {
                    // set upper line height
                    // if has little sibling
                    int siblingCount = Data.parent.children.Count;
                    int siblingIndex = -1;
                    for (int i = 0; i < siblingCount; i++)
                    {
                        if (Data.parent.children[i] == Data)
                        {
                            siblingIndex = i;
                            break;
                        }
                    }
                    if (siblingCount > 1 && siblingIndex < siblingCount - 1)
                    {
                        float totalHeight = DEFAULT_UPPER_LINE_HEIGHT;

                        if (IsExpanded)
                        {
                            List<TreeElement> childs = Data.AllChilds();
                            // calculate line height

                            for (int i = 0; i < childs.Count; i++)
                            {
                                TreeViewUIElementFlightScenario descendants = getElementCallback(childs[i]);
                                if (descendants.transform.localScale.y >= 0.999f)
                                    totalHeight += descendants.RectTransform.rect.height;
                            }
                        }

                        TreeViewUIElementFlightScenario littleSibling = getElementCallback(Data.parent.children[siblingIndex + 1]);
                        littleSibling.upperLine.Points[0] = new Vector2(upperLine.Points[0].x, totalHeight);
                        littleSibling.upperLine.SetVerticesDirty();
                        littleSibling.upperLine.SetLayoutDirty();
                    }
                }
            }
        }

        private void SetDirty()
        {
            texDraw.CalculateLayoutInputVertical();
            texDraw.CalculateLayoutInputHorizontal();
            texDraw.RecalculateClipping();
            texDraw.SetTextDirty();
            childLine.SetVerticesDirty();
            childLine.SetLayoutDirty();
            siblingLine.SetVerticesDirty();
            siblingLine.SetLayoutDirty();
        }

        protected override void OnShowElement()
        {
            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
        }

        protected override void ExpandCallback(bool isExpanded)
        {
            if (Data.hasChildren)
                childLine.gameObject.SetActive(isExpanded);

            EventManager.TriggerEvent(new FlightScenarioPanelUpdatedEvent());
        }
    }
}

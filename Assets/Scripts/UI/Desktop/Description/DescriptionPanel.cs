﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using VirtualTraining.Feature;

namespace VirtualTraining.UI.Desktop
{
    public class DescriptionPanel : UIPanel
    {
        // ui reference
        [SerializeField] TEXDraw title;
        [SerializeField] TextLink textLink;
        [SerializeField] TEXDraw description;
        [SerializeField] InteractionButton pdfButton;
        [SerializeField] InteractionButton schematicButton;
        [SerializeField] InteractionButton maintenanceButton;
        [SerializeField] InteractionButton troubleshootButton;
        [SerializeField] InteractionButton removeInstallButton;
        [SerializeField] InteractionButton flightScenarioButton;
        [SerializeField] InteractionButton resetButton;
        [SerializeField] InteractionButton previousMateriButton;
        [SerializeField] InteractionButton nextMateriButton;
        [SerializeField] ScrollRect scrollRect;

        [SerializeField] InteractionButton playPauseAnimation;
        [SerializeField] InteractionButton beginAnimation;
        [SerializeField] InteractionButton lastAnimation;

        [SerializeField] GameObject animationButtonController;
        [SerializeField] GameObject animationSection;

        [SerializeField] Slider sliderAnimation;
        [SerializeField] AnimationManager animationManager;

        // cache
        private MateriTreeElement currentMateriTreeElement;
        private List<MateriTreeElement> materies = new List<MateriTreeElement>();
        bool animationIsPlaying;

        bool animationSectionActive;

        protected override void Awake()
        {
            base.Awake();

            var materiTree = DatabaseManager.GetMateriTree();
            TreeElementUtility.TreeToList(materiTree, materies);

            // remove empty materi data
            List<MateriTreeElement> toRemove = new List<MateriTreeElement>();
            for (int i = 0; i < materies.Count; i++)
            {
                if (materies[i].depth < 0)
                    toRemove.Add(materies[i]);
                else if (materies[i].isFolder)
                    toRemove.Add(materies[i]);

                if(materies[i].MateriData.deviceMode == DeviceMode.VrOnly)
                    toRemove.Add(materies[i]);
            }
            for (int i = 0; i < toRemove.Count; i++)
            {
                materies.Remove(toRemove[i]);
            }
        }

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            EventManager.AddListener<AnimationPlayEvent>(PlayAnimationListener);
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);

            pdfButton.OnClickEvent += OpenPdf;
            schematicButton.OnClickEvent += OpenSchematic;
            maintenanceButton.OnClickEvent += OpenMaintenance;
            troubleshootButton.OnClickEvent += OpenTroubleshoot;
            removeInstallButton.OnClickEvent += OpenRemoveInstall;
            flightScenarioButton.OnClickEvent += OpenFlightScenario;
            resetButton.OnClickEvent += ResetMateri;
            previousMateriButton.OnClickEvent += PrevMateri;
            nextMateriButton.OnClickEvent += NextMateri;
            playPauseAnimation.OnClickEvent += PlayPauseAnimationListener;
            beginAnimation.OnClickEvent += BeginAnimationListener;
            lastAnimation.OnClickEvent += LastAnimationListener;
            sliderAnimation.onValueChanged.AddListener(delegate { AnimationSliderChanged(); });
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            EventManager.RemoveListener<AnimationPlayEvent>(PlayAnimationListener);

            pdfButton.OnClickEvent -= OpenPdf;
            schematicButton.OnClickEvent -= OpenSchematic;
            maintenanceButton.OnClickEvent -= OpenMaintenance;
            troubleshootButton.OnClickEvent -= OpenTroubleshoot;
            resetButton.OnClickEvent -= ResetMateri;
            previousMateriButton.OnClickEvent -= PrevMateri;
            nextMateriButton.OnClickEvent -= NextMateri;
            playPauseAnimation.OnClickEvent -= PlayPauseAnimationListener;
            beginAnimation.OnClickEvent -= BeginAnimationListener;
            lastAnimation.OnClickEvent -= LastAnimationListener;
            removeInstallButton.OnClickEvent -= OpenRemoveInstall;
            flightScenarioButton.OnClickEvent -= OpenFlightScenario;

            sliderAnimation.onValueChanged.RemoveListener(delegate { AnimationSliderChanged(); });
            animationManager.SetSlider(sliderAnimation, null, null);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            title.color = theme.genericTextColor;
            description.color = theme.genericTextColor;
        }

        private void DescriptionEventListener(DescriptionPanelEvent e)
        {
            title.text = e.materiTreeElement.MateriData.name;
            description.text = e.materiTreeElement.MateriData.description;

            textLink.SetContentData(e.materiTreeElement);

            currentMateriTreeElement = e.materiTreeElement;

            if (currentMateriTreeElement.MateriData.pdfs != null)
            {
                if (currentMateriTreeElement.MateriData.pdfs.Count > 0)
                {
                    bool enablePdf = true;
                    for (int i = 0; i < currentMateriTreeElement.MateriData.pdfs.Count; i++)
                    {
                        if (currentMateriTreeElement.MateriData.pdfs[i].pdfAsset == null)
                        {
                            enablePdf = false;
                            break;
                        }
                    }
                    pdfButton.gameObject.SetActive(enablePdf);
                }
                else
                {
                    pdfButton.gameObject.SetActive(false);
                }
            }
            else
            {
                pdfButton.gameObject.SetActive(false);
            }

            maintenanceButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.Maintenance);
            troubleshootButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.Troubleshoot);
            removeInstallButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.RemoveInstall);
            flightScenarioButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.FlightScenario);

            if (currentMateriTreeElement.MateriData.schematics != null && currentMateriTreeElement.MateriData.schematics.Count > 0)
            {
                int contextMenuSchematicCount = 0;

                for (int i = 0; i < currentMateriTreeElement.MateriData.schematics.Count; i++)
                {
                    if (string.IsNullOrEmpty(currentMateriTreeElement.MateriData.schematics[i].descriptionId))
                    {
                        contextMenuSchematicCount++;
                    }
                }

                schematicButton.gameObject.SetActive(contextMenuSchematicCount > 0);
            }
            else
                schematicButton.gameObject.SetActive(false);

            scrollRect.verticalNormalizedPosition = 0f;

            ShowPanel(ScrollUp);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            DisableDescriptionPanel();
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
            animationManager = FindObjectOfType<AnimationManager>();
            animationManager.SetSlider(sliderAnimation, ResetAnimationDoneListener, DoneAnimationListener);
            animationManager.ResetAnimation();
        }

        private void ScrollUp()
        {
            StartCoroutine(ScrollUpAction());
        }

        private IEnumerator ScrollUpAction()
        {
            while (scrollRect.verticalNormalizedPosition < 1f)
            {
                yield return null;
                scrollRect.verticalNormalizedPosition = 1f;
            }

            description.gameObject.SetActive(false);
            yield return null;
            description.gameObject.SetActive(true);
        }

        private void DisableDescriptionPanel()
        {
            ClosePanel(ClearUI);
        }

        private void ClearUI()
        {
            currentMateriTreeElement = null;
            title.text = "";
            description.text = "";
        }

        private int GetCurrentMateriIndex()
        {
            for (int i = 0; i < materies.Count; i++)
            {
                if (currentMateriTreeElement.id == materies[i].id)
                    return i;
            }

            return -1;
        }

        private void OpenPdf()
        {
            if (currentMateriTreeElement.MateriData.pdfs.Count == 1)
                EventManager.TriggerEvent(new ShowPdfEvent(currentMateriTreeElement.MateriData.pdfs[0]));
            else if (currentMateriTreeElement.MateriData.pdfs.Count > 1)
            {
                ContextMenu.Load();

                for (int i = 0; i < currentMateriTreeElement.MateriData.pdfs.Count; i++)
                {
                    string menuItem = currentMateriTreeElement.MateriData.pdfs[i].pdfAsset.name;
                    PdfData pdfData = currentMateriTreeElement.MateriData.pdfs[i];
                    ContextMenu.AddItem(menuItem, delegate { EventManager.TriggerEvent(new ShowPdfEvent(pdfData)); });
                }

                ContextMenu.Show(Canvas, this);
            }
        }

        private void OpenSchematic()
        {
            List<Schematic> contextMenuSchematic = new List<Schematic>();

            for (int i = 0; i < currentMateriTreeElement.MateriData.schematics.Count; i++)
            {
                if (string.IsNullOrEmpty(currentMateriTreeElement.MateriData.schematics[i].descriptionId))
                {
                    contextMenuSchematic.Add(currentMateriTreeElement.MateriData.schematics[i]);
                }
            }


            if (contextMenuSchematic.Count == 1)
                EventManager.TriggerEvent(new ShowSchematicEvent(contextMenuSchematic[0], true));
            else if (contextMenuSchematic.Count > 1)
            {
                ContextMenu.Load();

                for (int i = 0; i < contextMenuSchematic.Count; i++)
                {
                    string menuItem = contextMenuSchematic[i].image == null ? contextMenuSchematic[i].video.name : contextMenuSchematic[i].image.name;
                    Schematic schematic = contextMenuSchematic[i];
                    ContextMenu.AddItem(menuItem, delegate { EventManager.TriggerEvent(new ShowSchematicEvent(schematic, true)); });
                }

                ContextMenu.Show(Canvas, this);
            }
        }

        private void OpenMaintenance()
        {
            EventManager.TriggerEvent(new MaintenanceUIEvent(currentMateriTreeElement.MateriData));
        }

        private void OpenTroubleshoot()
        {
            EventManager.TriggerEvent(new TroubleshootUIEvent(currentMateriTreeElement.MateriData));
        }

        private void OpenRemoveInstall()
        {
            EventManager.TriggerEvent(new RemoveInstallUIEvent(currentMateriTreeElement.MateriData));
        }

        private void OpenFlightScenario()
        {
            EventManager.TriggerEvent(new FlightScenarioUIEvent(currentMateriTreeElement.MateriData));
        }

        private void PrevMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index--;

            if (index > -1)
            {
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[index]));
            }
        }

        private void NextMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index++;

            if (index < materies.Count)
            {
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[index]));
            }
        }

        private void ResetMateri()
        {
            EventManager.TriggerEvent(new MateriEvent(currentMateriTreeElement));
        }

        void PlayAnimationListener(AnimationPlayEvent e)
        {
            if (e.animationData == null)
            {
                animationSection.SetActive(false);
                return;
            }

            if (animationSection != null)
                animationSection.SetActive(true);

            animationIsPlaying = !animationIsPlaying;
            animationIsPlaying = true;
            playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);

            if (animationIsPlaying)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);
            else
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);

            if (e.animationData.isShowAnimationUI == false)
            {
                if (animationButtonController != null)
                    animationButtonController.SetActive(false);
            }
            else
            {
                if (animationButtonController != null)
                    animationButtonController.SetActive(true);
            }
        }

        void ResetAnimationDoneListener()
        {
            if (animationSection != null)
                animationSection.SetActive(false);
            animationIsPlaying = false;
        }

        void DoneAnimationListener()
        {
            animationIsPlaying = false;
            if (playPauseAnimation != null)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);
        }

        void PlayPauseAnimationListener()
        {
            if (animationManager == null)
                return;

            animationManager.PlayAnimation();

            animationIsPlaying = !animationIsPlaying;
            if (animationIsPlaying)
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().pause);
            else
                playPauseAnimation.SetIcon(DatabaseManager.GetUITexture().play);
        }

        void BeginAnimationListener()
        {
            if (animationManager == null)
                return;

            animationManager.BeginAnimation();
        }

        void LastAnimationListener()
        {
            if (animationManager == null)
                return;

            animationManager.LastAnimation();
        }

        public void AnimationSliderChanged()
        {
            if (animationManager == null)
                return;

            animationManager.ValueChanged();
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (currentMateriTreeElement != null)
                base.ShowAllPanelListener(e);
        }

        public bool IsMateriDataExist()
        {
            return currentMateriTreeElement != null;
        }

        protected override void OnStartClosePanel()
        {
            if (animationSection != null)
            {
                animationSectionActive = animationSection.activeSelf;
            }
        }

        protected override void OnStartMinimizePanel()
        {
            if (animationSection != null)
            {
                animationSectionActive = animationSection.activeSelf;
                animationSection.SetActive(false);
            }

            previousMateriButton.transform.parent.gameObject.SetActive(false);
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            if (animationSection != null)
            {
                animationSection.SetActive(animationSectionActive);
            }

            previousMateriButton.transform.parent.gameObject.SetActive(true);
        }
    }
}

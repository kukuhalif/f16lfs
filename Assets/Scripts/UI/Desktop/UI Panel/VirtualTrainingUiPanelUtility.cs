using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace VirtualTraining.UI.Desktop
{
    public static class VirtualTrainingUiPanelUtility
    {
        private class PanelData
        {
            public Vector2 anchorMin;
            public Vector2 anchorMax;
            public Vector2 pivot;
            public Vector2 anchoredPosition;
            public Vector2 sizeDelta;

            public PanelData(Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector2 anchoredPosition, Vector2 sizeDelta)
            {
                this.anchorMin = anchorMin;
                this.anchorMax = anchorMax;
                this.pivot = pivot;
                this.anchoredPosition = anchoredPosition;
                this.sizeDelta = sizeDelta;
            }

            public PanelData(PanelData data)
            {
                this.anchorMin = data.anchorMin;
                this.anchorMax = data.anchorMax;
                this.pivot = data.pivot;
                this.anchoredPosition = data.anchoredPosition;
                this.sizeDelta = data.sizeDelta;
            }
        }

        private class PanelState
        {
            public PanelData original;
            public PanelData windowed;
            public PanelData maximized;

            public PanelState(PanelData original, PanelData windowed, PanelData maximized)
            {
                if (original != null)
                    this.original = original;
                if (windowed != null)
                    this.windowed = windowed;
                if (maximized != null)
                    this.maximized = maximized;
            }
        }

        private static Dictionary<RectTransform, PanelState> PANEL_DATAS = new Dictionary<RectTransform, PanelState>();

        public static void SetOriginalState(RectTransform panel)
        {
            PanelData data = new PanelData(panel.anchorMin, panel.anchorMax, panel.pivot, panel.anchoredPosition, panel.sizeDelta);

            if (PANEL_DATAS.ContainsKey(panel))
            {
                PANEL_DATAS[panel].original = data;
            }
            else
            {
                PANEL_DATAS.Add(panel, new PanelState(data, null, null));
            }
        }

        public static void SetOriginalSize(RectTransform panel, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector2 size)
        {
            PanelData data = new PanelData(anchorMin, anchorMax, pivot, panel.anchoredPosition, size);

            if (PANEL_DATAS.ContainsKey(panel))
            {
                PANEL_DATAS[panel].original = data;
            }
            else
            {
                PANEL_DATAS.Add(panel, new PanelState(data, null, null));
            }
        }

        public static void SavePanelState(RectTransform panel, bool windowed)
        {
            if (panel == null)
                return;

            PanelData data = new PanelData(panel.anchorMin, panel.anchorMax, panel.pivot, panel.anchoredPosition, panel.sizeDelta);

            if (PANEL_DATAS.ContainsKey(panel))
            {
                if (windowed)
                    PANEL_DATAS[panel].windowed = data;
                else
                    PANEL_DATAS[panel].maximized = data;
            }
            else
            {
                if (windowed)
                    PANEL_DATAS.Add(panel, new PanelState(null, data, null));
                else
                    PANEL_DATAS.Add(panel, new PanelState(null, null, data));
            }
        }

        public static void FullscreenPanel(RectTransform panel, bool calculateTaskbar, float duration, TweenCallback afterAction = null)
        {
            SavePanelState(panel, true);

            RectTransform reference = VirtualTrainingUiUtility.GetFullscreenPanel(calculateTaskbar);

            panel.SetPivot(reference.pivot);
            panel.SetAnchors(reference.anchorMin, reference.anchorMax);

            panel.DOAnchorPos(reference.anchoredPosition, duration);

            if (afterAction == null)
                panel.DOSizeDelta(reference.sizeDelta, duration);
            else
                panel.DOSizeDelta(reference.sizeDelta, duration).OnComplete(afterAction);
        }

        public static void RestorePanel(RectTransform panel, bool fromWindowed, float duration, TweenCallback afterAction = null)
        {
            if (!PANEL_DATAS.ContainsKey(panel))
                return;

            PanelData data = null;

            if (fromWindowed)
                data = PANEL_DATAS[panel].windowed;
            else
                data = PANEL_DATAS[panel].maximized;

            if (data == null)
                data = PANEL_DATAS[panel].original;

            panel.DOAnchorMin(data.anchorMin, duration);
            panel.DOAnchorMax(data.anchorMax, duration);
            panel.DOAnchorPos(data.anchoredPosition, duration);
            panel.DOPivot(data.pivot, duration);

            if (afterAction == null)
                panel.DOSizeDelta(data.sizeDelta, duration);
            else
                panel.DOSizeDelta(data.sizeDelta, duration).OnComplete(afterAction);
        }

        public static void MinimizePanel(RectTransform panel, bool windowed, float duration, TweenCallback afterAction = null)
        {
            SavePanelState(panel, windowed);

            panel.SetPivot(VirtualTrainingUiUtility.MinimizePanelArea.pivot);
            panel.SetAnchors(VirtualTrainingUiUtility.MinimizePanelArea.anchorMin, VirtualTrainingUiUtility.MinimizePanelArea.anchorMax);

            panel.DOAnchorPos(VirtualTrainingUiUtility.MinimizePanelArea.anchoredPosition, duration);

            if (afterAction == null)
                panel.DOSizeDelta(VirtualTrainingUiUtility.MinimizePanelArea.sizeDelta, duration);
            else
                panel.DOSizeDelta(VirtualTrainingUiUtility.MinimizePanelArea.sizeDelta, duration).OnComplete(afterAction);
        }

        public static void ResetPanel(RectTransform panel, float duration, TweenCallback afterAction = null)
        {
            if (!PANEL_DATAS.ContainsKey(panel))
                return;

            PanelData data = PANEL_DATAS[panel].original;
            PANEL_DATAS[panel].windowed = new PanelData(PANEL_DATAS[panel].original);

            panel.DOPivot(data.pivot, duration);

            panel.DOAnchorMin(data.anchorMin, duration);
            panel.DOAnchorMax(data.anchorMax, duration);
            panel.DOAnchorPos(data.anchoredPosition, duration);
            if (afterAction == null)
                panel.DOSizeDelta(data.sizeDelta, duration);
            else
                panel.DOSizeDelta(data.sizeDelta, duration).OnComplete(afterAction);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class MinimizePanelButton : InteractionButton
    {
        private const string MINIMIZE_TOOLTIP = "";

        protected override ButtonColorTheme ButtonColor => theme.uiPanelMinimizeButton;

        protected override void Awake()
        {
            SetDisableRoundedCorner();
            SetIcon(DatabaseManager.GetUITexture().minimizePanel);
            DisableSFX();
            DisableHoverCover();

            if (!string.IsNullOrEmpty(MINIMIZE_TOOLTIP))
                SetTooltip(MINIMIZE_TOOLTIP);

            base.Awake();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class ClosePanelButton : InteractionButton
    {
        private const string CLOSE_TOOLTIP = "";

        protected override ButtonColorTheme ButtonColor => theme.uiPanelCloseButton;

        protected override void Awake()
        {
            SetDisableRoundedCorner();
            SetIcon(DatabaseManager.GetUITexture().closePanel);
            DisableSFX();
            DisableHoverCover();

            if (!string.IsNullOrEmpty(CLOSE_TOOLTIP))
                SetTooltip(CLOSE_TOOLTIP);

            base.Awake();
        }
    }
}

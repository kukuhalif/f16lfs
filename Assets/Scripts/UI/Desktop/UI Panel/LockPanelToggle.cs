using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class LockPanelToggle : InteractionToggle
    {
        private const string LOCK_TOOLTIP = "";

        protected override ToggleColorTheme ToggleColor => theme.lockPaneltoggle;

        protected override void Awake()
        {
            DisableHoverCover();

            if (!string.IsNullOrEmpty(LOCK_TOOLTIP))
                SetTooltip(LOCK_TOOLTIP);

            base.Awake();
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class RestoreMaximizePanelButton : InteractionButton
    {
        private const string FULLSCREEN_TOOLTIP = "";
        private const string RESTORE_TOOLTIP = "";

        protected override ButtonColorTheme ButtonColor => theme.uiPanelRestoreButton;

        protected override void Awake()
        {
            SetDisableRoundedCorner();
            SetIcon(DatabaseManager.GetUITexture().maximizePanel);
            DisableSFX();
            DisableHoverCover();

            if (!string.IsNullOrEmpty(FULLSCREEN_TOOLTIP))
                SetTooltip(FULLSCREEN_TOOLTIP);

            base.Awake();
        }

        public void SetToRestore()
        {
            if (!string.IsNullOrEmpty(RESTORE_TOOLTIP))
                SetTooltip(RESTORE_TOOLTIP);

            SetIcon(DatabaseManager.GetUITexture().windowedPanel);
        }

        public void SetToFullScreen()
        {
            if (!string.IsNullOrEmpty(FULLSCREEN_TOOLTIP))
                SetTooltip(FULLSCREEN_TOOLTIP);

            SetIcon(DatabaseManager.GetUITexture().maximizePanel);
        }

    }
}

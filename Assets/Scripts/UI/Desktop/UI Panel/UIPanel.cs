﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;
using DG.Tweening;

namespace VirtualTraining.UI.Desktop
{
    public class UIPanel : RoundedCornerImage
    {
        private static int selectedOrder = int.MinValue;

        private delegate void PanelAction(UIPanel sender, bool theHighest);
        private static event PanelAction PanelSelection;

        private enum ResizeMode
        {
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
            Left,
            Right,
            Top,
            Bottom
        }

        // global panel setting
        private const bool DISABLE_BUTTON_ON_FULLSCREEN = false;
        private const bool FULLSCREEN_WITHOUT_TASKBAR = true;
        private const float ANIMATION_DURATION = 0.5f;
        private const int RESIZE_PADDING = 15;

        private bool isDragging;
        private bool isResizing;
        private bool isActive = true;
        private bool isMinimized = false;
        private bool isLocked;
        private bool isMaximized = false;
        private bool isLeftClickDown;
        private bool firstReset;
        private ResizeMode resizeMode;
        private Canvas canvas;
        private CanvasGroup canvasGroup;
        private Vector2 originalPivot;
        private Vector2 originalAnchorMin;
        private Vector2 originalAnchorMax;
        private Vector2 firstDragPosition;
        private Vector2 originalDragPosition;
        private Vector2 originalSizeDelta;
        private Vector2 originalLocalPointerPosition;
        private Vector2 minSize;
        private Image titleImage;
        private Image bgImage;
        private UIInteraction titleInteraction;
        private UIInteraction bodyInteraction;
        private UIInteraction bottomLeftResizeInteraction;
        private UIInteraction bottomRightResizeInteraction;
        private UIInteraction topRightResizeInteraction;
        private UIInteraction topLeftResizeInteraction;
        private UIInteraction topResizeInteraction;
        private UIInteraction leftResizeInteraction;
        private UIInteraction rightResizeInteraction;
        private UIInteraction bottomResizeInteraction;
        private TextMeshProUGUI titleText;
        private RectTransform[] bodyBorderPositions = new RectTransform[0];
        private InteractionButton restoreCornerButton;

        [SerializeField] bool alwaysOnTop = false;
        [SerializeField] bool theHighest = false;
        [SerializeField] bool hideAtStart = true;
        [SerializeField] bool setTitleColor = true;
        [SerializeField] bool setBodyColor = true;
        [SerializeField] bool resizable = false;
        [SerializeField] bool addUIInteractionComponent = true;
        [SerializeField] bool ignoreShowAllPanelEvent;
        [SerializeField] bool ignoreMinimizeAllPanelEvent;
        [SerializeField] bool ignoreCloseAllPanelEvent;
        [SerializeField] bool ignoreResetAllPanelEvent;
        [SerializeField] bool disableSFX;
        [SerializeField] bool scaleFade;
        [SerializeField] InteractionButton minimizeButton;
        [SerializeField] InteractionButton maximizeWindowedButton;
        [SerializeField] InteractionButton closeButton;
        [SerializeField] InteractionToggle lockToggle;
        [SerializeField] RectTransform titlePanel;
        [SerializeField] RectTransform bodyPanel;
        [SerializeField] RectTransform backgroundPanel;

        public Canvas Canvas { get => canvas; }
        public CanvasGroup CanvasGroup { get => canvasGroup; set => canvasGroup = value; }
        public RectTransform BodyRect { get => bodyPanel; }
        public bool IsPanelActive { get => isActive; }
        public bool IsPanelMinimized { get => isMinimized; }
        public bool IsPanelMaximized { get => isMaximized; }
        public bool IsDragging { get => isDragging; }
        public bool IsResizing { get => isResizing; }

        protected float AnimationDuration { get => ANIMATION_DURATION; }
        public static int SelectedOrder { get => selectedOrder; }

        protected override void Awake()
        {
            base.Awake();

            // get image & texture
            if (titlePanel != null && titleImage == null)
                titleImage = titlePanel.GetComponent<Image>();

            if (bodyPanel != null && bgImage == null)
                bgImage = bodyPanel.GetComponent<Image>();

            // body rect
            if (bodyPanel == null)
                bodyPanel = gameObject.GetComponent<RectTransform>();

            minSize = bodyPanel.sizeDelta;
            //------------------------------------

            // title text
            if (titlePanel != null)
                titleText = titlePanel.transform.GetComponentInFirstChild<TextMeshProUGUI>();
            //------------------------------------

            // add ui interaction component
            if (addUIInteractionComponent)
            {
                if (titlePanel != null)
                {
                    titleInteraction = titlePanel.gameObject.AddComponent<UIInteraction>();
                    titleInteraction.OnClickDown += OnTitleClickDown;
                }

                if (bodyPanel != null)
                {
                    bodyInteraction = bodyPanel.gameObject.AddComponent<UIInteraction>();
                    bodyInteraction.OnClickDown += OnBodyClickDown;
                }
            }
            //------------------------------------

            // get container canvas
            Canvas canvas = transform.GetComponentsInParentRecursive<Canvas>();
            this.canvas = canvas;
            this.canvasGroup = canvas.GetComponent<CanvasGroup>();
            if (this.canvasGroup == null)
                this.canvasGroup = canvas.gameObject.AddComponent<CanvasGroup>();
            //------------------------------------

            // set selected order for all panel
            if (selectedOrder <= canvas.sortingOrder)
                selectedOrder = canvas.sortingOrder + 1;
            //------------------------------------

            // add select panel listener for non always on top panel
            PanelSelection += SelectPanelListener;
            //------------------------------------

            // add input listener
            VirtualTrainingInputSystem.OnLeftClickDown += LeftClickDown; // for update
            VirtualTrainingInputSystem.OnEndLeftClick += EndLeftClick;
            //------------------------------------

            // add panel button control listener
            if (minimizeButton != null)
                minimizeButton.OnClickEvent += Minimize;

            if (maximizeWindowedButton != null)
                maximizeWindowedButton.OnClickEvent += WindowedOrMaximize;

            if (closeButton != null)
                closeButton.OnClickEvent += ClosePanel;

            if (lockToggle != null)
                lockToggle.OnStateChangedEvent += LockPanelToggleListener;

            // minimize corner button
            if (DISABLE_BUTTON_ON_FULLSCREEN && maximizeWindowedButton != null)
            {
                GameObject rto = (GameObject)Instantiate(Resources.Load("UI/Restore Corner"));
                restoreCornerButton = rto.GetComponent<InteractionButton>();
                rto.transform.localScale = Vector3.one;

                RectTransform retr = rto.GetComponent<RectTransform>();
                Vector2 pos = retr.anchoredPosition;
                Vector2 minAnchor = retr.anchorMin;
                Vector2 maxAnchor = retr.anchorMax;
                Vector2 pivot = retr.pivot;

                rto.transform.SetParent(bodyPanel, true);

                retr.anchoredPosition = pos;
                retr.anchorMin = minAnchor;
                retr.anchorMax = maxAnchor;
                retr.pivot = pivot;

                rto.SetActive(false);
                restoreCornerButton.OnClickEvent += WindowedOrMaximize;
            }

            //------------------------------------
        }

        protected override void Start()
        {
            base.Start();

            // set original state
            if (bodyPanel != null)
            {
                originalAnchorMin = bodyPanel.anchorMin;
                originalAnchorMax = bodyPanel.anchorMax;
                originalPivot = bodyPanel.pivot;
            }
            //------------------------------------

            // set sorting order for always on top panel
            if (alwaysOnTop)
                canvas.sortingOrder = selectedOrder + 1;
            //------------------------------------

            // hide at start
            if (hideAtStart)
                ClosePanel(null, true, true, false);
            else
                StartCoroutine(ShowPanelAction(null, true, false, false));
            //------------------------------------

            // setup resize panel
            if (resizable)
            {
                // right resize panel
                GameObject rightResizeObj = new GameObject("right resize panel", typeof(RectTransform));
                rightResizeObj.transform.SetParent(transform);
                rightResizeObj.transform.Reset();

                RectTransform rightRect = rightResizeObj.GetComponent<RectTransform>();
                rightRect.pivot = new Vector2(1, 0.5f);
                rightRect.anchorMin = new Vector2(1, 0);
                rightRect.anchorMax = new Vector2(1, 1);
                rightRect.anchoredPosition = Vector2.zero;
                rightRect.sizeDelta = new Vector2(RESIZE_PADDING, -(RESIZE_PADDING * 2));

                rightResizeInteraction = rightResizeObj.AddComponent<UIInteraction>();
                rightResizeInteraction.AddImage();
                rightResizeInteraction.OnClickDown += OnRightResizeClickDown;
                rightResizeInteraction.OnCursorEnter += OnPointerRightResize;
                rightResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // left resize panel
                GameObject leftResizeObj = new GameObject("left resize panel", typeof(RectTransform));
                leftResizeObj.transform.SetParent(transform);
                leftResizeObj.transform.Reset();

                RectTransform leftRect = leftResizeObj.GetComponent<RectTransform>();
                leftRect.pivot = new Vector2(0, 0.5f);
                leftRect.anchorMin = new Vector2(0, 0);
                leftRect.anchorMax = new Vector2(0, 1);
                leftRect.anchoredPosition = Vector2.zero;
                leftRect.sizeDelta = new Vector2(RESIZE_PADDING, -(RESIZE_PADDING * 2));

                leftResizeInteraction = leftResizeObj.AddComponent<UIInteraction>();
                leftResizeInteraction.AddImage();
                leftResizeInteraction.OnClickDown += OnLeftResizeClickDown;
                leftResizeInteraction.OnCursorEnter += OnPointerLeftResize;
                leftResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // top resize panel
                GameObject topResizeObj = new GameObject("top resize panel", typeof(RectTransform));
                topResizeObj.transform.SetParent(transform);
                topResizeObj.transform.Reset();

                RectTransform topRect = topResizeObj.GetComponent<RectTransform>();
                topRect.pivot = new Vector2(0.5f, 1);
                topRect.anchorMin = new Vector2(0, 1);
                topRect.anchorMax = new Vector2(1, 1);
                topRect.anchoredPosition = Vector2.zero;
                topRect.sizeDelta = new Vector2(-(RESIZE_PADDING * 2), RESIZE_PADDING);

                topResizeInteraction = topResizeObj.AddComponent<UIInteraction>();
                topResizeInteraction.AddImage();
                topResizeInteraction.OnClickDown += OnTopResizeClickDown;
                topResizeInteraction.OnCursorEnter += OnPointerTopResize;
                topResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // bottom resize panel
                GameObject bottomResizeObj = new GameObject("bottom resize panel", typeof(RectTransform));
                bottomResizeObj.transform.SetParent(transform);
                bottomResizeObj.transform.Reset();

                RectTransform bottomRect = bottomResizeObj.GetComponent<RectTransform>();
                bottomRect.pivot = new Vector2(0.5f, 0);
                bottomRect.anchorMin = new Vector2(0, 0);
                bottomRect.anchorMax = new Vector2(1, 0);
                bottomRect.anchoredPosition = Vector2.zero;
                bottomRect.sizeDelta = new Vector2(-(RESIZE_PADDING * 2), RESIZE_PADDING);

                bottomResizeInteraction = bottomResizeObj.AddComponent<UIInteraction>();
                bottomResizeInteraction.AddImage();
                bottomResizeInteraction.OnClickDown += OnBottomResizeClickDown;
                bottomResizeInteraction.OnCursorEnter += OnPointerBottomResize;
                bottomResizeInteraction.OnCursorExit += OnPointerResizeExit;

                // top right resize panel
                GameObject topRightResizeObj = new GameObject("top right resize panel", typeof(RectTransform));
                topRightResizeObj.transform.SetParent(transform);
                topRightResizeObj.transform.Reset();

                RectTransform topRightRect = topRightResizeObj.GetComponent<RectTransform>();
                topRightRect.pivot = new Vector2(1, 1);
                topRightRect.anchorMin = new Vector2(1, 1);
                topRightRect.anchorMax = new Vector2(1, 1);
                topRightRect.anchoredPosition = Vector2.zero;
                topRightRect.sizeDelta = new Vector2(RESIZE_PADDING, RESIZE_PADDING);

                topRightResizeInteraction = topRightResizeObj.AddComponent<UIInteraction>();
                topRightResizeInteraction.AddImage();
                topRightResizeInteraction.OnClickDown += OnTopRightResizeClickDown;
                topRightResizeInteraction.OnCursorEnter += OnPointerTopRightResizeEnter;
                topRightResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // top left resize panel
                GameObject topLeftResizeObj = new GameObject("top left resize panel", typeof(RectTransform));
                topLeftResizeObj.transform.SetParent(transform);
                topLeftResizeObj.transform.Reset();

                RectTransform topLeftRect = topLeftResizeObj.GetComponent<RectTransform>();
                topLeftRect.pivot = new Vector2(0, 1);
                topLeftRect.anchorMin = new Vector2(0, 1);
                topLeftRect.anchorMax = new Vector2(0, 1);
                topLeftRect.anchoredPosition = Vector2.zero;
                topLeftRect.sizeDelta = new Vector2(RESIZE_PADDING, RESIZE_PADDING);

                topLeftResizeInteraction = topLeftResizeObj.AddComponent<UIInteraction>();
                topLeftResizeInteraction.AddImage();
                topLeftResizeInteraction.OnClickDown += OnTopLeftResizeClickDown;
                topLeftResizeInteraction.OnCursorEnter += OnPointerTopLeftResizeEnter;
                topLeftResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // bottom left resize panel
                GameObject bottomLeftResizeObj = new GameObject("bottom left resize panel", typeof(RectTransform));
                bottomLeftResizeObj.transform.SetParent(transform);
                bottomLeftResizeObj.transform.Reset();

                RectTransform bottomLeftRect = bottomLeftResizeObj.GetComponent<RectTransform>();
                bottomLeftRect.pivot = new Vector2(0, 0);
                bottomLeftRect.anchorMin = new Vector2(0, 0);
                bottomLeftRect.anchorMax = new Vector2(0, 0);
                bottomLeftRect.anchoredPosition = Vector2.zero;
                bottomLeftRect.sizeDelta = new Vector2(RESIZE_PADDING, RESIZE_PADDING);

                bottomLeftResizeInteraction = bottomLeftResizeObj.AddComponent<UIInteraction>();
                bottomLeftResizeInteraction.AddImage();
                bottomLeftResizeInteraction.OnClickDown += OnBottomLeftResizeClickDown;
                bottomLeftResizeInteraction.OnCursorEnter += OnPointerBottomLeftResizeEnter;
                bottomLeftResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // bottom right resize panel
                GameObject bottomRightResizeObj = new GameObject("bottom right resize panel", typeof(RectTransform));
                bottomRightResizeObj.transform.SetParent(transform);
                bottomRightResizeObj.transform.Reset();

                RectTransform bottomRightRect = bottomRightResizeObj.GetComponent<RectTransform>();
                bottomRightRect.pivot = new Vector2(1, 0);
                bottomRightRect.anchorMin = new Vector2(1, 0);
                bottomRightRect.anchorMax = new Vector2(1, 0);
                bottomRightRect.anchoredPosition = Vector2.zero;
                bottomRightRect.sizeDelta = new Vector2(RESIZE_PADDING, RESIZE_PADDING);

                bottomRightResizeInteraction = bottomRightResizeObj.AddComponent<UIInteraction>();
                bottomRightResizeInteraction.AddImage();
                bottomRightResizeInteraction.OnClickDown += OnBottomRightResizeClickDown;
                bottomRightResizeInteraction.OnCursorEnter += OnPointerBottomRightResizeEnter;
                bottomRightResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------
            }
            //------------------------------------

            // event listener
            if (!ignoreShowAllPanelEvent)
                EventManager.AddListener<ShowAllUIPanelEvent>(ShowAllPanelListener);

            if (!ignoreMinimizeAllPanelEvent)
                EventManager.AddListener<MinimizeAllUIPanelEvent>(MinimizeAllPanelListener);

            if (!ignoreCloseAllPanelEvent)
                EventManager.AddListener<CloseAllUIPanelEvent>(CloseAllPanelListener);

            if (titlePanel != null && !ignoreResetAllPanelEvent)
                EventManager.AddListener<ResetAllUIPanelEvent>(ResetAllPanelListener);

            EventManager.AddListener<FullscreenPanelEvent>(FullscreenListener);

            //------------------------------------

            // set panel original state
            VirtualTrainingUiPanelUtility.SetOriginalState(bodyPanel);
            //------------------------------------

            // borders
            if (resizable || titlePanel != null)
            {
                bodyBorderPositions = GenerateCornerRect(bodyPanel);
            }
        }

        public void SetTitlePanel(RectTransform newTitle)
        {
            titlePanel = newTitle;
        }

        private void FullscreenListener(FullscreenPanelEvent e)
        {
            if (e.fullscreen && e.panel != gameObject && alwaysOnTop)
            {
                ClosePanel(null, true, false, false);
            }
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            if (setTitleColor && titlePanel != null)
            {
                if (titleImage != null)
                    titleImage.color = theme.panelTitleColor;
            }

            if (setBodyColor && bodyPanel != null)
            {
                if (bgImage != null)
                    bgImage.color = theme.panelContentColor;
            }

            if (backgroundPanel != null)
            {
                Image backgroundImage = backgroundPanel.GetComponent<Image>();

                if (backgroundImage != null)
                    backgroundImage.color = theme.backgroundColor;
            }

            if (titleText != null)
                titleText.color = theme.panelTitleTextColor;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            PanelSelection -= SelectPanelListener;

            VirtualTrainingInputSystem.OnLeftClickDown -= LeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick -= EndLeftClick;

            if (addUIInteractionComponent)
            {
                if (titleInteraction != null)
                    titleInteraction.OnClickDown -= OnTitleClickDown;

                if (bodyInteraction != null)
                    bodyInteraction.OnClickDown -= OnBodyClickDown;
            }

            if (minimizeButton != null)
                minimizeButton.OnClickEvent -= Minimize;

            if (maximizeWindowedButton != null)
                maximizeWindowedButton.OnClickEvent -= WindowedOrMaximize;

            if (closeButton != null)
                closeButton.OnClickEvent -= ClosePanel;

            if (topResizeInteraction != null)
            {
                topResizeInteraction.OnClickDown -= OnTopResizeClickDown;
                topResizeInteraction.OnCursorEnter -= OnPointerTopResize;
                topResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (bottomResizeInteraction != null)
            {
                bottomResizeInteraction.OnClickDown -= OnBottomResizeClickDown;
                bottomResizeInteraction.OnCursorEnter -= OnPointerBottomResize;
                bottomResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (leftResizeInteraction != null)
            {
                leftResizeInteraction.OnClickDown -= OnLeftResizeClickDown;
                leftResizeInteraction.OnCursorEnter -= OnPointerLeftResize;
                leftResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (rightResizeInteraction != null)
            {
                rightResizeInteraction.OnClickDown -= OnRightResizeClickDown;
                rightResizeInteraction.OnCursorEnter -= OnPointerRightResize;
                rightResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (topLeftResizeInteraction != null)
            {
                topLeftResizeInteraction.OnClickDown -= OnTopLeftResizeClickDown;
                topLeftResizeInteraction.OnCursorEnter -= OnPointerTopLeftResizeEnter;
                topLeftResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (topRightResizeInteraction != null)
            {
                topRightResizeInteraction.OnClickDown -= OnTopRightResizeClickDown;
                topRightResizeInteraction.OnCursorEnter -= OnPointerTopRightResizeEnter;
                topRightResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (bottomLeftResizeInteraction != null)
            {
                bottomLeftResizeInteraction.OnClickDown -= OnBottomLeftResizeClickDown;
                bottomLeftResizeInteraction.OnCursorEnter -= OnPointerBottomLeftResizeEnter;
                bottomLeftResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (bottomRightResizeInteraction != null)
            {
                bottomRightResizeInteraction.OnClickDown -= OnBottomRightResizeClickDown;
                bottomRightResizeInteraction.OnCursorEnter -= OnPointerBottomRightResizeEnter;
                bottomRightResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }

            if (lockToggle != null)
            {
                lockToggle.OnStateChangedEvent -= LockPanelToggleListener;
            }

            // event listener
            if (!ignoreShowAllPanelEvent)
                EventManager.RemoveListener<ShowAllUIPanelEvent>(ShowAllPanelListener);

            if (!ignoreMinimizeAllPanelEvent)
                EventManager.RemoveListener<MinimizeAllUIPanelEvent>(MinimizeAllPanelListener);

            if (!ignoreCloseAllPanelEvent)
                EventManager.RemoveListener<CloseAllUIPanelEvent>(CloseAllPanelListener);

            if (titlePanel != null && !ignoreResetAllPanelEvent)
                EventManager.RemoveListener<ResetAllUIPanelEvent>(ResetAllPanelListener);

            EventManager.RemoveListener<FullscreenPanelEvent>(FullscreenListener);

            //------------------------------------

            if (restoreCornerButton != null)
                restoreCornerButton.OnClickEvent -= WindowedOrMaximize;

            VirtualTrainingCursor.ConfineCursor(false);
            VirtualTrainingCursor.DefaultCursor();
        }

        protected virtual void OnEnablePanel(bool confinePanel)
        {
            if (bodyPanel != null)
                bodyPanel.RefreshLayout();

            if (confinePanel)
                ConfinePanel();
        }

        protected virtual void OnStartMinimizePanel()
        {

        }

        protected virtual void OnStartClosePanel()
        {

        }

        protected virtual void OnDisablePanel()
        {

        }

        protected void SetTitleText(string text)
        {
            if (titleText != null)
                titleText.text = text;
        }

        private void SelectPanelListener(UIPanel sender, bool theHighest)
        {
            if (alwaysOnTop)
            {
                if (sender == this && theHighest)
                    canvas.sortingOrder = selectedOrder + 3;
                else
                    canvas.sortingOrder = selectedOrder + 2;
            }
            else
            {
                if (sender == this)
                {
                    if (theHighest)
                        canvas.sortingOrder = selectedOrder + 1;
                    else
                        canvas.sortingOrder = selectedOrder;

                }
                else
                {
                    canvas.sortingOrder -= 1;
                }
            }
        }

        private void OnTitleClickDown()
        {
            SetOnTop();

            if (isMaximized)
                return;

            VirtualTrainingCursor.ConfineCursor(true);

            isDragging = true;

            if (bodyPanel != null)
                firstDragPosition = bodyPanel.anchoredPosition;

            if (setTitleColor && titleImage != null)
                titleImage.color = theme.panelTitleDraggedColor;

            if (titleText != null)
                titleText.color = theme.panelTitleTextDraggedColor;
        }

        private void OnBodyClickDown()
        {
            SetOnTop();
        }

        private void OnPointerLeftResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.HorizontalResize();
        }

        private void OnPointerRightResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.HorizontalResize();
        }

        private void OnPointerTopResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.VerticalResize();
        }

        private void OnPointerBottomResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.VerticalResize();
        }

        private void OnPointerTopLeftResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.TopLeftResize();
        }

        private void OnPointerTopRightResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.TopRightResize();
        }

        private void OnPointerBottomLeftResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.BottomLeftResize();
        }

        private void OnPointerBottomRightResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.BottomRightResize();
        }

        private void OnPointerResizeExit()
        {
            if (!isResizing && !isLeftClickDown)
                VirtualTrainingCursor.DefaultCursor();
        }

        private void OnRightResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Right;
                bodyPanel.SetPivot(new Vector2(0, 0.5f));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnLeftResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Left;
                bodyPanel.SetPivot(new Vector2(1, 0.5f));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnTopResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Top;
                bodyPanel.SetPivot(new Vector2(0.5f, 0));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnBottomResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Bottom;
                bodyPanel.SetPivot(new Vector2(0.5f, 1));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnTopLeftResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.TopLeft;
                bodyPanel.SetPivot(new Vector2(1, 0));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnTopRightResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.TopRight;
                bodyPanel.SetPivot(new Vector2(0, 0));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnBottomLeftResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.BottomLeft;
                bodyPanel.SetPivot(new Vector2(1, 1));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnBottomRightResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.BottomRight;
                bodyPanel.SetPivot(new Vector2(0, 1));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void LeftClickDown(Vector2 axisValue)
        {
            isLeftClickDown = true;

            if (isDragging && titlePanel != null)
            {
                bodyPanel.anchoredPosition = firstDragPosition + axisValue / canvas.scaleFactor;
                //ConfinePanel();
            }

            else if (isResizing && bodyPanel != null)
            {
                Vector2 localPointerPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out localPointerPosition);
                Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;

                Rect fullRect = VirtualTrainingUiUtility.GetFullscreenPanel(!FULLSCREEN_WITHOUT_TASKBAR).rect;
                Vector2 maxSize = new Vector2(fullRect.width, fullRect.height);
                Vector2 sizeDelta = Vector2.zero;

                switch (resizeMode)
                {
                    case ResizeMode.TopLeft:

                        sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, offsetToOriginal.y);

                        break;
                    case ResizeMode.TopRight:

                        sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, offsetToOriginal.y);

                        break;
                    case ResizeMode.BottomLeft:

                        sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, -offsetToOriginal.y);

                        break;
                    case ResizeMode.BottomRight:

                        sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, -offsetToOriginal.y);

                        break;
                    case ResizeMode.Left:

                        sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, 0);

                        break;
                    case ResizeMode.Right:

                        sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, 0);

                        break;
                    case ResizeMode.Top:

                        sizeDelta = originalSizeDelta + new Vector2(0, offsetToOriginal.y);

                        break;
                    case ResizeMode.Bottom:

                        sizeDelta = originalSizeDelta + new Vector2(0, -offsetToOriginal.y);

                        break;
                }

                sizeDelta = new Vector2(
                    Mathf.Clamp(sizeDelta.x, minSize.x, maxSize.x),
                    Mathf.Clamp(sizeDelta.y, minSize.y, maxSize.y)
                );

                bodyPanel.sizeDelta = sizeDelta;

                transform.localPosition = originalDragPosition;
            }
        }

        private void EndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isLeftClickDown = false;

            if (isResizing)
            {
                VirtualTrainingCursor.DefaultCursor();
                if (bodyPanel != null)
                    bodyPanel.SetPivot(originalPivot);

                ConfinePanel();
                VirtualTrainingUiPanelUtility.SavePanelState(bodyPanel, !isMaximized);
            }

            if (isDragging)
                ConfinePanel();

            isDragging = false;
            isResizing = false;
            VirtualTrainingCursor.ConfineCursor(false);

            if (setTitleColor && titleImage != null)
                titleImage.color = theme.panelTitleColor;

            if (titleText != null)
                titleText.color = theme.panelTitleTextColor;

            PanelSizeUpdatedCallback();
        }

        private void RestoreFromTaskbarCallback()
        {
            ShowPanel(null, true, false, true, false);
        }

        private void ConfinePanel()
        {
            if (canvas.renderMode != RenderMode.ScreenSpaceOverlay)
                return;

            if (titlePanel == null)
                return;

            if (bodyPanel != null)
            {
                bodyPanel.ConfinePanel(!FULLSCREEN_WITHOUT_TASKBAR, bodyBorderPositions, true);
            }
        }

        private void LockPanelToggleListener(bool on)
        {
            isLocked = on;
        }

        protected virtual void PanelSizeUpdatedCallback()
        {

        }

        protected virtual void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            ShowPanel();
        }

        protected virtual void MinimizeAllPanelListener(MinimizeAllUIPanelEvent e)
        {
            if (IsPanelActive)
                Minimize();
        }

        protected virtual void CloseAllPanelListener(CloseAllUIPanelEvent e)
        {
            if (IsPanelActive)
                ClosePanel();
        }

        protected virtual void ResetAllPanelListener(ResetAllUIPanelEvent e)
        {
            if (maximizeWindowedButton != null)
                ((RestoreMaximizePanelButton)maximizeWindowedButton).SetToFullScreen();

            isMaximized = false;
            VirtualTrainingUiPanelUtility.ResetPanel(bodyPanel, ANIMATION_DURATION, AfterResizingPanelCallback);
        }

        public void SetRestoredDefaultSize(Vector2 size, bool restorePanel)
        {
            if (isMaximized && restorePanel)
            {
                WindowedOrMaximize();
                StartCoroutine(SetRestoredDefaultSizeAction(size, true, restorePanel));
            }
            else
            {
                StartCoroutine(SetRestoredDefaultSizeAction(size, false, restorePanel));
            }
        }

        IEnumerator SetRestoredDefaultSizeAction(Vector2 size, bool waitAnimation, bool setPanelSize)
        {
            if (waitAnimation)
                yield return new WaitForSeconds(ANIMATION_DURATION);
            else
                yield return null;

            VirtualTrainingUiPanelUtility.SetOriginalSize(bodyPanel, originalAnchorMin, originalAnchorMax, originalPivot, size);

            if (setPanelSize)
            {
                VirtualTrainingUiPanelUtility.ResetPanel(bodyPanel, ANIMATION_DURATION, AfterResizingPanelCallback);
                yield return new WaitForSeconds(ANIMATION_DURATION);
                isMaximized = false;
                ConfinePanel();
            }
        }

        public void SetOnTop()
        {
            PanelSelection.Invoke(this, theHighest);
        }

        public void SetBodyPosition(Vector2 position)
        {
            if (bodyPanel != null)
            {
                BodyRect.anchorMin = new Vector2(0.5f, 0.5f);
                BodyRect.anchorMax = new Vector2(0.5f, 0.5f);
                BodyRect.pivot = new Vector2(0.5f, 0.5f);

                bodyPanel.anchoredPosition = position;
            }
        }

        private void ClosePanel()
        {
            ClosePanel(null, false);
        }

        public void ClosePanel(Action onClose = null, bool forceClose = false, bool ignoreAnimation = false, bool playSFX = true)
        {
            if (forceClose)
            {
                if (isLocked && lockToggle != null)
                    lockToggle.IsOn = false;

                isLocked = false;
            }

            if (!isActive || isLocked)
            {
                onClose?.Invoke();
                return;
            }

            VirtualTrainingUiPanelUtility.SavePanelState(bodyPanel, !isMaximized);
            OnStartClosePanel();

            float duration = ANIMATION_DURATION;

            if (ignoreAnimation)
                duration = 0;

            bodyPanel.SetPivot(originalPivot);
            canvasGroup.DOFade(0, duration / 2f).OnComplete(() =>
            {
                isActive = false;
                isMinimized = false;
                EventManager.TriggerEvent(new CloseUIPanelEvent(gameObject.name));

                canvas.gameObject.SetActive(false);

                OnDisablePanel();
                onClose?.Invoke();
            });

            if (scaleFade)
                bodyPanel.DOScale(0, duration);

            if (playSFX && !disableSFX)
                AudioPlayer.PlaySFX(SFX.ClosePanel);
        }

        public void ShowPanel(Action onShow = null, bool restorePosition = true, bool firstReset = true, bool animate = true, bool confinePanel = true)
        {
            if (isActive)
            {
                SetOnTop();
                onShow?.Invoke();
                return;
            }

            EventManager.TriggerEvent(new ShowUIPanelEvent(gameObject));
            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.ShowPanel);

            canvas.gameObject.SetActive(true);

            if (restorePosition)
                VirtualTrainingUiPanelUtility.RestorePanel(bodyPanel, !isMaximized, ANIMATION_DURATION);
            else
                VirtualTrainingUiPanelUtility.SavePanelState(bodyPanel, !isMaximized);

            StartCoroutine(ShowPanelAction(onShow, firstReset, animate, confinePanel));
        }

        IEnumerator ShowPanelAction(Action onShow, bool firstReset, bool animate, bool confinePanel)
        {
            yield return null;
            isActive = true;
            isMinimized = false;

            if (firstReset && !this.firstReset)
            {
                VirtualTrainingUiPanelUtility.ResetPanel(bodyPanel, ANIMATION_DURATION);
                this.firstReset = true;
            }
            bodyPanel.SetPivot(originalPivot);

            SetOnTop();
            if (animate)
            {
                if (scaleFade)
                    bodyPanel.transform.localScale = new Vector3(0, 0, 0);
                canvasGroup.DOFade(1, ANIMATION_DURATION).OnComplete(() =>
                {
                    OnEnablePanel(confinePanel);
                    onShow?.Invoke();
                    PanelSizeUpdatedCallback();
                });

                if (scaleFade)
                    bodyPanel.DOScale(1, ANIMATION_DURATION);
            }
            else
            {
                canvasGroup.alpha = 1;
                bodyPanel.transform.localScale = Vector3.one;

                OnEnablePanel(confinePanel);
                onShow?.Invoke();
            }
        }

        public void Minimize()
        {
            if (!isActive || isLocked)
                return;

            isActive = false;
            isMinimized = true;
            EventManager.TriggerEvent(new MinimizeUIPanelEvent(gameObject.name, RestoreFromTaskbarCallback));
            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.MinimizePanel);

            OnStartMinimizePanel();

            canvasGroup.DOFade(0, ANIMATION_DURATION / 2f);
            VirtualTrainingUiPanelUtility.MinimizePanel(bodyPanel, !isMaximized, ANIMATION_DURATION, () =>
            {
                canvas.gameObject.SetActive(false);

                OnDisablePanel();
            });
        }

        public void WindowedOrMaximize()
        {
            if (isMaximized) // set to windowed
            {
                VirtualTrainingUiPanelUtility.RestorePanel(bodyPanel, isMaximized, ANIMATION_DURATION, () =>
                 {
                     AfterResizingPanelCallback();
                     EventManager.TriggerEvent(new FullscreenPanelEvent(false, gameObject));
                     FullscreenCallback(false);

                     isMinimized = false;

                     ((RestoreMaximizePanelButton)maximizeWindowedButton).SetToFullScreen();
                 });

                if (!disableSFX)
                    AudioPlayer.PlaySFX(SFX.WindowedPanel);

                if (DISABLE_BUTTON_ON_FULLSCREEN)
                {
                    minimizeButton?.gameObject.SetActive(true);
                    maximizeWindowedButton?.gameObject.SetActive(true);
                    closeButton?.gameObject.SetActive(true);
                    restoreCornerButton.gameObject.SetActive(false);
                }

                EnableRoundedCorner();
            }
            else // set to maximized
            {
                VirtualTrainingUiPanelUtility.FullscreenPanel(bodyPanel, !FULLSCREEN_WITHOUT_TASKBAR, ANIMATION_DURATION, () =>
                {
                    PanelSizeUpdatedCallback();
                    EventManager.TriggerEvent(new FullscreenPanelEvent(true, gameObject));
                    FullscreenCallback(true);

                    isMinimized = false;

                    ((RestoreMaximizePanelButton)maximizeWindowedButton).SetToRestore();
                });

                if (!disableSFX)
                    AudioPlayer.PlaySFX(SFX.MaximizePanel);

                if (DISABLE_BUTTON_ON_FULLSCREEN)
                {
                    minimizeButton?.gameObject.SetActive(false);
                    maximizeWindowedButton?.gameObject.SetActive(false);
                    closeButton?.gameObject.SetActive(false);
                    restoreCornerButton.gameObject.SetActive(true);
                }

                DisableRoundedCorner();
            }

            SetOnTop();
            isMaximized = !isMaximized;
        }

        private void AfterResizingPanelCallback()
        {
            ConfinePanel();
            PanelSizeUpdatedCallback();
        }

        protected virtual void FullscreenCallback(bool fullscreen)
        {

        }
    }
}


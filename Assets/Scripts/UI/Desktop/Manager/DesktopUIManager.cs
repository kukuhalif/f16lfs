using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.UI.Desktop
{
    public class DesktopUIManager : MonoBehaviour
    {
        [SerializeField] LandingPagePanel landingPagePanel;
        [SerializeField] Taskbar taskbar;
        [SerializeField] SystemPanel systemPanel;
        [SerializeField] TreeViewUIMateri treeViewUIMateri;

        private void Start()
        {
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);

            if (VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT == null)
            {
                landingPagePanel.ShowPanel();
                return;
            }

            taskbar.Show();
            systemPanel.ShowPanel(OnSystemPanelEnabled);
        }

        private void OnSystemPanelEnabled()
        {
            treeViewUIMateri.FocusAndClickElementId(VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.id);
            VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT = null;
        }
    }
}
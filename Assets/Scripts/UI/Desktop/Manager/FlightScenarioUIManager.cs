using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.UI.Desktop
{
    public class FlightScenarioUIManager : MonoBehaviour
    {
        //[SerializeField] LandingPagePanel landingPagePanel;
        [SerializeField] Taskbar taskbar;
        //[SerializeField] SystemPanel systemPanel;
        //[SerializeField] TreeViewUIMateri treeViewUIMateri;

        private void Start()
        {
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);

            if (VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT == null)
            {
                //landingPagePanel.ShowPanel();
                return;
            }

            //taskbar.Show();
            //systemPanel.ShowPanel(OnSystemPanelEnabled);

            // show flight scenario panel
            EventManager.TriggerEvent(new FlightScenarioUIEvent(VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.MateriData));
            // show flight monitor camera panel (external view)
            EventManager.TriggerEvent(new FlightMonitorCameraEvent());
        }

        /*private void OnSystemPanelEnabled()
        {
            treeViewUIMateri.FocusAndClickElement(VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT.id);
            VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT = null;
        }*/
    }
}
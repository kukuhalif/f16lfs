using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Mobile
{
    public class MobileUIManager : MonoBehaviour
    {
        [SerializeField] GameObject[] contentPanels;
        [SerializeField] GameObject[] uiPanels;

        [SerializeField] MobileLandingPagePanel landingPage;
        [SerializeField] MobileSettingPanel settingPanel;

        List<CanvasScaler> canvasScalers = new List<CanvasScaler>();
        List<CanvasScaler> ignoreScaleCanvasScalers = new List<CanvasScaler>();

        private void Start()
        {
            EventManager.AddListener<InitializeUIEvent>(InitializeUIListener);
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void InitializeUIListener(InitializeUIEvent e)
        {
            EventManager.RemoveListener<InitializeUIEvent>(InitializeUIListener);

            StartCoroutine(EnableUI(contentPanels, true, InitUiPanel));
        }

        private void InitUiPanel()
        {
            StartCoroutine(EnableUI(uiPanels, false, () =>
            {
                StartCoroutine(InitDone());
            }));
        }

        private IEnumerator InitDone()
        {
            Debug.Log("UI initialization finished");
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            yield return null;
            EventManager.TriggerEvent(new InitializationUIDoneEvent());

            SetCanvasResolution(SettingUtility.LastResolution);
        }

        private IEnumerator EnableUI(GameObject[] toInitialize, bool disableAfter, Action onComplete)
        {
            for (int i = 0; i < toInitialize.Length; i++)
            {
                yield return null;
                toInitialize[i].SetActive(true);
                UIElement[] uiElements = toInitialize[i].transform.GetAllComponentsInChilds<UIElement>();

                for (int e = 0; e < uiElements.Length; e++)
                {
                    if (uiElements[e] != null)
                    {
                        if (uiElements[e].IgnoreDynamicUiSize)
                        {
                            var isc = uiElements[e].transform.GetComponentsInParentRecursive<CanvasScaler>();
                            if (!ignoreScaleCanvasScalers.Contains(isc))
                                ignoreScaleCanvasScalers.Add(isc);
                        }
                        else
                        {
                            var cs = uiElements[e].transform.GetComponentsInParentRecursive<CanvasScaler>();
                            if (!canvasScalers.Contains(cs))
                                canvasScalers.Add(cs);
                        }
                    }
                }

                if (disableAfter)
                {
                    yield return null;
                    toInitialize[i].SetActive(false);
                }
            }

            onComplete.Invoke();
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            SetCanvasResolution(e.resolution);
        }

        private void SetCanvasResolution(Vector2 resolution)
        {
            Debug.Log("set canvas scaler");
            for (int i = 0; i < canvasScalers.Count; i++)
            {
                if (SettingUtility.LastSetting.dynamicUiSize)
                    canvasScalers[i].referenceResolution = resolution;
                else
                    canvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
            }

            for (int i = 0; i < ignoreScaleCanvasScalers.Count; i++)
            {
                ignoreScaleCanvasScalers[i].referenceResolution = VirtualTrainingUiUtility.DefaultUiResolution;
            }
        }

        private void SceneReadyListener(SceneReadyEvent sceneReadyEvent)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);

            /* Play last materi kalo abis dari scene mode lain, tapi tar dulu aja ini
            if (VirtualTrainingSceneManager.LAST_MATERI_TREE_ELEMENT != null)
            {

            }
            else
            {

            }
            */

            Debug.Log("scene ready");

            // gaperlu landing page camera
            EventManager.TriggerEvent(new LandingPageEnabledEvent(false));

            // prepare flight sim scenario scene for playing recorded animation // by RF
            EventManager.TriggerEvent(new FlightScenarioPreparePlayEvent());

            settingPanel.Initialize();

            landingPage.gameObject.SetActive(true);
            landingPage.ShowPanel();
        }
    }
}

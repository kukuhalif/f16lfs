using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using VirtualTraining.UI.VR;

namespace VirtualTraining.UI.Desktop
{

    public class FlightScenarioRecorderUIManager : MonoBehaviour
    {
        [SerializeField] UIPanel examplePanel;

        private void Start()
        {
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);

            //examplePanel.ShowPanel();
        }
    }
}
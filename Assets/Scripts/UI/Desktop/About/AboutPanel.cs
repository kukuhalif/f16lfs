using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class AboutPanel : UIPanel
    {
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] Image textBackground;
        [SerializeField] Image aboutIcon;

        Action onClosePanelCallback;

        protected override void Start()
        {
            base.Start();

            text.text = DatabaseManager.GetProjectDetails().about;

            if (DatabaseManager.GetProjectDetails().aboutIcon != null)
            {
                aboutIcon.sprite = DatabaseManager.GetProjectDetails().aboutIcon;
                AspectRatioFitter aspectRatioFitter = aboutIcon.GetComponent<AspectRatioFitter>();
                aspectRatioFitter.aspectRatio = (float)aboutIcon.sprite.texture.width / (float)aboutIcon.sprite.texture.height;
            }
            else
                aboutIcon.gameObject.SetActive(false);

            EventManager.AddListener<ShowAboutPanelEvent>(ShowListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<ShowAboutPanelEvent>(ShowListener);
        }

        protected override void OnDisablePanel()
        {
            onClosePanelCallback?.Invoke();
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            textBackground.color = theme.landingPageBgColor;
        }

        private void ShowListener(ShowAboutPanelEvent e)
        {
            onClosePanelCallback = e.onClosePanelCallback;
            ShowPanel();
        }
    }
}

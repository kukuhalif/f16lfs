using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class ObjectListPanel : UIPanel
    {
        [SerializeField] TreeViewUIObjectList treeView;
        [SerializeField] InteractionButton resetToggleButton;
        [SerializeField] TextMeshProUGUI[] titles;

        bool firstStart = true;
        bool resetToggles = false;
        bool firstClose = true;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<ObjectInteractionShowObjectListEvent>(ObjectInteractionListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            resetToggleButton.OnClickEvent += treeView.ResetAllToggles;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<ObjectInteractionShowObjectListEvent>(ObjectInteractionListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);

            resetToggleButton.OnClickEvent -= treeView.ResetAllToggles;
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (IsPanelMinimized)
                base.ShowAllPanelListener(e);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            for (int i = 0; i < titles.Length; i++)
            {
                titles[i].color = theme.genericTextColor;
            }
        }

        protected override void PanelSizeUpdatedCallback()
        {
            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        private void PlayMateriListener(MateriEvent e)
        {
            if (IsPanelActive)
                ClosePanel();
            else
                resetToggles = true;
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            if (IsPanelActive)
                ClosePanel();
            else
                resetToggles = true;
        }

        private void ObjectInteractionListener(ObjectInteractionShowObjectListEvent e)
        {
            if (e.on)
            {
                resetToggles = true;
                EventManager.TriggerEvent(new ResetPartObjectEvent());
                ShowPanel();
            }
            else
                ClosePanel();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (resetToggles)
            {
                treeView.ResetAllToggles();
                EventManager.TriggerEvent(new ResetPartObjectEvent());
                resetToggles = false;
            }
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            EventManager.TriggerEvent(new ObjectListPanelEvent(true));

            if (firstStart)
            {
                firstStart = false;
                StartCoroutine(treeView.RefreshAtStartUiElement());
            }

            EventManager.TriggerEvent(new ObjectListPanelUpdatedEvent());
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            EventManager.TriggerEvent(new ObjectListPanelEvent(false));

            if (!IsPanelMinimized)
            {
                resetToggles = true;

                if (!firstClose)
                    EventManager.TriggerEvent(new ResetPartObjectEvent());

                firstClose = false;
            }
        }
    }
}

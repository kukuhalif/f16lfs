using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class ConfirmationPanel : UIPanel
    {
        [SerializeField] TextMeshProUGUI label;
        [SerializeField] InteractionButton yesButton;
        [SerializeField] InteractionButton noButton;

        Action onYesCallback;
        Action onNoCallback;

        protected override void Start()
        {
            base.Start();

            yesButton.OnClickEvent += OnYes;
            noButton.OnClickEvent += OnNo;

            EventManager.AddListener<ConfirmationPanelEvent>(ConfirmationEventListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            yesButton.OnClickEvent += OnYes;
            noButton.OnClickEvent += OnNo;

            EventManager.RemoveListener<ConfirmationPanelEvent>(ConfirmationEventListener);
        }

        private void OnNo()
        {
            onNoCallback?.Invoke();
            ClosePanel(OnClosePanel);
        }

        private void OnYes()
        {
            onYesCallback?.Invoke();
            ClosePanel(OnClosePanel);
        }

        private void OnClosePanel()
        {
            EventManager.TriggerEvent(new CloseConfirmationPanelEvent());
        }

        private void ConfirmationEventListener(ConfirmationPanelEvent e)
        {
            if (IsPanelActive)
            {
                ClosePanel(() =>
                {
                    Show(e);
                });
            }
            else
            {
                Show(e);
            }
        }

        private void Show(ConfirmationPanelEvent e)
        {
            label.text = e.text;

            string yesText, noText;

            if (e.onYesCallback == null && e.onNoCallback == null)
            {
                yesText = "ok";
                noText = "";
            }
            else
            {
                yesText = "yes";
                noText = "no";
            }

            yesButton.SetText(yesText);
            onYesCallback = e.onYesCallback;

            if (e.onYesCallback == null && e.onNoCallback == null)
            {
                noButton.gameObject.SetActive(false);
            }
            else
            {
                noButton.gameObject.SetActive(true);
                onNoCallback = e.onNoCallback;
                noButton.SetText(noText);
            }

            ShowPanel();
        }
    }
}

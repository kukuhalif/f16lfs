﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paroxe.PdfRenderer;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class PDFPanel : UIPanel
    {
        [SerializeField] PDFViewer pdfViewer;
        [SerializeField] Image[] contentTitle;
        [SerializeField] Image[] contentBackground;
        [SerializeField] Text[] genericText;
        [SerializeField] Text[] titleTexts;
        [SerializeField] Image sideBar;
        [SerializeField] Image footerImage;

        Sprite footerTexture;

        PDFAsset pdf;
        int page;
        Dictionary<string, PDFAsset> pdfLinkLookup = new Dictionary<string, PDFAsset>();

        protected override void Awake()
        {
            base.Awake();
            if (footerImage != null)
                footerTexture = footerImage.sprite;
        }

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<ShowPdfEvent>(ShowPdfListener);
            EventManager.AddListener<ShowPdfLinkEvent>(ShowPdfLinkListener);

            // load pdf link assets
            var pdfLinks = DatabaseManager.GetPdfLink();
            for (int i = 0; i < pdfLinks.Count; i++)
            {
                pdfLinkLookup.Add(pdfLinks[i].name, pdfLinks[i]);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<ShowPdfEvent>(ShowPdfListener);
            EventManager.RemoveListener<ShowPdfLinkEvent>(ShowPdfLinkListener);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            pdf = null;
            ClosePanel();
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            for (int i = 0; i < titleTexts.Length; i++)
            {
                titleTexts[i].color = theme.panelTitleTextColor;
            }

            for (int i = 0; i < genericText.Length; i++)
            {
                genericText[i].color = theme.genericTextColor;
            }

            for (int i = 0; i < contentTitle.Length; i++)
            {
                contentTitle[i].color = theme.panelTitleColor;
            }

            for (int i = 0; i < contentBackground.Length; i++)
            {
                contentBackground[i].color = theme.panelContentColor;
            }

            if (sideBar != null)
                sideBar.color = theme.panelContentColor;
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            EventManager.TriggerEvent(new OnClosedPdfPanelEvent());
        }

        private void ShowPdfListener(ShowPdfEvent e)
        {
            if (e.pdfData != null) // from materi (with page data)
            {
                pdf = e.pdfData.pdfAsset;
                page = e.pdfData.page;

                if (page > 0)
                    page--;
                else
                    page = 0;

                SetTitleText(e.pdfData.pdfAsset.name);
                ShowPanel();
            }
            else if (e.pdfAsset != null) // from manual (pdf asset only)
            {
                pdf = e.pdfAsset;
                page = 0;

                SetTitleText(e.pdfAsset.name);
                ShowPanel();
            }
            else
            {
                pdf = null;
                ClosePanel();
            }
        }

        private void ShowPdfLinkListener(ShowPdfLinkEvent e)
        {
            if (pdfLinkLookup.ContainsKey(e.id))
            {
                pdf = pdfLinkLookup[e.id];
                page = 0;
                SetTitleText(pdf.name);

                ShowPanel();
            }
            else
            {
                Debug.LogError("pdf link not found, check materi config");
            }
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            pdfViewer.LoadDocumentFromAsset(pdf, page);
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (pdf != null)
                base.ShowAllPanelListener(e);
        }

        protected override void PanelSizeUpdatedCallback()
        {
            if (footerImage != null)
            {
                if (IsPanelMaximized)
                    footerImage.sprite = null;
                else
                    footerImage.sprite = footerTexture;
            }
        }
    }
}


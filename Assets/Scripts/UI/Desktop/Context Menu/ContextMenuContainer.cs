using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.UI.Desktop
{
    public class ContextMenuContainer : UIElement
    {
        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] ContextMenuItem template;
        [SerializeField] VerticalLayoutGroup verticalLayout;
        [SerializeField] RectTransform contextRect;
        [SerializeField] RectTransform[] corners;

        const float FADE_DURATION = 0.5f;

        public float originalHeight;

        List<ContextMenuItem> items = new List<ContextMenuItem>();

        protected override void Awake()
        {
            base.Awake();

            originalHeight = contextRect.sizeDelta.y;
        }

        protected override void ApplyTheme()
        {
            Image bg = GetComponentInChildren<Image>();
            bg.color = theme.panelContentColor;
        }

        public void AddMenuItem(string name, Action pressedCallback)
        {
            ContextMenuItem newItem = Instantiate(template);

            newItem.gameObject.SetActive(true);

            newItem.Setup(name, () =>
            {
                pressedCallback.Invoke();
                Destroy(gameObject);
            });

            newItem.transform.SetParent(template.gameObject.transform.parent);
            newItem.transform.Reset();

            items.Add(newItem);
        }

        public void Show(Canvas owner, UIPanel panel)
        {
            canvasGroup.alpha = 0f;
            transform.SetParent(owner.transform);
            transform.SetAsLastSibling();

            VirtualTrainingInputSystem.OnStartLeftClick += StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick += StartClick;
            VirtualTrainingInputSystem.OnStartRightClick += StartClick;

            transform.localScale = Vector3.one;
            panel.SetOnTop();
            StartCoroutine(RefreshLayout());
        }

        IEnumerator RefreshLayout()
        {
            verticalLayout.enabled = false;
            yield return null;
            verticalLayout.enabled = true;

            float height = 0;
            for (int i = 0; i < items.Count; i++)
            {
                height += items[i].Getheight();
            }

            if (height < originalHeight)
                contextRect.sizeDelta = new Vector2(contextRect.sizeDelta.x, height);
            else
                contextRect.sizeDelta = new Vector2(contextRect.sizeDelta.x, originalHeight);

            verticalLayout.enabled = false;
            yield return null;
            verticalLayout.enabled = true;

            contextRect.anchorMin = new Vector2(1, 1);
            contextRect.anchorMax = new Vector2(1, 1);
            contextRect.pivot = new Vector2(1, 1);

            contextRect.position = VirtualTrainingInputSystem.PointerPosition;

            canvasGroup.DOFade(1f, FADE_DURATION);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            VirtualTrainingInputSystem.OnStartLeftClick -= StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick -= StartClick;
            VirtualTrainingInputSystem.OnStartRightClick -= StartClick;
        }

        private void Update()
        {
            contextRect.ConfinePanel(false, corners, false);
        }

        private void StartClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (!contextRect.RectContains(pointerPosition))
                Destroy(gameObject);
        }

        public void Clear()
        {
            for (int i = 0; i < items.Count; i++)
            {
                Destroy(items[i].gameObject);
            }

            items.Clear();
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI.Desktop
{
    public class ContextMenuItem : MonoBehaviour
    {
        Action pressedCallback;

        [SerializeField] InteractionButton button;

        private void Start()
        {
            button.OnClickEvent += OnClick;
        }

        private void OnDestroy()
        {
            button.OnClickEvent -= OnClick;
        }

        private void OnClick()
        {
            pressedCallback.Invoke();
        }

        public void Setup(string text, Action pressedCallback)
        {
            button.SetText(text);
            this.pressedCallback = pressedCallback;
        }

        public float Getheight()
        {
            return GetComponent<RectTransform>().sizeDelta.y;
        }
    }
}

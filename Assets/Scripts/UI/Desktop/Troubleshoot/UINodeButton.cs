﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;
using Valve.VR;

namespace VirtualTraining.UI.Desktop
{
    public class UINodeButton : MonoBehaviour
    {
        TroubleshootGenerator generateUI;
        TroubleshootNode troubleshootNode;
        bool isInstantiated;
        InteractionButton button;

        [SerializeField] TextLink textLink;
        [SerializeField] List<UILineRenderer> startLine = new List<UILineRenderer>();
        [SerializeField] List<UILineRenderer> endLine = new List<UILineRenderer>();
        [SerializeField] List<UINodeButton> outputNodeButtons = new List<UINodeButton>();

        public GameObject textAddOn;
        public Vector2 textOffsets;
        public Vector2 collapsPosition;

        bool isExpanded;

        [SerializeField] float defaultMaxTextHeight;
        [SerializeField] float collapsedTextHeight;
        [SerializeField] TEXDraw textTitle;
        [SerializeField] VerticalLayoutGroup verticalLayout;
        [SerializeField] ContentSizeFitter textContentFitter;
        [SerializeField] GameObject seeMoreObject;

        [SerializeField] SteamVR_Action_Boolean expand;
        bool hovering;

        public int TroubleshootNodeId { get => troubleshootNode.id; }

        private void Start()
        {
            button = GetComponent<InteractionButton>();
            button.OnClickEvent += ClickButton;
            List<UILineRenderer> endLine = new List<UILineRenderer>();
            button.OnPointerEnterEvent += Button_OnPointerEnterEvent;
            button.OnPointerExitEvent += Button_OnPointerExitEvent;
        }

        private void OnEnable()
        {
            StartCoroutine(CheckHeight());
        }

        private void OnDestroy()
        {
            button.OnClickEvent -= ClickButton;
            button.OnPointerEnterEvent -= Button_OnPointerEnterEvent;
            button.OnPointerExitEvent += Button_OnPointerExitEvent;
        }

        private void Button_OnPointerEnterEvent()
        {
            hovering = true;
        }

        private void Button_OnPointerExitEvent()
        {
            hovering = false;
        }

        private void Update()
        {
            if (!hovering)
                return;

            if (expand.stateUp)
            {
                ExpandSwitch();
            }
        }

        public void Setup(string text, TroubleshootGenerator generator, TroubleshootNode node)
        {
            textTitle.text = text;
            generateUI = generator;
            troubleshootNode = node;
            generateUI.uINodeButtons.Add(this);
            textLink.SetContentData(node.content.troubleshootData);
        }

        public void AddedStartLineRenderer(UILineRenderer uILineRenderer)
        {
            startLine.Add(uILineRenderer);
        }
        public void AddedEndLineRenderer(UILineRenderer uILineRenderer)
        {
            endLine.Add(uILineRenderer);
        }
        public void AddedOutputNode(UINodeButton uiNodeButton)
        {
            outputNodeButtons.Add(uiNodeButton);
        }

        IEnumerator CheckHeight()
        {
            yield return null;

            // check current height
            float height = textTitle.rectTransform.rect.height;
            seeMoreObject.SetActive(height > defaultMaxTextHeight);

            yield return null;

            textContentFitter.enabled = false;
            textTitle.rectTransform.sizeDelta = new Vector2(textTitle.rectTransform.sizeDelta.x, collapsedTextHeight);

            yield return new WaitForSeconds(0.1f);
            RectTransform rectTemps = this.GetComponent<RectTransform>();
            collapsPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - rectTemps.sizeDelta.y / 2);
        }

        public void ClickButton()
        {
            if (gameObject.activeInHierarchy)
            {
                if (troubleshootNode != null && isInstantiated == false)
                {
                    isInstantiated = true;
                    generateUI.GenerateButton(troubleshootNode, this);
                }
                generateUI.currentButtonActive = this.GetComponent<RectTransform>();

                EventManager.TriggerEvent(new TroubleshootPlayEvent(troubleshootNode));
                for (int i = 0; i < generateUI.uINodeButtons.Count; i++)
                {
                    generateUI.uINodeButtons[i].ResetButton();
                }
                button.ForcePressedColor(true);

                StartCoroutine(RefreshText());
            }
            else
                EventManager.TriggerEvent(new TroubleshootPlayEvent(troubleshootNode));
        }

        public void ResetButton()
        {
            button.ForcePressedColor(false);
        }

        public string ExpandSwitch()
        {
            if (isExpanded)
            {
                textContentFitter.enabled = false;
                textTitle.rectTransform.sizeDelta = new Vector2(textTitle.rectTransform.sizeDelta.x, collapsedTextHeight);
                AudioPlayer.PlaySFX(SFX.ExpandTreeElement);
                StartCoroutine(CollapseContainer());
            }
            else
            {
                textContentFitter.enabled = true;
                AudioPlayer.PlaySFX(SFX.CollapseTreeElement);
                StartCoroutine(ExpandContainer());
            }

            isExpanded = !isExpanded;

            StartCoroutine(RefreshText());

            return isExpanded ? @"\link[c]{collapse}" : @"\link[c]{show more ...}";
        }

        IEnumerator RefreshText()
        {
            yield return null;
            verticalLayout.CalculateLayoutInputVertical();
            if (isExpanded)
                transform.SetAsLastSibling();
        }

        IEnumerator ExpandContainer()
        {
            yield return new WaitForSeconds(0.1f);
            Vector2 contentSizeTemp = new Vector2();
            RectTransform rectTemp = this.GetComponent<RectTransform>();

            contentSizeTemp = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - rectTemp.sizeDelta.y);
            Debug.Log(contentSizeTemp);
            Vector2 positionTemp = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - +rectTemp.sizeDelta.y / 2);
            generateUI.SizingContent(contentSizeTemp);

            UpdateStartLinePosition(positionTemp);
            UpdateEndLinePosition(positionTemp);

            if (outputNodeButtons.Count > 0)
            {
                for (int i = 0; i < outputNodeButtons.Count; i++)
                {
                    outputNodeButtons[i].TextPositionUpdate();
                }
            }
            TextPositionUpdate();
        }

        IEnumerator CollapseContainer()
        {
            yield return new WaitForSeconds(0.1f);
            Vector2 contentSizeTemp = new Vector2();
            RectTransform rectTemp = this.GetComponent<RectTransform>();

            contentSizeTemp = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y + rectTemp.sizeDelta.y);
            Debug.Log(contentSizeTemp);
            //generateUI.DecreaseSizeContent(contentSizeTemp);
            Vector2 positionTemp = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - +rectTemp.sizeDelta.y / 2);
            UpdateStartLinePosition(positionTemp);
            UpdateEndLinePosition(positionTemp);

            if (outputNodeButtons.Count > 0)
            {
                for (int i = 0; i < outputNodeButtons.Count; i++)
                {
                    outputNodeButtons[i].TextPositionUpdate();
                }
            }
            TextPositionUpdate();
        }

        void UpdateStartLinePosition(Vector2 startPosition)
        {
            if (startLine.Count <= 0)
                return;

            for (int i = 0; i < startLine.Count; i++)
            {
                if (startLine[i] != null)
                {
                    if (startLine[i].Points.Length > 0)
                    {
                        startLine[i].Points[0] = startPosition;
                        startLine[i].SetAllDirty();
                    }
                }
            }
        }

        void UpdateEndLinePosition(Vector2 endPosition)
        {
            if (endLine.Count <= 0)
                return;

            for (int i = 0; i < endLine.Count; i++)
            {
                if (endLine[i] != null)
                {
                    if (endLine[i].Points.Length > 0)
                    {
                        endLine[i].Points[endLine[i].Points.Length - 1] = endPosition;
                        endLine[i].SetAllDirty();
                    }
                }

            }
        }

        public void TextPositionUpdate()
        {
            if (generateUI != null && textAddOn != null && endLine.Count > 0)
            {
                for (int i = 0; i < endLine.Count; i++)
                {
                    if (endLine[i].Points[endLine[i].Points.Length - 1].x != endLine[i].Points[endLine[i].Points.Length - 2].x)
                        generateUI.SetupTextPosition(textAddOn, endLine[i], textOffsets, this, true);
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;
using System.Linq;
using TMPro;


namespace VirtualTraining.UI.Desktop
{
    public partial class TroubleshootGenerator : UIElement
    {
        public TroubleshootData troubleshootData;
        [SerializeField] Transform canvasParent;
        [SerializeField] Transform contentTransform;
        public float positionStart = 0.8f;
        [SerializeField] InteractionButton _buttonNode;
        TroubleshootNode _nodeRoot;
        [SerializeField] GameObject uiLineRenderer;
        public List<Vector2> points = new List<Vector2>();
        [SerializeField] Vector2 rootPosition;
        [SerializeField] Vector2 textOffset;
        Vector2 diffPosition;
        public string outputNodeName = "outputEmpty";
        public string inputNodeName = "inputEmpty";
        List<Vector2> positionStarts = new List<Vector2>();
        public float offsideX;
        public float offsideY;
        public GameObject addOnText;
        public Vector2 offsideInputNode;
        public Vector2 offsideOutputNode;
        public List<UINodeButton> uINodeButtons = new List<UINodeButton>();
        public RectTransform currentButtonActive;

        bool initialized = false;

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
        }

        protected override void Start()
        {
            base.Start();

            if (!initialized)
                Initialize();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            StartCoroutine(FocusMiddle());
        }

        public void Initialize()
        {
            EventManager.AddListener<TriggerTroubleshootInsidePanelEvent>(TriggerTroubleshootListener);

            RectTransform rect = contentTransform.gameObject.GetComponent<RectTransform>();
            Vector2 positionUp = new Vector2(0, (rect.sizeDelta.y / 2) * positionStart); // up
            positionStarts.Add(positionUp);
            Vector2 positionDown = new Vector2(0, (rect.sizeDelta.y / 2) * -positionStart); // down
            positionStarts.Add(positionDown);
            Vector2 positionLeft = new Vector2((rect.sizeDelta.x / 2 * -positionStart), 0); // left
            positionStarts.Add(positionLeft);
            Vector2 positionRight = new Vector2((rect.sizeDelta.x / 2 * positionStart), 0); // right
            positionStarts.Add(positionRight);
            rootPosition = positionStarts[(int)troubleshootData.startPositionGraph];

            GenerateRoot();

            initialized = true;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<TriggerTroubleshootInsidePanelEvent>(TriggerTroubleshootListener);
        }

        private void TriggerTroubleshootListener(TriggerTroubleshootInsidePanelEvent e)
        {
            if (!initialized)
                Initialize();

            bool found = false;
            foreach (var button in uINodeButtons)
            {
                if (button.TroubleshootNodeId == e.troubleshootNode.id)
                {
                    button.ClickButton();
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                foreach (var node in troubleshootData.troubleshootGraph.nodes)
                {
                    TroubleshootNode troubleshootNode = node as TroubleshootNode;
                    if (e.troubleshootNode.id == troubleshootNode.id)
                        EventManager.TriggerEvent(new TroubleshootPlayEvent(troubleshootNode));
                }
            }
        }

        IEnumerator FocusMiddle()
        {
            RectTransform rt = contentTransform.GetComponent<RectTransform>();
            Vector2 start = rt.anchoredPosition;
            Vector2 end = Vector2.zero;
            float t = 0;

            while (rt.anchoredPosition != end)
            {
                rt.anchoredPosition = Vector2.Lerp(start, end, t);
                t += Time.deltaTime * 3f;
                yield return null;
            }

            Debug.Log("troubleshoot focused");
        }

        public void GenerateRoot()
        {
            if (troubleshootData.troubleshootGraph != null)
            {
                for (int i = 0; i < troubleshootData.troubleshootGraph.nodes.Count; i++)
                {
                    if ((troubleshootData.troubleshootGraph.nodes[i].GetInputPort(inputNodeName).ConnectionCount == 0 && troubleshootData.troubleshootGraph.nodes[i].GetOutputPort(outputNodeName).ConnectionCount > 0) || troubleshootData.troubleshootGraph.nodes.Count == 1)
                    {
                        _nodeRoot = troubleshootData.troubleshootGraph.nodes[i] as TroubleshootNode;
                        //diffPosition = canvasParent.transform.TransformPoint(rootPosition) - canvasParent.transform.TransformPoint(ReverseYaxis(_nodeRoot.position));
                        Vector3 newVector = new Vector3(rootPosition.x, rootPosition.y, 0);
                        diffPosition = rootPosition - ReverseYaxis(_nodeRoot.position);
                        Debug.Log(rootPosition);
                        Debug.Log(canvasParent.transform.InverseTransformPoint(rootPosition));
                    }
                }
            }

            if (_nodeRoot != null)
            {
                InstantiateButton(_nodeRoot, _nodeRoot, true);
            }
        }

        public void GenerateButton(TroubleshootNode node, UINodeButton uiNodeButton)
        {
            if (troubleshootData.isSimplifiedMode)
                StartCoroutine(GenerateSimplifiedButton(node, uiNodeButton));
            else
                StartCoroutine(GenerateButtonBase(node, uiNodeButton));
        }

        IEnumerator GenerateButtonBase(TroubleshootNode node, UINodeButton uiNodeButton)
        {
            for (int i = 0; i < node.GetOutputPort(outputNodeName).ConnectionCount; i++)
            {
                yield return null;
                yield return null;
                yield return null;
                points.Clear();
                GameObject line = Instantiate(uiLineRenderer, canvasParent, false);
                line.transform.position = canvasParent.transform.position;
                line.transform.SetAsFirstSibling();
                UILineRenderer lineRenderer = line.GetComponent<UILineRenderer>();

                RectTransform rects = uiNodeButton.gameObject.GetComponent<RectTransform>();
                Vector2 setCenterNode = new Vector2(0, -rects.sizeDelta.y / 2);
                uiNodeButton.AddedStartLineRenderer(lineRenderer);

                points.Add(ReverseYaxis(node.position) + offsideOutputNode + setCenterNode);
                var noda = node.GetOutputPort(outputNodeName).GetConnection(i).node;
                TroubleshootNode nodeCurr = noda as TroubleshootNode;
                InstantiateButton(node, nodeCurr, false, uiNodeButton, lineRenderer);
            }
        }

        public void InstantiateButton(TroubleshootNode inputNode, TroubleshootNode node, bool isRoot, UINodeButton inputNodeButton = null, UILineRenderer lineRenderer = null)
        {
            InteractionButton buttonTemp;
            if (node.content.troubleshootData.prefabCustomButton != null)
            {
                buttonTemp = Instantiate(node.content.troubleshootData.prefabCustomButton.GetComponent<InteractionButton>(), canvasParent, false);
            }
            else
                buttonTemp = Instantiate(_buttonNode, canvasParent, false);

            buttonTemp.gameObject.SetActive(true);

            if (isRoot)
            {
                buttonTemp.transform.localPosition = rootPosition;
            }
            else
            {
                Vector2 positionTemp = SnappingButton(node.position, inputNode, node);
                node.position = positionTemp;
                buttonTemp.transform.position = canvasParent.transform.TransformPoint(ReverseYaxis(positionTemp));
            }

            buttonTemp.transform.SetAsLastSibling();
            UINodeButton uiNode = buttonTemp.GetComponent<UINodeButton>();
            if (inputNodeButton != null)
                inputNodeButton.AddedOutputNode(uiNode);
            if (lineRenderer != null)
                uiNode.AddedEndLineRenderer(lineRenderer);

            string text;

            if (!string.IsNullOrEmpty(node.content.troubleshootData.description))
                text = node.content.troubleshootData.description;
            else
                text = node.content.troubleshootData.title;

            uiNode.Setup(text, this, node);

            SizingContent(buttonTemp.transform.localPosition);
            if (inputNodeButton != null)
                StartCoroutine(DelayLineInstantiate(uiNode, node, inputNode, inputNodeButton, lineRenderer));
        }

        IEnumerator DelayLineInstantiate(UINodeButton uiNodeButton, TroubleshootNode outputnode, TroubleshootNode inputNode, UINodeButton startUINode, UILineRenderer lineRenderer = null)
        {
            //yield return new WaitForSeconds(1f);
            yield return null;
            yield return null;
            yield return null;
            if (lineRenderer != null)
            {
                RectTransform rectTrans = uiNodeButton.gameObject.GetComponent<RectTransform>();
                Vector2 centerUInodePosition = new Vector2(0, -rectTrans.sizeDelta.y / 2);
                for (int i = 0; i < inputNode.GetOutputPort(outputNodeName).ConnectionCount; i++)
                {
                    if (inputNode.GetOutputPort(outputNodeName).GetReroutePoints(i).Count > 0)
                    {
                        for (int j = 0; j < inputNode.GetOutputPort(outputNodeName).GetReroutePoints(i).Count; j++)
                        {
                            Vector2 lineVector = ReverseYaxis(inputNode.GetOutputPort(outputNodeName).GetReroutePoints(i)[j]);
                            lineVector = SnappingPoint(lineVector, ReverseYaxis(inputNode.position), ReverseYaxis(inputNode.GetOutputPort(outputNodeName).GetConnection(i).node.position));
                            lineVector = lineVector + offsideInputNode + centerUInodePosition;
                            points.Add(lineVector);
                            SizingContent(lineVector);
                        }
                    }
                }

                Vector2 parentNode = outputnode.position;
                parentNode = SnappingButton(parentNode, inputNode, outputnode);
                points.Add(ReverseYaxis(parentNode) + centerUInodePosition);
                RefreshLine(lineRenderer);
                StartCoroutine(InstantiateText(outputnode, inputNode, lineRenderer, uiNodeButton, startUINode));
            }
        }

        private void RefreshLine(UILineRenderer lineRenderer)
        {
            lineRenderer.Points = points.ToArray();
            lineRenderer.SetAllDirty();
        }

        public Vector2 ReverseYaxis(Vector2 vector)
        {
            vector = new Vector2(vector.x * troubleshootData.sizeMultiplierX, (vector.y * -1) * troubleshootData.sizeMultiplierY);
            Vector2 newVector = vector + diffPosition;

            return newVector;
        }
        public void SizingContent(Vector2 position)
        {
            RectTransform rect = contentTransform.gameObject.GetComponent<RectTransform>();
            float posx = position.x;
            float posy = position.y;

            if (posx < 0)
                posx = posx * -1;

            if (position.x < 0)
                posx = posx - canvasParent.localPosition.x;
            else
                posx = posx + canvasParent.localPosition.x;

            if ((rect.sizeDelta.x / 2 - offsideX) < posx)
            {
                float deltaPos = posx - (rect.sizeDelta.x / 2 - offsideX);
                rect.sizeDelta = new Vector2(rect.sizeDelta.x + deltaPos, rect.sizeDelta.y);
                if (position.x > 0)
                    deltaPos = deltaPos * -1;
                canvasParent.localPosition = new Vector2(canvasParent.localPosition.x + (deltaPos / 2), canvasParent.localPosition.y);
                rect.transform.localPosition = new Vector2((rect.transform.localPosition.x - (deltaPos / 2)), rect.transform.localPosition.y);
            }


            if (posy < 0)
                posy = posy * -1;

            if (position.y < 0)
                posy = posy - canvasParent.localPosition.y;
            else
                posy = posy + canvasParent.localPosition.y;

            if ((rect.sizeDelta.y / 2 - offsideY) < posy)
            {
                float deltaPos = posy - (rect.sizeDelta.y / 2 - offsideY);
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y + deltaPos);
                if (position.y > 0)
                    deltaPos = deltaPos * -1;
                canvasParent.localPosition = new Vector2(canvasParent.localPosition.x, canvasParent.localPosition.y + (deltaPos / 2));
                rect.transform.localPosition = new Vector2(rect.transform.localPosition.x, rect.transform.localPosition.y - (deltaPos / 2));
            }
        }

        public void DecreaseSizeContent(Vector2 position)
        {
            RectTransform rect = contentTransform.gameObject.GetComponent<RectTransform>();
            float posx = position.x;
            float posy = position.y;
            if (posy < 0)
                posy = posy * -1;

            if (position.y < 0)
                posy = posy - canvasParent.localPosition.y;
            else
                posy = posy + canvasParent.localPosition.y;

            if ((rect.sizeDelta.y / 2 - offsideY) > posy)
            {
                float deltaPos = posy - (rect.sizeDelta.y / 2 - offsideY);
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y + deltaPos);
                if (position.y > 0)
                    deltaPos = deltaPos * -1;
                canvasParent.localPosition = new Vector2(canvasParent.localPosition.x, canvasParent.localPosition.y - (deltaPos / 2));
                rect.transform.localPosition = new Vector2(rect.transform.localPosition.x, rect.transform.localPosition.y + (deltaPos / 2));
            }
        }

        IEnumerator InstantiateText(TroubleshootNode outputNode, TroubleshootNode inputNode, UILineRenderer lineRenderer, UINodeButton uiNodeButton, UINodeButton startUINode)
        {
            if (!string.IsNullOrEmpty(outputNode.content.troubleshootData.text) && lineRenderer.Points.Length > 1)
            {
                textOffset = new Vector2(outputNode.content.troubleshootData.offsetX, outputNode.content.troubleshootData.offsetY);
                GameObject textContainer = Instantiate(addOnText, canvasParent, false);
                TextMeshProUGUI TextAddOn = textContainer.GetComponentInChildren<TextMeshProUGUI>();
                TextAddOn.text = outputNode.content.troubleshootData.text;
                textContainer.gameObject.SetActive(true);

                startUINode.transform.SetAsLastSibling();
                SetupTextPosition(textContainer, lineRenderer, textOffset, startUINode);
                uiNodeButton.textAddOn = textContainer;
                uiNodeButton.textOffsets = textOffset;
                yield return null;
                textContainer.gameObject.SetActive(false);
                yield return null;
                textContainer.gameObject.SetActive(true);
            };
        }

        public void SetupTextPosition(GameObject textContainer, UILineRenderer lineRenderer, Vector2 textOffsets, UINodeButton uiNodeButton, bool isInstantiate = false)
        {
            if (lineRenderer.Points.Length == 2)
            {
                //Vector2 meanPosition = (outputNode.position + inputNode.position) / 2;
                Vector2 meanPosition;
                if (isInstantiate)
                {
                    Debug.Log(isInstantiate);
                    meanPosition = (lineRenderer.Points[0] + lineRenderer.Points[lineRenderer.Points.Length - 1]) / 2;
                }
                else
                    meanPosition = (uiNodeButton.collapsPosition + lineRenderer.Points[lineRenderer.Points.Length - 1]) / 2;

                TextPositioning(textContainer, meanPosition, textOffsets);
            }
            else if (lineRenderer.Points.Length == 3)
            {
                TextPositioning(textContainer, lineRenderer.Points[1], textOffsets);
            }
            else if (lineRenderer.Points.Length % 2 != 0)
            {
                int x = (lineRenderer.Points.Length - 1) / 2;
                TextPositioning(textContainer, lineRenderer.Points[x], textOffsets);
                Debug.Log("ganjil");
            }
            else
            {
                int x = lineRenderer.Points.Length / 2;
                int y = x - 1;
                Debug.Log(x + " " + y);
                Vector2 meanPosition = (lineRenderer.Points[x] + lineRenderer.Points[y]) / 2;
                TextPositioning(textContainer, meanPosition, textOffsets);
                Debug.Log("Genap");
            }
        }

        void TextPositioning(GameObject TextAddOn, Vector2 reroutePoints, Vector2 textOffset)
        {
            TextAddOn.transform.localPosition = reroutePoints + textOffset;
        }

    }
}


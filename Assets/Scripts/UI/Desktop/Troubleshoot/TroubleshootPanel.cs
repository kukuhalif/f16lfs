﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI.Desktop
{
    public class TroubleshootPanel : UIPanel
    {
        [SerializeField] GameObject prefabCanvastTroubleshoot;

        GameObject currentCanvasT;
        bool isTroubleshootDataExist = false;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<TroubleshootUIEvent>(TroubleshootListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<TroubleshootUIEvent>(TroubleshootListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClosePanel();
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (isTroubleshootDataExist)
                base.ShowAllPanelListener(e);
        }

        private void PlayMateriListener(MateriEvent e)
        {
            ClosePanel();
        }

        void TroubleshootListener(TroubleshootUIEvent e)
        {
            if (e.materi != null)
            {
                if (e.materi.troubleshootData.troubleshootGraph != null && currentCanvasT == null)
                {
                    currentCanvasT = Instantiate(prefabCanvastTroubleshoot, transform);
                    currentCanvasT.transform.SetSiblingIndex(1);
                    var tg = currentCanvasT.GetComponent<TroubleshootGenerator>();
                    tg.troubleshootData = e.materi.troubleshootData;
                    tg.Initialize();
                    currentCanvasT.SetActive(true);

                    isTroubleshootDataExist = true;
                }

                if (e.materi.troubleshootData.troubleshootGraph == null)
                {
                    Debug.LogError("troubleshoot database missing");
                    return;
                }

                ShowPanel();
            }
            else
            {
                ResetTroubleshoot();
            }
        }

        public void ResetTroubleshoot()
        {
            if (currentCanvasT != null)
                Destroy(currentCanvasT);

            isTroubleshootDataExist = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;
using UnityEngine.Serialization;

namespace VirtualTraining.UI.Desktop
{
    public class ButtonTroubleshootComponent : MonoBehaviour
    {
        [SerializeField] InteractionButton buttonZoomIn;
        [SerializeField] InteractionButton buttonZoomOut;
        [SerializeField] InteractionButton buttonFocus;

        [SerializeField] RectTransform rectPanel;
        [SerializeField] RectTransform rectContainer;
        [FormerlySerializedAs("generateUI")]
        [SerializeField] TroubleshootGenerator generateUI;
        [SerializeField] float speed;

        bool isChangeScale;
        bool isChangePosition;
        Vector2 StartScale;
        Vector2 DestScale;

        Vector2 StartPost;
        Vector2 DestPost;

        float lerpValue;
        private void Start()
        {
            buttonZoomIn.OnClickEvent += ClickZoomIn;
            buttonZoomOut.OnClickEvent += ClickZoomOut;
            buttonFocus.OnClickEvent += ClickFocus;
        }

        private void OnDestroy()
        {
            buttonZoomIn.OnClickEvent -= ClickZoomIn;
            buttonZoomOut.OnClickEvent -= ClickZoomOut;
            buttonFocus.OnClickEvent -= ClickFocus;
        }

        private void Update()
        {
            if (isChangeScale && lerpValue < 1)
            {
                lerpValue += Time.deltaTime * speed;
                rectPanel.localScale = Vector2.Lerp(StartScale, DestScale, lerpValue);
                if(isChangePosition)
                    rectPanel.anchoredPosition = Vector2.Lerp(StartPost,DestPost, lerpValue);
            }
            else
            {
                isChangeScale = false;
                isChangePosition = false;
            }
        }

        void ClickZoomIn()
        {
            if (rectPanel.transform.localScale.x < 2)
            {
                lerpValue = 0;
                StartScale = rectPanel.transform.localScale;
                DestScale = StartScale * 1.3f;
                isChangeScale = true;
            }
            Debug.Log("zoom in");
        }
        void ClickZoomOut()
        {
            if (rectPanel.transform.localScale.x > 1)
            {
                lerpValue = 0;
                StartScale = rectPanel.transform.localScale;
                DestScale = StartScale / 1.3f;
                isChangeScale = true;
            }
            Debug.Log("zoom out");
        }

        void ClickFocus()
        {
            Debug.Log("focus");
            if(generateUI.currentButtonActive != null)
            {
                lerpValue = 0;
                //rectPanel.transform.localScale = new Vector2(2, 2);
                StartScale = rectPanel.transform.localScale;
                DestScale = new Vector2(2, 2); ;
                StartPost = rectPanel.anchoredPosition;
                Vector2 containerTemp = (rectContainer.anchoredPosition * -1)*2;
                DestPost = (generateUI.currentButtonActive.anchoredPosition * -1)*2;
                DestPost = DestPost + containerTemp;
                Debug.Log(DestPost);
                isChangePosition = true;
                isChangeScale = true;
            }
        }
    }
}

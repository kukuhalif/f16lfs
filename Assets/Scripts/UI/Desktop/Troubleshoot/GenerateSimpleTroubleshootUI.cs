using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;


namespace VirtualTraining.UI.Desktop
{
    public partial class TroubleshootGenerator : UIElement
    {
        float rangeSnapping = 100;
        IEnumerator GenerateSimplifiedButton(TroubleshootNode node, UINodeButton uiNodeButton)
        {
            for (int i = 0; i < node.GetOutputPort(outputNodeName).ConnectionCount; i++)
            {
                yield return null;
                yield return null;
                yield return null;
                points.Clear();
                GameObject line = Instantiate(uiLineRenderer, canvasParent, false);
                line.transform.position = canvasParent.transform.position;
                line.transform.SetAsFirstSibling();
                UILineRenderer lineRenderer = line.GetComponent<UILineRenderer>();

                RectTransform rects = uiNodeButton.gameObject.GetComponent<RectTransform>();
                Vector2 setCenterNode = new Vector2(0, -rects.sizeDelta.y / 2);
                uiNodeButton.AddedStartLineRenderer(lineRenderer);

                points.Add(ReverseYaxis(node.position) + offsideOutputNode + setCenterNode);

                var noda = node.GetOutputPort(outputNodeName).GetConnection(i).node;
                TroubleshootNode nodeCurr = noda as TroubleshootNode;

                if (node.GetOutputPort(outputNodeName).GetReroutePoints(i).Count <= 0)
                {
                    CheckNodePosition(node, nodeCurr);
                }

                InstantiateButton(node, nodeCurr, false, uiNodeButton, lineRenderer);
            }
        }

        void CheckNodePosition(TroubleshootNode inputNode, TroubleshootNode outputNode)
        {
            float distanceX = Mathf.Abs(outputNode.position.x - inputNode.position.x);
            float distanceY = Mathf.Abs(outputNode.position.y - inputNode.position.y);

            if (distanceX > 100 && distanceY > 100)
            {
                if (troubleshootData.startPositionGraph == GraphStartPosition.Up || troubleshootData.startPositionGraph == GraphStartPosition.Down)
                {
                    float centerPositionY = (outputNode.position.y + inputNode.position.y) / 2;
                    Vector2 lineVector = new Vector2(inputNode.position.x, centerPositionY);
                    GeneratePoint(lineVector);

                    Vector2 lineVectorX = new Vector2(outputNode.position.x, centerPositionY);
                    GeneratePoint(lineVectorX);
                }
                else
                {
                    float centerPositionX = (outputNode.position.x + inputNode.position.x) / 2;
                    Vector2 lineVector = new Vector2(centerPositionX, inputNode.position.y);
                    GeneratePoint(lineVector);

                    Vector2 lineVectorX = new Vector2(centerPositionX, outputNode.position.y);
                    GeneratePoint(lineVectorX);
                }
            }
        }

        void GeneratePoint(Vector2 pointPosition)
        {
            pointPosition = ReverseYaxis(pointPosition);
            points.Add(pointPosition + offsideInputNode);
            SizingContent(pointPosition + offsideInputNode);
        }

        public Vector2 SnappingButton(Vector2 pointPosition, TroubleshootNode inputNode, TroubleshootNode outputNode)
        {
            float distanceX = Mathf.Abs(outputNode.position.x - inputNode.position.x);
            float distanceY = Mathf.Abs(outputNode.position.y - inputNode.position.y);
            if (distanceX < 100)
            {
                pointPosition = new Vector2(inputNode.position.x, outputNode.position.y);
            }
            else if (distanceY < 100)
            {
                pointPosition = new Vector2(outputNode.position.x, inputNode.position.y);
            }

            return pointPosition;
        }

        Vector2 SnappingPoint(Vector2 input, Vector2 inputNode, Vector2 outputNode)
        {
            if (Mathf.Abs(input.x - inputNode.x) < rangeSnapping)
                input.x = inputNode.x;
            if (Mathf.Abs(input.y - inputNode.y) < rangeSnapping)
                input.y = inputNode.y;
            if (Mathf.Abs(input.x - outputNode.x) < rangeSnapping)
                input.x = outputNode.x;
            if (Mathf.Abs(input.y - outputNode.y) < rangeSnapping)
                input.y = outputNode.y;

            return input;
        }
    }
}

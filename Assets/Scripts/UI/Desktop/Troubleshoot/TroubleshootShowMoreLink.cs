using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI.Desktop
{
    public class TroubleshootShowMoreLink : TEXLink
    {
        [SerializeField] UINodeButton nodeButton;
        [SerializeField] TEXDraw expandText;
        GameObject player;
        GameObject cameraVR;
        Vector2 positionVRCamera;

        private void Start()
        {
            onClick.AddListener(RunLink);
            EventManager.AddListener<ApplyUIThemesEvent>(ApplyThemeListener);
            Reload();
            isVR = VirtualTrainingCamera.IsVR;
            //player = GameObject.Find("Player");
            //positionVRCamera = new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);

            //if (player != null)
            //{
            //    cameraVR = player.transform.Find("HandInputModuleDispay").gameObject;
            //    triggerCamera = cameraVR.GetComponent<Camera>();
            //}

            if (isVR)
            {
                positionVRCamera = new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);
                cameraVR = VirtualTrainingCamera.handVR.gameObject;
                if (cameraVR != null)
                {
                    triggerCamera = cameraVR.GetComponent<Camera>();
                }
            }
        }

        private void OnDestroy()
        {
            onClick.RemoveListener(RunLink);
            EventManager.RemoveListener<ApplyUIThemesEvent>(ApplyThemeListener);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            TEXDraw td = GetComponent<TEXDraw>();
            td.raycastTarget = true;
        }

        private void ApplyThemeListener(ApplyUIThemesEvent e)
        {
            Reload();
        }

        protected override void DefaultCursor()
        {
            base.DefaultCursor();
            VirtualTrainingCursor.DefaultCursor();
        }

        protected override void LinkCursor()
        {
            base.LinkCursor();
            VirtualTrainingCursor.Link();
        }
        protected override void UpdateInput()
        {
            base.UpdateInput();

            if (isVR)
            {
                if (cameraVR != null)
                {
                    input_PressPos.Clear();
                    input_HoverPos = positionVRCamera;

                    if (VirtualTrainingCamera.IsClickVR)
                        input_PressPos.Add(positionVRCamera);

                }
            }
        }

        private void Reload()
        {
            UITheme theme = DatabaseManager.GetUiTheme();
            normal = theme.TextLinkNormalColor;
            hover = theme.TextLinkHoverColor;
            pressed = theme.TextLinkPressedColor;
            after = theme.TextLinkAfterColor;
        }

        private void RunLink(string txt)
        {
            expandText.text = nodeButton.ExpandSwitch();
            Debug.Log(txt);
        }
    }
}

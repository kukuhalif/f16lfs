using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public static class SettingUtility
    {
        private static SettingData LAST_SETTING;
        private static Vector2 LAST_RESOLUTION;

        static SettingUtility()
        {
            LAST_SETTING = null;
            LAST_RESOLUTION = Vector2.zero;
        }

        public static SettingData LastSetting
        {
            get
            {
                if (LAST_SETTING == null)
                    LAST_SETTING = GetSavedSetting();

                return LAST_SETTING;
            }
        }

        public static Vector2 LastResolution
        {
            get
            {
                if (LAST_RESOLUTION == Vector2.zero)
                {
                    string prefix = Application.productName;
                    if (PlayerPrefs.HasKey(prefix + " lastResolutionX") && PlayerPrefs.HasKey(prefix + " lastResolutionY"))
                    {
                        LAST_RESOLUTION = new Vector2(PlayerPrefs.GetInt(prefix + " lastResolutionX"), PlayerPrefs.GetInt(prefix + " lastResolutionY"));
                    }
                    else
                    {
                        LAST_RESOLUTION = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
                    }
                }

                return LAST_RESOLUTION;
            }
        }

        public static void SetLastSetting(SettingData settingData)
        {
            LAST_SETTING = settingData;
        }

        public static void SetLastResolution(Vector2 resolution)
        {
            LAST_RESOLUTION = resolution;

            string prefix = Application.productName;
            PlayerPrefs.SetInt(prefix + " lastResolutionX", (int)LAST_RESOLUTION.x);
            PlayerPrefs.SetInt(prefix + " lastResolutionY", (int)LAST_RESOLUTION.y);
        }

        public static SettingData GetSavedSetting()
        {
            string prefix = Application.productName;
            string json = "";
            SettingData data = null;

            if (PlayerPrefs.HasKey(prefix + " setting"))
            {
                json = PlayerPrefs.GetString(prefix + " setting");
                data = JsonUtility.FromJson<SettingData>(json);
            }

            if (data == null)
            {
                data = DatabaseManager.GetDefaultSetting();
            }

            return data;
        }

        public static void SetSettingData(SettingData data)
        {
            string prefix = Application.productName;
            string json = JsonUtility.ToJson(data);

            PlayerPrefs.SetString(prefix + " setting", json);
            PlayerPrefs.SetInt(prefix + " lastResolutionX", (int)LAST_RESOLUTION.x);
            PlayerPrefs.SetInt(prefix + " lastResolutionY", (int)LAST_RESOLUTION.y);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public abstract class DatabaseReferenceType<T, U> : IDatabaseReference
        where T : TreeElement
    {
        [SerializeField] private int elementId = -1;
        [NonSerialized] private T data = null;

        protected TreeElement treeElement
        {
            get
            {
                if (data == null)
                {
                    if (elementId == -1)
                        return null;

                    T root = GetRoot();
                    FindElement(root);
                }
                return data;
            }
        }

        private void FindElement(T currentElement)
        {
            if (elementId != -1 && elementId == currentElement.id)
            {
                data = currentElement;
            }

            if (data == null && currentElement.hasChildren)
            {
                for (int i = 0; i < currentElement.children.Count; i++)
                {
                    FindElement(currentElement.children[i] as T);
                }
            }
        }

        private void GetChilds(TreeElement parent, List<TreeElement> treeElements)
        {
            if (parent.hasChildren)
            {
                for (int i = 0; i < parent.children.Count; i++)
                {
                    treeElements.Add(parent.children[i]);

                    GetChilds(parent.children[i], treeElements);
                }
            }
        }

        protected abstract T GetRoot();
        public abstract U GetData();
        public abstract U GetData(TreeElement treeElement);

        public List<U> GetDataRecursive()
        {
            if (treeElement == null)
                return new List<U>();

            List<TreeElement> treeElements = new List<TreeElement>();
            GetChilds(treeElement, treeElements);

            List<U> datas = new List<U>();
            datas.Add(GetData(treeElement));
            for (int i = 0; i < treeElements.Count; i++)
            {
                datas.Add(GetData(treeElements[i]));
            }

            return datas;
        }

        public int GetId()
        {
            return elementId;
        }

        public void SetId(int id)
        {
            elementId = id;
            data = null;
        }
    }
}

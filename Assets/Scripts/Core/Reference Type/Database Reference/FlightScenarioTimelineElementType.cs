﻿using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class FlightScenarioTimelineElementType : DatabaseReferenceType<FlightScenarioTimelineTreeElement, FlightScenarioTimelineDataModel>
    {
        protected override FlightScenarioTimelineTreeElement GetRoot()
        {
            return DatabaseManager.GetFlightScenarioTimelineTree();
        }

        public override FlightScenarioTimelineDataModel GetData()
        {
            return GetData(treeElement);
        }

        public override FlightScenarioTimelineDataModel GetData(TreeElement treeElement)
        {
            var te = treeElement as FlightScenarioTimelineTreeElement;
            if (te == null)
                return null;
            return te.data;
        }
    }
}

﻿using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class VirtualButtonElementType : DatabaseReferenceType<VirtualButtonTreeElement, VirtualButtonDataModel>
    {
        protected override VirtualButtonTreeElement GetRoot()
        {
            return DatabaseManager.GetVirtualButtonTree();
        }

        public override VirtualButtonDataModel GetData()
        {
            return GetData(treeElement);
        }

        public override VirtualButtonDataModel GetData(TreeElement treeElement)
        {
            var te = treeElement as VirtualButtonTreeElement;
            if (te == null)
                return null;
            return te.VirtualButtonData;
        }
    }
}

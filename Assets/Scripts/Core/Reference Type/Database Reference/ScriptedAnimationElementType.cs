﻿using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class ScriptedAnimationElementType : DatabaseReferenceType<ScriptedAnimationTreeElement, ScriptedAnimationDataModel>
    {
        protected override ScriptedAnimationTreeElement GetRoot()
        {
            return DatabaseManager.GetScriptedAnimationTree();
        }

        public override ScriptedAnimationDataModel GetData()
        {
            return GetData(treeElement);
        }

        public override ScriptedAnimationDataModel GetData(TreeElement treeElement)
        {
            var te = treeElement as ScriptedAnimationTreeElement;
            if (te == null)
                return null;
            return te.data;
        }
    }
}

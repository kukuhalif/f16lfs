using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class MaintenanceElementType : DatabaseReferenceType<MaintenanceTreeElement, MaintenanceDataModel>
    {
        protected override MaintenanceTreeElement GetRoot()
        {
            return DatabaseManager.GetMaintenanceTree();
        }

        public override MaintenanceDataModel GetData()
        {
            return GetData(treeElement);
        }

        public override MaintenanceDataModel GetData(TreeElement treeElement)
        {
            var ma = treeElement as MaintenanceTreeElement;
            if (ma == null)
                return null;
            return ma.data;
        }
    }
}

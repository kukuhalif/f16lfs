using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class FlightScenarioElementType : DatabaseReferenceType<FlightScenarioTreeElement, FlightScenarioDataModel>
    {
        protected override FlightScenarioTreeElement GetRoot()
        {
            return DatabaseManager.GetFlightScenarioTree();
        }

        public override FlightScenarioDataModel GetData()
        {
            return GetData(treeElement);
        }

        public override FlightScenarioDataModel GetData(TreeElement treeElement)
        {
            var ma = treeElement as FlightScenarioTreeElement;
            if (ma == null)
                return null;
            return ma.data;
        }
    }

}

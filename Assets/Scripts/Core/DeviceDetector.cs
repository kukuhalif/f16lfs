using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceDetector : MonoBehaviour
{
    [SerializeField] GameObject tablet_ui;
    [SerializeField] GameObject smartphone_ui;

    public enum UI { tablet_ui, smartphone_ui }

    public static UI GetDevice()
    {
        UI selectedUI = UI.tablet_ui;

#if UNITY_EDITOR

        selectedUI = UI.tablet_ui;

#elif UNITY_STANDALONE_WIN

        selectedUI = UI.tablet_ui;

#elif UNITY_ANDROID

        selectedUI = UI.smartphone_ui;

#elif UNITY_IOS

        var identifier = SystemInfo.deviceModel;
        if (identifier.StartsWith("iPhone"))
        {
            selectedUI = UI.smartphone_ui;
        }
        else if (identifier.StartsWith("iPad"))
        {
             selectedUI = UI.tablet_ui;
        }
#endif

        return selectedUI;

    }

    void Start()
    {
        UI selectedUI = GetDevice();

        switch (selectedUI)
        {
            case UI.tablet_ui:

                tablet_ui.SetActive(true);

                break;

            case UI.smartphone_ui:

                smartphone_ui.SetActive(true);

                break;
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System;

namespace VirtualTraining.Core
{

#if UNITY_EDITOR
    public enum IconEnum
    {
        // anim
        AnimDelayPlay,
        AnimDisablePlaybar,
        AnimManualPlay,
        AnimPlayLoop,
        AnimPlaySequentially,

        // generic icon
        EditAdd,
        EditClear,
        EditCopy,
        EditCut,
        EditDelete,
        EditDuplicate,
        EditFavourite,
        EditFilter,
        EditInsert,
        EditMultipleSelect,
        EditPaste,
        EditPosition,
        EditRemove,

        // icon Set
        IconSet,

        // list editor
        ListFoldoutClose,
        ListFoldoutOpen,
        ListEditorGrabListElement,

        // part object
        PartObjectBlink,
        PartObjectHide,
        PartObjectHighlight,
        PartObjectIgnoreCutaway,
        PartObjectSelect,
        PartObjectSetLayer,
        PartObjectShow,
        PartObjectSolo,
        PartObjectXRay,

        // path
        PathMateri,

        // text
        TextCapitalize,
        TextSentenceCase,
        TextSmallCase,
    };

    [Serializable]
    public class EditorIcon
    {
        public IconEnum category;
        public Texture2D icon;

        public EditorIcon(IconEnum category, Texture2D icon)
        {
            this.category = category;
            this.icon = icon;
        }

        public EditorIcon()
        {

        }

        public EditorIcon(object obj)
        {
            Texture2D texture2D = obj as Texture2D;
            if (texture2D != null)
            {
                icon = texture2D;
            }
        }
    }

#endif

    [Serializable]
    public class SettingData
    {
        public float rotateSpeed;
        public float zoomSpeed;
        public float dragSpeed;
        public float cameraTransitionSpeed;
        public float voiceVolume;
        public float sfxVolume;
        public bool fullscreen;
        public bool dynamicUiSize;
        public int resolution;
        public int uiTheme;
        public bool showEnvironmentObject;
        public int quality;
        public float vrCanvasDistance;
        public float vrMenuHeight;
        public float resetObjectSpeed { get => cameraTransitionSpeed * 2f; }
        public float speedRotationVR;
        public float speedScrollbar;
        public float speedLerpMenuVR;
        public float durationConfirmationButton;
    }

    [Serializable]
    public class MinMaxSettingData
    {
        public Vector2 rotateSpeed;
        public Vector2 zoomSpeed;
        public Vector2 dragSpeed;
        public Vector2 moveCameraSpeed;
        public Vector2 bgmVolume;
        public Vector2 sfxVolume;
        public Vector2 vrCanvasDistance;
        public Vector2 vrMenuHeight;
        public Vector2 speedRotationVR;
        public Vector2 speedScrollbar;
        public Vector2 speedLerpMenuVR;
        public Vector2 durationConfirmationButton;

        public MinMaxSettingData()
        {
            rotateSpeed = new Vector2(1, 10);
            zoomSpeed = new Vector2(0.5f, 5f);
            dragSpeed = new Vector2(1, 10);
            moveCameraSpeed = new Vector2(1, 10);
            bgmVolume = new Vector2(0, 1);
            sfxVolume = new Vector2(0, 1);
            vrCanvasDistance = new Vector2(0, 10);
            vrMenuHeight = new Vector2(0, 10);
            speedRotationVR = new Vector2(0, 10);
            speedScrollbar = new Vector2(0, 10);
            speedLerpMenuVR = new Vector2(0, 10);
            durationConfirmationButton = new Vector2(0, 10);
        }
    }

    [Serializable]
    public class EditorConfiguration
    {
        public Color selectedElementColor;
        public int maxItemListFieldPage;

        public EditorConfiguration()
        {
            selectedElementColor = Color.blue;
            maxItemListFieldPage = 10;
        }
    }

    [Serializable]
    public class DeviceBehaviourPackage
    {
        public DeviceBehaviour objectSelector;
    }

    public enum SceneMode
    {
        Desktop,
        Tablet,
        Smartphone,
        VR
    }

    [Serializable]
    public class SceneConfig
    {
        public bool includeInBuildSetting;
        public SceneMode mode;

#if UNITY_EDITOR
        public List<SceneAsset> sceneAssets;
        public SceneAsset quizSceneAsset;
        public SceneAsset vrHeadSceneAsset;
        public SceneAsset networkingSceneAsset;
#endif
        public string mainScenePath;
        public List<string> scenes;
        public string quizScene;
        public string vrHeadScene;
        public string networkingScene;

        public DeviceBehaviourPackage behaviourPackage;

        public List<GameObject> persistentUiPrefabs;

        public SceneConfig(SceneMode mode)
        {
            this.mode = mode;
            scenes = new List<string>();
#if UNITY_EDITOR
            sceneAssets = new List<SceneAsset>();
#endif
        }
    }

    [Serializable]
    public class SystemConfig
    {
        // default scene mode
        [SerializeField] public SceneMode startingSceneMode;

        // addressable
        [SerializeField] public bool isAddressableAssetMode;

        // scenes
        [SerializeField] public List<SceneConfig> sceneConfigs;
        [SerializeField] public string startingScene;

#if UNITY_EDITOR
        [SerializeField] public SceneAsset startingSceneAsset;
        // editor icon
        [SerializeField] public List<EditorIcon> editorIcons;
#endif

        // setting
        [SerializeField] public float maxRaycastDistance;
        [SerializeField] public SettingData defaultSetting;
        [SerializeField] public MinMaxSettingData minMaxSetting;

        // editor
        [SerializeField] public EditorConfiguration editorConfig;
    }

    [CreateAssetMenu(fileName = "System Config", menuName = "Virtual Training/Config/System Config File")]
    public class VirtualTrainingSystemConfig : ScriptableObjectBase<SystemConfig>
    {
        [SerializeField] SystemConfig systemConfig;

        public override SystemConfig GetData()
        {
            return systemConfig;
        }

        public override void SetData(SystemConfig data)
        {
            systemConfig = data;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Video;
using System.Linq;

namespace VirtualTraining.Core
{
    public class CustomTreeElementReference<T>
        where T : TreeElement
    {
        public bool isFolder;
        public string completeName;
        public T data;

        public CustomTreeElementReference(bool isFolder, string completeName, T data)
        {
            this.isFolder = isFolder;
            this.completeName = completeName;
            this.data = data;
        }
    }

    public static class DatabaseManager
    {
        static VirtualTrainingSystemConfig SYS_CONFIG;
        static VirtualTrainingAppConfig APP_CONFIG;
        static VirtualTrainingContentConfig CONTENT_CONFIG;


        static DatabaseManager()
        {
            SYS_CONFIG = Resources.Load<VirtualTrainingSystemConfig>("System Config");
            APP_CONFIG = Resources.Load<VirtualTrainingAppConfig>("App Config");
            CONTENT_CONFIG = Resources.Load<VirtualTrainingContentConfig>("Content Config");
        }

#if UNITY_EDITOR

        public static void OnDeletedAsset(string assetPath)
        {
            var appData = APP_CONFIG.GetData();
            var contentData = CONTENT_CONFIG.GetData();

            if (!assetPath.Contains(appData.pathConfig.contentAssetPath))
                return;

            var extension = System.IO.Path.GetExtension(assetPath);
            if (extension == ".asset" || extension == ".pdf")
            {
                Debug.Log("start checking the contents of the database database which may be null (materi database, materi pdf, virtual button, scriptable animation and pdf ui and link)");

                // check materi database
                contentData.materiContainers = contentData.materiContainers.Where(x => x.materi != null).ToList();

                // check materi pdf
                foreach (var materiContainer in contentData.materiContainers)
                {
                    materiContainer.pdfs = materiContainer.pdfs.Where(x => x != null).ToList();
                }

                // check virtual button database
                contentData.virtualButtonDatabases = contentData.virtualButtonDatabases.Where(x => x != null).ToList();

                // check scriptable animation database
                contentData.scriptedAnimationDatabases = contentData.scriptedAnimationDatabases.Where(x => x != null).ToList();

                // check pdf ui
                contentData.pdfUis = contentData.pdfUis.Where(x => x != null).ToList();

                // check pdf link
                contentData.pdfLinks = contentData.pdfLinks.Where(x => x != null).ToList();
            }
        }

        public static SceneAsset GetStartingSceneAsset()
        {
            return SYS_CONFIG.GetData().startingSceneAsset;
        }

        public static List<EditorIcon> GetEditorIcons()
        {
            return SYS_CONFIG.GetData().editorIcons;
        }

#endif


        public static SceneConfig GetStartingSceneConfig()
        {
            var device = DeviceDetector.GetDevice();

            switch (device)
            {
                case DeviceDetector.UI.tablet_ui:

                    SYS_CONFIG.GetData().startingSceneMode = SceneMode.Tablet;

                    break;
                case DeviceDetector.UI.smartphone_ui:

                    SYS_CONFIG.GetData().startingSceneMode = SceneMode.Smartphone;

                    break;
                default:

                    SYS_CONFIG.GetData().startingSceneMode = SceneMode.Tablet;

                    break;
            }

            return GetSceneConfig(SYS_CONFIG.GetData().startingSceneMode);
        }

        public static void SetIsAddressableAssetMode(bool active)
        {
            SYS_CONFIG.GetData().isAddressableAssetMode = active;
        }

        public static bool IsAddressableAssetMode()
        {
            return SYS_CONFIG.GetData().isAddressableAssetMode;
        }

        public static List<MateriContainer> GetMateriConfigDatas()
        {
            return CONTENT_CONFIG.GetData().materiContainers;
        }

        public static List<SceneConfig> GetAllSceneConfig()
        {
            return SYS_CONFIG.GetData().sceneConfigs;
        }

        public static SceneConfig GetSceneConfig(SceneMode mode)
        {
            foreach (var sceneConfig in SYS_CONFIG.GetData().sceneConfigs)
            {
                if (sceneConfig.mode == mode)
                    return sceneConfig;
            }

            return null;
        }

        public static string GetUIScene(SceneConfig sceneConfig)
        {
            var scenes = sceneConfig.scenes;

            if (scenes.Count < 2)
                return "";

            return scenes[1];
        }

        public static string GetCameraScene(SceneConfig sceneConfig)
        {
            var scenes = sceneConfig.scenes;

            if (scenes.Count < 3)
                return "";

            return scenes[2];
        }

        public static VRData GetVRData()
        {
            return APP_CONFIG.GetData().vrData;
        }

        public static void SetVRData(VRData vrData)
        {
            APP_CONFIG.GetData().vrData = vrData;
        }

        public static List<Material> GetXrayMaterials()
        {
            return CONTENT_CONFIG.GetData().xRayMaterials;
        }

        public static Color GetDefaultHighlightColor()
        {
            return CONTENT_CONFIG.GetData().highlightColor;
        }

        public static Color GetDefaultSelectedColor()
        {
            return CONTENT_CONFIG.GetData().selectedColor;
        }

        public static float GetOutlineWidth()
        {
            return CONTENT_CONFIG.GetData().outlineWidth;
        }

        public static Color GetDefaultBlinkColor()
        {
            return CONTENT_CONFIG.GetData().blinkColor;
        }

        public static Color GetDefaultFresnelColor()
        {
            return CONTENT_CONFIG.GetData().fresnelColor;
        }

        public static VideoClip GetIntroVideo()
        {
            return APP_CONFIG.GetData().video.intro;
        }

        public static List<SFXData> GetSFXDatas()
        {
            return APP_CONFIG.GetData().sfxs;
        }

        public static string GetStartingSceneName()
        {
            return SYS_CONFIG.GetData().startingScene;
        }

        public static float GetMaxRaycastDistance()
        {
            return SYS_CONFIG.GetData().maxRaycastDistance;
        }

        public static ObjectListTreeElement GetObjectListTree()
        {
            var elements = CONTENT_CONFIG.GetData().objectListDatabase.GetData().treeElements;
            var tree = TreeElementUtility.ListToTree(elements);
            return tree;
        }

        public static List<CullingTreeElement> GetCullingDatas()
        {
            return CONTENT_CONFIG.GetData().cullingDatabase.GetData().treeElements;
        }

        public static ModelVersion GetModelVersion()
        {
            if (CONTENT_CONFIG.GetData().modelVersionDatabase == null)
                return null;

            return CONTENT_CONFIG.GetData().modelVersionDatabase.GetData();
        }

        public static List<MateriTreeAsset> GetMateriAssets()
        {
            List<MateriTreeAsset> materiTreeAssets = new List<MateriTreeAsset>();
            var mats = CONTENT_CONFIG.GetData().materiContainers;
            foreach (var mat in mats)
            {
                materiTreeAssets.Add(mat.materi);
            }
            return materiTreeAssets;
        }

        public static List<ScriptedAnimationTreeAsset> GetScriptedAnimationDatabases()
        {
            List<ScriptedAnimationTreeAsset> scriptedAnimationDatabases = new List<ScriptedAnimationTreeAsset>();
            var databases = CONTENT_CONFIG.GetData().scriptedAnimationDatabases;
            foreach (var database in databases)
            {
                scriptedAnimationDatabases.Add(database);
            }
            return scriptedAnimationDatabases;
        }

        public static List<VirtualButtonTreeAsset> GetVirtualButtonDatabases()
        {
            List<VirtualButtonTreeAsset> virtualbuttonDatabase = new List<VirtualButtonTreeAsset>();
            var databases = CONTENT_CONFIG.GetData().virtualButtonDatabases;
            foreach (var database in databases)
            {
                virtualbuttonDatabase.Add(database);
            }
            return virtualbuttonDatabase;
        }

        private static T GetTree<T>(T root, List<List<T>> sources, bool firstChildIsFolder) where T : TreeElement
        {
            List<T> childs = new List<T>();
            for (int i = 0; i < sources.Count; i++)
            {
                var tree = TreeElementUtility.ListToTree(sources[i]);

                tree.parent = root;
                childs.Add(tree);
            }

            root.children = new List<TreeElement>();

            for (int i = 0; i < childs.Count; i++)
            {
                childs[i].parent = root;

                if (firstChildIsFolder)
                {
                    childs[i].isFolder = true;

                    if (childs[i].id <= 0)
                        childs[i].id = i + 1;
                }

                root.children.Add(childs[i]);
            }

            return root;
        }

        public static MateriTreeElement GetMateriTree()
        {
            var sources = CONTENT_CONFIG.GetData().materiContainers;

            List<List<MateriTreeElement>> materies = new List<List<MateriTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                if (sources[i].materi != null)
                {
                    List<MateriTreeElement> elements = new List<MateriTreeElement>(sources[i].materi.GetData().treeElements);

                    for (int p = 0; p < elements.Count; p++)
                    {
                        for (int d = 0; d < elements[p].MateriData.pdfs.Count; d++)
                        {
                            if (sources[i].pdfs.Count > elements[p].MateriData.pdfs[d].index)
                                elements[p].MateriData.pdfs[d].pdfAsset = sources[i].pdfs[elements[p].MateriData.pdfs[d].index];
                        }
                    }

                    materies.Add(elements);
                }
            }

            MateriTreeElement root = new MateriTreeElement("root", -1, 0, MateriElementType.Materi, true);

            return GetTree(root, materies, true);
        }

        public static MaintenanceTreeElement GetMaintenanceTree()
        {
            List<List<MaintenanceTreeElement>> maintenances = new List<List<MaintenanceTreeElement>>();
            var mats = GetMateriTree();

            GetMaintenance(mats, maintenances);

            MaintenanceTreeElement root = new MaintenanceTreeElement("", -1, 0, true);
            List<MaintenanceTreeElement> materies = new List<MaintenanceTreeElement>();

            for (int i = 0; i < maintenances.Count; i++)
            {
                var materiAsset = GetMaintenanceTree(maintenances[i]);
                materies.Add((MaintenanceTreeElement)materiAsset);
            }

            root.children = new List<TreeElement>();

            for (int i = 0; i < materies.Count; i++)
            {
                root.children.Add(materies[i]);
                materies[i].parent = root;
            }

            return root;
        }

        public static List<CustomTreeElementReference<MaintenanceTreeElement>> GetEditorMaintenanceList()
        {
            List<CustomTreeElementReference<MaintenanceTreeElement>> maintenances = new List<CustomTreeElementReference<MaintenanceTreeElement>>();
            var mats = GetMateriTree();

            GetEditorMaintenance(mats, maintenances);

            return maintenances;
        }

        public static List<TroubleshootNode> GetTroubleshootNodes()
        {
            List<string> troubleshootNames = new List<string>();
            List<TroubleshootNode> troubleshoots = new List<TroubleshootNode>();
            var mats = GetMateriTree();

            GetTroubleshoot(mats, troubleshoots, troubleshootNames);

            return troubleshoots;
        }

        private static void GetEditorMaintenance(MateriTreeElement materiTreeElement, List<CustomTreeElementReference<MaintenanceTreeElement>> maintenances)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.Maintenance &&
                materiTreeElement.MateriData.maintenanceTree != null &&
                materiTreeElement.MateriData.maintenanceTree.GetData().treeElements.Count > 0)
            {
                // to calculate maintenance tree complete name
                TreeElementUtility.ListToTree(materiTreeElement.MateriData.maintenanceTree.GetData().treeElements);

                for (int i = 0; i < materiTreeElement.MateriData.maintenanceTree.GetData().treeElements.Count; i++)
                {
                    MaintenanceTreeElement maintenanceTreeElement = materiTreeElement.MateriData.maintenanceTree.GetData().treeElements[i];
                    if (maintenanceTreeElement.depth > -1)
                    {
                        string completeName = "(materi) -> " + materiTreeElement.CompleteName + "/(maintenance) -> " + maintenanceTreeElement.CompleteName;
                        CustomTreeElementReference<MaintenanceTreeElement> newData = new CustomTreeElementReference<MaintenanceTreeElement>(maintenanceTreeElement.isFolder, completeName, maintenanceTreeElement);
                        maintenances.Add(newData);
                    }
                }
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetEditorMaintenance((MateriTreeElement)materiTreeElement.children[i], maintenances);
                }
            }
        }

        private static void GetMaintenance(MateriTreeElement materiTreeElement, List<List<MaintenanceTreeElement>> maintenances)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.Maintenance &&
                 materiTreeElement.MateriData.maintenanceTree != null &&
                materiTreeElement.MateriData.maintenanceTree.GetData().treeElements.Count > 0)
            {
                maintenances.Add(materiTreeElement.MateriData.maintenanceTree.GetData().treeElements);
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetMaintenance((MateriTreeElement)materiTreeElement.children[i], maintenances);
                }
            }
        }

        private static void GetTreeElementListFromTreeAndCompleteNames<T>(T treeElement, List<T> results, List<string> completeNameResults)
            where T : TreeElement
        {
            string completeName = treeElement.CompleteName;
            completeNameResults.Add(completeName);
            results.Add(treeElement);

            if (treeElement.hasChildren)
            {
                for (int i = 0; i < treeElement.children.Count; i++)
                {
                    GetTreeElementListFromTreeAndCompleteNames<T>((T)treeElement.children[i], results, completeNameResults);
                }
            }
        }

        private static void GetTroubleshoot(MateriTreeElement materiTreeElement, List<TroubleshootNode> troubleshootNodes, List<string> troubleshootNames)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.Troubleshoot &&
                materiTreeElement.MateriData.troubleshootData != null &&
                materiTreeElement.MateriData.troubleshootData.troubleshootGraph != null &&
                materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes.Count > 0)
            {
                for (int i = 0; i < materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes.Count; i++)
                {
                    if (materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes[i] != null)
                    {
                        TroubleshootNode tn = materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes[i] as TroubleshootNode;
                        string completeName = "(materi) -> " + materiTreeElement.CompleteName + "/(troubleshoot) -> " + tn.name;
                        tn.CompleteName = completeName;
                        troubleshootNodes.Add(tn);
                        troubleshootNames.Add(materiTreeElement.MateriData.name);
                    }
                }
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetTroubleshoot((MateriTreeElement)materiTreeElement.children[i], troubleshootNodes, troubleshootNames);
                }
            }
        }

        public static MaintenanceTreeElement GetMaintenanceTree(List<MaintenanceTreeElement> elements, string rootName = "root")
        {
            List<List<MaintenanceTreeElement>> materies = new List<List<MaintenanceTreeElement>>();
            materies.Add(elements);

            MaintenanceTreeElement root = new MaintenanceTreeElement(rootName, -1, 0, true);

            return GetTree(root, materies, false);
        }

        public static ScriptedAnimationTreeAsset GetScriptedAnimationDatabase(int elementId)
        {
            var databases = CONTENT_CONFIG.GetData().scriptedAnimationDatabases;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].GetData().treeElements.Count; j++)
                {
                    if (databases[i].GetData().treeElements[j].id == elementId)
                        return databases[i];
                }
            }

            return null;
        }

        public static ScriptedAnimationTreeElement GetScriptedAnimationTree()
        {
            var sources = CONTENT_CONFIG.GetData().scriptedAnimationDatabases;

            List<List<ScriptedAnimationTreeElement>> materies = new List<List<ScriptedAnimationTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                List<ScriptedAnimationTreeElement> elements = new List<ScriptedAnimationTreeElement>(sources[i].GetData().treeElements);
                materies.Add(elements);
            }

            ScriptedAnimationTreeElement root = new ScriptedAnimationTreeElement("root", -1, 0, true);

            return GetTree(root, materies, true);
        }

        public static VirtualButtonTreeAsset GetVirtualButtonDatabase(int elementId)
        {
            var databases = CONTENT_CONFIG.GetData().virtualButtonDatabases;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].GetData().treeElements.Count; j++)
                {
                    if (databases[i].GetData().treeElements[j].id == elementId)
                        return databases[i];
                }
            }

            return null;
        }

        public static FlightScenarioTimelineTreeAsset GetFlightScenarioTimelineDatabase(int elementId)
        {
            var databases = CONTENT_CONFIG.GetData().flightScenarioTimelineDatabases;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].GetData().treeElements.Count; j++)
                {
                    if (databases[i].GetData().treeElements[j].id == elementId)
                        return databases[i];
                }
            }

            return null;
        }

        public static MateriTreeElement GetMateriElement(int elementId)
        {
            var databases = CONTENT_CONFIG.GetData().materiContainers;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].materi.GetData().treeElements.Count; j++)
                {
                    if (databases[i].materi.GetData().treeElements[j].id == elementId)
                        return databases[i].materi.GetData().treeElements[j];
                }
            }

            return null;
        }

        public static VirtualButtonTreeElement GetVirtualButtonTree()
        {
            var sources = CONTENT_CONFIG.GetData().virtualButtonDatabases;

            List<List<VirtualButtonTreeElement>> materies = new List<List<VirtualButtonTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                List<VirtualButtonTreeElement> elements = new List<VirtualButtonTreeElement>(sources[i].GetData().treeElements);
                materies.Add(elements);
            }

            VirtualButtonTreeElement root = new VirtualButtonTreeElement("root", -1, 0, true);

            return GetTree(root, materies, true);
        }

        public static FlightScenarioTimelineTreeElement GetFlightScenarioTimelineTree()
        {
            var sources = CONTENT_CONFIG.GetData().flightScenarioTimelineDatabases;

            List<List<FlightScenarioTimelineTreeElement>> materies = new List<List<FlightScenarioTimelineTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                List<FlightScenarioTimelineTreeElement> elements = new List<FlightScenarioTimelineTreeElement>(sources[i].GetData().treeElements);
                materies.Add(elements);
            }

            FlightScenarioTimelineTreeElement root = new FlightScenarioTimelineTreeElement("root", -1, 0, true);

            return GetTree(root, materies, true);
        }


        public static SettingData GetDefaultSetting()
        {
            return SYS_CONFIG.GetData().defaultSetting;
        }

        public static MinMaxSettingData GetMinMaxSetting()
        {
            return SYS_CONFIG.GetData().minMaxSetting;
        }

        public static List<Paroxe.PdfRenderer.PDFAsset> GetPdfUi()
        {
            return CONTENT_CONFIG.GetData().pdfUis;
        }

        public static List<Paroxe.PdfRenderer.PDFAsset> GetPdfLink()
        {
            return CONTENT_CONFIG.GetData().pdfLinks;
        }

        public static void SetUiTheme(int index)
        {
            APP_CONFIG.GetData().uiConfig.selectedTheme = index;
        }

        public static int GetUiThemeIndex()
        {
            return APP_CONFIG.GetData().uiConfig.selectedTheme;
        }

        public static List<UITheme> GetUIThemes()
        {
            return APP_CONFIG.GetData().uiConfig.uiThemes;
        }

        public static UITheme GetUiTheme()
        {
            if (APP_CONFIG.GetData().uiConfig.selectedTheme >= 0 && APP_CONFIG.GetData().uiConfig.selectedTheme < APP_CONFIG.GetData().uiConfig.uiThemes.Count)
                return APP_CONFIG.GetData().uiConfig.uiThemes[APP_CONFIG.GetData().uiConfig.selectedTheme];

            APP_CONFIG.GetData().uiConfig.selectedTheme = GetDefaultSetting().uiTheme;
            return APP_CONFIG.GetData().uiConfig.uiThemes[APP_CONFIG.GetData().uiConfig.selectedTheme];
        }

        public static AnimationCurve GetUIFadeInCurve()
        {
            return APP_CONFIG.GetData().uiConfig.fadeInCurve;
        }

        public static AnimationCurve GetUIFadeOutCurve()
        {
            return APP_CONFIG.GetData().uiConfig.fadeOutCurve;
        }

        public static float GetUITransitionSpeed()
        {
            return APP_CONFIG.GetData().uiConfig.transitionSpeed;
        }

        public static ProjectDetails GetProjectDetails()
        {
            return CONTENT_CONFIG.GetData().projectDetails;
        }

        public static Cursor GetCursorTexture()
        {
            return APP_CONFIG.GetData().cursorTexture;
        }

        public static UITexture GetUITexture()
        {
            return APP_CONFIG.GetData().uiConfig.uiTexture;
        }

        public static List<VideoClip> GetAppsLoadingVideos()
        {
            return APP_CONFIG.GetData().video.appsLoadingVideos;
        }

        public static List<VideoClip> GetQuizLoadingVideos()
        {
            return APP_CONFIG.GetData().video.quizLoadingVideos;
        }

        public static QuizData getQuizData()
        {
            return CONTENT_CONFIG.GetData().quizData;

        }

        public static EditorConfiguration GetEditorConfiguration()
        {
            return SYS_CONFIG.GetData().editorConfig;
        }

        public static CameraData GetCameraData()
        {
            return CONTENT_CONFIG.GetData().cameraData;
        }

        public static ContentPathConfig GetContentPathConfig()
        {
            return APP_CONFIG.GetData().pathConfig;
        }
        public static VRDataContent GetContentVRData()
        {
            return CONTENT_CONFIG.GetData().vrDataContent;
        }

        public static CameraDestination GetCameraPreset(string name)
        {
            List<CameraPreset> presets = CONTENT_CONFIG.GetData().cameraData.cameraPresets;
            for (int i = 0; i < presets.Count; i++)
            {
                if (name == presets[i].name)
                {
                    return presets[i].destination;
                }
            }

            return null;
        }

        public static CutawayConfig GetCutawayConfig()
        {
            return CONTENT_CONFIG.GetData().cutawayConfig;
        }

        public static OutlineNormalVector GetOutlineNormalVector()
        {
            return CONTENT_CONFIG.GetData().outlineNormalVector;
        }

        public static List<PartNumberTreeAsset> GetPartNumberDatabases()
        {
            return CONTENT_CONFIG.GetData().partNumberDatabases;
        }

        private static void GetEditorFlightScenario(MateriTreeElement materiTreeElement, List<CustomTreeElementReference<FlightScenarioTreeElement>> flightScenarios)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.FlightScenario &&
                materiTreeElement.MateriData.flightScenarioTree != null &&
                materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements.Count > 0)
            {
                // to calculate flight scenario tree complete name
                TreeElementUtility.ListToTree(materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements);

                for (int i = 0; i < materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements.Count; i++)
                {
                    FlightScenarioTreeElement flightScenarioTreeElement = materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements[i];
                    if (flightScenarioTreeElement.depth > -1)
                    {
                        string completeName = "(materi) -> " + materiTreeElement.CompleteName + "/(flight scenario) -> " + flightScenarioTreeElement.CompleteName;
                        CustomTreeElementReference<FlightScenarioTreeElement> newData = new CustomTreeElementReference<FlightScenarioTreeElement>(flightScenarioTreeElement.isFolder, completeName, flightScenarioTreeElement);
                        flightScenarios.Add(newData);
                    }
                }
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetEditorFlightScenario((MateriTreeElement)materiTreeElement.children[i], flightScenarios);
                }
            }
        }

        private static void GetFlightScenario(MateriTreeElement materiTreeElement, List<List<FlightScenarioTreeElement>> flightScenarios)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.FlightScenario &&
                materiTreeElement.MateriData.flightScenarioTree != null &&
                materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements.Count > 0)
            {
                flightScenarios.Add(materiTreeElement.MateriData.flightScenarioTree.GetData().treeElements);
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetFlightScenario((MateriTreeElement)materiTreeElement.children[i], flightScenarios);
                }
            }
        }


        public static List<CustomTreeElementReference<FlightScenarioTreeElement>> GetEditorFlightScenarioList()
        {
            List<CustomTreeElementReference<FlightScenarioTreeElement>> flightScenarios = new List<CustomTreeElementReference<FlightScenarioTreeElement>>();
            var mats = GetMateriTree();

            GetEditorFlightScenario(mats, flightScenarios);

            return flightScenarios;
        }

        public static FlightScenarioTreeElement GetFlightScenarioTree(List<FlightScenarioTreeElement> elements, string rootName = "root")
        {
            List<List<FlightScenarioTreeElement>> materies = new List<List<FlightScenarioTreeElement>>();
            materies.Add(elements);

            FlightScenarioTreeElement root = new FlightScenarioTreeElement(rootName, -1, 0, true);

            return GetTree(root, materies, false);
        }

        public static FlightScenarioTreeElement GetFlightScenarioTree()
        {
            List<List<FlightScenarioTreeElement>> flightScenarios = new List<List<FlightScenarioTreeElement>>();
            var mats = GetMateriTree();

            GetFlightScenario(mats, flightScenarios);

            FlightScenarioTreeElement root = new FlightScenarioTreeElement("", -1, 0, true);
            List<FlightScenarioTreeElement> materies = new List<FlightScenarioTreeElement>();

            for (int i = 0; i < flightScenarios.Count; i++)
            {
                var materiAsset = GetFlightScenarioTree(flightScenarios[i]);
                materies.Add((FlightScenarioTreeElement)materiAsset);
            }

            root.children = new List<TreeElement>();

            for (int i = 0; i < materies.Count; i++)
            {
                root.children.Add(materies[i]);
                materies[i].parent = root;
            }

            return root;
        }
    }
}

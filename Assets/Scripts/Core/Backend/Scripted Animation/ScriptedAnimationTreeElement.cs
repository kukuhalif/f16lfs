﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
	[Serializable]
	public class ScriptedAnimationTreeElement : TreeElement
    {
        [SerializeField] ScriptedAnimationDataModel scriptedAnimationDataModel;

		public ScriptedAnimationDataModel data { get => scriptedAnimationDataModel; }

		public ScriptedAnimationTreeElement(string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
		{
            scriptedAnimationDataModel = new ScriptedAnimationDataModel(name);
		}

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(data);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            scriptedAnimationDataModel = JsonUtility.FromJson<ScriptedAnimationDataModel>(data) as ScriptedAnimationDataModel;
        }
    }
}
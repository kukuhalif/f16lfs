﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class ScriptedAnimationData : ITreeElementListWrapper
    {
        [SerializeField] public List<ScriptedAnimationTreeElement> treeElements = new List<ScriptedAnimationTreeElement>();

        public TreeElement CreateRoot()
        {
            return new ScriptedAnimationTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "New Scripted Animation Database", menuName = "Virtual Training/Materi/Scripted Animation Database")]
    public class ScriptedAnimationTreeAsset : ScriptableObjectTreeBase<ScriptedAnimationData>
    {
        [SerializeField] ScriptedAnimationData data;

        public override TreeElement GetTreeElement(int index)
        {
            return data.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return data.treeElements.Count;
        }

        public override ScriptedAnimationData GetTreeElementListWrapper()
        {
            return data;
        }

        public override void SetData(ScriptedAnimationData data)
        {
            this.data = data;
        }
    }
}

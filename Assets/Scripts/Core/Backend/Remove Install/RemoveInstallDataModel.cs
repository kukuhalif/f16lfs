using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class RemoveInstallFigure : Figure
    {
        public List<GameObjectType> targets;

        public RemoveInstallFigure() : base()
        {
            targets = new List<GameObjectType>();
        }
    }

    [System.Serializable]
    public class RemoveInstallDataModel
    {
        public string title;
        public string removeDescription;
        public string installDescription;
        public RemoveInstallFigure figure;

        public RemoveInstallDataModel()
        {
            title = "new remove install";
            removeDescription = "";
            installDescription = "";
            figure = new RemoveInstallFigure();
        }
    }
}

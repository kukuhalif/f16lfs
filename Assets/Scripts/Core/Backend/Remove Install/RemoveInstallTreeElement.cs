using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class RemoveInstallTreeElement : TreeElement
    {
        [SerializeField] RemoveInstallDataModel removeInstallData;

        public RemoveInstallTreeElement(string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
        {
            removeInstallData = new RemoveInstallDataModel();
        }

        public RemoveInstallDataModel data { get => removeInstallData; }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(removeInstallData);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            removeInstallData = JsonUtility.FromJson<RemoveInstallDataModel>(data) as RemoveInstallDataModel;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class RemoveInstallData : ITreeElementListWrapper
    {
        [SerializeField] public List<RemoveInstallTreeElement> treeElements = new List<RemoveInstallTreeElement>();

        public TreeElement CreateRoot()
        {
            return new RemoveInstallTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    public class RemoveInstallTreeAsset : ScriptableObjectTreeBase<RemoveInstallData>
    {
        [SerializeField] RemoveInstallData removeInstallData;

        public override TreeElement GetTreeElement(int index)
        {
            return removeInstallData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return removeInstallData.treeElements.Count;
        }

        public override RemoveInstallData GetTreeElementListWrapper()
        {
            return removeInstallData;
        }

        public override void SetData(RemoveInstallData data)
        {
            removeInstallData = data;
        }
    }
}

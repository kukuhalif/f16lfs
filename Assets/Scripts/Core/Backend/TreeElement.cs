using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public abstract class TreeElement
    {
        public bool isFolder;
        public int id;
        public string name;
        public int depth;

        [NonSerialized] public TreeElement parent;
        [NonSerialized] public List<TreeElement> children;

        public List<TreeElement> Parents()
        {
            List<TreeElement> parents = new List<TreeElement>();
            TreeElement current = parent;
            while (current != null)
            {
                parents.Add(current);
                current = current.parent;
            }
            return parents;
        }

        public TreeElement Root()
        {
            TreeElement current = parent;
            while (current != null)
            {
                current = current.parent;
            }
            return parent;
        }

        public List<TreeElement> AllChilds()
        {
            List<TreeElement> childs = new List<TreeElement>();
            AllChilds(this, childs);
            return childs;
        }

        private void AllChilds(TreeElement element, List<TreeElement> childs)
        {
            if (element.hasChildren)
                for (int i = 0; i < element.children.Count; i++)
                {
                    childs.Add(element.children[i]);
                    AllChilds(element.children[i], childs);
                }
        }

        public string CompleteName
        {
            get
            {
                if (parent == null)
                    return "";

                List<string> names = new List<string>();

                TreeElement currentParent = this;

                while (currentParent.parent != null)
                {
                    names.Add(currentParent.name);
                    currentParent = currentParent.parent;
                }

                string completeName = "";

                for (int i = names.Count - 1; i >= 0; i--)
                {
                    if (i > 0)
                        completeName += names[i] + "/";
                    else
                        completeName += names[i];
                }

                return completeName;
            }
        }

        public bool hasChildren
        {
            get { return children != null && children.Count > 0; }
        }

        public TreeElement()
        {
        }

        public TreeElement(string name, int depth, int id, bool isfolder)
        {
            this.name = name;
            this.id = id;
            this.depth = depth;
            this.isFolder = isfolder;
        }

        public abstract string GetDuplicateJsonData();

        public abstract void SetDuplicatedJsonData(string data);
    }

}



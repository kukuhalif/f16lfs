using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class FlightSimRecordingItem
    {
        public enum FlightSimRecordingType { Transform, AnimatorParameter, ObjectField }

        public FlightSimRecordingType animationType;
        public GameObjectType animatedObject;
        [HideInInspector] public bool recursive;
        [HideInInspector] public int skipFrames;

        public FlightSimRecordingItem(FlightSimRecordingType animType, GameObjectType animObject)
        {
            animationType = animType;
            animatedObject = animObject;
            recursive = false;
            skipFrames = 10;            
        }

        public FlightSimRecordingItem(object obj) : this()
        {
            animatedObject = new GameObjectType();
            GameObject gameObject = obj as GameObject;
            GameObjectReference gor = gameObject.GetComponent<GameObjectReference>();
            if (gor != null)
                animatedObject.Id = gor.Id;
        }

        public FlightSimRecordingItem()
        {
            animationType = FlightSimRecordingType.Transform;
            animatedObject = new GameObjectType();
            recursive = false;
            skipFrames = 10;
        }

    }
}
using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class FlightScenarioTimelineTreeElement : TreeElement
    {
        [SerializeField] FlightScenarioTimelineDataModel flightScenarioTimelineData;

        public FlightScenarioTimelineDataModel data { get => flightScenarioTimelineData; }

        public FlightScenarioTimelineTreeElement(string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
        {
            flightScenarioTimelineData = new FlightScenarioTimelineDataModel();
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(data);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            flightScenarioTimelineData = JsonUtility.FromJson<FlightScenarioTimelineDataModel>(data);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class TrackBinding
    {
        public TrackAsset track;
        public Object bindingObject;
        public GameObjectType gameObjectTypeBinding;
    }

    [System.Serializable]
    public class FlightScenarioTimelineDataModel
    {
        public string title;
        public string description;
        public FlightSimRecordingData recordedFlightData;
        public TimelineAsset scenarioTimeline;
        public List<TrackBinding> trackBindings;

        public FlightScenarioTimelineDataModel()
        {
            title = "new flight scenario timeline";
            description = "";
            trackBindings = new List<TrackBinding>();
        }
    }
}
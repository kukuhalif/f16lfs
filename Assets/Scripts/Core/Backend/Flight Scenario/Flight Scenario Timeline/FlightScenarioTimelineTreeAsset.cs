using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class FlightScenarioTimelineData : ITreeElementListWrapper
    {
        [SerializeField] public List<FlightScenarioTimelineTreeElement> treeElements = new List<FlightScenarioTimelineTreeElement>();

        public TreeElement CreateRoot()
        {
            return new FlightScenarioTimelineTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "new flight scenario timeline database", menuName = "Virtual Training/Flight Scenario/Flight Scenario Timeline Database")]
    public class FlightScenarioTimelineTreeAsset : ScriptableObjectTreeBase<FlightScenarioTimelineData>
    {
        [SerializeField] FlightScenarioTimelineData flightScenarioTimelineData;

        public override TreeElement GetTreeElement(int index)
        {
            return flightScenarioTimelineData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return flightScenarioTimelineData.treeElements.Count;
        }

        public override FlightScenarioTimelineData GetTreeElementListWrapper()
        {
            return flightScenarioTimelineData;
        }

        public override void SetData(FlightScenarioTimelineData data)
        {
            flightScenarioTimelineData = data;
        }
    }
}

using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class FlightScenarioTreeElement : TreeElement
    {
        [SerializeField] FlightScenarioDataModel flightScenarioData;

        public FlightScenarioDataModel data { get => flightScenarioData; }

        public FlightScenarioTreeElement(string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
        {
            flightScenarioData = new FlightScenarioDataModel();
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(data);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            flightScenarioData = JsonUtility.FromJson<FlightScenarioDataModel>(data);
        }
    }
}

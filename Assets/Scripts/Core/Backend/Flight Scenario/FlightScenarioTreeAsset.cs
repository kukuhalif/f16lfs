using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class FlightScenarioData : ITreeElementListWrapper
    {
        [SerializeField] public List<FlightScenarioTreeElement> treeElements = new List<FlightScenarioTreeElement>();

        public TreeElement CreateRoot()
        {
            return new FlightScenarioTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    //[CreateAssetMenu(fileName = "new flight scenario tree database", menuName = "Virtual Training/Flight Scenario/Flight Scenario Tree Database")]
    public class FlightScenarioTreeAsset : ScriptableObjectTreeBase<FlightScenarioData>
    {
        [SerializeField] FlightScenarioData flightScenarioData;

        public override TreeElement GetTreeElement(int index)
        {
            return flightScenarioData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return flightScenarioData.treeElements.Count;
        }

        public override FlightScenarioData GetTreeElementListWrapper()
        {
            return flightScenarioData;
        }

        public override void SetData(FlightScenarioData data)
        {
            flightScenarioData = data;
        }
    }
}

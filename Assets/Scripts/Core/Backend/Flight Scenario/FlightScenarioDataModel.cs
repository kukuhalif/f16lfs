using System.Collections;
using System.Collections.Generic;
using UnityEngine.Timeline;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class FlightScenarioFigure : Figure
    {
        public FlightScenarioElementType cameraOverrideSource;
        public int cameraOverrideSourceFigureIndex;

        public FlightScenarioElementType partObjectOverrideSource;
        public int partObjectOverrideSourceFigureIndex;

        public FlightScenarioFigure() : base()
        {
            cameraOverrideSource = new FlightScenarioElementType();
            cameraOverrideSourceFigureIndex = 0;

            partObjectOverrideSource = new FlightScenarioElementType();
            partObjectOverrideSourceFigureIndex = 0;
        }
    }

    [System.Serializable]
    public class FlightScenarioDataModel
    {
        public string title;
        public string description;
        public List<FlightScenarioFigure> figures;

        //public FlightSimRecordingData recordedFlightData;
        //public TimelineAsset scenarioTimeline;
        public FlightScenarioTimelineElementType flightScenarioTimelineElmtType;
        public int markerNum;

        public FlightScenarioDataModel()
        {
            title = "new flight scenario step";
            description = "";
            figures = new List<FlightScenarioFigure>
            {
                new FlightScenarioFigure()
            };
            flightScenarioTimelineElmtType = new FlightScenarioTimelineElementType();
            markerNum = 0;
        }
    }
}

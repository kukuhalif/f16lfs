using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Timeline;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class ClipData
    {
        public string clipName;
        public GameObjectType clipObject;
        public FlightSimRecordingItem.FlightSimRecordingType clipType;
        public AnimationClip clipAnim;

        public ClipData()
        {
            clipName = string.Empty;
            clipObject = null;
            clipType = FlightSimRecordingItem.FlightSimRecordingType.Transform;
            clipAnim = null;
        }

        public ClipData(string clipName, GameObjectType clipObject, FlightSimRecordingItem.FlightSimRecordingType clipType, AnimationClip clipAnim)
        {
            this.clipName = clipName;
            this.clipObject = clipObject;
            this.clipType = clipType;
            this.clipAnim = clipAnim;
        }
    }

    [System.Serializable]
    public enum AircraftType { Helicopter, Airplane };
    [System.Serializable]
    public enum AircraftMode { Cold, Hot, Flying };

    [System.Serializable]
    public class FlightSimRecordingData : ScriptableObject
    {
        // recording data
        public string recordingName;
        //public string clipPrefix;
        public List<ClipData> clipDatas;
        public TimelineAsset timeline;
        public TimelineAsset scenarioTimeline;

        // aircraft init data
        public AircraftType aircraftType;
        public AircraftMode aircraftMode;
        public Vector2 aircraftLocation;
        public float aircraftAltitude;
        public float aircraftSpeed;
        public float aircraftVerticalSpeed;
        public float aircraftHeading;

        // environment data
        public float environmentTime; // Time value: from 00:00 to 24:00
        public float environmentCloud; // Cloud thickness value: from 0 (clear) to 1 (cloudy)
        public bool cityLightsStatus;

        FlightSimRecordingData() {
            recordingName = "recording";
            //clipPrefix = "recording";
            clipDatas = new List<ClipData>();
            aircraftType = AircraftType.Helicopter;
            aircraftMode = AircraftMode.Hot;
            aircraftLocation = new Vector2(0f, 0f);
            aircraftAltitude = 0f;
            aircraftSpeed = 100f;
            aircraftVerticalSpeed = 10f;
            aircraftHeading = 0f;
            environmentTime = 12f;
            environmentCloud = 0.5f;
            cityLightsStatus = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class TroubleshootFigure : Figure
    {
        public TroubleshootNodeType cameraOverrideSource;
        public int cameraOverrideSourceFigureIndex;

        public TroubleshootNodeType partObjectOverrideSource;
        public int partObjectOverrideSourceFigureIndex;

        public TroubleshootFigure() : base()
        {
            cameraOverrideSource = new TroubleshootNodeType();
            cameraOverrideSourceFigureIndex = 0;

            partObjectOverrideSource = new TroubleshootNodeType();
            partObjectOverrideSourceFigureIndex = 0;
        }
    }

    [Serializable]
    public class TroubleshootDataModel
    {
        public string title;
        public string text;
        public List<TroubleshootFigure> figures;
        public GameObject prefabCustomButton;
        public string description;
        public bool disableEnvironment;

        public GraphStartPosition startPositionGraph;
        public float sizeMultiplierX;
        public float sizeMultiplierY;
        public bool isSimplifiedMode;
        public int indexNode;

        public float offsetX;
        public float offsetY;


        public TroubleshootDataModel()
        {
            title = "New Troubleshoot Node";
            sizeMultiplierY = 1;
            sizeMultiplierX = 1;
            offsetX = 0;
            offsetY = 0;

            figures = new List<TroubleshootFigure>();
            figures.Add(new TroubleshootFigure());
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    public enum GraphStartPosition
    {
        Up,
        Down,
        Left,
        Right,
    }

    [System.Serializable]
    public class ContentTroubleshootNodeGraph
    {
        [FormerlySerializedAs("troubleshootdata")]
        [SerializeField] public TroubleshootDataModel troubleshootData;
        [SerializeField] public GraphStartPosition startPositionGraph;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class CullingData : ITreeElementListWrapper
    {
        [SerializeField] public List<CullingTreeElement> treeElements = new List<CullingTreeElement>();

        public TreeElement CreateRoot()
        {
            return new CullingTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "New Culling Database", menuName = "Virtual Training/FreePlay/Culling Database")]
    public class CullingTreeAsset : ScriptableObjectTreeBase<CullingData>
    {
        [SerializeField] CullingData culling;

        public override TreeElement GetTreeElement(int index)
        {
            return culling.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return culling.treeElements.Count;
        }

        public override CullingData GetTreeElementListWrapper()
        {
            return culling;
        }

        public override void SetData(CullingData data)
        {
            culling = data;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class CullingDataModel
    {
        public List<GameObjectType> covers;
        public List<GameObjectType> culleds;

        public CullingDataModel()
        {
            this.covers = new List<GameObjectType>();
            this.culleds = new List<GameObjectType>();
        }
    }
}

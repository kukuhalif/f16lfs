using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class PartNumberData : ITreeElementListWrapper
    {
        [SerializeField] public List<PartNumberTreeElement> treeElements = new List<PartNumberTreeElement>();

        public TreeElement CreateRoot()
        {
            return new PartNumberTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "New Part Number Database", menuName = "Virtual Training/FreePlay/Part Number Database")]
    public class PartNumberTreeAsset : ScriptableObjectTreeBase<PartNumberData>
    {
        [SerializeField] PartNumberData partNumberData;

        public override TreeElement GetTreeElement(int index)
        {
            return partNumberData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return partNumberData.treeElements.Count;
        }

        public override PartNumberData GetTreeElementListWrapper()
        {
            return partNumberData;
        }

        public override void SetData(PartNumberData data)
        {
            partNumberData = data;
        }
    }
}

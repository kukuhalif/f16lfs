using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class PartNumberDataModel
    {
        public List<GameObjectType> parts;

        public PartNumberDataModel()
        {
            parts = new List<GameObjectType>();
        }
    }
}

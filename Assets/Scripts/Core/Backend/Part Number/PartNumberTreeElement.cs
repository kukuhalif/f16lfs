using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class PartNumberTreeElement : TreeElement
    {
        [SerializeField] private PartNumberDataModel partNumberData;

        public PartNumberDataModel PartNumberData { get => partNumberData; }

        public PartNumberTreeElement(string name, int depth, int id, bool isfolder) : base(name, depth, id, isfolder)
        {
            partNumberData = new PartNumberDataModel();
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(partNumberData);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            partNumberData = JsonUtility.FromJson<PartNumberDataModel>(data) as PartNumberDataModel;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MateriTypeModelVersion
    {
        public SceneMode sceneMode;
        public MateriElementType materiType;
        public List<GameObjectType> enableObjects;
        public List<GameObjectType> disableObjects;

        public MateriTypeModelVersion()
        {
            sceneMode = SceneMode.Desktop;
            materiType = MateriElementType.Materi;
            enableObjects = new List<GameObjectType>();
            disableObjects = new List<GameObjectType>();
        }
    }

    [System.Serializable]
    public class ModelVersion
    {
        public List<MateriTypeModelVersion> materiTypeModelVersions;

        public ModelVersion()
        {
            materiTypeModelVersions = new List<MateriTypeModelVersion>();
        }
    }

    [CreateAssetMenu(fileName = "Model Version", menuName = "Virtual Training/Feature/Model Version")]
    public class ModelVersionDatabase : ScriptableObjectBase<ModelVersion>
    {
        [SerializeField] ModelVersion modelVersion;

        public override ModelVersion GetData()
        {
            return modelVersion;
        }

        public override void SetData(ModelVersion data)
        {
            modelVersion = data;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MaintenanceData : ITreeElementListWrapper
    {
        [SerializeField] public List<MaintenanceTreeElement> treeElements = new List<MaintenanceTreeElement>();

        public TreeElement CreateRoot()
        {
            return new MaintenanceTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    public class MaintenanceTreeAsset : ScriptableObjectTreeBase<MaintenanceData>
    {
        [SerializeField] MaintenanceData maintenanceData;

        public override TreeElement GetTreeElement(int index)
        {
            return maintenanceData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return maintenanceData.treeElements.Count;
        }

        public override MaintenanceData GetTreeElementListWrapper()
        {
            return maintenanceData;
        }

        public override void SetData(MaintenanceData data)
        {
            maintenanceData = data;
        }
    }
}

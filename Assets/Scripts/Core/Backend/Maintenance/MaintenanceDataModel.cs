﻿using System.Collections;
using System.Collections.Generic;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MaintenanceFigure : Figure
    {
        public MaintenanceElementType cameraOverrideSource;
        public int cameraOverrideSourceFigureIndex;

        public MaintenanceElementType partObjectOverrideSource;
        public int partObjectOverrideSourceFigureIndex;

        public MaintenanceFigure() : base()
        {
            cameraOverrideSource = new MaintenanceElementType();
            cameraOverrideSourceFigureIndex = 0;

            partObjectOverrideSource = new MaintenanceElementType();
            partObjectOverrideSourceFigureIndex = 0;
        }
    }

    [System.Serializable]
    public class MaintenanceDataModel
    {
        public string title;
        public string description;
        public bool disableEnvironment;
        public List<MaintenanceFigure> figures;

        public MaintenanceDataModel()
        {
            title = "New Maintenace Data";
            description = "";
            disableEnvironment = false;

            figures = new List<MaintenanceFigure>();
            figures.Add(new MaintenanceFigure());
        }
    }
}

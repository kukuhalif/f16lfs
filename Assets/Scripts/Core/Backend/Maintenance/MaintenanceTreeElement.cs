﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public class MaintenanceTreeElement : TreeElement
    {
        [SerializeField] MaintenanceDataModel maintenanceData;

        public MaintenanceDataModel data { get => maintenanceData; }

        public MaintenanceTreeElement(string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
        {
            maintenanceData = new MaintenanceDataModel();
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(data);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            maintenanceData = JsonUtility.FromJson<MaintenanceDataModel>(data) as MaintenanceDataModel;
        }
    }
}

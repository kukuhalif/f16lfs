﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Core
{

    [System.Serializable]
    public class ObjectListData : ITreeElementListWrapper
    {
        [SerializeField] public List<ObjectListTreeElement> treeElements = new List<ObjectListTreeElement>();

        public TreeElement CreateRoot()
        {
            return new ObjectListTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "New Object List Database", menuName = "Virtual Training/FreePlay/Object List Database")]
    public class ObjectListTreeAsset : ScriptableObjectTreeBase<ObjectListData>
    {
        [SerializeField] ObjectListData objectlistData;

        public override TreeElement GetTreeElement(int index)
        {
            return objectlistData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return objectlistData.treeElements.Count;
        }

        public override ObjectListData GetTreeElementListWrapper()
        {
            return objectlistData;
        }

        public override void SetData(ObjectListData data)
        {
            objectlistData = data;
        }
    }
}


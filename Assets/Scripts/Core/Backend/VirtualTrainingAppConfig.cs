using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Video;

namespace VirtualTraining.Core
{
    [Serializable]
    public class SFXData
    {
        public SFX sfx;
        public AudioClip clip;

        public SFXData(object obj)
        {
            AudioClip clip = obj as AudioClip;
            this.clip = clip;
        }

        public SFXData()
        {

        }
    }

    [Serializable]
    public class CursorTexture
    {
        public Texture2D texture;
        public Vector2 hotspot;

        public CursorTexture()
        {
            hotspot = new Vector2();
        }

        public CursorTexture(object obj) : this()
        {
            Texture2D tex = obj as Texture2D;
            texture = tex;
        }
    }

    [Serializable]
    public class AnimatedCursor
    {
        public int fps;
        public List<CursorTexture> frames;

        public AnimatedCursor()
        {
            fps = 5;
            frames = new List<CursorTexture>();
        }
    }

    [Serializable]
    public class UITexture
    {
        public Sprite play;
        public Sprite pause;
        public Sprite minimizePanel;
        public Sprite windowedPanel;
        public Sprite maximizePanel;
        public Sprite closePanel;
        public Sprite dropdownArrow;
        public Sprite checkmark;
    }

    [Serializable]
    public class UIInteractionTheme
    {
        public Color backgroundColor;
        public Color contentColor;
    }

    [Serializable]
    public class ButtonColorTheme
    {
        public UIInteractionTheme normal;
        public UIInteractionTheme hover;
        public UIInteractionTheme pressed;

        public ButtonColorTheme()
        {
            normal = new UIInteractionTheme();
            hover = new UIInteractionTheme();
            pressed = new UIInteractionTheme();
        }
    }

    [Serializable]
    public class ToggleColorTheme
    {
        public UIInteractionTheme normalOff;
        public UIInteractionTheme hoverOff;
        public UIInteractionTheme pressedOff;

        public UIInteractionTheme normalOn;
        public UIInteractionTheme hoverOn;
        public UIInteractionTheme pressedOn;

        public ToggleColorTheme()
        {
            normalOff = new UIInteractionTheme();
            hoverOff = new UIInteractionTheme();
            pressedOff = new UIInteractionTheme();

            normalOn = new UIInteractionTheme();
            hoverOn = new UIInteractionTheme();
            pressedOn = new UIInteractionTheme();
        }
    }

    [Serializable]
    public class QuizUiTheme
    {
        public Color historyAnswer;
        public Color historyTrueAnswer;

        public Sprite defaultQuestionButtonIcon;
        public Sprite answeredQuestionButtonIcon;

        public ButtonColorTheme defaultQuestionButton;
        public ButtonColorTheme answeredQuestionButton;
    }

    [Serializable]
    public class HoverCoverTheme
    {
        public Color underlineColor;
        public Sprite underlineSprite;

        public Color color;
        public Sprite bottom;
        public Sprite top;
        public Sprite left;
        public Sprite right;
        public Sprite bottomRight;
    }

    [Serializable]
    public class UITheme
    {
        public string name;

        public Color landingPageBgColor;

        public HoverCoverTheme hoverCoverTheme;

        public ButtonColorTheme button;
        public ButtonColorTheme treeElementFolderButton;
        public ButtonColorTheme uiPanelMinimizeButton;
        public ButtonColorTheme uiPanelRestoreButton;
        public ButtonColorTheme uiPanelCloseButton;

        public ToggleColorTheme toggle;
        public ToggleColorTheme lockPaneltoggle;

        public Color panelTitleColor;
        public Color panelTitleDraggedColor;
        public Color panelContentColor;
        public Color panelTitleTextColor;
        public Color panelTitleTextDraggedColor;

        public Color backgroundColor;

        public Color genericTextColor;

        public Color sliderHandleDefaultColor;
        public Color sliderHandleHighlightedColor;
        public Color sliderHandlePressedColor;
        public Color sliderHandleSelectedColor;
        public Color sliderHandleDisabledColor;
        public Color sliderFillColor;
        public Color sliderBgColor;

        public Color scrollbarHandleDefaultColor;
        public Color scrollbarHandleHighlightedColor;
        public Color scrollbarHandlePressedColor;
        public Color scrollbarHandleSelectedColor;
        public Color scrollbarHandleDisabledColor;
        public Color scrollbarBgColor;

        public Color inputFieldTextNormalColor;
        public Color inputFieldTextHighlightColor;
        public Color inputFieldTextPressedColor;
        public Color inputFieldTextSelectedColor;
        public Color inputFieldTextDisabledColor;
        public Color inputFieldBgColor;

        public Color dropdownTextNormalColor;
        public Color dropdownTextHighlightColor;
        public Color dropdownTextPressedColor;
        public Color dropdownTextSelectedColor;
        public Color dropdownTextDisabledColor;

        public Color TextLinkNormalColor;
        public Color TextLinkHoverColor;
        public Color TextLinkPressedColor;
        public Color TextLinkAfterColor;

        public Color calloutBackgroundColor;
        public Color calloutTextColor;

        public Color tooltipBackgroundColor;
        public Color tooltipTextColor;

        public QuizUiTheme quizUiTheme;

        public float roundedPanelRadius;

        public UITheme()
        {
            button = new ButtonColorTheme();
            uiPanelMinimizeButton = new ButtonColorTheme();
            uiPanelRestoreButton = new ButtonColorTheme();
            uiPanelCloseButton = new ButtonColorTheme();
            quizUiTheme = new QuizUiTheme();
            toggle = new ToggleColorTheme();
            lockPaneltoggle = new ToggleColorTheme();
            hoverCoverTheme = new HoverCoverTheme();
        }
    }

    [Serializable]
    public class Cursor
    {
        public AnimatedCursor link;
        public AnimatedCursor horizontalResize;
        public AnimatedCursor verticalResize;
        public AnimatedCursor slashResize;
        public AnimatedCursor backSlashResize;
        public AnimatedCursor loading;
        public AnimatedCursor vbClick;
        public AnimatedCursor vbPress;
        public AnimatedCursor vbDragHorizontal;
        public AnimatedCursor vbDragVertical;
        public AnimatedCursor vbDragBoth;

        public Cursor()
        {
            link = new AnimatedCursor();
            horizontalResize = new AnimatedCursor();
            verticalResize = new AnimatedCursor();
            slashResize = new AnimatedCursor();
            backSlashResize = new AnimatedCursor();
            loading = new AnimatedCursor();
            vbClick = new AnimatedCursor();
            vbPress = new AnimatedCursor();
            vbDragHorizontal = new AnimatedCursor();
            vbDragVertical = new AnimatedCursor();
            vbDragBoth = new AnimatedCursor();
        }
    }

    [Serializable]
    public class VideoData
    {
        [SerializeField] public VideoClip intro;
        [SerializeField] public List<VideoClip> appsLoadingVideos;
        [SerializeField] public List<VideoClip> quizLoadingVideos;

        public VideoData()
        {
            appsLoadingVideos = new List<VideoClip>();
            quizLoadingVideos = new List<VideoClip>();
        }
    }

    [Serializable]
    public class VRUIData
    {
        [SerializeField] public Vector3 defaultPosition;

        public VRUIData()
        {
            defaultPosition = Vector3.zero;
        }
    }

    [Serializable]
    public class VRData
    {
        [SerializeField] public Vector3 defaultPosition;
        [SerializeField] public Vector3 defaultRotation;

        [SerializeField] public VRUIData systemPosition;
        [SerializeField] public VRUIData confirmationPosition;
        [SerializeField] public VRUIData helpPosition;
        [SerializeField] public VRUIData settingPosition;
        [SerializeField] public VRUIData descriptionPosition;
        [SerializeField] public VRUIData cutawayPosition;
        [SerializeField] public VRUIData videoSchematicPosition;
        [SerializeField] public VRUIData maintenancePosition;
        [SerializeField] public VRUIData troubleshootPosition;
        [SerializeField] public VRUIData monitorCameraPosition;

        public VRData()
        {
            defaultPosition = Vector3.zero;
            defaultRotation = Vector3.zero;
            systemPosition = new VRUIData();
            confirmationPosition = new VRUIData();
            helpPosition = new VRUIData();
            settingPosition = new VRUIData();
            descriptionPosition = new VRUIData();
            cutawayPosition = new VRUIData();
            videoSchematicPosition = new VRUIData();
            maintenancePosition = new VRUIData();
            troubleshootPosition = new VRUIData();
            monitorCameraPosition = new VRUIData();
        }
    }

    [Serializable]
    public class UICOnfig
    {
        [NonSerialized] public int selectedTheme;
        [SerializeField] public UITexture uiTexture;
        [SerializeField] public List<UITheme> uiThemes;
        [SerializeField] public AnimationCurve fadeInCurve;
        [SerializeField] public AnimationCurve fadeOutCurve;
        [SerializeField] public float transitionSpeed;

        public UICOnfig()
        {
            uiThemes = new List<UITheme>();
        }
    }

    [Serializable]
    public class AppConfig
    {
        // audio
        [SerializeField] public List<SFXData> sfxs;

        // ui
        [SerializeField] public UICOnfig uiConfig;

        // cursor
        [SerializeField] public Cursor cursorTexture;

        // video
        [SerializeField] public VideoData video;

        // path config
        [SerializeField] public ContentPathConfig pathConfig;

        // vr
        [SerializeField] public VRData vrData;
    }

    [CreateAssetMenu(fileName = "App Config", menuName = "Virtual Training/Config/App Config File")]
    public class VirtualTrainingAppConfig : ScriptableObjectBase<AppConfig>
    {
        [SerializeField] AppConfig appConfig;

        public override AppConfig GetData()
        {
            return appConfig;
        }

        public override void SetData(AppConfig data)
        {
            appConfig = data;
        }
    }
}
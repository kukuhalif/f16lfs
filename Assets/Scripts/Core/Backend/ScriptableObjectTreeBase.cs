using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public interface ITreeElementListWrapper
    {
        IList GetTreeElements();
        TreeElement CreateRoot();
    }

    public abstract class ScriptableObjectTreeBase<T> : ScriptableObjectBase<T>
        where T : class, ITreeElementListWrapper
    {
        public abstract T GetTreeElementListWrapper();
        public abstract int GetTreeElementCount();
        public abstract TreeElement GetTreeElement(int index);

#if UNITY_EDITOR

        public void RegenerateAllTreeElementId()
        {
            ValidateWrapper();

            int count = GetTreeElementCount();
            for (int i = 0; i < count; i++)
            {
                var oldId = GetTreeElement(i).id;

                int instanceId = GetInstanceID();
                int random = UnityEngine.Random.Range(-10000, 10000);
                string idString = oldId.ToString() + instanceId.ToString() + i.ToString() + random.ToString() + GetTreeElement(i).CompleteName;
                int newId = Animator.StringToHash(idString);

                GetTreeElement(i).id = newId;

                Debug.Log(GetTreeElement(i).name + ", new id : " + newId);
            }
        }

#endif

        public void ValidateWrapper()
        {
            T wrapper = GetTreeElementListWrapper();

            if (wrapper == null)
                wrapper = System.Activator.CreateInstance(typeof(T)) as T;

            if (GetTreeElementCount() == 0)
                wrapper.GetTreeElements().Add(wrapper.CreateRoot());
        }

        public sealed override T GetData()
        {
            ValidateWrapper();

            int count = GetTreeElementCount();
            for (int i = 0; i < count; i++)
            {
                if (GetTreeElement(i).depth == -1)
                {
                    GetTreeElement(i).name = name;
                    break;
                }
            }

            return GetTreeElementListWrapper();
        }

        public sealed override string GetJson()
        {
            ValidateWrapper();

            int count = GetTreeElementCount();
            for (int i = 0; i < count; i++)
            {
                if (GetTreeElement(i).depth == -1)
                {
                    GetTreeElement(i).name = "Root";
                    break;
                }
            }

            return JsonUtility.ToJson(this);
        }
    }
}

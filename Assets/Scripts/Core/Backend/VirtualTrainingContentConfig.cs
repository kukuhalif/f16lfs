using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paroxe.PdfRenderer;
using UnityEditor;

namespace VirtualTraining.Core
{
    [Serializable]
    public class MateriContainer
    {
        [SerializeField] public MateriTreeAsset materi;
        [SerializeField] public List<PDFAsset> pdfs;

        public MateriContainer(object obj) : this()
        {
            MateriTreeAsset asset = obj as MateriTreeAsset;
            materi = asset;
        }

        public MateriContainer()
        {
            pdfs = new List<PDFAsset>();
        }
    }

    [Serializable]
    public class QuizData
    {
        [SerializeField] public int quizAllAmount;
        [SerializeField] public int quizSectionAmount;

        public QuizData()
        {
            quizAllAmount = 50;
            quizSectionAmount = 10;
        }
    }

    [Serializable]
    public class CameraData
    {
        public CameraDestination mainCamera;
        public CameraDestination landingPageCamera;
        public List<CameraPreset> cameraPresets;
        public CameraDestination vrCamera;

        public CameraData()
        {
            mainCamera = new CameraDestination();
            vrCamera = new CameraDestination();
            landingPageCamera = new CameraDestination();
            cameraPresets = new List<CameraPreset>();
        }
    }

    [Serializable]
    public class ContentPathConfig
    {
        [SerializeField] public string contentAssetPath;

        public ContentPathConfig()
        {
            contentAssetPath = "Content Files";
        }
    }

    [Serializable]
    public class CameraPreset
    {
        public string name;
        public CameraDestination destination;

        public CameraPreset()
        {
            name = "new preset";
            destination = new CameraDestination();
        }
    }

    [Serializable]
    public class ProjectDetails
    {
        public string projectDescription;
        public Sprite landingPageIcon;
        public Sprite mainMenuIcon;
        public Sprite aboutIcon;
        public Sprite quizIcon;
        public PDFAsset helpPDF;
        public string about;

        public ProjectDetails()
        {
            projectDescription = "";
            about = "";
        }
    }
    [Serializable]
    public class VRDataContent
    {
        public bool isVREnable;
        public VRDataContent()
        {
            isVREnable = false;
        }
    }


    [Serializable]
    public class CutawayConfig
    {
        [SerializeField] public float lineWidth;
        [SerializeField] public float gizmoSize;
        [SerializeField] public Color lineColor;
        [SerializeField] public Vector3 minPos;
        [SerializeField] public Vector3 maxPos;
        [SerializeField] public Vector3 maxScale;
    }

    [Serializable]
    public class ContentConfig
    {
        // materi database
        [SerializeField] public List<MateriContainer> materiContainers;

        // pdf
        [SerializeField] public List<PDFAsset> pdfUis;
        [SerializeField] public List<PDFAsset> pdfLinks;

        // other database
        [SerializeField] public ObjectListTreeAsset objectListDatabase;
        [SerializeField] public CullingTreeAsset cullingDatabase;
        [SerializeField] public ModelVersionDatabase modelVersionDatabase;
        [SerializeField] public List<VirtualButtonTreeAsset> virtualButtonDatabases;
        [SerializeField] public List<ScriptedAnimationTreeAsset> scriptedAnimationDatabases;
        [SerializeField] public List<PartNumberTreeAsset> partNumberDatabases;
        [SerializeField] public List<FlightScenarioTimelineTreeAsset> flightScenarioTimelineDatabases;

        // material
        [SerializeField] public Color highlightColor;
        [SerializeField] public Color blinkColor;
        [SerializeField] public Color fresnelColor;
        [SerializeField] public List<Material> xRayMaterials;

        // project info
        [SerializeField] public ProjectDetails projectDetails;

        // quiz
        [SerializeField] public QuizData quizData;

        // camera
        [SerializeField] public CameraData cameraData;

        // cutaway
        [SerializeField] public CutawayConfig cutawayConfig;

        // outline
        [SerializeField] public OutlineNormalVector outlineNormalVector;
        [SerializeField] public float outlineWidth;
        [SerializeField] public Color selectedColor;

        // vr
        [SerializeField] public VRDataContent vrDataContent;

        // flight scenario
        /*[SerializeField] public FolderReferenceType flightSimRecordingSaveFolderRef;
        [SerializeField] public string flightSimRecordingSaveFolder = "Assets/Content Files/Recordings";
        [SerializeField] public string flightSimRecordingFilePrefix = "recording";
        [SerializeField] public int flightSimRecordingSkipFrames = 10;
        [SerializeField] public FolderReferenceType flightScenarioSaveFolderRef;
        [SerializeField] public string flightScenarioSaveFolder = "Assets/Content Files/Scenarios";
        [SerializeField] public string flightScenarioFilePrefix = "scenario";
        */

        public ContentConfig()
        {
            materiContainers = new List<MateriContainer>();
            pdfUis = new List<PDFAsset>();
            pdfLinks = new List<PDFAsset>();
            virtualButtonDatabases = new List<VirtualButtonTreeAsset>();
            scriptedAnimationDatabases = new List<ScriptedAnimationTreeAsset>();
            partNumberDatabases = new List<PartNumberTreeAsset>();
        }
    }

    [CreateAssetMenu(fileName = "Content Config", menuName = "Virtual Training/Config/Content Config File")]
    public class VirtualTrainingContentConfig : ScriptableObjectBase<ContentConfig>
    {
        [SerializeField] ContentConfig contentConfig;

        public override ContentConfig GetData()
        {
            return contentConfig;
        }

        public override void SetData(ContentConfig data)
        {
            contentConfig = data;
        }
    }
}
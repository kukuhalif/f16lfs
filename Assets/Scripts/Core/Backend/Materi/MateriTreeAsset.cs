using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MateriData : ITreeElementListWrapper
    {
        [SerializeField] public List<MateriTreeElement> treeElements = new List<MateriTreeElement>();

        public TreeElement CreateRoot()
        {
            return new MateriTreeElement("Root", -1, 0, MateriElementType.Materi, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "New Materi Database", menuName = "Virtual Training/Materi/Materi Database")]
    public class MateriTreeAsset : ScriptableObjectTreeBase<MateriData>
    {
        [SerializeField] MateriData materiData;

        public override TreeElement GetTreeElement(int index)
        {
            return materiData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return materiData.treeElements.Count;
        }

        public override MateriData GetTreeElementListWrapper()
        {
            return materiData;
        }

        public override void SetData(MateriData data)
        {
            materiData = data;
        }
    }
}

using System;
using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public class MateriTreeElement : TreeElement
    {
        [SerializeField] Materi materiData;
        static int index;

        public Materi MateriData { get => materiData; set => materiData = value; }

        public MateriTreeElement(string name, int depth, int id, MateriElementType elementType, bool isFolder) : base(name, depth, id, isFolder)
        {
            materiData = new Materi(name, elementType);
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(MateriData);
        }

        public override void SetDuplicatedJsonData(string data)
        {
#if UNITY_EDITOR
            materiData = JsonUtility.FromJson<Materi>(data) as Materi;

            if (materiData.elementType == MateriElementType.Troubleshoot)
            {
                string oldPath = AssetDatabase.GetAssetPath(materiData.troubleshootData.troubleshootGraph);
                index = 0;
                CheckNameTroubleshoot(materiData.name, oldPath, materiData);
            }
#endif
        }

        void CheckNameTroubleshoot(string name, string oldPath, Materi materi)
        {
#if UNITY_EDITOR
            index++;
            name = name + index;
            if (!File.Exists(Application.dataPath + "/Content Files/Troubleshoot/" + name + ".asset"))
            {
                string newPath = "Assets/Content Files/Troubleshoot/" + name + ".asset";
                AssetDatabase.CopyAsset(oldPath, newPath);
                AssetDatabase.Refresh();
                TroubleshootGraph t = (TroubleshootGraph)AssetDatabase.LoadAssetAtPath(newPath, typeof(TroubleshootGraph));
                materi.troubleshootData.troubleshootGraph = t;
            }
            else
            {
                CheckNameTroubleshoot(name, oldPath, materi);
            }

#endif
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Paroxe.PdfRenderer;
using UnityEngine.Timeline;


namespace VirtualTraining.Core
{
    public enum MateriElementType
    {
        Materi,
        Maintenance,
        Troubleshoot,
        FlightScenario,
        RemoveInstall
    }

    public enum PartObjectAction
    {
        None,
        Solo,
        Disable,
        Highlight,
        Xray,
        Enable,
        Select,
        Blink,
        SetLayer,
        IgnoreCutaway,
        Fresnel,
        Hide
    };

    public enum LayerOption
    {
        MainCamera,
        MonitorCamera
    }

    public enum CutawayState
    {
        None,
        Plane,
        Box,
        Corner
    }

    public enum OverrideVirtualButton
    {
        DefaultValue,
    }

    public enum AnimationType
    {
        AnimManual,
        AnimScript,
        AnimVB
    }

    [Serializable]
    public class PartObject
    {
        public GameObjectType target;
        public PartObjectAction action;

        public bool isRecursive;
        public bool useDefaultColor;
        public Color customColor;

        public bool setHighlightColor;

        public float blinkSpeed;

        public int xrayIndex;
        public LayerOption layerOption;

        public bool fresnelWithBlink;

        public PartObject(object obj) : this()
        {
            target = new GameObjectType();
            GameObject gameObject = obj as GameObject;
            GameObjectReference gor = gameObject.GetComponent<GameObjectReference>();
            if (gor != null)
                target.Id = gor.Id;
        }

        public PartObject()
        {
            target = new GameObjectType();
            action = PartObjectAction.Solo;
            isRecursive = true;
            useDefaultColor = true;
            setHighlightColor = false;
            customColor = Color.white;
            blinkSpeed = 1f;
            xrayIndex = 0;
            layerOption = LayerOption.MainCamera;
        }
    }

    [Serializable]
    public class CameraDestination
    {
        public Vector3 position;
        public Quaternion rotation;
        public bool isOrthographic;
        public bool isRotateFromView;
        public bool isMovementDisabled;
        public bool useParentPositionAndRotation;
        public float orthographicSize;
        public float fov;
        public GameObjectType target;
        public GameObjectType parent;

        public CameraDestination()
        {
            position = Vector3.one;
            rotation = Quaternion.identity;
            isOrthographic = false;
            isRotateFromView = false;
            isMovementDisabled = false;
            useParentPositionAndRotation = false;
            orthographicSize = 5f;
            fov = 50f;
            target = new GameObjectType();
            parent = new GameObjectType();
        }

        public CameraDestination(CameraDestination obj) : this()
        {
            this.position = obj.position;
            this.rotation = obj.rotation;
            this.isOrthographic = obj.isOrthographic;
            this.isRotateFromView = obj.isRotateFromView;
            this.orthographicSize = obj.orthographicSize;
            this.fov = obj.fov;
            if (obj.target != null && obj.target.gameObject != null)
                this.target.Id = obj.target.Id;
        }

        public override string ToString()
        {
            if (target.gameObject == null)
                return "pos : " + position + ", " + (isOrthographic ? ("orthographic, size : " + orthographicSize) : ("perspective, fov : " + fov));

            return "pos : " + target.gameObject.transform.position + ", " + (isOrthographic ? ("orthographic, size : " + orthographicSize) : ("perspective, fov : " + fov));
        }
    }

    public enum DeviceMode
    {
        AllDevice,
        DesktopOnly,
        VrOnly
    }

    [Serializable]
    public class FigureCamera
    {
        public string displayName;
        public CameraDestination destination;
        public DeviceMode mode;

        public FigureCamera()
        {
            displayName = "new figure camera";
            destination = new CameraDestination();
            mode = DeviceMode.AllDevice;
        }

        public FigureCamera(CameraDestination destination)
        {
            displayName = "new figure camera";
            this.destination = destination;
        }
    }

    [Serializable]
    public class MonitorCamera
    {
        public string displayName;
        public CameraDestination destination;

        public MonitorCamera()
        {
            displayName = "new monitor camera";
            destination = new CameraDestination();
        }
    }

    [Serializable]
    public class Schematic
    {
        public VideoClip video;
        public Texture2D image;
        public bool isLooping;
        public bool showImmediately;
        public string descriptionId;

        public Schematic(object param) : this()
        {
            VideoClip vid = param as VideoClip;
            Texture2D tex = param as Texture2D;

            if (vid != null)
                video = vid;
            else if (tex != null)
                image = tex;
        }

        public Schematic()
        {
            isLooping = true;
            showImmediately = false;
            descriptionId = "";
        }

        public Schematic(VideoClip video, Texture2D image, bool isLooping, bool showImmediately, string descriptionId) : this()
        {
            this.video = video;
            this.image = image;
            this.isLooping = isLooping;
            this.showImmediately = showImmediately;
            this.descriptionId = descriptionId;
        }
    }

    [Serializable]
    public class Cutaway
    {
        public CutawayState state;
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;

        public Cutaway()
        {
            state = CutawayState.None;
            position = new Vector3();
            rotation = new Vector3();
            scale = Vector3.one;
        }
    }

    [Serializable]
    public class Figure
    {
        public string name;
        public List<FigureCamera> cameraDestinations;
        public Cutaway cutaway;
        public List<MonitorCamera> monitorCameras;
        public List<PartObject> partObjects;
        public List<GameObject> callouts;
        public List<GameObject> helpers;
        public List<VirtualButtonFigureData> virtualButtonElement;
        public List<AnimationFigureData> animationFigureDatas;
        public bool isSequenceAnimation;
        public bool isShowUiAnimation;
        public bool isReverseAnimationOnReset;
        public AudioClip clip;

        public Figure()
        {
            name = "new figure";
            cutaway = new Cutaway();
            monitorCameras = new List<MonitorCamera>();
            partObjects = new List<PartObject>();
            callouts = new List<GameObject>();
            helpers = new List<GameObject>();
            virtualButtonElement = new List<VirtualButtonFigureData>();
            animationFigureDatas = new List<AnimationFigureData>();
            isSequenceAnimation = false;
            isShowUiAnimation = false;

            cameraDestinations = new List<FigureCamera>();
            cameraDestinations.Add(new FigureCamera()); // default camera destination
            
        }
    }

    [Serializable]
    public class PdfData
    {
        [NonSerialized] public PDFAsset pdfAsset;
        public int index;
        public int page;

        public PdfData()
        {
            index = 0;
            page = 1;
        }
    }

    [Serializable]
    public class TroubleshootData
    {
        public TroubleshootGraph troubleshootGraph;
        public float sizeMultiplierX;
        public float sizeMultiplierY;
        public bool isSimplifiedMode;
        public GraphStartPosition startPositionGraph;
        public string CompleteNameMateri;

        public TroubleshootData()
        {
            sizeMultiplierY = 1;
            sizeMultiplierX = 1;
            isSimplifiedMode = true;
        }
    }

    [Serializable]
    public class Materi
    {
        public string name;
        public MateriElementType elementType;

        public string description;
        public bool disableEnvironment;
        public DeviceMode deviceMode;
        public List<Figure> figures;
        public List<Schematic> schematics;
        public List<PdfData> pdfs;
        public TroubleshootData troubleshootData;
        public MaintenanceTreeAsset maintenanceTree;
        public FlightScenarioTreeAsset flightScenarioTree;
        public RemoveInstallTreeAsset removeInstallTree;

        public Materi(string name, MateriElementType elementType) : this()
        {
            this.name = name;
            this.elementType = elementType;
        }

        public Materi()
        {
            name = "new materi";
            elementType = MateriElementType.Materi;
            description = "";
            disableEnvironment = false;
            schematics = new List<Schematic>();
            pdfs = new List<PdfData>();
            pdfs.Add(new PdfData());
            figures = new List<Figure>();
            figures.Add(new Figure());
            troubleshootData = new TroubleshootData();
        }
    }


    [Serializable]
    public class AnimationFigureData
    {
        public GameObjectType gameObjectInteraction;
        public ScriptedAnimationElementType scriptedAnimationElement;
        public string animationName;
        public string animationLayer;
        public string animationSpeedParameter;
        public AnimatorControllerType animatorController;
        public bool isReverseAnimation;
        public bool isDisabledFinished;
        public bool isEnabledAtStart;
        public bool isAnimatedScript;
        public int sequentialQueue;
        public bool isSequential;
        public bool isInit;
        public bool isVirtualButton;
        public VirtualButtonOnAnimation virtualButton;
        public bool isShowAnimationUI;
        public AnimationType animationType;
        public bool ignoreIfAlreadyPlayed;
        public bool isReverseAnimationOnReset;

        public AnimationFigureData()
        {
            scriptedAnimationElement = new ScriptedAnimationElementType();
            gameObjectInteraction = new GameObjectType();
            animatorController = new AnimatorControllerType();
            virtualButton = new VirtualButtonOnAnimation();
            animationLayer = "Base Layer";
            animationSpeedParameter = "speed";
            isShowAnimationUI = false;
        }

        public AnimationFigureData(object obj) : this()
        {
            GameObject gameObject = obj as GameObject;
            if (gameObject == null)
                return;

            GameObjectReference gor = gameObject.GetComponent<GameObjectReference>();
            if (gor == null)
                return;

            gameObjectInteraction.Id = gor.Id;
            isAnimatedScript = true;
            animationType = AnimationType.AnimScript;
            animationLayer = "Base Layer";
            animationSpeedParameter = "speed";
        }
    }

    [Serializable]
    public class VirtualButtonFigureData
    {
        public VirtualButtonElementType virtualButtonElementType;
        public bool isOverrideVirtualButton;
        public List<OverrideVariableVirtualButton> overrideVirtualButton = new List<OverrideVariableVirtualButton>();

        public VirtualButtonFigureData()
        {
            virtualButtonElementType = new VirtualButtonElementType();
            overrideVirtualButton = new List<OverrideVariableVirtualButton>();
        }
    }

    [Serializable]
    public class OverrideVariableVirtualButton
    {
        public OverrideVirtualButton OverrideVirtualButton;
        public float speed;
        public float defaultValue;
    }

    [Serializable]
    public class VirtualButtonOnAnimation
    {
        public VirtualButtonElementType virtualButtonElementType;
        public float startValue;
        public float endValue;
        public float speedVirtualButton;
        public float startValueY;
        public float endValueY;
        public bool isOverride;

        public VirtualButtonOnAnimation()
        {
            virtualButtonElementType = new VirtualButtonElementType();
            endValue = 1;
            endValueY = 1;
            speedVirtualButton = 1;
        }
    }
}
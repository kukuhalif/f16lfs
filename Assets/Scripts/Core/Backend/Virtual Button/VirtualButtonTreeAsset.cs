﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class VirtualButtonData : ITreeElementListWrapper
    {
        [SerializeField] public List<VirtualButtonTreeElement> treeElements = new List<VirtualButtonTreeElement>();

        public TreeElement CreateRoot()
        {
            return new VirtualButtonTreeElement("Root", -1, 0, true);
        }

        public IList GetTreeElements()
        {
            return treeElements;
        }
    }

    [CreateAssetMenu(fileName = "New Virtual Button Database", menuName = "Virtual Training/Materi/Virtual Button Database")]
    public class VirtualButtonTreeAsset : ScriptableObjectTreeBase<VirtualButtonData>
    {
        [SerializeField] VirtualButtonData virtualbuttonData;

        public override TreeElement GetTreeElement(int index)
        {
            return virtualbuttonData.treeElements[index];
        }

        public override int GetTreeElementCount()
        {
            return virtualbuttonData.treeElements.Count;
        }

        public override VirtualButtonData GetTreeElementListWrapper()
        {
            return virtualbuttonData;
        }

        public override void SetData(VirtualButtonData data)
        {
            virtualbuttonData = data;
        }
    }


}

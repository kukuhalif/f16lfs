﻿
using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
	[Serializable]
	public class VirtualButtonTreeElement : TreeElement
	{
		[SerializeField] VirtualButtonDataModel virtualButtonData;

        public VirtualButtonDataModel VirtualButtonData { get => virtualButtonData; set => virtualButtonData = value; }

		public VirtualButtonTreeElement(string name, int depth, int id, bool isFolder) : base(name, depth, id, isFolder)
		{
			VirtualButtonData = new VirtualButtonDataModel();
		}

		public override string GetDuplicateJsonData()
		{
			return JsonUtility.ToJson(virtualButtonData);
		}

		public override void SetDuplicatedJsonData(string data)
		{
			virtualButtonData = JsonUtility.FromJson<VirtualButtonDataModel>(data) as VirtualButtonDataModel;
		}
	}
}
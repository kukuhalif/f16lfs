﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public abstract class ScriptableObjectBase<T> : ScriptableObject
    {
        public abstract T GetData();
        public abstract void SetData(T data);
        public virtual string GetJson()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
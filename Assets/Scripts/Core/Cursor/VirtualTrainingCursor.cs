using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public class VirtualTrainingCursor : MonoBehaviour
    {
        static VirtualTrainingCursor INSTANCE;

        public delegate void LoadingCursorDelegate(bool loading);
        public static event LoadingCursorDelegate LoadingCursorEvent;

        int index;
        bool lockState;
        AnimatedCursor currentCursor = new AnimatedCursor();

        private void Start()
        {
            INSTANCE = this;
        }

        public static void DefaultCursor()
        {
            if (INSTANCE != null)
                INSTANCE.CancelAnimation();

            UnityEngine.Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            LoadingCursorEvent?.Invoke(false);
        }

        public static void ConfineCursor(bool confine)
        {
            UnityEngine.Cursor.lockState = confine ? CursorLockMode.Confined : CursorLockMode.None;
        }

        private void AnimateCursor()
        {
            UnityEngine.Cursor.SetCursor(currentCursor.frames[index].texture, currentCursor.frames[index].hotspot, CursorMode.Auto);
            index++;
            if (index >= currentCursor.frames.Count)
                index = 0;
        }

        private void CancelAnimation()
        {
            if (gameObject != null)
                CancelInvoke("AnimateCursor");

            lockState = false;
            currentCursor = null;
        }

        private void StartAnimateCursor(AnimatedCursor animatedCursor)
        {
            if (lockState || currentCursor == animatedCursor)
                return;

            index = 0;
            currentCursor = animatedCursor;

            CancelInvoke("AnimateCursor");

            if (currentCursor.frames.Count == 1)
                AnimateCursor();
            else if (currentCursor.frames.Count > 1)
                InvokeRepeating("AnimateCursor", 0f, 1f / (float)currentCursor.fps);
            else
                DefaultCursor();
        }

        public static void Link()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().link);
        }

        public static void HorizontalResize()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().horizontalResize);
        }

        public static void VerticalResize()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().verticalResize);
        }

        public static void TopLeftResize()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().backSlashResize);
        }

        public static void TopRightResize()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().slashResize);
        }

        public static void BottomLeftResize()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().slashResize);
        }

        public static void BottomRightResize()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().backSlashResize);
        }

        public static void Loading()
        {
            if (INSTANCE != null)
            {
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().loading);
                INSTANCE.lockState = true;
                LoadingCursorEvent?.Invoke(true);
            }
        }

        public static void VBClickInteraction()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().vbClick);
        }

        public static void VBPressInteraction()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().vbPress);
        }

        public static void VBHorizontalInteraction()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().vbDragHorizontal);
        }

        public static void VBVerticalInteraction()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().vbDragVertical);
        }

        public static void VBBothInteraction()
        {
            if (INSTANCE != null)
                INSTANCE.StartAnimateCursor(DatabaseManager.GetCursorTexture().vbDragBoth);
        }

    }
}

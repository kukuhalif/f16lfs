﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [DisallowMultipleComponent]
    public class GameObjectReference : MonoBehaviour
    {
        private static Dictionary<int, GameObjectReference> REFERENCE_LOOKUP = new Dictionary<int, GameObjectReference>();
        [SerializeField] private int id;

        public int Id { get => id; }

        public static GameObjectReference GetReference(int id)
        {
            if (REFERENCE_LOOKUP.ContainsKey(id))
                return REFERENCE_LOOKUP[id];
            return null;
        }

        public static void ClearGameobjectReferenceLookup()
        {
            REFERENCE_LOOKUP.Clear();
        }

        public void RegisterObject()
        {
            if (REFERENCE_LOOKUP.ContainsKey(id))
            {
                string path = "";
                var parents = transform.GetAllParents();
                foreach (var parent in parents)
                {
                    path = parent.gameObject.name + "/" + path;
                }
                Debug.LogError("gameobject id already registered : " + path + "/" + gameObject.name + " , id : " + id);
            }
            else
                REFERENCE_LOOKUP.Add(Id, this);
        }

        public void SetId(int id)
        {
            this.id = id;
        }
    }
}
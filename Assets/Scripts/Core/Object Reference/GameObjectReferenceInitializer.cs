using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public class GameObjectReferenceInitializer : MonoBehaviour
    {
        private void Awake()
        {
            EventManager.AddListener<InitializeGameObjectReferenceEvent>(InitializeGameObjectReferenceListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<InitializeGameObjectReferenceEvent>(InitializeGameObjectReferenceListener);
        }

        private void InitializeGameObjectReferenceListener(InitializeGameObjectReferenceEvent e)
        {
            GameObjectReference goRef = GetComponent<GameObjectReference>();
            if (goRef != null)
            {
                goRef.RegisterObject();
            }

            GameObject[] childs = gameObject.GetAllChildsExclude(e.objectRoot);

            for (int i = 0; i < childs.Length; i++)
            {
                GameObjectReference gof = childs[i].gameObject.GetComponent<GameObjectReference>();
                if (gof != null)
                {
                    gof.RegisterObject();
                }
            }
        }
    }
}

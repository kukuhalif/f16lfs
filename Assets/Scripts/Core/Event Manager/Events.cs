﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paroxe.PdfRenderer;
using UnityEngine.Video;

namespace VirtualTraining.Core
{
    /// <summary>
    /// for gameobject reference outside object root
    /// </summary>
    public class InitializeGameObjectReferenceEvent : VirtualTrainingEvent
    {
        public GameObject objectRoot;

        public InitializeGameObjectReferenceEvent(GameObject objectRoot)
        {
            this.objectRoot = objectRoot;
        }
    }

    public class MateriEvent : VirtualTrainingEvent
    {
        public MateriTreeElement materiTreeElement;

        public MateriEvent(MateriTreeElement materiTreeElement)
        {
            this.materiTreeElement = materiTreeElement;
        }
    }

    public class MateriEventIndex : VirtualTrainingEvent
    {
        public int index;

        public MateriEventIndex(int index)
        {
            this.index = index;
        }
    }

    public class CameraArriveEvent : VirtualTrainingEvent
    {

    }

    public class PlayFigureEvent : VirtualTrainingEvent
    {
        public MateriElementType type;
        public int index;

        public PlayFigureEvent(MateriElementType type, int index)
        {
            this.type = type;
            this.index = index;
        }
    }

    public class NextFigureEvent : VirtualTrainingEvent
    {
        public MateriElementType type;

        public NextFigureEvent(MateriElementType type)
        {
            this.type = type;
        }
    }

    public class PrevFigureEvent : VirtualTrainingEvent
    {
        public MateriElementType type;

        public PrevFigureEvent(MateriElementType type)
        {
            this.type = type;
        }
    }

    public class FreePlayEvent : VirtualTrainingEvent
    {
        public bool toLandingPage;

        /// <summary>
        /// reset all materi event, to landing page camera or to default camera position
        /// </summary>
        public FreePlayEvent(bool toLandingPage = false)
        {
            this.toLandingPage = toLandingPage;
        }
    }

    public class UpdateFigurePanelEvent : VirtualTrainingEvent
    {
        public MateriElementType type;
        public string name;
        public int currentFigure;
        public int figureCount;
        public Figure figure;

        public UpdateFigurePanelEvent(string name, int currentFigure, int figureCount, Figure figure, MateriElementType type)
        {
            this.type = type;
            this.name = name;
            this.currentFigure = currentFigure;
            this.figureCount = figureCount;
            this.figure = figure;
        }
    }

    public class UpdateFigureCameraPanelEvent : VirtualTrainingEvent
    {
        public int cameraIndex;
        public MateriElementType type;
        public List<FigureCamera> figureCameras;

        public UpdateFigureCameraPanelEvent(int cameraIndex, List<FigureCamera> figureCameras, MateriElementType type)
        {
            this.cameraIndex = cameraIndex;
            this.type = type;
            this.figureCameras = figureCameras;
        }

        public UpdateFigureCameraPanelEvent()
        {
            this.figureCameras = null;
        }
    }

    public class PartObjectEvent : VirtualTrainingEvent
    {
        public bool fromContentPlayer;
        public List<PartObject> objects;
        public Action[] doneCallbacks;

        public PartObjectEvent(bool fromContentPlayer, List<PartObject> objects, params Action[] doneCallbacks)
        {
            this.fromContentPlayer = fromContentPlayer;
            this.objects = objects;
            this.doneCallbacks = doneCallbacks;
        }
    }

    public class ResetPartObjectEvent : VirtualTrainingEvent
    {

    }

    public class MonitorCameraEvent : VirtualTrainingEvent
    {
        public List<MonitorCamera> monitors;
        public MateriElementType type;

        public MonitorCameraEvent(List<MonitorCamera> monitors, MateriElementType type)
        {
            this.monitors = monitors;
            this.type = type;
        }

        public MonitorCameraEvent()
        {
            monitors = null;
        }
    }

    public class PlayMonitorCameraEvent : VirtualTrainingEvent
    {
        public int index;
        public MonitorCamera destination;

        public PlayMonitorCameraEvent(int index, MonitorCamera destination)
        {
            this.index = index;
            this.destination = destination;
        }
    }

    public class CutawayEvent : VirtualTrainingEvent
    {
        public Cutaway cutaway;

        public CutawayEvent(Cutaway cutaway)
        {
            this.cutaway = cutaway;
        }

        public CutawayEvent()
        {
            cutaway = null;
        }
    }

    public class GetGizmoTransformEvent : VirtualTrainingEvent
    {
        public Action<Transform> getTransformCallback;

        public GetGizmoTransformEvent(Action<Transform> getTransformCallback)
        {
            this.getTransformCallback = getTransformCallback;
        }
    }

    public class InitializeUIEvent : VirtualTrainingEvent
    {

    }

    public class InitializationUIDoneEvent : VirtualTrainingEvent
    {

    }

    public class TroubleshootUIEvent : VirtualTrainingEvent
    {
        public Materi materi;
        public TroubleshootUIEvent(Materi data)
        {
            materi = data;
        }
        public TroubleshootUIEvent()
        {

        }
    }

    public class TroubleshootPlayEvent : VirtualTrainingEvent
    {
        public TroubleshootNode troubleshootNode;

        public TroubleshootPlayEvent(TroubleshootNode troubleshootNode)
        {
            this.troubleshootNode = troubleshootNode;
        }

        public TroubleshootPlayEvent()
        {

        }
    }

    public class MaintenanceUIEvent : VirtualTrainingEvent
    {
        public Materi materi;
        public MaintenanceUIEvent(Materi data)
        {
            materi = data;
        }
        public MaintenanceUIEvent()
        {

        }
    }

    public class MaintenancePlayEvent : VirtualTrainingEvent
    {
        public MaintenanceTreeElement MaintenanceTreeElement;

        public MaintenancePlayEvent(MaintenanceTreeElement maintenanceTreeElement)
        {
            MaintenanceTreeElement = maintenanceTreeElement;
        }

        public MaintenancePlayEvent()
        {

        }
    }

    public class RemoveInstallUIEvent : VirtualTrainingEvent
    {
        public Materi materi;
        public RemoveInstallUIEvent(Materi data)
        {
            materi = data;
        }
        public RemoveInstallUIEvent()
        {

        }
    }

    public class RemoveInstallPlayEvent : VirtualTrainingEvent
    {
        public RemoveInstallTreeElement root;
        public RemoveInstallTreeElement currentElement;
        public bool isRemoving;

        public RemoveInstallPlayEvent(RemoveInstallTreeElement root, RemoveInstallTreeElement currentElement, bool isRemoving)
        {
            this.root = root;
            this.currentElement = currentElement;
            this.isRemoving = isRemoving;
        }

        public RemoveInstallPlayEvent()
        {

        }
    }

    // todo : remove this event, scenario is integrated into materi scene mode
    public class FlightScenarioUIEvent : VirtualTrainingEvent
    {
        public Materi materi;
        public FlightScenarioUIEvent(Materi data)
        {
            materi = data;
        }
        public FlightScenarioUIEvent()
        {

        }
    }

    public class FlightScenarioPreparePlayEvent : VirtualTrainingEvent
    {
        public FlightScenarioPreparePlayEvent()
        {

        }
    }

    public class FlightScenarioPlayEvent : VirtualTrainingEvent
    {
        public FlightScenarioTreeElement flightScenarioTreeElement;

        public FlightScenarioPlayEvent(FlightScenarioTreeElement data)
        {
            flightScenarioTreeElement = data;
        }

        public FlightScenarioPlayEvent()
        {

        }
    }

    public class DescriptionPanelEvent : VirtualTrainingEvent
    {
        public MateriTreeElement materiTreeElement;
        public int figureIndex;
        public int cameraIndex;

        public DescriptionPanelEvent(MateriTreeElement materiTreeElement, int figureIndex, int cameraIndex)
        {
            this.materiTreeElement = materiTreeElement;
            this.figureIndex = figureIndex;
            this.cameraIndex = cameraIndex;
        }
    }

    public class ShowPdfEvent : VirtualTrainingEvent
    {
        public PdfData pdfData;
        public PDFAsset pdfAsset;

        public ShowPdfEvent(PdfData pdfData)
        {
            this.pdfData = pdfData;
        }

        public ShowPdfEvent(PDFAsset pdfAsset)
        {
            this.pdfAsset = pdfAsset;
        }

        public ShowPdfEvent()
        {
            pdfData = null;
            pdfAsset = null;
        }
    }

    public class SetSchematicDataEvent : VirtualTrainingEvent
    {
        public List<Schematic> schematics;

        public SetSchematicDataEvent(List<Schematic> schematics)
        {
            this.schematics = schematics;
        }

        public SetSchematicDataEvent()
        {
            schematics = null;
        }
    }

    public class DisableEnvironmentEvent : VirtualTrainingEvent
    {
        public bool disable;
        public bool reset;

        public DisableEnvironmentEvent(bool disable)
        {
            this.disable = disable;
            this.reset = false;
        }

        public DisableEnvironmentEvent()
        {
            disable = false;
            reset = true;
        }
    }

    public class MonitorCameraInteractionEvent : VirtualTrainingEvent
    {
        public GameObject interactionObject;

        public MonitorCameraInteractionEvent(GameObject interactionObject)
        {
            this.interactionObject = interactionObject;
        }
    }

    public class ResetObjectInteractionModeEvent : VirtualTrainingEvent
    {

    }

    public class ObjectInteractionRotateEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionRotateEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionMultipleSelectionEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionMultipleSelectionEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionPullApartEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionPullApartEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionResetCameraEvent : VirtualTrainingEvent
    {

    }

    public class ObjectInteractionResetObjectEvent : VirtualTrainingEvent
    {

    }

    public class ObjectInteractionLockCameraEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionLockCameraEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionXRayEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionXRayEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionCutawayEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionCutawayEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionVisibleEvent : VirtualTrainingEvent
    {
        public bool visible;

        public ObjectInteractionVisibleEvent(bool visible)
        {
            this.visible = visible;
        }
    }

    public class ObjectInteractionShowObjectListEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionShowObjectListEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ApplyUIThemesEvent : VirtualTrainingEvent
    {

    }

    public class ConfirmationPanelEvent : VirtualTrainingEvent
    {
        public string text;
        public Action onYesCallback;
        public Action onNoCallback;

        public ConfirmationPanelEvent(string text, Action onYesCallback, Action onNoCallback)
        {
            this.text = text;
            this.onYesCallback = onYesCallback;
            this.onNoCallback = onNoCallback;
        }
    }

    public class ApplySettingEvent : VirtualTrainingEvent
    {
        public SettingData data;
        public Vector2 resolution;

        public ApplySettingEvent(SettingData data, Vector2 resolution)
        {
            this.data = data;
            this.resolution = resolution;
        }
    }

    public class ShowLoadingScreenEvent : VirtualTrainingEvent
    {
        public List<VideoClip> clips;
        public Func<float> progress;
        public Func<string> label;
        public Action endLoadingCallback;
        public Action onShowPanelCallback;
        public bool hideUI;

        public ShowLoadingScreenEvent(List<VideoClip> clips, Func<float> progress, Func<string> label, Action onShowPanelCallback, Action endLoadingCallback, bool hideUI)
        {
            this.clips = clips;
            this.progress = progress;
            this.label = label;
            this.endLoadingCallback = endLoadingCallback;
            this.onShowPanelCallback = onShowPanelCallback;
            this.hideUI = hideUI;
        }

        public ShowLoadingScreenEvent(VideoClip clip, Func<float> progress, Func<string> label, Action onShowPanelCallback, Action endLoadingCallback, bool hideUI)
        {
            this.clips = new List<VideoClip>();
            this.clips.Add(clip);
            this.progress = progress;
            this.label = label;
            this.endLoadingCallback = endLoadingCallback;
            this.onShowPanelCallback = onShowPanelCallback;
            this.hideUI = hideUI;
        }
    }

    public class DisableCameraMovementEvent : VirtualTrainingEvent
    {
        public bool disable;

        public DisableCameraMovementEvent(bool disable)
        {
            this.disable = disable;
        }
    }

    public class SwitchCutawayGizmoModeEvent : VirtualTrainingEvent
    {
        // translate or rotate
        public bool translate;

        public SwitchCutawayGizmoModeEvent(bool translate)
        {
            this.translate = translate;
        }
    }

    public class SwitchCutawayModeEvent : VirtualTrainingEvent
    {
        public CutawayState state;

        public SwitchCutawayModeEvent(CutawayState state)
        {
            this.state = state;
        }
    }

    public class ResetCutawayEvent : VirtualTrainingEvent
    {

    }

    public class CursorEnterMonitorPanelEvent : VirtualTrainingEvent
    {
        public bool enter;

        public CursorEnterMonitorPanelEvent(bool enter)
        {
            this.enter = enter;
        }
    }

    public class RaycastObjectFromMonitorCameraEvent : VirtualTrainingEvent
    {
        public GameObject target;

        public RaycastObjectFromMonitorCameraEvent(GameObject target)
        {
            this.target = target;
        }
    }

    public class PlayFigureLinkEvent : VirtualTrainingEvent
    {
        public string name;
        public MateriElementType type;
        public object contentData;

        public PlayFigureLinkEvent(string name, MateriElementType type, object contentData)
        {
            this.name = name;
            this.type = type;
            this.contentData = contentData;
        }
    }

    public class PlayCameraFigureLinkEvent : VirtualTrainingEvent
    {
        public string figureName;
        public string cameraName;
        public MateriElementType type;
        public object contentData;

        public PlayCameraFigureLinkEvent(string figureName, string cameraName, MateriElementType type, object contentData)
        {
            this.figureName = figureName;
            this.cameraName = cameraName;
            this.type = type;
            this.contentData = contentData;
        }
    }

    public class CameraPresetEvent : VirtualTrainingEvent
    {
        public string name;

        public CameraPresetEvent(string name)
        {
            this.name = name;
        }
    }

    public class QuitAppEvent : VirtualTrainingEvent
    {

    }

    public class SceneReadyEvent : VirtualTrainingEvent
    {
        public SceneConfig sceneConfig;

        public SceneReadyEvent(SceneConfig sceneConfig)
        {
            this.sceneConfig = sceneConfig;
        }
    }

    public class CameraChangedEvent : VirtualTrainingEvent
    {

    }

    public class AnimationPlayEvent : VirtualTrainingEvent
    {
        public AnimationFigureData animationData;
        public bool isResetAnimation;

        public AnimationPlayEvent(AnimationFigureData data, bool isResetAnimation = false)
        {
            animationData = data;
            this.isResetAnimation = isResetAnimation;
        }

        public AnimationPlayEvent()
        {
            animationData = null;
        }
    }

    public class CameraControllerReadyEvent : VirtualTrainingEvent
    {

    }

    public class SessionStartedEvent : VirtualTrainingEvent
    {
        public bool isClientOnly;

        public SessionStartedEvent(bool isClientOnly)
        {
            this.isClientOnly = isClientOnly;
        }
    }

    public class OnClientStopEvent : VirtualTrainingEvent
    {

    }

    public class RightHandTriggerEvent : VirtualTrainingEvent
    {
        public bool isClickDown;
        public RightHandTriggerEvent(bool isClickDown)
        {
            this.isClickDown = isClickDown;
        }
    }

    // todo : remove this event, unify all input through virtualtraininginputsystem
    public class RightHandGripEvent : VirtualTrainingEvent
    {
        public GameObject interactionObject;
        public Transform HandVR;
        public RightHandGripEvent()
        {
        }

        public RightHandGripEvent(GameObject interactionObject, Transform HandVR)
        {
            this.interactionObject = interactionObject;
            this.HandVR = HandVR;
        }
    }

    // todo : remove this event, activate ui based on materi type enum instead
    public class FlightMonitorCameraEvent : VirtualTrainingEvent
    {

    }
    public class FlightMonitorCameraCloseEvent : VirtualTrainingEvent
    {

    }

    public class PartNumberInteractionEvent : VirtualTrainingEvent
    {
        public GameObject gameObject;
        public string partNumber;

        public PartNumberInteractionEvent(GameObject gameObject, string partNumber)
        {
            this.gameObject = gameObject;
            this.partNumber = partNumber;
        }
    }

    public class UnloadNetworkingSceneEvent : VirtualTrainingEvent
    {

    }
}
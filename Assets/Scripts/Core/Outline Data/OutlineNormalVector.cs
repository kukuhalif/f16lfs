using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VirtualTraining.Core
{
    [CreateAssetMenu(fileName = "Outline Normal Vector", menuName = "Virtual Training/Feature/Outline Normal Vector")]
    public class OutlineNormalVector : ScriptableObject
    {
        [System.Serializable]
        class MeshNormal
        {
            public Mesh mesh;
            public List<Vector3> smoothNormals;

            public MeshNormal(Mesh mesh, List<Vector3> smoothNormals)
            {
                this.mesh = mesh;
                this.smoothNormals = smoothNormals;
            }
        }

        [SerializeField][HideInInspector] private List<MeshNormal> meshNormals = new List<MeshNormal>();
        Dictionary<Mesh, List<Vector3>> meshNormalLookup;

        public int MeshCount()
        {
            return meshNormals.Count;
        }

        public void Clear()
        {
            meshNormals = new List<MeshNormal>();
        }

        public void InitDataLookup()
        {
            meshNormalLookup = new Dictionary<Mesh, List<Vector3>>();
            for (int i = 0; i < meshNormals.Count; i++)
            {
                if (meshNormals[i].mesh != null && !meshNormalLookup.ContainsKey(meshNormals[i].mesh))
                    meshNormalLookup.Add(meshNormals[i].mesh, meshNormals[i].smoothNormals);

                // assign latest data
                if (meshNormalLookup.ContainsKey(meshNormals[i].mesh))
                    meshNormalLookup[meshNormals[i].mesh] = meshNormals[i].smoothNormals;
            }
        }

        public void SetData(Mesh mesh, List<Vector3> smoothNormals)
        {
            bool alreadyExist = false;
            for (int i = 0; i < meshNormals.Count; i++)
            {
                if (meshNormals[i].mesh == mesh)
                {
                    meshNormals[i].smoothNormals = smoothNormals;
                    alreadyExist = true;
                    break;
                }
            }

            if (!alreadyExist)
                meshNormals.Add(new MeshNormal(mesh, smoothNormals));
        }

        public List<Vector3> GetSmoothNormal(Mesh mesh)
        {
            if (meshNormalLookup.ContainsKey(mesh))
                return meshNormalLookup[mesh];

            return new List<Vector3>();
        }

        public Mesh GetMesh(int index)
        {
            return meshNormals[index].mesh;
        }

        public void RemoveNullMesh()
        {
            meshNormals.RemoveAll(x => x.mesh == null);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(OutlineNormalVector))]
    public class OutlineDataEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            OutlineNormalVector outlineNormalVector = target as OutlineNormalVector;
            GUILayout.Label("mesh data count : " + outlineNormalVector.MeshCount().ToString());

            if (GUILayout.Button("clear"))
            {
                if (EditorUtility.DisplayDialog("outline data", "really ? delete baked mesh normal vector data ?", "yes", "no"))
                {
                    outlineNormalVector.Clear();
                }
            }
        }
    }
#endif
}
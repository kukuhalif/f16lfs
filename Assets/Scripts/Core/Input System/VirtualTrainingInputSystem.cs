﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

namespace VirtualTraining.Core
{
    public class VirtualTrainingInputSystem : MonoBehaviour
    {
        public delegate void OnInputAction(Vector2 pointerPosition, GameObject raycastedUI);
        public delegate void OnInputAxisAction(Vector2 axisValue);

        Vector2 leftClickStartAxisValue;
        bool leftClickDown;
        public static event OnInputAction OnStartLeftClick;
        public static event OnInputAxisAction OnLeftClickDown;
        public static event OnInputAction OnEndLeftClick;

        Vector2 rightClickStartAxisValue;
        bool rightClickDown;
        public static event OnInputAction OnStartRightClick;
        public static event OnInputAxisAction OnRightClickDown;
        public static event OnInputAction OnEndRightClick;

        Vector2 middleClickStartAxisValue;
        bool middleClickDown;
        public static event OnInputAction OnStartMiddleClick;
        public static event OnInputAxisAction OnMiddleClickDown;
        public static event OnInputAction OnEndMiddleClick;

        public delegate void OnMouseScrollAction(float value, GameObject raycastedUI);
        public static event OnMouseScrollAction OnMouseScroll;

        public delegate void OnMoveAction(Vector2 deltaPosition);
        public static event OnMoveAction OnMovePointer;

        public delegate void OnTouched(bool isFingerOverUI);
        public static event OnTouched OnTouchAdded;
        public static event OnTouched OnTouchRemoved;

        public delegate void RaycastedUIDelegate(GameObject raycastedUI);
        public static event RaycastedUIDelegate RaycastedUIUpdated;

        delegate void LocalDelegate();
        static event LocalDelegate RaycastedUIUpdate;

        public static int TouchCount { get => Touch.activeTouches.Count; }
        public static Vector2 GetTouchPosition(int index) { return Touch.activeTouches[index].screenPosition; }
        //public SteamVR_Action_Boolean grabObject;

        int touchCount;
        float deltaX, deltaY;
        List<RaycastResult> results = new List<RaycastResult>();

        private GameObject raycastedUI;

        private void Awake()
        {
            EnhancedTouchSupport.Enable();
        }

        private void Start()
        {
            //grabObject = SteamVR_Input.GetBooleanAction("GrabGrip");
            RaycastedUIUpdate += UpdateRaycastedUIListener;
        }

        private void OnDestroy()
        {
            RaycastedUIUpdate -= UpdateRaycastedUIListener;
        }

        public static Vector2 PointerPosition
        {
            get
            {
                if (Touch.activeTouches.Count > 0)
                    return Touch.activeTouches[0].screenPosition;
                return Input.mousePosition;
            }
        }

        public static void UpdateRaycastedUI()
        {
            RaycastedUIUpdate?.Invoke();
        }

        private void UpdateRaycastedUIListener()
        {
            if (TouchCount == 0)
                IsPointerOverUI();
        }

        private GameObject RaycastedUI
        {
            get => raycastedUI;
            set
            {
                raycastedUI = value;
                RaycastedUIUpdated?.Invoke(raycastedUI);
            }
        }

        private bool IsPointerOverUI()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            if (results.Count > 0)
            {
                RaycastedUI = results[0].gameObject;
            }
            else
                RaycastedUI = null;

            return results.Count > 0;
        }

        private bool IsTouchOverUI(Vector2 screenPosition)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = screenPosition;
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            if (results.Count > 0)
                RaycastedUI = results[0].gameObject;
            else
                RaycastedUI = null;

            return results.Count > 0;
        }

        private void Update()
        {
            IsPointerOverUI();

            if (touchCount < Touch.activeTouches.Count)
                OnTouchAdded?.Invoke(IsTouchOverUI(Touch.activeTouches[Touch.activeTouches.Count - 1].screenPosition));
            else if (touchCount > Touch.activeTouches.Count)
                OnTouchRemoved?.Invoke(false);

            touchCount = Touch.activeTouches.Count;

            // delta pointer
            if (Touch.activeTouches.Count == 0)
            {
                deltaX = Input.GetAxis("Mouse X");
                deltaY = Input.GetAxis("Mouse Y");
                OnMovePointer?.Invoke(new Vector2(deltaX, deltaY));
            }
            else
            {
                OnMovePointer?.Invoke(Touch.activeTouches[0].delta);
            }

            if (Input.GetMouseButtonDown(0)) // left click down
            {
                LeftClickDown();
            }
            else if (Input.GetMouseButtonUp(0)) // left click up
            {
                LeftClickUp();
            }
            else if (Input.GetMouseButtonDown(1)) // right click down
            {
                rightClickStartAxisValue = PointerPosition;
                OnStartRightClick?.Invoke(rightClickStartAxisValue, RaycastedUI);
                rightClickDown = true;
            }
            else if (Input.GetMouseButtonUp(1)) // right click up
            {
                OnEndRightClick?.Invoke(PointerPosition, RaycastedUI);
                rightClickDown = false;
            }
            else if (Input.GetMouseButtonDown(2)) // middle click down
            {
                middleClickStartAxisValue = PointerPosition;
                OnStartMiddleClick?.Invoke(middleClickStartAxisValue, RaycastedUI);
                middleClickDown = true;
            }
            else if (Input.GetMouseButtonUp(2)) // middle click up
            {
                OnEndMiddleClick?.Invoke(PointerPosition, RaycastedUI);
                middleClickDown = false;
            }

            OnMouseScroll?.Invoke(Input.mouseScrollDelta.y, RaycastedUI);

            if (leftClickDown)
                OnLeftClickDown?.Invoke(PointerPosition - leftClickStartAxisValue);

            if (rightClickDown)
                OnRightClickDown?.Invoke(PointerPosition - rightClickStartAxisValue);

            if (middleClickDown)
                OnMiddleClickDown?.Invoke(PointerPosition - middleClickStartAxisValue);

        }

        public void LeftClickDown()
        {
            leftClickStartAxisValue = PointerPosition;
            OnStartLeftClick?.Invoke(leftClickStartAxisValue, RaycastedUI);
            leftClickDown = true;

            if (VirtualTrainingCamera.IsVR)
                EventManager.TriggerEvent(new RightHandTriggerEvent(true));
        }

        public void LeftClickUp()
        {
            OnEndLeftClick?.Invoke(PointerPosition, RaycastedUI);
            leftClickDown = false;

            if (VirtualTrainingCamera.IsVR)
                EventManager.TriggerEvent(new RightHandTriggerEvent(false));
        }

    }
}
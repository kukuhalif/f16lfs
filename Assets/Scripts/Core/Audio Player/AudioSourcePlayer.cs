﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Core
{
    public class AudioSourcePlayer : MonoBehaviour
    {
        [SerializeField] AudioSource sfxAudioSource;
        [SerializeField] AudioSource materiNarationAudioSource;

        private Dictionary<SFX, AudioClip> sfxLookup = new Dictionary<SFX, AudioClip>();

        private void Awake()
        {
            List<SFXData> SFXDatas = DatabaseManager.GetSFXDatas();

            for (int i = 0; i < SFXDatas.Count; i++)
            {
                sfxLookup.Add(SFXDatas[i].sfx, SFXDatas[i].clip);
            }

            AudioPlayer.Initialize(this);
        }

        private void Start()
        {
            var settingData = SettingUtility.LastSetting;

            sfxAudioSource.volume = settingData.sfxVolume;
            materiNarationAudioSource.volume = settingData.voiceVolume;

            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            sfxAudioSource.volume = e.data.sfxVolume;
            materiNarationAudioSource.volume = e.data.voiceVolume;
        }

        public void SetSFXVolume(float volume)
        {
            sfxAudioSource.volume = volume;
        }

        public void PlaySFX(SFX sfx)
        {
            if (!sfxLookup.ContainsKey(sfx))
            {
                Debug.LogError(sfx.ToString() + " sfx clip is null");
                return;
            }

            sfxAudioSource.clip = sfxLookup[sfx];
            sfxAudioSource.Play();
        }

        public void PlayNaration(AudioClip audio)
        {
            materiNarationAudioSource.clip = audio;
            materiNarationAudioSource.Play();
        }

        public void PauseNaration()
        {
            materiNarationAudioSource.Pause();
        }

        public void StopNaration()
        {
            materiNarationAudioSource.Stop();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public abstract class CameraController : MonoBehaviour
    {
        public abstract Camera GetCamera();

        public abstract void ResetCamera(Action arriveAction);

        public abstract void MoveCamera(CameraDestination cameraDestination, Action arriveAction);

        public abstract void MoveCamera(Vector3 position, Quaternion rotation, Transform cameraTarget, bool isOrthographic, bool isRotateFromView, float fov, float orthographicSize, Action arriveAction);

        public abstract bool IsMoving();

        public abstract void AudioListenerSetup();

        public abstract void ForceSetPositionAndRotation(Vector3 position, Vector3 rotation);

        public abstract void SetCameraDataFromServer(Vector3 position, Vector3 rotation, float fov, float orthographicSize);

        public void PauseCameraNoWait()
        {
            StartCoroutine(PauseCameraRoutine());
        }

        public IEnumerator PauseCameraRoutine()
        {
            if (PauseRenderingPanel.INSTANCE != null)
            {
                yield return PauseRenderingPanel.INSTANCE.Enable();
                yield return null;
            }
        }

        public void ResumeCamera()
        {
            if (PauseRenderingPanel.INSTANCE != null)
            {
                PauseRenderingPanel.INSTANCE.Disable();
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public static class VirtualTrainingCamera
    {
        static CameraController CAMERA_CONTROLLER;
        static Camera CURRENT_CAMERA;
        static Camera CURRENT_MONITOR_CAMERA;
        static Action<Vector2> ORBIT_MONITOR_CAMERA;
        static Action CAMERA_ARRIVE_ACTION;
        static bool IS_PULLING;
        static bool IS_MOVEMENT_ENABLED = true;
        static int MAIN_CAMERA_LAYER;
        static int MONITOR_CAMERA_LAYER;
        static bool IS_VR;
        static bool IS_CLICK_VR;
        static CameraDestination DEFAULT_CAMERA;
        static GameObject HAND_VR;
        static float VR_LASER_DISTANCE = 1;
        static Collider CAMERA_COLLIDER;

        public static Camera CurrentCamera { get => CURRENT_CAMERA; }
        public static Camera MonitorCamera { get => CURRENT_MONITOR_CAMERA; }
        public static Transform CameraTransform
        {
            get
            {
                if (CURRENT_CAMERA != null)
                    return CURRENT_CAMERA.transform;
                else
                    return null;
            }
        }
        public static Vector3 CameraPosition { get => CURRENT_CAMERA.transform.position; }
        public static Vector3 CameraRotation { get => CURRENT_CAMERA.transform.rotation.eulerAngles; }
        public static Transform MonitorCameraTransform { get => CURRENT_MONITOR_CAMERA.transform; }
        public static Transform handVR
        {
            //get => HAND_VR.transform;

            get
            {
                if (HAND_VR != null)
                    return HAND_VR.transform;
                else
                    return null;
            }
        }
        public static bool IsVR { get => IS_VR; }
        public static bool IsClickVR { get => IS_CLICK_VR; set => IS_CLICK_VR = value; }
        public static bool IsPulling { get => IS_PULLING; set => IS_PULLING = value; }

        public static CameraController CameraController { get => CAMERA_CONTROLLER; }

        public static float DistanceVR
        {
            get => VR_LASER_DISTANCE;

            set
            {
                if (value >= .3f && value < 3)
                    VR_LASER_DISTANCE = value;
            }
        }
        public static bool IsMoving
        {
            get
            {
                if (CAMERA_CONTROLLER == null)
                    return false;

                return CAMERA_CONTROLLER.IsMoving();
            }
        }

        public static Vector2 PositionOnMonitorCamera;
        public static RectTransform monitorCameraPanel;


        static VirtualTrainingCamera()
        {
            MAIN_CAMERA_LAYER = LayerMask.NameToLayer("Main Camera");
            MONITOR_CAMERA_LAYER = LayerMask.NameToLayer("Monitor Camera");
        }

        public static int MainCameraLayer
        {
            get => MAIN_CAMERA_LAYER;
        }

        public static int MonitorCameraLayer
        {
            get => MONITOR_CAMERA_LAYER;
        }

        public static bool IsMovementEnabled
        {
            set
            {
                IS_MOVEMENT_ENABLED = value;
            }
            get
            {
                return IS_MOVEMENT_ENABLED && !IS_PULLING;
            }
        }

        public static void SetCurrentCameraController(CameraController cameraController, bool isVR, Collider cameraCollider, GameObject handVR = null)
        {
            // setup current data
            IS_VR = isVR;
            CURRENT_CAMERA = cameraController.GetCamera();
            CAMERA_CONTROLLER = cameraController;
            DEFAULT_CAMERA = isVR ? DatabaseManager.GetCameraData().vrCamera : DatabaseManager.GetCameraData().mainCamera;
            CAMERA_COLLIDER = cameraCollider;
            if (isVR)
                HAND_VR = handVR;

            // destroy all audio listener
            var listeners = GameObject.FindObjectsOfType<AudioListener>();
            foreach (var listener in listeners)
            {
                GameObject.Destroy(listener);
            }

            // setup audio listener in camera controller
            CAMERA_CONTROLLER.AudioListenerSetup();
            EventManager.TriggerEvent(new CameraChangedEvent());
        }

        public static void SetCurrentMonitorCamera(Camera camera, Action<Vector2> orbitCameraCallback)
        {
            CURRENT_MONITOR_CAMERA = camera;
            ORBIT_MONITOR_CAMERA = orbitCameraCallback;
        }

        public static void OrbitMonitorCamera(Vector2 pointerDelta)
        {
            if (ORBIT_MONITOR_CAMERA != null)
                ORBIT_MONITOR_CAMERA.Invoke(pointerDelta);
        }

        public static bool Raycast(ref RaycastHit hitInfo, LayerMask layer)
        {
            if (CURRENT_CAMERA == null)
                return false;

            Ray ray;

            if (IS_VR == true && HAND_VR != null)
                ray = new Ray(HAND_VR.transform.position, HAND_VR.transform.forward);
            else
                ray = CURRENT_CAMERA.ScreenPointToRay(VirtualTrainingInputSystem.PointerPosition);

            if (Physics.Raycast(ray, out hitInfo, DatabaseManager.GetMaxRaycastDistance(), layer))
            {
                return true;
            }

            return false;
        }

        public static bool Raycast(ref RaycastHit hitInfo)
        {
            if (CURRENT_CAMERA == null)
                return false;

            Ray ray;

            if (IS_VR == true)
                ray = new Ray(HAND_VR.transform.position, HAND_VR.transform.forward);
            else
                ray = CURRENT_CAMERA.ScreenPointToRay(VirtualTrainingInputSystem.PointerPosition);

            if (Physics.Raycast(ray, out hitInfo, DatabaseManager.GetMaxRaycastDistance()))
            {
                return true;
            }

            return false;
        }

        public static bool RaycastFromMonitorCamera(ref RaycastHit hitInfo, Vector2 cursorPosition, RectTransform panelAreaRect)
        {
            // get pointer position relative to second monitor area ui
            Vector2 result;
            Vector3 normalizedValue;
            monitorCameraPanel = panelAreaRect;
            if (IsVR)
            {
                Vector2 positionVRCamera = new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);
                RectTransformUtility.ScreenPointToLocalPointInRectangle(panelAreaRect, positionVRCamera, handVR.GetComponent<Camera>(), out result);
            }
            else
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(panelAreaRect, cursorPosition, null, out result);
            }
            // position result x and y from -0.5 to 0.5
            normalizedValue = new Vector3(result.x / panelAreaRect.rect.width, result.y / panelAreaRect.rect.height, 0);

            // Viewport coordinates are normalized and relative to the camera. The bottom-left of the camera is (0,0); the top-right is (1,1)
            normalizedValue.x += 0.5f;
            normalizedValue.y += 0.5f;

            if (IsVR)
            {
                PositionOnMonitorCamera = normalizedValue;
            }

            // create ray from monitor camera viewport
            Ray ray = CURRENT_MONITOR_CAMERA.ViewportPointToRay(normalizedValue);

            var hit = Physics.Raycast(ray, out hitInfo);

            if (hit)
            {
                Debug.DrawRay(ray.GetPoint(0), ray.direction * Vector3.Distance(CURRENT_MONITOR_CAMERA.transform.position, hitInfo.point), Color.red, 0.1f);
            }
            else
            {
                Debug.DrawRay(ray.GetPoint(0), ray.direction * 100, Color.blue, 0.1f);
            }

            return hit;
        }

        public static bool RaycastFromMonitorCameraVR(ref RaycastHit hitInfo, RectTransform panelAreaRect, LayerMask layer)
        {
            // raycast from hand to panel
            bool hitPanel = Raycast(ref hitInfo, layer);
            if (!hitPanel)
                return false;

            monitorCameraPanel = panelAreaRect;

            //if (hitInfo.transform != panelAreaRect.transform)
            //    return false;

            // get local hit position on panel
            var screenPos = CurrentCamera.WorldToScreenPoint(hitInfo.point);
            Vector2 localPoint = new Vector2();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(panelAreaRect, screenPos, CurrentCamera, out localPoint);

            // position result x and y from -0.5 to 0.5
            Vector3 normalizedValue = new Vector3(localPoint.x / panelAreaRect.rect.width, localPoint.y / panelAreaRect.rect.height, 0);

            // Viewport coordinates are normalized and relative to the camera. The bottom-left of the camera is (0,0); the top-right is (1,1)
            normalizedValue.x += 0.5f;
            normalizedValue.y += 0.5f;

            // create ray from monitor camera viewport
            Ray ray = CURRENT_MONITOR_CAMERA.ViewportPointToRay(normalizedValue);

            var hit = Physics.Raycast(ray, out hitInfo);

            if (hit)
            {
                Debug.DrawRay(ray.GetPoint(0), ray.direction * Vector3.Distance(CURRENT_MONITOR_CAMERA.transform.position, hitInfo.point), Color.red, 0.1f);
            }
            else
            {
                Debug.DrawRay(ray.GetPoint(0), ray.direction * 100, Color.blue, 0.1f);
            }

            return hit;
        }

        public static void MoveCamera(CameraDestination cameraDestination, Action arriveAction)
        {
            CAMERA_ARRIVE_ACTION = arriveAction;
            if (CAMERA_COLLIDER != null)
                CAMERA_COLLIDER.enabled = false;
            EventManager.TriggerEvent(new DisableCameraMovementEvent(cameraDestination.isMovementDisabled));

            if (CAMERA_CONTROLLER == null)
                ArriveAction();
            else
            {
                // default camera position is 1,1,1 (check camera destination constructor)
                if (cameraDestination.position == Vector3.one)
                    CAMERA_CONTROLLER.MoveCamera(DEFAULT_CAMERA, ArriveAction);
                else
                    CAMERA_CONTROLLER.MoveCamera(cameraDestination, ArriveAction);
            }
        }

        public static void ResetCameraPosition(Action arriveAction)
        {
            CAMERA_ARRIVE_ACTION = arriveAction;
            if (CAMERA_COLLIDER != null)
                CAMERA_COLLIDER.enabled = false;
            EventManager.TriggerEvent(new DisableCameraMovementEvent(false));

            if (CAMERA_CONTROLLER == null)
                ArriveAction();
            else
                CAMERA_CONTROLLER.ResetCamera(ArriveAction);
        }

        private static void ArriveAction()
        {
            CAMERA_ARRIVE_ACTION?.Invoke();
            if (CAMERA_COLLIDER != null)
                CAMERA_COLLIDER.enabled = true;

            EventManager.TriggerEvent(new CameraArriveEvent());
        }

        public static void SetCameraDataFromServer(Vector3 position, Vector3 rotation, float fov, float orthographicSize)
        {
            CAMERA_CONTROLLER.SetCameraDataFromServer(position, rotation, fov, orthographicSize);
        }

        public static IEnumerator PauseCamera()
        {
            if (!IS_VR)
                yield return CAMERA_CONTROLLER.PauseCameraRoutine();
        }
    }
}
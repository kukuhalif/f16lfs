#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Core
{
    [CustomEditor(typeof(PauseRenderingPanel))]
    public class PauseRenderingInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("enable"))
            {
                VirtualTrainingCamera.CameraController.PauseCameraNoWait();
            }

            if (GUILayout.Button("disable"))
            {
                VirtualTrainingCamera.CameraController.ResumeCamera();
            }
        }
    }
}

#endif
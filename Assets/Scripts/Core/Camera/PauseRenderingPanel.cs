using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace VirtualTraining.Core
{
    public class PauseRenderingPanel : MonoBehaviour
    {
        public static PauseRenderingPanel INSTANCE;

        [SerializeField] Transform image;

        RenderTexture screenshotTexture;

        private void Start()
        {
            INSTANCE = this;

            Disable();

            // https://forum.unity.com/threads/resulting-screenshot-image-in-game-is-upside-down-and-not-correctly-positioned.848230/
            var graphicDevice = SystemInfo.graphicsDeviceType;
            var flipY = graphicDevice == GraphicsDeviceType.OpenGLCore ||
                graphicDevice == GraphicsDeviceType.OpenGLES2 ||
                graphicDevice == GraphicsDeviceType.OpenGLES3 ?  // hapus vulkan, karena sekarang vulkan harus di flip
                false :
                true;

            if (flipY)
                image.localScale = new Vector3(1, -1, 1);

            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        public IEnumerator Enable()
        {
            yield return new WaitForEndOfFrame();
            ScreenCapture.CaptureScreenshotIntoRenderTexture(screenshotTexture);
            yield return null;
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            screenshotTexture = new RenderTexture((int)e.resolution.x, (int)e.resolution.y, 24);
            var rawImage = GetComponentInChildren<RawImage>();
            rawImage.texture = screenshotTexture;

            Debug.Log("generate screenshot texture : " + e.resolution);
        }
    }
}
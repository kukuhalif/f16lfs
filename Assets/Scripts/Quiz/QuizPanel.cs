using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class QuizPanel : UIPanel
    {
        [SerializeField] InteractionButton submitButton;
        [SerializeField] InteractionButton exitButton;
        [SerializeField] InteractionButton nextButton;
        [SerializeField] InteractionButton prevButton;

        [SerializeField] TextMeshProUGUI questionNumber;
        [SerializeField] TextMeshProUGUI materiName;
        [SerializeField] TextMeshProUGUI timeRemaining;
        [SerializeField] TextMeshProUGUI questionText;
        [SerializeField] TextMeshProUGUI answerA;
        [SerializeField] TextMeshProUGUI answerB;
        [SerializeField] TextMeshProUGUI answerC;
        [SerializeField] TextMeshProUGUI answerD;
        [SerializeField] List<InteractionToggle> answerToggles = new List<InteractionToggle>();

        [SerializeField] ChooseMateriPanel chooseMateriPanel;
        [SerializeField] QuizResultPanel quizResultPanel;
        [SerializeField] QuizManager quizManager;
        [SerializeField] QuizDatabase quizDatabase;

        [SerializeField] Transform quizButtonParent;
        [SerializeField] QuizQuestionButton quizQuestionButtonTemplate;
        [SerializeField] RectTransform quizQuestionContent;

        List<QuizQuestionButton> quizQuestionButtons = new List<QuizQuestionButton>();
        int currentQuestionNumber;

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            if (questionNumber != null)
                questionNumber.color = theme.genericTextColor;

            materiName.color = theme.genericTextColor;
            timeRemaining.color = theme.genericTextColor;
        }

        protected override void Start()
        {
            base.Start();

            if (submitButton != null)
                submitButton.OnClickEvent += SubmitQuiz;

            if (exitButton != null)
                exitButton.OnClickEvent += ExitQuiz;

            if (nextButton != null)
                nextButton.OnClickEvent += NextQuiz;

            if (prevButton != null)
                prevButton.OnClickEvent += PrevQuiz;

            for (int i = 0; i < answerToggles.Count; i++)
            {
                answerToggles[i].OnStateChangedEvent += SetAnswerToggle;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (submitButton != null)
                submitButton.OnClickEvent -= SubmitQuiz;

            if (exitButton != null)
                exitButton.OnClickEvent -= ExitQuiz;

            if (nextButton != null)
                nextButton.OnClickEvent -= NextQuiz;

            if (prevButton != null)
                prevButton.OnClickEvent -= PrevQuiz;

            for (int i = 0; i < answerToggles.Count; i++)
            {
                answerToggles[i].OnStateChangedEvent -= SetAnswerToggle;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            StartCoroutine(ScrollQuestionPanel());
        }

        void SubmitQuiz()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to submit the answer?", ConfirmationQuizDone, null));
        }

        void ConfirmationQuizDone()
        {
            quizResultPanel.ShowPanel();
            this.ClosePanel();
            quizResultPanel.SetResultQuiz();
            quizManager.EndQuizNew();
            QuizCurrentState.currentQuestionNumber = QuizCurrentState.currentQuestionNumber + 1;
            for (int i = 0; i < answerToggles.Count; i++)
            {
                answerToggles[i].IsOn = false;
            }
        }

        void ExitQuiz()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to exit current session?", ConfirmationExitQuiz, null));
        }

        void ConfirmationExitQuiz()
        {
            chooseMateriPanel.ShowPanel();
            this.ClosePanel();
            QuizCurrentState.currentQuestionNumber = QuizCurrentState.currentQuestionNumber + 1;
            for (int i = 0; i < answerToggles.Count; i++)
            {
                answerToggles[i].IsOn = false;
            }
        }

        void NextQuiz()
        {
            if (QuizCurrentState.maxQuestionNumber - 1 > QuizCurrentState.currentQuestionNumber)
                SetupQuiz(QuizCurrentState.currentQuestionNumber + 1);
        }
        void PrevQuiz()
        {
            if (0 < QuizCurrentState.currentQuestionNumber)
                SetupQuiz(QuizCurrentState.currentQuestionNumber - 1);
        }

        public void SetupQuiz(int value)
        {
            currentQuestionNumber = value;
            ResetQuizAnswer(value);
            QuizCurrentState.currentQuestionNumber = value;
            QuizA quizCurrent = quizManager.selectedQuiz[value] as QuizA;
            questionText.text = quizCurrent.Question;
            answerA.text = "A. " + quizCurrent.AnswerOption[0].Answer;
            answerB.text = "B. " + quizCurrent.AnswerOption[1].Answer;
            answerC.text = "C. " + quizCurrent.AnswerOption[2].Answer;
            answerD.text = "D. " + quizCurrent.AnswerOption[3].Answer;
            int y = value + 1;

            if (questionNumber != null)
                questionNumber.text = "Question " + y + " of " + QuizCurrentState.maxQuestionNumber;

            if (gameObject.activeInHierarchy)
                StartCoroutine(DelayReset());
        }
        IEnumerator DelayReset()
        {
            questionText.gameObject.SetActive(false);
            yield return null;
            questionText.gameObject.SetActive(true);
        }
        void SetAnswerToggle(bool on)
        {
            if (currentQuestionNumber == QuizCurrentState.currentQuestionNumber)
            {
                bool isNotSkip = false;
                QuizA quizCurrent = quizManager.selectedQuiz[QuizCurrentState.currentQuestionNumber] as QuizA;
                for (int i = 0; i < answerToggles.Count; i++)
                {
                    if (answerToggles[i].IsOn)
                    {
                        quizCurrent.AnswerOption[i].Selected = true;
                        if (quizCurrent.AnswerOption[i].IsTrue)
                            quizCurrent.ResultAnswer = true;
                        else
                            quizCurrent.ResultAnswer = false;
                        isNotSkip = true;

                        quizQuestionButtons[currentQuestionNumber].SetAnsweredIcon();
                    }
                    else
                        quizCurrent.AnswerOption[i].Selected = false;
                }

                if (isNotSkip == false)
                {
                    quizQuestionButtons[currentQuestionNumber].SetDefaultIcon();
                    quizCurrent.Skip = true;
                }
                else
                    quizCurrent.Skip = false;
            }
        }

        public void SetupQuizPanel()
        {
            materiName.text = QuizCurrentState.title;
            GenerateQuestionButtons();
        }

        void ResetQuizAnswer(int value)
        {
            int x = int.MaxValue;

            QuizA quizCurrent = quizManager.selectedQuiz[value] as QuizA;
            for (int i = 0; i < quizCurrent.AnswerOption.Count; i++)
            {
                if (quizCurrent.AnswerOption[i].Selected)
                    x = i;
            }

            for (int i = 0; i < answerToggles.Count; i++)
            {
                if (i == x)
                    answerToggles[i].IsOn = true;
                else
                    answerToggles[i].IsOn = false;
            }
        }

        public void DestroyQuestionButtons()
        {
            foreach (var btn in quizQuestionButtons)
            {
                Destroy(btn.gameObject);
            }
        }

        void GenerateQuestionButtons()
        {
            quizQuestionButtonTemplate.gameObject.SetActive(false);

            var allQuiz = quizManager.selectedQuiz;
            for (int i = 0; i < allQuiz.Count; i++)
            {
                var newButton = Instantiate(quizQuestionButtonTemplate.gameObject);
                newButton.transform.SetParent(quizButtonParent);
                newButton.transform.localScale = Vector3.one;
                var qb = newButton.GetComponent<QuizQuestionButton>();
                qb.Setup(i, QuizButtonPressedCallback);

                quizQuestionButtons.Add(qb);
            }
        }

        IEnumerator ScrollQuestionPanel()
        {
            yield return null;

            float value = quizQuestionContent.anchoredPosition.y;
            float speed = value;

            while (value > 0)
            {
                quizQuestionContent.anchoredPosition = new Vector2(0, value);
                value -= Time.deltaTime * speed;

                yield return null;
            }

            quizQuestionContent.anchoredPosition = new Vector2(0, 0);
        }

        void QuizButtonPressedCallback(int quizIndex)
        {
            SetupQuiz(quizIndex);
        }
    }
}

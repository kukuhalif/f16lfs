using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.UI;
using UnityEngine.UI;

namespace VirtualTraining.Quiz
{
    public class QuizBackground : UIElement
    {
        [SerializeField] Image background;

        protected override void ApplyTheme()
        {
            background.color = theme.backgroundColor;
        }
    }
}

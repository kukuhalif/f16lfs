using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.Quiz
{
    public class HistoryResultPanel : UIPanel
    {
        [SerializeField] InteractionButton backButton;
        [SerializeField] GameObject quizHistoryTemplate;
        [SerializeField] Transform quizHistoryParent;
        [SerializeField] ChooseMateriPanel chooseMateriPanel;
        [SerializeField] Scrollbar scrollbar;

        protected override void Start()
        {
            base.Start();
            backButton.OnClickEvent += BackAnswer;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            backButton.OnClickEvent -= BackAnswer;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            StartCoroutine(ToTop());
        }

        IEnumerator ToTop()
        {
            float time = 0f;
            while (time < 1f)
            {
                scrollbar.value = 1f;
                time += Time.deltaTime;
                yield return null;
            }
        }

        public void SetupHistoryPanel()
        {
            EventManager.TriggerEvent(new LoadDataEvent(quizHistoryParent, quizHistoryTemplate));
        }

        void BackAnswer()
        {
            this.ClosePanel(() =>
            {
                chooseMateriPanel.ShowPanel();
            });
        }
    }
}

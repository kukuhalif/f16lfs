using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class OpenResultNew : VirtualTrainingEvent
    {

        public List<string> Result;

        public OpenResultNew(List<string> result)
        {
            Result = result;
        }
    }
    public class HistoryResultButton : UIElementInteraction
    {
        public HistoryResultPanel historyResultPanel;
        public Button resultButton;
        public InteractionButton printButton;

        protected override void Start()
        {
            base.Start();

            resultButton.onClick.AddListener(OpenResultAnswerPanel);
            printButton.OnClickEvent += PrintQuiz;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            resultButton.onClick.RemoveListener(OpenResultAnswerPanel);
            printButton.OnClickEvent -= PrintQuiz;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            var bg = GetComponent<Image>();
            bg.color = theme.button.normal.backgroundColor;

            var texts = transform.GetAllComponentsInChilds<TextMeshProUGUI>();
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].color = theme.button.normal.contentColor;
            }
        }

        void OpenResultAnswerPanel()
        {
            historyResultPanel.ClosePanel();
            LoadTextData load = GetComponent<LoadTextData>();
            EventManager.TriggerEvent(new OpenResultNew(load.Reports));
        }

        void PrintQuiz()
        {
            LoadTextData load = GetComponent<LoadTextData>();
            if (load.resultSnapshotData == null)
                return;

            string resultSnapshotData = load.resultSnapshotData;
            byte[] datas = System.Convert.FromBase64String(resultSnapshotData);

            Texture2D tex = new Texture2D(Screen.width, Screen.height);
            tex.LoadImage(datas);

            //Print.PrintTexture(tex.EncodeToPNG(), 1, "");

            PrinterManager.INSTANCE.Print(tex);
        }

        protected override void OnPointerClick()
        {

        }

        protected override void OnPointerEnter()
        {
            HoverCoverUI.Show(rectTransform, hoverCoverDirection, useUnderline);
        }

        protected override void OnPointerExit()
        {
            HoverCoverUI.Hide();
        }

        protected override Image GetIconImage()
        {
            return null;
        }

        protected override List<Transform> GetAnimatedObject()
        {
            return new List<Transform>();
        }
    }
}


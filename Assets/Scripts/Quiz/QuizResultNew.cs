using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.Quiz
{
    public class QuizResultNew : MonoBehaviour
    {
        public GameObject UIObject;
        [SerializeField]
        private CanvasGroup _BGQuiz;
        Texture2D snapshot;

        public bool isShapshotDone;

        [SerializeField] GameObject[] enabledScreenshot;
        [SerializeField] GameObject[] disabledScreenshot;

        public void InitResult()
        {
            isShapshotDone = false;
            snapshot = null;

            _BGQuiz.DOFade(0, 0.5f);
            //Snapshot();
            UIObject.GetComponent<CanvasGroup>().DOFade(1, 0.5f).OnComplete(Snapshot);
            //UIObject.SetActive(true);
        }
        void Snapshot()
        {
            StartCoroutine(SnapshotAction());
        }

        IEnumerator SnapshotAction()
        {
            for (int i = 0; i < enabledScreenshot.Length; i++)
            {
                enabledScreenshot[i].SetActive(true);
            }

            for (int i = 0; i < disabledScreenshot.Length; i++)
            {
                disabledScreenshot[i].SetActive(false);
            }

            yield return null;
            yield return new WaitForEndOfFrame();

            snapshot = ScreenCapture.CaptureScreenshotAsTexture(4);
            isShapshotDone = true;

            yield return null;

            for (int i = 0; i < enabledScreenshot.Length; i++)
            {
                enabledScreenshot[i].SetActive(false);
            }

            for (int i = 0; i < disabledScreenshot.Length; i++)
            {
                disabledScreenshot[i].SetActive(true);
            }
        }

        public string GetSnapshotData()
        {
            byte[] bytes = snapshot.EncodeToPNG();
            string enc = System.Convert.ToBase64String(bytes);
            return enc;
        }
    }
}


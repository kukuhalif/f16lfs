using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;
using VirtualTraining.UI;

namespace VirtualTraining.Quiz
{
    public class QuizQuestionButton : InteractionButton
    {
        Action<int> pressedCallback;
        int quizIndex;

        ButtonColorTheme currentTheme;

        protected override ButtonColorTheme ButtonColor => currentTheme;

        public void Setup(int quizIndex, Action<int> pressedCallback)
        {
            currentTheme = theme.quizUiTheme.defaultQuestionButton;
            gameObject.SetActive(true);
            SetText((quizIndex + 1).ToString());
            this.quizIndex = quizIndex;
            this.pressedCallback = pressedCallback;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            SetDefaultIcon();
        }

        public void SetDefaultIcon()
        {
            currentTheme = theme.quizUiTheme.defaultQuestionButton;
            SetIcon(theme.quizUiTheme.defaultQuestionButtonIcon);
            ApplyTheme();
        }

        public void SetAnsweredIcon()
        {
            currentTheme = theme.quizUiTheme.answeredQuestionButton;
            SetIcon(theme.quizUiTheme.answeredQuestionButtonIcon);
            ApplyTheme();
        }

        protected override void OnPointerClick()
        {
            base.OnPointerClick();
            pressedCallback.Invoke(quizIndex);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace VirtualTraining.Quiz
{
    public class PrinterManager : MonoBehaviour
    {
        public static PrinterManager INSTANCE;

        private void Start()
        {
            INSTANCE = this;
        }

        public void Print(Texture2D texture)
        {
#if UNITY_EDITOR

            // its not working on il2cpp :(, but works in editor

            string path = Application.persistentDataPath + "/quiz_report.png";
            SaveTextureToFile(texture, path);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.FileName = path;
            process.StartInfo.Verb = "print";
            process.Start();
#else
            StartCoroutine(PrintUsingBat(texture));
#endif
        }

        IEnumerator PrintUsingBat(Texture2D texture)
        {
            string path = Directory.GetCurrentDirectory() + "/quiz_report.png";

            path = path.Replace("/", @"\");

            SaveTextureToFile(texture, path);

            // wait save temp image file saved to disk
            while (!File.Exists(path))
                yield return null;

            // load bat file
            var batPath = Directory.GetCurrentDirectory() + "/print.bat";

            // create bat file if file not created yet
            if (!File.Exists(batPath))
            {
                File.Create(batPath).Close();
                yield return null;
            }

            // set bat content
            var batContent = "mspaint /p #" + path + "#";
            batContent = batContent.Replace('#', '"');

            // write bat content to file
            StreamWriter writer = new StreamWriter(batPath);
            writer.WriteLine("@echo off");
            writer.WriteLine("title Print Images");
            writer.WriteLine(batContent);
            writer.WriteLine(("taskkill /IM #mspaint.exe#").Replace('#', '"'));
            writer.Close();

            yield return null;

            // run bat file
            Application.OpenURL(batPath);
        }

        void SaveTextureToFile(Texture2D texture, string filename)
        {
            //Bitmap bmp = new Bitmap(50, 50);
            //Graphics g = Graphics.FromImage(bmp);

            ////paint the bitmap here with g. …
            ////I just fill a recctangle here
            //g.FillRectangle(Brushes.Green, 0, 0, 50, 50);

            ////dispose and save the file
            //g.Dispose();
            //bmp.Save(@"filepath", System.Drawing.Imaging.ImageFormat.Png);
            //bmp.Dispose();

            File.WriteAllBytes(filename, texture.EncodeToPNG());
        }

        public static void RemoveTempImageResult()
        {
            string[] files;

#if UNITY_EDITOR
            files = Directory.GetFiles(Application.persistentDataPath);
#else
            files = Directory.GetFiles(Directory.GetCurrentDirectory());
#endif

            foreach (var file in files)
            {
                FileInfo file_info = new FileInfo(file);
                if (file_info.Extension == ".png" && file_info.Name.Contains("quiz_report"))
                {
                    Debug.Log(file + " removed");
                    file_info.Delete();
                }
            }

        }
    }
}

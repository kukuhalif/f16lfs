using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using VirtualTraining.SceneManagement;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class LoginPanel : UIPanel
    {
        [SerializeField] ChooseMateriPanel chooseMateriPanel;
        [SerializeField] QuizResultPanel quizResultPanel;

        [SerializeField] InteractionButton startQuizButton;
        [SerializeField] InteractionButton closeLoginButton;
        [SerializeField] InteractionButton yesConfirmButton;
        [SerializeField] InteractionButton noConfirmButton;
        [SerializeField] InteractionButton okConfirmButton;

        [SerializeField] TMP_InputField inputName;
        [SerializeField] TMP_InputField inputID;
        [SerializeField] GameObject confirmationPanel;
        [SerializeField] GameObject warningPanel;

        [SerializeField] Image quizIcon;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();

            if (DatabaseManager.GetProjectDetails().quizIcon != null)
            {
                quizIcon.sprite = DatabaseManager.GetProjectDetails().quizIcon;
                AspectRatioFitter aspectRatioFitter = quizIcon.GetComponent<AspectRatioFitter>();
                aspectRatioFitter.aspectRatio = (float)quizIcon.sprite.texture.width / (float)quizIcon.sprite.texture.height;
            }
            else
                quizIcon.gameObject.SetActive(false);

            startQuizButton.OnClickEvent += StartQuiz;
            closeLoginButton.OnClickEvent += CloseLoginPanel;
            yesConfirmButton.OnClickEvent += ConfirmationYes;
            noConfirmButton.OnClickEvent += ConfirmationNo;
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            startQuizButton.OnClickEvent -= StartQuiz;
            closeLoginButton.OnClickEvent -= CloseLoginPanel;
            yesConfirmButton.OnClickEvent -= ConfirmationYes;
            noConfirmButton.OnClickEvent -= ConfirmationNo;
        }

        void StartQuiz()
        {
            if (string.IsNullOrEmpty(inputID.text) || string.IsNullOrEmpty(inputName.text))
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Name or ID is empty", null, null));
            }
            else
            {
                this.ClosePanel(() =>
                    {
                        chooseMateriPanel.ShowPanel();
                        QuizCurrentState.studentID = inputID.text;
                        QuizCurrentState.name = inputName.text;
                        //chooseMateriPanel.SetName();
                        //quizResultPanel.SetName();
                    });
            }
        }

        void ConfirmationYes()
        {
            VirtualTrainingSceneManager.CloseQuiz();
        }

        void ConfirmationNo()
        {
            confirmationPanel.SetActive(false);
        }

        void CloseLoginPanel()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to exit current session?", ConfirmationYes, null));
        }
    }
}

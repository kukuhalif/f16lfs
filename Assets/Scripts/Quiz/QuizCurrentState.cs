using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Quiz
{
    public static class QuizCurrentState 
    {
        public static string title;
        public static string name;
        public static string studentID;
        public static string date;
        public static float score;
        public static int currentQuestionNumber;
        public static int maxQuestionNumber;
    }
}


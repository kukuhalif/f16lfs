﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using VirtualTraining.Core;

namespace VirtualTraining.Quiz
{
    #region Quiz ans answer abstract class
    [System.Serializable]
    public abstract class Quiz
    {
        public bool Selected;
        public bool ResultAnswer;
        public bool Skip;
        public abstract void ShowQuiz();

        virtual public void SetResultAnswer(bool isTrue)
        {
            ResultAnswer = isTrue;
        }

        virtual public bool GetLastResultQuiz()
        {
            return ResultAnswer;
        }

        public abstract string[] GetReport();

        public abstract void SendResultReport(Answer answerData);

        public abstract void ResetState();
    }

    public abstract class Answer
    {
        public abstract bool CheckAnswer();
        public abstract void ResetState();
    }
    #endregion

    #region Quiz n Answer implement

    [System.Serializable]
    public class QuizA : Quiz
    {
        public string Question;
        public List<OptionalAnswer> AnswerOption;

        public QuizA(string question, List<OptionalAnswer> answerOption)
        {
            Question = question;
            AnswerOption = answerOption;
        }

        public override void SendResultReport(Answer answerData)
        {
            OptionalAnswer AnsweredTemp = (OptionalAnswer)answerData;
            AnsweredTemp.Selected = true;
        }

        public override void ShowQuiz()
        {
            EventManager.TriggerEvent((QuizEvent)(new QuizAEvent(this)));
        }

        public override void ResetState()
        {
            Selected = false;
            for (int i = 0; i < AnswerOption.Count; i++)
            {
                AnswerOption[i].ResetState();
            }
        }

        public override string[] GetReport()
        {
            string answer = "", trueAnswer = "";

            for (int i = 0; i < AnswerOption.Count; i++)
            {
                if (!Skip && AnswerOption[i].Selected)
                    answer = AnswerOption[i].Answer;
                if (AnswerOption[i].IsTrue)
                    trueAnswer = AnswerOption[i].Answer;
            }

            if (answer == trueAnswer)
                return new string[] { "\n\tQuestion : " + Question + "\n\tYour answer : " + answer };

            return new string[] { "\n\tQuestion : " + Question + "\n\tYour answer : " + answer + "\n\tTrue answer : " + trueAnswer };
        }
    }

    [System.Serializable]
    public class QuestionB
    {
        public string Question;
        public List<OptionalAnswer> OptionAnswer;

        public QuestionB(string question, List<OptionalAnswer> optionAnswer)
        {
            Question = question;
            OptionAnswer = optionAnswer;
        }
    }
    [System.Serializable]
    public class QuizB : Quiz
    {
        public List<QuestionB> Question;

        public QuizB(List<QuestionB> question)
        {
            Question = question;
        }

        public override void SendResultReport(Answer answerData)
        {
            OptionalAnswer AnsweredTemp = (OptionalAnswer)answerData;
            AnsweredTemp.Selected = true;
        }

        public override void ShowQuiz()
        {
            EventManager.TriggerEvent((QuizEvent)(new QuizBEvent(this)));
        }

        public override void ResetState()
        {
            Selected = false;
            for (int i = 0; i < Question.Count; i++)
            {
                for (int ii = 0; ii < Question[i].OptionAnswer.Count; ii++)
                {
                    Question[i].OptionAnswer[ii].ResetState();
                }
            }
        }

        public override string[] GetReport()
        {
            List<string> reports = new List<string>();
            for (int i = 0; i < Question.Count; i++)
            {
                string answer = "", trueAnswer = "";
                for (int ii = 0; ii < Question[i].OptionAnswer.Count; ii++)
                {
                    if (Question[i].OptionAnswer[ii].Selected)
                        answer = Question[i].OptionAnswer[ii].Answer;
                    if (Question[i].OptionAnswer[ii].IsTrue)
                        trueAnswer = Question[i].OptionAnswer[ii].Answer;
                }
                if (!Skip && answer == "")
                    answer = Question[i].OptionAnswer[0].Answer;

                if (answer != trueAnswer)
                    reports.Add("\n\titem no " + (i + 1) + " is incorrect \n\t\tQuestion : " + Question[i].Question + "\n\t\tYour answer : " + answer + "\n\t\tTrue answer : " + trueAnswer);
                else
                    reports.Add("\n\titem no " + (i + 1) + " is correct \n\t\tQuestion : " + Question[i].Question + "\n\t\tYour answer : " + answer);
            }
            return reports.ToArray();
        }
    }


    [System.Serializable]
    public class OptionalAnswer : Answer
    {
        public string Answer;
        public bool IsTrue;
        public bool Selected;

        public OptionalAnswer(string answer, bool isTrue)
        {
            Answer = answer;
            IsTrue = isTrue;
        }

        public override bool CheckAnswer()
        {
            return IsTrue;
        }

        public override void ResetState()
        {
            Selected = false;
        }
    }
    #endregion
}
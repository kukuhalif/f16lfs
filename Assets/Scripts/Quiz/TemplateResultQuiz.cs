using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.Quiz
{
    public class TemplateResultQuiz : MonoBehaviour
    {
        public TextMeshProUGUI number;
        public TextMeshProUGUI question;
        public TextMeshProUGUI yourAnswer;
        public TextMeshProUGUI trueAnswer;

        public void SetColor()
        {
            var theme = DatabaseManager.GetUiTheme();
            yourAnswer.color = theme.quizUiTheme.historyAnswer;
            trueAnswer.color = theme.quizUiTheme.historyTrueAnswer;
        }

        public void Init(string number, string question, string yourAnswer, string trueAnswer)
        {
            this.number.text = number;
            this.question.text = question;
            this.yourAnswer.text = yourAnswer;
            if (!string.IsNullOrEmpty(trueAnswer))
            {
                this.trueAnswer.gameObject.SetActive(true);
                this.trueAnswer.text = trueAnswer;
            }
            else
                this.trueAnswer.gameObject.SetActive(false);
        }
    }
}


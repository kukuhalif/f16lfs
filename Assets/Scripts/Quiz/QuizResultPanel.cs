using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class QuizResultPanel : UIPanel
    {
        [SerializeField] InteractionButton SeeAnswerButton;
        [SerializeField] InteractionButton DoneButton;
        [SerializeField] TextMeshProUGUI textName;
        [SerializeField] TextMeshProUGUI textMateri;
        [SerializeField] TextMeshProUGUI textYourScore;
        [SerializeField] TextMeshProUGUI textScore;
        [SerializeField] TextMeshProUGUI textAnswer;
        [SerializeField] QuizAnswerPanel quizAnswerPanel;
        [SerializeField] ChooseMateriPanel chooseMateriPanel;
        [SerializeField] QuizDatabase quizDatabase;
        [SerializeField] QuizManager quizManager;
        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            //textName.color = theme.genericTextColor;
            textMateri.color = theme.genericTextColor;
            textYourScore.color = theme.genericTextColor;
            textScore.color = theme.genericTextColor;
            textAnswer.color = theme.genericTextColor;
        }
        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            SeeAnswerButton.OnClickEvent += SeeAnswer;
            DoneButton.OnClickEvent += DoneResult;

        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            SeeAnswerButton.OnClickEvent -= SeeAnswer;
            DoneButton.OnClickEvent -= DoneResult;
        }

        void SeeAnswer()
        {
            this.ClosePanel(() =>
            {
                //quizAnswerPanel.ShowPanel();
                //quizAnswerPanel.SetupAnswerPanel(quizManager.reportManager.GetReports());
                EventManager.TriggerEvent(new OpenResultNew(quizManager.reportManager.GetReports()));
            });
        }

        void DoneResult()
        {
            this.ClosePanel(() =>
            {
                chooseMateriPanel.ShowPanel();
            });
        }

        public void SetName()
        {
            textName.text = QuizCurrentState.name;
        }

        public void SetResultQuiz()
        {
            textMateri.text = QuizCurrentState.title;
            //textName.text = QuizCurrentState.name;
            float score = (CheckQuizAnswer() / QuizCurrentState.maxQuestionNumber) * 100;
            QuizCurrentState.score = score;
            textScore.text = score.ToString();
            textAnswer.text = "Right Answer : " + CheckQuizAnswer().ToString() + "/" + QuizCurrentState.maxQuestionNumber;
        }

        float CheckQuizAnswer()
        {
            float x = 0;

            //for (int i = 0; i < quizDatabase.quizADatabase.Count; i++)
            //{
            //    for (int j = 0; j < quizDatabase.quizADatabase[i].AnswerOption.Count; j++)
            //    {
            //        if (quizDatabase.quizADatabase[i].AnswerOption[j].IsTrue && quizDatabase.quizADatabase[i].AnswerOption[j].Selected)
            //        {
            //            x++;
            //        }
            //    }
            //}

            for (int i = 0; i < quizManager.selectedQuiz.Count; i++)
            {
                QuizA currentQuiz = quizManager.selectedQuiz[i] as QuizA;
                for (int j = 0; j < currentQuiz.AnswerOption.Count; j++)
                {
                    if (currentQuiz.AnswerOption[j].IsTrue && currentQuiz.AnswerOption[j].Selected)
                    {
                        x++;
                    }
                }
            }
            return x;
        }
    }
}

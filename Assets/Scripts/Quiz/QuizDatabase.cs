using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using TMPro;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Quiz
{
    public class QuizDatabase : MonoBehaviour
    {

        public TMP_Dropdown SelectMateriDropdown;

        public List<string> defaultPath = new List<string>();
        //bikin satu jenis quiz satu list

        [Header("Database Quiz")]
        [SerializeField]
        public List<QuizA> quizADatabase;

        [Header("Initialization Quiz")]
        [SerializeField]
        private int _QuizTypeAmount;

        bool IsNull;
        [SerializeField] bool loadA;
        bool AStilLoad;
        public bool loadDatabaseFinished;
        public int quizA;

        [SerializeField]
        public int allMateriAmount;

        private void Start()
        {
            SetDropdown();
        }


        void WarningLoadQuizDatabase()
        {
            VirtualTrainingSceneManager.CloseQuiz();
        }

        void SetDropdown()
        {
            //string path = System.IO.Path.Combine(Application.streamingAssetsPath, "QuizData/QuizDatabase/");
            string path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), Application.productName, "Quiz/Database/");
            string[] files = Directory.GetFiles(path, "*.fyr");

            SelectMateriDropdown.options.Clear();
            SelectMateriDropdown.options.Add(new TMP_Dropdown.OptionData("All Section"));

            for (int i = 0; i < files.Length; i++)
            {
                SelectMateriDropdown.options.Add(new TMP_Dropdown.OptionData(Path.GetFileNameWithoutExtension(files[i])));
            }
        }

        public void Init()
        {
            SetPath();
            StartCoroutine(ImportDatabase());
        }

        private void SetPath()
        {
            if (SelectMateriDropdown.value == 0)
            {
                for (int i = 1; i < SelectMateriDropdown.options.Count; i++)
                {
                    //defaultPath.Add(System.IO.Path.Combine(Application.streamingAssetsPath, "QuizData/QuizDatabase/" + SelectMateriDropdown.options[i].text + ".fyr"));
                    defaultPath.Add(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments),Application.productName, "Quiz/Database/" + SelectMateriDropdown.options[i].text + ".fyr"));
                }
            }
            else
            {
                //defaultPath.Add(System.IO.Path.Combine(Application.streamingAssetsPath, "QuizData/QuizDatabase/" + SelectMateriDropdown.options[SelectMateriDropdown.value].text + ".fyr"));
                defaultPath.Add(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), Application.productName, "Quiz/Database/" + SelectMateriDropdown.options[SelectMateriDropdown.value].text + ".fyr"));
            }

            for (int i = 0; i < defaultPath.Count; i++)
            {
                defaultPath[i] = defaultPath[i].Replace(@"\", "/");
            }
        }
        List<int> GenerateRandom(int count, int min, int max)
        {
            if (max <= min || count < 0 ||
                    (count > max - min && max - min > 0))
            {
                return new List<int>();
            }

            System.Random random = new System.Random(System.DateTime.Now.Millisecond);

            HashSet<int> candidates = new HashSet<int>();

            for (int top = max - count; top < max; top++)
            {
                if (!candidates.Add(random.Next(min, top + 1)))
                {
                    candidates.Add(top);
                }
            }

            List<int> result = candidates.ToList();
            Debug.Log(random);
            for (int i = result.Count - 1; i > 0; i--)
            {
                int k = random.Next(i + 1);
                int tmp = result[k];
                result[k] = result[i];
                result[i] = tmp;
            }
            return result;
        }

        [SerializeField] List<QuizA> qaz = new List<QuizA>();

        //randon quis dengan panjang yg bisa ditentukan
        public List<Quiz> GetQuizList(ref int quizAmount, bool randomQuiz)
        {
            // all materi
            if (SelectMateriDropdown.value == 0)
                quizAmount = allMateriAmount;
            else // per sistem
            {
                quizAmount = quizA;
            }

            if (IsNull)
                return null;
            List<Quiz> randomResults = new List<Quiz>();

            if (randomQuiz)
            {
                Debug.Log("randomm");
                List<int> randomIndex = GenerateRandom(quizAmount, 0, quizAmount);

                for (int i = 0; i < randomIndex.Count; i++)
                {
                    int quizType = 1;
                    int random = 0;
                    switch (quizType)
                    {
                        case 1: //quiz a
                            if (!loadA)
                                break;
                            random = randomIndex[randomIndex[i]];
                            if (random >= quizADatabase.Count - 1)
                                random = quizADatabase.Count - 1;

                            //if (!_QuizADatabase[random].Selected)
                            //{
                            randomResults.Add(quizADatabase[random]);
                            quizADatabase[random].Selected = true;
                            //}

                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                //if (LoadA && LoadB)
                //    quizAmount = QuizA + QuizB;
                //else if (LoadA)
                //    quizAmount = QuizA;
                //else if (LoadB)
                //    quizAmount = QuizB;

                if (loadA)
                    for (int i = 0; i < quizAmount; i++)
                    {
                        randomResults.Add(quizADatabase[i]);
                    }
            }
            for (int i = 0; i < randomResults.Count; i++)
            {
                qaz.Add((QuizA)randomResults[i]);
            }
            return randomResults;
        }

        public void ResetDatabase()
        {
            qaz.Clear();
            defaultPath.Clear();
            quizADatabase.Clear();
        }

        #region import database
        IEnumerator ImportDatabase()
        {
            loadDatabaseFinished = false;

            for (int i = 0; i < defaultPath.Count; i++)
            {
                if (loadA)
                {
                    if (File.Exists(defaultPath[i]))
                    {
                        AStilLoad = true;
                        StartCoroutine(ImportDatabase(defaultPath[i], quizADatabase, true));
                    }
                    else
                    {
                        EventManager.TriggerEvent(new PopUpQuizEvent(-1, "Database Quiz NOT FOUND !!"));
                        IsNull = true;
                    }
                }

                yield return null;
            }
            while ((loadA && AStilLoad))
                yield return null;

            loadDatabaseFinished = true;
        }

        IEnumerator ImportDatabase<T>(string path, List<T> quizDatas, bool a) where T : Quiz
        {
            string[] datas = File.ReadAllLines(path);
            for (int i = 0; i < datas.Length; i++)
            {
                quizDatas.Add(JsonUtility.FromJson<T>(EncryptionManager.Decrypt(datas[i])));
                if (i % 2 == 0)
                    yield return null;
            }
            if (a)
                AStilLoad = false;
        }

        #endregion
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Quiz
{

    public class HistoryManager : MonoBehaviour
    {
        [SerializeField]
        private Transform _HistoryQuizParent;

        [SerializeField]
        private GameObject _HistoryQuiz;

        public void OpenHistory()
        {
            LoadFromDatabase();
        }
        void LoadFromDatabase()
        {
            EventManager.TriggerEvent(new LoadDataEvent(_HistoryQuizParent, _HistoryQuiz));
            LoadTextData.SelectedLoadTextData = null;
        }

        public void OpenHistoryDetail()
        {
            if (LoadTextData.SelectedLoadTextData == null)
                return;

            if (LoadTextData.SelectedLoadTextData.Reports != null && LoadTextData.SelectedLoadTextData.AnswerStatus != null)
                EventManager.TriggerEvent(new ShowDetailHistoryEvent(LoadTextData.SelectedLoadTextData.Reports, LoadTextData.SelectedLoadTextData.AnswerStatus));
        }

        public void PrintReport()
        {
            if (LoadTextData.SelectedLoadTextData == null)
                return;

            string resultSnapshotData = LoadTextData.SelectedLoadTextData.resultSnapshotData;

            byte[] datas = System.Convert.FromBase64String(resultSnapshotData);

            Texture2D tex = new Texture2D(Screen.width, Screen.height);
            tex.LoadImage(datas);

            //Print.PrintTexture(tex.EncodeToPNG(), 1, "");

            PrinterManager.INSTANCE.Print(tex);

            //Text[] dataTexts = LoadTextData.SelectedLoadTextData.DataText;
            //string reportData = "";

            //reportData =
            //    "username : " + dataTexts[0].text + "\n" +
            //    "materi : " + UppercaseWords(dataTexts[1].text) + "\n" +
            //    "student id : " + dataTexts[2].text + "\n" +
            //    "total score : " + dataTexts[5].text + "\n" +
            //    "right answer : " + LoadTextData.SelectedLoadTextData.rightAnswer + "\n" +
            //    "quiz amount : " + LoadTextData.SelectedLoadTextData.quizAmount + "\n" +
            //    "time : " + dataTexts[4].text + "\n" +
            //    "date : " + dataTexts[3].text;


            //QuizResult.PrintReport(reportData);
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.Linq;
using VirtualTraining.Core;

namespace VirtualTraining.Quiz
{
    public static class DataSaveLoad
    {

        static string _DefaultPath = "";

        public static void InitSaveLoadDatabase()
        {
            _DefaultPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), Application.productName + "/Quiz/History");
            Debug.Log(_DefaultPath);
            if (!Directory.Exists(_DefaultPath))
                Directory.CreateDirectory(_DefaultPath);

            //var info = new DirectoryInfo(_DefaultPath);
            //var fileInfo = info.GetFiles("*.fyr", SearchOption.TopDirectoryOnly);
            //Debug.Log(fileInfo.Length);
        }

        public static void SaveResult(ResultQuizData quizData)
        {
            StreamWriter writer = new StreamWriter(_DefaultPath + "/qR_" + (System.DateTime.Now.ToLongTimeString().Replace(":", "_")) + ".fyr");
            Debug.Log(_DefaultPath + "/qR_" + (System.DateTime.Now.ToLongTimeString().Replace(":", "_")) + ".fyr");
            SavedData data = new SavedData(quizData);
            writer.Write(EncryptionManager.Encrypt(JsonUtility.ToJson(data)));
            writer.Close();
        }

        public static FileInfo[] GetHistoryCount()
        {
            var info = new DirectoryInfo(_DefaultPath);
            var files = info.GetFiles("*.fyr", SearchOption.TopDirectoryOnly).OrderBy(p => p.CreationTime).ToArray();

            return files;
        }

        public static SavedData LoadResult(FileInfo fileInfo)
        {
            return JsonUtility.FromJson<SavedData>(EncryptionManager.Decrypt(File.ReadAllText(fileInfo.FullName)));
        }

        public static List<SavedData> LoadResult()
        {
            List<SavedData> databaseSaved = new List<SavedData>();

            var info = new DirectoryInfo(_DefaultPath);
            var files = info.GetFiles("*.fyr", SearchOption.TopDirectoryOnly).OrderBy(p => p.CreationTime).ToArray();

            foreach (var file in files)
            {
                SavedData data = JsonUtility.FromJson<SavedData>(EncryptionManager.Decrypt(File.ReadAllText(file.FullName)));
                databaseSaved.Add(data);
            }
            return databaseSaved;
        }

        [System.Serializable]
        public class SavedData
        {
            public string[] Datas;
            public List<bool> AnswerStatus;
            public List<string> Reports;
            public string resultPictureData;

            public SavedData(ResultQuizData result)
            {
                Datas = new string[6];
                Datas[0] = result.Username;
                Datas[1] = result.MateriName;
                Datas[2] = result.StudentId;
                Datas[3] = result.Date;
                Datas[4] = result.Duration;
                Datas[5] = result.Score;
                AnswerStatus = result.AnswerStatus;
                Reports = result.Reports;
                resultPictureData = result.ReportPicture;
            }
        }
    }
}

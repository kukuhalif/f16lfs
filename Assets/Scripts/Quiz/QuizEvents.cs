﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;


namespace VirtualTraining.Quiz
{
    public abstract class QuizEvent : VirtualTrainingEvent
    {

    }

    public class CloseQuizUI : VirtualTrainingEvent
    {

    }

    public class QuizAEvent : QuizEvent
    {
        public QuizA QuizData;

        public QuizAEvent(QuizA quizData)
        {
            QuizData = quizData;
        }
    }
    public class QuizBEvent : QuizEvent
    {
        //DUMMY QUIZ B
        public QuizB QuizData;

        public QuizBEvent(QuizB quizData)
        {
            QuizData = quizData;
        }
    }
    #region end quiz
    public class EndQuizEvent : VirtualTrainingEvent
    {
        public Quiz QuizData;
        public bool ResultAnswer;

        public EndQuizEvent(Quiz quizData, bool resultAnswer)
        {
            QuizData = quizData;
            ResultAnswer = resultAnswer;
        }
    }
    #endregion

    #region QuizManager
    public class InitQuizManagerEvent : VirtualTrainingEvent
    {

    }
    public class StartQuizEvent : VirtualTrainingEvent
    {

    }
    public class SetUsernameEvent : VirtualTrainingEvent
    {
        public string User;
        public string Id;

        public SetUsernameEvent(string user, string id)
        {
            User = user;
            Id = id;
        }
    }
    #endregion

    #region SaveLoad
    public class SaveDataEvent : VirtualTrainingEvent
    {

        public ResultQuizData Result;

        public SaveDataEvent(ResultQuizData result)
        {
            Result = result;
        }
    }

    public class LoadDataEvent : VirtualTrainingEvent
    {
        public Transform DataParent;
        public GameObject InstantiateObject;

        public LoadDataEvent(Transform parent, GameObject obj)
        {
            DataParent = parent;
            InstantiateObject = obj;
        }
    }

    public class LoadHistoryDataDoneEvent : VirtualTrainingEvent
    {

    }
    #endregion

    #region Misc
    public class BlockerButtonEvent : VirtualTrainingEvent
    {
        public bool IsBlock;

        public BlockerButtonEvent(bool isBlock)
        {
            IsBlock = isBlock;
        }
    }
    #endregion

    #region QuizHistory
    public class ShowDetailHistoryEvent : VirtualTrainingEvent
    {
        public List<string> reports;
        public List<bool> status;

        public ShowDetailHistoryEvent(List<string> reports, List<bool> status)
        {
            this.reports = reports;
            this.status = status;
        }
    }
    #endregion

    #region LoginQuiz
    public class InitLoginEvent : VirtualTrainingEvent
    {
    }
    #endregion

    #region Pop Up
    public class PopUpQuizEvent : VirtualTrainingEvent
    {
        public float Duration;
        public string TextPops;
        public PopUpType Type;

        public PopUpQuizEvent(float duration, string text, PopUpType type = PopUpType.LOGIN)
        {
            Duration = duration;
            TextPops = text;
            Type = type;
        }
    }
    #endregion

}

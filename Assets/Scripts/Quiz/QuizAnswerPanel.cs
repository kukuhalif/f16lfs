using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class QuizAnswerPanel : UIPanel
    {
        [SerializeField] InteractionButton backButton;
        [SerializeField] TextMeshProUGUI textAnswer;
        [SerializeField] QuizResultPanel quizResultPanel;
        [SerializeField] TemplateResultQuiz templateResultQuiz;
        [SerializeField] Transform templateParent;
        [SerializeField] HistoryResultPanel historyResultPanel;
        [SerializeField] Scrollbar verticalScrollbar;
        [SerializeField] List<TemplateResultQuiz> templateResultQuizes = new List<TemplateResultQuiz>();
        float progress;
        string label;
        bool setScrollbarValue;

        private float Progress()
        {
            return progress;
        }

        private string Label()
        {
            return label;
        }
        void EndLoading()
        {
            Debug.Log("loading finished");
        }

        protected override void Start()
        {
            base.Start();
            backButton.OnClickEvent += BackAnswer;
            EventManager.AddListener<OpenResultNew>(SetupAnswerPanel);

        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            backButton.OnClickEvent -= BackAnswer;
            EventManager.RemoveListener<OpenResultNew>(SetupAnswerPanel);
        }

        void BackAnswer()
        {
            this.ClosePanel(() =>
            {
                for (int i = 0; i < templateResultQuizes.Count; i++)
                {
                    Destroy(templateResultQuizes[i].gameObject);
                }
                templateResultQuizes.Clear();
                if (QuizManager.isDoingQuiz)
                    quizResultPanel.ShowPanel();
                else
                    historyResultPanel.ShowPanel();
            });
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);
            setScrollbarValue = false;
        }

        IEnumerator SetScrollbarValue()
        {
            while (setScrollbarValue)
            {
                yield return null;
                verticalScrollbar.value = 1;
            }
        }

        public void SetupAnswerPanel(OpenResultNew e)
        {
            templateResultQuiz.SetColor();

            this.ShowPanel();
            string reportText = "";
            for (int i = 0; i < e.Result.Count; i++)
            {
                reportText += e.Result[i];
                int x = i + 1;
                string number = "Number " + x;
                var rpt = e.Result[i].Split("\n"[0]);
                string question = rpt[1].Trim();
                question = question.Replace("Question :", "<b> Question : </b>");
                string yourAnswer = rpt[2].Trim();
                yourAnswer = yourAnswer.Replace("Your answer :", "<color=white><b> Your answer : </b></color>");
                string trueAnswer = "";
                if (rpt.Length > 3)
                {
                    trueAnswer = rpt[3].Trim();
                    trueAnswer = trueAnswer.Replace("True answer :", "<color=white><b> True answer : </b></color>");
                }
                GameObject template = Instantiate(templateResultQuiz.gameObject);
                TemplateResultQuiz quiz = template.GetComponent<TemplateResultQuiz>();
                templateResultQuizes.Add(quiz);
                quiz.Init(number, question, yourAnswer, trueAnswer);
                template.SetActive(true);
                quiz.transform.SetParent(templateParent);
                quiz.transform.localScale = Vector3.one;
            }
            progress = 1f;
            setScrollbarValue = true;
            StartCoroutine(SetScrollbarValue());
        }
    }
}

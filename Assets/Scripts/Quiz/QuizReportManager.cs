﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using VirtualTraining.Core;

namespace VirtualTraining.Quiz
{

    public class QuizReportManager : MonoBehaviour
    {
        #region load data

        List<string[]> ReportDatas = new List<string[]>();
        List<bool> ResultAnswers = new List<bool>();
        public void SetReportDatas(List<Quiz> quizDatas)
        {
            ReportDatas.Clear();
            ResultAnswers.Clear();
            for (int i = 0; i < quizDatas.Count; i++)
            {
                ReportDatas.Add(quizDatas[i].GetReport());
                ResultAnswers.Add(quizDatas[i].ResultAnswer);
            }
            Debug.Log("set report data");
        }

        public List<bool> GetAnswerStatus()
        {
            return ResultAnswers;
        }

        public List<string> GetReports()
        {
            List<string> reports = new List<string>();
            for (int i = 0; i < ReportDatas.Count; i++)
            {
                string dt = "";
                for (int ii = 0; ii < ReportDatas[i].Length; ii++)
                {
                    dt += ReportDatas[i][ii];
                }
                reports.Add(dt);
            }
            return reports;
        }

        #endregion

        #region UI

        List<QuizReportButton> ReportButtons = new List<QuizReportButton>();

        [SerializeField]
        CanvasGroup ReportCanvasGroup;
        Tween ReportCanvasTweener;

        public void OpenReport()
        {
            //AudioPlayer.PlaySFX(SFX.ButtonClick);
            EventManager.TriggerEvent(new BlockerButtonEvent(true));

            InstantiateReportButtons();

            ReportCanvasGroup.gameObject.SetActive(true);

            if (ReportCanvasTweener != null)
                ReportCanvasTweener.Kill(true);

            ReportCanvasGroup.alpha = 0f;
            ReportCanvasTweener = ReportCanvasGroup.DOFade(1, 0.5f).OnComplete(DoneFadeIn);
        }

        public void CloseReport()
        {
            //AudioPlayer.PlaySFX(SFX.Warning);
            if (ReportCanvasTweener != null)
                ReportCanvasTweener.Kill(true);

            ReportCanvasGroup.alpha = 1f;
            ReportCanvasTweener = ReportCanvasGroup.DOFade(0, 0.5f).OnComplete(DoneFadeout).OnComplete(DoneFadeout);

            for (int i = 0; i < ReportButtons.Count; i++)
            {
                Destroy(ReportButtons[i].gameObject);
            }
            ReportButtons.Clear();
        }
        void DoneFadeout()
        {
            ReportCanvasGroup.gameObject.SetActive(false);
        }

        void DoneFadeIn()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
        }

        #endregion

        #region instantiate items

        [SerializeField]
        QuizReportButton ReportItemTemplate;

        void InstantiateReportButtons()
        {
            for (int i = 0; i < ReportDatas.Count; i++)
            {
                QuizReportButton newBtn = Instantiate(ReportItemTemplate);
                newBtn.gameObject.SetActive(true);
                newBtn.transform.SetParent(ReportItemTemplate.transform.parent);
                newBtn.transform.Reset();
                newBtn.Setup(i + 1, ResultAnswers[i], ReportDatas[i]);
                ReportButtons.Add(newBtn);
            }
        }

        #endregion
    }

}
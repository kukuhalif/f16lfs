using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.UI;
using DG.Tweening;
using System.IO;
using VirtualTraining.Core;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class QuizManager : MonoBehaviour
    {
        [Header("FROM DATABASE")]
        private QuizDatabase dbQuiz;

        [Header("MANAGER")]
        int quizAmount;
        [SerializeField]
        public List<Quiz> selectedQuiz = new List<Quiz>();

        private int _CurrentCount = 0, _LastQuestion = 0;

        [Header("MANAGER")]
        public TextMeshProUGUI timeCount;

        [SerializeField]
        public QuizReportManager reportManager;

        private float timeDuration = 0;
        private bool stopTimer = false;
        string minutes = "";
        string seconds = "";
        string time = "";
        string fullDate = "";
        string user = "", studentID = "";

        int trueAnswer = 0;

        private QuizResultNew quizResult;

        [SerializeField] QuizPanel quizPanel;

        bool DatabaseInit;

        [SerializeField]
        InteractionToggle randomToggle;
        public static bool isDoingQuiz;
        float progress;
        string label;

        void Awake()
        {
            EventManager.AddListener<EndQuizEvent>(EndQuiz);
            EventManager.AddListener<InitQuizManagerEvent>(Init);
            EventManager.AddListener<StartQuizEvent>(StartQuiz);

            dbQuiz = GetComponent<QuizDatabase>();
            quizResult = GetComponent<QuizResultNew>();
            quizAmount = DatabaseManager.getQuizData().quizAllAmount;
            dbQuiz.allMateriAmount = DatabaseManager.getQuizData().quizAllAmount;
            dbQuiz.quizA = DatabaseManager.getQuizData().quizSectionAmount;
            DatabaseInit = false;
        }
        private void OnDestroy()
        {
            EventManager.RemoveListener<EndQuizEvent>(EndQuiz);
            EventManager.RemoveListener<InitQuizManagerEvent>(Init);
            EventManager.RemoveListener<StartQuizEvent>(StartQuiz);
        }

        private void Start()
        {
            Init(null); //initsialisai tanpa event di awal
            PrinterManager.RemoveTempImageResult();
        }

        public void OnMateriDropdownChanged(Dropdown dropdown)
        {
            if (dropdown.value == 0)
            {
                randomToggle.IsOn = true;
                //RandomToggle.interactable = false;
            }
            else
            {
                randomToggle.IsOn = false;
                //RandomToggle.interactable = true;
                AudioPlayer.PlaySFX(SFX.ButtonClick);
            }
        }
        private float Progress()
        {
            return progress;
        }

        private string Label()
        {
            return label;
        }
        public void InitQuizData(System.Action initDoneCallback)
        {
            ResetQuizManager();

            VirtualTraining.Core.EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetQuizLoadingVideos(), Progress, Label, null, EndLoading, true));
            //VirtualTraining.Core.EventManager.TriggerEvent(new ShowLoadingScreenEvent());
            Debug.Log("init quiz");
            progress = 0.1f;
            label = "loading quiz";
            //loadingLoadDatabaseUI.SetActive(true);

            if (!DatabaseInit)
            {
                dbQuiz.Init();
                //dbHistory.Init();
            }
            StartCoroutine(WaitLoadQuizData(initDoneCallback));
        }

        void EndLoading()
        {
            Debug.Log("loading finished");
        }

        IEnumerator WaitLoadQuizData(System.Action initDoneCallback)
        {
            while (!dbQuiz.loadDatabaseFinished)
            {
                yield return null;
            }
            DatabaseInit = true;
            selectedQuiz = dbQuiz.GetQuizList(ref quizAmount, randomToggle.IsOn);

            //loadingLoadDatabaseUI.SetActive(false);
            QuizCurrentState.maxQuestionNumber = quizAmount;
            quizPanel.SetupQuiz(0);
            Debug.Log("finished load quiz");
            progress = 1;
            //EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetQuizLoadingVideos(), false, true, Progress, Label, EndLoading, false));

            initDoneCallback.Invoke();
        }

        void Init(InitQuizManagerEvent e)
        {
            //_Tittle.text = Application.productName;

            ResetAll();

            string dayName = System.DateTime.Now.ToString("dddd");
            string day = System.DateTime.Now.Day.ToString();
            string month = System.DateTime.Now.Month.ToString();
            string year = System.DateTime.Now.Year.ToString();
            fullDate = day + "-" + month + "-" + year;
            QuizCurrentState.date = dayName + ", " + fullDate;

            if (selectedQuiz == null || selectedQuiz.Count <= 0)
                return;
        }

        private void Update()
        {
            if (!stopTimer)
            {
                timeDuration += Time.deltaTime;
                float minutes = Mathf.Floor(timeDuration / 60);
                float seconds = Mathf.RoundToInt(timeDuration % 60);

                if (minutes < 10)
                    this.minutes = "0" + minutes.ToString();
                else
                    this.minutes = minutes.ToString();
                if (seconds < 10)
                    this.seconds = "0" + Mathf.RoundToInt(seconds).ToString();
                else
                    this.seconds = seconds.ToString();
                time = this.minutes + ":" + this.seconds;
                timeCount.text = time;
            }
        }
        void StartQuiz(StartQuizEvent e)
        {
            stopTimer = false;
        }

        void EndQuiz(EndQuizEvent e)
        {
            e.QuizData.SetResultAnswer(e.ResultAnswer);
            StartCoroutine(DoneQuiz());
        }

        public void EndQuizNew()
        {
            StartCoroutine(DoneQuiz());
        }

        IEnumerator DoneQuiz()
        {
            yield return new WaitForSeconds(0);
            //_CurrentCount++;
            //if (_CurrentCount > _LastQuestion)
            //    _LastQuestion = _CurrentCount;
            //if (_CurrentCount < quizAmount)
            //{

            //}
            //else
            //{
            //    EventManager.TriggerEvent(new BlockerButtonEvent(false));
            //    //_UICanvas.DOFade(0, 1);
            //    float totalResult = GetAllResultScore();

            //    stopTimer = true;

            //    string dayName = System.DateTime.Now.ToString("dddd");
            //    string day = System.DateTime.Now.Day.ToString();
            //    string month = System.DateTime.Now.Month.ToString();
            //    string year = System.DateTime.Now.Year.ToString();
            //    string date = day + "-" + month + "-" + year;
            //    string fullDate = dayName + ", " + date;

            //    quizResult.gameObject.SetActive(true);
            //    quizResult.InitResult(quizAmount, trueAnswer, time, totalResult.ToString("F2"), user, dbQuiz.SelectMateriDropdown.options[dbQuiz.SelectMateriDropdown.value].text, studentID, fullDate);

            //    reportManager.SetReportDatas(selectedQuiz);

            //    while (!quizResult.isShapshotDone)
            //        yield return null;

            //    yield return null;
            //    ResultQuizData r = new ResultQuizData(user, studentID, this.fullDate, time, totalResult.ToString("F2"), reportManager.GetReports(), reportManager.GetAnswerStatus(), dbQuiz.SelectMateriDropdown.options[dbQuiz.SelectMateriDropdown.value].text, quizResult.GetSnapshotData());

            //    ResetPlayedQuizState();
            //}
            //Debug.Log("QUIZ DONE");
            //_UICanvas.DOFade(0, 1);
            float totalResult = QuizCurrentState.score;

            stopTimer = true;

            string dayName = System.DateTime.Now.ToString("dddd");
            string day = System.DateTime.Now.Day.ToString();
            string month = System.DateTime.Now.Month.ToString();
            string year = System.DateTime.Now.Year.ToString();
            string date = day + "-" + month + "-" + year;
            string fullDate = dayName + ", " + date;

            //quizResult.gameObject.SetActive(true);
            quizResult.InitResult();
            reportManager.SetReportDatas(selectedQuiz);

            while (!quizResult.isShapshotDone)
                yield return null;

            yield return null;
            ResultQuizData r = new ResultQuizData(QuizCurrentState.name, QuizCurrentState.studentID, this.fullDate, time, totalResult.ToString("F2"), reportManager.GetReports(), reportManager.GetAnswerStatus(), dbQuiz.SelectMateriDropdown.options[dbQuiz.SelectMateriDropdown.value].text, quizResult.GetSnapshotData());

            ResetPlayedQuizState();
        }

        public void ForceDoneQuiz()
        {
            EventManager.TriggerEvent(new CloseQuizUI());
            for (; _CurrentCount < quizAmount; _CurrentCount++)
            {
                if (selectedQuiz.Count > _CurrentCount)
                {
                    selectedQuiz[_CurrentCount].Skip = true;
                    selectedQuiz[_CurrentCount].ResetState();
                }
            }
            StartCoroutine(DoneQuiz());
        }

        void ResetPlayedQuizState()
        {
            for (int i = 0; i < selectedQuiz.Count; i++)
            {
                selectedQuiz[i].ResetState();
                selectedQuiz[i].Skip = false;
            }
        }

        private float GetAllResultScore()
        {
            float total = 0;
            trueAnswer = 0;

            for (int i = 0; i < selectedQuiz.Count; i++)
            {
                if (!selectedQuiz[i].Skip)
                {
                    bool isTrue = selectedQuiz[i].GetLastResultQuiz();
                    if (isTrue)
                        trueAnswer++;
                }
            }

            total = Mathf.Round(trueAnswer) / Mathf.Round(quizAmount) * 100;

            return total;
        }

        void ResetAll()
        {
            stopTimer = true;
            selectedQuiz = new List<Quiz>();
            timeDuration = 0;
            _CurrentCount = 0;
            _LastQuestion = 0;
        }

        void ResetQuizManager()
        {
            DatabaseInit = false;
            dbQuiz.ResetDatabase();
        }
    }
}

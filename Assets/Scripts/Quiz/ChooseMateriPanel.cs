using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using VirtualTraining.SceneManagement;
using TMPro;

namespace VirtualTraining.Quiz
{
    public class ChooseMateriPanel : UIPanel
    {
        [SerializeField] InteractionButton startButton;
        [SerializeField] InteractionButton historyResult;
        [SerializeField] InteractionButton closeChooseMateri;
        [SerializeField] TextMeshProUGUI textName;
        [SerializeField] LoginPanel loginPanel;
        [SerializeField] QuizPanel quizPanel;
        [SerializeField] HistoryResultPanel historyResultPanel;
        [SerializeField] QuizManager quizManager;
        [SerializeField] TMP_Dropdown tmpDropdown;
        [SerializeField] InteractionToggle randomToggle;
        [SerializeField] GameObject confirmationPanel;

        [SerializeField] InteractionButton yesConfirmButton;
        [SerializeField] InteractionButton noConfirmButton;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            startButton.OnClickEvent += StartQuizListener;
            historyResult.OnClickEvent += OpenHistoryResult;
            closeChooseMateri.OnClickEvent += CloseChooseMateriQuiz;
            yesConfirmButton.OnClickEvent += ConfirmationYes;
            noConfirmButton.OnClickEvent += ConfirmationNo;
        }

        public void SetName()
        {
            textName.text = "Hi, " + QuizCurrentState.name;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            startButton.OnClickEvent -= StartQuizListener;
            historyResult.OnClickEvent -= OpenHistoryResult;
            closeChooseMateri.OnClickEvent -= CloseChooseMateriQuiz;
            yesConfirmButton.OnClickEvent -= ConfirmationYes;
            noConfirmButton.OnClickEvent -= ConfirmationNo;
        }

        void StartQuizListener()
        {
            quizManager.InitQuizData(() =>
            {
                quizPanel.DestroyQuestionButtons();
                this.ClosePanel(() =>
                {
                    QuizCurrentState.title = tmpDropdown.captionText.text;
                    quizPanel.SetupQuizPanel();

                    quizPanel.ShowPanel(() =>
                    {
                        EventManager.TriggerEvent(new StartQuizEvent());
                        QuizManager.isDoingQuiz = true;
                    });
                });
            });
        }

        void OpenHistoryResult()
        {
            this.ClosePanel(() =>
            {
                EventManager.AddListener<LoadHistoryDataDoneEvent>(HistoryLoaded);
                historyResultPanel.SetupHistoryPanel();
            });
        }

        private void HistoryLoaded(LoadHistoryDataDoneEvent e)
        {
            EventManager.RemoveListener<LoadHistoryDataDoneEvent>(HistoryLoaded);
            QuizManager.isDoingQuiz = false;
            historyResultPanel.ShowPanel();
        }

        void CloseChooseMateriQuiz()
        {
            //loginPanel.ShowPanel();
            //confirmationPanel.SetActive(true);
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to logout current session?", ConfirmationYes, null));
            //this.ClosePanel();
        }

        void ConfirmationYes()
        {
            VirtualTrainingSceneManager.CloseQuiz();
            //VirtualTrainingSceneManager.OpenQuiz();
        }

        void ConfirmationNo()
        {
            confirmationPanel.SetActive(false);
        }

        public void CheckDropdown()
        {
            if (tmpDropdown.value == 0)
            {
                randomToggle.gameObject.SetActive(true);
            }
            else
                randomToggle.gameObject.SetActive(false);
        }
    }
}

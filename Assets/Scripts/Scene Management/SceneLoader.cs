using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public class SceneLoader : MonoBehaviour
    {
        bool finished;
        float maxProgress, progress, fakeProgress;
        string info;

        public string GetInfo()
        {
            return info;
        }

        public float GetProgress()
        {
            float prog = progress > fakeProgress ? progress : fakeProgress;

            if (finished)
                return 1f;

            if (float.IsInfinity(prog))
                return fakeProgress;

            if (prog >= 1f)
                return maxProgress;

            return prog;
        }

        public void ResetProgress()
        {
            progress = fakeProgress = 0f;
            finished = false;
        }

        public void CompleteProgress()
        {
            progress = 1f;
            finished = true;
        }

        private void Start()
        {
            VirtualTrainingSceneManager.SetSceneLoader(this);
        }

        private void LoadMainSceneAddressableCompleted(AsyncOperationHandle<SceneInstance> obj)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(VirtualTrainingSceneManager.CurrentMainScene));
        }

        private void LoadSceneCompleted(AsyncOperation obj)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(VirtualTrainingSceneManager.CurrentMainScene));
        }

        public void LoadScenes(List<string> oldScenes, List<string> newScenes, string newMainScenePath)
        {
            info = "now loading";
            progress = fakeProgress = 0f;
            finished = false;
            maxProgress = Random.Range(0.96f, 0.98f);
            Invoke("AddFakeProgress", Random.Range(0.1f, 0.5f));
            StartCoroutine(LoadScenesRoutine(oldScenes, newScenes, newMainScenePath));
        }

        private void AddFakeProgress()
        {
            fakeProgress += Time.deltaTime * Random.Range(5f, 25);
            if (fakeProgress >= maxProgress)
                fakeProgress = maxProgress;

            Invoke("AddFakeProgress", Random.Range(0.3f, 1.5f));
        }

        private bool IsSceneLoaded(string sceneName)
        {
            int count = SceneManager.sceneCount;
            for (int i = 0; i < count; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.name == sceneName)
                    return true;
            }

            return false;
        }

        private IEnumerator LoadScenesRoutine(List<string> oldScenes, List<string> newScenes, string newMainScenePath)
        {
            var toLoad = new List<string>();
            var toUnload = new List<string>();

            AsyncOperation unloadAssetAsync;

            for (int i = 0; i < newScenes.Count; i++)
            {
                if (!oldScenes.Contains(newScenes[i]))
                    toLoad.Add(newScenes[i]);
            }

            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < oldScenes.Count; i++)
            {
                if (!newScenes.Contains(oldScenes[i]))
                    toUnload.Add(oldScenes[i]);
            }

            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < toUnload.Count; i++)
            {
                Debug.Log("unload " + toUnload[i]);
                info = "unloading " + toUnload[i];
                AsyncOperation unloadAsync = SceneManager.UnloadSceneAsync(toUnload[i], UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
                while (!unloadAsync.isDone)
                    yield return null;

                System.GC.Collect();
                yield return null;

                unloadAssetAsync = Resources.UnloadUnusedAssets();

                while (!unloadAssetAsync.isDone)
                    yield return null;

                progress = (float)i / (float)(toLoad.Count + toUnload.Count);
            }

            float unloadProgress = progress;

            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < toLoad.Count; i++)
            {
                if (IsSceneLoaded(toLoad[i]))
                    continue;

                yield return null;

                Debug.Log("load " + toLoad[i]);
                info = "loading " + toLoad[i];
                if (i == 0) // main scene
                {
                    if (DatabaseManager.IsAddressableAssetMode())
                    {
                        AsyncOperationHandle<SceneInstance> addresableAsyncOperation = Addressables.LoadSceneAsync(newMainScenePath, LoadSceneMode.Additive, true);
                        addresableAsyncOperation.Completed += LoadMainSceneAddressableCompleted;

                        while (!addresableAsyncOperation.IsDone)
                            yield return null;
                    }
                    else
                    {
                        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(toLoad[i], LoadSceneMode.Additive);
                        asyncOperation.completed += LoadSceneCompleted;

                        while (!asyncOperation.isDone)
                            yield return null;
                    }
                }
                else // other scenes
                {
                    AsyncOperation otherOperation = SceneManager.LoadSceneAsync(toLoad[i], LoadSceneMode.Additive); // other scene

                    while (!otherOperation.isDone)
                        yield return null;
                }

                progress = ((float)i + unloadProgress) / (float)(toLoad.Count + toUnload.Count);
            }

            unloadAssetAsync = Resources.UnloadUnusedAssets();

            while (!unloadAssetAsync.isDone)
                yield return null;

            info = "initializing";
            CancelInvoke("AddFakeProgress");
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public abstract class SceneModeManager : MonoBehaviour
    {
        protected void SceneReady()
        {
            EventManager.TriggerEvent(new SceneReadyEvent(VirtualTrainingSceneManager.SceneConfig));
        }
    }
}

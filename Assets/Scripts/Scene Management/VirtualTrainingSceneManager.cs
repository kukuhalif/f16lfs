﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public static class VirtualTrainingSceneManager
    {
        public static MateriTreeElement LAST_MATERI_TREE_ELEMENT;

        static GameObject OBJECT_ROOT;

        public const string MODEL_LAYER_NAME = "Default";
        public const string OBJECT_ROOT_NAME = "Object Root";

        static SceneConfig CURRENT_SCENE_CONFIG;
        static string CURRENT_MAIN_SCENE;

        static SceneLoader SCENE_LOADER;

        static Func<GameObject, bool> IS_OBJECT_CULLED;

        static int MODEL_LAYER;

        public static bool isVRLoaded;
        static VRLoader VR_LOADER;

        public static SceneMode CurrentSceneMode { get => CURRENT_SCENE_CONFIG.mode; }

        static VirtualTrainingSceneManager()
        {
            OBJECT_ROOT = GameObject.Find(OBJECT_ROOT_NAME);
            MODEL_LAYER = LayerMask.GetMask(MODEL_LAYER_NAME);
        }

        public static void InitStartingSceneMode(SceneConfig config)
        {
            CURRENT_SCENE_CONFIG = config;
        }

        public static void SetSceneLoader(SceneLoader sceneLoader)
        {
            SCENE_LOADER = sceneLoader;
        }

        public static void SetVRLoader(VRLoader vrLoader)
        {
            VR_LOADER = vrLoader;
        }

        public static GameObject GameObjectRoot
        {
            get
            {
                if (OBJECT_ROOT == null)
                    OBJECT_ROOT = GameObject.Find(OBJECT_ROOT_NAME);
                return OBJECT_ROOT;
            }
        }

        public static int ModelLayer { get => MODEL_LAYER; }
        public static float LoadSceneProgress { get => SCENE_LOADER.GetProgress(); }

        public static string LoadSceneInfo { get => SCENE_LOADER.GetInfo(); }

        public static SceneConfig SceneConfig { get => CURRENT_SCENE_CONFIG; }
        public static string CurrentMainScene { get => CURRENT_MAIN_SCENE; }

        public static void CompleteLoadSceneProgress()
        {
            SCENE_LOADER.CompleteProgress();
        }

        public static void SetIsObjectCulledCallback(Func<GameObject, bool> callbackFunction)
        {
            IS_OBJECT_CULLED = callbackFunction;
        }

        public static bool IsObjectCulled(GameObject obj)
        {
            if (IS_OBJECT_CULLED == null)
                return false;

            return IS_OBJECT_CULLED(obj);
        }

        #region scene switch

        public static void LoadStartingScene()
        {
            CURRENT_MAIN_SCENE = CURRENT_SCENE_CONFIG.scenes[0];
            SCENE_LOADER.LoadScenes(new List<string>(), CURRENT_SCENE_CONFIG.scenes, CURRENT_SCENE_CONFIG.mainScenePath);
        }

        public static void SwitchSceneMode(SceneMode mode)
        {
            var prevScenes = CURRENT_SCENE_CONFIG.scenes;
            CURRENT_SCENE_CONFIG = DatabaseManager.GetSceneConfig(mode);
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
            CURRENT_MAIN_SCENE = CURRENT_SCENE_CONFIG.scenes[0];
            SCENE_LOADER.LoadScenes(prevScenes, CURRENT_SCENE_CONFIG.scenes, CURRENT_SCENE_CONFIG.mainScenePath);
        }

        public static void LoadVRScene()
        {
            VR_LOADER.LoadVRScene(true);
        }
        public static void UnloadVRScene()
        {
            VR_LOADER.LoadVRScene(false);
        }
        public static void LoadHeadVRScene(Action callback)
        {
            VR_LOADER.OpenVRHeadScene(callback);
        }
        public static void UnloadHeadVRScene()
        {
            VR_LOADER.CloseVRHeadScene();
        }

        public static bool DestroyVRPlugin()
        {
            return VR_LOADER.DestroyVRLoader();
        }

        private static void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
            EventManager.TriggerEvent(new FreePlayEvent());
            SCENE_LOADER.CompleteProgress();
        }

        #endregion

        #region quiz

        public static void OpenQuiz()
        {
            SCENE_LOADER.ResetProgress();
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetQuizLoadingVideos(), () => SCENE_LOADER.GetProgress(), () => "Load Quiz", () =>
              {
                  SceneManager.UnloadSceneAsync(DatabaseManager.GetUIScene(CURRENT_SCENE_CONFIG), UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
                  EventManager.AddListener<InitializationUIDoneEvent>(QuizInitDoneListener);
                  SceneManager.LoadSceneAsync(CURRENT_SCENE_CONFIG.quizScene, LoadSceneMode.Additive);

              }, null, true));
        }

        public static void CloseQuiz()
        {
            SCENE_LOADER.ResetProgress();
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetQuizLoadingVideos(), () => SCENE_LOADER.GetProgress(), () => "Unload Quiz", () =>
              {
                  SceneManager.UnloadSceneAsync(CURRENT_SCENE_CONFIG.quizScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
                  EventManager.AddListener<InitializationUIDoneEvent>(QuizInitDoneListener);
                  SceneManager.LoadSceneAsync(DatabaseManager.GetUIScene(CURRENT_SCENE_CONFIG), LoadSceneMode.Additive);

              }, null, true));
        }

        private static void QuizInitDoneListener(InitializationUIDoneEvent e)
        {
            EventManager.RemoveListener<InitializationUIDoneEvent>(QuizInitDoneListener);
            EventManager.TriggerEvent(new SceneReadyEvent(CURRENT_SCENE_CONFIG));
            EventManager.TriggerEvent(new FreePlayEvent());
            SCENE_LOADER.CompleteProgress();
            Resources.UnloadUnusedAssets();
        }

        public static bool CheckDatabaseQuiz()
        {
            string path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), Application.productName, "Quiz/Database/");
            Debug.Log(path);
            if (!System.IO.Directory.Exists(path))
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Folder Quiz Database Not Found", null, null));
                return false;
            }
            string[] files = Directory.GetFiles(path, "*.fyr");

            if (files.Length == 0)
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Folder Quiz Database is Empty", null, null));
                return false;
            }

            return true;
        }

        #endregion

        #region vr head

        public static void OpenVRHead()
        {
            VR_LOADER.positionCameraDesktop = VirtualTrainingCamera.CameraController.transform.position;
            VR_LOADER.eulerCameraDesktop = VirtualTrainingCamera.CameraController.transform.eulerAngles;
            SCENE_LOADER.ResetProgress();
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => SCENE_LOADER.GetProgress(), () => "Load VR Head", () =>
               {
                   SceneManager.UnloadSceneAsync(DatabaseManager.GetCameraScene(CURRENT_SCENE_CONFIG), UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
                   EventManager.AddListener<CameraControllerReadyEvent>(CameraControllerDoneListener);
                   SceneManager.LoadSceneAsync(CURRENT_SCENE_CONFIG.vrHeadScene, LoadSceneMode.Additive);

               }, null, true));
        }

        public static void CloseVRHead()
        {
            SCENE_LOADER.ResetProgress();
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => SCENE_LOADER.GetProgress(), () => "Unload VR Head", () =>
             {
                 SceneManager.UnloadSceneAsync(CURRENT_SCENE_CONFIG.vrHeadScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
                 EventManager.AddListener<CameraControllerReadyEvent>(CameraControllerDoneListener);
                 SceneManager.LoadSceneAsync(DatabaseManager.GetCameraScene(CURRENT_SCENE_CONFIG), LoadSceneMode.Additive);

             }, null, true));
        }
        private static void CameraControllerDoneListener(CameraControllerReadyEvent e)
        {
            EventManager.RemoveListener<CameraControllerReadyEvent>(CameraControllerDoneListener);
            VirtualTrainingCamera.CameraController.ForceSetPositionAndRotation(VR_LOADER.positionCameraDesktop, VR_LOADER.eulerCameraDesktop);
            SCENE_LOADER.CompleteProgress();
            Resources.UnloadUnusedAssets();
        }

        #endregion

        #region networking

        public static void OpenNetworking()
        {
            SCENE_LOADER.ResetProgress();
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => SCENE_LOADER.GetProgress(), () => "Load Networking", () =>
            {
                EventManager.AddListener<InitializationUIDoneEvent>(NetworkingInitDoneListener);
                SceneManager.LoadSceneAsync(CURRENT_SCENE_CONFIG.networkingScene, LoadSceneMode.Additive);

            }, null, true));
        }

        public static void CloseNetworking()
        {
            SCENE_LOADER.ResetProgress();
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => SCENE_LOADER.GetProgress(), () => "Unload Networking", () =>
            {
                SceneManager.UnloadSceneAsync(CURRENT_SCENE_CONFIG.networkingScene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects).completed += UnloadNetworkingDone;

            }, null, true));
        }

        private static void UnloadNetworkingDone(AsyncOperation obj)
        {
            EventManager.TriggerEvent(new SceneReadyEvent(CURRENT_SCENE_CONFIG));
            SCENE_LOADER.CompleteProgress();
            Resources.UnloadUnusedAssets();
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        private static void NetworkingInitDoneListener(InitializationUIDoneEvent e)
        {
            EventManager.RemoveListener<InitializationUIDoneEvent>(QuizInitDoneListener);
            EventManager.TriggerEvent(new SceneReadyEvent(CURRENT_SCENE_CONFIG));
            SCENE_LOADER.CompleteProgress();
            Resources.UnloadUnusedAssets();
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        #endregion
    }
}

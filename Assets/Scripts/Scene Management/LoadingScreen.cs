using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] Camera cam;
        [SerializeField] VideoPlayer player;
        [SerializeField] GameObject canvas;
        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] AspectRatioFitter introVideoAspect;
        [SerializeField] CanvasScaler canvasScaler;

        bool playIntro, ending, skipIntroListenerAdded;
        VideoClip intro;

        Coroutine waitIntroRoutine;

        private void Start()
        {
            canvasScaler.referenceResolution = SettingUtility.LastResolution;
            intro = DatabaseManager.GetIntroVideo();

            if (intro != null)
            {
                var settingData = SettingUtility.LastSetting;
                player.clip = intro;
                player.enabled = true;
                introVideoAspect.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                introVideoAspect.aspectRatio = (float)intro.width / (float)intro.height;
                player.SetDirectAudioVolume(0, settingData.voiceVolume);
                player.Play();
                canvas.SetActive(true);
                playIntro = true;

                VirtualTrainingInputSystem.OnStartLeftClick += SkipIntroVideo;
                skipIntroListenerAdded = true;
                Debug.Log("skip intro video added");
                waitIntroRoutine = StartCoroutine(WaitVideo());
            }
            else
                StartCoroutine(LoadingRoutine());
        }

        private void OnDestroy()
        {
            if (skipIntroListenerAdded)
            {
                VirtualTrainingInputSystem.OnStartLeftClick -= SkipIntroVideo;
                Debug.Log("skip intro video removed on destroy");
            }
        }

        private void SkipIntroVideo(Vector2 pointerPosition, GameObject raycastedUI)
        {
            VirtualTrainingInputSystem.OnStartLeftClick -= SkipIntroVideo;
            skipIntroListenerAdded = false;
            Debug.Log("skip intro video removed on click");

            StopCoroutine(waitIntroRoutine);
            VideoEnd();
            StartCoroutine(LoadingRoutine());
        }

        IEnumerator WaitVideo()
        {
            yield return new WaitForSeconds((float)player.clip.length);
            VideoEnd();
            StartCoroutine(LoadingRoutine());
        }

        private void VideoEnd()
        {
            player.Stop();
            player.enabled = false;
            ending = true;
            playIntro = false;
        }

        private void Update()
        {
            if (playIntro)
                return;

            if (ending)
            {
                canvasGroup.alpha -= Time.deltaTime * 2f;
                if (canvasGroup.alpha <= 0 && LoadingProgress() >= 1f)
                {
                    ending = false;
                    Destroy(gameObject);
                }
                return;
            }
        }

        private IEnumerator LoadingRoutine()
        {
            // init starting scene mode
            yield return null;
            VirtualTrainingSceneManager.InitStartingSceneMode(DatabaseManager.GetStartingSceneConfig());

            // show loading screen
            yield return null;
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), LoadingProgress, LoadingText, OnShowLoadingScreenCallback, OnEndLoadingScreenCallback, false));
        }

        private float LoadingProgress()
        {
            return VirtualTrainingSceneManager.LoadSceneProgress;
        }

        private string LoadingText()
        {
            return VirtualTrainingSceneManager.LoadSceneInfo;
        }

        private void OnShowLoadingScreenCallback()
        {
            // add listener
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);

            // load scene
            VirtualTrainingSceneManager.LoadStartingScene();
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);

            VirtualTrainingSceneManager.CompleteLoadSceneProgress();
        }

        private void OnEndLoadingScreenCallback()
        {
            cam.enabled = false;
            if (intro == null)
                ending = true;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement.Desktop
{
    public class FlightScenarioRecorderSceneModeManager : FeatureInitializer
    {
        float loadProgress;

        private IEnumerator Start()
        {
            yield return null;
            loadProgress = 0f;
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetQuizLoadingVideos(), () => loadProgress, () => "load flight sim recorder", () =>
            {
                VirtualTrainingSceneManager.InitStartingSceneMode(DatabaseManager.GetStartingSceneConfig());
                Initialize(InitDoneCallback);
            },
            () =>
            {

            }, false));
            
        }

        private void InitDoneCallback()
        {
            SceneReady();
            loadProgress = 1f;
            Debug.Log("flight sim recorder ready");
        }
    }
}
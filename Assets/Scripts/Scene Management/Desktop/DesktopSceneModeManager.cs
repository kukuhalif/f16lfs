using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement.Desktop
{
    public class DesktopSceneModeManager : FeatureInitializer
    {
        private void Start()
        {
            Initialize(InitDoneCallback);
        }

        private void InitDoneCallback()
        {
            SceneReady();
        }
    }
}

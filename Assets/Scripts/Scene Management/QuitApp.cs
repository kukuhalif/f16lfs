using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public class QuitApp : MonoBehaviour
    {
        float progress, totalTask;
        string loadingText;

        private void Start()
        {
            EventManager.AddListener<QuitAppEvent>(QuitListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<QuitAppEvent>(QuitListener);
        }

        private void QuitListener(QuitAppEvent e)
        {
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), GetProgress, QuitLoadingText, () =>
            {
                StartCoroutine(Quitter());

            }, null, false));
        }

        private IEnumerator Quitter()
        {
            // disable model
            Transform root = VirtualTrainingSceneManager.GameObjectRoot.transform;

            // scenes to unload
            var scenes = VirtualTrainingSceneManager.SceneConfig.scenes;

            // calculate total task
            totalTask = root.childCount + scenes.Count;
            int objCount = root.childCount;

            // destroy model object
            for (int i = 0; i < root.childCount; i++)
            {
                loadingText = "unloading part " + root.GetChild(i).name;

                var ch = root.GetChild(i);
                Destroy(ch.gameObject);

                progress = (float)i / ((float)totalTask);
                yield return null;
            }

            yield return null;

            loadingText = "unloading environment";

            EnvironmentObject environmentObject = GameObject.FindObjectOfType<EnvironmentObject>();
            if (environmentObject != null)
                Destroy(environmentObject.gameObject);

            yield return null;

            // unload scenes
            for (int i = 0; i < scenes.Count; i++)
            {
                loadingText = "unload " + scenes[i];

                var unloadAsync = SceneManager.UnloadSceneAsync(scenes[i], UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);

                while (!unloadAsync.isDone)
                    yield return null;

                System.GC.Collect();
                yield return null;

                var unloadAssetAsync = Resources.UnloadUnusedAssets();

                while (!unloadAssetAsync.isDone)
                    yield return null;

                progress = ((float)i + (float)objCount) / ((float)totalTask);
            }

            loadingText = "goodbye :)";

            yield return null;

            if (VirtualTrainingSceneManager.isVRLoaded)
            {
                while (VirtualTrainingSceneManager.DestroyVRPlugin() == false)
                {
                    Debug.Log("vr belum di destroy");
                    yield return null;
                }
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
            //System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
        }

        private float GetProgress()
        {
            return progress >= 1f ? 0.99f : progress;
        }

        private string QuitLoadingText()
        {
            return loadingText;
        }
    }
}
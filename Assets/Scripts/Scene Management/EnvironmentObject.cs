using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public class EnvironmentObject : MonoBehaviour
    {
        SettingData currentSetting;

        private void Start()
        {
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.AddListener<DisableEnvironmentEvent>(DisableBackgroundObjectListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<DisableEnvironmentEvent>(DisableBackgroundObjectListener);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            currentSetting = e.data;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(currentSetting.showEnvironmentObject);
            }
        }

        private void DisableBackgroundObjectListener(DisableEnvironmentEvent e)
        {
            if (currentSetting == null)
                return;

            if (e.reset)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    transform.GetChild(i).gameObject.SetActive(currentSetting.showEnvironmentObject);
                }
            }
            else
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    transform.GetChild(i).gameObject.SetActive(!e.disable);
                }
            }
        }
    }
}

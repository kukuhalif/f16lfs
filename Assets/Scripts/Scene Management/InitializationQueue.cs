﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.SceneManagement
{
    public abstract class InitializationQueue : MonoBehaviour
    {
        public abstract void Initialize();
        public abstract void ObjectSetup(GameObject obj);
    }
}

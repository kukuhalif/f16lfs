using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Management;
using VirtualTraining.Core;
//using Valve.VR;
using UnityEngine.XR;
using UnityEngine.EventSystems;
using System;

namespace VirtualTraining.SceneManagement
{
    public class VRLoader : MonoBehaviour
    {
        [SerializeField] bool isVRScene;
        float progress;
        string label;
        bool isVRSceneOnly;
        public Vector3 positionCameraDesktop;
        public Vector3 eulerCameraDesktop;

        SceneMode lastSceneMode;

        void Start()
        {
            VirtualTrainingSceneManager.SetVRLoader(this);
        }

        private void OnDestroy()
        {
            DestroyVRLoader();
        }

        //#if UNITY_EDITOR
        // Update is called once per frame
        //void Update()
        //{
        //    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.V))
        //    {
        //        SwitchToVR();
        //    }

        //    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.B))
        //    {
        //        SwitchToLastMode();
        //    }
        //    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Q))
        //    {
        //        EventManager.TriggerEvent(new QuitAppEvent());
        //    }
        //}

        //#endif
        void SetUnactiveSteamVR()
        {
            GameObject vrModule = GameObject.Find("[VRModule]");
            GameObject steamVR = GameObject.Find("[SteamVR]");
            //GameObject parent = new GameObject("parent temp");
            if (vrModule != null)
            {
                Destroy(vrModule.gameObject);
            }
            if (steamVR != null)
            {
                Destroy(steamVR.gameObject);
            }
        }

        public void LoadVRScene(bool loadVR)
        {
            if (loadVR)
            {
                //if (isHardwarePresent() == false)
                //{
                //    EventManager.TriggerEvent(new ConfirmationPanelEvent("VR Device is Not Connected", null, null));
                //}
                //else
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to Enter VR?", SwitchToVR, StopXR));
            }
            else
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to Exit VR?", () => { StartCoroutine(SwitchToLastMode()); }, null));
            }
        }

        IEnumerator SwitchToLastMode()
        {
            AudioPlayer.StopMateri();
            EventManager.TriggerEvent(new ResetObjectInteractionModeEvent());
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new ResetPartObjectEvent());

            yield return null;

            GameObject[] childArray = VirtualTrainingSceneManager.GameObjectRoot.GetAllChilds();
            foreach (var child in childArray)
            {
                child.gameObject.layer = 0;
            }

            yield return null;

            StopXR();

            yield return null;

            if (DatabaseManager.GetStartingSceneConfig().mode == SceneMode.VR && isVRSceneOnly)
            {
                Application.Quit();
            }
            else
                EventManager.TriggerEvent(new ShowLoadingScreenEvent(
                    DatabaseManager.GetAppsLoadingVideos(),
                    () => VirtualTrainingSceneManager.LoadSceneProgress,
                    Label,
                    () => VirtualTrainingSceneManager.SwitchSceneMode(lastSceneMode),
                    null,
                    true
                    ));
        }
        void SwitchToVR()
        {
            lastSceneMode = VirtualTrainingSceneManager.CurrentSceneMode;
            label = "Please Wait, Checking VR Device";
            progress = 0.1f;
            StartXRInit();
        }

        void StartXRInit()
        {
            StartCoroutine(StartXR(OpenVRScene));
        }

        void OpenVRScene()
        {
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), () => VirtualTrainingSceneManager.LoadSceneProgress,
    Label, () => VirtualTrainingSceneManager.SwitchSceneMode(SceneMode.VR), null, true));

        }

        void LoadVR()
        {
            VirtualTrainingSceneManager.SwitchSceneMode(SceneMode.VR);
        }
        public bool isHardwarePresent()
        {
            if (!XRGeneralSettings.Instance.Manager.isInitializationComplete)
            {
                XRGeneralSettings.Instance.Manager.InitializeLoaderSync();
                XRGeneralSettings.Instance.Manager.StartSubsystems();
            }
            //try
            //{
            //    // Do something that can throw an exception
            //    XRGeneralSettings.Instance.Manager.InitializeLoaderSync();
            //}
            //catch (Exception e)
            //{
            //    Debug.Log("tessst");
            //    //Debug.LogException(e, this);
            //}


            var xrDisplaySubsystems = new List<XRDisplaySubsystem>();
            SubsystemManager.GetInstances<XRDisplaySubsystem>(xrDisplaySubsystems);
            foreach (var xrDisplay in xrDisplaySubsystems)
            {
                if (xrDisplay.running)
                {
                    xrDisplay.Start();
                    return true;
                }
            }

            XRGeneralSettings.Instance.Manager.StopSubsystems();
            XRGeneralSettings.Instance.Manager.DeinitializeLoader();
            return false;
        }
        private float Progress()
        {
            return progress;
        }
        private string Label()
        {
            return label;
        }

        void EndLoading()
        {
            Debug.Log("loading finished");
        }
        public IEnumerator StartXR(Action changeState, Action buttonCallback = null)
        {
            //yield return new WaitForSeconds(1);

            Debug.Log("Initializing XR...");
            if (isHardwarePresent() == false)
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("VR Device is Not Connected", null, null));
                yield break;
            }


            if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
                yield return null;
            else
                yield return XRGeneralSettings.Instance.Manager.InitializeLoader();

            if (XRGeneralSettings.Instance.Manager.activeLoader == null)
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("VR Device is Not Connected", null, null));
                Debug.Log("Initializing XR Failed. Directing to Normal Interaciton Mode...!.");
                StopXR();
                DirectToNormal();
            }
            else
            {
                Debug.Log("Initialization Finished.Starting XR Subsystems...");
                bool loaderSuccess;
                //Try to start all subsystems and check if they were all successfully started ( thus HMD prepared).
                if (XRGeneralSettings.Instance.Manager.activeLoader.Start() == true)
                {

                    loaderSuccess = XRGeneralSettings.Instance.Manager.activeLoader.Start();
                    XRGeneralSettings.Instance.Manager.StartSubsystems();
                }
                else
                    loaderSuccess = true;
                Debug.Log(XRGeneralSettings.Instance.Manager.activeLoader.name);

                if (isHardwarePresent() == false)
                {
                    EventManager.TriggerEvent(new ConfirmationPanelEvent("VR Device is Not Connecteds", null, null));
                    loaderSuccess = false;
                    yield return null;
                }
                Debug.Log(isHardwarePresent());
                if (loaderSuccess)
                {
                    if (isHardwarePresent() == false)
                    {
                        StopXR();
                        EventManager.TriggerEvent(new ConfirmationPanelEvent("VR Device is Not Connected", null, null));
                    }
                    else
                    {
                        EventManager.TriggerEvent(new ResetObjectInteractionModeEvent());
                        VirtualTrainingSceneManager.isVRLoaded = true;
                        yield return null;
                        changeState();
                        if (buttonCallback != null)
                            buttonCallback();
                    }
                }
                else
                {
                    Debug.LogError("Starting Subsystems Failed. Directing to Normal Interaciton Mode...!");
                    StopXR();
                    DirectToNormal();
                }
            }
            progress = 1;
        }

        void StopXR()
        {
            if (XRGeneralSettings.Instance.Manager.activeLoader != null)
            {
                Debug.Log("XR stopped completely.");
                //SteamVR.Initialize(false);
                SetUnactiveSteamVR();
                //SteamVR.ExitTemporarySession();
                //SteamVR.Initialize(false);
                Debug.Log(XRGeneralSettings.Instance.Manager.isInitializationComplete);
                DirectToNormal();
                VirtualTrainingSceneManager.isVRLoaded = false;
            }
        }

        void DirectToNormal()
        {
            Debug.Log("Fell back to Mouse & Keyboard Interaciton!");
            if (XRGeneralSettings.Instance.Manager.activeLoader != null)
            {
                Debug.Log("yuhuu");
                XRGeneralSettings.Instance.Manager.activeLoader.Stop();
                XRGeneralSettings.Instance.Manager.DeinitializeLoader();
            }
        }

        public bool DestroyVRLoader()
        {
            if (XRGeneralSettings.Instance.Manager.activeLoader != null)
            {
                bool condiition = XRGeneralSettings.Instance.Manager.activeLoader.Stop();
                if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
                {
                    XRGeneralSettings.Instance.Manager.DeinitializeLoader();
                }

                return condiition;
            }
            StopXR();
            return false;
        }

        private void OnApplicationQuit()
        {
            if (XRGeneralSettings.Instance.Manager.activeLoader != null)
            {
                XRGeneralSettings.Instance.Manager.activeLoader.Stop();
                if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
                    XRGeneralSettings.Instance.Manager.DeinitializeLoader();
            }
            Debug.Log("quiitt");
            StopXR();
        }

        public void OpenVRHeadScene(Action openCallback)
        {
            StartCoroutine(StartXR(OpenVRHeadCondition, openCallback));
        }

        public void CloseVRHeadScene()
        {
            StopXR();
            VirtualTrainingSceneManager.CloseVRHead();
        }

        void OpenVRHeadCondition()
        {
            VirtualTrainingSceneManager.OpenVRHead();
            VirtualTrainingCamera.CurrentCamera.gameObject.SetActive(false); ;
        }
    }
}

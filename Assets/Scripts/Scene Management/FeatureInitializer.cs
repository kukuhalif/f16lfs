using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement
{
    public abstract class FeatureInitializer : SceneModeManager
    {
        const int MAX_OBJ_PER_FRAME = 500;

        bool uiDone, featureDone;
        Action initDoneCallback;

        protected void Initialize(Action doneCallback)
        {
            StartCoroutine(Init(doneCallback));
        }

        private IEnumerator Init(Action doneCallback)
        {
            yield return null;
            uiDone = featureDone = false;
            initDoneCallback = doneCallback;
            yield return null;
            EventManager.AddListener<InitializationUIDoneEvent>(UiInitDoneListener);
            EventManager.TriggerEvent(new InitializeUIEvent());

            if (VirtualTrainingSceneManager.GameObjectRoot == null)
            {
                Debug.LogWarning("game object root null");
            }
            else
            {
                Debug.Log("start initialization");
                StartCoroutine(FeatureInitialization());
            }
        }

        private void UiInitDoneListener(InitializationUIDoneEvent e)
        {
            EventManager.RemoveListener<InitializationUIDoneEvent>(UiInitDoneListener);
            uiDone = true;
            InitDone();
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        private void InitDone()
        {
            if (uiDone && featureDone)
            {
                initDoneCallback.Invoke();
            }
        }

        private IEnumerator FeatureInitialization()
        {
            InitializationQueue[] queue = FindObjectsOfType<InitializationQueue>();
            foreach (var q in queue)
            {
                q.Initialize();
            }

            List<GameObject> allObj = new List<GameObject>();
            allObj.Add(VirtualTrainingSceneManager.GameObjectRoot);

            GameObject[] childs = VirtualTrainingSceneManager.GameObjectRoot.GetAllChilds();

            allObj.AddRange(childs);

            Debug.Log("start initiazing " + allObj.Count + " objects");

            int counter = 0;
            GameObjectReference.ClearGameobjectReferenceLookup();

            for (int i = 0; i < allObj.Count; i++)
            {
                GameObjectReference gof = allObj[i].gameObject.GetComponent<GameObjectReference>();
                if (gof != null)
                {
                    gof.RegisterObject();
                }

                for (int q = 0; q < queue.Length; q++)
                {
                    queue[q].ObjectSetup(allObj[i]);
                }

                // disable animator
                Animator anim = allObj[i].GetComponent<Animator>();
                if (anim != null)
                    if (anim.gameObject.tag != "FlightSim") // by RF
                        anim.enabled = false;

                if (counter > MAX_OBJ_PER_FRAME)
                {
                    counter = 0;
                    yield return null;
                }

                counter++;
            }

            yield return null;
            EventManager.TriggerEvent(new InitializeGameObjectReferenceEvent(VirtualTrainingSceneManager.GameObjectRoot));

            yield return null;
            Debug.Log("initialization finished");
            featureDone = true;
            InitDone();
        }
    }
}

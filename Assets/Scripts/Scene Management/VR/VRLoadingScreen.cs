using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using VirtualTraining.Core;
using Valve.VR;
using UnityEngine.XR;
using UnityEngine.EventSystems;
using System;
using UnityEngine.XR.Management;

namespace VirtualTraining.SceneManagement.VR
{
    public class VRLoadingScreen : MonoBehaviour
    {
        [SerializeField] Camera cam;
        [SerializeField] GameObject playerVR;
        [SerializeField] VideoPlayer player;
        [SerializeField] GameObject canvas;
        [SerializeField] CanvasGroup canvasGroup;

        bool loadingScreenTriggered, touched, ending;
        VideoClip intro;

        public GameObject panelConfirmation;
        public GameObject panelLoading;

        [SerializeField] Transform uiSpawner;

        private void Start()
        {
            StartCoroutine(CheckVRHardware());
        }

        private void Init()
        {
            VirtualTrainingInputSystem.OnTouchAdded += TouchListener;

            intro = DatabaseManager.GetIntroVideo();
            if (intro != null)
            {
                var data = SettingUtility.GetSavedSetting();
                player.clip = intro;
                player.enabled = true;
                player.SetDirectAudioVolume(0, data.voiceVolume);
                player.Play();
                canvas.SetActive(true);

                StartCoroutine(WaitVideo());
            }

            StartCoroutine(LoadingRoutine());
        }

        private void OnDestroy()
        {
            VirtualTrainingInputSystem.OnTouchAdded -= TouchListener;
        }

        private void TouchListener(bool isFingerOverUI)
        {
            touched = true;
        }

        IEnumerator WaitVideo()
        {
            yield return new WaitForSeconds((float)player.clip.length);
            VideoEnd();
        }

        private void VideoEnd()
        {
            if (uiSpawner != null)
                uiSpawner.SetParent(null);

            //uiSpawner.gameObject.SetActive(true);
            player.Stop();
            player.enabled = false;
            ending = true;

            if (VirtualTrainingCamera.CurrentCamera != null)
            {
                VirtualTrainingCamera.CurrentCamera.enabled = true;
            }
        }

        private void Update()
        {
            if (ending)
            {
                canvasGroup.alpha -= Time.deltaTime * 2f;
                if (canvasGroup.alpha <= 0 && LoadingProgress() >= 1f)
                {
                    ending = false;
                    canvas.SetActive(false);
                    Destroy(gameObject);
                }
                return;
            }

            if (loadingScreenTriggered)
                return;

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Mouse0) || touched)
            {
                loadingScreenTriggered = true;
                VideoEnd();
            }
        }

        private IEnumerator LoadingRoutine()
        {
            // init starting scene mode
            yield return null;
            VirtualTrainingSceneManager.InitStartingSceneMode(DatabaseManager.GetStartingSceneConfig());

            // show loading screen
            yield return null;
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), LoadingProgress, LoadingText, OnShowLoadingScreenCallback, OnEndLoadingScreenCallback, false));
        }

        private float LoadingProgress()
        {
            return VirtualTrainingSceneManager.LoadSceneProgress;
        }

        private string LoadingText()
        {
            return VirtualTrainingSceneManager.LoadSceneInfo;
        }

        private void OnShowLoadingScreenCallback()
        {
            // add listener
            EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);

            // load scene
            VirtualTrainingSceneManager.LoadStartingScene();
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
            if (uiSpawner != null)
                uiSpawner.SetParent(null);

            VirtualTrainingSceneManager.CompleteLoadSceneProgress();

            if (intro = DatabaseManager.GetIntroVideo())
            {
                if (intro == null)
                {
                    if (VirtualTrainingCamera.CurrentCamera != null)
                    {
                        VirtualTrainingCamera.CurrentCamera.enabled = true;
                    }
                }
            }

        }

        private void OnEndLoadingScreenCallback()
        {
            cam.enabled = false;
            player.gameObject.SetActive(false);
            if (intro == null)
                ending = true;
        }

        IEnumerator CheckVRHardware()
        {
            yield return XRGeneralSettings.Instance.Manager.InitializeLoader();
            bool loaderSuccess;
            Debug.Log(XRGeneralSettings.Instance.Manager.isInitializationComplete);
            if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
            {
                loaderSuccess = XRGeneralSettings.Instance.Manager.activeLoader.Start();
                XRGeneralSettings.Instance.Manager.StartSubsystems();
                yield return null;
                if (loaderSuccess)
                {
                    if (isHardwarePresent() == false)
                    {

                        CloseLoadingPanel();
                    }
                    else
                    {
                        panelLoading.SetActive(true);
                        panelConfirmation.SetActive(false);
                        Init();
                    }
                }
                else
                {
                    CloseLoadingPanel();
                }
            }
            else
            {
                CloseLoadingPanel();
            }
        }

        void CloseLoadingPanel()
        {
            panelLoading.SetActive(false);
            panelConfirmation.SetActive(true);
            loadingScreenTriggered = true;
        }
        public bool isHardwarePresent()
        {
            var xrDisplaySubsystems = new List<XRDisplaySubsystem>();
            SubsystemManager.GetInstances<XRDisplaySubsystem>(xrDisplaySubsystems);
            foreach (var xrDisplay in xrDisplaySubsystems)
            {
                if (xrDisplay.running)
                {
                    xrDisplay.Start();
                    return true;
                }
            }
            return false;
        }
        public void StopXR()
        {
            if (XRGeneralSettings.Instance.Manager.activeLoader != null)
            {
                Debug.Log("XR stopped completely.");
                //SteamVR.Initialize(false);
                SteamVR.ExitTemporarySession();
                SteamVR.Initialize(false);
            }
            Application.Quit();
        }
    }
}

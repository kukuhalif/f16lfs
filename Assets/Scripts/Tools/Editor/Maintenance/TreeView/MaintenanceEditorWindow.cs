﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    class MaintenanceEditorWindow : TreeViewEditorBase<MaintenanceTreeAsset, MaintenanceEditorWindow, MaintenanceData, MaintenanceTreeElement>
    {
        MaintenanceContentEditor maintenanceContentEditor;

        public static void GetWindow()
        {
            MaintenanceEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }
        public static MaintenanceEditorWindow OnOpenAsset(int instanceID)
        {
            MaintenanceEditorWindow window = OpenEditorWindow(instanceID, true);
            if (window != null)
            {
                window.Initialize();
                return window;
            }

            return null;
        }

        protected override TreeViewWithTreeModel<MaintenanceTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<MaintenanceTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<MaintenanceTreeElement>(TreeViewState, treeModel);
        }

        protected override MaintenanceTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new MaintenanceTreeElement("New Maintenance Data", depth, id, false);
        }

        protected override void Initialize()
        {
            // if scriptable object removed
            if (ScriptableObjectOriginalFile == null)
                return;

            base.Initialize();
            var xrayMats = DatabaseManager.GetXrayMaterials();

            maintenanceContentEditor = new MaintenanceContentEditor(ApplyOverrideData, RecordUndo, () => WindowId, xrayMats);
        }

        protected override void OnDisable()
        {
            ApplyOverrideData();
            base.OnDisable();

            if (maintenanceContentEditor != null)
                maintenanceContentEditor.SaveCameraDestination();
        }

        protected override void Save()
        {
            ApplyOverrideData();
            base.Save();
        }

        private void ApplyOverrideData()
        {
            var trees = ScriptableObjectTemp.GetData().treeElements;
            for (int i = 0; i < trees.Count; i++)
            {
                for (int f = 0; f < trees[i].data.figures.Count; f++)
                {
                    if (trees[i].data.figures[f].cameraOverrideSource.GetId() != -1)
                    {
                        trees[i].data.figures[f].cameraDestinations.Clear();
                        var cams = trees[i].data.figures[f].cameraOverrideSource.GetData().figures[trees[i].data.figures[f].cameraOverrideSourceFigureIndex].cameraDestinations;
                        for (int s = 0; s < cams.Count; s++)
                        {
                            trees[i].data.figures[f].cameraDestinations.Add(VirtualTrainingEditorUtility.CloneObject(cams[s]));
                        }
                    }

                    if (trees[i].data.figures[f].partObjectOverrideSource.GetId() != -1)
                    {
                        trees[i].data.figures[f].partObjects.Clear();
                        var parts = trees[i].data.figures[f].partObjectOverrideSource.GetData().figures[trees[i].data.figures[f].partObjectOverrideSourceFigureIndex].partObjects;
                        for (int s = 0; s < parts.Count; s++)
                        {
                            trees[i].data.figures[f].partObjects.Add(VirtualTrainingEditorUtility.CloneObject(parts[s]));
                        }
                    }
                }
            }
        }

        protected override void ContentView(List<MaintenanceTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder && maintenanceContentEditor != null)
                    maintenanceContentEditor.DetailInspector(selectedElements[0].id, selectedElements[0], detailPanelRect, true);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        selectedElements[i].name = maintenanceContentEditor.PreviewInspector(selectedElements[i].name, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "maintenance name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].name = selectedElements[i].data.title;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].data.title = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            EditorGUILayout.EndVertical();
        }
    }
}
#endif

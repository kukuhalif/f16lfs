﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(MaintenanceTreeAsset))]
    public class MaintenanceTreeAssetEditor : TreeAssetEditorBase<MaintenanceTreeAsset, MaintenanceTreeElement, MaintenanceData>
    {
        protected override MaintenanceTreeAsset GetAsset()
        {
            return (MaintenanceTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<MaintenanceData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<MaintenanceTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}
#endif

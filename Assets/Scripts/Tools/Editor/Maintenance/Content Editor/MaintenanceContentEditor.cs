#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{

    public class MaintenanceContentEditor : ContentEditorBase<MaintenanceFigure>
    {
        Action applyOverrideCallback;
        MaintenanceDataModel playedMaintenance;

        int currentFigureIndex = 0;
        int currentCameraIndex = 0;

        public MaintenanceContentEditor(Action applyOverrideCallback, Action beforeModifiedCallback, Func<string> windowId, List<Material> xrayMats) :
            base(beforeModifiedCallback, windowId, xrayMats,
                new CategoryTab[] { CategoryTab.FIGURE_LIST, CategoryTab.ANIMATION, CategoryTab.VIRTUAL_BUTTON },
                new FigureTab[] { FigureTab.CAMERA, FigureTab.PART_OBJECT, FigureTab.CALLOUT, FigureTab.HELPER, FigureTab.CUTAWAY })
        {
            this.applyOverrideCallback = applyOverrideCallback;
        }

        protected override MaintenanceFigure GetFigure(object data, int index)
        {
            MaintenanceDataModel maintenance = data as MaintenanceDataModel;
            return maintenance.figures[index];
        }

        protected override int GetFigureCount(object data)
        {
            MaintenanceDataModel maintenance = data as MaintenanceDataModel;
            return maintenance.figures.Count;
        }

        protected override List<Schematic> GetSchematicFromData(object data)
        {
            return null;
        }

        protected override void ShowFiguresField(object data, string listName, string fieldId)
        {
            MaintenanceDataModel maintenance = data as MaintenanceDataModel;
            ShowListField(listName, maintenance.figures, fieldId);
        }

        protected override void CameraTab(object data, string fieldId)
        {
            MaintenanceFigure figure = data as MaintenanceFigure;

            ShowReferenceField<MaintenanceElementType, MaintenanceTreeElement>("overide camera", fieldId + "override cam", ref figure.cameraOverrideSource);

            if (figure.cameraOverrideSource.GetId() != -1)
            {
                var figures = figure.cameraOverrideSource.GetData().figures;
                List<string> figOption = new List<string>();
                for (int i = 0; i < figures.Count; i++)
                {
                    figOption.Add(figures[i].name);
                }
                if (figures.Count > figure.cameraOverrideSourceFigureIndex)
                    figure.cameraOverrideSourceFigureIndex = ShowDropdownField("select figure source", fieldId + "figure select", figures[figure.cameraOverrideSourceFigureIndex].name, figOption);
                else
                    figure.cameraOverrideSourceFigureIndex = figures.Count - 1;
            }

            if (figure.cameraOverrideSource.GetId() == -1)
            {
                base.CameraTab(data, fieldId);
            }
        }

        protected override void PartObjectTab(object data, string fieldId)
        {
            MaintenanceFigure figure = data as MaintenanceFigure;

            ShowReferenceField<MaintenanceElementType, MaintenanceTreeElement>("overide part object", fieldId + "override part obj", ref figure.partObjectOverrideSource);

            if (figure.partObjectOverrideSource.GetId() != -1)
            {
                var figures = figure.partObjectOverrideSource.GetData().figures;
                List<string> figOption = new List<string>();
                for (int i = 0; i < figures.Count; i++)
                {
                    figOption.Add(figures[i].name);
                }
                if (figures.Count > figure.partObjectOverrideSourceFigureIndex)
                    figure.partObjectOverrideSourceFigureIndex = ShowDropdownField("select figure source", fieldId + "figure select", figures[figure.partObjectOverrideSourceFigureIndex].name, figOption);
                else
                    figure.partObjectOverrideSourceFigureIndex = figures.Count - 1;
            }

            if (figure.partObjectOverrideSource.GetId() == -1)
            {
                base.PartObjectTab(data, fieldId);
            }
        }

        protected override void TopDetailInspector(object data, Rect panelRect)
        {
            MaintenanceTreeElement maintenanceTreeElement = data as MaintenanceTreeElement;
            MaintenanceDataModel maintenanceData = maintenanceTreeElement.data;

            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("play maintenance (alt + t)") || PlayTestShortcut.PressedWindowId == WindowId)
                {
                    PlayTestShortcut.Played();
                    applyOverrideCallback.Invoke();
                    playedMaintenance = maintenanceData;
                    currentFigureIndex = 0;
                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new MaintenancePlayEvent(maintenanceTreeElement));
                }

                if (playedMaintenance == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("reset maintenance"))
                {
                    playedMaintenance = null;
                    EventManager.TriggerEvent(new MaintenancePlayEvent());
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous figure"))
                {
                    currentFigureIndex--;
                    if (currentFigureIndex < 0)
                        currentFigureIndex = playedMaintenance.figures.Count - 1;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PlayFigureLinkEvent(playedMaintenance.figures[currentFigureIndex].name, MateriElementType.Maintenance, playedMaintenance));
                }

                if (GUILayout.Button("play next figure"))
                {
                    currentFigureIndex++;
                    if (playedMaintenance.figures.Count <= currentFigureIndex)
                        currentFigureIndex = 0;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PlayFigureLinkEvent(playedMaintenance.figures[currentFigureIndex].name, MateriElementType.Maintenance, playedMaintenance));
                }

                GUILayout.Label("current figure : " + currentFigureIndex);

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous camera"))
                {
                    currentCameraIndex--;
                    if (currentCameraIndex < 0)
                        currentCameraIndex = playedMaintenance.figures[currentFigureIndex].cameraDestinations.Count - 1;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedMaintenance.figures[currentFigureIndex].name, playedMaintenance.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.Maintenance, playedMaintenance));
                }

                if (GUILayout.Button("play next camera"))
                {
                    currentCameraIndex++;
                    if (playedMaintenance.figures[currentFigureIndex].cameraDestinations.Count <= currentCameraIndex)
                        currentCameraIndex = 0;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedMaintenance.figures[currentFigureIndex].name, playedMaintenance.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.Maintenance, playedMaintenance));
                }

                GUILayout.Label("current camera : " + currentCameraIndex);

                GUILayout.EndHorizontal();

                if (playedMaintenance == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);

            ShowSimpleField("maintenance name", ref maintenanceData.title);
            ShowSimpleField("disable environment", ref maintenanceData.disableEnvironment);
            ShowTextArea("description", panelRect.width - 15f, ref maintenanceData.description, true);
        }

        protected override void BottomDetailInspector(object data, Rect panelRect)
        {
            MaintenanceTreeElement maintenanceTreeElement = data as MaintenanceTreeElement;
            ShowTab("categories", maintenanceTreeElement.data);
        }
    }
}

#endif
﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.IO;
using UnityEngine.AddressableAssets;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using Unity​Editor.AddressableAssets.Settings.GroupSchemas;
using UnityEditor.AddressableAssets.Build;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class Builder
    {
        [MenuItem("Virtual Training/Builder/Regular/Windows/Final Build")]
        static void FinalRegularBuildForWindows()
        {
            BuildPlayer(BuildTarget.StandaloneWindows64, BuildOptions.CompressWithLz4HC, false, true);
        }

        [MenuItem("Virtual Training/Builder/Regular/Windows/Test Build")]
        static void TestRegularBuildForWindows()
        {
            BuildPlayer(BuildTarget.StandaloneWindows64, BuildOptions.Development, false, false);
        }

        [MenuItem("Virtual Training/Builder/Addressable/Windows/Final Build")]
        static void FinalBuildForWindows()
        {
            BuildPlayer(BuildTarget.StandaloneWindows64, BuildOptions.CompressWithLz4HC, true, true);
        }

        [MenuItem("Virtual Training/Builder/Addressable/Windows/Test Build")]
        static void TestBuildForWindows()
        {
            BuildPlayer(BuildTarget.StandaloneWindows64, BuildOptions.Development, true, false);
        }

        [MenuItem("Virtual Training/Builder/Addressable/Asset/Configure Asset Group")]
        public static void ConfigureAddressableAssets()
        {
            UnityEditor.EditorApplication.isPlaying = false;

            var setting = AddressableAssetSettingsDefaultObject.Settings;
            if (setting == null)
            {
                if (EditorUtility.DisplayDialog("builder", "Attempting to inspect default Addressables Settings, but no settings file exists.  Open 'Window/Asset Management/Addressables/Groups' to create addressables setting files.", "ok"))
                {

                }

                return;
            }

            // Clean
            AddressableAssetSettings.CleanPlayerContent();

            // get default profile settings
            var profileSettings = setting.profileSettings;
            string profileId = profileSettings.GetProfileId("Default");
            AddressableAssetSettingsDefaultObject.Settings.activeProfileId = profileId;

            // clear groups
            AddressableAssetSettingsDefaultObject.Settings.groups.Clear();

            // add model scene
            AddModelSceneToGroup();

            string assetPath = DatabaseManager.GetContentPathConfig().contentAssetPath;

            // add material
            AddAssetToGroup("material", assetPath, "mat");

            // add model
            AddAssetToGroup("model", assetPath, "fbx");

            // add textures
            AddAssetToGroup("texture png", assetPath, "png");
            AddAssetToGroup("texture jpg", assetPath, "jpg");
            AddAssetToGroup("texture jpeg", assetPath, "jpeg");

            // add animation
            AddAssetToGroup("animation", assetPath, "anim");

            // add animator controller
            AddAssetToGroup("animator", assetPath, "controller");

            // save asset
            AssetDatabase.SaveAssets();
        }

        [MenuItem("Virtual Training/Builder/Addressable/Asset/Configure and Build Asset Group")]
        public static bool ConfigureAndBuildAddressableAssets()
        {
            // unplay
            UnityEditor.EditorApplication.isPlaying = false;

            // check addressable setting files
            var setting = AddressableAssetSettingsDefaultObject.Settings;
            if (setting == null)
            {
                if (EditorUtility.DisplayDialog("builder", "Attempting to inspect default Addressables Settings, but no settings file exists.  Open 'Window/Asset Management/Addressables/Groups' to create addressables setting files.", "ok"))
                {

                }

                return false;
            }

            if (EditorUtility.DisplayDialog("builder", "configure and build addressable asset group ?", "yes", "no"))
            {
                // Configure
                ConfigureAddressableAssets();

                // Build addressables
                AddressablesPlayerBuildResult result;
                AddressableAssetSettings.BuildPlayerContent(out result);

                if (string.IsNullOrEmpty(result.Error))
                    return true;
                else
                {
                    UnityEngine.Debug.LogError(result.Error);
                    return false;
                }
            }

            return true;
        }

        static void AddModelSceneToGroup()
        {
            AddressableAssetGroup modelGroup = AddressableAssetSettingsDefaultObject.Settings.CreateGroup("scene", false, false, true, null, typeof(ContentUpdateGroupSchema), typeof(BundledAssetGroupSchema));

            var entriesAdded = new List<AddressableAssetEntry>();

            foreach (var sceneConfig in DatabaseManager.GetAllSceneConfig())
            {
                var assetpath = AssetDatabase.GetAssetPath(AssetDatabase.LoadAssetAtPath(sceneConfig.mainScenePath, typeof(SceneAsset)));
                var guid = AssetDatabase.AssetPathToGUID(assetpath);

                var e = AddressableAssetSettingsDefaultObject.Settings.CreateOrMoveEntry(guid, modelGroup, false, false);

                entriesAdded.Add(e);
            }

            modelGroup.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entriesAdded, false, true);
            AddressableAssetSettingsDefaultObject.Settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entriesAdded, true, false);
        }

        static void AddAssetToGroup(string groupName, string contentPath, string extension)
        {
            AddressableAssetGroup group = AddressableAssetSettingsDefaultObject.Settings.CreateGroup(groupName, false, false, true, null, typeof(ContentUpdateGroupSchema), typeof(BundledAssetGroupSchema));

            var entriesAdded = new List<AddressableAssetEntry>();

            string[] files = Directory.GetFiles(contentPath, "*." + extension, SearchOption.AllDirectories);
            foreach (string file in files)
            {
                string assetPath = file.Replace(Application.dataPath, "").Replace('\\', '/');

                var guid = AssetDatabase.AssetPathToGUID(assetPath);

                // only add asset to asset group if asset is not default asset
                DefaultAsset defaultAsset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(DefaultAsset)) as DefaultAsset;
                if (defaultAsset == null)
                {
                    var e = AddressableAssetSettingsDefaultObject.Settings.CreateOrMoveEntry(guid, group, false, false);
                    entriesAdded.Add(e);
                }
                else
                {
                    UnityEngine.Debug.LogError(assetPath + " is invalid or not supported type");
                }
            }

            group.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entriesAdded, false, true);
            AddressableAssetSettingsDefaultObject.Settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entriesAdded, true, false);
        }

        static void BuildPlayer(BuildTarget target, BuildOptions buildOptions, bool addressable, bool finalBuild)
        {
            // set mode
            DatabaseManager.SetIsAddressableAssetMode(addressable);

            // set build setting scenes
            VirtualTrainingEditorUtility.SetBuildSettingScenes();

            // unplay
            UnityEditor.EditorApplication.isPlaying = false;

            // Get filename
            string path = EditorUtility.SaveFolderPanel("Choose Location of Built", "", "");
            if (string.IsNullOrEmpty(path))
                return;

            // cleanup folder
            Directory.Delete(path, true);

            // re create folder
            Directory.CreateDirectory(path);

            // delete temp
            string[] filePaths = Directory.GetFiles(Application.dataPath + "/Temp");
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            // refresh asset database
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

            // build AddressableAssets
            if (addressable)
            {
                var result = ConfigureAndBuildAddressableAssets();
                if (!result)
                    return;
            }

            // get scene names
            List<string> scenes = new List<string>();
            foreach (var scene in EditorBuildSettings.scenes)
            {
                scenes.Add(scene.path);
            }

            // add model scene to build setting if regular build
            if (!addressable)
            {
                foreach (var sceneConfig in DatabaseManager.GetAllSceneConfig())
                {
                    scenes.Add(sceneConfig.mainScenePath);
                }
            }

            // Build player
            BuildPipeline.BuildPlayer(scenes.ToArray(), path + "/" + Application.productName + ".exe", target, buildOptions);

            // remove addresabble asset if addresable asset mode is false
            if (!DatabaseManager.IsAddressableAssetMode())
            {
                var aaPath = Path.Combine(path, Application.productName + "_Data/StreamingAssets/aa");
                if (Directory.Exists(aaPath))
                    Directory.Delete(aaPath, true);
            }

            // Remove BackUpThisFolder_ButDontShipItWithYourGame if finalBuild true
            if (finalBuild)
            {
                string cppDir = path + "/" + Application.productName + "_BackUpThisFolder_ButDontShipItWithYourGame";
                if (Directory.Exists(cppDir))
                {
                    UnityEngine.Debug.Log("BackUpThisFolder_ButDontShipItWithYourGame Deleted");
                    Directory.Delete(cppDir, true);
                }

                string burstDir = path + "/" + Application.productName + "_BurstDebugInformation_DoNotShip";
                if (Directory.Exists(burstDir))
                {
                    UnityEngine.Debug.Log("BurstDebugInformation_DoNotShip Deleted");
                    Directory.Delete(burstDir, true);
                }
            }

            // Run the app (Process class from System.Diagnostics)
            // Process proc = new Process();
            // proc.StartInfo.FileName = path + "/" + Application.productName + ".exe";
            // proc.Start();

            // Show window explorer
            Process.Start(path);
        }
    }
}

#endif
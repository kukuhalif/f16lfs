﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(VirtualButtonTreeAsset))]
    public class VirtualButtonTreeAssetEditor : TreeAssetEditorBase<VirtualButtonTreeAsset, VirtualButtonTreeElement, VirtualButtonData>
    {
        protected override VirtualButtonTreeAsset GetAsset()
        {
            return (VirtualButtonTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<VirtualButtonData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<VirtualButtonTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}

#endif

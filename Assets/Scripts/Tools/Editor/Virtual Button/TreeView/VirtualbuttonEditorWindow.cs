﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    class VirtualButtonEditorWindow : TreeViewEditorBase<VirtualButtonTreeAsset, VirtualButtonEditorWindow, VirtualButtonData, VirtualButtonTreeElement>
    {
        VirtualButtonContentEditor virtualbuttonContentEditor;
        [MenuItem("Virtual Training/Materi/Virtual Button Editor &v")]
        public static void GetWindow()
        {
            VirtualButtonEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualButtonEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
                return true;
            }
            return false;
        }

        public static VirtualButtonEditorWindow GetWindow(int instanceID)
        {
            VirtualButtonEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
            }
            return window;
        }

        public static VirtualButtonEditorWindow GetWindow(int instanceId, bool newWindow = false, bool alwaysInitialize = true)
        {
            VirtualButtonEditorWindow window = OpenEditorWindow(instanceId, newWindow);
            if (window != null)
            {
                if (alwaysInitialize || window.TreeView == null)
                {
                    window.Load();
                    window.Initialize();
                }
                return window;
            }
            return null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            virtualbuttonContentEditor = new VirtualButtonContentEditor(RecordUndo, () => WindowId);
        }

        protected override TreeViewWithTreeModel<VirtualButtonTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<VirtualButtonTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<VirtualButtonTreeElement>(TreeViewState, treeModel);
        }

        protected override VirtualButtonTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new VirtualButtonTreeElement("new virtual button", depth, id, false);
        }

        protected override void OnDisable()
        {
            if (virtualbuttonContentEditor != null)
            {
                virtualbuttonContentEditor.SaveObjectPosition();
            }
            base.OnDisable();
        }

        protected override void ContentView(List<VirtualButtonTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();
            for (int i = 0; i < selectedElements.Count; i++)
            {
                if (!selectedElements[0].isFolder)
                {
                    virtualbuttonContentEditor.ShowContent(selectedElements[i].VirtualButtonData, i);
                    selectedElements[i].VirtualButtonData.name = selectedElements[i].CompleteName;
                }
            }
            EditorGUILayout.EndVertical();
        }

        protected override void TopLeftButtonAction()
        {
            var vbDatabase = DatabaseManager.GetVirtualButtonDatabases();
            var virtualbuttonTree = DatabaseManager.GetVirtualButtonTree();

            List<VirtualButtonTreeElement> virtualbuttonList = new List<VirtualButtonTreeElement>();
            TreeElementUtility.TreeToList(virtualbuttonTree, virtualbuttonList);

            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("edit content config"), false, () =>
            {
                VirtualTrainingContentConfigEditor.ShowContentConfig();
            });

            for (int i = 0; i < vbDatabase.Count; i++)
            {
                VirtualButtonTreeAsset asset = vbDatabase[i];
                if (InstanceId != asset.GetInstanceID())
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " open -> " + asset.name), false, () =>
                    {
                        Close();
                        GetWindow(asset.GetInstanceID());
                    });
                }
                else
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " current virtual button -> " + asset.name), false, null);
                }
            }

            for (int i = 0; i < virtualbuttonList.Count; i++)
            {
                string name = virtualbuttonList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && virtualbuttonList[i].depth > -1)
                {
                    int elementId = virtualbuttonList[i].id;
                    menu.AddItem(new GUIContent("all content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            menu.ShowAsContext();
        }

        protected override bool UseTopLeftButton()
        {
            return true;
        }

        private void OpenDatabase(int elementId)
        {
            var databases = DatabaseManager.GetVirtualButtonDatabases();
            for (int d = 0; d < databases.Count; d++)
            {
                var elements = databases[d].GetData().treeElements;
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elementId == elements[i].id)
                    {
                        if (InstanceId != databases[d].GetInstanceID())
                        {
                            Close();
                            var window = GetWindow(databases[d].GetInstanceID(), false);
                            window.TreeView.SetSelection(new List<int> { elementId });
                        }
                        else
                        {
                            TreeView.SetSelection(new List<int> { elementId });
                        }
                        break;
                    }
                }
            }
        }
    }
}

#endif

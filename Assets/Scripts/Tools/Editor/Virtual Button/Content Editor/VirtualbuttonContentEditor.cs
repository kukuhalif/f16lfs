﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public partial class VirtualButtonContentEditor : PropertyField
    {
        VirtualButtonDataModel vbDataCurrent;
        bool isEditingObject;
        string currentControlName;
        GameObject maxObjectTemp;
        GameObject minObjectTemp;

        GameObject parentTemp;
        GameObject objTemp00;
        GameObject objTemp01;
        GameObject objTemp10;
        GameObject objTemp11;

        private GameObject currentInteractionObject;
        /// <summary>
        /// Show Content Virtual Button
        /// </summary>
        /// <param name="virtualbuttonData"><data in virtual button data model/param>
        /// <param name="index"><index data model in list parent/param>
        public void ShowContent(VirtualButtonDataModel virtualbuttonData, int index)
        {
            vbDataCurrent = virtualbuttonData;
            if (Application.isPlaying)
                isAppPlaying = true;
            else
                isAppPlaying = false;

            EditorGUI.BeginDisabledGroup(isAppPlaying == false);
            if (GUILayout.Button(isPreview ? "Stop Preview" : "Virtual Button Preview"))
            {
                isPreview = !isPreview;
                if (isPreview)
                {
                    EventManager.TriggerEvent(new VirtualButtonEvent(new List<VirtualButtonDataModel>() { virtualbuttonData }));
                    EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
                }
                else
                {
                    EventManager.TriggerEvent(new VirtualButtonEventDestroy());
                    EventManager.TriggerEvent(new ResetPartObjectEvent());
                }

            }
            EditorGUI.EndDisabledGroup();
            ShowTab("categories", virtualbuttonData, index.ToString() + 1);

        }

        void ShowVirtualButton(VirtualButtonDataModel virtualButtonData, int index)
        {
            int idx = 0;
            EditorGUI.BeginDisabledGroup(isEditingObject == true);
            GUILayout.BeginVertical();
            ShowSimpleField("Virtual Button Type:", index.ToString() + idx++, ref virtualButtonData.vbType);

            switch ((int)virtualButtonData.vbType)
            {
                case 0:
                    ShowVBClick(virtualButtonData, index.ToString() + idx++);
                    break;
                case 1:
                    ShowVBDrag(virtualButtonData, index.ToString() + idx++);
                    break;
                case 2:
                    ShowVBPress(virtualButtonData, index.ToString() + idx++);
                    break;
                default:
                    ShowVBClick(virtualButtonData, index.ToString() + idx++);
                    break;
            }

            GUILayout.EndVertical();
            EditorGUI.EndDisabledGroup();
        }

        void ShowVBDrag(VirtualButtonDataModel virtualbuttonDataChild, string index)
        {
            int idx = 0;
            ShowSlider("Default Value", index + idx++, ref virtualbuttonDataChild.defaultValue, 0, 1);
            if (!virtualbuttonDataChild.isSequentialDrag)
            {
                ShowSimpleField("Elastic", index + idx++, ref virtualbuttonDataChild.isElastic);
                EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.isElastic == false);
                ShowSimpleField("Elastic Speed", index + idx++, ref virtualbuttonDataChild.elasticSpeed);
                EditorGUI.EndDisabledGroup();
            }
            ShowSimpleField("Axis Count", index + idx++, ref virtualbuttonDataChild.axisCount);
            //ShowSimpleField("Drag Direction", index + idx++, ref virtualbuttonDataChild.direction);
            if (virtualbuttonDataChild.clickTargetDatas.Count > 0)
            {
                //if (virtualbuttonDataChild.direction == DragDirection.Horizontal)
                //    EditObjectPosition(virtualbuttonDataChild, true, virtualbuttonDataChild.clickTargetDatas[0].gameObject.gameObject, "Edit Position Horizontal");

                //else if (virtualbuttonDataChild.direction == DragDirection.Vertical)
                //    EditObjectPosition(virtualbuttonDataChild, false, virtualbuttonDataChild.clickTargetDatas[0].gameObject.gameObject, "Edit Position Vertical");
                //else
                //{
                //    //EditObjectPosition(virtualbuttonDataChild, true, virtualbuttonDataChild.clickTargetDatas[0].gameObject.gameObject, "Edit Position Horizontal");
                //    //EditObjectPosition(virtualbuttonDataChild, true, virtualbuttonDataChild.clickTargetDatas[0].gameObject.gameObject, "Edit Position Horizontal");
                //    EditObjectPositionDrag(virtualbuttonDataChild, virtualbuttonDataChild.clickTargetDatas[0].gameObject.gameObject, "Edit Position 2 Axis");
                //}

            }
            EditorGUILayout.BeginHorizontal();

            //EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.direction == DragDirection.Vertical);
            //ShowSimpleField("Delta Position X", index + idx++, ref virtualbuttonDataChild.deltaPositionX);
            //EditorGUI.EndDisabledGroup();

            //EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.direction == DragDirection.Horizontal);
            //ShowSimpleField("Delta Position Y", index + idx++, ref virtualbuttonDataChild.deltaPositionY);
            //EditorGUI.EndDisabledGroup();

            //if (virtualbuttonDataChild.direction == DragDirection.Both)
            //{
            //    virtualbuttonDataChild.isSequentialDrag = false;
            //}

            if (virtualbuttonDataChild.axisCount == DirectionAxisCount.TwoAxis)
            {
                virtualbuttonDataChild.isSequentialDrag = false;
            }

            EditorGUILayout.EndHorizontal();

            if (/*virtualbuttonDataChild.direction != DragDirection.Both*/virtualbuttonDataChild.axisCount == DirectionAxisCount.OneAxis)
            {
                ShowSimpleField("Sequential", index + idx++, ref virtualbuttonDataChild.isSequentialDrag);
                if (virtualbuttonDataChild.isSequentialDrag == true)
                {
                    ShowSimpleField("Sequence Method", index + idx++, ref virtualbuttonDataChild.sequencePlayMethod);
                    if (virtualbuttonDataChild.isSequentialDrag)
                    {
                        if (virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampParameter ||
               virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation ||
               virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                        {
                            ShowSimpleField("Time Stamp Value Type", index.ToString() + idx++, ref virtualbuttonDataChild.vbPlayStampValueType);
                            ShowListField("Time Stamp Values", virtualbuttonDataChild.sequentialDragDatas, index.ToString() + idx++);
                        }
                    }
                }
            }
        }
        /*[DrawGizmo(GizmoType.Selected | GizmoType.Active)]
        static void DrawGizmoForMyScript(Vector3 position)
        {
            Gizmos.DrawCube(position, new Vector3(1, 1, 1));
        }*/

        void MakeAnObjectPosition(GameObject objectSelection,GameObject parentTemp,Vector3 positionObject,ref GameObject objectTemp , string nameObject)
        {
            objectSelection.AddComponent<GizmosVB>();
            objectSelection.name = nameObject;
            objectSelection.transform.SetParent(currentInteractionObject.transform);
            objectSelection.transform.localPosition = positionObject;
            objectSelection.transform.SetParent(parentTemp.transform);
            objectSelection.transform.SetAsLastSibling();
            objectTemp = objectSelection;
        }

        void DestroyObject(GameObject objectDestroy)
        {
            if (isAppPlaying)
                UnityEngine.Object.Destroy(objectDestroy.gameObject);
            else
                UnityEngine.Object.DestroyImmediate(objectDestroy.gameObject);
        }

        void EditObjectPosition2Axis(ClickTargetData clickTarget, GameObject interactionObject, string controlName, string buttonName)
        {
            if (!isEditingObject)
            {
                if (GUILayout.Button(buttonName))
                {
                    this.currentInteractionObject = interactionObject;
                    clickTargetDataTemp = clickTarget;
                    Vector3 position00;
                    Vector3 position01;
                    Vector3 position10;
                    Vector3 position11;

                    position00 = clickTarget.minimalPositionXY;
                    position01 = clickTarget.maximalPositionX;
                    position10 = clickTarget.maximalPositionY;
                    position11 = clickTarget.maximalPositionXY;

                    currentControlName = buttonName + controlName;
                    isEditingObject = true;

                    parentTemp = new GameObject();
                    Vector3 scaleInteraction = new Vector3(interactionObject.transform.lossyScale.x, interactionObject.transform.lossyScale.y, interactionObject.transform.lossyScale.z);
                    parentTemp.transform.localScale = scaleInteraction;
                    parentTemp.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform);
                    parentTemp.name = "Parent Temp";
                    Debug.Log("minx " + clickTarget.minimalPositionXY + " interaction " + interactionObject.transform.position);

                    GameObject object00 = new GameObject();
                    MakeAnObjectPosition(object00, parentTemp, position00,ref objTemp00, "Posisi Minimal X dan Y ");

                    GameObject object01 = new GameObject();
                    MakeAnObjectPosition(object01, parentTemp, position01,ref objTemp01, "Posisi Maksimal X");

                    GameObject object10 = new GameObject();
                    MakeAnObjectPosition(object10, parentTemp, position10,ref objTemp10, "Posisi Maksimal Y");

                    GameObject object11 = new GameObject();
                    MakeAnObjectPosition(object11, parentTemp, position11,ref objTemp11, "Posisi Maksimal X dan Y");

                    EditorGUIUtility.PingObject(object11);
                    Selection.activeGameObject = object11;
                }
            }
            else
            {
                if (buttonName + controlName == currentControlName )
                {
                    if (GUILayout.Button("Save Position"))
                    {
                        if (EditorUtility.DisplayDialog("save position", "save position ?", "yes", "no"))
                        {
                            SaveObjectPosition();
                        }
                    }

                    if (GUILayout.Button("Reset Position"))
                    {
                        if (EditorUtility.DisplayDialog("reset position", "reset position ?", "yes", "no"))
                        {
                            ResetObjectPosition(interactionObject);
                        }
                    }
                }
            }
        }

        void EditObjectPosition(ClickTargetData vbData, GameObject interactionObject, string controlName,string buttonName)
        {
            if (!isEditingObject)
            {
                if (GUILayout.Button(buttonName))
                {
                    this.currentInteractionObject = interactionObject;
                    clickTargetDataTemp = vbData;

                    //Vector3 positionMax = interactionObject.transform.position + vbData.maximalPositionX;
                    //Vector3 positionMin= interactionObject.transform.position + vbData.minimalPositionXY;

                    Vector3 positionMax = vbData.maximalPositionX;
                    Vector3 positionMin = vbData.minimalPositionXY;

                    currentControlName = buttonName + controlName;
                    isEditingObject = true;

                    parentTemp = new GameObject();
                    Vector3 scaleInteraction = new Vector3(interactionObject.transform.lossyScale.x, interactionObject.transform.lossyScale.y, interactionObject.transform.lossyScale.z);
                    parentTemp.transform.localScale = scaleInteraction;
                    parentTemp.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform);
                    parentTemp.name = "Parent Temp";

                    GameObject maxObject = new GameObject();
                    MakeAnObjectPosition(maxObject, parentTemp, positionMax,ref objTemp01, "Posisi Maximal");

                    GameObject minObject = new GameObject();
                    MakeAnObjectPosition(minObject, parentTemp, positionMin, ref objTemp00, "Posisi Minimal");

                    EditorGUIUtility.PingObject(minObject);
                    Selection.activeGameObject = minObject;
                }
            }
            else
            {
                if (buttonName + controlName == currentControlName )
                {
                    if(GUILayout.Button("Save Position"))
                    {
                        if (EditorUtility.DisplayDialog("save position", "save position ?", "yes", "no"))
                        {
                            SaveObjectPosition();
                        }
                    }

                    if (GUILayout.Button("Reset Position"))
                    {
                        if (EditorUtility.DisplayDialog("reset position", "reset position ?", "yes", "no"))
                        {
                            ResetObjectPosition(interactionObject);
                        }
                    }
                }
            }
        }

        void ResetObjectPosition(GameObject interactionObject)
        {
            if (clickTargetDataTemp == null)
                return;

            if (objTemp00 != null)
            {
                objTemp00.transform.localPosition = interactionObject.transform.position + clickTargetDataTemp.minimalPositionXY;
            }
            if (objTemp01 != null)
            {
                objTemp01.transform.localPosition = interactionObject.transform.position + clickTargetDataTemp.maximalPositionX;
            }
            if (objTemp10 != null)
            {
                objTemp10.transform.localPosition = interactionObject.transform.position + clickTargetDataTemp.maximalPositionY;
            }
            if (objTemp11 != null)
            {
                objTemp11.transform.localPosition = interactionObject.transform.position + clickTargetDataTemp.maximalPositionXY;
            }
        }

        public void SaveObjectPosition()
        {
            if (clickTargetDataTemp == null || currentInteractionObject == null)
                return;

            if (objTemp00 != null)
            {
                objTemp00.transform.SetParent(currentInteractionObject.transform);
                clickTargetDataTemp.minimalPositionXY = objTemp00.transform.localPosition;
                Debug.Log("MinXY " + objTemp00.transform.localPosition + " All " + objTemp00.transform.position);
                DestroyObject(objTemp00);
            }

            if (objTemp01 != null)
            {
                objTemp01.transform.SetParent(currentInteractionObject.transform);
                clickTargetDataTemp.maximalPositionX = objTemp01.transform.localPosition;
                DestroyObject(objTemp01);
            }

            if (objTemp10 != null)
            {
                objTemp10.transform.SetParent(currentInteractionObject.transform);
                clickTargetDataTemp.maximalPositionY = objTemp10.transform.localPosition;
                DestroyObject(objTemp10);
            }

            if (objTemp11 != null)
            {
                objTemp11.transform.SetParent(currentInteractionObject.transform);
                clickTargetDataTemp.maximalPositionXY = objTemp11.transform.localPosition;
                DestroyObject(objTemp11);
            }

            if (parentTemp != null)
            {
                DestroyObject(parentTemp);
            }

            isEditingObject = false;

            clickTargetDataTemp = null;
            objTemp00 = null;
            objTemp01 = null;
            objTemp10 = null;
            objTemp11 = null;
        }

        void ShowVBClick(VirtualButtonDataModel virtualbuttonDataChild, string index)
        {
            int idx = 0;
            ShowSlider("Default Value", index + idx++, ref virtualbuttonDataChild.defaultValue, 0, 1);
            ShowSimpleField("Sequential", index + idx++, ref virtualbuttonDataChild.isSequentialClick);

            if (virtualbuttonDataChild.isSequentialClick)
            {
                ShowSimpleField("Sequence Method", index + idx++, ref virtualbuttonDataChild.sequencePlayMethod);
                if (virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampParameter ||
    virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation ||
    virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                {
                    ShowSimpleField("Time Stamp Value Type", index.ToString() + idx++, ref virtualbuttonDataChild.vbPlayStampValueType);
                    ShowListField("Time Stamp Values", virtualbuttonDataChild.sequentialClickDatas, index.ToString() + idx++);
                }
            }
        }

        void ShowVBPress(VirtualButtonDataModel virtualbuttonDataChild, string index)
        {
            int idx = 0;
            ShowSlider("Default Value", index + idx++, ref virtualbuttonDataChild.defaultValue, 0, 1);
            ShowSimpleField("Return to start", index + idx++, ref virtualbuttonDataChild.isReturnToStart);

            ShowSimpleField("Elastic", index + idx++, ref virtualbuttonDataChild.isElastic);

            EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.isElastic == false);
            ShowSimpleField("Elastic Speed", index + idx++, ref virtualbuttonDataChild.elasticSpeed);
            ShowSimpleField("Elastic Type", index + idx++, ref virtualbuttonDataChild.elasticType);
            EditorGUI.EndDisabledGroup();

            if (virtualbuttonDataChild.isElastic && virtualbuttonDataChild.elasticType == ElasticType.Both)
                virtualbuttonDataChild.isReturnToStart = false;
        }

        void ShowVbModifierSetActive(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            ShowListField("List Object Interaction Modifier", modifierData.interactionObjectDatas, listid + idx++);
        }
        void ShowVbModifierBlink(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            GUILayout.Label("variable blink");
            ShowSimpleField("Blink", listid + idx++, ref modifierData.isBlink);

            if (modifierData.isBlink)
            {
                ShowSimpleField("Delay Blinking Time", listid + idx++, ref modifierData.delayTimeBlink);
                ShowSimpleField("Minimal Intensity", listid + idx++, ref modifierData.minIntensity);
                ShowSimpleField("Maximal Intensity", listid + idx++, ref modifierData.maxIntensity);
            }

            ShowSimpleField("Change Color", listid + idx++, ref modifierData.isChangeColor);

            if (modifierData.isChangeColor)
            {
                ShowSimpleField("Delay Color Time", listid + idx++, ref modifierData.delayTimeColor);
                ShowSimpleField("Color 1", listid + idx++, ref modifierData.color1);
                ShowSimpleField("Color 2", listid + idx++, ref modifierData.color2);
            }
            ShowSimpleField("Recursive Child", listid + idx++, ref modifierData.isRecursiveChild);
            ShowListField("List Object Interaction Modifier", modifierData.interactionObjectDatas, listid + idx++);
        }

        void ShowVbModifierSkybox(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            GUILayout.Label("variable skybox");
            ShowSimpleField("Click Skybox Type", listid + idx++, ref modifierData.skyBoxType);
            EditorGUI.BeginDisabledGroup(modifierData.skyBoxType == VBSKyboxType.Value);
            ShowSimpleField("Speed", listid + idx++, ref modifierData.speed);
            EditorGUI.EndDisabledGroup();

            ShowSimpleField("Destination Exposure", listid + idx++, ref modifierData.dayExposure);
            ShowSimpleField("Destination Intensity Multiplier", listid + idx++, ref modifierData.dayIntensityMultiplier);
            ShowSimpleField("Light Object", listid + idx++, ref modifierData.light);
            if (modifierData.light.gameObject != null)
            {
                EditorGUILayout.BeginHorizontal();
                ShowSimpleField("Transform X", listid + idx++, ref modifierData.lightTransformX);
                ShowSimpleField("Transform Y", listid + idx++, ref modifierData.lightTransformY);
                ShowSimpleField("Transform Z", listid + idx++, ref modifierData.lightTransformZ);
                EditorGUILayout.EndHorizontal();
            }
        }
        void ShowModifierExceptValue(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            GUILayout.Label("variable except values");
            ShowSimpleField("Except Value Type", listid + idx++, ref modifierData.exceptValueType);
        }

        void ShowEffectLerp(VirtualButtonEffectData vbEffect, string listid)
        {
            int idx = 0;
            ShowSimpleField("Animator", listid + idx++, ref vbEffect.animator);
            ShowSimpleField("Min Value", listid + idx++, ref vbEffect.minValue);
            ShowSimpleField("Max Value", listid + idx++, ref vbEffect.maxValue);
            ShowSimpleField("Parameter", listid + idx++, ref vbEffect.parameter);
            ShowSlider("Speed Acceleration", listid + idx++, ref vbEffect.speedAcc, 0, 1);
        }
    }
}
#endif

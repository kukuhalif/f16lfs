#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VirtualTraining.Core;
using System.IO;

namespace VirtualTraining.Tools
{
    public class RemovedDatabaseChecker : EditorWindow
    {
        [MenuItem("Virtual Training/Database Tools/Removed Database Checker")]
        public static void OpenWindow()
        {
            var window = GetWindow<RemovedDatabaseChecker>();
            window.titleContent = new GUIContent("Removed Database Checker");
        }

        PropertyField propertyField;
        List<ScriptableObject> toDeleteDatabases = new List<ScriptableObject>();

        private void OnEnable()
        {
            propertyField = new PropertyField(null, "Removed Database Checker");
            propertyField.CreateListView("removed database", null, null, OnRemovedDatabaseCallback, null);
        }

        private object OnRemovedDatabaseCallback(string listId, object element, int index)
        {
            ScriptableObject scriptableObject = element as ScriptableObject;

            propertyField.ShowAssetField(listId, listId + index, ref scriptableObject);

            return scriptableObject;
        }

        private void OnGUI()
        {
            GUILayout.Label("mencari file database maintenance, troubleshoot, flight scenario dan remove install yang telah di hapus di materi database");
            if (GUILayout.Button("scan " + DatabaseManager.GetContentPathConfig().contentAssetPath + " directory"))
            {
                toDeleteDatabases.Clear();
                ScanDatabases();
            }

            propertyField.ShowListField("removed database", toDeleteDatabases);
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("remove files"))
            {
                if (EditorUtility.DisplayDialog("Removed Database Checker", "remove all files in list ?", "yes", "no"))
                {
                    foreach (var file in toDeleteDatabases)
                    {
                        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(file));
                    }
                }

                toDeleteDatabases.Clear();
            }
        }

        private void ScanDatabases()
        {
            MateriElementType[] types = Enum.GetValues(typeof(MateriElementType)) as MateriElementType[];
            for (int i = 0; i < types.Length; i++)
            {
                if (types[i] != MateriElementType.Materi)
                    ScanDatabase(types[i]);
            }
        }

        private void ScanDatabase(MateriElementType materiType)
        {
            if (!Directory.Exists(Application.dataPath + "/Content Files/" + materiType.ToString()))
                return;

            string[] fileEntries = Directory.GetFiles(Application.dataPath + "/" + "Content Files/" + materiType.ToString());

            foreach (string fileName in fileEntries)
            {
                int index = fileName.LastIndexOf("/");
                string localPath = "Assets/" + "Content Files";

                if (index > 0)
                    localPath += fileName.Substring(index);

                List<MateriContainer> config = DatabaseManager.GetMateriConfigDatas();
                ScriptableObject databaseFile = null;

                switch (materiType)
                {
                    case MateriElementType.Maintenance:
                        databaseFile = AssetDatabase.LoadAssetAtPath(localPath, typeof(MaintenanceTreeAsset)) as MaintenanceTreeAsset;
                        break;
                    case MateriElementType.Troubleshoot:
                        databaseFile = AssetDatabase.LoadAssetAtPath(localPath, typeof(TroubleshootGraph)) as TroubleshootGraph;
                        break;
                    case MateriElementType.FlightScenario:
                        databaseFile = AssetDatabase.LoadAssetAtPath(localPath, typeof(FlightScenarioTreeAsset)) as FlightScenarioTreeAsset;
                        break;
                    case MateriElementType.RemoveInstall:
                        databaseFile = AssetDatabase.LoadAssetAtPath(localPath, typeof(RemoveInstallTreeAsset)) as RemoveInstallTreeAsset;
                        break;
                }


                if (databaseFile != null)
                {
                    ScriptableObject databaseFromMateri = null;
                    bool deleteFile = true;

                    for (int i = 0; i < config.Count; i++)
                    {
                        if (config[i].materi != null)
                            for (int j = 0; j < config[i].materi.GetData().treeElements.Count; j++)
                            {
                                switch (materiType)
                                {
                                    case MateriElementType.Maintenance:
                                        databaseFromMateri = config[i].materi.GetData().treeElements[j].MateriData.maintenanceTree;
                                        break;
                                    case MateriElementType.Troubleshoot:
                                        databaseFromMateri = config[i].materi.GetData().treeElements[j].MateriData.troubleshootData.troubleshootGraph;
                                        break;
                                    case MateriElementType.FlightScenario:
                                        databaseFromMateri = config[i].materi.GetData().treeElements[j].MateriData.flightScenarioTree;
                                        break;
                                    case MateriElementType.RemoveInstall:
                                        databaseFromMateri = config[i].materi.GetData().treeElements[j].MateriData.removeInstallTree;
                                        break;
                                }

                                if (databaseFromMateri != null && databaseFile.GetInstanceID() == databaseFromMateri.GetInstanceID())
                                {
                                    deleteFile = false;
                                    break;
                                }
                            }
                    }

                    if (deleteFile == true)
                    {
                        toDeleteDatabases.Add(databaseFile);
                    }
                }
            }
        }
    }
}

#endif
#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class ReplaceDescriptionTextTool : EditorWindowDatabaseBase<StringPairDatabase, ReplaceDescriptionTextTool, StringPairData>
    {
        [MenuItem("Virtual Training/Database Tools/Replace Description Text")]
        public static void GetWindow()
        {
            ReplaceDescriptionTextTool window = OpenEditorWindow();
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            ReplaceDescriptionTextTool window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                return true;
            }
            return false;
        }

        PropertyField propertyField;
        List<string> databases;
        Vector2 scrollPos;

        protected override void OnEnable()
        {
            base.OnEnable();

            scrollPos = new Vector2();
            propertyField = new PropertyField(null, "remove invalid character");
            propertyField.CreateListView("text pair", null, null, OnElementCallback, null);
        }

        private object OnElementCallback(string listId, object element, int index)
        {
            StringPair pair = element as StringPair;
            propertyField.ShowSimpleField("old text", "old" + listId + index, ref pair.OldString);
            GUILayout.Space(30);
            propertyField.ShowSimpleField("new text", "new" + listId + index, ref pair.newString);
            return pair;
        }

        protected override void RenderGUI()
        {
            scrollPos = GUILayout.BeginScrollView(scrollPos);

            GUILayout.Space(20);

            propertyField.ShowListField("text pair", ScriptableObjectTemp.GetData().pairs);

            if (databases != null)
                for (int i = 0; i < databases.Count; i++)
                {
                    GUILayout.Label(databases[i] + " updated");
                }

            GUILayout.EndScrollView();

            if (GUILayout.Button("replace text"))
            {
                ReplaceCharacter();
            }

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("load pair data"))
            {
                if (EditorUtility.DisplayDialog("config", "load ?", "ok", "cancel"))
                    Load();
            }

            if (GUILayout.Button("save pair data"))
            {
                if (EditorUtility.DisplayDialog("config", "save ?", "ok", "cancel"))
                {
                    Save();
                    propertyField.LoadColor();
                }
            }

            GUILayout.EndHorizontal();
        }

        private void GetAssetDatabase(Type type, Action<object> updateCallback)
        {
            string typeName = type.ToString();
            string[] assetsGUID = AssetDatabase.FindAssets("t:" + typeName);

            foreach (var assetGUID in assetsGUID)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(assetGUID);
                updateCallback(AssetDatabase.LoadAssetAtPath(assetPath, type));
            }
        }

        private void ReplaceCharacter()
        {
            GetAssetDatabase(typeof(MateriTreeAsset), AssetCallback);

            AssetDatabase.SaveAssets();
        }

        private void AssetCallback(object asset)
        {
            MateriTreeAsset materiDatabase = asset as MateriTreeAsset;

            var materiTreeElements = materiDatabase.GetData().treeElements;

            databases = new List<string>();

            databases.Add(materiDatabase.name);

            foreach (var materiElement in materiTreeElements)
            {
                // materi data
                materiElement.MateriData.description = UpdateText(materiElement.MateriData.description);

                // maintenance data
                if (materiElement.MateriData.maintenanceTree != null && materiElement.MateriData.maintenanceTree.GetData().treeElements != null)
                {
                    var maintenanceElement = materiElement.MateriData.maintenanceTree.GetData().treeElements;
                    foreach (var maintenance in maintenanceElement)
                    {
                        maintenance.name = UpdateText(maintenance.name);
                        maintenance.data.title = UpdateText(maintenance.data.title);
                        maintenance.data.description = UpdateText(maintenance.data.description);
                    }

                    databases.Add(materiDatabase.name + " maintenance");
                }

                // troubleshoot data
                if (materiElement.MateriData.troubleshootData != null && materiElement.MateriData.troubleshootData.troubleshootGraph != null)
                {
                    var troubleshootElement = materiElement.MateriData.troubleshootData.troubleshootGraph.nodes;
                    foreach (var troubleshoot in troubleshootElement)
                    {
                        TroubleshootNode troubleshootNode = troubleshoot as TroubleshootNode;
                        troubleshootNode.name = UpdateText(troubleshootNode.name);
                        troubleshootNode.content.troubleshootData.title = UpdateText(troubleshootNode.content.troubleshootData.title);
                        troubleshootNode.content.troubleshootData.description = UpdateText(troubleshootNode.content.troubleshootData.description);
                    }

                    databases.Add(materiDatabase.name + " troubleshoot");

                    EditorUtility.SetDirty(materiElement.MateriData.troubleshootData.troubleshootGraph);
                }
            }

            EditorUtility.SetDirty(materiDatabase);
        }

        private string UpdateText(string text)
        {
            for (int i = 0; i < ScriptableObjectTemp.GetData().pairs.Count; i++)
            {
                text = text.Replace(ScriptableObjectTemp.GetData().pairs[i].OldString, ScriptableObjectTemp.GetData().pairs[i].newString);
            }

            return text;
        }
    }
}

#endif
#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.IO;

public class RemoveAllTempAsset
{
    [MenuItem("Virtual Training/Database Tools/Remove Temp Asset")]
    public static void RemoveTemp()
    {
        if (EditorUtility.DisplayDialog("remove temp", "close all databases, or your data may be lost", "ok sudah ditutup semua", "belum saya tutup"))
        {
            if (!Directory.Exists(Application.dataPath + "/Temp"))
            {
                return;
            }
            string[] tempFolder = { "Assets/Temp" };
            foreach (var asset in AssetDatabase.FindAssets("", tempFolder))
            {
                var path = AssetDatabase.GUIDToAssetPath(asset);
                AssetDatabase.DeleteAsset(path);
            }
        }
    }
}
#endif 

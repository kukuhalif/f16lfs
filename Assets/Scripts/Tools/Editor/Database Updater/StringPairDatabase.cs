﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [System.Serializable]
    public class StringPair
    {
        public string OldString;
        public string newString;

        public StringPair()
        {
            OldString = "";
            newString = "";
        }
    }

    [System.Serializable]
    public class StringPairData
    {
        public List<StringPair> pairs;

        public StringPairData()
        {
            pairs = new List<StringPair>();
        }
    }

    [CreateAssetMenu(fileName = "String Pair", menuName = "Virtual Training/Tools/String Pair")]
    public class StringPairDatabase : ScriptableObjectBase<StringPairData>
    {
        [SerializeField] StringPairData pairData;

        public override StringPairData GetData()
        {
            return pairData;
        }

        public override void SetData(StringPairData data)
        {
            pairData = data;
        }
    }
}

#endif
#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class CheckDuplicatedTreeElementId
    {
        [MenuItem("Virtual Training/Database Tools/Check duplicated tree element id (materi, virtual button and scripted animation)")]
        public static void DoCheck()
        {
            var contentConfig = Resources.Load<VirtualTrainingContentConfig>("content config");
            ContentConfig config = contentConfig.GetData();

            // materi
            CheckTreeElementDuplicateId materiCheck = new CheckTreeElementDuplicateId(MateriRegenerateIdCallback);
            for (int i = 0; i < config.materiContainers.Count; i++)
            {
                var elements = config.materiContainers[i].materi.GetData().treeElements;
                for (int j = 0; j < elements.Count; j++)
                {
                    materiCheck.Check(elements[j].id, elements[j], config.materiContainers[i].materi);
                }
                EditorUtility.SetDirty(config.materiContainers[i].materi);
            }

            // virtual button
            CheckTreeElementDuplicateId vbCheck = new CheckTreeElementDuplicateId(VBRegenerateIdCallback);
            for (int i = 0; i < config.virtualButtonDatabases.Count; i++)
            {
                var elements = config.virtualButtonDatabases[i].GetData().treeElements;
                for (int j = 0; j < elements.Count; j++)
                {
                    vbCheck.Check(elements[j].id, elements[j], config.virtualButtonDatabases[i]);
                }
                EditorUtility.SetDirty(config.virtualButtonDatabases[i]);
            }

            // scripted animation
            CheckTreeElementDuplicateId animCheck = new CheckTreeElementDuplicateId(ScriptedAnimRegenerateIdCallback);
            for (int i = 0; i < config.scriptedAnimationDatabases.Count; i++)
            {
                var elements = config.scriptedAnimationDatabases[i].GetData().treeElements;
                for (int j = 0; j < elements.Count; j++)
                {
                    animCheck.Check(elements[j].id, elements[j], config.scriptedAnimationDatabases[i]);
                }
                EditorUtility.SetDirty(config.scriptedAnimationDatabases[i]);
            }

            AssetDatabase.SaveAssets(); //save file immediately
            Debug.Log("done !");
        }

        private static int MateriRegenerateIdCallback(object element, Object file)
        {
            MateriTreeElement materiElement = element as MateriTreeElement;
            int newId = MateriGenerateElementId(materiElement, file);
            materiElement.id = newId;
            Debug.Log("materi id check, new id for -> " + materiElement.name + " -> " + newId);
            return newId;
        }

        private static int VBRegenerateIdCallback(object element, Object file)
        {
            VirtualButtonTreeElement vbElement = element as VirtualButtonTreeElement;
            int newId = VBGenerateElementId(vbElement, file);
            vbElement.id = newId;
            Debug.Log("vb id check, new id for -> " + vbElement.name + " -> " + newId);
            return newId;
        }

        private static int ScriptedAnimRegenerateIdCallback(object element, Object file)
        {
            ScriptedAnimationTreeElement animElement = element as ScriptedAnimationTreeElement;
            int newId = ScriptedAnimGenerateElementId(animElement, file);
            animElement.id = newId;
            Debug.Log("scripted anim id check, new id for -> " + animElement.name + " -> " + newId);
            return newId;
        }

        private static int MateriGenerateElementId(TreeElement element, Object file)
        {
            MateriTreeAsset asset = file as MateriTreeAsset;

            int id = 0;
            int instanceId = file.GetInstanceID();
            int elementCount = 0;
            for (int i = 0; i < asset.GetData().treeElements.Count; i++)
            {
                if (asset.GetData().treeElements[i] == element as MateriTreeElement)
                {
                    elementCount = i;
                    break;
                }
            }

            int random = UnityEngine.Random.Range(-10000, 10000);

            string st = instanceId.ToString() + elementCount.ToString() + random.ToString() + element.CompleteName;
            id = Animator.StringToHash(st);

            return id;
        }

        private static int VBGenerateElementId(TreeElement element, Object file)
        {
            VirtualButtonTreeAsset asset = file as VirtualButtonTreeAsset;

            int id = 0;
            int instanceId = file.GetInstanceID();
            int elementCount = 0;
            for (int i = 0; i < asset.GetData().treeElements.Count; i++)
            {
                if (asset.GetData().treeElements[i] == element as VirtualButtonTreeElement)
                {
                    elementCount = i;
                    break;
                }
            }

            int random = UnityEngine.Random.Range(-10000, 10000);

            string st = instanceId.ToString() + elementCount.ToString() + random.ToString() + element.CompleteName;
            id = Animator.StringToHash(st);

            return id;
        }

        private static int ScriptedAnimGenerateElementId(TreeElement element, Object file)
        {
            ScriptedAnimationTreeAsset asset = file as ScriptedAnimationTreeAsset;

            int id = 0;
            int instanceId = file.GetInstanceID();
            int elementCount = 0;
            for (int i = 0; i < asset.GetData().treeElements.Count; i++)
            {
                if (asset.GetData().treeElements[i] == element as ScriptedAnimationTreeElement)
                {
                    elementCount = i;
                    break;
                }
            }

            int random = UnityEngine.Random.Range(-10000, 10000);

            string st = instanceId.ToString() + elementCount.ToString() + random.ToString() + element.CompleteName;
            id = Animator.StringToHash(st);

            return id;
        }
    }
}

#endif
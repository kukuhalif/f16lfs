#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using VirtualTraining.Core;
using Unity.EditorCoroutines.Editor;

namespace VirtualTraining.Tools
{
    public class ModifiedAssetDetector : UnityEditor.AssetModificationProcessor
    {
        class NewAssetDetectorOwner
        {
            public IEnumerator OnCreatedAsset(string newFilePath)
            {
                yield return new WaitForChangedResult();

                var newFile = AssetDatabase.LoadMainAssetAtPath(newFilePath);

                if (newFile != null)
                {
                    Debug.Log("new asset detected : " + newFilePath);

                    // get regenerate tree element id method
                    var regenerateIdMethod = newFile.GetType().GetMethod("RegenerateAllTreeElementId");

                    if (regenerateIdMethod != null)
                    {
                        Debug.Log("regenerate all tree element id");
                        regenerateIdMethod.Invoke(newFile, null);

                        // save asset
                        EditorUtility.SetDirty(newFile);
                        AssetDatabase.SaveAssets();

                        Debug.Log(newFile.name + " tree element id regenerated");
                    }
                }
            }
        }

        class DeleteAssetDetectorOwner
        {
            public IEnumerator OnDeletedAsset(string deletedFilePath)
            {
                Debug.Log("deleted asset detected : " + deletedFilePath);

                yield return new WaitForChangedResult();

                DatabaseManager.OnDeletedAsset(deletedFilePath);
            }
        }

        // new asset callback
        static void OnWillCreateAsset(string assetPath)
        {
            if (assetPath.Contains("Assets/Temp"))
                return;

            NewAssetDetectorOwner routineOwner = new NewAssetDetectorOwner();

            EditorCoroutineUtility.StartCoroutine(routineOwner.OnCreatedAsset(assetPath), routineOwner);
        }

        static AssetDeleteResult OnWillDeleteAsset(string assetPath, RemoveAssetOptions options)
        {
            if (assetPath.Contains("Assets/Temp"))
                return AssetDeleteResult.DidNotDelete;

            DeleteAssetDetectorOwner routineOwner = new DeleteAssetDetectorOwner();

            EditorCoroutineUtility.StartCoroutine(routineOwner.OnDeletedAsset(assetPath), routineOwner);

            return AssetDeleteResult.DidNotDelete;
        }
    }
}
#endif
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class AsignLayerEditor : EditorWindow
    {
        bool recursive = true;
        [SerializeField] int layerValue;
        [SerializeField] public string layerTemp;
        List<string> nameLayers = new List<string>();
        List<int> layers = new List<int>();
        Camera mainCamera;

        [MenuItem("Virtual Training/Model/Assign Layer")]
        private static void ShowWindow()
        {
            var window = GetWindow(typeof(AsignLayerEditor));
            window.titleContent = new GUIContent("assign layer tool");
        }
        private void OnEnable()
        {
            GetLayer();
            mainCamera = Camera.main;
        }

        void SetLayerl(GameObject obj)
        {
            if (obj != null)
            {
                obj.layer = LayerMask.NameToLayer(layerTemp);
            }
            if (recursive == true)
            {
                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    SetLayerl(obj.transform.GetChild(i).gameObject);
                }
            }
        }
        void GetLayer()
        {
            SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
            SerializedProperty layerProp = tagManager.FindProperty("layers");
            for (int i = 0; i < layerProp.arraySize; i++)
            {
                SerializedProperty sp = layerProp.GetArrayElementAtIndex(i);
                if (sp.stringValue != "")
                {
                    nameLayers.Add(sp.stringValue);
                    layers.Add(LayerMask.GetMask(sp.stringValue));
                }
            }
        }

        public void OnGUI()
        {
            recursive = EditorGUILayout.Toggle("Recursive", recursive);
            layerValue = EditorGUILayout.Popup("Layer", layerValue, nameLayers.ToArray());
            layerTemp = nameLayers[layerValue];

            string names = "";

            if (Selection.gameObjects.Length == 0)
            {
                GUILayout.Label("please select target objects !");
                EditorGUI.BeginDisabledGroup(true);
            }
            else
                names = "on : ";

            var selection = Selection.gameObjects;

            for (int i = 0; i < selection.Length; i++)
            {
                if (i < selection.Length - 2)
                    names += selection[i].name + " , ";
                else if (i < selection.Length - 1)
                    names += selection[i].name + " and ";
                else
                    names += selection[i].name;
            }

            if (GUILayout.Button("Set Layer " + names))
            {
                if (EditorUtility.DisplayDialog("Set Layer", "are you sure ?", "yes", "no"))
                {
                    for (int i = 0; i < Selection.gameObjects.Length; i++)
                    {
                        SetLayerl(Selection.gameObjects[i]);
                        GetLayer();
                    }
                }
            }

            GUILayout.Space(10f);

            mainCamera = EditorGUILayout.ObjectField("Main Camera", mainCamera, typeof(Camera), true) as Camera;

            if (GUILayout.Button("Set Main Camera Culling Mask"))
            {
                if (mainCamera != null)
                {
                    int monitorLayer = LayerMask.GetMask("Monitor Camera");

                    for (int i = 0; i < layers.Count; i++)
                    {
                        if (layers[i] != monitorLayer)
                            mainCamera.cullingMask |= layers[i];
                    }
                }
            }

            if (Selection.gameObjects.Length == 0)
            {
                EditorGUI.EndDisabledGroup();
            }

            Repaint();
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Unity.EditorCoroutines.Editor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class BakeOutlineEffect : EditorWindow
    {
        [MenuItem("Virtual Training/Model/Bake Outline Effect")]
        static void OpenWindow()
        {
            var window = GetWindow<BakeOutlineEffect>();
            window.titleContent = new GUIContent("Bake outline");
        }

        [MenuItem("Virtual Training/Model/Bake Outline Effect", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        class ModelData
        {
            public GameObject model;

            public ModelData(object obj)
            {
                GameObject go = (GameObject)obj;
                if (go != null)
                    this.model = go;
            }

            public ModelData()
            {

            }
        }

        List<ModelData> models;
        List<Mesh> meshes;
        PropertyField propertyField;

        OutlineNormalVector database;

        private void OnEnable()
        {
            models = new List<ModelData>();
            propertyField = new PropertyField(null, "bake outline");
            propertyField.CreateListView("model", null, null, OnModelElementDrawCallback, null, null, false, typeof(GameObject));
            database = DatabaseManager.GetOutlineNormalVector();
        }

        private object OnModelElementDrawCallback(string listId, object element, int index)
        {
            ModelData model = element as ModelData;

            propertyField.ShowSimpleField("model", listId + index, ref model.model);

            return model;
        }

        private void OnGUI()
        {
            if (database.MeshCount() > 0 && GUILayout.Button("check null mesh"))
            {
                int nullCount = 0;
                for (int i = 0; i < database.MeshCount(); i++)
                {
                    var mesh = database.GetMesh(i);
                    if (mesh == null)
                    {
                        nullCount++;
                        Debug.Log("index : " + i + " is null");
                    }
                }

                database.RemoveNullMesh();
                Debug.Log("null count " + nullCount + " removed");
            }

            if (database.MeshCount() > 0 && GUILayout.Button("clear " + database.MeshCount() + " data"))
            {
                if (EditorUtility.DisplayDialog("bake outline", "are you sure ? clear previous data ?", "yes", "no"))
                    database.Clear();
            }

            if (GUILayout.Button("Bake"))
            {
                meshes = new List<Mesh>();

                for (int i = 0; i < models.Count; i++)
                {
                    GetSharedMeshes(models[i].model);
                }

                BakeMeshes();
            }

            propertyField.ShowListField("model", models);
        }

        private void GetSharedMeshes(GameObject model)
        {
            MeshFilter[] meshFilters = model.transform.GetAllComponentsInChilds<MeshFilter>();

            for (int i = 0; i < meshFilters.Length; i++)
            {
                if (meshFilters[i] == null)
                    continue;

                Mesh sharedMesh = meshFilters[i].sharedMesh;
                if (sharedMesh != null && !meshes.Contains(sharedMesh))
                    meshes.Add(sharedMesh);
            }
        }

        private void BakeMeshes()
        {
            string waitInfo = " start calculating smooth normal vector of " + meshes.Count + " meshes";
            EditorUtility.DisplayProgressBar("bake outline", waitInfo, 0f);

            Debug.Log("start baking");
            EditorCoroutineUtility.StartCoroutineOwnerless(MeshProcess());
        }

        private void SkinnedMeshProcess(SkinnedMeshRenderer skinnedMesh)
        {
            Debug.Log("skinned mesh renderer process : " + skinnedMesh.name);
            // Clear UV3 on skinned mesh renderers
            skinnedMesh.sharedMesh.uv4 = new Vector2[skinnedMesh.sharedMesh.vertexCount];
        }

        private IEnumerator MeshProcess()
        {
            Debug.Log("baking task started");

            for (int i = 0; i < meshes.Count; i++)
            {
                string info = "calculating ( " + i + "/" + meshes.Count + " ) : " + meshes[i].name;
                EditorUtility.DisplayProgressBar("bake outline", info, (float)i / (float)meshes.Count);

                // Group vertices by location
                var groups = meshes[i].vertices.Select((vertex, index) => new KeyValuePair<Vector3, int>(vertex, index)).GroupBy(pair => pair.Key);

                var smoothNormals = new List<Vector3>(meshes[i].normals);

                // Average normals for grouped vertices
                foreach (var group in groups)
                {
                    // Skip single vertices
                    if (group.Count() == 1)
                    {
                        continue;
                    }

                    // Calculate the average normal
                    var smoothNormal = Vector3.zero;

                    foreach (var pair in group)
                    {
                        smoothNormal += meshes[i].normals[pair.Value];
                    }

                    smoothNormal.Normalize();

                    // Assign smooth normal to each vertex
                    foreach (var pair in group)
                    {
                        smoothNormals[pair.Value] = smoothNormal;
                    }
                }

                //meshes[i].SetUVs(3, smoothNormals);
                database.SetData(meshes[i], smoothNormals);

                EditorUtility.SetDirty(meshes[i]);

                yield return null;
            }

            Debug.Log("baking end");
            EditorUtility.DisplayProgressBar("bake outline", "done!", 1f);

            yield return null;

            EditorUtility.ClearProgressBar();
            EditorUtility.SetDirty(database);
            AssetDatabase.SaveAssets();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VirtualTraining.Tools
{
    public class CheckTreeElementDuplicateId
    {
        Dictionary<int, object> elementLookup;
        // element , database file
        Func<object, UnityEngine.Object, int> regenerateIdCallback;

        public CheckTreeElementDuplicateId(Func<object, UnityEngine.Object, int> regenerateIdCallback)
        {
            elementLookup = new Dictionary<int, object>();
            this.regenerateIdCallback = regenerateIdCallback;
        }

        public void Check(int id, object element, UnityEngine.Object file)
        {
            if (elementLookup.ContainsKey(id))
            {
                int newId = regenerateIdCallback.Invoke(element, file);
                elementLookup.Add(newId, element);
            }
            else
            {
                elementLookup.Add(id, element);
            }
        }
    }
}

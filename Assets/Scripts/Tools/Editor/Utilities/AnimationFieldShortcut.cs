using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public static class AnimationFieldShortcut
    {
        static bool pressed;
        static GameObject[] selectedObjects;

        public static bool Pressed { get => pressed; }
        public static void SwitchOff()
        {
            pressed = false;
        }

        public static GameObject[] GetSelectedObjects()
        {
            return selectedObjects;
        }

        [MenuItem("Virtual Training Shortcut/Assign Selected Objects to Opened or Expanded Animation &g", false)]
        static void AssignObjectShortcut()
        {
            selectedObjects = Selection.gameObjects;
            pressed = true;
        }

        [MenuItem("Virtual Training Shortcut/Assign Selected Objects to Opened or Expanded Animation &g", true)]
        static bool AssignObjectShortcutVerify()
        {
            return !string.IsNullOrEmpty(VirtualTrainingEditorState.GetLastFocusedWindowId());
        }
    }
}


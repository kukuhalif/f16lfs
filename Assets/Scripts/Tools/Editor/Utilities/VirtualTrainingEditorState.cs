﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public static class VirtualTrainingEditorState
    {
        static VirtualTrainingEditorState()
        {
            TREE_VIEW_STATE_LOOKUP = new Dictionary<string, TreeViewState>();
            TAB_STATE_LOOKUP = new Dictionary<string, int>();
            FOLDOUT_STATE_LOOKUP = new Dictionary<string, bool>();
            TREE_ELEMENTS = new List<object>();
            LIST_ELEMENTS = new List<object>();
        }

        public static void SaveStates(string windowId)
        {
            SaveTreeViewState(windowId);
            SaveTabState();
            SaveFoldoutState();
        }

        #region treeview

        static Dictionary<string, TreeViewState> TREE_VIEW_STATE_LOOKUP;

        public static TreeViewState GetTreeViewState(string id)
        {
            if (TREE_VIEW_STATE_LOOKUP.ContainsKey(id))
            {
                TreeViewState state = TREE_VIEW_STATE_LOOKUP[id];
                if (state == null)
                    state = new TreeViewState();

                return state;
            }

            TreeViewState newState = new TreeViewState();

            if (EditorPrefs.HasKey("treeState" + id))
            {
                string json = EditorPrefs.GetString("treeState" + id);
                newState = JsonUtility.FromJson<TreeViewState>(json);
            }

            TREE_VIEW_STATE_LOOKUP.Add(id, newState);
            return newState;
        }

        static void SaveTreeViewState(string id)
        {
            if (string.IsNullOrEmpty(id))
                return;

            var state = GetTreeViewState(id);
            string json = JsonUtility.ToJson(state);
            EditorPrefs.SetString("treeState" + id, json);
        }

        #endregion

        #region tab

        static Dictionary<string, int> TAB_STATE_LOOKUP;

        public static int GetTabState(string windowId, string fieldId)
        {
            string id = windowId + fieldId;

            if (TAB_STATE_LOOKUP.ContainsKey(id))
            {
                return TAB_STATE_LOOKUP[id];
            }

            int state = 0;

            if (EditorPrefs.HasKey(id))
            {
                state = EditorPrefs.GetInt(id);
            }

            TAB_STATE_LOOKUP.Add(id, state);

            return state;
        }

        public static void SetTabState(string windowId, string fieldId, int state)
        {
            TAB_STATE_LOOKUP[windowId + fieldId] = state;
        }

        static void SaveTabState()
        {
            foreach (var key in TAB_STATE_LOOKUP.Keys)
            {
                EditorPrefs.SetInt(key, TAB_STATE_LOOKUP[key]);
            }
        }

        #endregion

        #region foldout

        static Dictionary<string, bool> FOLDOUT_STATE_LOOKUP;

        public static bool GetFoldoutState(string windowId, string fieldId)
        {
            string id = windowId + fieldId;

            if (FOLDOUT_STATE_LOOKUP.ContainsKey(id))
            {
                return FOLDOUT_STATE_LOOKUP[id];
            }

            bool state = true;

            if (EditorPrefs.HasKey(id))
            {
                state = EditorPrefs.GetBool(id);
            }

            FOLDOUT_STATE_LOOKUP.Add(id, state);

            return state;
        }

        public static void SetFoldoutState(string windowId, string fieldId, bool state)
        {
            FOLDOUT_STATE_LOOKUP[windowId + fieldId] = state;
        }

        static void SaveFoldoutState()
        {
            foreach (var key in FOLDOUT_STATE_LOOKUP.Keys)
            {
                EditorPrefs.SetBool(key, FOLDOUT_STATE_LOOKUP[key]);
            }
        }

        #endregion

        #region drag n drop

        static int DRAGGED_ID = -1;

        public static void SetDraggedId(int id)
        {
            DRAGGED_ID = id;
        }

        public static void ClearDraggedId()
        {
            DRAGGED_ID = -1;
        }

        public static int GetDraggedId()
        {
            return DRAGGED_ID;
        }

        #endregion

        #region tree element clipboard

        static List<object> TREE_ELEMENTS;

        public static void SetTreeElementClipboard(List<object> treeElements)
        {
            VirtualTrainingEditorState.TREE_ELEMENTS = treeElements;
        }

        public static List<T> GetTreeElementClipboard<T>() where T : TreeElement
        {
            List<T> newDatas = new List<T>();

            for (int i = 0; i < TREE_ELEMENTS.Count; i++)
            {
                try
                {
                    newDatas.Add(VirtualTrainingEditorUtility.CloneObject<T>((T)TREE_ELEMENTS[i]));
                }
                catch (System.Exception e)
                {
                    return new List<T>();
                }
            }

            return newDatas;
        }

        #endregion

        #region list field clipboard

        static List<object> LIST_ELEMENTS;

        public static void SetListFieldElementClipboard(List<object> elements)
        {
            LIST_ELEMENTS = elements;
        }

        public static List<T> GetListFieldElementClipboard<T>()
        {
            List<T> newDatas = new List<T>();

            for (int i = 0; i < LIST_ELEMENTS.Count; i++)
            {
                try
                {
                    newDatas.Add(VirtualTrainingEditorUtility.CloneObject<T>((T)LIST_ELEMENTS[i]));
                }
                catch (System.Exception e)
                {
                    newDatas.Add((T)LIST_ELEMENTS[i]);
                }
            }

            return newDatas;
        }

        #endregion

        #region editor window

        static string LAST_FOCUSED_WINDOW_ID;

        public static void SetLastFocusedWindowId(string windowId)
        {
            LAST_FOCUSED_WINDOW_ID = windowId;
        }

        public static string GetLastFocusedWindowId()
        {
            return LAST_FOCUSED_WINDOW_ID;
        }

        #endregion
    }
}

#endif
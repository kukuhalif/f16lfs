﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using Object = UnityEngine.Object;
using System.IO;

namespace VirtualTraining.Tools
{
    public abstract class EditorWindowDatabaseBase<S, W, T> : EditorWindow
        where S : ScriptableObjectBase<T>
        where W : EditorWindow
    {
        string windowNameId;
        int databaseInstanceId;
        bool initialized;
        bool enableRendering;

        [SerializeField] S scriptableObjectOriginal;
        [SerializeField] S scriptableObjectTemp;
        [SerializeField] Stack<T> undoActions = new Stack<T>();
        [SerializeField] Stack<T> redoActions = new Stack<T>();

        public string GetWindowId()
        {
            return windowNameId;
        }

        protected string WindowId
        {
            get => windowNameId;
        }

        protected int InstanceId
        {
            get => databaseInstanceId;
        }

        protected S ScriptableObjectOriginalFile
        {
            get => scriptableObjectOriginal;
        }

        protected S ScriptableObjectTemp
        {
            get => scriptableObjectTemp;
        }

        protected bool EnableRendering
        {
            get => enableRendering;
        }

        protected static W OpenEditorWindow()
        {
            string key = "lastEditor" + typeof(S).ToString();
            string lastFile = "";
            if (EditorPrefs.HasKey(key))
                lastFile = EditorPrefs.GetString(key);

            string[] guids = AssetDatabase.FindAssets("t:" + typeof(S).ToString());
            bool windowOpened = false;

            foreach (string guid in guids)
            {
                Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(S));
                if (string.IsNullOrEmpty(lastFile) || obj.name == lastFile)
                {
                    windowOpened = true;
                    return OpenEditorWindow(obj as S, false);
                }
            }
            if (!windowOpened && guids.Length > 0)
            {
                Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guids[0]), typeof(S));
                return OpenEditorWindow(obj as S, false);
            }
            return null;
        }

        protected static W OpenEditorWindow(int instanceID, bool newWindow = true)
        {
            // get actual database asset from instance id
            S scriptableObjectAsset = EditorUtility.InstanceIDToObject(instanceID) as S;

            // check if window is already opened
            W oldWindow = null;
            W[] oldWindows = Resources.FindObjectsOfTypeAll<W>();

            foreach (var window in oldWindows)
            {
                EditorWindowDatabaseBase<S, W, T> thisWindowType = window as EditorWindowDatabaseBase<S, W, T>;
                if (thisWindowType != null)
                {
                    if (thisWindowType.ScriptableObjectOriginalFile == scriptableObjectAsset)
                    {
                        oldWindow = window;
                        break;
                    }
                }
            }

            // is window is already opened, dont open new window
            if (oldWindow != null)
                newWindow = false;

            if (scriptableObjectAsset != null)
            {
                return OpenEditorWindow(scriptableObjectAsset, newWindow, oldWindow);
            }
            return null; // we did not handle the open
        }

        static W OpenEditorWindow(S scriptableObjectAsset, bool newWindow, W oldWindow = null)
        {
            string key = "lastEditor" + typeof(S).ToString();
            EditorPrefs.SetString(key, scriptableObjectAsset.name);

            W window = null;

            if (oldWindow != null)
            {
                window = oldWindow;
            }
            else
            {
                if (newWindow)
                {
                    window = CreateInstance<W>();
                    window.Show();
                }
                else
                {
                    window = GetWindow<W>();
                }
            }

            window.Focus();
            window.Repaint();
            window.titleContent = new GUIContent(scriptableObjectAsset.name);
            EditorWindowDatabaseBase<S, W, T> editorWindowDatabase = window as EditorWindowDatabaseBase<S, W, T>;
            editorWindowDatabase.scriptableObjectOriginal = scriptableObjectAsset;
            editorWindowDatabase.databaseInstanceId = scriptableObjectAsset.GetInstanceID();
            editorWindowDatabase.windowNameId = scriptableObjectAsset.name;

            // temp file init
            editorWindowDatabase.scriptableObjectTemp = null;

            if (!Directory.Exists(Application.dataPath + "/Temp"))
                Directory.CreateDirectory(Application.dataPath + "/Temp");

            string path = "Assets/Temp/" + editorWindowDatabase.scriptableObjectOriginal.name + "_" + editorWindowDatabase.scriptableObjectOriginal.GetInstanceID() + ".asset";
            editorWindowDatabase.scriptableObjectTemp = AssetDatabase.LoadAssetAtPath(path, typeof(S)) as S;

            if (editorWindowDatabase.scriptableObjectTemp == null)
            {
                editorWindowDatabase.scriptableObjectTemp = ScriptableObject.CreateInstance<S>();
                editorWindowDatabase.scriptableObjectTemp.name = editorWindowDatabase.scriptableObjectOriginal.name;
                AssetDatabase.CreateAsset(editorWindowDatabase.scriptableObjectTemp, path);
            }

            T data = editorWindowDatabase.scriptableObjectOriginal.GetData();
            T convertedData = VirtualTrainingEditorUtility.CloneObject<T>(data);

            editorWindowDatabase.scriptableObjectTemp.SetData(convertedData);
            EditorUtility.SetDirty(editorWindowDatabase.scriptableObjectTemp);
            AssetDatabase.SaveAssets(); //save file immediately

            VirtualTrainingEditorState.SetLastFocusedWindowId(editorWindowDatabase.windowNameId);

            editorWindowDatabase.initialized = true;
            editorWindowDatabase.enableRendering = true;

            return window;
        }

        public EditorWindowDatabaseBase()
        {
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }
        ~EditorWindowDatabaseBase()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
        }

        protected virtual void PlayModeStateChanged(PlayModeStateChange obj)
        {
            if (ScriptableObjectOriginalFile != null)
            {
                switch (obj)
                {
                    case PlayModeStateChange.EnteredEditMode:
                        enableRendering = true;
                        break;
                    case PlayModeStateChange.ExitingEditMode:
                        enableRendering = false;
                        break;
                    case PlayModeStateChange.EnteredPlayMode:
                        EventManager.AddListener<SceneReadyEvent>(SceneReadyListener);
                        enableRendering = false;
                        break;
                    case PlayModeStateChange.ExitingPlayMode:
                        enableRendering = false;
                        break;
                }
            }
            Repaint();
        }

        private void SceneReadyListener(SceneReadyEvent e)
        {
            EventManager.RemoveListener<SceneReadyEvent>(SceneReadyListener);
            enableRendering = true;
            Repaint();
        }

        protected virtual void Save()
        {
            if (scriptableObjectTemp == null || scriptableObjectOriginal == null)
                return;

            T convertedData = VirtualTrainingEditorUtility.CloneObject<T>(ScriptableObjectTemp.GetData());
            scriptableObjectOriginal.SetData(convertedData);
            EditorUtility.SetDirty(scriptableObjectOriginal);
            AssetDatabase.SaveAssets(); //save file immediately

            ClearUndoRedoAction();
            GUI.FocusControl(null);
        }

        protected bool Load()
        {
            if (scriptableObjectOriginal == null || ScriptableObjectTemp == null)
                return false;

            T data = scriptableObjectOriginal.GetData();
            T clonedData = VirtualTrainingEditorUtility.CloneObject(data);
            ScriptableObjectTemp.SetData(clonedData);
            EditorUtility.SetDirty(ScriptableObjectTemp);
            AssetDatabase.SaveAssets(); //save file immediately

            ClearUndoRedoAction();
            GUI.FocusControl(null);

            return true;
        }

        private void OnFocus()
        {
            if (!string.IsNullOrEmpty(WindowId))
                VirtualTrainingEditorState.SetLastFocusedWindowId(WindowId);
        }

        protected virtual void OnEnable()
        {

        }

        protected virtual void OnDisable()
        {
            // save editor states
            VirtualTrainingEditorState.SaveStates(WindowId);

            // if scriptable object is removed
            if (scriptableObjectOriginal == null || ScriptableObjectTemp == null)
                return;

            // check window instance
            bool exist = false;
            W[] oldWindows = Resources.FindObjectsOfTypeAll<W>();

            foreach (var window in oldWindows)
            {
                EditorWindowDatabaseBase<S, W, T> thisWindowType = window as EditorWindowDatabaseBase<S, W, T>;
                if (thisWindowType != null)
                {
                    if (thisWindowType.ScriptableObjectOriginalFile == scriptableObjectOriginal)
                    {
                        exist = true;
                        break;
                    }
                }
            }

            if (!exist)
                return;

            string newData = ScriptableObjectTemp.GetJson();
            string oldData = scriptableObjectOriginal.GetJson();

            if (string.IsNullOrEmpty(newData))
                return;

            if (!string.Equals(newData, oldData))
            {
                if (EditorUtility.DisplayDialog(titleContent.text, "save data ?", "yes", "no"))
                {
                    Save();
                }
            }
        }

        protected virtual void OnGUI()
        {
            if (!initialized)
            {
                OpenEditorWindow();
                Load();

                initialized = true;
            }

            if (enableRendering)
            {
                RenderGUI();
            }
            else
            {
                GUILayout.BeginVertical();
                GUILayout.FlexibleSpace();
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("now loading ...");
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.EndVertical();

                Repaint();
            }
        }

        protected abstract void RenderGUI();

        T CopyData(T original)
        {
            T convertedData = VirtualTrainingEditorUtility.CloneObject<T>(original);
            return convertedData;
        }

        protected int UndoCount()
        {
            return undoActions.Count;
        }

        protected int RedoCount()
        {
            return redoActions.Count;
        }

        protected void RecordUndo()
        {
            undoActions.Push(CopyData(ScriptableObjectTemp.GetData()));
        }

        protected void UndoAction()
        {
            redoActions.Push(CopyData(ScriptableObjectTemp.GetData()));

            if (undoActions.Count > 0)
            {
                T undoAction = CopyData(undoActions.Pop());
                ScriptableObjectTemp.SetData(undoAction);
            }

            GUI.FocusControl(null);
        }

        protected void RedoAction()
        {
            undoActions.Push(CopyData(ScriptableObjectTemp.GetData()));

            if (redoActions.Count > 0)
            {
                T redoAction = CopyData(redoActions.Pop());
                ScriptableObjectTemp.SetData(redoAction);
            }

            GUI.FocusControl(null);
        }

        protected void ClearUndoRedoAction()
        {
            undoActions.Clear();
            redoActions.Clear();
        }
    }
}

#endif
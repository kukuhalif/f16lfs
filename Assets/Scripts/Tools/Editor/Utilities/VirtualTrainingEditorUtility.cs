﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using VirtualTraining.Core;
using UnityEditor.SceneManagement;

namespace VirtualTraining.Tools
{
    public static class VirtualTrainingEditorUtility
    {
        public static void SetModelSceneDirty(SceneMode mode)
        {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetSceneByPath(DatabaseManager.GetSceneConfig(mode).mainScenePath));
        }

        public static T CloneObject<T>(T source)
        {
            string json = JsonUtility.ToJson(source);
            return JsonUtility.FromJson<T>(json);
        }

        public static void CopyToClipboard(string txt)
        {
            TextEditor te = new TextEditor();
            te.text = txt;
            te.SelectAll();
            te.Copy();
        }

        [MenuItem("Virtual Training/Builder/Build Setting Mode/Addressable Assets Mode")]
        public static void SetToAddressableAssetsBuildMode()
        {
            DatabaseManager.SetIsAddressableAssetMode(true);
            SetBuildSettingScenes();
        }

        [MenuItem("Virtual Training/Builder/Build Setting Mode/Regular Mode")]
        public static void SetToRegularBuildMode()
        {
            DatabaseManager.SetIsAddressableAssetMode(false);
            SetBuildSettingScenes();
        }

        public static void SetBuildSettingScenes()
        {
            List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();

            // start
            string start = AssetDatabase.GetAssetPath(DatabaseManager.GetStartingSceneAsset());
            editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(start, true));

            // all scene configs
            foreach (var sceneConfig in DatabaseManager.GetAllSceneConfig())
            {
                if (!sceneConfig.includeInBuildSetting)
                    continue;

                // add main scene if not using addresable asset
                if (!DatabaseManager.IsAddressableAssetMode())
                {
                    string mainScenePath = AssetDatabase.GetAssetPath(sceneConfig.sceneAssets[0]);
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(mainScenePath, true));
                }

                // other scene
                var scenes = sceneConfig.sceneAssets;
                for (int i = 1; i < scenes.Count; i++) // excude main scene
                {
                    string scenePath = AssetDatabase.GetAssetPath(scenes[i]);
                    if (!string.IsNullOrEmpty(scenePath))
                        editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
                }

                // quiz
                if (sceneConfig.quizSceneAsset != null)
                {
                    string quizScenePath = AssetDatabase.GetAssetPath(sceneConfig.quizSceneAsset);
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(quizScenePath, true));
                }

                // vr head
                if (sceneConfig.vrHeadSceneAsset != null)
                {
                    string vrheadScenePath = AssetDatabase.GetAssetPath(sceneConfig.vrHeadSceneAsset);
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(vrheadScenePath, true));
                }

                // networking
                if (sceneConfig.networkingSceneAsset != null)
                {
                    string networkingScenePath = AssetDatabase.GetAssetPath(sceneConfig.networkingSceneAsset);
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(networkingScenePath, true));
                }
            }

            // remove duplicated scene
            var distinctItems = editorBuildSettingsScenes.GroupBy(x => x.path).Select(y => y.First());

            // set scenes to build setting
            EditorBuildSettings.scenes = distinctItems.ToArray();
        }

        public static bool LoadMainSceneWarningField(ref SceneMode modelSceneMode, float windowWidth)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("scene mode", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH));
            modelSceneMode = (SceneMode)EditorGUILayout.EnumPopup(modelSceneMode, GUILayout.Width(windowWidth - VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH - 10));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            var scene = EditorSceneManager.GetSceneByPath(DatabaseManager.GetSceneConfig(modelSceneMode).mainScenePath);
            if (!scene.isLoaded)
            {
                GUI.color = Color.yellow;
                if (DatabaseManager.GetSceneConfig(modelSceneMode).scenes.Count > 0)
                {
                    EditorGUILayout.HelpBox("Please load and activate " + DatabaseManager.GetSceneConfig(modelSceneMode).scenes[0] + " Scene to use Model Setup!", MessageType.Warning);
                }
                else
                {
                    EditorGUILayout.HelpBox("Check scenes configuration in system config !", MessageType.Warning);
                }
                return false;
            }

            return true;
        }
    }
}

#endif

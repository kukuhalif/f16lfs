﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        object ShowSimpleField<T>(string title, T data, float labelWidth, float fieldWidth)
        {
            EditorGUIUtility.labelWidth = labelWidth;

            if (typeof(T) == typeof(string))
            {
                return EditorGUILayout.TextField(title, Convert.ToString(data), GUILayout.Width(fieldWidth));
            }
            else if (typeof(T) == typeof(int))
            {
                return EditorGUILayout.IntField(title, Convert.ToInt32(data), GUILayout.Width(fieldWidth));
            }
            else if (typeof(T) == typeof(float))
            {
                return EditorGUILayout.FloatField(title, Convert.ToSingle(data), GUILayout.Width(fieldWidth));
            }
            else if (typeof(T) == typeof(bool))
            {
                return EditorGUILayout.Toggle(title, Convert.ToBoolean(data), GUILayout.Width(fieldWidth));
            }
            else if (typeof(T) == typeof(GameObject))
            {
                GameObject obj = data as GameObject;
                obj = EditorGUILayout.ObjectField(title, obj, typeof(GameObject), true, GUILayout.Width(fieldWidth)) as GameObject;
                return obj;
            }
            else if (typeof(T) == typeof(Color))
            {
                object colorObj = data;
                Color colorTemp = (Color)colorObj;
                colorTemp = new Color(colorTemp.r, colorTemp.g, colorTemp.b, colorTemp.a);
                return EditorGUILayout.ColorField(new GUIContent(title), colorTemp, true, true, true, GUILayout.Width(fieldWidth));
            }
            else if (data is Enum)
            {
                return EditorGUILayout.EnumPopup(title, data as Enum, GUILayout.Width(fieldWidth));
            }
            else if (data is AnimationCurve)
            {
                AnimationCurve animationCurve = data as AnimationCurve;
                EditorGUILayout.CurveField(title, animationCurve, GUILayout.Width(fieldWidth));
                return animationCurve;
            }
            else if (typeof(T) == typeof(GameObjectType))
            {
                GameObjectType objType = data as GameObjectType;
                objType.gameObject = EditorGUILayout.ObjectField(title, objType.gameObject, typeof(GameObject), true, GUILayout.Width(fieldWidth)) as GameObject;
                return objType;
            }
            else if (typeof(T) == typeof(AnimatorControllerType))
            {
                AnimatorControllerType animType = data as AnimatorControllerType;
                animType.animationController = EditorGUILayout.ObjectField(title, animType.animationController, typeof(Animator), true, GUILayout.Width(fieldWidth)) as Animator;
                return animType;
            }
            else if (typeof(T) == typeof(LightControlType))
            {
                LightControlType lightType = data as LightControlType;
                lightType.lightController = EditorGUILayout.ObjectField(title, lightType.lightController, typeof(Light), true, GUILayout.Width(fieldWidth)) as Light;
                return lightType;
            }
            else if (typeof(T) == typeof(FolderReferenceType))
            {
                FolderReferenceType folderRefType = data as FolderReferenceType;
                folderRefType.folder = EditorGUILayout.ObjectField(title, folderRefType.folder, typeof(DefaultAsset), false, GUILayout.Width(fieldWidth)) as DefaultAsset;
                folderRefType.path = AssetDatabase.GetAssetPath(folderRefType.folder);
                return folderRefType;
            }

            return default;
        }

        /// <summary>
        /// show string, int, float or boolean field
        /// </summary>
        /// <typeparam name="T">data type</typeparam>
        /// <param name="title">field title</param>
        /// <param name="controlName">unique control name</param>
        /// <param name="data">field data</param>
        public void ShowSimpleField<T>(string title, string controlName, ref T data, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)
        {
            // todo : if data is color, only compare color if color picker is closed

            GUI.SetNextControlName(controlName);
            if (GUI.GetNameOfFocusedControl() == controlName)
            {
                GUI.color = selectedElementColor;
                T newData = (T)ShowSimpleField(title, data, labelWidth, fieldWidth);
                if (!Equals(newData, data))
                    beforeModifiedCallback?.Invoke();
                data = newData;
                GUI.color = Color.white;
            }
            else // todo : this is temporary fix for color picker
                data = (T)ShowSimpleField(title, data, labelWidth, fieldWidth);
        }

        /// <summary>
        /// show string, int, float or boolean field
        /// </summary>
        /// <typeparam name="T">data type</typeparam>
        /// <param name="title">field title as unique control name</param>
        /// <param name="data">field data</param>
        public void ShowSimpleField<T>(string title, ref T data, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)
        {
            ShowSimpleField(title, title, ref data, labelWidth, fieldWidth);
        }

        /// <summary>
        /// show text area
        /// </summary>
        /// <param name="title">field title as unique control name</param>
        /// /// <param name="width">text area width</param>
        /// <param name="data">field data</param>
        public void ShowTextArea(string title, float width, ref string data, bool showGenerateLinkButton, float leftSpacing = -1f)
        {
            ShowTextArea(title, title, width, ref data, showGenerateLinkButton, leftSpacing);
        }

        /// <summary>
        /// show text area
        /// </summary>
        /// <param name="title">field title as unique control name</param>
        /// <param name="controlName">unique control name</param>
        /// <param name="width">text area width</param>
        /// <param name="data">field data</param>
        public void ShowTextArea(string title, string controlName, float width, ref string data, bool showGenerateLinkButton, float leftSpacing)
        {
            GUI.SetNextControlName(controlName);
            if (GUI.GetNameOfFocusedControl() == controlName)
            {
                GUI.color = selectedElementColor;

                GUILayout.Label(title);
                Rect lastRect = GUILayoutUtility.GetLastRect();
                EditorStyles.textField.wordWrap = true;

                GUI.color = Color.white;

                string newData = EditorGUILayout.TextArea(data, GUILayout.Width(width), GUILayout.MinHeight(VirtualTrainingEditorConfig.DEFAULT_MIN_TEXT_AREA_HEIGHT), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true));
                if (showGenerateLinkButton)
                    newData = TextFormatter(newData, lastRect, leftSpacing);
                if (newData != data)
                    beforeModifiedCallback?.Invoke();
                data = newData;
            }
            else
            {
                GUILayout.Label(title);
                Rect lastRect = GUILayoutUtility.GetLastRect();
                EditorStyles.textField.wordWrap = true;
                data = EditorGUILayout.TextArea(data, GUILayout.Width(width), GUILayout.MinHeight(VirtualTrainingEditorConfig.DEFAULT_MIN_TEXT_AREA_HEIGHT), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true));

                if (showGenerateLinkButton)
                    TextFormatter("", lastRect, leftSpacing);
            }
        }

        private string TextFormatter(string text, Rect lastRect, float leftSpacing)
        {
            TextEditor editor = typeof(EditorGUI)
                .GetField("activeEditor", BindingFlags.Static | BindingFlags.NonPublic)
                .GetValue(null) as TextEditor;

            GUILayout.BeginHorizontal();

            const float buttonWidth = 110f;

            if (leftSpacing < 0)
                lastRect.x += buttonWidth;
            else
                lastRect.x += leftSpacing;

            lastRect.width = buttonWidth;

            if (GUI.Button(lastRect, "start bullet"))
            {
                if (editor != null)
                {
                    string bullet = @"\begin{itemize}";
                    foreach (var c in bullet)
                    {
                        editor.Insert(c);
                    }

                    text = editor.text;
                }
            }

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "end bullet"))
            {
                if (editor != null)
                {
                    string bullet = @"\end{itemize}";
                    foreach (var c in bullet)
                    {
                        editor.Insert(c);
                    }

                    text = editor.text;
                }
            }

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "start numbering"))
            {
                if (editor != null)
                {
                    string bullet = @"\begin{enumerate}";
                    foreach (var c in bullet)
                    {
                        editor.Insert(c);
                    }

                    text = editor.text;
                }
            }

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "end numbering"))
            {
                if (editor != null)
                {
                    string bullet = @"\end{enumerate}";
                    foreach (var c in bullet)
                    {
                        editor.Insert(c);
                    }

                    text = editor.text;
                }
            }

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "item"))
            {
                if (editor != null)
                {
                    string bullet = @"\item";
                    foreach (var c in bullet)
                    {
                        editor.Insert(c);
                    }

                    text = editor.text;
                }
            }

            EditorGUI.BeginDisabledGroup(editor == null || string.IsNullOrEmpty(text) || string.IsNullOrEmpty(editor.SelectedText));

            //lastRect.x += buttonWidth;

            //if (GUI.Button(lastRect, "pdf link"))
            //{
            //    if (editor != null)
            //    {
            //        string link = @"\link[pdf_code]{code}";
            //        string selected = editor.SelectedText;
            //        string newLink = link.Replace("code", selected);

            //        editor.ReplaceSelection(newLink);
            //        text = editor.text;
            //    }
            //}

            //lastRect.x += buttonWidth;

            //if (GUI.Button(lastRect, "schematic link"))
            //{
            //    if (editor != null)
            //    {
            //        string link = @"\link[schematic_code]{code}";
            //        string selected = editor.SelectedText;
            //        string newLink = link.Replace("code", selected);

            //        editor.ReplaceSelection(newLink);
            //        text = editor.text;
            //    }
            //}

            //lastRect.x += buttonWidth;

            //if (GUI.Button(lastRect, "figure link"))
            //{
            //    if (editor != null)
            //    {
            //        string link = @"\link[figure_code]{code}";
            //        string selected = editor.SelectedText;
            //        string newLink = link.Replace("code", selected);

            //        editor.ReplaceSelection(newLink);
            //        text = editor.text;
            //    }
            //}

            //lastRect.x += buttonWidth;

            //if (GUI.Button(lastRect, "camera figure link"))
            //{
            //    if (editor != null)
            //    {
            //        string link = @"\link[camerafigure_code]{code}";
            //        string selected = editor.SelectedText;
            //        string newLink = link.Replace("code", selected);

            //        editor.ReplaceSelection(newLink);
            //        text = editor.text;
            //    }
            //}

            //lastRect.x += buttonWidth;

            //if (GUI.Button(lastRect, "camera preset link"))
            //{
            //    if (editor != null)
            //    {
            //        string link = @"\link[camerapreset_code]{code}";
            //        string selected = editor.SelectedText;
            //        string newLink = link.Replace("code", selected);

            //        editor.ReplaceSelection(newLink);
            //        text = editor.text;
            //    }
            //}

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "center"))
            {
                if (editor != null)
                {
                    string centering = @"\begin{center} text \end{center}";
                    string selected = editor.SelectedText;
                    string newText = centering.Replace("text", selected);

                    editor.ReplaceSelection(newText);
                    text = editor.text;
                }
            }

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "bold"))
            {
                if (editor != null)
                {
                    string bold = @"{\bf text }";
                    string selected = editor.SelectedText;
                    string newText = bold.Replace("text", selected);

                    editor.ReplaceSelection(newText);
                    text = editor.text;
                }
            }

            lastRect.x += buttonWidth;

            if (GUI.Button(lastRect, "italic"))
            {
                if (editor != null)
                {
                    string italic = @"{\it text }";
                    string selected = editor.SelectedText;
                    string newText = italic.Replace("text", selected);

                    editor.ReplaceSelection(newText);
                    text = editor.text;
                }
            }

            for (int i = 0; i < favoriteColors.Count; i++)
            {
                lastRect.x += buttonWidth;

                if (GUI.Button(lastRect, "color : " + favoriteColors[i].name))
                {
                    if (editor != null)
                    {
                        string coloring = @"\color{#hex} text \color{}";
                        coloring = coloring.Replace("hex", ColorUtility.ToHtmlStringRGBA(favoriteColors[i].color));
                        string selected = editor.SelectedText;
                        string newText = coloring.Replace("text", selected);

                        editor.ReplaceSelection(newText);
                        text = editor.text;
                    }
                }
            }

            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            return text;
        }

        private static Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];
            for (int i = 0; i < pix.Length; ++i)
            {
                pix[i] = col;
            }
            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }

        public static void DrawTooltip(Rect iconRect, string tooltip, float width, Vector2 offset)
        {
            if (iconRect.Contains(Event.current.mousePosition))
            {
                iconRect.y += offset.y;
                iconRect.x += offset.x;
                iconRect.width = width;
                iconRect.height = 20f;
                GUI.color = Color.white;
                GUI.Box(iconRect, "", TooltipStyle);
                GUI.contentColor = Color.white;
                GUI.color = Color.white;
                GUI.Label(iconRect, tooltip, CenteredTextStyle);
            }
        }

        public static void DrawTooltip(string tooltip)
        {
            Rect rect = GUILayoutUtility.GetLastRect();
            DrawTooltip(rect, tooltip, 120f, new Vector2(-rect.width * 0.75f, -rect.height));
        }

        public bool DrawButtonIcon(Texture2D icon, string tooltip)
        {
            bool pressed = GUILayout.Button(new GUIContent(icon));
            DrawTooltip(tooltip);
            return pressed;
        }

        public static bool DrawButton(IconEnum icon, string tooltip, float width = -1f)
        {
            bool pressed;
            if (width > -1f)
                pressed = GUILayout.Button(new GUIContent(EditorIconResources.Get(icon)), GUILayout.Width(width));
            else
                pressed = GUILayout.Button(new GUIContent(EditorIconResources.Get(icon)));

            DrawTooltip(tooltip);
            return pressed;
        }

        public static bool DrawButton(Rect rect, IconEnum icon)
        {
            return GUI.Button(rect, new GUIContent(EditorIconResources.Get(icon)));
        }

        /// <summary>
        /// show asset field
        /// </summary>
        /// <param name="title">field title as unique control name</param>
        /// <param name="data">field data</param>
        public void ShowAssetField<T>(string title, ref T data, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH) where T : UnityEngine.Object
        {
            ShowAssetField(title, title, ref data, labelWidth, fieldWidth);
        }

        /// <summary>
        /// show asset field
        /// </summary>
        /// <param name="title">field title as unique control name</param>
        /// <param name="controlName">unique control name</param>
        /// <param name="data">field data</param>
        public void ShowAssetField<T>(string title, string controlName, ref T data, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH) where T : UnityEngine.Object
        {
            // rect for drop area
            Rect dropArea = new Rect();

            EditorGUIUtility.labelWidth = labelWidth;

            GUI.SetNextControlName(controlName);
            if (GUI.GetNameOfFocusedControl() == controlName)
            {
                GUI.color = selectedElementColor;
                T newData = EditorGUILayout.ObjectField(title, data, typeof(T), false, GUILayout.Width(fieldWidth)) as T;
                if (newData != data)
                    beforeModifiedCallback?.Invoke();
                data = newData;
                GUI.color = Color.white;
            }
            else
            {
                EditorGUILayout.ObjectField(title, data, typeof(T), false, GUILayout.Width(fieldWidth));
            }

            // set focus to this field if pointer dropped asset
            dropArea = GUILayoutUtility.GetLastRect();
            Event currentEvent = Event.current;

            if (dropArea.Contains(currentEvent.mousePosition))
            {
                GUI.FocusControl(controlName);
            }
        }

        /// <summary>
        /// show dropdown from list string
        /// </summary>
        /// <param name="title">field title as control name</param>
        /// <param name="selected">selected data</param>
        /// <param name="option">dropdown option</param>
        public int ShowDropdownField(string title, string selected, List<string> option, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)
        {
            return ShowDropdownField(title, title, selected, option, labelWidth, fieldWidth);
        }

        /// <summary>
        /// show dropdown from list string
        /// </summary>
        /// <param name="title">field title</param>
        /// <param name="controlName">unique control name</param>
        /// <param name="selected">selected data</param>
        /// <param name="option">dropdown option</param>
        public int ShowDropdownField(string title, string controlName, string selected, List<string> option, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_DROPDOWN_FIELD_WIDTH)
        {
            int selectedIndex = 0;
            for (int i = 0; i < option.Count; i++)
            {
                if (string.Equals(option[i], selected))
                {
                    selectedIndex = i;
                    break;
                }
            }

            EditorGUIUtility.labelWidth = labelWidth;

            GUI.SetNextControlName(controlName);
            if (GUI.GetNameOfFocusedControl() == controlName)
            {
                GUI.color = selectedElementColor;
                int result = EditorGUILayout.Popup(title, selectedIndex, option.ToArray(), GUILayout.Width(fieldWidth));
                if (result != selectedIndex)
                    beforeModifiedCallback?.Invoke();
                GUI.color = Color.white;

                return result;
            }
            else
                return EditorGUILayout.Popup(title, selectedIndex, option.ToArray(), GUILayout.Width(fieldWidth));
        }

        /// <summary>
        /// show slider for float value
        /// </summary>
        /// <param name="title">slider label</param>
        /// <param name="controlName">unique control name</param>
        /// <param name="value">float value</param>
        /// <param name="minValue">min slider value</param>
        /// <param name="maxValue">max slider value</param>
        public void ShowSlider(string title, string controlName, ref float value, float minValue, float maxValue, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_DROPDOWN_FIELD_WIDTH)
        {
            // todo : only compare data on mouse up
            // todo : enable compare data

            EditorGUIUtility.labelWidth = labelWidth;

            GUI.SetNextControlName(controlName);

            //if (GUI.GetNameOfFocusedControl() == controlName)
            //{
            //    float temp = EditorGUILayout.Slider(title, value, minValue, maxValue, GUILayout.Width(fieldWidth));
            //    if (temp != value)
            //    {
            //        beforeModifiedCallback?.Invoke();
            //        value = temp;
            //    }
            //}
            //else
            //{
            value = EditorGUILayout.Slider(title, value, minValue, maxValue, GUILayout.Width(fieldWidth));
            //}
        }

        /// <summary>
        /// show slider for float value
        /// </summary>
        /// <param name="title">slider label also as control name</param>
        /// <param name="value">float value</param>
        /// <param name="minValue">min slider value</param>
        /// <param name="maxValue">max slider value</param>
        public void ShowSlider(string title, ref float value, float minValue, float maxValue, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_DROPDOWN_FIELD_WIDTH)
        {
            ShowSlider(title, title, ref value, minValue, maxValue, labelWidth, fieldWidth);
        }
    }
}

#endif
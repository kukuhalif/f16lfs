﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        [MenuItem("Virtual Training Shortcut/Save Camera Position &-", false)]
        static void SaveCameraPosition()
        {
            isSaveCameraPressed = true;
        }

        [MenuItem("Virtual Training Shortcut/Save Camera Position &-", true)]
        static bool SaveCameraPositionVerify()
        {
            return isEditingCamera;
        }

        static bool isEditingCamera;
        static bool isSaveCameraPressed;
        private CameraDestination cameraDestination;
        private string currentEditCameraControlName = "";
        private Camera cameraPointer = null;

        private void CameraDestinationInitialization()
        {
            isEditingCamera = false;
            isSaveCameraPressed = false;
        }

        public void ShowCameraPositionField(string cameraName, string controlName, ref CameraDestination cameraDestination, bool showCameraMovementField, bool configData = false, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label(cameraName);

            float buttonWidth = fieldWidth - labelWidth;

            if (!isEditingCamera)
            {
                if (DrawButton(IconEnum.EditPosition, "edit this camera", 30f))
                {
                    this.cameraDestination = cameraDestination;
                    isEditingCamera = true;
                    currentEditCameraControlName = controlName;

                    GameObject cameraPointer = new GameObject("camera pointer");
                    cameraPointer.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.transform, true);
                    cameraPointer.transform.SetAsLastSibling();
                    cameraPointer.transform.localPosition = cameraDestination.position;
                    cameraPointer.transform.localRotation = cameraDestination.rotation;
                    this.cameraPointer = cameraPointer.AddComponent<Camera>();
                    this.cameraPointer.fieldOfView = cameraDestination.fov;
                    this.cameraPointer.orthographic = cameraDestination.isOrthographic;
                    this.cameraPointer.orthographicSize = cameraDestination.orthographicSize;
                    this.cameraPointer.nearClipPlane = 0.01f;
                    EditorGUIUtility.PingObject(cameraPointer);
                    Selection.activeGameObject = cameraPointer;
                }

                if (configData)
                {
                    GUILayout.Space(10f);
                    ShowSimpleField("target", controlName + "cameraTarget", ref cameraDestination.target, 40f, 200f);

                    GUILayout.Space(5f);
                    if (DrawButton(IconEnum.EditFavourite, "add to favourite", 30f))
                    {
                        var favorite = EditorWindow.GetWindow<FavoriteEditor>();
                        favorite.AddCameraDestination(cameraDestination);
                    }
                }
                else
                {
                    if (showCameraMovementField)
                    {
                        ShowSimpleField("rotate from view", controlName + "rotate fov", ref cameraDestination.isRotateFromView, 95f, 100f);

                        GUILayout.Space(20f);
                        ShowSimpleField("disable movement", controlName + "disable move", ref cameraDestination.isMovementDisabled, 110f, 110f);
                        GUILayout.Space(10f);
                    }

                    GUILayout.Space(10f);
                    ShowSimpleField("target", controlName + "cameraTarget", ref cameraDestination.target, 40f, 200f);

                    if (!showCameraMovementField)
                    {
                        GUILayout.Space(10f);
                        ShowSimpleField("parent", controlName + "cameraParent", ref cameraDestination.parent, 40f, 200f);
                        GUILayout.Space(10f);
                        ShowSimpleField("use parent position", controlName + "use parent pos", ref cameraDestination.useParentPositionAndRotation, 110f, 110f);
                        GUILayout.Space(20f);
                    }

                    GUILayout.Space(5f);
                    if (DrawButton(IconEnum.EditFavourite, "add to favourite", 30f))
                    {
                        var favorite = EditorWindow.GetWindow<FavoriteEditor>();
                        favorite.AddCameraDestination(cameraDestination);
                    }
                }

                if (FavoriteEditor.tempCameraDestination != null && GUILayout.Button("paste from favorite", GUILayout.Width(buttonWidth)))
                {
                    cameraDestination = new CameraDestination(FavoriteEditor.tempCameraDestination);
                }

                GUILayout.Space(5f);
                GUILayout.Label(cameraDestination.ToString(), GUILayout.ExpandWidth(true));
            }
            else
            {
                GUILayout.Label("( press ctrl + shift + f to place camera on scene view )");

                if (currentEditCameraControlName == controlName && GUILayout.Button("save " + cameraName, GUILayout.Width(buttonWidth * 2)))
                {
                    isEditingCamera = false;
                    isSaveCameraPressed = true;
                }
            }

            GUILayout.FlexibleSpace();

            GUILayout.EndHorizontal();

            if (currentEditCameraControlName == controlName && isSaveCameraPressed)
            {
                SaveCameraDestination();
            }
        }

        public void SaveCameraDestination()
        {
            if (cameraDestination == null)
                return;

            beforeModifiedCallback?.Invoke();

            isSaveCameraPressed = false;
            isEditingCamera = false;

            cameraDestination.position = cameraPointer.transform.localPosition;
            cameraDestination.rotation = cameraPointer.transform.localRotation;
            cameraDestination.isOrthographic = cameraPointer.orthographic;
            cameraDestination.orthographicSize = cameraPointer.orthographicSize;
            cameraDestination.fov = cameraPointer.fieldOfView;

            if (Application.isPlaying)
                Object.Destroy(cameraPointer.gameObject);
            else
                Object.DestroyImmediate(cameraPointer.gameObject);

            cameraPointer = null;
            currentEditCameraControlName = "";

            cameraDestination = null;
        }
    }
}

#endif
﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditor.PackageManager.UI;
using UnityEditor.SceneManagement;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        string windowId;
        Action beforeModifiedCallback;
        Color selectedElementColor;
        private static GUIStyle tooltipStyle = null;
        List<FavoriteEditor.FavoriteColor> favoriteColors;
        static GUIStyle CENTERED_TEXT_STYLE;

        protected string WindowId
        {
            get => windowId;
        }

        public Color SelectedElementColor
        {
            get => selectedElementColor;
        }

        public static GUIStyle TooltipStyle
        {
            get
            {
                if (tooltipStyle == null)
                {
                    tooltipStyle = new GUIStyle(GUI.skin.box);
                    tooltipStyle.normal.background = MakeTex(2, 2, new Color(0f, 0f, 0f, 1f));
                }

                return tooltipStyle;
            }
        }

        public static GUIStyle CenteredTextStyle
        {
            get
            {
                if (CENTERED_TEXT_STYLE == null)
                {
                    CENTERED_TEXT_STYLE = new GUIStyle("label");
                    CENTERED_TEXT_STYLE.alignment = TextAnchor.MiddleCenter;
                }

                return CENTERED_TEXT_STYLE;
            }
        }

        /// <summary>
        /// use this constructor if you are not concerned about the game object reference in the scene model
        /// </summary>
        /// /// <param name="beforeModifiedCallback">this action will invoked before any field value is updated / modified</param>
        /// <param name="windowId">editor window id</param>
        public PropertyField(Action beforeModifiedCallback, string windowId)
        {
            this.windowId = windowId;
            this.beforeModifiedCallback = beforeModifiedCallback;
            ReferenceFieldInitialization();
            CameraDestinationInitialization();
            LoadColor();
            tooltipStyle = null;
        }

        /// <summary>
        /// use this constructor if you want to make sure model scene is loaded (game object reference data)
        /// </summary>
        /// <param name="beforeModifiedCallback">this action will invoked before any field value is updated / modified</param>
        /// <param name="getWindowId">get window id function (from WindowId field EditorWindowDatabaseBase class)</param>
        public PropertyField(Action beforeModifiedCallback, Func<string> getWindowId)
        {
            this.beforeModifiedCallback = beforeModifiedCallback;
            ReferenceFieldInitialization();
            CameraDestinationInitialization();
            LoadColor();
            tooltipStyle = null;

            EditorCoroutineUtility.StartCoroutineOwnerless(Initialization(getWindowId));
        }

        private IEnumerator Initialization(Func<string> getWindowId)
        {
            yield return null;

            do
            {
                this.windowId = getWindowId.Invoke();
                yield return null;
            }
            while (this.windowId == null);

            yield return null;

            bool windowExist = false;
            EditorWindow[] windows = Resources.FindObjectsOfTypeAll<EditorWindow>();
            foreach (EditorWindow window in windows)
            {
                var getEditorWindowId = window.GetType().GetMethod("GetWindowId");
                if (getEditorWindowId != null)
                {
                    object wn = null;

                    do
                    {
                        wn = getEditorWindowId.Invoke(window, null);
                        yield return null;
                    }
                    while (wn == null);

                    if (wn != null)
                    {
                        string id = wn.ToString();

                        if (id == windowId)
                        {
                            windowExist = true;
                        }
                    }
                }
            }

            if (windowExist)
            {
                List<bool> sceneLoaded = new List<bool>();
                var sceneModeNames = Enum.GetNames(typeof(SceneMode));
                for (int i = 0; i < sceneModeNames.Length; i++)
                {
                    var modelScene = EditorSceneManager.GetSceneByPath(DatabaseManager.GetSceneConfig((SceneMode)i).mainScenePath);
                    sceneLoaded.Add(modelScene.isLoaded);
                }

                if (!sceneLoaded.Contains(true))
                {
                    if (EditorUtility.DisplayDialog("Property field", "model/main scene is not loaded, close editor without saving data", "ok"))
                    {

                    }
                }
            }
        }

        public void LoadColor()
        {
            selectedElementColor = DatabaseManager.GetEditorConfiguration().selectedElementColor;

            favoriteColors = new List<FavoriteEditor.FavoriteColor>();
            FavoriteEditor.GetData("/" + Application.productName + "/FavoriteColor.txt", (string data) =>
            {
                string[] colorDatas = data.Split(',');
                try
                {
                    favoriteColors.Add(new FavoriteEditor.FavoriteColor(colorDatas[0], new Color(float.Parse(colorDatas[1]), float.Parse(colorDatas[2]), float.Parse(colorDatas[3]), float.Parse(colorDatas[4]))));
                }
                catch (System.Exception e)
                {

                }
            });
        }

        public void TriggerBeforeModifiedCallback()
        {
            beforeModifiedCallback?.Invoke();
        }
    }
}

#endif
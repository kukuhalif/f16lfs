#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    public enum CategoryTab
    {
        FIGURE,
        FIGURE_LIST,
        SCHEMATIC,
        ANIMATION,
        VIRTUAL_BUTTON
    }

    public enum FigureTab
    {
        CAMERA,
        PART_OBJECT,
        CALLOUT,
        HELPER,
        CUTAWAY
    }

    public abstract class ContentEditorBase<T> : PropertyField
        where T : Figure
    {
        public class FigureAnimationState
        {
            public bool isSequenceAnimation;
            public bool isShowAnimationUI;
            public bool isReverseAnimationOnReset;
        }

        public class MateriViewData
        {
            Dictionary<Figure, FigureAnimationState> figureAnimationStateLookup;
            public Figure figure;
            public List<AnimationFigureData> animations = new List<AnimationFigureData>();
            public List<VirtualButtonFigureData> virtualButtonElements = new List<VirtualButtonFigureData>();
            public List<Figure> figureOption = new List<Figure>();

            public bool IsSequenceAnimation
            {
                get
                {
                    return figureAnimationStateLookup[figure].isSequenceAnimation;
                }
                set
                {
                    figureAnimationStateLookup[figure].isSequenceAnimation = value;
                }
            }

            public bool isShowAnimationUI
            {
                get
                {
                    return figureAnimationStateLookup[figure].isShowAnimationUI;
                }
                set
                {
                    figureAnimationStateLookup[figure].isShowAnimationUI = value;
                }
            }

            public bool isReverseAnimationOnReset
            {
                get
                {
                    return figureAnimationStateLookup[figure].isReverseAnimationOnReset;
                }
                set
                {
                    figureAnimationStateLookup[figure].isReverseAnimationOnReset = value;
                }
            }

            public MateriViewData(Figure figure, List<Figure> figureOption, List<AnimationFigureData> animations = null, List<VirtualButtonFigureData> virtualButtonElements = null, Dictionary<Figure, FigureAnimationState> figureAnimationStateLookup = null)
            {
                this.figureAnimationStateLookup = figureAnimationStateLookup;
                this.figure = figure;
                this.figureOption = figureOption;
                if (animations != null)
                {
                    for (int i = 0; i < animations.Count; i++)
                    {
                        this.animations.Add(animations[i]);
                    }
                }

                if (virtualButtonElements != null)
                {
                    for (int i = 0; i < virtualButtonElements.Count; i++)
                    {
                        this.virtualButtonElements.Add(virtualButtonElements[i]);
                    }
                }

            }
            public MateriViewData()
            {
                figure = null;
                animations = new List<AnimationFigureData>();
                figureOption = new List<Figure>();
                virtualButtonElements = new List<VirtualButtonFigureData>();
            }
        }

        Dictionary<Figure, FigureAnimationState> FigureAnimationStateLookup = new Dictionary<Figure, FigureAnimationState>();

        EditorGUISplitView splitter;
        int currentTreeElementId;
        bool isAnimationInit;
        bool isVirtualButtonInit;

        CategoryTab[] categoryTabs;
        FigureTab[] figureTabs;

        List<MateriViewData> animDatas;
        List<MateriViewData> virtualButtonDatas;
        PartObjectEditor partObjectEditor;
        CutawayEditor cutawayEditor;

        Figure figureTemp;

        public ContentEditorBase(Action beforeModifiedCallback, Func<string> windowId, List<Material> xrayMats, CategoryTab[] categoryTabs, FigureTab[] figureTabs) : base(beforeModifiedCallback, windowId)
        {
            // initialization
            splitter = new EditorGUISplitView(EditorGUISplitView.Direction.Vertical, WindowId + "description");
            animDatas = new List<MateriViewData>();
            virtualButtonDatas = new List<MateriViewData>();
            partObjectEditor = new PartObjectEditor(beforeModifiedCallback, windowId.Invoke(), xrayMats);
            cutawayEditor = new CutawayEditor(GameObject.FindObjectOfType<CutawayManager>());
            this.categoryTabs = categoryTabs;
            this.figureTabs = figureTabs;

            // create category tabs
            List<string> categoryList = new List<string>();
            foreach (var categoryName in categoryTabs)
            {
                categoryList.Add(categoryName.ToString().Replace('_', ' '));
            }
            CreateTab("categories", OnCategoriesTabCallback, categoryList.ToArray());

            // create figure tabs
            List<string> figureList = new List<string>();
            foreach (var figureName in figureTabs)
            {
                figureList.Add(figureName.ToString().Replace('_', ' '));
            }
            CreateTab("figure category", OnFigureListTabCallback, figureList.ToArray());

            // create other list view
            CreateListView("figure", FoldoutTextFigureCallback, null, null, OnFigureDetailDrawCallback);
            CreateListView("figure camera", null, null, OnCameraElementDrawCallback, null);
            CreateListView("monitor camera", null, null, OnMonitorCameraElementDrawCallback, null);
            CreateListView("callout prefab", null, null, OnCalloutElementDrawCallback, null, null, true, typeof(GameObject));
            CreateListView("helper prefab", null, null, OnHelperElementDrawCallback, null, null, true, typeof(GameObject));

            CreateListView("schematic", null, null, OnSchematicElementDrawCallback, null, null, false, typeof(VideoClip), typeof(Texture2D));

            CreateListView("virtual button", FoldoutTextVirtualButtonCallback, null, null, OnVirtualButtonDrawCallback);
            CreateListView("virtual button element", FoldoutTextVirtualButtonListCallback, null, null, OnVirtualButtonElementDrawCallback);
            CreateListView("virtual button override", null, null, OnVirtualButtonOverrideDrawCallback, null);

            CreateListView("animation", FoldoutTextAnimationCallback, null, null, OnAnimationDrawCallback);
            CreateListView("animation element", null, null, OnAnimationElementDrawCallback, null, AnimationElementFilterStringCallback, false, typeof(GameObject));
        }

        protected abstract T GetFigure(object data, int index);
        protected abstract int GetFigureCount(object data);
        protected abstract void ShowFiguresField(object data, string listName, string fieldId);
        protected abstract List<Schematic> GetSchematicFromData(object data);
        protected abstract void TopDetailInspector(object data, Rect panelRect);
        protected abstract void BottomDetailInspector(object data, Rect panelRect);

        protected virtual void CameraTab(object data, string fieldId)
        {
            Figure figure = data as Figure;
            figureTemp = figure;

            ShowListField("figure camera", figure.cameraDestinations, fieldId + "cam 1");
            ShowListField("monitor camera", figure.monitorCameras, fieldId + "cam 2");
        }

        protected virtual void PartObjectTab(object data, string fieldId)
        {
            Figure figure = data as Figure;

            partObjectEditor.Show(figure.partObjects, fieldId + "part obj", 0);
        }

        public void DetailInspector(int id, object data, Rect panelRect, bool splitTopAndBottom)
        {
            if (id != currentTreeElementId)
            {
                isAnimationInit = false;
                isVirtualButtonInit = false;
            }

            currentTreeElementId = id;

            if (splitTopAndBottom)
                splitter.BeginSplitView(0f, panelRect.width);

            TopDetailInspector(data, panelRect);

            if (splitTopAndBottom)
                splitter.Split();

            BottomDetailInspector(data, panelRect);

            if (splitTopAndBottom)
                splitter.EndSplitView();
        }

        public string PreviewInspector(string name, int index)
        {
            ShowSimpleField("materi name", index.ToString(), ref name);
            return name;
        }

        private void OnCategoriesTabCallback(int selected, object data, string fieldId)
        {
            if (categoryTabs.Length == 0)
                return;

            if (!(selected < categoryTabs.Length))
                selected = 0;

            CategoryTab selectedTab = categoryTabs[selected];

            switch (selectedTab)
            {
                // figure and figure list case sama aja isinya
                case CategoryTab.FIGURE:

                    ShowFiguresField(data, "figure", fieldId);
                    partObjectEditor.TurnOffAssignSelectedObjects();
                    isAnimationInit = false;
                    isVirtualButtonInit = false;
                    break;

                case CategoryTab.FIGURE_LIST:

                    ShowFiguresField(data, "figure", fieldId);
                    partObjectEditor.TurnOffAssignSelectedObjects();
                    isAnimationInit = false;
                    isVirtualButtonInit = false;
                    break;

                case CategoryTab.SCHEMATIC:

                    ShowListField("schematic", GetSchematicFromData(data), fieldId + 1);
                    isAnimationInit = false;
                    isVirtualButtonInit = false;
                    break;

                case CategoryTab.ANIMATION:

                    OpenAnimationTab(data, fieldId, 2);
                    isVirtualButtonInit = false;
                    break;

                case CategoryTab.VIRTUAL_BUTTON:

                    OpenVirtualButtonTab(data, fieldId, 3);
                    isAnimationInit = false;
                    break;
            }

        }

        #region figure

        private string FoldoutTextFigureCallback(object element)
        {
            Figure figure = element as Figure;
            return figure.name;
        }

        private object OnFigureDetailDrawCallback(string listId, object element, int index)
        {
            Figure figure = element as Figure;

            GUILayout.BeginHorizontal();
            ShowSimpleField("figure name", "figure name" + listId + index + 1, ref figure.name);

            if (GUILayout.Button("copy link"))
            {
                string link = @"\link[figure_" + figure.name + "]{link to -> " + figure.name + "}";
                VirtualTrainingEditorUtility.CopyToClipboard(link);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            ShowAssetField("audio clip", listId + index + 3, ref figure.clip);
            ShowTab("figure category", figure, listId + index + 2);

            return figure;
        }

        private void OnFigureListTabCallback(int selected, object data, string fieldId)
        {
            if (figureTabs.Length == 0)
                return;

            Figure figure = data as Figure;
            int idx = 0;

            if (!(selected < figureTabs.Length))
                selected = 0;

            FigureTab selectedTab = figureTabs[selected];

            switch (selectedTab)
            {
                case FigureTab.CAMERA:

                    CameraTab(data, fieldId);

                    break;

                case FigureTab.PART_OBJECT:

                    PartObjectTab(data, fieldId);

                    break;

                case FigureTab.CALLOUT:

                    ShowListField("callout prefab", figure.callouts, fieldId + idx++);

                    break;

                case FigureTab.HELPER:

                    ShowListField("helper prefab", figure.helpers, fieldId + idx++);

                    break;

                case FigureTab.CUTAWAY:

                    cutawayEditor.Show(figure.cutaway, this, fieldId);

                    break;
            }
        }

        private object OnCameraElementDrawCallback(string listId, object element, int index)
        {
            FigureCamera figureCamera = element as FigureCamera;

            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();

            ShowSimpleField("name", listId + "destination Name" + index, ref figureCamera.displayName, 40f, 200f);

            if (GUILayout.Button("copy link") && figureTemp != null)
            {
                string link = @"\link[camerafigure_" + figureTemp.name + "/" + figureCamera.displayName + "]{link to -> " + figureTemp.name + "/" + figureCamera.displayName + "}";
                VirtualTrainingEditorUtility.CopyToClipboard(link);
            }

            GUILayout.Space(5f);
            ShowCameraPositionField("camera " + (index + 1), listId + "destination" + index, ref figureCamera.destination, true);

            GUILayout.EndHorizontal();

            ShowSimpleField("mode", "cam" + listId + index, ref figureCamera.mode, 40f, 200f);

            GUILayout.EndVertical();

            return figureCamera;
        }

        private object OnMonitorCameraElementDrawCallback(string listId, object element, int index)
        {
            MonitorCamera monitorCamera = element as MonitorCamera;

            ShowSimpleField("name", listId + "monitorName" + index, ref monitorCamera.displayName, 40f, 200f);
            GUILayout.Space(5f);
            ShowCameraPositionField("monitor camera " + (index + 1), listId + "monitorDestination" + index, ref monitorCamera.destination, false);

            return monitorCamera;
        }

        private object OnCalloutElementDrawCallback(string listId, object element, int index)
        {
            GameObject go = element as GameObject;
            Callout callout = null;
            if (go != null)
                callout = go.GetComponent<Callout>();

            ShowAssetField("callout prefab", listId + index, ref callout);

            if (callout == null)
                return go;

            return callout.gameObject;
        }

        private object OnHelperElementDrawCallback(string listId, object element, int index)
        {
            GameObject helper = element as GameObject;

            ShowSimpleField("helper prefab", listId + index, ref helper);

            return helper;
        }

        #endregion

        #region schematic

        private object OnSchematicElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();

            Schematic schematic = element as Schematic;

            if (schematic.image == null && schematic.video == null)
            {
                EditorGUILayout.LabelField("drop image / video here ...");
                Rect elementRect = GUILayoutUtility.GetLastRect();
                Event e = Event.current;

                if (elementRect.Contains(e.mousePosition) && e.type == EventType.DragExited)
                {
                    var selections = DragAndDrop.objectReferences;

                    if (selections.Length > 0)
                    {
                        Texture2D image = (object)selections[0] as Texture2D;
                        VideoClip video = (object)selections[0] as VideoClip;

                        if (image != null)
                            schematic.image = image;
                        else if (video != null)
                            schematic.video = video;
                    }
                }
            }
            else
            {
                if (schematic.image != null)
                    ShowAssetField("image", listId + index + 1, ref schematic.image);

                else if (schematic.video != null)
                {
                    ShowAssetField("video", listId + index + 2, ref schematic.video);
                    ShowSimpleField("loop", listId + index + 3, ref schematic.isLooping);
                }

                ShowSimpleField("show immediately", listId + index + 4, ref schematic.showImmediately);

                GUILayout.BeginHorizontal();
                ShowSimpleField("description id", listId + index + 5, ref schematic.descriptionId);

                if (!string.IsNullOrEmpty(schematic.descriptionId))
                {
                    if (GUILayout.Button("copy link"))
                    {
                        string fileName = "";

                        if (schematic.image != null)
                            fileName = schematic.image.name;
                        else if (schematic.video != null)
                            fileName = schematic.video.name;
                        else
                            fileName = schematic.descriptionId;

                        string link = @"\link[schematic_" + schematic.descriptionId + "]{link to -> " + fileName + "}";
                        VirtualTrainingEditorUtility.CopyToClipboard(link);
                    }
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();

            return schematic;
        }

        #endregion

        #region animation

        void OpenAnimationTab(object data, string fieldId, int idx)
        {
            int figureCount = GetFigureCount(data);

            if (isAnimationInit == false)
            {
                for (int i = 0; i < figureCount; i++)
                {
                    if (!FigureAnimationStateLookup.ContainsKey(GetFigure(data, i)))
                    {
                        FigureAnimationState state = new FigureAnimationState();
                        state.isSequenceAnimation = GetFigure(data, i).isSequenceAnimation;
                        FigureAnimationStateLookup.Add(GetFigure(data, i), state);
                    }
                }

                animDatas.Clear();
                for (int l = 0; l < figureCount; l++)
                {
                    if (GetFigure(data, l).animationFigureDatas.Count > 0)
                    {
                        List<Figure> figureOpts = new List<Figure>();
                        for (int f = 0; f < figureCount; f++)
                        {
                            figureOpts.Add(GetFigure(data, f));
                        }

                        MateriViewData view = new MateriViewData(GetFigure(data, l), figureOpts, GetFigure(data, l).animationFigureDatas, null, FigureAnimationStateLookup);
                        view.IsSequenceAnimation = GetFigure(data, l).isSequenceAnimation;
                        view.isShowAnimationUI = GetFigure(data, l).isShowUiAnimation;
                        view.isReverseAnimationOnReset = GetFigure(data, l).isReverseAnimationOnReset;
                        animDatas.Add(view);
                    }
                }

                isAnimationInit = true;
            }

            if (AnimationFieldShortcut.Pressed && VirtualTrainingEditorState.GetLastFocusedWindowId() == WindowId)
            {
                for (int i = 0; i < animDatas.Count; i++)
                {
                    if (VirtualTrainingEditorState.GetFoldoutState(WindowId, "animationcategories" + idx + i))
                    {
                        GameObject[] selecteds = AnimationFieldShortcut.GetSelectedObjects();
                        for (int s = 0; s < selecteds.Length; s++)
                        {
                            GameObjectReference gor = selecteds[s].GetComponent<GameObjectReference>();
                            if (gor != null)
                            {
                                AnimationFigureData newAnimation = new AnimationFigureData();
                                newAnimation.isAnimatedScript = true;
                                newAnimation.animationType = AnimationType.AnimScript;
                                newAnimation.gameObjectInteraction.Id = gor.Id;
                                animDatas[i].animations.Add(newAnimation);
                            }
                        }
                    }
                }

                AnimationFieldShortcut.SwitchOff();
                Debug.Log("assign object to animation shortcut");
            }

            ShowListField("animation", animDatas, fieldId + idx);

            if (figureCount != 0)
            {
                for (int i = 0; i < animDatas.Count; i++)
                {
                    //check data baru
                    if (animDatas[i].figure == null)
                    {
                        List<Figure> figureOpts = new List<Figure>();
                        for (int f = 0; f < figureCount; f++)
                        {
                            figureOpts.Add(GetFigure(data, f));
                        }

                        animDatas[i] = new MateriViewData(GetFigure(data, 0), figureOpts, new List<AnimationFigureData>(), null, FigureAnimationStateLookup);
                    }
                }

                for (int i = 0; i < figureCount; i++)
                {
                    GetFigure(data, i).animationFigureDatas.Clear();

                    for (int j = 0; j < animDatas.Count; j++)
                    {
                        string figureMateri = JsonUtility.ToJson(GetFigure(data, i));
                        string animDatas = JsonUtility.ToJson(this.animDatas[j].figure);

                        if (string.Equals(figureMateri, animDatas))
                        {
                            GetFigure(data, i).isSequenceAnimation = this.animDatas[j].IsSequenceAnimation;
                            GetFigure(data, i).isShowUiAnimation = this.animDatas[j].isShowAnimationUI;
                            GetFigure(data, i).isReverseAnimationOnReset = this.animDatas[j].isReverseAnimationOnReset;
                            for (int m = 0; m < this.animDatas[j].animations.Count; m++)
                            {
                                if (this.animDatas[j].IsSequenceAnimation)
                                {
                                    if (this.animDatas[j].animations[m].isInit == false)
                                    {
                                        this.animDatas[j].animations[m].sequentialQueue = m;
                                        this.animDatas[j].animations[m].isInit = true;
                                    }
                                }
                                this.animDatas[j].animations[m].isSequential = this.animDatas[j].IsSequenceAnimation;
                                this.animDatas[j].animations[m].isShowAnimationUI = this.animDatas[j].isShowAnimationUI;
                                this.animDatas[j].animations[m].isReverseAnimationOnReset = this.animDatas[j].isReverseAnimationOnReset;

                                GetFigure(data, i).animationFigureDatas.Add(this.animDatas[j].animations[m]);
                            }
                        }
                    }
                }
            }
            else
            {
                animDatas.Clear();
            }
        }

        private string FoldoutTextAnimationCallback(object element)
        {
            MateriViewData materiView = element as MateriViewData;
            if (materiView.figure != null)
                return materiView.figure.name;
            else
                return "none figure";
        }

        protected virtual object OnAnimationDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            MateriViewData animationOnFigure = element as MateriViewData;

            int selectedIndex = 0;
            List<string> figuresName = new List<string>();
            for (int i = 0; i < animationOnFigure.figureOption.Count; i++)
            {
                string name = i + "_" + animationOnFigure.figureOption[i].name;
                figuresName.Add(name);
                if (object.Equals(animationOnFigure.figure, animationOnFigure.figureOption[i]))
                {
                    selectedIndex = i;
                }
            }

            if (figuresName.Count > 0)
            {
                selectedIndex = ShowDropdownField("figure", listId + index + 2, figuresName[selectedIndex], figuresName);
                bool isSequence = animationOnFigure.IsSequenceAnimation;
                ShowSimpleField("Sequence Animation", listId + index + 3, ref isSequence, 155f, 305f);
                bool isShowAnimationUI = animationOnFigure.isShowAnimationUI;
                ShowSimpleField("Show ui animation", listId + index + 4, ref isShowAnimationUI, 155f, 305f);
                bool isReverseOnReset = animationOnFigure.isReverseAnimationOnReset;
                ShowSimpleField("Reverse animation on reset", listId + index + 5, ref isReverseOnReset, 155f, 305f);
                animationOnFigure.IsSequenceAnimation = isSequence;
                animationOnFigure.isShowAnimationUI = isShowAnimationUI;
                animationOnFigure.isReverseAnimationOnReset = isReverseOnReset;
                animationOnFigure.figure = animationOnFigure.figureOption[selectedIndex];
                ShowListField("animation element", animationOnFigure.animations, listId + index + 5);
            }

            GUILayout.EndVertical();
            return animationOnFigure;
        }

        private object OnAnimationElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            AnimationFigureData animationFigureData = element as AnimationFigureData;

            if (animationFigureData.isSequential)
                ShowSimpleField("sequence queue", listId + index + idx++, ref animationFigureData.sequentialQueue);

            ShowSimpleField("animation type", listId + index + idx++, ref animationFigureData.animationType);

            if (animationFigureData.animationType == AnimationType.AnimManual)
            {
                ShowSimpleField("Animation state", listId + index + idx++, ref animationFigureData.animationName);
                ShowSimpleField("Animator layer", listId + index + idx++, ref animationFigureData.animationLayer);
                ShowSimpleField("Animator speed parameter", listId + index + idx++, ref animationFigureData.animationSpeedParameter);
                ShowSimpleField("Animator controller", listId + index + idx++, ref animationFigureData.animatorController);
            }
            else if (animationFigureData.animationType == AnimationType.AnimScript)
            {
                ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Animation", listId + index + idx++, ref animationFigureData.scriptedAnimationElement);
                ShowSimpleField("Object Animation", listId + index + idx++, ref animationFigureData.gameObjectInteraction);
                ShowSimpleField("Reverse Animation", listId + index + idx++, ref animationFigureData.isReverseAnimation);
                ShowSimpleField("Disabled After Finish", listId + index + idx++, ref animationFigureData.isDisabledFinished);
                if (animationFigureData.isReverseAnimation == true)
                {
                    ShowSimpleField("Enabled at Start", listId + index + idx++, ref animationFigureData.isEnabledAtStart);
                }
            }
            else if (animationFigureData.animationType == AnimationType.AnimVB)
            {
                ShowReferenceField<VirtualButtonElementType, VirtualButtonTreeElement>("virtual button", listId + index + idx++, ref animationFigureData.virtualButton.virtualButtonElementType);

                if (animationFigureData.virtualButton.virtualButtonElementType.GetData() != null)
                {
                    if (animationFigureData.virtualButton.virtualButtonElementType.GetData().vbType == VirtualButtonType.Drag)
                    {
                        ShowSimpleField("override value", listId + index + idx++, ref animationFigureData.virtualButton.isOverride);
                        if (animationFigureData.virtualButton.isOverride == true)
                        {
                            ShowSimpleField("start value", listId + index + idx++, ref animationFigureData.virtualButton.startValue);
                            ShowSimpleField("end value", listId + index + idx++, ref animationFigureData.virtualButton.endValue);
                            ShowSimpleField("speed", listId + index + idx++, ref animationFigureData.virtualButton.speedVirtualButton);
                            if (animationFigureData.virtualButton.virtualButtonElementType.GetData().direction == DragDirection.Both)
                            {
                                ShowSimpleField("start value Y", listId + index + idx++, ref animationFigureData.virtualButton.startValueY);
                                ShowSimpleField("end value Y", listId + index + idx++, ref animationFigureData.virtualButton.endValueY);
                            }
                        }
                    }
                }
            }

            ShowSimpleField("Ignore if already played", listId + index + idx++, ref animationFigureData.ignoreIfAlreadyPlayed);

            GUILayout.EndVertical();
            return animationFigureData;
        }

        private string AnimationElementFilterStringCallback(object element)
        {
            AnimationFigureData animationFigureData = element as AnimationFigureData;

            if (animationFigureData.animationType == AnimationType.AnimScript)
            {
                if (animationFigureData.gameObjectInteraction.gameObject != null)
                    return animationFigureData.gameObjectInteraction.gameObject.name;
                else
                    return animationFigureData.animationName;
            }
            else if (animationFigureData.animationType == AnimationType.AnimVB)
            {
                if (animationFigureData.virtualButton.virtualButtonElementType.GetData() != null)
                    return animationFigureData.virtualButton.virtualButtonElementType.GetData().name;
                else
                    return animationFigureData.animationName;
            }
            else
            {
                if (animationFigureData.animatorController.gameObject != null)
                    return animationFigureData.animatorController.gameObject.name;
                else
                    return animationFigureData.animationName;
            }
        }

        #endregion

        #region virtual button

        private string FoldoutTextVirtualButtonCallback(object element)
        {
            MateriViewData materiView = element as MateriViewData;
            if (materiView.figure != null)
                return materiView.figure.name;
            else
                return "none figure";
        }

        private object OnVirtualButtonDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();

            MateriViewData virtualbuttonFigures = element as MateriViewData;

            int selectedIndex = 0;
            List<string> figuresName = new List<string>();
            for (int i = 0; i < virtualbuttonFigures.figureOption.Count; i++)
            {
                string name = i + "_" + virtualbuttonFigures.figureOption[i].name;
                figuresName.Add(name);
                if (object.Equals(virtualbuttonFigures.figure, virtualbuttonFigures.figureOption[i]))
                {
                    selectedIndex = i;
                }
            }

            if (figuresName.Count > 0)
            {
                selectedIndex = ShowDropdownField("figure", listId + index + 2, figuresName[selectedIndex], figuresName);
                virtualbuttonFigures.figure = virtualbuttonFigures.figureOption[selectedIndex];
                ShowListField("virtual button element", virtualbuttonFigures.virtualButtonElements, listId + index + 3);
            }

            GUILayout.EndVertical();
            return virtualbuttonFigures;
        }
        private string FoldoutTextVirtualButtonListCallback(object element)
        {
            VirtualButtonFigureData materiView = element as VirtualButtonFigureData;
            if (materiView.virtualButtonElementType.GetData() != null)
                return materiView.virtualButtonElementType.GetData().name;
            else
                return "none virtual button";
        }
        private object OnVirtualButtonElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VirtualButtonFigureData virtualbuttonElement = element as VirtualButtonFigureData;
            ShowReferenceField<VirtualButtonElementType, VirtualButtonTreeElement>("Virtual Button", listId + index + idx++, ref virtualbuttonElement.virtualButtonElementType);
            if (virtualbuttonElement.virtualButtonElementType.GetData() != null)
            {
                ShowSimpleField("Override Value", listId + index + idx++, ref virtualbuttonElement.isOverrideVirtualButton);
                if (virtualbuttonElement.isOverrideVirtualButton)
                {
                    ShowListField("virtual button override", virtualbuttonElement.overrideVirtualButton, listId + index + idx);
                }
            }

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        private object OnVirtualButtonOverrideDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            OverrideVariableVirtualButton virtualbuttonElement = element as OverrideVariableVirtualButton;

            ShowSimpleField("Override Variable", listId + index + idx++, ref virtualbuttonElement.OverrideVirtualButton);
            if (virtualbuttonElement.OverrideVirtualButton == OverrideVirtualButton.DefaultValue)
                ShowSimpleField("Default Value", listId + index + idx++, ref virtualbuttonElement.defaultValue);

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        void OpenVirtualButtonTab(object data, string fieldId, int idx)
        {
            int figureCount = GetFigureCount(data);

            if (isVirtualButtonInit == false)
            {
                virtualButtonDatas.Clear();
                for (int l = 0; l < figureCount; l++)
                {
                    if (GetFigure(data, l).virtualButtonElement.Count > 0)
                    {
                        List<Figure> figureOpts = new List<Figure>();
                        for (int f = 0; f < figureCount; f++)
                        {
                            figureOpts.Add(GetFigure(data, f));
                        }

                        MateriViewData view = new MateriViewData(GetFigure(data, l), figureOpts, null, GetFigure(data, l).virtualButtonElement, FigureAnimationStateLookup);
                        virtualButtonDatas.Add(view);
                    }
                }

                isVirtualButtonInit = true;
            }
            ShowListField("virtual button", virtualButtonDatas, fieldId + idx++);

            if (figureCount != 0)
            {
                for (int i = 0; i < virtualButtonDatas.Count; i++)
                {
                    if (virtualButtonDatas[i].figure == null)
                    {
                        List<Figure> figureOpts = new List<Figure>();
                        for (int f = 0; f < figureCount; f++)
                        {
                            figureOpts.Add(GetFigure(data, f));
                        }

                        virtualButtonDatas[i] = new MateriViewData(GetFigure(data, 0), figureOpts, null, new List<VirtualButtonFigureData>(), FigureAnimationStateLookup);
                    }
                }

                for (int i = 0; i < figureCount; i++)
                {
                    GetFigure(data, i).virtualButtonElement.Clear();

                    for (int j = 0; j < virtualButtonDatas.Count; j++)
                    {
                        string figureMateri = JsonUtility.ToJson(GetFigure(data, i));
                        string animDatas = JsonUtility.ToJson(virtualButtonDatas[j].figure);
                        if (string.Equals(figureMateri, animDatas))
                        {
                            for (int m = 0; m < virtualButtonDatas[j].virtualButtonElements.Count; m++)
                            {
                                GetFigure(data, i).virtualButtonElement.Add(virtualButtonDatas[j].virtualButtonElements[m]);
                            }
                        }
                    }
                }
            }
            else
            {
                virtualButtonDatas.Clear();
            }
        }

        #endregion
    }
}

#endif
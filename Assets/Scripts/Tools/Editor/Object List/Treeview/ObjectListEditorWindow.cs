﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class ObjectListEditorWindow : TreeViewEditorBase<ObjectListTreeAsset, ObjectListEditorWindow, ObjectListData, ObjectListTreeElement>
    {
        ObjectListContentEditor objectlistContentEditor;
        int createHierarchyMaxDepth = 2;
        string find = "", replace = "";

        [MenuItem("Virtual Training/FreePlay/Object List Editor &o")]
        public static void GetWindow()
        {
            ObjectListEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            ObjectListEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
                return true;
            }
            return false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            objectlistContentEditor = new ObjectListContentEditor(RecordUndo, () => WindowId);
        }

        protected override TreeViewWithTreeModel<ObjectListTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<ObjectListTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<ObjectListTreeElement>(TreeViewState, treeModel);
        }

        protected override ObjectListTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new ObjectListTreeElement("new object", depth, id, false);
        }

        private void CreateHierarchy()
        {
            TreeView.SelectAllRows();
            TreeView.RemoveItem();
            TreeView.Reload();

            GameObject objRoot = VirtualTrainingSceneManager.GameObjectRoot;
            for (int i = 0; i < objRoot.transform.childCount; i++)
            {
                CreateHierarchy(objRoot.transform.GetChild(i).gameObject);
            }
        }

        private void CreateHierarchy(GameObject obj)
        {
            if (createHierarchyMaxDepth > 0 && obj.transform.GetAllParents().Count - 1 >= createHierarchyMaxDepth)
                return;

            ObjectListTreeElement newElement = new ObjectListTreeElement(obj.name, -1, TreeView.TreeModel.GenerateUniqueID(), false);
            int parentId = 0;

            var rows = TreeView.GetRows();
            foreach (var row in rows)
            {
                if (row.displayName == obj.transform.parent.name)
                {
                    parentId = row.id;
                    break;
                }
            }

            ObjectListTreeElement parent = TreeView.FindElement(parentId);

            GameObjectType got = new GameObjectType(obj);
            newElement.ObjectListData.objects.Add(got);

            TreeView.TreeModel.AddElement(newElement, parent, int.MaxValue);
            TreeView.ExpandAll();

            for (int c = 0; c < obj.transform.childCount; c++)
            {
                CreateHierarchy(obj.transform.GetChild(c).gameObject);
            }
        }

        private void FindAndReplace()
        {
            var data = ScriptableObjectTemp.GetData();
            var elements = data.GetTreeElements();
            for (int i = 0; i < elements.Count; i++)
            {
                ObjectListTreeElement objectListTreeElement = elements[i] as ObjectListTreeElement;
                objectListTreeElement.name = objectListTreeElement.name.Replace(find, replace);
            }
            TreeView.Reload();
        }

        protected override void ContentView(List<ObjectListTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            GUILayout.Space(10f);

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder)
                    objectlistContentEditor.DetailInspector(selectedElements[0].ObjectListData);
            }
            else
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        objectlistContentEditor.PreviewInspector(selectedElements[i].ObjectListData, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].ObjectListData.name)
                    {
                        selectedElements[i].name = selectedElements[i].ObjectListData.name;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].ObjectListData.name)
                    {
                        selectedElements[i].ObjectListData.name = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            createHierarchyMaxDepth = EditorGUILayout.IntField("max depth (0 = all)", createHierarchyMaxDepth, GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH));
            if (GUILayout.Button("create hierarchy from object root", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
            {
                CreateHierarchy();
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            find = EditorGUILayout.TextField("find", find, GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH));
            EditorGUILayout.BeginHorizontal();
            replace = EditorGUILayout.TextField("replace", replace, GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH));
            if (GUILayout.Button("replace", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
            {
                FindAndReplace();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndVertical();
        }
    }
}
#endif

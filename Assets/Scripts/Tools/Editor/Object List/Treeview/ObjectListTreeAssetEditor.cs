﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(ObjectListTreeAsset))]
    public class ObjectListTreeAssetEditor : TreeAssetEditorBase<ObjectListTreeAsset, ObjectListTreeElement, ObjectListData>
    {
        // Start is called before the first frame update
        protected override ObjectListTreeAsset GetAsset()
        {
            return (ObjectListTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<ObjectListData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<ObjectListTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}

#endif

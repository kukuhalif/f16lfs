﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using XNodeEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomNodeGraphEditor(typeof(TroubleshootGraph))]
    public class TroubleshootGraphEditor : NodeGraphEditor
    {
        public static Materi currentMateri;
        public static TroubleshootGraph graphTemp;
        bool loadPressed, savePressed;
        public static EditorGUISplitView splitter;
        TroubleshootContentEditor troubleshootContentEditor;
        TroubleshootGraph currentGraph;
        string currentControlName;
        Texture2D texture;
        private Vector2 contentViewScrollPosition = new Vector2();
        TroubleshootNode content;

        public static void GetWindow()
        {
            OpenEditorWindow();
        }

        public TroubleshootGraphEditor()
        {
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }
        ~TroubleshootGraphEditor()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
        }

        private void PlayModeStateChanged(PlayModeStateChange obj)
        {
            InitializeOnPlay();
        }

        void InitializeOnPlay()
        {
            if (EditorPrefs.HasKey("OpenTroubleshoot"))
            {
                //string path = EditorPrefs.GetString("OpenTroubleshoot");
                //TroubleshootGraph troubleshoot = (TroubleshootGraph)AssetDatabase.LoadAssetAtPath(path, typeof(TroubleshootGraph));
                //graphTemp = troubleshoot;
                //var xrayMats = DatabaseManager.GetXrayMaterials();
                //troubleshootContentEditor = new TroubleshootContentEditor(ApplyOverrideData, null, graphTemp.name, xrayMats);
                //if (graphTemp != null)
                //{
                //    string assetPath = AssetDatabase.GetAssetPath(graphTemp.GetInstanceID());
                //    NodeEditorWindow.SetAssetPath(assetPath, troubleshoot.name);
                //}
                //VirtualTrainingEditorState.SetLastFocusedWindowId(graphTemp.name);

                // close window aja, daripada error
                window.Close();
            }
        }

        public override void OnWindowFocus()
        {
            base.OnWindowFocus();
            if (graphTemp != null)
                VirtualTrainingEditorState.SetLastFocusedWindowId(graphTemp.name);
        }

        public override void OnOpen()
        {
            base.OnOpen();
            NodeEditorWindow.OnCloseEditorAction += TroubleshootGraphEditor_OnCloseEditorAction;
            splitter = new EditorGUISplitView(EditorGUISplitView.Direction.Horizontal, "xNode");

            if (graphTemp != null)
            {
                var xrayMats = DatabaseManager.GetXrayMaterials();
                troubleshootContentEditor = new TroubleshootContentEditor(ApplyOverrideData, null, () => "troubleshoot", xrayMats);
            }

            if (File.Exists(Application.dataPath + "/Temp/" + NodeEditorWindow.TempName + ".asset"))
            {
                XNode.NodeGraph currentGraphTemp = (XNode.NodeGraph)AssetDatabase.LoadAssetAtPath("Assets/Temp/" + NodeEditorWindow.TempName + ".asset", typeof(XNode.NodeGraph));
                currentGraph = currentGraphTemp as TroubleshootGraph;
            }


            texture = new Texture2D(1, 1);
            texture.SetPixel(0, 0, new Color(0.22f, 0.22f, 0.22f));
            texture.Apply();
        }

        private void TroubleshootGraphEditor_OnCloseEditorAction()
        {
            ApplyOverrideData();
            if (currentGraph != null)
                EditorUtility.SetDirty(currentGraph);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

            troubleshootContentEditor.SaveCameraDestination();

            NodeEditorWindow.OnCloseEditorAction -= TroubleshootGraphEditor_OnCloseEditorAction;
            //NodeEditorWindow.SaveTempFile();
            if (checkMD5(NodeEditorWindow.AssetPath) != checkMD5(Application.dataPath + "/Temp/" + NodeEditorWindow.TempName + ".asset"))
            {
                if (EditorUtility.DisplayDialog("Save", "Save?", "yes", "no"))
                {
                    NodeEditorWindow.SaveTempFile();
                }
            }
        }
        public string checkMD5(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        return Encoding.Default.GetString(md5.ComputeHash(stream));
                    }
                }
            }
            return null;
        }

        // Use this for initialization
        public override string GetNodeMenuName(System.Type type)
        {
            //Debug.Log(type.Namespace);
            if (type.Namespace == "VirtualTraining.Core")
            {
                return base.GetNodeMenuName(type).Replace("Virtual Training/Core/", "");
            }
            else return null;
        }

        void ShortcutInput()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.KeyDown:

                    if (e.alt)
                    {
                        if (e.keyCode == KeyCode.L)
                        {
                            loadPressed = true;
                            e.Use();
                        }
                        else if (e.keyCode == KeyCode.S)
                        {
                            savePressed = true;
                            e.Use();
                        }
                    }
                    break;
            }
        }

        Rect toolbarRect
        {
            get { return new Rect(20f, 00f, splitter.GetSplitPosition() - 10f, 0); }
        }

        Rect graphTroubleshootRect
        {
            get { return new Rect(0, 0, splitter.GetSplitPosition() - 3f, window.position.height - 30); }
        }
        protected Rect detailPanelRect
        {
            get { return new Rect(0f, 0f, window.position.width - splitter.GetSplitPosition() - 5f, window.position.height - 40); }
        }

        Rect bottomToolbar
        {
            get { return new Rect(0, window.position.height - 30f, window.position.width, 30f); }
        }

        public override void OnGUI()
        {
            base.OnGUI();
            ShortcutInput();
            splitter.BeginSplitView(toolbarRect.y, toolbarRect.height + graphTroubleshootRect.height);

            NodeEditorWindow.newGraphRect = graphTroubleshootRect;


            splitter.Split();
            GUILayout.BeginArea(detailPanelRect);

            GUI.skin.box.normal.background = texture;
            GUI.Box(detailPanelRect, GUIContent.none);

            contentViewScrollPosition = EditorGUILayout.BeginScrollView(contentViewScrollPosition);
            EditorGUILayout.BeginVertical();

            if (currentGraph != null)
            {
                if (content != null)
                {
                    TroubleshootNode contentTemp = Selection.activeObject as TroubleshootNode;
                    if (contentTemp != null)
                    {
                        if (contentTemp != content)
                        {
                            content = contentTemp;

                            // unfocus element
                            GUI.FocusControl(null);
                        }
                    }
                }
                else
                {
                    if (currentGraph.nodes.Count > 0)
                        content = currentGraph.nodes[0] as TroubleshootNode;
                }
                if (troubleshootContentEditor != null && troubleshootContentEditor != null && content != null)
                    troubleshootContentEditor.DetailInspector(content.id, content, detailPanelRect, true);

                if (!string.IsNullOrEmpty(GUI.GetNameOfFocusedControl()))
                {
                    if (GUI.GetNameOfFocusedControl() != currentControlName)
                    {
                        EditorUtility.SetDirty(currentGraph);
                        currentControlName = GUI.GetNameOfFocusedControl();
                    }
                }
                EditorUtility.SetDirty(currentGraph);
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
            GUILayout.EndArea();
            splitter.EndSplitView();

            GUILayout.BeginHorizontal();
            GUI.skin.box.normal.background = texture;
            GUI.Box(bottomToolbar, GUIContent.none);

            GUILayout.FlexibleSpace();

            if (loadPressed || GUILayout.Button("load (alt + L)", GUILayout.Width(100)))
            {
                if (EditorUtility.DisplayDialog("Load", "Load ?", "yes", "no"))
                {
                    EditorUtility.SetDirty(currentGraph);
                    NodeEditorWindow.LoadTempFile();
                    OnOpen();
                }
                loadPressed = false;
            }

            if (savePressed || GUILayout.Button("save (alt + S)", GUILayout.Width(100)))
            {
                if (EditorUtility.DisplayDialog("Save", "Save ?", "yes", "no"))
                {
                    ApplyOverrideData();
                    if (currentGraph != null)
                        EditorUtility.SetDirty(currentGraph);

                    NodeEditorWindow.SaveTempFile();
                }
                savePressed = false;
            }

            GUILayout.EndHorizontal();
            window.Repaint();
        }

        private void Save()
        {
            if (EditorUtility.DisplayDialog("Save", "Save ?", "yes", "no"))
            {
                ApplyOverrideData();
                if (currentGraph != null)
                    EditorUtility.SetDirty(currentGraph);

                NodeEditorWindow.SaveTempFile();
            }
        }

        private void ApplyOverrideData()
        {
            for (int i = 0; i < currentGraph.nodes.Count; i++)
            {
                if (currentGraph.nodes[i] == null)
                {
                    currentGraph.nodes.RemoveAt(i);
                    return;
                }

                TroubleshootNode troubleshootNode = currentGraph.nodes[i] as TroubleshootNode;
                for (int f = 0; f < troubleshootNode.content.troubleshootData.figures.Count; f++)
                {
                    if (troubleshootNode.content.troubleshootData.figures[f].cameraOverrideSource.GetId() != -1)
                    {
                        troubleshootNode.content.troubleshootData.figures[f].cameraDestinations.Clear();
                        var cams = troubleshootNode.content.troubleshootData.figures[f].cameraOverrideSource.GetData().figures[troubleshootNode.content.troubleshootData.figures[f].cameraOverrideSourceFigureIndex].cameraDestinations;
                        for (int s = 0; s < cams.Count; s++)
                        {
                            troubleshootNode.content.troubleshootData.figures[f].cameraDestinations.Add(VirtualTrainingEditorUtility.CloneObject(cams[s]));
                        }
                    }

                    if (troubleshootNode.content.troubleshootData.figures[f].partObjectOverrideSource.GetId() != -1)
                    {
                        troubleshootNode.content.troubleshootData.figures[f].partObjects.Clear();
                        var parts = troubleshootNode.content.troubleshootData.figures[f].partObjectOverrideSource.GetData().figures[troubleshootNode.content.troubleshootData.figures[f].partObjectOverrideSourceFigureIndex].partObjects;
                        for (int s = 0; s < parts.Count; s++)
                        {
                            troubleshootNode.content.troubleshootData.figures[f].partObjects.Add(VirtualTrainingEditorUtility.CloneObject(parts[s]));
                        }
                    }
                }
            }
        }

        public static NodeEditorWindow OpenEditorWindow()
        {
            string key = "lastTroubleshootEditor";
            string lastFile = "";
            if (EditorPrefs.HasKey(key))
                lastFile = EditorPrefs.GetString(key);

            string[] guids = AssetDatabase.FindAssets("t:" + typeof(TroubleshootGraph).ToString());
            foreach (string guid in guids)
            {
                UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(TroubleshootGraph));
                if (string.IsNullOrEmpty(lastFile) || obj.name == lastFile)
                {
                    return OpenEditorWindow(obj as TroubleshootGraph);
                }
            }
            return null;
        }

        public static NodeEditorWindow OpenEditorWindow(TroubleshootGraph scriptableObjectAsset)
        {

            string key = "lastTroubleshootEditor";
            EditorPrefs.SetString(key, scriptableObjectAsset.name);

            NodeEditorWindow window = EditorWindow.GetWindow<NodeEditorWindow>();
            window.Focus();
            window.Repaint();
            window.titleContent = new GUIContent("Troubleshoot");

            return window;
        }

        public static void SetMateri(ref Materi materi)
        {
            NodeEditorWindow[] oldWindows = Resources.FindObjectsOfTypeAll<NodeEditorWindow>();

            if (oldWindows != null && oldWindows.Length > 0)
            {
                NodeEditorWindow window = EditorWindow.GetWindow<NodeEditorWindow>();
                if (window != null)
                {
                    TroubleshootGraphEditor tge = window.graphEditor as TroubleshootGraphEditor;
                    if (tge != null)
                    {
                        tge.Save();
                    }
                }
            }

            currentMateri = materi;
            graphTemp = materi.troubleshootData.troubleshootGraph;

            string key = "OpenTroubleshoot";
            string pathInstance = AssetDatabase.GetAssetPath(materi.troubleshootData.troubleshootGraph.GetInstanceID());

            EditorPrefs.SetString(key, pathInstance);
            NodeEditorWindow.CopyNodeGraph(materi.troubleshootData.troubleshootGraph.GetInstanceID());
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

            graphTemp = (TroubleshootGraph)AssetDatabase.LoadAssetAtPath("Assets/Temp/" + NodeEditorWindow.TempName + ".asset", typeof(TroubleshootGraph));
            OpenEditorWindow(graphTemp);
            VirtualTrainingEditorState.SetLastFocusedWindowId(graphTemp.name);

            // unfocus element
            GUI.FocusControl(null);
        }
    }
}

#endif
﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNodeEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomNodeEditor(typeof(TroubleshootNode))]
    public class TroubleshootNodeEditor : NodeEditor
    {
        public override void OnHeaderGUI()
        {
            base.OnHeaderGUI();
            TroubleshootNode node = target as TroubleshootNode;
            node.name = node.content.troubleshootData.title;
        }
        public override void OnBodyGUI()
        {
            base.OnBodyGUI();
        }
    }
}

#endif
#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{

    public class TroubleshootContentEditor : ContentEditorBase<TroubleshootFigure>
    {
        Action applyOverrideCallback;
        TroubleshootDataModel playedTroubleshoot;
        List<string> troubleshootNodes = new List<string>();

        int currentFigureIndex = 0;
        int currentCameraIndex = 0;

        public TroubleshootContentEditor(Action applyOverrideCallback, Action beforeModifiedCallback, Func<string> windowId, List<Material> xrayMats) :
            base(beforeModifiedCallback, windowId, xrayMats,
                new CategoryTab[] { CategoryTab.FIGURE_LIST, CategoryTab.ANIMATION, CategoryTab.VIRTUAL_BUTTON },
                new FigureTab[] { FigureTab.CAMERA, FigureTab.PART_OBJECT, FigureTab.CALLOUT, FigureTab.HELPER, FigureTab.CUTAWAY })
        {
            this.applyOverrideCallback = applyOverrideCallback;
        }

        protected override List<Schematic> GetSchematicFromData(object data)
        {
            return null;
        }

        protected override TroubleshootFigure GetFigure(object data, int index)
        {
            TroubleshootNode troubleshootNode = data as TroubleshootNode;
            return troubleshootNode.content.troubleshootData.figures[index];
        }

        protected override int GetFigureCount(object data)
        {
            TroubleshootNode troubleshootNode = data as TroubleshootNode;
            return troubleshootNode.content.troubleshootData.figures.Count;
        }

        protected override void ShowFiguresField(object data, string listName, string fieldId)
        {
            TroubleshootNode troubleshootNode = data as TroubleshootNode;
            ShowListField(listName, troubleshootNode.content.troubleshootData.figures, fieldId);
        }

        protected override void CameraTab(object data, string fieldId)
        {
            TroubleshootFigure figure = data as TroubleshootFigure;

            ShowReferenceField<TroubleshootNodeType, TroubleshootNode>("overide camera", fieldId + "override cam", ref figure.cameraOverrideSource);

            if (figure.cameraOverrideSource.GetId() != -1)
            {
                var figures = figure.cameraOverrideSource.GetData().figures;
                List<string> figOption = new List<string>();
                for (int i = 0; i < figures.Count; i++)
                {
                    figOption.Add(figures[i].name);
                }
                if (figures.Count > figure.cameraOverrideSourceFigureIndex)
                    figure.cameraOverrideSourceFigureIndex = ShowDropdownField("select figure source", fieldId + "figure select", figures[figure.cameraOverrideSourceFigureIndex].name, figOption);
                else
                    figure.cameraOverrideSourceFigureIndex = figures.Count - 1;
            }

            if (figure.cameraOverrideSource.GetId() == -1)
            {
                base.CameraTab(data, fieldId);
            }
        }

        protected override void PartObjectTab(object data, string fieldId)
        {
            TroubleshootFigure figure = data as TroubleshootFigure;

            ShowReferenceField<TroubleshootNodeType, TroubleshootNode>("overide part object", fieldId + "override part obj", ref figure.partObjectOverrideSource);

            if (figure.partObjectOverrideSource.GetId() != -1)
            {
                var figures = figure.partObjectOverrideSource.GetData().figures;
                List<string> figOption = new List<string>();
                for (int i = 0; i < figures.Count; i++)
                {
                    figOption.Add(figures[i].name);
                }
                if (figures.Count > figure.partObjectOverrideSourceFigureIndex)
                    figure.partObjectOverrideSourceFigureIndex = ShowDropdownField("select figure source", fieldId + "figure select", figures[figure.partObjectOverrideSourceFigureIndex].name, figOption);
                else
                    figure.partObjectOverrideSourceFigureIndex = figures.Count - 1;
            }

            if (figure.partObjectOverrideSource.GetId() == -1)
            {
                base.PartObjectTab(data, fieldId);
            }
        }

        protected override void BottomDetailInspector(object data, Rect panelRect)
        {
            TroubleshootNode troubleshootNode = data as TroubleshootNode;
            ShowTab("categories", troubleshootNode);
        }

        protected override void TopDetailInspector(object data, Rect panelRect)
        {
            TroubleshootNode troubleshootNode = data as TroubleshootNode;
            ContentTroubleshootNodeGraph troubleshoot = troubleshootNode.content;
            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("play troubleshoot (alt + t)") || PlayTestShortcut.PressedWindowId == WindowId)
                {
                    PlayTestShortcut.Played();
                    applyOverrideCallback.Invoke();
                    playedTroubleshoot = troubleshoot.troubleshootData;
                    currentFigureIndex = 0;
                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new TroubleshootPlayEvent(troubleshootNode));
                }

                if (playedTroubleshoot == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("Reset Troubleshoot"))
                {
                    playedTroubleshoot = null;
                    EventManager.TriggerEvent(new TroubleshootPlayEvent());
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous figure"))
                {
                    currentFigureIndex--;
                    if (currentFigureIndex < 0)
                        currentFigureIndex = playedTroubleshoot.figures.Count - 1;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PlayFigureLinkEvent(playedTroubleshoot.figures[currentFigureIndex].name, MateriElementType.Troubleshoot, playedTroubleshoot));
                }

                if (GUILayout.Button("play next figure"))
                {
                    currentFigureIndex++;
                    if (playedTroubleshoot.figures.Count <= currentFigureIndex)
                        currentFigureIndex = 0;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PlayFigureLinkEvent(playedTroubleshoot.figures[currentFigureIndex].name, MateriElementType.Troubleshoot, playedTroubleshoot));
                }

                GUILayout.Label("current figure : " + currentFigureIndex);

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous camera"))
                {
                    currentCameraIndex--;
                    if (currentCameraIndex < 0)
                        currentCameraIndex = playedTroubleshoot.figures[currentFigureIndex].cameraDestinations.Count - 1;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedTroubleshoot.figures[currentFigureIndex].name, playedTroubleshoot.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.Troubleshoot, playedTroubleshoot));
                }

                if (GUILayout.Button("play next camera"))
                {
                    currentCameraIndex++;
                    if (playedTroubleshoot.figures[currentFigureIndex].cameraDestinations.Count <= currentCameraIndex)
                        currentCameraIndex = 0;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedTroubleshoot.figures[currentFigureIndex].name, playedTroubleshoot.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.Troubleshoot, playedTroubleshoot));
                }

                GUILayout.Label("current camera : " + currentCameraIndex);

                GUILayout.EndHorizontal();

                if (playedTroubleshoot == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);
            troubleshootNodes.Clear();

            ShowSimpleField("Node Title", ref troubleshoot.troubleshootData.title);

            ShowSimpleField("Node Add On Text", ref troubleshoot.troubleshootData.text);
            if (!string.IsNullOrEmpty(troubleshoot.troubleshootData.text))
            {
                ShowSimpleField("Offset X Text ", ref troubleshoot.troubleshootData.offsetX);
                ShowSimpleField("Offset Y Text ", ref troubleshoot.troubleshootData.offsetY);
            }

            ShowAssetField("Custom Button Prefab", ref troubleshoot.troubleshootData.prefabCustomButton);
            ShowSimpleField("Disable Environment", ref troubleshoot.troubleshootData.disableEnvironment);

            ShowTextArea("description", panelRect.width - 15f, ref troubleshoot.troubleshootData.description, true);

        }
    }
}

#endif
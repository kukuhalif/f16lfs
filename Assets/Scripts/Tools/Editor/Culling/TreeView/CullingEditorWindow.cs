#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class CullingEditorWindow : TreeViewEditorBase<CullingTreeAsset, CullingEditorWindow, CullingData, CullingTreeElement>
    {
        CullingContentEditor cullingContentEditor;

        [MenuItem("Virtual Training/FreePlay/Culling Editor")]
        public static void GetWindow()
        {
            CullingEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        [MenuItem("Virtual Training/FreePlay/Culling Editor", true)]
        public static bool GetWindowValidate()
        {
            return !Application.isPlaying;
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            if (Application.isPlaying)
                return false;

            CullingEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
                return true;
            }
            return false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            cullingContentEditor = new CullingContentEditor(RecordUndo, () => WindowId);
        }

        protected override TreeViewWithTreeModel<CullingTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<CullingTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<CullingTreeElement>(TreeViewState, treeModel);
        }

        protected override CullingTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new CullingTreeElement("new object", depth, id, false);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void ContentView(List<CullingTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            for (int i = 0; i < selectedElements.Count; i++)
            {
                if (!selectedElements[i].isFolder)
                    cullingContentEditor.Inspector(selectedElements[i].data, i);
            }

            EditorGUILayout.EndVertical();
        }

        protected override void Save()
        {
            // set all mesh collider to default condition
            var allMeshColls = VirtualTrainingSceneManager.GameObjectRoot.transform.GetAllComponentsInChilds<MeshCollider>();

            foreach (var meshColl in allMeshColls)
            {
                if (meshColl != null)
                {
                    meshColl.isTrigger = false;
                    meshColl.convex = false;
                }
            }

            // set cover mesh collider to convex & is trigger
            for (int i = 0; i < ScriptableObjectTemp.GetTreeElementCount(); i++)
            {
                var element = ScriptableObjectTemp.GetTreeElement(i) as CullingTreeElement;
                foreach (var cover in element.data.covers)
                {
                    var meshColls = cover.gameObject.transform.GetAllComponentsInChilds<MeshCollider>();
                    foreach (var meshColl in meshColls)
                    {
                        if (meshColl != null)
                        {
                            meshColl.convex = true;
                            meshColl.isTrigger = true;
                        }
                    }
                }
            }

            // set model scene dirty
            var modelScene = UnityEngine.SceneManagement.SceneManager.GetSceneByName(DatabaseManager.GetSceneConfig(SceneMode.Desktop).mainScenePath);
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(modelScene);

            // save data
            base.Save();
        }
    }
}

#endif
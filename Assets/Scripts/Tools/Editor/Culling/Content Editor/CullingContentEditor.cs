#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class CullingContentEditor : PropertyField
    {
        public CullingContentEditor(Action beforeModifiedCallback, Func<string> windowId) : base(beforeModifiedCallback, windowId)
        {
            CreateListView("objects", null, null, OnObjectCallback, null, null, false, typeof(GameObject));
        }

        private object OnObjectCallback(string listId, object element, int index)
        {
            GameObjectType got = element as GameObjectType;
            ShowSimpleField("object", listId + index, ref got);
            return got;
        }

        public void Inspector(CullingDataModel culling, int index)
        {
            GUILayout.Label("Cover objects");
            ShowListField("objects", culling.covers, "cover" + index);

            GUILayout.Space(10);

            GUILayout.Label("Culled objects");
            ShowListField("objects", culling.culleds, "culled" + index);
        }
    }
}

#endif
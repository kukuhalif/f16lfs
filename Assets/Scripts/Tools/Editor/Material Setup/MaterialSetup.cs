﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.EditorCoroutines.Editor;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class MaterialSetup : EditorWindow
    {
        [MenuItem("Virtual Training/Model/Material Setup")]
        static void OpenWindow()
        {
            var window = GetWindow<MaterialSetup>();
            window.titleContent = new GUIContent("Material Setup");
        }

        [MenuItem("Virtual Training/Material Setup", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        GameObject root;
        Shader opaqueShader;
        Shader transparentShader;
        bool disableShadowTransparentMaterial = true;
        List<string> transparentKeywords = new List<string>();
        PropertyField propertyField;

        private void OnEnable()
        {
            root = VirtualTrainingSceneManager.GameObjectRoot;
            opaqueShader = Shader.Find("CrossSectionGraph/Virtual Training Opaque ShaderGraph");
            transparentShader = Shader.Find("CrossSectionGraph/Virtual Training Transparent ShaderGraph");
            transparentKeywords.Clear();
            transparentKeywords.Add("glass");
            propertyField = new PropertyField(null, "material setup");
            propertyField.CreateListView("transparent material keyword", null, null, TransparentKeywordElementListCallback, null);
            disableShadowTransparentMaterial = true;
        }

        private object TransparentKeywordElementListCallback(string listId, object element, int index)
        {
            string keyword = element as string;
            propertyField.ShowSimpleField("keyword", listId + index, ref keyword);
            return keyword;
        }

        private IEnumerator Setup(GameObject obj)
        {
            EditorUtility.DisplayProgressBar("material setup", "setup starting", 0f);
            int counter = 0;

            Renderer[] renderer = obj.transform.GetAllComponentsInChilds<Renderer>();
            Debug.Log("renderer count : " + renderer.Length);

            for (int i = 0; i < renderer.Length; i++)
            {
                if (renderer[i] != null)
                {
                    Material[] mats = renderer[i].sharedMaterials;
                    for (int m = 0; m < mats.Length; m++)
                    {
                        if (mats[m] != null)
                        {
                            bool isTransparent = false;
                            for (int t = 0; t < transparentKeywords.Count; t++)
                            {
                                if (mats[m].name.ToLower().Contains(transparentKeywords[t].ToLower()))
                                {
                                    isTransparent = true;
                                    break;
                                }
                            }

                            if (isTransparent)
                            {
                                if (mats[m].shader != transparentShader)
                                    mats[m].shader = transparentShader;

                                renderer[i].shadowCastingMode = disableShadowTransparentMaterial ? UnityEngine.Rendering.ShadowCastingMode.Off : UnityEngine.Rendering.ShadowCastingMode.On;
                                renderer[i].receiveShadows = disableShadowTransparentMaterial ? false : true;
                            }
                            else
                            {
                                if (mats[m].shader != opaqueShader)
                                    mats[m].shader = opaqueShader;
                            }
                        }
                    }

                    float progress = (float)i / (float)renderer.Length;
                    EditorUtility.DisplayProgressBar("material setup", "setup " + renderer[i].name, progress);

                    if (counter > 500)
                    {
                        counter = 0;
                        yield return null;
                    }
                    counter++;
                }
            }

            Debug.Log("material setup done");
            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayDialog("material setup", "setup is complete", "ok");
        }

        private void OnGUI()
        {
            GUILayout.Label("untuk parent object dibawah ini masukkan object yang ingin di setup saja / object yg baru dimasukkan, namun jika ingin nge setup semua object masukkan object root");
            root = EditorGUILayout.ObjectField("Parent object", root, typeof(GameObject), true) as GameObject;

            GUILayout.Space(5f);

            GUILayout.Label("masukkan opaque shader yang akan di assign ke materials");
            opaqueShader = EditorGUILayout.ObjectField("Opaque shader", opaqueShader, typeof(Shader), false) as Shader;

            GUILayout.Space(5f);

            GUILayout.Label("masukkan transparent shader yang akan di assign ke materials");
            transparentShader = EditorGUILayout.ObjectField("Transparent shader", transparentShader, typeof(Shader), false) as Shader;

            GUILayout.Space(5f);
            propertyField.ShowSimpleField("Disable shadow transparent material", ref disableShadowTransparentMaterial, 210);

            GUILayout.Space(5f);

            propertyField.ShowListField("transparent material keyword", transparentKeywords);

            GUILayout.Space(10f);

            if (root == null || opaqueShader == null || transparentShader == null)
                EditorGUI.BeginDisabledGroup(true);

            if (GUILayout.Button("Assign shader to materials"))
            {
                EditorCoroutineUtility.StartCoroutine(Setup(root), this);
            }

            if (root == null || opaqueShader == null || transparentShader == null)
                EditorGUI.EndDisabledGroup();
        }
    }
}

#endif

﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Feature;
using UnityEditor.IMGUI.Controls;
using Unity.EditorCoroutines.Editor;
using VirtualTraining.SceneManagement;
using VirtualTraining.Core;
using UnityEditor.SceneManagement;

namespace VirtualTraining.Tools
{
    public class ModelUpdater : EditorWindow
    {
        [MenuItem("Virtual Training/Model/Model Updater &U")]
        static void OpenWindow()
        {
            var window = GetWindow<ModelUpdater>();
            window.titleContent = new GUIContent("Model Updater");
        }

        [MenuItem("Virtual Training/Model/Model Updater &U", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        class ObjectPair
        {
            public GameObject oldObj;
            public GameObject newObj;
            public int oldId;
            public int newId;
            public bool defaultConnection;
            public Color lineColor
            {
                get
                {
                    if (defaultConnection)
                        return Color.green;
                    else return Color.red;
                }
            }

            public ObjectPair(GameObject oldObj, GameObject newObj, bool defaultConnection)
            {
                this.oldObj = oldObj;
                this.newObj = newObj;
                oldId = oldObj.GetInstanceID();
                newId = newObj.GetInstanceID();
                this.defaultConnection = defaultConnection;
            }
        }

        const int MAX_OBJ_PER_FRAME = 500;

        MultiColumnHeaderState oldHeaderState, newHeaderState;
        TreeViewState oldTreeViewState, newTreeViewState;
        ModelUpdaterTreeView oldTreeView, newTreeView;
        GameObject oldObj, newObj, instantiatedNewObjectParent;
        List<ObjectPair> objectPairs = new List<ObjectPair>();
        List<int> availableOldTree = new List<int>();
        List<int> availableNewTree = new List<int>();
        bool showTree = true;
        bool showLine = true;
        bool onlyShowNotFound = false;
        bool isSearching;
        bool isSearchOld;
        bool copyTransform = true;
        //bool onlyReplacePairedObjects = false; not available for now
        int searchId;
        Rect searchRectDebug;
        float treeViewVerticalPos = 170f;
        float lineVerticalOffset = 30f;

        SceneMode modelSceneMode = SceneMode.Desktop;

        bool isWorking;

        private void OnEnable()
        {
            isWorking = false;
            ReloadTree();
            modelSceneMode = DatabaseManager.GetStartingSceneConfig().mode;
        }

        private MultiColumnHeaderState NewMultiHeaderState()
        {
            MultiColumnHeaderState.Column column1 = new MultiColumnHeaderState.Column();

            column1.allowToggleVisibility = false;
            column1.autoResize = true;
            column1.canSort = false;
            column1.headerContent = new GUIContent("object");
            column1.width = position.width / 4f;

            MultiColumnHeaderState.Column column2 = new MultiColumnHeaderState.Column();

            column2.allowToggleVisibility = false;
            column2.autoResize = true;
            column2.canSort = false;
            column2.headerContent = new GUIContent("action");
            column2.width = position.width / 4f;

            MultiColumnHeaderState newState = new MultiColumnHeaderState(new MultiColumnHeaderState.Column[] { column1, column2 });
            return newState;
        }

        private void ReloadTree()
        {
            oldTreeViewState = new TreeViewState();
            var oldTreeModel = new TreeModel<ModelTreeElement>(CreateNewTreeModel());
            oldHeaderState = NewMultiHeaderState();
            var oldMultiHeader = new MultiColumnHeader(oldHeaderState);
            oldTreeView = new ModelUpdaterTreeView(true, onlyShowNotFound, oldTreeViewState, oldTreeModel, oldMultiHeader, oldObj, OldTreeDebugPair, DragCallback, ContextClickedCallback, IsHasPair);
            oldTreeView.Reload();
            oldTreeView.SetFirstBuildRow(false);

            newTreeViewState = new TreeViewState();
            var newTreeModel = new TreeModel<ModelTreeElement>(CreateNewTreeModel());
            newHeaderState = NewMultiHeaderState();
            var newMultiHeader = new MultiColumnHeader(newHeaderState);
            newTreeView = new ModelUpdaterTreeView(false, onlyShowNotFound, newTreeViewState, newTreeModel, newMultiHeader, newObj, NewTreeDebugPair, DragCallback, ContextClickedCallback, IsHasPair);
            newTreeView.Reload();
            newTreeView.SetFirstBuildRow(false);

            availableOldTree.Clear();
            availableNewTree.Clear();

            if (oldObj != null && newObj != null)
            {
                SearchObjectPair();
                SearchAvailableObject(true, oldObj);
                SearchAvailableObject(false, newObj);
            }
        }

        private bool IsHasPair(int id, bool old)
        {
            foreach (var item in objectPairs)
            {
                if (old && id == item.oldId)
                    return true;

                if (!old && id == item.newId)
                    return true;
            }
            return false;
        }

        private bool IsPaired(bool old, int id)
        {
            bool found = false;
            foreach (var pair in objectPairs)
            {
                if (old && pair.oldId == id)
                {
                    found = true;
                    break;
                }
                if (!old && pair.newId == id)
                {
                    found = true;
                    break;
                }
            }

            return found;
        }

        private void ContextClickedCallback(bool old, int id)
        {
            if (oldObj != null && newObj != null)
            {
                if (!IsPaired(old, id))
                    return;

                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("detach connection"), false, () =>
                 {
                     int removeIndex = -1;
                     for (int i = 0; i < objectPairs.Count; i++)
                     {
                         if (old && objectPairs[i].oldId == id)
                         {
                             removeIndex = i;
                             break;
                         }
                         if (!old && objectPairs[i].newId == id)
                         {
                             removeIndex = i;
                             break;
                         }
                     }

                     if (removeIndex != -1)
                     {
                         var pair = objectPairs[removeIndex];
                         availableOldTree.Add(pair.oldId);
                         availableNewTree.Add(pair.newId);
                         objectPairs.RemoveAt(removeIndex);
                     }
                 });

                bool searchFromParent = false;
                GameObject oldObj = null, newObj = null;

                foreach (var pair in objectPairs)
                {
                    if (old && pair.oldId == id)
                    {
                        searchFromParent = true;
                        oldObj = pair.oldObj;
                        newObj = pair.newObj;
                        break;
                    }

                    if (!old && pair.newId == id)
                    {
                        searchFromParent = true;
                        oldObj = pair.oldObj;
                        newObj = pair.newObj;
                        break;
                    }
                }

                if (searchFromParent)
                {
                    menu.AddItem(new GUIContent("search child pair from this parent"), false, () =>
                    {
                        SearchPairFromParent(oldObj, newObj);
                    });
                }

                menu.ShowAsContext();
            }
        }

        private void DragCallback(bool old, int id)
        {
            if (oldObj != null && newObj != null)
            {
                if (IsPaired(old, id))
                    return;

                if (old)
                    searchRectDebug = oldTreeView.GetCellRect(id);
                else
                    searchRectDebug = newTreeView.GetCellRect(id);

                if (old)
                    searchRectDebug.position += new Vector2(10, treeViewVerticalPos + lineVerticalOffset);
                else
                    searchRectDebug.position += new Vector2(position.width / 2f, treeViewVerticalPos + lineVerticalOffset);

                searchRectDebug.width = 10f;

                StartSearchPair(old, id);
                DragAndDrop.StartDrag("search pair");
            }
        }

        private List<Rect> AvailableRects()
        {
            List<int> ids = isSearchOld ? availableNewTree : availableOldTree;
            List<Rect> rects = new List<Rect>();

            for (int i = 0; i < ids.Count; i++)
            {
                Rect cellRect = new Rect();
                if (isSearchOld)
                    cellRect = newTreeView.GetCellRect(ids[i]);
                else
                    cellRect = oldTreeView.GetCellRect(ids[i]);

                if (isSearchOld)
                    cellRect.position += new Vector2(position.width / 2f, treeViewVerticalPos + lineVerticalOffset);
                else
                    cellRect.position += new Vector2(10, treeViewVerticalPos + lineVerticalOffset);

                cellRect.width -= 5f;

                rects.Add(cellRect);
            }

            return rects;
        }

        private void StartSearchPair(bool old, int id)
        {
            isSearching = true;
            isSearchOld = old;
            searchId = id;
        }

        private string OldTreeDebugPair(int id)
        {
            for (int i = 0; i < objectPairs.Count; i++)
            {
                if (id == objectPairs[i].oldId)
                {
                    if (objectPairs[i].oldObj != null)
                        return "will be replaced";

                    return "";
                }
            }
            return "will be deleted";
        }

        private string NewTreeDebugPair(int id)
        {
            for (int i = 0; i < objectPairs.Count; i++)
            {
                if (id == objectPairs[i].newId)
                {
                    if (objectPairs[i].newObj != null)
                        return "will replace";

                    return "";
                }
            }
            return "will be added";
        }

        private ModelTreeElement CreateHiddenRoot()
        {
            var root = new ModelTreeElement();
            root.depth = -1;
            root.name = "Root";
            root.id = 0;
            return root;
        }

        private IList<ModelTreeElement> CreateNewTreeModel()
        {
            var data = new List<ModelTreeElement>();
            data.Add(CreateHiddenRoot());
            return data;
        }

        private void SearchAvailableObject(bool old, GameObject obj)
        {
            bool found = false;
            for (int i = 0; i < objectPairs.Count; i++)
            {
                if (old && objectPairs[i].oldObj == obj)
                {
                    found = true;
                    break;
                }
                if (!old && objectPairs[i].newObj == obj)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                if (old)
                    availableOldTree.Add(obj.GetInstanceID());
                else
                    availableNewTree.Add(obj.GetInstanceID());
            }

            for (int i = 0; i < obj.transform.childCount; i++)
            {
                SearchAvailableObject(old, obj.transform.GetChild(i).gameObject);
            }
        }

        private void SearchObjectPair()
        {
            Debug.Log("start search object pair");
            objectPairs.Clear();
            SearchObjectPair(newObj);
            Debug.Log("total pair is " + objectPairs.Count);
        }

        private void SearchObjectPair(GameObject newObj)
        {
            GameObject oldObj = FindPair(newObj);
            if (oldObj != null)
            {
                bool oldFound = false;
                foreach (var pair in objectPairs)
                {
                    if (pair.oldObj == oldObj)
                    {
                        oldFound = true;
                        break;
                    }
                }
                bool newFound = false;
                foreach (var pair in objectPairs)
                {
                    if (pair.newObj == newObj)
                    {
                        newFound = true;
                        break;
                    }
                }
                if (!oldFound && !newFound)
                    objectPairs.Add(new ObjectPair(oldObj, newObj, true));
            }

            for (int i = 0; i < newObj.transform.childCount; i++)
            {
                SearchObjectPair(newObj.transform.GetChild(i).gameObject);
            }
        }

        private void SearchGameobject(GameObject obj, int id, ref GameObject result)
        {
            if (obj.GetInstanceID() == id)
                result = obj;

            if (result == null)
                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    SearchGameobject(obj.transform.GetChild(i).gameObject, id, ref result);
                }
        }

        private void SearchGameobject(GameObject obj, GameObject reference, ref GameObject result, Transform objRoot, Transform referenceRoot)
        {
            if (IsPairing(obj, reference, objRoot, referenceRoot, false))
                result = obj;

            if (result == null)
                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    SearchGameobject(obj.transform.GetChild(i).gameObject, reference, ref result, objRoot, referenceRoot);
                }
        }

        private GameObject FindPair(GameObject newObj)
        {
            GameObject pair = null;
            FindPair(newObj, oldObj, ref pair);
            return pair;
        }

        private void FindPair(GameObject newObj, GameObject oldObj, ref GameObject pair)
        {
            if (IsPairing(newObj, oldObj, this.newObj.transform, this.oldObj.transform, false))
            {
                pair = oldObj;
            }

            if (pair == null)
            {
                for (int i = 0; i < oldObj.transform.childCount; i++)
                {
                    FindPair(newObj, oldObj.transform.GetChild(i).gameObject, ref pair);
                }
            }
        }

        private bool IsPairing(GameObject a, GameObject b, Transform aRoot, Transform bRoot, bool removeRootFromComparison)
        {
            if (a.name != b.name)
                return false;

            var aAncestors = a.transform.GetAllParents(aRoot);
            var bAncestors = b.transform.GetAllParents(bRoot);

            if (removeRootFromComparison)
            {
                aAncestors.Remove(aRoot);
                bAncestors.Remove(bRoot);
            }

            if (aAncestors.Count != bAncestors.Count)
                return false;

            for (int i = 0; i < aAncestors.Count; i++)
            {
                if (aAncestors[i].name != bAncestors[i].name)
                    return false;
            }

            return true;
        }

        private IEnumerator ReplaceObject()
        {
            Debug.Log("start replace object");
            Debug.Log("instantiate new object");
            EditorUtility.DisplayProgressBar("model updater", "instantiate new object", 0f);

            string newObjName = newObj.name;
            Transform parent = oldObj.transform.parent;
            instantiatedNewObjectParent = Instantiate(newObj);
            instantiatedNewObjectParent.transform.SetParent(parent, true);

            instantiatedNewObjectParent.name = newObjName;

            if (copyTransform)
            {
                instantiatedNewObjectParent.transform.position = oldObj.transform.position;
                instantiatedNewObjectParent.transform.localPosition = oldObj.transform.localPosition;
                instantiatedNewObjectParent.transform.rotation = oldObj.transform.rotation;
                instantiatedNewObjectParent.transform.localRotation = oldObj.transform.localRotation;
                instantiatedNewObjectParent.transform.localScale = oldObj.transform.localScale;
            }

            Debug.Log("start looping for " + objectPairs.Count + " pairs");

            int counter = 0;

            for (int i = 0; i < objectPairs.Count; i++)
            {
                GameObject instantiatedNewObjectPair = null;
                SearchGameobject(instantiatedNewObjectParent, objectPairs[i].newObj, ref instantiatedNewObjectPair, instantiatedNewObjectParent.transform, newObj.transform);

                if (copyTransform)
                {
                    instantiatedNewObjectPair.transform.position = objectPairs[i].oldObj.transform.position;
                    instantiatedNewObjectPair.transform.localPosition = objectPairs[i].oldObj.transform.localPosition;
                    instantiatedNewObjectPair.transform.rotation = objectPairs[i].oldObj.transform.rotation;
                    instantiatedNewObjectPair.transform.localRotation = objectPairs[i].oldObj.transform.localRotation;
                    instantiatedNewObjectPair.transform.localScale = objectPairs[i].oldObj.transform.localScale;
                }

                foreach (var component in objectPairs[i].oldObj.GetComponents<Component>())
                {
                    if (component != null)
                    {
                        float progress = (float)i / (float)objectPairs.Count;

                        if (instantiatedNewObjectPair != null)
                        {
                            EditorUtility.DisplayProgressBar("model updater", "copy " + component.name, progress);

                            UnityEditorInternal.ComponentUtility.CopyComponent(component);
                            var temps = instantiatedNewObjectPair.GetComponents<Component>();

                            bool alreadyExist = false;

                            foreach (var temp in temps)
                            {
                                Type a = temp.GetType();
                                Type b = component.GetType();
                                if (a == b)
                                {
                                    UnityEditorInternal.ComponentUtility.PasteComponentValues(component);
                                    alreadyExist = true;
                                }
                            }

                            if (!alreadyExist)
                                UnityEditorInternal.ComponentUtility.PasteComponentAsNew(instantiatedNewObjectPair);
                        }
                    }
                    else
                        Debug.LogError(objectPairs[i].newObj.name + " is null");

                    if (counter > MAX_OBJ_PER_FRAME)
                    {
                        counter = 0;
                        yield return null;
                    }
                    counter++;
                }
            }

            Debug.Log("destroy old object");
            EditorUtility.DisplayProgressBar("model updater", "destroy old object", 1f);

            DestroyImmediate(oldObj);

            EditorUtility.DisplayProgressBar("model updater", "bone joint connection setup", 0.95f);
            Debug.Log("bone joint connection setup");
            BoneJointData[] boneJointDatas = VirtualTrainingSceneManager.GameObjectRoot.transform.GetAllComponentsInChilds<BoneJointData>();
            for (int i = 0; i < boneJointDatas.Length; i++)
            {
                if (boneJointDatas[i] != null)
                {
                    boneJointDatas[i].BoneJointConnectionSetupFromModelUpdater();
                }
            }

            EditorUtility.DisplayProgressBar("model updater", "set scene dirty", 1f);
            Debug.Log("set scene dirty");
            VirtualTrainingEditorUtility.SetModelSceneDirty(modelSceneMode);
            EditorUtility.ClearProgressBar();

            Debug.Log("start model setup for " + instantiatedNewObjectParent.name);
            ModelSetup.StartSetup(instantiatedNewObjectParent, this, DoneUpdateAction, modelSceneMode);
        }

        private void DoneUpdateAction()
        {
            isWorking = false;

            newObj = oldObj = null;
            objectPairs.Clear();
            availableNewTree.Clear();
            availableOldTree.Clear();

            ReloadTree();
        }

        private Rect OldTreeViewRect(float treeViewWidth)
        {
            return new Rect(20, treeViewVerticalPos, treeViewWidth, position.height - treeViewVerticalPos - 10f);
        }

        private Rect NewTreeViewRect(float treeViewWidth)
        {
            return new Rect(position.width / 2f, treeViewVerticalPos, treeViewWidth, position.height - treeViewVerticalPos - 10f);
        }

        private void SearchPairFromParent(GameObject oldParent, GameObject newParent)
        {
            Transform[] oldChilds = oldParent.transform.GetAllChilds();
            Transform[] newChilds = newParent.transform.GetAllChilds();

            foreach (var oldChild in oldChilds)
            {
                foreach (var newChild in newChilds)
                {
                    if (oldChild.name == newChild.name)
                    {
                        bool isPairing = IsPairing(oldChild.gameObject, newChild.gameObject, oldParent.transform, newParent.transform, true);

                        if (isPairing)
                        {
                            bool oldFound = false;
                            foreach (var pair in objectPairs)
                            {
                                if (pair.oldObj == oldChild.gameObject)
                                {
                                    oldFound = true;
                                    break;
                                }
                            }
                            bool newFound = false;
                            foreach (var pair in objectPairs)
                            {
                                if (pair.newObj == newChild.gameObject)
                                {
                                    newFound = true;
                                    break;
                                }
                            }

                            if (!oldFound && !newFound)
                            {
                                objectPairs.Add(new ObjectPair(oldChild.gameObject, newChild.gameObject, false));
                                availableOldTree.Remove(oldChild.gameObject.GetInstanceID());
                                availableNewTree.Remove(newChild.gameObject.GetInstanceID());
                            }
                        }
                    }
                }
            }
        }

        private void SearchNotFoundElementByName()
        {
            // get childs and itself
            List<Transform> oldElements = new List<Transform>(oldObj.transform.GetAllChilds());
            oldElements.Insert(0, oldObj.transform);
            List<Transform> newElements = new List<Transform>(newObj.transform.GetAllChilds());
            newElements.Insert(0, newObj.transform);

            // get used element
            List<GameObject> usedOldObjs = new List<GameObject>();
            List<GameObject> usedNewObjs = new List<GameObject>();
            foreach (var pair in objectPairs)
            {
                usedOldObjs.Add(pair.oldObj);
                usedNewObjs.Add(pair.newObj);
            }

            // search not found old element
            foreach (var oldElement in oldElements)
            {
                bool found = false;
                foreach (var pair in objectPairs)
                {
                    if (oldElement.gameObject.GetInstanceID() == pair.oldId)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    foreach (var newElement in newElements)
                    {
                        if (oldElement.gameObject.name == newElement.gameObject.name && !usedNewObjs.Contains(newElement.gameObject))
                        {
                            ObjectPair pair = new ObjectPair(oldElement.gameObject, newElement.gameObject, false);
                            objectPairs.Add(pair);
                            availableOldTree.Remove(pair.oldId);
                            availableNewTree.Remove(pair.newId);
                            usedNewObjs.Add(newElement.gameObject);
                            break;
                        }
                    }
                }
            }

            // search not found new element
            foreach (var newElement in newElements)
            {
                bool found = false;
                foreach (var pair in objectPairs)
                {
                    if (newElement.gameObject.GetInstanceID() == pair.newId)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    foreach (var oldElement in oldElements)
                    {
                        if (oldElement.gameObject.name == newElement.gameObject.name && !usedOldObjs.Contains(oldElement.gameObject))
                        {
                            ObjectPair pair = new ObjectPair(oldElement.gameObject, newElement.gameObject, false);
                            objectPairs.Add(pair);
                            availableOldTree.Remove(pair.oldId);
                            availableNewTree.Remove(pair.newId);
                            usedOldObjs.Add(oldElement.gameObject);
                            break;
                        }
                    }
                }
            }
        }

        private void LabelAndSearch(Rect oldTreeRect, Rect newTreeRect)
        {
            Rect oldLabelRect = new Rect(10, treeViewVerticalPos - 20, 100, 20);
            GUI.Label(oldLabelRect, "Old Object");
            Rect oldSearchRect = oldLabelRect;
            oldSearchRect.x += 100;
            oldSearchRect.width = oldTreeRect.width - 100;
            oldTreeView.Search(oldSearchRect);

            Rect newLabelRect = new Rect(position.width / 2f, treeViewVerticalPos - 20, 100, 20);
            GUI.Label(newLabelRect, "New Object");
            Rect newSearchRect = newLabelRect;
            newSearchRect.x += 100f;
            newSearchRect.width = newTreeRect.width - 100;
            newTreeView.Search(newSearchRect);
        }

        private void OnGUI()
        {
            if (isWorking)
            {
                GUILayout.Label("now loading ...");
                return;
            }

            if (!VirtualTrainingEditorUtility.LoadMainSceneWarningField(ref modelSceneMode, position.width))
            {
                return;
            }

            GameObject newOld = (GameObject)EditorGUILayout.ObjectField("old object", oldObj, typeof(GameObject), true);
            GameObject newNew = (GameObject)EditorGUILayout.ObjectField("new object", newObj, typeof(GameObject), true);

            if (newOld != oldObj || newNew != newObj)
            {
                oldObj = newOld;
                newObj = newNew;
                ReloadTree();
            }

            if (oldObj == null || newObj == null)
                EditorGUI.BeginDisabledGroup(true);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("reset connection"))
            {
                if (EditorUtility.DisplayDialog("replace object", "reset connection ?", "yes", "no"))
                {
                    ReloadTree();
                }
            }

            if (GUILayout.Button("search not found element by name"))
            {
                if (EditorUtility.DisplayDialog("replace object", "search not found element by name ?", "yes", "no"))
                {
                    SearchNotFoundElementByName();
                }
            }

            if (GUILayout.Button("search child connection from custom connection (red)"))
            {
                if (EditorUtility.DisplayDialog("replace object", "search child connection ?", "yes", "no"))
                {
                    List<ObjectPair> nonDefaultPairs = new List<ObjectPair>();
                    foreach (var pair in objectPairs)
                    {
                        if (!pair.defaultConnection)
                            nonDefaultPairs.Add(pair);
                    }
                    foreach (var pair in nonDefaultPairs)
                    {
                        SearchPairFromParent(pair.oldObj, pair.newObj);
                    }
                }
            }

            if (GUILayout.Button("replace object"))
            {
                if (EditorUtility.DisplayDialog("replace object", "replace old object with new object ?", "yes", "no"))
                {
                    isWorking = true;
                    EditorCoroutineUtility.StartCoroutine(ReplaceObject(), this);
                }
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            showTree = GUILayout.Toggle(showTree, "show tree");
            GUILayout.FlexibleSpace();
            float originalValue = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 305f;
            copyTransform = EditorGUILayout.Toggle("copy position, rotation and scale from old gameobject", copyTransform);
            EditorGUIUtility.labelWidth = originalValue;
            GUILayout.EndHorizontal();

            //GUILayout.BeginHorizontal();
            showLine = GUILayout.Toggle(showLine, "show pair line");
            //GUILayout.FlexibleSpace();
            //originalValue = EditorGUIUtility.labelWidth;
            //EditorGUIUtility.labelWidth = 305f;
            //onlyReplacePairedObjects = EditorGUILayout.Toggle("only replace paired objects ", onlyReplacePairedObjects);
            //EditorGUIUtility.labelWidth = originalValue;
            //GUILayout.EndHorizontal();

            bool only = GUILayout.Toggle(onlyShowNotFound, "only show not found");
            if (only != onlyShowNotFound)
            {
                onlyShowNotFound = only;
                oldTreeView.OnlyShowNotFound(onlyShowNotFound);
                newTreeView.OnlyShowNotFound(onlyShowNotFound);
                oldTreeView.Reload();
                newTreeView.Reload();
            }

            float treeViewWidth = position.width / 2f - 20f;

            if (oldObj == null || newObj == null)
                EditorGUI.EndDisabledGroup();

            Rect oldTreeRect = OldTreeViewRect(treeViewWidth);
            Rect newTreeRect = NewTreeViewRect(treeViewWidth);

            LabelAndSearch(oldTreeRect, newTreeRect);

            if (showTree)
            {
                if (oldTreeView != null)
                {
                    Rect sliderRect = oldTreeRect;
                    sliderRect.x = 5;
                    sliderRect.width = 10;

                    int bottom = oldTreeView.GetRows().Count * (int)EditorGUIUtility.singleLineHeight - (int)oldTreeRect.height;
                    oldTreeView.VerticalPosition = GUI.VerticalSlider(sliderRect, oldTreeView.VerticalPosition, 0, bottom);

                    GUILayout.BeginArea(oldTreeRect);

                    oldTreeRect.height = oldTreeView.totalHeight + EditorGUIUtility.singleLineHeight;
                    oldTreeRect.x = 0;
                    oldTreeRect.y = -oldTreeView.VerticalPosition;

                    oldTreeView.OnGUI(oldTreeRect);

                    GUILayout.EndArea();
                }

                if (newTreeView != null)
                {
                    Rect sliderRect = newTreeRect;
                    sliderRect.x = position.width - 15f;
                    sliderRect.width = 10;

                    int bottom = newTreeView.GetRows().Count * (int)EditorGUIUtility.singleLineHeight - (int)newTreeRect.height;
                    newTreeView.VerticalPosition = GUI.VerticalSlider(sliderRect, newTreeView.VerticalPosition, 0, bottom);

                    GUILayout.BeginArea(newTreeRect);

                    newTreeRect.height = newTreeView.totalHeight + EditorGUIUtility.singleLineHeight;
                    newTreeRect.y = -newTreeView.VerticalPosition;
                    newTreeRect.x = 0;

                    newTreeView.OnGUI(newTreeRect);

                    GUILayout.EndArea();
                }
            }

            if (isSearching)
            {
                if (Event.current.type == EventType.DragExited)
                    isSearching = false;

                EditorGUI.DrawRect(searchRectDebug, Color.red);

                var mousePos = Event.current.mousePosition;
                var availableRects = AvailableRects();
                for (int i = 0; i < availableRects.Count; i++)
                {
                    if (availableRects[i].Contains(mousePos))
                    {
                        Rect cell = availableRects[i];
                        cell.width = 10f;
                        EditorGUI.DrawRect(cell, Color.green);

                        if (Event.current.type == EventType.DragExited)
                        {
                            GameObject oldObj = null;
                            GameObject newObj = null;

                            if (isSearchOld)
                            {
                                SearchGameobject(this.oldObj, searchId, ref oldObj);
                                SearchGameobject(this.newObj, availableNewTree[i], ref newObj);
                            }
                            else
                            {
                                SearchGameobject(this.oldObj, availableOldTree[i], ref oldObj);
                                SearchGameobject(this.newObj, searchId, ref newObj);
                            }

                            objectPairs.Add(new ObjectPair(oldObj, newObj, false));
                            availableOldTree.Remove(oldObj.GetInstanceID());
                            availableNewTree.Remove(newObj.GetInstanceID());

                            isSearching = false;

                            break;
                        }
                    }
                    else
                    {
                        Rect cell = availableRects[i];
                        cell.width = 10f;
                        EditorGUI.DrawRect(cell, Color.cyan);
                    }
                }
            }

            if (showLine)
            {
                Handles.BeginGUI();

                Rect oldTreeLineRect = OldTreeViewRect(treeViewWidth);
                Rect newTreeLineRect = NewTreeViewRect(treeViewWidth);

                foreach (var pair in objectPairs)
                {
                    var oldElement = oldTreeView.GetLineRect(pair.oldId);
                    var newElement = newTreeView.GetLineRect(pair.newId);

                    oldElement.position += new Vector2(15, treeViewVerticalPos + lineVerticalOffset);
                    newElement.position += new Vector2((position.width / 2f) + 5, treeViewVerticalPos + lineVerticalOffset);

                    var oldRows = oldTreeView.GetRows();
                    var newRows = newTreeView.GetRows();

                    bool oldFound = false;
                    bool newFound = false;

                    foreach (var oldItem in oldRows)
                    {
                        if (oldItem.id == pair.oldId)
                        {
                            oldFound = true;
                            break;
                        }
                    }

                    foreach (var newItem in newRows)
                    {
                        if (newItem.id == pair.newId)
                        {
                            newFound = true;
                            break;
                        }
                    }

                    if (oldFound && newFound)
                    {
                        Handles.color = pair.lineColor;

                        Vector2 oldPos = oldElement.position;
                        Vector2 newPos = newElement.position;

                        if (oldTreeLineRect.Contains(oldPos) && newTreeLineRect.Contains(newPos))
                        {
                            Handles.DrawLine(oldPos, newPos);
                        }
                        else
                        {
                            if (oldTreeLineRect.Contains(oldElement.position))
                            {
                                if (newPos.y < treeViewVerticalPos)
                                {
                                    Handles.DrawLine(oldPos, new Vector2(position.width / 2f, treeViewVerticalPos));
                                }
                                else if (newPos.y > newTreeLineRect.height + treeViewVerticalPos)
                                {
                                    Handles.DrawLine(oldPos, new Vector2(position.width / 2f, newTreeLineRect.height + treeViewVerticalPos));
                                }
                            }
                            else if (newTreeLineRect.Contains(newElement.position))
                            {
                                if (oldPos.y < treeViewVerticalPos)
                                {
                                    Handles.DrawLine(new Vector2(position.width / 2f, treeViewVerticalPos), newPos);
                                }
                                else if (oldPos.y > newTreeLineRect.height + treeViewVerticalPos)
                                {
                                    Handles.DrawLine(new Vector2(position.width / 2f, newTreeLineRect.height + treeViewVerticalPos), newPos);
                                }
                            }
                        }
                    }
                }

                if (isSearching)
                {
                    Rect searchRect = new Rect();
                    if (isSearchOld)
                    {
                        searchRect = oldTreeView.GetLineRect(searchId);
                        searchRect.position += new Vector2(10, treeViewVerticalPos + lineVerticalOffset);
                    }
                    else
                    {
                        searchRect = newTreeView.GetLineRect(searchId);
                        searchRect.position += new Vector2(position.width / 2f, treeViewVerticalPos + lineVerticalOffset);
                    }

                    Handles.color = Color.red;
                    Handles.DrawLine(searchRect.position, Event.current.mousePosition);
                }

                Handles.EndGUI();
            }

            Repaint();
        }
    }
}

#endif
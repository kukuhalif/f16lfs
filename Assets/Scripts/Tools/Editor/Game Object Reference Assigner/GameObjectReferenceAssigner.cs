#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class GameObjectReferenceAssigner : EditorWindow
    {
        [MenuItem("Virtual Training/Database Tools/GameObject Reference Assigner")]
        static void OpenWindow()
        {
            var window = GetWindow<GameObjectReferenceAssigner>();
            window.titleContent = new GUIContent("Reference Assigner");
        }

        [MenuItem("Virtual Training/Database Tools/GameObject Reference Assigner", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        SceneMode modelSceneMode = SceneMode.Desktop;
        PropertyField propertyField;

        class GameobjectData
        {
            public GameObject gameObject;
            public bool recursive;

            public GameobjectData()
            {
                this.gameObject = null;
                this.recursive = true;
            }

            public GameobjectData(object gameObject)
            {
                GameObject go = gameObject as GameObject;
                if (go != null)
                    this.gameObject = go;

                this.recursive = true;
            }
        }

        List<GameobjectData> gameobjectDatas = new List<GameobjectData>();
        List<GameObject> roots = new List<GameObject>();

        private void OnEnable()
        {
            propertyField = new PropertyField(null, "Reference Assigner");
            propertyField.CreateListView("gameobject", null, null, OnModelElementDrawCallback, null, null, false, typeof(GameObject));
            modelSceneMode = DatabaseManager.GetStartingSceneConfig().mode;
        }

        private object OnModelElementDrawCallback(string listId, object element, int index)
        {
            GameobjectData gd = element as GameobjectData;

            GUILayout.BeginHorizontal();
            propertyField.ShowSimpleField("gameobject", listId + index + "a", ref gd.gameObject);
            GUILayout.Space(20);
            propertyField.ShowSimpleField("recursive", listId + index + "b", ref gd.recursive, 70, 80);
            GUILayout.EndHorizontal();

            return gd;
        }

        private void OnGUI()
        {
            if (!VirtualTrainingEditorUtility.LoadMainSceneWarningField(ref modelSceneMode, position.width))
            {
                return;
            }

            GUILayout.Label("masukkan gameobject yang ingin ditambahkan komponen gameobject reference");
            propertyField.ShowListField("gameobject", gameobjectDatas);

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("assign gameobject reference"))
            {
                if (EditorUtility.DisplayDialog("Reference Assigner", "assign gameobject reference component to gameobjects ?", "ok", "cancel"))
                {
                    roots.Clear();

                    foreach (var gd in gameobjectDatas)
                    {
                        SetId(gd.gameObject);

                        if (gd.recursive)
                        {
                            var children = gd.gameObject.GetAllChilds();
                            foreach (var child in children)
                            {
                                SetId(child);
                            }
                        }
                    }

                    RootSetup();

                    VirtualTrainingEditorUtility.SetModelSceneDirty(modelSceneMode);
                }
            }
        }

        private void SetId(GameObject obj)
        {
            // 1. Gameobject reference
            GameObjectReference gameObjectReference = obj.GetComponent<GameObjectReference>();
            if (gameObjectReference == null)
                gameObjectReference = obj.AddComponent<GameObjectReference>();

            // if id is empty then set id
            if (gameObjectReference.Id == 0)
            {
                int instanceId = obj.transform.GetInstanceID();
                int combinedId = Animator.StringToHash(instanceId.ToString() + obj.name);
                gameObjectReference.SetId(combinedId);
            }

            var root = obj.transform.root;
            if (!roots.Contains(root.gameObject))
                roots.Add(root.gameObject);
        }
        private void RootSetup()
        {
            string rootNames = "";
            for (int i = 0; i < roots.Count; i++)
            {
                GameObjectReferenceInitializer gameObjectReference = roots[i].GetComponent<GameObjectReferenceInitializer>();
                if (gameObjectReference == null)
                    gameObjectReference = roots[i].AddComponent<GameObjectReferenceInitializer>();

                rootNames += roots[i].name;
                if (i < roots.Count - 2)
                {
                    rootNames += roots[i].name + " ,";
                }
                else if (i < roots.Count - 1)
                {
                    rootNames += roots[i].name + " and ";
                }
            }

            if (EditorUtility.DisplayDialog("Reference Assigner", "GameObjectReferenceInitializer component added to [ " + rootNames + " ]", "ok")) { }
        }
    }
}

#endif
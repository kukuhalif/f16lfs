#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using Paroxe.PdfRenderer;

namespace VirtualTraining.Tools
{
    public class MateriEditorWindow : TreeViewEditorBase<MateriTreeAsset, MateriEditorWindow, MateriData, MateriTreeElement>
    {
        MateriContentEditor materiContentEditor;

        [MenuItem("Virtual Training/Materi/Materi Editor &m")]
        public static void GetWindow()
        {
            MateriEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            MateriEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
                return true;
            }
            return false;
        }

        public static MateriEditorWindow GetWindow(int instanceId, bool newWindow = false, bool alwaysInitialize = true)
        {
            MateriEditorWindow window = OpenEditorWindow(instanceId, newWindow);
            if (window != null)
            {
                if (alwaysInitialize || window.TreeView == null)
                {
                    window.Initialize();
                }
                return window;
            }
            return null;
        }

        private bool showMiddleToolbar = false;

        protected override TreeViewWithTreeModel<MateriTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<MateriTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<MateriTreeElement>(TreeViewState, treeModel, IsVR);
        }

        private bool IsVR(MateriTreeElement element)
        {
            if (element.MateriData.deviceMode == DeviceMode.VrOnly)
                return true;

            return false;
        }

        protected override MateriTreeElement NewTreeElement(int depth, int id, int command)
        {
            switch ((MateriElementType)command)
            {
                case MateriElementType.Materi:

                    return new MateriTreeElement("new materi", depth, id, MateriElementType.Materi, false);

                case MateriElementType.Maintenance:

                    return new MateriTreeElement("new maintenance", depth, id, MateriElementType.Maintenance, false);

                case MateriElementType.Troubleshoot:

                    return new MateriTreeElement("new troubleshoot", depth, id, MateriElementType.Troubleshoot, false);

                case MateriElementType.FlightScenario:

                    return new MateriTreeElement("new flight scenario", depth, id, MateriElementType.FlightScenario, false);

                case MateriElementType.RemoveInstall:

                    return new MateriTreeElement("new remove install", depth, id, MateriElementType.RemoveInstall, false);

                default:

                    return new MateriTreeElement("new element", depth, id, MateriElementType.Materi, false);
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            List<PDFAsset> pdfAssets = new List<PDFAsset>();
            var configDatas = DatabaseManager.GetMateriConfigDatas();
            foreach (var configData in configDatas)
            {
                if (configData.materi == ScriptableObjectOriginalFile)
                {
                    pdfAssets = configData.pdfs;
                    break;
                }
            }
            var xrayMats = DatabaseManager.GetXrayMaterials();

            materiContentEditor = new MateriContentEditor(RecordUndo, () => WindowId, pdfAssets, xrayMats);
            TreeView.SetCustomContextMenuInsideElement(InsideElementContextMenuItem());
            TreeView.SetCustomContextMenuOutsideElement(OutsideElementContextMenuItem());
        }

        protected override void OnDisable()
        {
            if (materiContentEditor != null)
                materiContentEditor.SaveCameraDestination();

            base.OnDisable();
        }

        protected override bool UseTopLeftButton()
        {
            return true;
        }

        protected override bool UseAfterSearchbar()
        {
            return true;
        }

        protected override void AfterSearchbarAction(Rect rect)
        {
            bool pressed = GUI.Button(rect, TreeView.specialSearchActive ? "all item" : "vr only");

            PropertyField.DrawTooltip(rect, "show only vr materi & tree is flattened when searching", 320f, new Vector2(-320f + rect.width, rect.height / 2f));

            if (pressed)
            {
                TreeView.specialSearchActive = !TreeView.specialSearchActive;
                TreeView.Reload();
            }
        }

        private void OpenDatabase(int elementId)
        {
            var databases = DatabaseManager.GetMateriAssets();
            for (int d = 0; d < databases.Count; d++)
            {
                var elements = databases[d].GetData().treeElements;
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elementId == elements[i].id)
                    {
                        if (InstanceId != databases[d].GetInstanceID())
                        {
                            Close();
                            var window = GetWindow(databases[d].GetInstanceID());
                            window.TreeView.SetSelection(new List<int> { elementId });
                        }
                        else
                        {
                            TreeView.SetSelection(new List<int> { elementId });
                        }
                        break;
                    }
                }
            }
        }

        protected override void TopLeftButtonAction()
        {
            var materiAssets = DatabaseManager.GetMateriAssets();
            var materiTree = DatabaseManager.GetMateriTree();

            List<MateriTreeElement> materiList = new List<MateriTreeElement>();
            TreeElementUtility.TreeToList(materiTree, materiList);

            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("edit content config"), false, () =>
            {
                VirtualTrainingContentConfigEditor.ShowContentConfig();
            });

            for (int i = 0; i < materiAssets.Count; i++)
            {
                MateriTreeAsset asset = materiAssets[i];
                if (InstanceId != asset.GetInstanceID())
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " open -> " + asset.name), false, () =>
                    {
                        Close();
                        GetWindow(asset.GetInstanceID());
                    });
                }
                else
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " current materi -> " + asset.name), false, null);
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("all content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.Materi)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("general content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.Maintenance)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("maintenance content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.Troubleshoot)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("troubleshoot content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.FlightScenario)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("flight scenario content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.RemoveInstall)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("remove install content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            menu.ShowAsContext();
        }

        private GenericMenu InsideElementContextMenuItem()
        {
            GenericMenu menu = CreateContextItems(false);
            menu.AddItem(new GUIContent("copy"), false, () =>
            {
                TreeView.CopyItems();
            });
            menu.AddItem(new GUIContent("paste"), false, () =>
            {
                TreeView.PasteItems();
            });
            menu.AddItem(new GUIContent("duplicate"), false, () =>
            {
                TreeView.DuplicateItems();
            });
            menu.AddItem(new GUIContent("remove"), false, () =>
            {
                TreeView.RemoveItem();
            });

            return menu;
        }

        private GenericMenu OutsideElementContextMenuItem()
        {
            return CreateContextItems(true);
        }

        private GenericMenu CreateContextItems(bool paste)
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("create new folder"), false, () =>
            {
                TreeView.CreateNewItem((int)MateriElementType.Materi, true);
            });
            menu.AddItem(new GUIContent("create new/general content"), false, () =>
             {
                 TreeView.CreateNewItem((int)MateriElementType.Materi);
             });
            menu.AddItem(new GUIContent("create new/maintenance content"), false, () =>
             {
                 TreeView.CreateNewItem((int)MateriElementType.Maintenance);
             });
            menu.AddItem(new GUIContent("create new/troubleshoot content"), false, () =>
             {
                 TreeView.CreateNewItem((int)MateriElementType.Troubleshoot);
             });
            menu.AddItem(new GUIContent("create new/flight scenario content"), false, () =>
            {
                TreeView.CreateNewItem((int)MateriElementType.FlightScenario);
            });
            menu.AddItem(new GUIContent("create new/remove install content"), false, () =>
            {
                TreeView.CreateNewItem((int)MateriElementType.RemoveInstall);
            });

            if (paste)
            {
                menu.AddItem(new GUIContent("paste"), false, () =>
                {
                    TreeView.PasteItems();
                });
            }

            return menu;
        }

        protected override void ContentView(List<MateriTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();
            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder && materiContentEditor != null)
                    materiContentEditor.DetailInspector(selectedElements[0].id, selectedElements[0], detailPanelRect, true);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        selectedElements[i].name = materiContentEditor.PreviewInspector(selectedElements[i].name, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "materi name" || GUI.GetNameOfFocusedControl() == "troubleshoot name" || GUI.GetNameOfFocusedControl() == "maintenance name" || GUI.GetNameOfFocusedControl() == "flight scenario name" || GUI.GetNameOfFocusedControl() == "remove install name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].MateriData.name)
                    {
                        selectedElements[i].name = selectedElements[i].MateriData.name;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].MateriData.name)
                    {
                        selectedElements[i].MateriData.name = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            EditorGUILayout.EndVertical();
        }

        protected override void BottomMiddleToolbar()
        {
            base.BottomMiddleToolbar();

            if (showMiddleToolbar)
            {
                if (GUILayout.Button("Removed Database Checker", EditorStyles.miniButton))
                {
                    RemovedDatabaseChecker.OpenWindow();
                }

                if (GUILayout.Button("Replace Description", EditorStyles.miniButton))
                {
                    ReplaceDescriptionTextTool.GetWindow();
                }

                if (GUILayout.Button("Check Duplicated Tree Element Id", EditorStyles.miniButton))
                {
                    if (EditorUtility.DisplayDialog("hello", "look at your console", "ok", "nanti dulu aja"))
                        CheckDuplicatedTreeElementId.DoCheck();
                }

                GUILayout.Label("others -> Virtual Training > Database Tools");

                if (GUILayout.Button("hide extra tools"))
                {
                    showMiddleToolbar = false;
                }
            }
            else
            {
                if (GUILayout.Button("show extra tools"))
                {
                    showMiddleToolbar = true;
                }
            }
        }
    }
}

#endif
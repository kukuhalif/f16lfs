#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(MateriTreeAsset))]
    public class MateriTreeAssetEditor : TreeAssetEditorBase<MateriTreeAsset, MateriTreeElement, MateriData>
    {
        protected override MateriTreeAsset GetAsset()
        {
            return (MateriTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<MateriData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<MateriTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}

#endif
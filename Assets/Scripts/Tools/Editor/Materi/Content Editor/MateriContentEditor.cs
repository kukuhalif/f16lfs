#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using Paroxe.PdfRenderer;
using System.IO;

namespace VirtualTraining.Tools
{

    public class MateriContentEditor : ContentEditorBase<Figure>
    {
        MateriTreeElement playedMateri;
        int currentFigureIndex;
        int currentCameraIndex;
        List<string> pdfAssetNames = new List<string>();
        List<PDFAsset> pdfAssets = new List<PDFAsset>();

        public MateriContentEditor(Action beforeModifiedCallback, Func<string> windowId, List<PDFAsset> pdfAssets, List<Material> xrayMats) :
            base(beforeModifiedCallback, windowId, xrayMats,
                new CategoryTab[] { CategoryTab.FIGURE_LIST, CategoryTab.SCHEMATIC, CategoryTab.ANIMATION, CategoryTab.VIRTUAL_BUTTON },
                new FigureTab[] { FigureTab.CAMERA, FigureTab.PART_OBJECT, FigureTab.CALLOUT, FigureTab.HELPER, FigureTab.CUTAWAY })
        {
            this.pdfAssets = pdfAssets;

            for (int i = 0; i < pdfAssets.Count; i++)
            {
                pdfAssetNames.Add(pdfAssets[i].name);
            }

            CreateListView("pdf", null, null, OnPdfElementDrawCallback, null);
        }

        protected override List<Schematic> GetSchematicFromData(object data)
        {
            Materi materi = data as Materi;
            return materi.schematics;
        }

        protected override Figure GetFigure(object data, int index)
        {
            Materi materi = data as Materi;
            return materi.figures[index];
        }

        protected override int GetFigureCount(object data)
        {
            Materi materi = data as Materi;
            return materi.figures.Count;
        }

        protected override void ShowFiguresField(object data, string listName, string fieldId)
        {
            Materi materi = data as Materi;
            ShowListField(listName, materi.figures, fieldId);
        }

        protected override void TopDetailInspector(object data, Rect panelRect)
        {
            MateriTreeElement materiTreeElement = data as MateriTreeElement;
            Materi materiData = materiTreeElement.MateriData;

            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("play materi (alt + t)") || PlayTestShortcut.PressedWindowId == WindowId)
                {
                    PlayTestShortcut.Played();
                    playedMateri = materiTreeElement;
                    currentFigureIndex = 0;
                    currentCameraIndex = 0;

                    for (int i = 0; i < materiData.pdfs.Count; i++)
                    {
                        if (pdfAssets.Count > i)
                            materiData.pdfs[i].pdfAsset = pdfAssets[materiData.pdfs[i].index];
                        else
                            Debug.LogError("setup materi pdf at materi config please");
                    }

                    EventManager.TriggerEvent(new MateriEvent(materiTreeElement));
                }

                if (playedMateri == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("reset materi / free play"))
                {
                    currentFigureIndex = 0;
                    currentCameraIndex = 0;
                    playedMateri = null;
                    EventManager.TriggerEvent(new FreePlayEvent());
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous figure"))
                {
                    currentFigureIndex--;
                    if (currentFigureIndex < 0)
                        currentFigureIndex = playedMateri.MateriData.figures.Count - 1;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PrevFigureEvent(MateriElementType.Materi));
                }

                if (GUILayout.Button("play next figure"))
                {
                    currentFigureIndex++;
                    if (playedMateri.MateriData.figures.Count <= currentFigureIndex)
                        currentFigureIndex = 0;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new NextFigureEvent(MateriElementType.Materi));
                }

                GUILayout.Label("current figure : " + currentFigureIndex);

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous camera"))
                {
                    currentCameraIndex--;
                    if (currentCameraIndex < 0)
                        currentCameraIndex = playedMateri.MateriData.figures[currentFigureIndex].cameraDestinations.Count - 1;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedMateri.MateriData.figures[currentFigureIndex].name, playedMateri.MateriData.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.Materi, playedMateri));
                }

                if (GUILayout.Button("play next camera"))
                {
                    currentCameraIndex++;
                    if (playedMateri.MateriData.figures[currentFigureIndex].cameraDestinations.Count <= currentCameraIndex)
                        currentCameraIndex = 0;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedMateri.MateriData.figures[currentFigureIndex].name, playedMateri.MateriData.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.Materi, playedMateri));
                }

                GUILayout.Label("current camera : " + currentCameraIndex);

                GUILayout.EndHorizontal();

                if (playedMateri == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);

            if (materiData.elementType == MateriElementType.Materi)
            {
                ShowSimpleField("materi name", ref materiData.name);
                ShowSimpleField("device mode", ref materiData.deviceMode);
                ShowSimpleField("disable environment", ref materiData.disableEnvironment);

                ShowListField("pdf", materiData.pdfs);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description, true);
            }
            else if (materiData.elementType == MateriElementType.Maintenance)
            {
                GUILayout.Label("maintenance");
                GUILayout.BeginHorizontal();
                ShowAssetField("maintenance database", ref materiData.maintenanceTree);
                if (GUILayout.Button(materiData.maintenanceTree == null ? "Create Maintenance Database" : "Open Maintenance Window"))
                {
                    OpenDatabase<MaintenanceTreeAsset>(materiData, () =>
                    {
                        return materiData.maintenanceTree;
                    },
                    (database) =>
                    {
                        materiData.maintenanceTree = database;
                    },
                    (instanceId) =>
                    {
                        MaintenanceEditorWindow.OnOpenAsset(instanceId);
                    });
                }
                GUILayout.EndHorizontal();
                ShowSimpleField("maintenance name", ref materiData.name);
                ShowSimpleField("device mode", ref materiData.deviceMode);
                ShowSimpleField("disable environment", ref materiData.disableEnvironment);
                ShowListField("pdf", materiData.pdfs);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description, true);
            }
            else if (materiData.elementType == MateriElementType.Troubleshoot)
            {
                GUILayout.Label("troubleshoot");
                GUILayout.BeginHorizontal();
                ShowAssetField("troubleshoot database", ref materiData.troubleshootData.troubleshootGraph);
                if (GUILayout.Button(materiData.troubleshootData.troubleshootGraph == null ? "Create Troubleshoot Database" : "Open Troubleshoot Database"))
                {
                    OpenDatabase<TroubleshootGraph>(materiData, () =>
                    {
                        return materiData.troubleshootData.troubleshootGraph;
                    },
                    (database) =>
                    {
                        materiData.troubleshootData.troubleshootGraph = database;
                    },
                    (instanceId) =>
                    {
                        TroubleshootGraphEditor.SetMateri(ref materiData);
                    });
                }
                GUILayout.EndHorizontal();
                materiData.troubleshootData.CompleteNameMateri = materiTreeElement.CompleteName;
                ShowSimpleField("troubleshoot name", ref materiData.name);
                ShowSimpleField("device mode", ref materiData.deviceMode);
                ShowSimpleField("disable environment", ref materiData.disableEnvironment);
                ShowSimpleField("Simplified Mode", "Simplified Mode", ref materiData.troubleshootData.isSimplifiedMode);
                ShowSimpleField("Size X Scale", "Size X Scalee", ref materiData.troubleshootData.sizeMultiplierX);
                ShowSimpleField("Size Y Scale", "Size Y Scalee", ref materiData.troubleshootData.sizeMultiplierY);
                ShowSimpleField("start postion", "Graph Start Position", ref materiData.troubleshootData.startPositionGraph);
                ShowListField("pdf", materiData.pdfs);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description, true);
            }
            else if (materiData.elementType == MateriElementType.FlightScenario)
            {
                GUILayout.Label("flight scenario");
                GUILayout.BeginHorizontal();
                ShowAssetField("flight scenario database", ref materiData.flightScenarioTree);
                if (GUILayout.Button(materiData.flightScenarioTree == null ? "Create Flight Scenario Database" : "Open Flight Scenario Database"))
                {
                    OpenDatabase<FlightScenarioTreeAsset>(materiData, () =>
                    {
                        return materiData.flightScenarioTree;
                    },
                    (database) =>
                    {
                        materiData.flightScenarioTree = database;
                    },
                    (instanceId) =>
                    {
                        FlightScenarioEditorWindow.OnOpenAsset(instanceId);
                    });
                }
                GUILayout.EndHorizontal();
                ShowSimpleField("flight scenario name", ref materiData.name);
                ShowSimpleField("device mode", ref materiData.deviceMode);
                ShowSimpleField("disable environment", ref materiData.disableEnvironment);
                ShowListField("pdf", materiData.pdfs);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description, true);
            }
            else if (materiData.elementType == MateriElementType.RemoveInstall)
            {
                GUILayout.Label("remove install");
                GUILayout.BeginHorizontal();
                ShowAssetField("remove install database", ref materiData.removeInstallTree);
                if (GUILayout.Button(materiData.removeInstallTree == null ? "Create Remove Install Database" : "Open Remove Install Database"))
                {
                    OpenDatabase<RemoveInstallTreeAsset>(materiData, () =>
                    {
                        return materiData.removeInstallTree;
                    },
                    (database) =>
                    {
                        materiData.removeInstallTree = database;
                    },
                    (instanceId) =>
                    {
                        RemoveInstallEditorWindow.OnOpenAsset(instanceId);
                    });
                }
                GUILayout.EndHorizontal();
                ShowSimpleField("remove install name", ref materiData.name);
                ShowSimpleField("device mode", ref materiData.deviceMode);
                ShowSimpleField("disable environment", ref materiData.disableEnvironment);
                ShowListField("pdf", materiData.pdfs);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description, true);
            }
        }

        protected override void BottomDetailInspector(object data, Rect panelRect)
        {
            MateriTreeElement materiTreeElement = data as MateriTreeElement;
            ShowTab("categories", materiTreeElement.MateriData);
        }

        #region pdf

        private object OnPdfElementDrawCallback(string listId, object element, int index)
        {
            PdfData pdfMateri = element as PdfData;

            GUILayout.BeginVertical();

            if (pdfAssetNames.Count > 0)
            {
                pdfMateri.index = ShowDropdownField("pdf", listId + index, pdfAssetNames[pdfMateri.index], pdfAssetNames);
                ShowSimpleField("page", listId + index + 1, ref pdfMateri.page);
            }
            else
            {
                GUILayout.Label("setup materi pdf at materi config please");
            }

            GUILayout.EndVertical();

            return pdfMateri;
        }

        #endregion


        #region open database

        private void OpenDatabase<T>(Materi materiData, Func<T> getDatabase, Action<T> setDatabase, Action<int> openFileCallback)
            where T : ScriptableObject
        {
            string folderName = materiData.elementType.ToString();
            string dir = Path.GetDirectoryName(Application.dataPath) + "/" + DatabaseManager.GetContentPathConfig().contentAssetPath + "/" + folderName;

            if (!Directory.Exists(dir))
            {
                Debug.Log("create directory " + dir);
                Directory.CreateDirectory(dir);
            }

            if (getDatabase.Invoke() == null)
            {
                int fileNameEnding = -1;
                string filePath = "";

                do
                {
                    fileNameEnding++;
                    filePath = Path.GetDirectoryName(Application.dataPath) + "/" + DatabaseManager.GetContentPathConfig().contentAssetPath + "/" + folderName + "/" + materiData.name + "_" + fileNameEnding + ".asset";
                } while (File.Exists(filePath));

                T newFile = ScriptableObject.CreateInstance<T>();
                string objectName = materiData.name + "_" + fileNameEnding;
                string assetPath = DatabaseManager.GetContentPathConfig().contentAssetPath + "/" + folderName + "/" + objectName + ".asset";

                newFile.name = objectName;

                AssetDatabase.CreateAsset(newFile, assetPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                Debug.Log("create new database " + assetPath);

                setDatabase.Invoke(newFile);

                openFileCallback.Invoke(newFile.GetInstanceID());
            }
            else
            {
                Debug.Log("open existing database " + AssetDatabase.GetAssetPath(getDatabase.Invoke()));
                openFileCallback.Invoke(getDatabase.Invoke().GetInstanceID());
            }
        }

        #endregion
    }

}

#endif
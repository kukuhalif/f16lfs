﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class CutawayEditor
    {
        static CutawayEditor CURRENT_EDITOR;

        Cutaway currentCutawayData;

        CutawayManager cutawayManager;

        CutawayState stateTemp;

        Vector3 minPos;
        Vector3 maxPos;

        Vector3 maxScale;

        public CutawayEditor(CutawayManager cutawayManager)
        {
            this.cutawayManager = cutawayManager;

            minPos = DatabaseManager.GetCutawayConfig().minPos;
            maxPos = DatabaseManager.GetCutawayConfig().maxPos;
            maxScale = DatabaseManager.GetCutawayConfig().maxScale;

            CURRENT_EDITOR = this;
        }

        public void Show(Cutaway cutaway, PropertyField propertyField, string fieldId)
        {
            if (cutawayManager == null || cutawayManager.Gizmo == null)
            {
                GUILayout.Label("cutaway manager or cutaway gizmo not found");
                GUILayout.Label("enter play mode");
                return;
            }

            if (currentCutawayData != cutaway && CURRENT_EDITOR == this)
            {
                EventManager.TriggerEvent(new CutawayEvent(cutaway));
            }

            currentCutawayData = cutaway;

            fieldId += "cutaway";
            int idx = 0;

            if (cutaway.state != CutawayState.None)
                GUILayout.BeginHorizontal();

            stateTemp = cutaway.state;
            propertyField.ShowSimpleField("cutaway", fieldId + idx++, ref cutaway.state);

            if (stateTemp != cutaway.state)
            {
                EventManager.TriggerEvent(new CutawayEvent(cutaway));
                stateTemp = cutaway.state;
            }

            if (cutaway.state != CutawayState.None)
            {
                if (GUILayout.Button("default position", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                {
                    propertyField.TriggerBeforeModifiedCallback();

                    var bounds = cutawayManager.GetBounds(VirtualTrainingSceneManager.GameObjectRoot);

                    cutaway.position = bounds.center;
                    cutaway.scale = bounds.size;
                    cutaway.rotation = Quaternion.identity.eulerAngles;

                    EventManager.TriggerEvent(new CutawayEvent(cutaway));
                }

                if (GUILayout.Button("edit min max slider value", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                {
                    VirtualTrainingAppConfigEditor.OpenCutawayConfig();
                }

                if (CURRENT_EDITOR != this && GUILayout.Button("activate preview", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                {
                    CURRENT_EDITOR = this;
                    currentCutawayData = cutaway;
                    EventManager.TriggerEvent(new CutawayEvent(cutaway));
                }

                if (CURRENT_EDITOR == this && GUILayout.Button("disable preview", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                {
                    CURRENT_EDITOR = null;
                    EventManager.TriggerEvent(new CutawayEvent());
                }

                GUILayout.EndHorizontal();

                GUILayout.Space(10f);
                propertyField.ShowSlider("x position", fieldId + idx++, ref cutaway.position.x, minPos.x, maxPos.x);
                propertyField.ShowSlider("y position", fieldId + idx++, ref cutaway.position.y, minPos.y, maxPos.y);
                propertyField.ShowSlider("z position", fieldId + idx++, ref cutaway.position.z, minPos.z, maxPos.z);

                GUILayout.Space(10f);
                propertyField.ShowSlider("x rotation", fieldId + idx++, ref cutaway.rotation.x, 0, 360);
                propertyField.ShowSlider("y rotation", fieldId + idx++, ref cutaway.rotation.y, 0, 360);
                propertyField.ShowSlider("z rotation", fieldId + idx++, ref cutaway.rotation.z, 0, 360);

                GUILayout.Space(10f);
                propertyField.ShowSlider("x scale", fieldId + idx++, ref cutaway.scale.x, 0, maxScale.x);
                propertyField.ShowSlider("y scale", fieldId + idx++, ref cutaway.scale.y, 0, maxScale.y);
                propertyField.ShowSlider("z scale", fieldId + idx++, ref cutaway.scale.z, 0, maxScale.z);

                if (CURRENT_EDITOR == this)
                {
                    cutawayManager.Gizmo.transform.position = cutaway.position;
                    cutawayManager.Gizmo.transform.rotation = Quaternion.Euler(cutaway.rotation);
                    cutawayManager.transform.localScale = cutaway.scale;
                }
            }
        }
    }
}
#endif

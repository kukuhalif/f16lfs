using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class PartObjectEditor : PropertyField
    {
        bool ENABLE_FRESNEL_AND_HIDE = false;

        bool swipeAllToLeft;
        bool swipeAllToRight;
        bool clickedPartObjectHeader;
        bool assignSelecteds;

        Action beforeModifiedCallback;
        PartObjectAction clickedPartObjectAction;
        List<string> xrayMats = new List<string>();

        static List<PartObjectEditor> REGISTERED_EDITORS = new List<PartObjectEditor>();

        [MenuItem("Virtual Training Shortcut/Assign Selected Objects to Opened or Expanded Part Objects &d", false)]
        static void AssignObjectShortcut()
        {
            REGISTERED_EDITORS.RemoveAll(delegate (PartObjectEditor o) { return o == null; });
            foreach (var partObjectEditor in REGISTERED_EDITORS)
            {
                if (partObjectEditor.WindowId == VirtualTrainingEditorState.GetLastFocusedWindowId())
                    partObjectEditor.assignSelecteds = true;
            }
        }

        [MenuItem("Virtual Training Shortcut/Assign Selected Objects to Opened or Expanded Part Objects &d", true)]
        static bool AssignObjectShortcutVerify()
        {
            return REGISTERED_EDITORS.Count > 0 && !string.IsNullOrEmpty(VirtualTrainingEditorState.GetLastFocusedWindowId());
        }

        public PartObjectEditor(Action beforeModifiedCallback, string windowId, List<Material> xrayMats) : base(beforeModifiedCallback, windowId)
        {
            this.beforeModifiedCallback = beforeModifiedCallback;
            for (int i = 0; i < xrayMats.Count; i++)
            {
                this.xrayMats.Add(xrayMats[i].name);
            }
            CreateListView("part object", null, OnPartObjectHeaderDrawCallback, OnPartObjectElementDrawCallback, OnPartObjectDetailDrawCallback, FilterPartObjectElementCallback, false, typeof(GameObject));
            REGISTERED_EDITORS.Add(this);
            REGISTERED_EDITORS = REGISTERED_EDITORS.Distinct().ToList();
        }

        public void TurnOffAssignSelectedObjects()
        {
            assignSelecteds = false;
        }

        public void Show(List<PartObject> partObjects, string fieldId, int index)
        {
            if (assignSelecteds && VirtualTrainingEditorState.GetFoldoutState(WindowId, fieldId + index))
            {
                foreach (var obj in Selection.objects)
                {
                    GameObject gameObject = obj as GameObject;
                    if (gameObject != null)
                    {
                        GameObjectReference gor = gameObject.GetComponent<GameObjectReference>();
                        if (gor != null)
                        {
                            PartObject newPartObj = new PartObject();
                            newPartObj.target.Id = gor.Id;
                            partObjects.Add(newPartObj);
                        }
                    }
                }

                Debug.Log("assign object to part object shortcut");
            }

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("paste from favorite"))
            {
                FavoriteEditor favoriteEditor = FavoriteEditor.GetFavoriteEditor();
                var temps = favoriteEditor.GetTempGameobjectList();
                for (int i = 0; i < temps.Count; i++)
                {
                    PartObject newPart = new PartObject();
                    newPart.target.Id = temps[i].Id;
                    partObjects.Add(newPart);
                }
            }

            if (GUILayout.Button("add to favorite"))
            {
                FavoriteEditor favoriteEditor = FavoriteEditor.GetFavoriteEditor();

                List<GameObject> gameObjects = new List<GameObject>();
                for (int i = 0; i < partObjects.Count; i++)
                {
                    gameObjects.Add(partObjects[i].target.gameObject);
                }

                favoriteEditor.AddGameobjectList("new gameobject list", gameObjects);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            ShowListField("part object", partObjects, fieldId + index);

            if (clickedPartObjectHeader)
            {
                beforeModifiedCallback?.Invoke();

                foreach (var part in partObjects)
                {
                    part.action = clickedPartObjectAction;
                }

                clickedPartObjectHeader = false;
            }

            if (swipeAllToLeft)
            {
                beforeModifiedCallback?.Invoke();

                foreach (var part in partObjects)
                {
                    part.action--;

                    if ((int)part.action <= 0)
                        part.action = PartObjectAction.Solo;
                }

                swipeAllToLeft = false;
            }

            if (swipeAllToRight)
            {
                beforeModifiedCallback?.Invoke();

                foreach (var part in partObjects)
                {
                    part.action++;

                    int count = ENABLE_FRESNEL_AND_HIDE ? Enum.GetValues(typeof(PartObjectAction)).Length : Enum.GetValues(typeof(PartObjectAction)).Length - 2;
                    if ((int)part.action >= count)
                        part.action = ENABLE_FRESNEL_AND_HIDE ? PartObjectAction.Hide : PartObjectAction.IgnoreCutaway;
                }

                swipeAllToRight = false;
            }
        }

        private string FilterPartObjectElementCallback(object element)
        {
            PartObject partObject = element as PartObject;

            if (partObject.target.gameObject == null)
                return "null";

            return partObject.target.gameObject.name;
        }

        private void OnPartObjectHeaderDrawCallback()
        {
            EditorGUILayout.LabelField("target object", GUILayout.Width(135f));

            if (DrawButton(IconEnum.PartObjectSolo, "solo")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Solo; }
            if (DrawButton(IconEnum.PartObjectHide, "disable")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Disable; }
            if (DrawButton(IconEnum.PartObjectHighlight, "highlight")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Highlight; }
            if (DrawButton(IconEnum.PartObjectXRay, "x ray")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Xray; }
            if (DrawButton(IconEnum.PartObjectShow, "enable")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Enable; }
            if (DrawButton(IconEnum.PartObjectSelect, "select")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Select; }
            if (DrawButton(IconEnum.PartObjectBlink, "blink")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Blink; }
            if (DrawButton(IconEnum.PartObjectSetLayer, "set layer")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.SetLayer; }
            if (DrawButton(IconEnum.PartObjectIgnoreCutaway, "ignore cutaway")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.IgnoreCutaway; }

            // optional, icon masih dummy ckck
            if (ENABLE_FRESNEL_AND_HIDE && DrawButton(IconEnum.PartObjectIgnoreCutaway, "fresnel")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Fresnel; }
            if (ENABLE_FRESNEL_AND_HIDE && DrawButton(IconEnum.PartObjectIgnoreCutaway, "hide")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Hide; }

            if (GUILayout.Button("geser kekiri")) { swipeAllToLeft = true; }
            if (GUILayout.Button("geser kekanan")) { swipeAllToRight = true; }
        }

        private object OnPartObjectElementDrawCallback(string listId, object element, int index)
        {
            PartObject partObject = element as PartObject;

            string id = listId + "partObj" + index;

            ShowSimpleField("", id, ref partObject.target, 0f, 120f);
            GUILayout.Space(19f);
            float toggleWidth = 36f;

            var actions = Enum.GetValues(typeof(PartObjectAction));
            bool[] states = new bool[actions.Length];

            int elementCount = ENABLE_FRESNEL_AND_HIDE ? states.Length : states.Length - 2;

            // start from 1 because element 0 is none
            for (int i = 1; i < elementCount; i++)
            {
                states[i] = partObject.action == (PartObjectAction)actions.GetValue(i);
                ShowSimpleField("", id + i, ref states[i], 0f, toggleWidth);
                if (states[i])
                    partObject.action = (PartObjectAction)actions.GetValue(i);
            }

            return partObject;
        }

        private object OnPartObjectDetailDrawCallback(string listId, object element, int index)
        {
            PartObject partObject = element as PartObject;
            string id = listId + "partObjDetail" + index;
            int idx = 0;

            switch (partObject.action)
            {
                case PartObjectAction.Solo:

                    ShowSimpleField("recursive", id + idx++, ref partObject.isRecursive);

                    break;
                case PartObjectAction.Disable:

                    // no data

                    break;
                case PartObjectAction.Highlight:

                    ShowSimpleField("recursive", id + idx++, ref partObject.isRecursive);
                    ShowSimpleField("set highlight color", id + idx++, ref partObject.setHighlightColor);
                    EditorGUI.BeginDisabledGroup(!partObject.setHighlightColor);
                    ShowSimpleField("default highlight color", id + idx++, ref partObject.useDefaultColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultColor);
                    ShowSimpleField("custom highlight color", id + idx++, ref partObject.customColor);
                    EditorGUI.EndDisabledGroup();
                    EditorGUI.EndDisabledGroup();
                    XrayMaterialDropdown("x ray variant", id + idx++, ref partObject.xrayIndex);

                    break;
                case PartObjectAction.Xray:

                    ShowSimpleField("recursive", id + idx++, ref partObject.isRecursive);
                    XrayMaterialDropdown("x ray variant", id + idx++, ref partObject.xrayIndex);

                    break;
                case PartObjectAction.Enable:

                    // no data

                    break;
                case PartObjectAction.Select:

                    ShowSimpleField("recursive", id + idx++, ref partObject.isRecursive);
                    ShowSimpleField("set selected color", id + idx++, ref partObject.setHighlightColor);
                    EditorGUI.BeginDisabledGroup(!partObject.setHighlightColor);
                    ShowSimpleField("default selected color", id + idx++, ref partObject.useDefaultColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultColor);
                    ShowSimpleField("custom selected color", id + idx++, ref partObject.customColor);
                    EditorGUI.EndDisabledGroup();
                    EditorGUI.EndDisabledGroup();

                    break;
                case PartObjectAction.Blink:

                    ShowSimpleField("default blink color", id + idx++, ref partObject.useDefaultColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultColor);
                    ShowSimpleField("custom blink color", id + idx++, ref partObject.customColor);
                    EditorGUI.EndDisabledGroup();
                    ShowSimpleField("recursive", id + idx++, ref partObject.isRecursive);
                    ShowSimpleField("speed", id + idx++, ref partObject.blinkSpeed);

                    break;
                case PartObjectAction.SetLayer:

                    ShowSimpleField("layer", id + idx++, ref partObject.layerOption);

                    break;
                case PartObjectAction.IgnoreCutaway:
                    break;
                case PartObjectAction.Fresnel:

                    ShowSimpleField("recursive", id + idx++, ref partObject.isRecursive);
                    ShowSimpleField("fresnel default color", id + idx++, ref partObject.useDefaultColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultColor);
                    ShowSimpleField("fresnel color", id + idx++, ref partObject.customColor);
                    EditorGUI.EndDisabledGroup();
                    ShowSimpleField("fresnel with blink", id + idx++, ref partObject.fresnelWithBlink);
                    ShowSimpleField("blink speed", id + idx++, ref partObject.blinkSpeed);

                    break;
            }

            return partObject;
        }

        private void XrayMaterialDropdown(string title, string controlName, ref int index)
        {
            index = ShowDropdownField(title, controlName, xrayMats[index], xrayMats);
        }
    }
}


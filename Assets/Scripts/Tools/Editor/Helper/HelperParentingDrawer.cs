#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(HelperParenting))]
    [CanEditMultipleObjects]
    public class HelperParentingDrawer : Editor
    {
        PropertyField propertyField;

        public override void OnInspectorGUI()
        {
            if (propertyField == null)
                propertyField = new PropertyField(null, "helper parenting");

            HelperParenting helperParenting = target as HelperParenting;
            propertyField.ShowSimpleField("helper parent", ref helperParenting.parent);
        }
    }
}

#endif

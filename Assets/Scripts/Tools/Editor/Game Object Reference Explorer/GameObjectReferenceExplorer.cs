#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class GameObjectReferenceExplorer : EditorWindow
    {
        [MenuItem("Virtual Training/Database Tools/GameObject Reference Explorer")]
        public static void OpenWindow()
        {
            var window = GetWindow<GameObjectReferenceExplorer>();
            window.titleContent = new GUIContent("Reference Explorer");
        }

        class GameobjectData
        {
            public bool isObjectRoot;
            public bool objectRootChild;
            public string path;
            public GameObject gameObject;
            public int id;

            public GameobjectData(GameObjectReference gameObjectReference)
            {
                id = gameObjectReference.Id;
                gameObject = gameObjectReference.gameObject;
                var parents = gameObject.transform.GetAllParents();
                foreach (var parent in parents)
                {
                    path = parent.gameObject.name + "/" + path;
                    if (parent.gameObject == VirtualTrainingSceneManager.GameObjectRoot)
                        objectRootChild = true;
                }

                if (gameObject == VirtualTrainingSceneManager.GameObjectRoot)
                    isObjectRoot = true;
            }
        }

        SceneMode modelSceneMode = SceneMode.Desktop;
        string listTitle = "gameobject reference list (filter with gameobject name or id)";
        List<GameobjectData> gameobjectDatas = new List<GameobjectData>();
        PropertyField propertyField;
        bool showDuplicated;
        bool lastShowDuplicated;
        bool generatingNewId;
        Dictionary<int, List<GameObject>> gameobjectIdLookup = new Dictionary<int, List<GameObject>>();
        List<GameobjectData> duplicatedGameobjectDatas = new List<GameobjectData>();
        int tempId = 0;

        private void OnEnable()
        {
            propertyField = new PropertyField(null, "GameObject Reference Explorer");
            propertyField.CreateListView(listTitle, null, null, OnGameobjectReferenceCallback, null, FilterStringCallback);

            modelSceneMode = DatabaseManager.GetStartingSceneConfig().mode;

            Refresh();
        }

        private void Refresh()
        {
            gameobjectDatas.Clear();
            var references = FindObjectsOfType<GameObjectReference>();
            foreach (var reference in references)
            {
                gameobjectDatas.Add(new GameobjectData(reference));
            }
        }

        private string FilterStringCallback(object element)
        {
            GameobjectData data = element as GameobjectData;

            return data.gameObject.name + " " + data.id.ToString();
        }

        private object OnGameobjectReferenceCallback(string listId, object element, int index)
        {
            GameobjectData data = element as GameobjectData;

            propertyField.ShowSimpleField("gameobject", listId + index + "a", ref data.gameObject, 80, 300);

            GUILayout.Space(10);

            tempId = data.id;
            propertyField.ShowSimpleField("id", listId + index + "b", ref tempId, 15, 200);

            GUILayout.Space(10);

            if (showDuplicated && GUILayout.Button("regenerate id"))
            {
                if (EditorUtility.DisplayDialog("Reference Explorer", "are you sure to regenerate this id? any data that references this game object will be lost", "yes", "no"))
                {
                    EditorCoroutineUtility.StartCoroutineOwnerless(RegenerateId(data));
                }
            }

            if (data.isObjectRoot)
                GUILayout.Label("is object root -> " + data.path);
            else
                GUILayout.Label((data.objectRootChild ? "is object root child -> " : "outside object root -> ") + data.path);

            return data;
        }

        private IEnumerator RegenerateId(GameobjectData data)
        {
            generatingNewId = true;

            yield return null;

            Dictionary<int, GameObjectReference> objectReferenceLookup = new Dictionary<int, GameObjectReference>();
            GameObjectReference[] references = FindObjectsOfType<GameObjectReference>();

            foreach (GameObjectReference reference in references)
            {
                if (!objectReferenceLookup.ContainsKey(reference.Id))
                    objectReferenceLookup.Add(reference.Id, reference);
            }

            int invalidIdCounter = 0;
            int newId = 0;
            string parentName = "";

            do
            {
                var parents = data.gameObject.transform.GetAllParents();
                parentName = "";

                for (int p = parents.Count - 1; p >= 0; p--)
                {
                    parentName += parents[p].name + " / ";
                }

                newId = Animator.StringToHash(parentName + invalidIdCounter);

                invalidIdCounter++;
            } while (objectReferenceLookup.ContainsKey(newId));

            yield return null;

            data.gameObject.GetComponent<GameObjectReference>().SetId(newId);

            EditorUtility.DisplayDialog("Reference Explorer", "new id : " + newId, "ok");

            Refresh();
            CheckDuplicated();

            EditorUtility.SetDirty(data.gameObject);
            VirtualTrainingEditorUtility.SetModelSceneDirty(modelSceneMode);

            yield return null;

            generatingNewId = false;
            Repaint();
        }

        private void CheckDuplicated()
        {
            gameobjectIdLookup.Clear();
            foreach (var go in gameobjectDatas)
            {
                if (!gameobjectIdLookup.ContainsKey(go.id))
                    gameobjectIdLookup.Add(go.id, new List<GameObject>());

                gameobjectIdLookup[go.id].Add(go.gameObject);
            }

            duplicatedGameobjectDatas.Clear();
            foreach (var go in gameobjectIdLookup.Keys)
            {
                var gameobjects = gameobjectIdLookup[go];

                if (gameobjects.Count > 1)
                    foreach (var gameobject in gameobjects)
                    {
                        duplicatedGameobjectDatas.Add(new GameobjectData(gameobject.GetComponent<GameObjectReference>()));
                    }
            }
        }

        private void OnGUI()
        {
            if (!VirtualTrainingEditorUtility.LoadMainSceneWarningField(ref modelSceneMode, position.width))
            {
                return;
            }

            if (generatingNewId)
            {
                GUILayout.Label("generating new Id");
            }
            else
            {
                propertyField.ShowSimpleField("show only duplicated id", ref showDuplicated);

                if (lastShowDuplicated != showDuplicated)
                {
                    if (showDuplicated)
                        CheckDuplicated();

                    lastShowDuplicated = showDuplicated;
                }

                if (showDuplicated)
                    propertyField.ShowListFieldReadOnly(listTitle, duplicatedGameobjectDatas);
                else
                    propertyField.ShowListFieldReadOnly(listTitle, gameobjectDatas);

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("refresh"))
                {
                    GUI.FocusControl(null);

                    Refresh();

                    if (showDuplicated)
                        CheckDuplicated();

                    Repaint();
                }
            }
        }
    }
}

#endif
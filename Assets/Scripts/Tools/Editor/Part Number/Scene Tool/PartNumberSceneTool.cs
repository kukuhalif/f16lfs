#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(GameObjectReference))]
    public class PartNumberSceneTool : GameObjectReferenceEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("add part number (alt + n)"))
            {
                var partNumberEditor = PartNumberEditorWindow.GetEditor();
                partNumberEditor.AddOrFocus(((GameObjectReference)target).Id, ((GameObjectReference)target).name);
            }
        }
    }
}

#endif
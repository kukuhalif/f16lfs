#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class PartNumberContentEditor : PropertyField
    {
        public PartNumberContentEditor(Action beforeModifiedCallback, Func<string> windowId) : base(beforeModifiedCallback, windowId)
        {
            CreateListView("parts", null, null, PartElementCallback, null, null, false, typeof(GameObject));
        }

        private object PartElementCallback(string listId, object element, int index)
        {
            GameObjectType got = element as GameObjectType;
            ShowSimpleField("part", listId + index, ref got);
            return got;
        }

        public void PreviewInspector(PartNumberDataModel data, int index)
        {

        }

        public void DetailInspector(PartNumberDataModel data)
        {
            ShowListField("parts", data.parts);
        }
    }
}

#endif
#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using UnityEditor.IMGUI.Controls;

namespace VirtualTraining.Tools
{
    public class PartNumberEditorWindow : TreeViewEditorBase<PartNumberTreeAsset, PartNumberEditorWindow, PartNumberData, PartNumberTreeElement>
    {
        PartNumberContentEditor partNumberContentEditor;

        [MenuItem("Virtual Training/FreePlay/Part Number Editor")]
        public static void GetWindow()
        {
            PartNumberEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            PartNumberEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
                return true;
            }
            return false;
        }

        [MenuItem("Virtual Training Shortcut/Add Part Number &n")]
        public static void AddPartNumber()
        {
            var partNumberEditor = PartNumberEditorWindow.GetEditor();

            List<int> newIds = new List<int>();

            foreach (var obj in Selection.gameObjects)
            {
                var gor = obj.GetComponent<GameObjectReference>();
                if (gor != null)
                {
                    newIds.Add(partNumberEditor.AddOrFocus(gor.Id, obj.name));
                }
            }

            partNumberEditor.TreeView.SetSelection(newIds, TreeViewSelectionOptions.RevealAndFrame);
        }

        [MenuItem("Virtual Training Shortcut/Add Part Number &n", true)]
        public static bool AddPartNumberValidator()
        {
            return Selection.gameObjects.Length > 0;
        }

        public static PartNumberEditorWindow GetEditor()
        {
            if (HasOpenInstances<PartNumberEditorWindow>())
            {
                return GetWindow<PartNumberEditorWindow>();
            }
            else
            {
                PartNumberEditorWindow window = OpenEditorWindow();
                if (window != null)
                {
                    window.Initialize();
                }
                return window;
            }
        }

        public int AddOrFocus(int objectId, string name)
        {
            // find
            var elements = ScriptableObjectTemp.GetData().treeElements;
            foreach (var element in elements)
            {
                PartNumberTreeElement pte = element as PartNumberTreeElement;
                foreach (var part in pte.PartNumberData.parts)
                {
                    if (part.Id == objectId)
                    {
                        TreeView.SetSelection(new[] { element.id }, TreeViewSelectionOptions.RevealAndFrame);
                        return element.id;
                    }
                }
            }

            // add
            var newItem = TreeView.CreateNewItem();
            newItem.PartNumberData.parts.Add(new GameObjectType(objectId));
            newItem.name = name;
            TreeView.Reload();
            return newItem.id;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            partNumberContentEditor = new PartNumberContentEditor(RecordUndo, () => WindowId);
        }

        protected override TreeViewWithTreeModel<PartNumberTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<PartNumberTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<PartNumberTreeElement>(TreeViewState, treeModel);
        }

        protected override PartNumberTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new PartNumberTreeElement("new object", depth, id, false);
        }

        protected override void ContentView(List<PartNumberTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            GUILayout.Space(10f);

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder)
                    partNumberContentEditor.DetailInspector(selectedElements[0].PartNumberData);
            }
            else
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        partNumberContentEditor.PreviewInspector(selectedElements[i].PartNumberData, i);
                }
            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndVertical();
        }
    }
}
#endif

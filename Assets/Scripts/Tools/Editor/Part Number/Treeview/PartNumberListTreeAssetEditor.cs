#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(PartNumberTreeAsset))]
    public class PartNumberListTreeAssetEditor : TreeAssetEditorBase<PartNumberTreeAsset, PartNumberTreeElement, PartNumberData>
    {
        // Start is called before the first frame update
        protected override PartNumberTreeAsset GetAsset()
        {
            return (PartNumberTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<PartNumberData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<PartNumberTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}

#endif

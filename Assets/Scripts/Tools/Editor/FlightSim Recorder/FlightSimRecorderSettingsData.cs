using UnityEngine;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [System.Serializable]
    public class FlightSimRecorderSettingsData
    {
        // recording settings
        //public string saveFolderLocation;
        public FolderReferenceType saveFolderReference;
        
        public string recordingPrefix;
        //public string clipPrefix;
        public int skipFrames;
        public List<FlightSimRecordingItem> animationItems;

        // aircraft data
        public AircraftType aircraftType;
        public AircraftMode aircraftMode;
        public Vector2 aircraftLocation;
        [Range(0.0f, 13000.0f)]
        public float aircraftAltitude;
        public float aircraftSpeed;
        public float aircraftVerticalSpeed;
        [Range(0.0f, 359.9f)]
        public float aircraftHeading;

        // environment data
        [Range(0.0f, 24.0f)]
        public float environmentTime;
        [Range(0.0f, 1.0f)]
        public float environmentCloud;
        public bool cityLightsStatus;

        public FlightSimRecorderSettingsData()
        {
            saveFolderReference = new FolderReferenceType();
            //saveFolderReference.path = DatabaseManager.GetContentPathConfig().contentAssetPath + "Recordings";
            recordingPrefix = "recording";
            //clipPrefix = "anim";
            skipFrames = 10;
            animationItems = new List<FlightSimRecordingItem>();

            // aircraft data
            aircraftType = AircraftType.Helicopter;
            aircraftMode = AircraftMode.Hot;
            aircraftLocation = new Vector2(0f, 0f);
            aircraftAltitude = 100f;
            aircraftSpeed = 100f;
            aircraftVerticalSpeed = 10f;
            aircraftHeading = 0f;

            // environment data
            environmentTime = 12f; // Time value: from 00:00 to 24:00
            environmentCloud = 0.5f; // Cloud thickness value: from 0 (clear) to 1 (cloudy)
            cityLightsStatus = false;
        }
    }
}
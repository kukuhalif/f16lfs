using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(ObjectFieldRecordingHelper))]
    public class ObjectFieldRecordingHelperEditor : Editor
    {
        Color backgroundColor;
        ObjectFieldRecordingHelper objFieldRecordingHelper;



        private void OnEnable()
        {
            objFieldRecordingHelper = (ObjectFieldRecordingHelper)target;
        }

        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;

            serializedObject.Update();

            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(2f);
            GUI.color = Color.cyan;
            EditorGUILayout.HelpBox("Object Fields", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);

            // Get object properties
            //objProps = objPropRecordingHelper.gameObject.GetType().GetProperties();

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }
    }
}

#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(AnimatorRecordingHelper))]
    public class AnimatorRecordingHelperEditor : Editor
    {
        Color backgroundColor;
        AnimatorRecordingHelper animRecordingHelper;
        Animator animator;
        private AnimatorController animatorController;

        private void OnEnable()
        {
            animRecordingHelper = (AnimatorRecordingHelper)target;
            animRecordingHelper.animatorToRecord = animRecordingHelper.transform.parent.GetComponent<Animator>();
            animator = animRecordingHelper.animatorToRecord;
            animatorController = animator.runtimeAnimatorController as AnimatorController;

            // init animParam array
            /*for (int i = 0; i < 16; i++)
            {
                AnimationParameterController.AnimParam animParam = new AnimationParameterController.AnimParam();
                animParam.recordable = false;
                animParam.name = "";
                animParamController.animParams[i] = animParam;
            }*/
        }

        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;

            serializedObject.Update();

            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(2f);
            GUI.color = Color.cyan;
            EditorGUILayout.HelpBox("Animation Parameters", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);

            if (animator != null && animatorController != null)
            {
                // Get animator parameters
                AnimatorControllerParameter[] animatorParams = animatorController.parameters;
                int idx = 0;
                foreach (AnimatorControllerParameter param in animatorParams)
                {
                    if (idx == animRecordingHelper.animParams.Count)
                    {
                        AnimatorRecordingHelper.AnimParam animParam = new AnimatorRecordingHelper.AnimParam();
                        animParam.name = param.name;
                        animParam.recordable = false;
                        animRecordingHelper.animParams.Add(animParam);
                    }
                    animRecordingHelper.animParams[idx].name = param.name;
                    idx++;
                }
            }

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
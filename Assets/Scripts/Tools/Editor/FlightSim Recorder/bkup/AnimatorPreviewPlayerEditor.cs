#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(AnimatorPreviewPlayer))]
    public class AnimatorPreviewPlayerEditor : Editor
    {
        Color backgroundColor;
        AnimatorPreviewPlayer animPreviewPlayer;
        //Animator animator;
        //private AnimatorController animatorController;

        private void OnEnable()
        {
            animPreviewPlayer = (AnimatorPreviewPlayer)target;
            animPreviewPlayer.animator = animPreviewPlayer.GetComponent<Animator>();
            //animator = animPreviewPlayer.animator;
            //animatorController = animator.runtimeAnimatorController as AnimatorController;

        }

        public override void OnInspectorGUI()
        {
            backgroundColor = GUI.backgroundColor;

            //serializedObject.Update();

            // ----------------------------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.Space(2f);
            GUI.color = Color.blue;
            EditorGUILayout.HelpBox("Animator Control", MessageType.None);
            GUI.color = backgroundColor;
            GUILayout.Space(2f);

            if (GUILayout.Button("Play Animator", GUILayout.Height(30)))
            {
                animPreviewPlayer.Play();
            }

            if (GUILayout.Button("Stop Animator", GUILayout.Height(30)))
            {
                animPreviewPlayer.Stop();
            }

            if (GUILayout.Button("open animation window"))
            {
                AnimationWindow[] animWindow = Resources.FindObjectsOfTypeAll(typeof(AnimationWindow)) as AnimationWindow[];
                AnimatorControllerLayer[] animLayers = (animPreviewPlayer.animator.runtimeAnimatorController as AnimatorController).layers;
                AnimatorState defState = animLayers[0].stateMachine.defaultState;

                EditorApplication.ExecuteMenuItem("Window/Animation/Animation");

                AnimationClip defStateClip = (AnimationClip)defState.motion;
                Selection.activeObject = defStateClip;
                EditorGUIUtility.PingObject(defStateClip);
                animWindow[0].animationClip = defStateClip;

                Selection.objects = new Object[] { animPreviewPlayer.gameObject };
                //animWindow[0].frame = 20;

                EditorCurveBinding[] bindings = AnimationUtility.GetCurveBindings(defStateClip);
                EditorCurveBinding binding = bindings[0];
                AnimationCurve curve = AnimationUtility.GetEditorCurve(defStateClip, binding);
            }

            DrawDefaultInspector();

            //serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
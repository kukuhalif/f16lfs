using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CreateAssetMenu(fileName = "new flightsim recorder setting", menuName = "Virtual Training/Flight Scenario/FlightSim Recorder Setting")]
    public class FlightSimRecorderSettingsAsset : ScriptableObjectBase<FlightSimRecorderSettingsData>
    {
        [SerializeField] FlightSimRecorderSettingsData data;

        public override FlightSimRecorderSettingsData GetData()
        {
            return data;
        }

        public override void SetData(FlightSimRecorderSettingsData data)
        {
            this.data = data;
        }
    }
}
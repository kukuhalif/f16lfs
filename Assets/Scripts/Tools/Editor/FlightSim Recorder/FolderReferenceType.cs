using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace VirtualTraining.Tools
{
    [Serializable]
    public class FolderReferenceType
    {
        [SerializeField] public DefaultAsset folder;
        [SerializeField] private string _path;
        //[SerializeField] public string guid;
        public string path
        {
            get
            {
                _path = AssetDatabase.GetAssetPath(folder);
                return _path;
            }
            set
            {
                _path = value;
                if (value == null)
                {
                    folder = null;
                }
                else
                {
                    folder = AssetDatabase.LoadAssetAtPath<DefaultAsset>(value);
                }
            }
        }
    }
}
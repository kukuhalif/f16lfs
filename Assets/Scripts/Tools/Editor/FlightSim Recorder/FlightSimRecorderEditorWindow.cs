using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.Timeline;
using UnityEditor.Callbacks;
using Mewlist.MassiveClouds;
using Oyedoyin.Common;
using Oyedoyin.FixedWing;
using HelicopterSim;
//using AirplaneSim;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using UnityEngine.Playables;

namespace VirtualTraining.Tools
{

    public class FlightSimRecorderEditorWindow : EditorWindowDatabaseBase<FlightSimRecorderSettingsAsset, FlightSimRecorderEditorWindow, FlightSimRecorderSettingsData>
    {
        //private static FlightSimRecorderEditorWindow window = null;

        EditorGUISplitView horizontalSplitView = new EditorGUISplitView(EditorGUISplitView.Direction.Horizontal, "hsplitter 1");
        EditorGUISplitView verticalSplitViewRight = new EditorGUISplitView(EditorGUISplitView.Direction.Vertical, "vsplitter 1");

        enum LoadedScene { Airplane, Helicopter, Other };
        LoadedScene loadedScene = LoadedScene.Other;

        FlightSimRecorder animationRecorder;
        int animationRecorderInstanceId;
        FlightScenarioPlayer flightScenarioPlayer;
        int flightScenarioPlayerInstanceId;

        FlightSimRecorderSettingsAsset recorderSettingsAsset;
        int settingIndex;
        int soInstanceID;

        /*[SerializeField] string saveFolderLocation = "Assets/Recordings";
        [SerializeField] FolderReferenceType saveFolderReference = new FolderReferenceType();
        [SerializeField] string recordingPrefix = "skenario";
        [SerializeField] [Range(1,60)] int skipFrame = 10;
        [SerializeField] List<FlightSimRecordingItem> animationItems = new List<FlightSimRecordingItem>();
        [SerializeField] AircraftType aircraftType = AircraftType.Helicopter;
        [SerializeField] AircraftMode aircraftMode = AircraftMode.Hot;
        [SerializeField] Vector2 aircraftLocation = new Vector2(0f, 0f);
        [SerializeField] float aircraftAltitude = 100f;
        [SerializeField] float aircraftSpeed = 100f;
        [SerializeField] float aircraftVerticalSpeed = 10f;
        [SerializeField] float aircraftHeading = 0f;

        [Range(0.0f, 24.0f)]
        [SerializeField] float atmosPosX24 = 12f;
        [Range(0.0f, 1.0f)]
        [SerializeField] float atmosPosY = 0.8f;

        [SerializeField] bool cityLightsStatus = false;
        */
        string saveFolderLocation;
        int selectedIndex;
        //[SerializeField] TimelineAsset scenarioTimeline;
        

        string playButtonText = "Prepare Playing";
        string recordButtonText = "Prepare Recording";
        string status = "Ready...";

        //SerializedObject serializedObject;

        Vector2 settingScrollPosition, folderScrollPosition, dataScrollPosition;
        bool selectionChange = false;
        bool showClipDataArray = true;
        enum EditorStatus { Ready, PreparedRecording, Recording, PreparedPlaying, Playing }
        EditorStatus editorStatus = EditorStatus.Ready;
        AircraftType currentAircraftType;

        FlightSimRecordingData animRecAsset;

        PropertyField propertyField;

        List<StepMarker> stepMarkers;
        int markerNum = 0;

        [MenuItem("Virtual Training/Flight Scenario/FlightSim Recorder")]
        public static void GetWindow()
        {
            FlightSimRecorderEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                //GUIContent recordIcon = EditorGUIUtility.IconContent("Animation.Record");
                //window.titleContent = new GUIContent("FlightSim Recorder", recordIcon.image);
                window.CheckLoadedScene();
                if (window.loadedScene != LoadedScene.Other)
                {
                    //window.selectedIndex = 0;
                    //serializedObject = new SerializedObject(this);
                    window.InitializeData();
                }
                else
                {
                    Debug.LogWarning("FlightSim Recording Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
                }
            }
        }

        public static FlightSimRecorderEditorWindow GetWindow(int instanceID)
        {
            FlightSimRecorderEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                GUIContent recordIcon = EditorGUIUtility.IconContent("Animation.Record");
                window.titleContent = new GUIContent("FlightSim Recorder", recordIcon.image);
                window.soInstanceID = instanceID;
                window.CheckLoadedScene();
                if (window.loadedScene != LoadedScene.Other)
                {
                    //window.selectedIndex = 0;
                    //serializedObject = new SerializedObject(this);
                    window.InitializeData();
                }
                else
                {
                    Debug.LogWarning("FlightSim Recording Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
                }
            }
            return window;
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            FlightSimRecorderEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                //GUIContent recordIcon = EditorGUIUtility.IconContent("Animation.Record");
                //window.titleContent = new GUIContent("FlightSim Recorder", recordIcon.image);
                window.soInstanceID = instanceID;
                window.CheckLoadedScene();
                if (window.loadedScene != LoadedScene.Other)
                {
                    //selectedIndex = -1;
                    //serializedObject = new SerializedObject(this);
                    window.InitializeData();
                }
                else
                {
                    Debug.LogWarning("FlightSim Recording Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
                }
                return true;
            }
            return false;
        }

        /*static void Init()
        {
            AnimationRecorderEditor window = (AnimationRecorderEditor)EditorWindow.GetWindow(typeof(AnimationRecorderEditor), false, "Animation Recorder Editor", true);
            window.Show();

        */

        /*[MenuItem("Assets/Create/AnimationRecordData")]
        public static void CreateAsset()
        {
            ScriptableObjectUtility.CreateAsset<AnimationRecordData>();
        }*/

        private void Awake()
        {
            EditorApplication.hierarchyChanged += OnHierarchyWindowChanged;
        }

        private void OnDestroy()
        {
            EditorApplication.hierarchyChanged -= OnHierarchyWindowChanged;
        }

        private void OnHierarchyWindowChanged()
        {
            CheckLoadedScene();
            //InitializeData();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            propertyField = new PropertyField(null, "flightsim recorder");
            propertyField.CreateListView("Animation Items", null, null, AnimationItemElementCallback, null, null, false, typeof(GameObject));

            CheckLoadedScene();
            if (loadedScene != LoadedScene.Other)
            {
                //selectedIndex = -1;
                //serializedObject = new SerializedObject(this);
                InitializeData();
            }
            else
            {
                Debug.LogWarning("FlightSim Recording Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
            }
        }

        protected override void OnDisable()
        {

        }

        private object AnimationItemElementCallback(string listId, object element, int index)
        {
            FlightSimRecordingItem animationItem = element as FlightSimRecordingItem;
            
            GUILayout.BeginVertical();

            propertyField.ShowSimpleField("Animation Type", listId + index, ref animationItem.animationType, 150, 400);
            propertyField.ShowSimpleField("Animation Object", listId + index + 1, ref animationItem.animatedObject, 150, 400);

            GUILayout.EndVertical();

            return animationItem;
        }

        private void CheckLoadedScene()
        {
            // check loaded scene
            //isFlightSimSceneLoaded = (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("AirplaneSim") ||
            //    SceneManager.GetActiveScene() == SceneManager.GetSceneByName("HelicopterSim"));
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("AirplaneSim"))
                loadedScene = LoadedScene.Airplane;
            else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("HelicopterSim"))
                loadedScene = LoadedScene.Helicopter;
            else
                loadedScene = LoadedScene.Other;
        }

        /*private void OnFocus()
        {
            CheckLoadedScene();
        }

        public void AfterChangeMode()
        {
            Debug.LogWarning("Masuk AfterChangeMode FlightSimRecorderEditor");
            if (serializedObject == null)
            {
                serializedObject = new SerializedObject(this);
                InitializeData();
            }
        }

        public void BeforeChangeMode()
        {
            Debug.LogWarning("Masuk BeforeChangeMode FlightSimRecorderEditor");
            if (serializedObject != null)
            {
                serializedObject.Dispose();
            }
        }*/

        public void InitializeData()
        {
            if (loadedScene != LoadedScene.Other)
            {
                animationRecorder = FindObjectOfType<FlightSimRecorder>(true);
                animationRecorderInstanceId = animationRecorder.GetInstanceID();

                flightScenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);
                flightScenarioPlayerInstanceId = flightScenarioPlayer.GetInstanceID();

                selectedIndex = 0;
                settingIndex = -1;
            }
        }

        private void OnValidate()
        {
            if (loadedScene != LoadedScene.Other)
            {
                FlightSimRecorderSettingsData data = ScriptableObjectTemp.GetData();
                // Check Aircraft Type changes..
                if (currentAircraftType != data.aircraftType)
                {
                    if (data.aircraftType == AircraftType.Helicopter)
                    {
                        Debug.Log("Change aircraft to Helicopter");
                        HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                        if (helicopterController == null)
                        {
                            Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                            status = "ERROR: Gameobject with Helicopter Controller (Rotary Wing) component not found!";
                            data.aircraftType = currentAircraftType;
                            return;
                        }
                    }
                    else if (data.aircraftType == AircraftType.Airplane)
                    {
                        Debug.Log("Change aircraft to Airplane");
                        FixedController airplaneController = FindObjectOfType<FixedController>(true);
                        if (airplaneController == null)
                        {
                            Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                            status = "ERROR: Gameobject with Silantro Controller (Fixed Wing) component not found!";
                            data.aircraftType = currentAircraftType;
                            return;
                        }
                    }
                    currentAircraftType = data.aircraftType;
                }
            }
        }

        //void OnGUI()
        protected override void RenderGUI()
        {
            if (loadedScene != LoadedScene.Other)
            {
                FlightSimRecorderSettingsData data = ScriptableObjectTemp.GetData();

                selectionChange = false;

                //serializedObject.Update();

                //EditorGUI.BeginChangeCheck();

                var backgroundColor = GUI.backgroundColor;

                animationRecorder = (FlightSimRecorder)EditorUtility.InstanceIDToObject(animationRecorderInstanceId);
                flightScenarioPlayer = (FlightScenarioPlayer)EditorUtility.InstanceIDToObject(flightScenarioPlayerInstanceId);
                if (animationRecorder == null || flightScenarioPlayer == null)
                {
                    // recheck loaded scene
                    CheckLoadedScene();
                    InitializeData();
                }


                EditorGUILayout.BeginVertical();

                // ====================================== BEGIN SPLIT VIEW
                horizontalSplitView.BeginSplitView(0f, position.height - 25);
                #region LEFTPANEL

                EditorGUILayout.BeginVertical();

                settingScrollPosition = EditorGUILayout.BeginScrollView(settingScrollPosition, false, false, GUIStyle.none, GUI.skin.verticalScrollbar, GUIStyle.none);

                // -------------------------------------FLIGHTSIM RECORDING SETTINGS

                GUI.color = Color.cyan;
                EditorGUILayout.HelpBox("FlightSim Recorder Settings", MessageType.None);
                GUI.color = backgroundColor;

                GUILayout.Space(5f);
                propertyField.ShowSimpleField("Save Folder Location", ref data.saveFolderReference);
                if (data.saveFolderReference != null)
                    saveFolderLocation = data.saveFolderReference.path;
                else
                    saveFolderLocation = "";
                EditorGUILayout.LabelField(" ", saveFolderLocation);

                GUILayout.Space(5f);
                propertyField.ShowSimpleField("Recording Prefix Name", ref data.recordingPrefix);

                GUILayout.Space(5f);
                propertyField.ShowSimpleField("Records every n frame", ref data.skipFrames);
                                
                GUILayout.Space(5f);
                propertyField.ShowListField("Animation Items", data.animationItems);

                // -------------------------------------AIRCRAFT INITIALIZATION SETTINGS

                GUILayout.Space(3f);
                GUI.color = Color.cyan;
                EditorGUILayout.HelpBox("Aircraft Initialization Settings", MessageType.None);
                GUI.color = backgroundColor;

                //propertyField.ShowSimpleField("Type", ref data.aircraftType);
                if (loadedScene == LoadedScene.Airplane)
                    data.aircraftType = AircraftType.Airplane;
                else if (loadedScene == LoadedScene.Helicopter)
                    data.aircraftType = AircraftType.Helicopter;
                EditorGUILayout.LabelField("Type", data.aircraftType.ToString());
                propertyField.ShowSimpleField("Start Mode", ref data.aircraftMode);
                propertyField.ShowSimpleField("Start Location X (m)", ref data.aircraftLocation.x);
                propertyField.ShowSimpleField("Start Location Y (m)", ref data.aircraftLocation.y);
                //EditorGUILayout.PropertyField(serializedObject.FindProperty("aircraftLocation"), new GUIContent("Start Location (m)"));
                propertyField.ShowSimpleField("Start Heading (�)", ref data.aircraftHeading);
                if (data.aircraftMode == AircraftMode.Flying)
                {
                    propertyField.ShowSimpleField("Start Altitude (m)", ref data.aircraftAltitude);
                    propertyField.ShowSimpleField("Start Horz Speed (m/s)", ref data.aircraftSpeed);
                    propertyField.ShowSimpleField("Start Vert Speed (m/s)", ref data.aircraftVerticalSpeed);
                }

                EditorGUILayout.BeginHorizontal();

                if (GUILayout.Button("Get Aircraft Init Transform From Scene", GUILayout.Height(30), GUILayout.Width(250)))
                {
                    GetAircraftSceneSettings(data);
                }

                if (GUILayout.Button("Reset Aircraft Init Settings", GUILayout.Height(30), GUILayout.Width(250)))
                {
                    ResetAircraftInitSettings(data);
                }

                EditorGUILayout.EndHorizontal();
                // -------------------------------------ENVIRONMENT SETTINGS

                GUILayout.Space(3f);
                GUI.color = Color.cyan;
                EditorGUILayout.HelpBox("Enviroment Settings", MessageType.None);
                GUI.color = backgroundColor;

                propertyField.ShowSlider("Time (00.00 > 24:00)", ref data.environmentTime, 0, 24);
                propertyField.ShowSlider("Clouds (Clear > Cloudy)", ref data.environmentCloud, 0, 1);
                propertyField.ShowSimpleField("Enable City Lights", ref data.cityLightsStatus);

                RunwayLights cityLights = FindObjectOfType<RunwayLights>(true);
                if (cityLights != null)
                    cityLights.gameObject.SetActive(data.cityLightsStatus);

                EditorGUILayout.BeginHorizontal();

                if (GUILayout.Button("Get Clouds Settings from Scene", GUILayout.Height(30), GUILayout.Width(250)))
                {
                    // get environment from scene data
                    AtmosPad atmosPad = FindObjectOfType<AtmosPad>(true);
                    if (atmosPad != null)
                    {
                        data.environmentTime = atmosPad.Pointer.x * 24;
                        data.environmentCloud = 1 - atmosPad.Pointer.y;
                    }
                }

                if (GUILayout.Button("Set Clouds Settings to Scene", GUILayout.Height(30), GUILayout.Width(250))) 
                {
                    // set environment from input
                    AtmosPad atmosPad = FindObjectOfType<AtmosPad>(true);
                    if (atmosPad != null)
                    {
                        EditorGUIUtility.PingObject(atmosPad);
                        Selection.objects = new UnityEngine.Object[] { atmosPad };
                        float atmosPosXNormalized = data.environmentTime / 24f;
                        float atmosPosYNormalized = 1 - data.environmentCloud;
                        atmosPad.SetPointer(new Vector2(atmosPosXNormalized, atmosPosYNormalized));
                    }
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.EndScrollView();

                GUILayout.FlexibleSpace();

                // -------------------------------------SET RECORDING DATA TO ANIMATION RECORDER
                SendDataToRecorder(data);

                // -------------------------------------LOAD/SAVE RECORDING SETTINGS

                GUILayout.Space(3f);
                GUILayout.BeginHorizontal();

                // display current recorder setting asset file
                //var currentSettingAsset = scriptableObjectOriginal.GetInstanceID();

                // get recorder settings assets at recording folder location
                //recorderSettingsAsset = EditorGUILayout.ObjectField("Recorder Settings File", recorderSettingsAsset, typeof(FlightSimRecorderSettingsAsset), false) as FlightSimRecorderSettingsAsset;
                string[] settingGuids = null;
                List<string> settingNames = new List<string>();
                string[] settingFolderLocations = new string[] { "Assets/Content Files/Recordings/" };
                settingGuids = AssetDatabase.FindAssets("t:FlightSimRecorderSettingsAsset", settingFolderLocations);

                for (int i = 0; i < settingGuids.Length; i++)
                {
                    var settingAsset = AssetDatabase.GUIDToAssetPath(settingGuids[i]);
                    settingNames.Add(Path.GetFileName(settingAsset));
                }

                settingIndex = EditorGUILayout.Popup("Open Settings From File", settingIndex, settingNames.ToArray());

                if (GUILayout.Button("Load File", GUILayout.Width(100)))
                {                    
                    int instanceID = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(settingGuids[settingIndex]), typeof(FlightSimRecorderSettingsAsset)).GetInstanceID();
                    OnOpenAsset(instanceID, 0);
                    settingIndex = -1;
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(3f);
                GUILayout.BeginHorizontal();
                GUIContent loadIcon = EditorGUIUtility.IconContent("d_scrollup@2x");
                if (GUILayout.Button(new GUIContent("Load Recording Settings", loadIcon.image, "Load recording settings from an asset file"), GUILayout.Height(30)))
                {
                    //LoadRecordingSettings();
                    Load();
                }

                GUILayout.Space(5f);

                GUIContent saveIcon = EditorGUIUtility.IconContent("d_scrolldown@2x");
                if (GUILayout.Button(new GUIContent("Save Recording Settings", saveIcon.image, "Save recording settings as an asset file"), GUILayout.Height(30)))
                {
                    //SaveRecordingSettings();
                    Save();
                }
                GUILayout.EndHorizontal();

                //verticalSplitViewLeft.EndSplitView();

                EditorGUILayout.EndVertical();

                //GUILayout.Space(20f);
                //GUILayout.FlexibleSpace();
                #endregion LEFT_PANEL

                // ====================================== HORIZONTAL SPLITTER
                horizontalSplitView.Split();

                #region RIGHT_PANEL

                EditorGUILayout.BeginVertical();

                verticalSplitViewRight.BeginSplitView(0f, position.width);                

                // -------------------------------------ANIMATION RECORD LIST

                GUI.color = Color.cyan;
                EditorGUILayout.HelpBox("FlightSim Recordings", MessageType.None);
                GUI.color = backgroundColor;

                GUILayout.Space(3f);
                EditorGUILayout.LabelField(" Select flight simulation recording data asset to play...");
                GUILayout.Space(5f);

                // create animation record list
                //var animRecList = serializedObject.FindProperty("animationRecords");
                //EditorGUILayout.PropertyField(animRecList, new GUIContent("Animation Record List"));

                // get subfolders of save folder location
                string[] animFolders = null;
                if (saveFolderLocation.Length > 0)
                {
                    animFolders = AssetDatabase.GetSubFolders(saveFolderLocation);
                }

                // get recording assets at save folder location
                string[] assetGuids = null;
                string[] folderLocations = new string[] { saveFolderLocation };
                if (saveFolderLocation.Length > 0)
                {
                    assetGuids = AssetDatabase.FindAssets("t:FlightSimRecordingData", folderLocations);
                }

                //if (animFolders != null)
                if (assetGuids != null)
                {
                    //if (selectedIndex >= animFolders.Length)
                    if (selectedIndex >= assetGuids.Length)
                    {
                        selectionChange = true;
                        selectedIndex = -1;
                    }
                    //EditorGUILayout.Space(5f);
                    //EditorGUILayout.PropertyField(serializedObject.FindProperty("animFolders"), new GUIContent("Animation Folder List"));        

                    folderScrollPosition = EditorGUILayout.BeginScrollView(folderScrollPosition, false, true, GUIStyle.none, GUI.skin.verticalScrollbar, GUIStyle.none);

                    // button list style
                    GUIStyle itemStyle = new GUIStyle(GUI.skin.button);  //make a new GUIStyle

                    itemStyle.alignment = TextAnchor.MiddleLeft; //align text to the left
                    itemStyle.active.background = itemStyle.normal.background;  //gets rid of button click background style.
                    itemStyle.margin = new RectOffset(0, 0, 0, 0);

                    // Get selected item in list
                    //for (int i = 0; i < animFolders.Length; i++)
                    GUIContent recordIcon = EditorGUIUtility.IconContent("Animation.Record");
                    for (int i = 0; i < assetGuids.Length; i++)
                    {
                        GUI.backgroundColor = (selectedIndex == i) ? Color.yellow : Color.clear;

                        //var animFolder = animFolders[i];
                        var recordAsset = AssetDatabase.GUIDToAssetPath(assetGuids[i]);

                        //Rect lastRect = GUILayoutUtility.GetLastRect();
                        if (GUILayout.Button(new GUIContent(" " + Path.GetFileName(recordAsset), recordIcon.image), itemStyle))
                        {
                            selectionChange = true;
                            selectedIndex = i;
                            //Debug.Log("Selected index = " + i);
                            status = "Selected index = " + i;
                        }
                    }
                    GUI.backgroundColor = backgroundColor;

                    EditorGUILayout.EndScrollView();

                    GUILayout.Space(5f);

                    // ====================================== VERTICAL SPLITTER
                    verticalSplitViewRight.Split();

                    // -------------------------------------SELECTED ANIMATION RECORD DATA

                    GUI.color = Color.cyan;
                    EditorGUILayout.HelpBox("FlightSim Recording Data", MessageType.None);
                    GUI.color = backgroundColor;

                    if (selectedIndex >= 0)
                    {
                        var recordAsset = AssetDatabase.GUIDToAssetPath(assetGuids[selectedIndex]);

                        //animRecAsset = AssetDatabase.LoadAssetAtPath<FlightSimRecordingData>(assetPath);
                        animRecAsset = AssetDatabase.LoadAssetAtPath<FlightSimRecordingData>(recordAsset);
                        if (selectionChange)
                        {
                            EditorGUIUtility.PingObject(animRecAsset);
                        }

                        if (animRecAsset == null)
                        {
                            status = "Selected asset is not an animation record data asset. Null data found!";
                        }
                        else
                        {
                            dataScrollPosition = EditorGUILayout.BeginScrollView(dataScrollPosition, false, true, GUIStyle.none, GUI.skin.verticalScrollbar, GUIStyle.none);

                            // Display animation record data
                            EditorGUILayout.LabelField(" Asset Name", animRecAsset.recordingName);
                            //EditorGUILayout.LabelField(" Clip Prefix", animRecAsset.clipPrefix);
                            //EditorGUILayout.LabelField(" Timeline Asset", animRecAsset.timeline.ToString());
                            EditorGUILayout.ObjectField(" Recorded Timeline Asset", animRecAsset.timeline, typeof(TimelineAsset), false);
                            GUI.color = Color.yellow;
                            animRecAsset.scenarioTimeline = EditorGUILayout.ObjectField(" Scenario Timeline Asset", animRecAsset.scenarioTimeline, typeof(TimelineAsset), false) as TimelineAsset;
                            GUI.color = backgroundColor;
                            if ((animRecAsset.aircraftType == AircraftType.Helicopter && loadedScene == LoadedScene.Helicopter) ||
                                (animRecAsset.aircraftType == AircraftType.Airplane && loadedScene == LoadedScene.Airplane))
                            {
                                EditorGUILayout.LabelField(" Aircraft Type", animRecAsset.aircraftType.ToString());
                            }
                            else
                            {
                                // display warning status
                                GUI.color = Color.red;
                                GUIContent warnIcon = EditorGUIUtility.IconContent("console.warnicon");
                                EditorGUILayout.LabelField(new GUIContent(" Aircraft Type"), new GUIContent(animRecAsset.aircraftType.ToString() + " (Simulation scene does not match!)", warnIcon.image));
                                GUI.color = backgroundColor;
                            }

                            showClipDataArray = EditorGUILayout.Foldout(showClipDataArray, new GUIContent(" Clip Data"));
                            if (showClipDataArray)
                            {
                                EditorGUI.indentLevel++;
                                var elmt = 0;
                                foreach (ClipData clipData in animRecAsset.clipDatas)
                                {
                                    EditorGUILayout.LabelField("Clip " + elmt); //, clipData.clipName);
                                    EditorGUI.indentLevel++;
                                    EditorGUILayout.LabelField("Clip Name", clipData.clipName);
                                    EditorGUILayout.LabelField("Clip Object Id", clipData.clipObject.Id.ToString());
                                    if (clipData.clipObject.gameObject != null)
                                        EditorGUILayout.LabelField("Clip Object Name", clipData.clipObject.gameObject.name.ToString());
                                    EditorGUILayout.LabelField("Clip Animation Type", clipData.clipType.ToString());
                                    EditorGUILayout.LabelField("Clip Animation Asset", clipData.clipAnim.ToString());
                                    EditorGUI.indentLevel--;
                                    elmt++;
                                }
                                EditorGUI.indentLevel--;
                            }

                            EditorGUILayout.LabelField(" Aircraft Mode", animRecAsset.aircraftMode.ToString());
                            EditorGUILayout.LabelField(" Aircraft Location", animRecAsset.aircraftLocation.ToString());
                            EditorGUILayout.LabelField(" Aircraft Altitude (m)", animRecAsset.aircraftAltitude.ToString());
                            EditorGUILayout.LabelField(" Aircraft Speed (m/s)", animRecAsset.aircraftSpeed.ToString());
                            EditorGUILayout.LabelField(" Aircraft Vertical Speed (m/s)", animRecAsset.aircraftVerticalSpeed.ToString());
                            EditorGUILayout.LabelField(" Aircraft Heading (�)", animRecAsset.aircraftHeading.ToString());
                            EditorGUILayout.LabelField(" Environment Time (00.00 - 24.00)", animRecAsset.environmentTime.ToString());
                            EditorGUILayout.LabelField(" Environment Cloud", animRecAsset.environmentCloud.ToString());
                            EditorGUILayout.LabelField(" City Lights Enabled", animRecAsset.cityLightsStatus.ToString());

                            GUILayout.Space(10f);
                            EditorGUILayout.HelpBox("Edit Data", MessageType.None);

                            animRecAsset.aircraftMode = (AircraftMode)EditorGUILayout.EnumPopup(" Aircraft Mode", animRecAsset.aircraftMode);
                            animRecAsset.aircraftLocation = EditorGUILayout.Vector2Field(" Aircraft Location", animRecAsset.aircraftLocation);
                            animRecAsset.aircraftAltitude = EditorGUILayout.FloatField(" Aircraft Altitude (m)", animRecAsset.aircraftAltitude);
                            animRecAsset.aircraftSpeed = EditorGUILayout.FloatField(" Aircraft Speed (m/s)", animRecAsset.aircraftSpeed);
                            animRecAsset.aircraftVerticalSpeed = EditorGUILayout.FloatField(" Aircraft Vertical Speed (m/s)", animRecAsset.aircraftVerticalSpeed);
                            animRecAsset.aircraftHeading = EditorGUILayout.FloatField(" Aircraft Heading (�)", animRecAsset.aircraftHeading);
                            animRecAsset.environmentTime = EditorGUILayout.FloatField(" Env Time (00.00 - 24.00)", animRecAsset.environmentTime);
                            animRecAsset.environmentCloud = EditorGUILayout.FloatField(" Env Cloud (0 - 1)", animRecAsset.environmentCloud);
                            animRecAsset.cityLightsStatus = EditorGUILayout.Toggle(" City Lights Enabled", animRecAsset.cityLightsStatus);

                            EditorGUILayout.EndScrollView();
                        }
                    }

                    GUILayout.FlexibleSpace();
                    GUILayout.Space(3f);

                    // **** Timeline control buttons ****
                    GUILayout.BeginHorizontal();

                    if (editorStatus == EditorStatus.Recording) GUI.enabled = false;
                    if (GUILayout.Button(new GUIContent(" Rewind", EditorGUIUtility.IconContent("Animation.FirstKey").image), GUILayout.Height(30)))
                    {
                        if (flightScenarioPlayer.playableDirector.state == PlayState.Paused)
                            flightScenarioPlayer.playableDirector.time = 0f;
                        else
                        {
                            flightScenarioPlayer.playableDirector.Pause();
                            flightScenarioPlayer.playableDirector.time = 0f;
                            flightScenarioPlayer.playableDirector.Resume();
                            flightScenarioPlayer.aircraftController.Pause(false);
                        }
                        TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                    }
                    if (GUILayout.Button(new GUIContent(" Play", EditorGUIUtility.IconContent("Animation.Play").image), GUILayout.Height(30)))
                    {
                        flightScenarioPlayer.playableDirector.Resume();
                        flightScenarioPlayer.aircraftController.Pause(false);
                        TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                    }
                    if (GUILayout.Button(new GUIContent(" Pause", EditorGUIUtility.IconContent("PauseButton").image), GUILayout.Height(30)))
                    {
                        flightScenarioPlayer.playableDirector.Pause();
                        flightScenarioPlayer.aircraftController.Pause(true);
                        TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                    }

                    if (editorStatus == EditorStatus.Recording)
                        GUI.enabled = false;
                    else if (stepMarkers == null)
                        GUI.enabled = false;
                    else if (stepMarkers.Count <= 0)
                        GUI.enabled = false;

                    if (GUILayout.Button(new GUIContent(" Previous Marker", EditorGUIUtility.IconContent("Animation.PrevKey").image), GUILayout.Height(30)))
                    {
                        markerNum--;
                        if (markerNum < 0) markerNum = 0;
                        if (flightScenarioPlayer.playableDirector.state == PlayState.Paused)
                            flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                        else
                        {
                            flightScenarioPlayer.playableDirector.Pause();
                            flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                            flightScenarioPlayer.playableDirector.Resume();
                            flightScenarioPlayer.aircraftController.Pause(false);
                        }
                        TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                    }
                    if (GUILayout.Button(new GUIContent(" Next Marker", EditorGUIUtility.IconContent("Animation.NextKey").image), GUILayout.Height(30)))
                    {
                        markerNum++;
                        if (markerNum >= stepMarkers.Count) markerNum = stepMarkers.Count - 1;
                        if (flightScenarioPlayer.playableDirector.state == PlayState.Paused)
                            flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                        else
                        {
                            flightScenarioPlayer.playableDirector.Pause();
                            flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                            flightScenarioPlayer.playableDirector.Resume();
                            flightScenarioPlayer.aircraftController.Pause(false);
                        }
                        TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                    }
                    GUI.enabled = true;

                    GUILayout.EndHorizontal();
                    // **************

                    GUILayout.Space(3f);
                    // -------------------------------------PLAYING BUTTON

                    if (editorStatus == EditorStatus.Recording || animRecAsset == null) // Play Button is inactive when recording
                        GUI.enabled = false;

                    GUIContent playIcon = EditorGUIUtility.IconContent("Animation.Play");
                    if (GUILayout.Button(new GUIContent(playButtonText, playIcon.image, "Prepare objects and settings to enable playing"), GUILayout.Height(30))) // Play Button On click 
                    {
                        // get FlightSimRecordingPlayer
                        FlightSimRecordingPlayer animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>(true);
                        // get FlightScenarioPlayer
                        FlightScenarioPlayer scenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);

                        switch (editorStatus)
                        {
                            case EditorStatus.Ready:
                            case EditorStatus.PreparedRecording:

                                PreparePlaying(animationRecorder, animationPlayer, scenarioPlayer, animRecAsset);
                                ReloadTimeline(animRecAsset.scenarioTimeline, scenarioPlayer.playableDirector);

                                if (animRecAsset != null)
                                    if (animRecAsset.scenarioTimeline != null)
                                        if (animRecAsset.scenarioTimeline.markerTrack != null)
                                            stepMarkers = animRecAsset.scenarioTimeline.markerTrack.GetMarkers().OfType<StepMarker>().ToList();

                                status = "Settings for playing has been enabled. Ready to Start Playing...";
                                playButtonText = " Start Playing";
                                editorStatus = EditorStatus.PreparedPlaying;
                                recordButtonText = " Prepare Recording";
                                break;

                            case EditorStatus.PreparedPlaying:
                                status = "Playing...";
                                playButtonText = " Stop Playing";
                                editorStatus = EditorStatus.Playing;

                                EditorApplication.isPlaying = true;

                                scenarioPlayer.playableDirector.Play();
                                break;

                            case EditorStatus.Playing:
                                EditorApplication.isPlaying = false;

                                ResetRecordingPlaying(animationRecorder, animationPlayer, scenarioPlayer, data);

                                status = "Playing has been stopped...";
                                playButtonText = " Prepare Playing";
                                editorStatus = EditorStatus.Ready;

                                break;
                        }
                    }
                    GUI.enabled = true;
                    GUILayout.Space(3f);

                    // -------------------------------------RECORDING BUTTON

                    if (editorStatus == EditorStatus.Playing) GUI.enabled = false; // Record Button is inactive when playing

                    if (GUILayout.Button(new GUIContent(recordButtonText, recordIcon.image, "Prepare objects and settings to enable recording (will enter Play Mode)"), GUILayout.Height(30))) // Record Button On click 
                    {
                        // get FlightSimRecorder
                        //FlightSimRecorder animationRecorder = FindObjectOfType<FlightSimRecorder>(true);
                        // get FlightSimRecordingPlayer
                        FlightSimRecordingPlayer animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>(true);
                        // get FlightScenarioPlayer
                        FlightScenarioPlayer scenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);

                        switch (editorStatus)
                        {
                            case EditorStatus.Ready:
                            case EditorStatus.PreparedPlaying:

                                PrepareRecording(animationRecorder, animationPlayer, scenarioPlayer, data);
                                EditorApplication.isPlaying = true; // workaround: Run the game when Prepare Recording

                                status = "Settings for recording has been enabled. Ready to Start Recording...";
                                recordButtonText = " Start Recording";
                                editorStatus = EditorStatus.PreparedRecording;
                                playButtonText = " Prepare Playing";
                                break;

                            case EditorStatus.PreparedRecording:
                                status = "Recording...";
                                recordButtonText = " Stop Recording";
                                editorStatus = EditorStatus.Recording;

                                animationRecorder._canRecord = true;

                                if (EditorApplication.isPlaying)
                                {
                                    Debug.Log("Start Recording when game is running.");
                                    //animationsRecorder._canRecord = true;
                                    animationRecorder.StartRecording(); // explicitly call animationRecorder.StartRecording
                                }
                                else
                                {   // --- NOT WORKING ??!!!
                                    Debug.Log("Start Recording when game has not running.");
                                    //animationsRecorder._canRecord = true; // --> to call animationRecorder.StartRecording at animationRecorder.Start()
                                    EditorApplication.isPlaying = true;
                                }

                                break;

                            case EditorStatus.Recording:

                                animationRecorder.StopRecording();
                                EditorApplication.isPlaying = false;

                                ResetRecordingPlaying(animationRecorder, animationPlayer, scenarioPlayer, data);

                                status = "Recording has been stopped.";
                                recordButtonText = " Prepare Recording";
                                editorStatus = EditorStatus.Ready;

                                break;
                        }
                    }
                    GUI.enabled = true;
                    GUILayout.Space(3f);

                    // -------------------------------------RESET BUTTON (Back to Simulation, no Playing/Recording)

                    GUIContent resetIcon = EditorGUIUtility.IconContent("Refresh");
                    if (GUILayout.Button(new GUIContent(" Reset Recording/Playing (back to Simulation Mode)", resetIcon.image, "Reset objects and setting to disable playing and recording (back to simulation)"), GUILayout.Height(30))) // Reset Button On click 
                    {
                        // get FlightSimRecorder
                        //FlightSimRecorder animationRecorder = FindObjectOfType<FlightSimRecorder>(true);
                        // get FlightSimRecordingPlayer
                        FlightSimRecordingPlayer animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>(true);
                        // get FlightScenarioPlayer
                        FlightScenarioPlayer scenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);

                        ResetRecordingPlaying(animationRecorder, animationPlayer, scenarioPlayer, data);

                        status = "Settings for Recording and/or Playing has been reset/disabled.";
                        recordButtonText = " Prepare Recording";
                        playButtonText = " Prepare Playing";
                        editorStatus = EditorStatus.Ready;
                    }
                }

                verticalSplitViewRight.EndSplitView();

                EditorGUILayout.EndVertical();

                #endregion RIGHT_PANEL

                horizontalSplitView.EndSplitView();

                // ====================================== END SPLIT VIEW

                //GUI.backgroundColor = Color.red; // new Color(0.2f, 0.2f, 0.2f);
                GUILayout.Label("Status: " + status);

                EditorGUILayout.EndVertical();

                //if (EditorGUI.EndChangeCheck())
                //    serializedObject.ApplyModifiedProperties();
            }
            else
            {
                GUI.color = Color.yellow;
                EditorGUILayout.HelpBox("Please load and activate AirplaneSim Scene or HelicopterSim Scene to use FlightSim Recorder!", MessageType.Warning);
            }
        }

        protected override void OnGUI()
        {
            if (loadedScene != LoadedScene.Other)
            {
                base.OnGUI();
            }
            else
            {
                GUI.color = Color.yellow;
                EditorGUILayout.HelpBox("Please load and activate AirplaneSim Scene or HelicopterSim Scene to use FlightSim Recorder!", MessageType.Warning);
            }
        }

        private void SendDataToRecorder(FlightSimRecorderSettingsData data)
        {
            // -------------------------------------SET RECORDING SETTING DATA TO ANIMATION RECORDER

            animationRecorder._saveFolderLocation = data.saveFolderReference.path;
            animationRecorder._recordingPrefix = data.recordingPrefix;
            animationRecorder._skipFrame = data.skipFrames;
            animationRecorder._animationItems = data.animationItems;

            animationRecorder._aircraftType = data.aircraftType;
            animationRecorder._aircraftMode = data.aircraftMode;
            animationRecorder._aircraftLocation = data.aircraftLocation;
            animationRecorder._aircraftAltitude = data.aircraftAltitude;
            animationRecorder._aircraftSpeed = data.aircraftSpeed;
            animationRecorder._aircraftVerticalSpeed = data.aircraftVerticalSpeed;
            animationRecorder._aircraftHeading = data.aircraftHeading;

            animationRecorder._environmentTime = data.environmentTime;
            animationRecorder._environmentCloud = data.environmentCloud;
            animationRecorder._cityLightsStatus = data.cityLightsStatus;

            animationRecorder._selectedIndex = selectedIndex;
            //animationRecorder._scenarioTimeline = scenarioTimeline;
        }

        // Prepare objects and settings to enable playing
        private void PreparePlaying(FlightSimRecorder animationRecorder, FlightSimRecordingPlayer animationPlayer, FlightScenarioPlayer scenarioPlayer, FlightSimRecordingData animRecAsset)
        {
            // disable animation recorder
            animationRecorder.gameObject.SetActive(false);
            animationRecorder.enabled = false;
            
            // enable animation player
            animationPlayer.gameObject.SetActive(true);
            animationPlayer.enabled = true;
            animationPlayer.playableDirector.enabled = true;
            // enable scenario player
            scenarioPlayer.gameObject.SetActive(true);
            scenarioPlayer.enabled = true;
            scenarioPlayer.playableDirector.enabled = true;

            if (animRecAsset.aircraftType == AircraftType.Helicopter)
            {
                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController == null)
                {
                    Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                    return;
                }

                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController != null)
                    airplaneController.gameObject.SetActive(false);

                //helicopterController.enabled = false;
                helicopterController.gameObject.SetActive(true);
                helicopterController.m_playMode = HelicopterController.PlayMode.Animation;
                helicopterController.m_startMode = (HelicopterController.StartMode)animRecAsset.aircraftMode; // as HelicopterController.StartMode;

                // enable kinematic (disable physics) on helicopter
                Rigidbody rb = helicopterController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = true;
                }

                // disable lever components to prevent conflicts with timeline player
                HelicopterLever[] levers = helicopterController.gameObject.GetComponentsInChildren<HelicopterLever>();
                foreach (HelicopterLever lever in levers)
                {
                    lever.enabled = false;
                }

                /*// disable dial components with animator movement type to prevent conflicts with timeline player
                HelicopterDial[] dials = helicopterController.gameObject.GetComponentsInChildren<HelicopterDial>();
                foreach (HelicopterDial dial in dials)
                {
                    if (dial.movementType == HelicopterDial.MovementType.Animator)
                        dial.enabled = false;
                }*/

            }
            else if (animRecAsset.aircraftType == AircraftType.Airplane)
            {
                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController == null)
                {
                    Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                    return;
                }

                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController != null)
                    helicopterController.gameObject.SetActive(false);

                //airplaneController.enabled = false;
                airplaneController.gameObject.SetActive(true);
                airplaneController.m_playMode = FixedController.PlayMode.Animation;
                airplaneController.m_startMode = (FixedController.StartMode)animRecAsset.aircraftMode; // as AirplaneController.StartMode;

                // enable kinematic (disable physics) on airplane
                Rigidbody rb = airplaneController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = true;
                }
                // disable lever components to prevent conflicts with timeline player
                SilantroLever[] levers = airplaneController.gameObject.GetComponentsInChildren<SilantroLever>();
                foreach (SilantroLever lever in levers)
                {
                    lever.enabled = false;
                }

                /*// disable dial components with animator movement type to prevent conflicts with timeline player
                SilantroDial[] dials = airplaneController.gameObject.GetComponentsInChildren<SilantroDial>();
                foreach (SilantroDial dial in dials)
                {
                    if (dial.movementType == SilantroDial.MovementType.Animator)
                        dial.enabled = false;
                }*/

            }

            // setup environment from recorded data
            float atmosPosXNormalized = animRecAsset.environmentTime / 24f;
            float atmosPosYNormalized = 1 - animRecAsset.environmentCloud;

            AtmosPad atmosPad = FindObjectOfType<AtmosPad>(true);
            if (atmosPad != null)
            {
                atmosPad.SetPointer(new Vector2(atmosPosXNormalized, atmosPosYNormalized));
            }
            
            RunwayLights cityLights = FindObjectOfType<RunwayLights>(true);
            if (cityLights != null)
            {
                cityLights.gameObject.SetActive(animRecAsset.cityLightsStatus);
            }

            // load timelineAsset
            TimelineAsset timeline = animRecAsset.timeline;

            // set playableDirector to loaded timelineAsset
            animationPlayer.playableDirector.playableAsset = timeline;

            // loop on timeline output tracks
            int animTrackIdx = 0;
            for (int trackIdx = 0; trackIdx < timeline.outputTrackCount; trackIdx++)
            {
                // get timeline track
                TrackAsset track = timeline.GetOutputTrack(trackIdx);
                // check if timeline track type is animation track
                if (track.GetType() == typeof(AnimationTrack))
                {
                    // get clip data
                    ClipData clipData = animRecAsset.clipDatas[animTrackIdx];

                    // get gameobject reference
                    GameObject go = clipData.clipObject.gameObject;

                    if (go != null)
                    {
                        // make sure gameobject is active
                        go.SetActive(true);

                        // get gameobject animator component
                        Animator animator = go.GetComponent<Animator>();
                        if (animator == null)
                        {
                            Debug.Log("Create Animator...");
                            // add Animator component if not exist
                            animator = go.AddComponent(typeof(Animator)) as Animator;
                        }
                        // make sure animator component is active
                        animator.enabled = true;
                        // bind gameobject with animator component
                        animationPlayer.playableDirector.SetGenericBinding(track, go);

                        // set Animator Recording Helper status to Play Animation
                        if (clipData.clipType == FlightSimRecordingItem.FlightSimRecordingType.AnimatorParameter)
                        {
                            AnimatorRecordingHelper animParamController = go.GetComponent<AnimatorRecordingHelper>();
                            if (animParamController != null)
                            {
                                animParamController.enabled = true;
                                animParamController.helperMode = AnimatorRecordingHelper.HelperMode.PlayAnimation;
                            }
                            else
                            {
                                Debug.LogWarning("Animator Recording Helper component not found...");
                            }
                        }
                    }
                    animTrackIdx++;
                }
            }

            if (scenarioPlayer != null && animRecAsset.scenarioTimeline != null)
            {
                // set scenario playableDirector to scenario timelineAsset
                /*switch (scenarioOption)
                {
                    case ScenarioOption.New:
                        scenarioTimeline = CreateNewScenario(animRecAsset, animationPlayer, scenarioPlayer);
                        break;
                    case ScenarioOption.Default:
                        scenarioTimeline = AssetDatabase.LoadAssetAtPath<TimelineAsset>("Assets/Content Files/Scenarios/Default Scenario.playable");
                        break;
                    case ScenarioOption.Select:
                        break;
                }*/

                scenarioPlayer.playableDirector.playableAsset = animRecAsset.scenarioTimeline;
                // refresh timeline editor window
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);

                // loop on scenario timeline root tracks
                //int scenarioTrackIdx = 0;
                for (int trackIdx = 0; trackIdx < animRecAsset.scenarioTimeline.outputTrackCount; trackIdx++)
                {
                    // get timeline track
                    TrackAsset track = animRecAsset.scenarioTimeline.GetOutputTrack(trackIdx);

                    // check if timeline track type is signal track
                    if (track.GetType() == typeof(SignalTrack))
                    {
                        // bind signal track to scenarioPlayer signalReceiver
                        SignalReceiver signalReceiver = scenarioPlayer.gameObject.GetComponent<SignalReceiver>();
                        scenarioPlayer.playableDirector.SetGenericBinding(track, signalReceiver);
                    }
                    else if (track.GetType() == typeof(ControlTrack))
                    {
                        // get the first clip (assume there is only one control clip exist)
                        TimelineClip controlClip = track.GetClips().ElementAt(0);

                        if (controlClip != null)
                        {
                            // update control clip duration to match recorded timeline duration
                            controlClip.duration = animRecAsset.timeline.duration;
                        }
                        else
                        {
                            Debug.LogError("Scenario Timeline Control Clip not found!");
                        }
                    }
                }

                // play scenario
                //scenarioPlayer.playableDirector.Play();
            }
        }

        // Prepare objects and setting to enable recording
        private void PrepareRecording(FlightSimRecorder animationRecorder, FlightSimRecordingPlayer animationPlayer, FlightScenarioPlayer scenarioPlayer, FlightSimRecorderSettingsData data)
        {
            // SendDataToRecorder();

            // Enable animation recorder
            animationRecorder.gameObject.SetActive(true);
            animationRecorder.enabled = true;
            // Disable animation player
            animationPlayer.gameObject.SetActive(false);
            animationPlayer.enabled = false;
            // Disable scenario player
            scenarioPlayer.gameObject.SetActive(false);
            scenarioPlayer.enabled = false;

            // set aircraft initialization
            if (data.aircraftType == AircraftType.Helicopter)
            {
                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController == null)
                {
                    Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                    return;
                }

                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController != null)
                    airplaneController.gameObject.SetActive(false);

                //helicopterController.enabled = true;
                helicopterController.gameObject.SetActive(true);
                helicopterController.m_playMode = HelicopterController.PlayMode.Simulation;
                helicopterController.m_startMode = (HelicopterController.StartMode)data.aircraftMode; // as HelicopterController.StartMode;
                if (data.aircraftMode == AircraftMode.Flying)
                {
                    helicopterController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y);
                    helicopterController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    helicopterController.m_startSpeed = data.aircraftSpeed;
                    helicopterController.m_startAltitude = data.aircraftAltitude;
                    helicopterController.m_startClimbRate = data.aircraftVerticalSpeed;
                    helicopterController.m_startHeading = data.aircraftHeading;
                }
                else
                {
                    helicopterController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y);
                    helicopterController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    helicopterController.m_startSpeed = 0f;
                    helicopterController.m_startAltitude = 0f;
                    helicopterController.m_startClimbRate = 0f;
                    helicopterController.m_startHeading = data.aircraftHeading;
                }
                // disable kinematic (enable physics) on helicopter
                Rigidbody rb = helicopterController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                }
                // enable lever components to visualize flight control using animator
                HelicopterLever[] levers = helicopterController.gameObject.GetComponentsInChildren<HelicopterLever>();
                foreach (HelicopterLever lever in levers)
                {
                    lever.enabled = true;
                }
                /*// enable dial components 
                HelicopterDial[] dials = helicopterController.gameObject.GetComponentsInChildren<HelicopterDial>();
                foreach (HelicopterDial dial in dials)
                {
                    if (dial.movementType == HelicopterDial.MovementType.Animator) // || dial.movementType == HelicopterDial.MovementType.AnimationFrame)
                        dial.enabled = true;
                }*/

                // disable simulation reset
                helicopterController.EnableReset(false);

            }
            else if (data.aircraftType == AircraftType.Airplane)
            {
                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController == null)
                {
                    Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                    return;
                }

                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController != null)
                    helicopterController.gameObject.SetActive(false);

                //helicopterController.enabled = true;
                airplaneController.gameObject.SetActive(true);
                airplaneController.m_playMode = FixedController.PlayMode.Simulation;
                airplaneController.m_startMode = (FixedController.StartMode)data.aircraftMode; // as HelicopterController.StartMode;
                if (data.aircraftMode == AircraftMode.Flying)
                {
                    airplaneController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y); ;
                    airplaneController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    airplaneController.m_startSpeed = data.aircraftSpeed;
                    airplaneController.m_startAltitude = data.aircraftAltitude;
                    airplaneController.m_startClimbRate = data.aircraftVerticalSpeed;
                    airplaneController.m_startHeading = data.aircraftHeading;
                }
                else
                {
                    airplaneController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y); ;
                    airplaneController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    airplaneController.m_startSpeed = 0f;
                    airplaneController.m_startAltitude = 0f;
                    airplaneController.m_startClimbRate = 0f;
                    airplaneController.m_startHeading = data.aircraftHeading;
                }
                // disable kinematic (enable physics) on airplane
                Rigidbody rb = airplaneController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                }
                // enable lever components to visualize flight control using animator
                SilantroLever[] levers = airplaneController.gameObject.GetComponentsInChildren<SilantroLever>();
                foreach (SilantroLever lever in levers)
                {
                    lever.enabled = true;
                }

                // enable dial components
                /*SilantroDial[] dials = airplaneController.gameObject.GetComponentsInChildren<SilantroDial>();
                foreach (SilantroDial dial in dials)
                {
                    if (dial.movementType == SilantroDial.MovementType.Animator)
                        dial.enabled = true;
                }*/

                // disable simulation reset
                airplaneController.EnableReset(false);

            }

            // get environment from scene data
            AtmosPad atmosPad = FindObjectOfType<AtmosPad>(true);
            if (atmosPad != null)
            {
                data.environmentTime = atmosPad.Pointer.x * 24;
                data.environmentCloud = 1 - atmosPad.Pointer.y;
            }

            // set recording flag true for all animation item which type is Animation Parameter
            foreach (FlightSimRecordingItem animItem in data.animationItems)
            {
                // get Animated GameObject
                GameObject go = animItem.animatedObject.gameObject;
                if (go == null)
                {
                    Debug.LogError("Animated Gameobject is not found.");
                    continue;
                }
                // make sure gameobject is active
                go.SetActive(true);

                // get Animator component
                Animator animator = go.GetComponent<Animator>();
                if (animator == null)
                {
                    Debug.Log("Parameterized Animator is not found.");
                    continue;
                }
                // make sure animator component is active
                animator.enabled = true;

                // set Animator Recording Helper mode to Record Animation
                if (animItem.animationType == FlightSimRecordingItem.FlightSimRecordingType.AnimatorParameter)
                {
                    // get Animator Recording Helper
                    AnimatorRecordingHelper animParamRecorder = go.GetComponent<AnimatorRecordingHelper>();
                    if (animParamRecorder == null)
                    {
                        Debug.LogWarning("Animator Recording Helper component is not found...");
                        continue;
                    }
                    // make sure Animator Recording Helper is active and mode is Record Animation
                    animParamRecorder.enabled = true;
                    animParamRecorder.helperMode = AnimatorRecordingHelper.HelperMode.RecordAnimation;
                }
            }
        }

        // Reset objects and setting to disable playing and recording (back to simulation)
        private void ResetRecordingPlaying(FlightSimRecorder animationRecorder, FlightSimRecordingPlayer animationPlayer, FlightScenarioPlayer scenarioPlayer, FlightSimRecorderSettingsData data)
        {
            // SaveRecordData();

            // Disable animation recorder
            animationRecorder.gameObject.SetActive(false);
            animationRecorder.enabled = false;
            // Disable animation recorder
            animationPlayer.gameObject.SetActive(false);
            animationPlayer.enabled = false;
            // Disable scenario player
            scenarioPlayer.gameObject.SetActive(false);
            scenarioPlayer.enabled = false;

            // set aircraft initialization
            if (data.aircraftType == AircraftType.Helicopter)
            {
                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController == null)
                {
                    Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                    return;
                }

                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController != null)
                    airplaneController.gameObject.SetActive(false);

                helicopterController.gameObject.SetActive(true);
                helicopterController.m_playMode = HelicopterController.PlayMode.Simulation;
                helicopterController.m_startMode = (HelicopterController.StartMode)data.aircraftMode; // as HelicopterController.StartMode;
                if (data.aircraftMode == AircraftMode.Flying)
                {
                    helicopterController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y);
                    helicopterController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    helicopterController.m_startHeading = data.aircraftHeading;
                    helicopterController.m_startSpeed = data.aircraftSpeed;
                    helicopterController.m_startAltitude = data.aircraftAltitude;
                    helicopterController.m_startClimbRate = data.aircraftVerticalSpeed;
                }
                else
                {
                    helicopterController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y);
                    helicopterController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    helicopterController.m_startHeading = data.aircraftHeading;
                    helicopterController.m_startSpeed = 0f;
                    helicopterController.m_startAltitude = 0f;
                    helicopterController.m_startClimbRate = 0f;
                }

                // disable kinematic (enable physics) on aircraft controller
                Rigidbody rb = helicopterController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                }

                // enable lever components to visualize flight control using animator
                HelicopterLever[] levers = helicopterController.gameObject.GetComponentsInChildren<HelicopterLever>();
                foreach (HelicopterLever lever in levers)
                {
                    lever.enabled = true;
                }

                /*// enable dial components 
                HelicopterDial[] dials = helicopterController.gameObject.GetComponentsInChildren<HelicopterDial>();
                foreach (HelicopterDial dial in dials)
                {
                    if (dial.movementType == HelicopterDial.MovementType.Animator) // || dial.movementType == HelicopterDial.MovementType.AnimationFrame)
                        dial.enabled = true;
                }*/

                // enable simulation reset
                helicopterController.EnableReset(true);
            }
            else if (data.aircraftType == AircraftType.Airplane)
            {
                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController == null)
                {
                    Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                    return;
                }

                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController != null)
                    helicopterController.gameObject.SetActive(false);

                airplaneController.gameObject.SetActive(true);
                airplaneController.m_playMode = FixedController.PlayMode.Simulation;
                airplaneController.m_startMode = (FixedController.StartMode)data.aircraftMode; // as HelicopterController.StartMode;
                if (data.aircraftMode == AircraftMode.Flying)
                {
                    airplaneController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y); ;
                    airplaneController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    airplaneController.m_startHeading = data.aircraftHeading;
                    airplaneController.m_startSpeed = data.aircraftSpeed;
                    airplaneController.m_startAltitude = data.aircraftAltitude;
                    airplaneController.m_startClimbRate = data.aircraftVerticalSpeed;
                }
                else
                {
                    airplaneController.transform.position = new Vector3(data.aircraftLocation.x, 0, data.aircraftLocation.y); ;
                    airplaneController.transform.rotation = Quaternion.Euler(0, data.aircraftHeading, 0);
                    airplaneController.m_startHeading = data.aircraftHeading;
                    airplaneController.m_startSpeed = 0f;
                    airplaneController.m_startAltitude = 0f;
                    airplaneController.m_startClimbRate = 0f;
                }


                // disable kinematic (enable physics) on aircraft controller
                Rigidbody rb = airplaneController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                }

                // enable lever components to visualize flight control using animator
                SilantroLever[] levers = airplaneController.gameObject.GetComponentsInChildren<SilantroLever>();
                foreach (SilantroLever lever in levers)
                {
                    lever.enabled = true;
                }

                // enable dial components
                /*SilantroDial[] dials = airplaneController.gameObject.GetComponentsInChildren<SilantroDial>();
                foreach (SilantroDial dial in dials)
                {
                    if (dial.movementType == SilantroDial.MovementType.Animator)
                        dial.enabled = true;
                }*/

                // enable simulation reset
                airplaneController.EnableReset(true);
            }

            // disable animator recording helpers
            AnimatorRecordingHelper[] animRecordingHelpers = FindObjectsOfType<AnimatorRecordingHelper>(true);
            foreach (AnimatorRecordingHelper animRecordingHelper in animRecordingHelpers)
            {
                animRecordingHelper.enabled = false;
            }

        }

        private void GetAircraftSceneSettings(FlightSimRecorderSettingsData data)
        {
            // set aircraft initialization
            if (data.aircraftType == AircraftType.Helicopter)
            {
                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController == null)
                {
                    Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                    return;
                }

                data.aircraftLocation = new Vector2(helicopterController.transform.position.x, helicopterController.transform.position.z);
                data.aircraftAltitude = helicopterController.transform.position.y;
                data.aircraftHeading = Mathf.Atan2(helicopterController.transform.forward.x, helicopterController.transform.forward.z) * Mathf.Rad2Deg;
                data.aircraftHeading = data.aircraftHeading < 0 ? 360 + data.aircraftHeading : data.aircraftHeading;
            }
            else if (data.aircraftType == AircraftType.Airplane)
            {
                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController == null)
                {
                    Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                    return;
                }

                data.aircraftLocation = new Vector2(airplaneController.transform.position.x, airplaneController.transform.position.z);
                data.aircraftAltitude = airplaneController.transform.position.y;
                data.aircraftHeading = Mathf.Atan2(airplaneController.transform.forward.x, airplaneController.transform.forward.z) * Mathf.Rad2Deg;
            }
        }

        private void ResetAircraftInitSettings(FlightSimRecorderSettingsData data)
        {
            data.aircraftLocation = new Vector2(0f, 0f);
            data.aircraftHeading = 0f;
            data.aircraftSpeed = 0f;
            data.aircraftAltitude = 0f;
            data.aircraftVerticalSpeed = 0f;

            // set aircraft initialization
            if (data.aircraftType == AircraftType.Helicopter)
            {
                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController == null)
                {
                    Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                    return;
                }

                helicopterController.m_startMode = (HelicopterController.StartMode)data.aircraftMode; // as HelicopterController.StartMode;

                helicopterController.transform.position = new Vector3(0, 0, 0);
                helicopterController.transform.rotation = Quaternion.Euler(0, 0, 0);
                helicopterController.m_startHeading = 0f;
                helicopterController.m_startSpeed = 0f;
                helicopterController.m_startAltitude = 0f;
                helicopterController.m_startClimbRate = 0f;
            }
            else if (data.aircraftType == AircraftType.Airplane)
            {
                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController == null)
                {
                    Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                    return;
                }

                airplaneController.m_startMode = (FixedController.StartMode)data.aircraftMode; // as HelicopterController.StartMode;

                airplaneController.transform.position = new Vector3(0, 0, 0); ;
                airplaneController.transform.rotation = Quaternion.Euler(0, 0, 0);
                airplaneController.m_startHeading = 0f;
                airplaneController.m_startSpeed = 0f;
                airplaneController.m_startAltitude = 0f;
                airplaneController.m_startClimbRate = 0f;
            }
        }

        private void ReloadTimeline(TimelineAsset timeline, PlayableDirector playableDirector)
        {
            if (playableDirector != null)
            {
                Selection.activeGameObject = playableDirector.gameObject;
                if (timeline != null)
                {
                    // set selected timeline asset to playableDirector playableAsset
                    playableDirector.playableAsset = timeline;
                    TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                }
            }
        }

        /*private void SaveRecordingSettings()
        {
            string path = EditorUtility.SaveFilePanelInProject("Save animation recording settings to asset file", "animRecSetting", "asset", "Please enter file name to save settings to");
            if (path.Length != 0)
            {
                FlightSimRecorderSettingsAsset animRecSettingsAsset = ScriptableObject.CreateInstance<FlightSimRecorderSettingsAsset>();
                FlightSimRecorderSettingsData animRecSettingsData = new FlightSimRecorderSettingsData();

                // create animation recording settings data asset
                animRecSettingsData.saveFolderLocation = saveFolderLocation;
                animRecSettingsData.clipFolder = clipFolder;
                animRecSettingsData.clipPrefix = clipPrefix;
                animRecSettingsData.skipFrames = skipFrame;

                animRecSettingsData.animationItems = new List<FlightSimRecordingItem>();
                foreach (FlightSimRecordingItem animationItem in animationItems)
                {
                    //FlightSimRecordingItemSettingsData animItem = new FlightSimRecordingItemSettingsData(animationItem.animationType, animationItem.animatedObject.Id);
                    animRecSettingsData.animationItems.Add(animationItem);
                }

                animRecSettingsData.aircraftType = aircraftType;
                animRecSettingsData.aircraftMode = aircraftMode;
                animRecSettingsData.aircraftLocation = aircraftLocation;
                animRecSettingsData.aircraftAltitude = aircraftAltitude;
                animRecSettingsData.aircraftSpeed = aircraftSpeed;
                animRecSettingsData.aircraftVerticalSpeed = aircraftVerticalSpeed;
                animRecSettingsData.aircraftHeading = aircraftHeading;

                animRecSettingsData.environmentTime = atmosPosX24;
                animRecSettingsData.environmentCloud = atmosPosY;
                //animRecSettingsData.cityLightsObjectName = cityLights.gameObject.name;
                animRecSettingsData.cityLightsStatus = cityLightsStatus;

                // save recording setting data asset
                animRecSettingsAsset.SetData(animRecSettingsData);
                AssetDatabase.CreateAsset(animRecSettingsAsset, path);

                // save unsaved assets 
                AssetDatabase.SaveAssets();
            }
        }

        private void LoadRecordingSettings()
        {
            string[] filters = { "Setting asset files", "asset" };
            string path = EditorUtility.OpenFilePanelWithFilters("Load animation recording settings data", "Assets/Recordings", filters);
            if (path.Length != 0)
            {
                string relativePath = path;
                if (path.StartsWith(Application.dataPath))
                    relativePath = "Assets" + path.Substring(Application.dataPath.Length);
                Debug.Log("Load recording settings path: " + relativePath);
                FlightSimRecorderSettingsAsset animRecSettingsAsset = AssetDatabase.LoadAssetAtPath<FlightSimRecorderSettingsAsset>(relativePath);

                if (animRecSettingsAsset == null)
                {
                    //Debug.LogWarning("Selected folder is not an animation record folder. Animation data is not found!");
                    status = "Invalid animation recording setting data...!";
                }
                else
                {
                    // load animation recording settings data asset
                    saveFolderLocation = animRecSettingsAsset.GetData().saveFolderLocation;
                    clipFolder = animRecSettingsAsset.GetData().clipFolder;
                    clipPrefix = animRecSettingsAsset.GetData().clipPrefix;
                    skipFrame = animRecSettingsAsset.GetData().skipFrames;

                    animationItems = new List<FlightSimRecordingItem>();
                    foreach (FlightSimRecordingItem animationItem in animRecSettingsAsset.GetData().animationItems)
                    {
                        //GameObjectType gameObjectRef = GameObjectReference.GetReference(animationItem.animatedObjectId);
                        //FlightSimRecordingItem animItem = new FlightSimRecordingItem(animationItem.animationType, gameObjectRef);
                        animationItems.Add(animationItem);
                    }

                    aircraftType = animRecSettingsAsset.GetData().aircraftType;
                    aircraftMode = animRecSettingsAsset.GetData().aircraftMode;
                    aircraftLocation = animRecSettingsAsset.GetData().aircraftLocation;
                    aircraftAltitude = animRecSettingsAsset.GetData().aircraftAltitude;
                    aircraftSpeed = animRecSettingsAsset.GetData().aircraftSpeed;
                    aircraftVerticalSpeed = animRecSettingsAsset.GetData().aircraftVerticalSpeed;
                    aircraftHeading = animRecSettingsAsset.GetData().aircraftHeading;

                    atmosPosX24 = animRecSettingsAsset.GetData().environmentTime;
                    atmosPosY = animRecSettingsAsset.GetData().environmentCloud;
                    //RunwayLights runwayLights = FindObjectOfType<RunwayLights>(true);
                    //cityLights = runwayLights.gameObject;
                    //if (cityLights.name != animRecSettingsData.cityLightsObjectName)
                    //    cityLights = null;
                    //cityLights = GameObject.Find(animRecSettingsData.cityLightsObjectName);
                    cityLightsStatus = animRecSettingsAsset.GetData().cityLightsStatus;
                }
                serializedObject.Update();
                //serializedObject.ApplyModifiedProperties();
                Repaint();
            }
        }

        TimelineAsset CreateNewScenario(FlightSimRecordingData animRecAsset, FlightSimRecordingPlayer animationPlayer, FlightScenarioPlayer scenarioPlayer)
        {
            TimelineAsset timeline = CreateInstance<TimelineAsset>();
            //timeline.name = name;

            // get new asset path
            string newAssetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/Content Files/Scenarios/Scenario-" + animRecAsset.clipFolder + ".playable");
            // save new timeline asset
            AssetDatabase.CreateAsset(timeline, newAssetPath);

            // ------------------ CREATE TRACKS

            // create default marker in marker track
            timeline.CreateMarkerTrack();
            // create default signal track
            SignalTrack signalTrack = timeline.CreateTrack<SignalTrack>("Signal Track Gen");
            // create default label track
            RangeLabelTrack labelTrack = timeline.CreateTrack<RangeLabelTrack>("Label Track Gen");
            // create default control track
            ControlTrack controlTrack = timeline.CreateTrack<ControlTrack>("Recorded Flight Gen");

            // ------------------ CREATE TRACK CLIPS AND/OR MARKERS

            // add default scenario step destination marker at time = 0
            Marker startMarker = timeline.markerTrack.CreateMarker(typeof(StepMarker), 0) as Marker;
            startMarker.name = "Step 1 Gen";

            // create control clip to control Animation Player
            TimelineClip controlClip = controlTrack.CreateClip<ControlPlayableAsset>();
            controlClip.displayName = "AnimationPlayer Gen";
            controlClip.duration = animRecAsset.timeline.duration;

            // set exposed reference to Animation Player gameobject
            ControlPlayableAsset controlAsset = controlClip.asset as ControlPlayableAsset;
            controlAsset.sourceGameObject.exposedName = new PropertyName("AnimationPlayer");
            scenarioPlayer.playableDirector.SetReferenceValue(controlAsset.sourceGameObject.exposedName, animationPlayer.gameObject);

            // add default pause signal at the end
            SignalEmitter pauseSignal = signalTrack.CreateMarker(typeof(SignalEmitter), timeline.duration) as SignalEmitter;
            SignalAsset pauseSignalAsset = AssetDatabase.LoadAssetAtPath<SignalAsset>("Assets/Content Files/TimelineSignals/PauseSignal.signal");
            pauseSignal.name = "End Gen";
            pauseSignal.asset = pauseSignalAsset;

            // add default label clip
            TimelineClip labelClip = labelTrack.CreateClip<RangeLabelClip>();
            labelClip.displayName = "Step 1";
            labelClip.start = 0;
            labelClip.duration = timeline.duration;

            AssetDatabase.SaveAssets();            
        
            return timeline;
        }

        private void SetScenarioCreationMode()
        {
            GameObject scenarioLoader;
            EventSystem[] localEventSystems;
            FlightScenarioPlayer flightScenarioPlayer;
            FlightScenarioCameraController[] flightScenarioCameraControllers;
            FlightScenarioMonitorCamera[] flightScenarioMonitorCameras;
            HelicopterCamera helicopterCamera;
            SilantroCamera silantroCamera;

            // disable scenario loader gameobject
            scenarioLoader = FindObjectOfType<FlightScenarioLoader>(true).gameObject;
            scenarioLoader.SetActive(false);

            // enable local event system
            localEventSystems = FindObjectsOfType<EventSystem>(true);
            foreach (EventSystem localEventSystem in localEventSystems)
            {
                if (localEventSystem.gameObject.name == "LocalEventSystem")
                    localEventSystem.gameObject.SetActive(true);
            }

            // disable flight scenario player component
            flightScenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);
            flightScenarioPlayer.enabled = false;

            // disable flight scenario camera controller & flight scenario monitor camera
            flightScenarioCameraControllers = FindObjectsOfType<FlightScenarioCameraController>(true);
            foreach (FlightScenarioCameraController controller in flightScenarioCameraControllers)
                controller.enabled = false;

            flightScenarioMonitorCameras = FindObjectsOfType<FlightScenarioMonitorCamera>(true);
            foreach (FlightScenarioMonitorCamera monitor in flightScenarioMonitorCameras)
                monitor.enabled = false;

            // enable helicopter camera control & airplane camera control
            helicopterCamera = FindObjectOfType<HelicopterCamera>(true);
            if (helicopterCamera != null)
                helicopterCamera.enabled = true;
            silantroCamera = FindObjectOfType<SilantroCamera>(true);
            if (silantroCamera != null)
                silantroCamera.enabled = true;

            isScenarioCreationMode = true;
            SaveRecordData();
            SaveChanges();
            SaveScene();
        }

        private void SetTemplateIntegrationMode()
        {
            GameObject scenarioLoader;
            EventSystem[] localEventSystems;
            FlightScenarioPlayer flightScenarioPlayer;
            FlightScenarioCameraController[] flightScenarioCameraControllers;
            FlightScenarioMonitorCamera[] flightScenarioMonitorCameras;
            HelicopterCamera helicopterCamera;
            SilantroCamera silantroCamera;

            // enable scenario loader
            scenarioLoader = FindObjectOfType<FlightScenarioLoader>(true).gameObject;
            scenarioLoader.SetActive(true);

            // disable local event system
            localEventSystems = FindObjectsOfType<EventSystem>(true);
            foreach (EventSystem localEventSystem in localEventSystems)
            {
                if (localEventSystem.gameObject.name == "LocalEventSystem")
                    localEventSystem.gameObject.SetActive(false);
            }

            // enable flight scenario player component
            flightScenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);
            flightScenarioPlayer.enabled = false;

            // enable flight scenario camera controller & flight scenario monitor camera
            flightScenarioCameraControllers = FindObjectsOfType<FlightScenarioCameraController>(true);
            foreach (FlightScenarioCameraController controller in flightScenarioCameraControllers)
                controller.enabled = true;

            flightScenarioMonitorCameras = FindObjectsOfType<FlightScenarioMonitorCamera>(true);
            foreach (FlightScenarioMonitorCamera monitor in flightScenarioMonitorCameras)
                monitor.enabled = true;

            // disable helicopter camera control & airplane camera control
            helicopterCamera = FindObjectOfType<HelicopterCamera>(true);
            if (helicopterCamera != null)
                helicopterCamera.enabled = false;
            silantroCamera = FindObjectOfType<SilantroCamera>(true);
            if (silantroCamera != null)
                silantroCamera.enabled = false;

            isScenarioCreationMode = false;
            SaveRecordData();
            SaveChanges();
            SaveScene();
        }

        private void SaveScene()
        {
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        }*/

        /*private object OnElementClassListCallback(string listId, object element, int index)
        {
            FlightSimRecordingItem recordingItem = element as FlightSimRecordingItem;

            EditorGUILayout.BeginVertical();
            propertyField.ShowAssetField("Animated Object", ref recordingItem.animatedObject);
            propertyField.ShowSimpleField("Animation Type", ref recordingItem.animationType);
            EditorGUILayout.EndVertical();

            return recordingItem;
        }*/

        /*void OnSelectionChange()
        {
            //selectionIDs = Selection.instanceIDs;
            Debug.Log("Selection instanceIDs: " + Selection.instanceIDs);
        }

        void Update()
        {
            //Rect clickArea = EditorGUILayout.GetControlRect();
            //Event current = Event.current;



            if (recording)
            {
                if (EditorApplication.isPlaying && !EditorApplication.isPaused)
                {
                    RecordImages();
                    Repaint();
                }
                else
                    status = "Waiting for Editor to Play";
            }
        }

        private static void PlayableDirectorRebinding(PlayableDirector playableDirector)
        {
            if (playableDirector != null)
            {
                var timeline = playableDirector.playableAsset as TimelineAsset;
                if (timeline != null)
                {
                    var bindings = new Dictionary<Object, Object>();
                    foreach (var playableBinding in timeline.outputs)
                    {
                        bindings.Add(playableBinding.sourceObject, playableDirector.GetGenericBinding(playableBinding.sourceObject));
                    }
                    var timelineGameObject = playableDirector.gameObject;
                    GameObject.DestroyImmediate(playableDirector);
                    playableDirector = timelineGameObject.AddComponent<PlayableDirector>();
                    foreach (var entry in bindings)
                    {
                        playableDirector.SetGenericBinding(entry.Key, entry.Value);
                    }
                }
            }
        }*/

    }
}
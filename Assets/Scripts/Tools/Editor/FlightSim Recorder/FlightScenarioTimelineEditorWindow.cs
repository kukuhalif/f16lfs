using UnityEngine;
using UnityEditor;
using UnityEngine.Timeline;
using UnityEditor.Timeline;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;
using System.Linq;
using HelicopterSim;
using Mewlist.MassiveClouds;
using Oyedoyin.Common;
using Oyedoyin.FixedWing;

namespace VirtualTraining.Tools
{
    public enum TimelinePropertiesTab
    {
        TRACK_BINDINGS,
        STEP_MARKERS
    }

    public class FlightScenarioTimelineEditorWindow : TreeViewEditorBase<FlightScenarioTimelineTreeAsset, FlightScenarioTimelineEditorWindow, FlightScenarioTimelineData, FlightScenarioTimelineTreeElement>
    {
        //bool isFlightSimSceneLoaded = false;
        enum LoadedScene { Airplane, Helicopter, Other };
        LoadedScene loadedScene = LoadedScene.Other;

        FlightScenarioPlayer flightScenarioPlayer;
        FlightSimRecorder animationRecorder;
        FlightSimRecordingPlayer animationPlayer;
        //TimelineAsset currentScenarioTimeline = null;
        //FlightSimRecordingData currentRecordedFlightData = null;
        //FlightScenarioTimelineTreeElement currentElement = null;

        EditorGUISplitView splitter;

        PropertyField propertyField;

        TimelinePropertiesTab[] timelinePropertiesTabs;

        List<StepMarker> stepMarkers;
        int markerNum = 0;

        [MenuItem("Virtual Training/Flight Scenario/Flight Scenario Timeline Editor")]
        public static void GetWindow()
        {
            FlightScenarioTimelineEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                //GUIContent icon = EditorGUIUtility.IconContent("Camera Gizmo");
                // window.titleContent = new GUIContent("Flight Scenario TL Editor", icon.image);

                window.CheckLoadedScene();
                if (window.loadedScene != LoadedScene.Other)
                {
                    window.Initialize();
                    //window.flightScenarioPlayer = FindObjectOfType<FlightScenarioPlayer>();
                    //window.animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>();
                }
                else
                {
                    Debug.LogWarning("Flight Scenario Timeline Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
                }
            }
        }

        public static FlightScenarioTimelineEditorWindow GetWindow(int instanceID)
        {
            FlightScenarioTimelineEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.CheckLoadedScene();
                if (window.loadedScene != LoadedScene.Other)
                {
                    window.Initialize();
                }
                else
                {
                    Debug.LogWarning("Flight Scenario Timeline Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
                }
            }
            return window;
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        //public static FlightScenarioTimelineEditorWindow OnOpenAsset(int instanceID)
        {
            FlightScenarioTimelineEditorWindow window = OpenEditorWindow(instanceID, true);
            if (window != null)
            {
                GUIContent icon = EditorGUIUtility.IconContent("Camera Gizmo");
                window.titleContent = new GUIContent("Flight Scenario TL Editor", icon.image);

                window.CheckLoadedScene();
                if (window.loadedScene != LoadedScene.Other)
                {
                    window.Initialize();
                }
                else
                {
                    Debug.LogWarning("Flight Scenario Timeline Editor requires AirplaneSim Scene or HelicopterSim Scene to be loaded and active.");
                }

                return true;
            }

            return false;
        }

        /*public static FlightScenarioTimelineEditorWindow OnOpenAsset(int instanceID, TimelineAsset scenarioTimelineAsset)
        {
            FlightScenarioTimelineEditorWindow window = OpenEditorWindow(instanceID, true);
            if (window != null)
            {
                window.scenarioTimeline = scenarioTimelineAsset;
                window.Initialize();
                return window;
            }

            return null;
        }*/

        private void Awake()
        {
            EditorApplication.hierarchyChanged += OnHierarchyWindowChanged;
        }

        private void OnDestroy()
        {
            EditorApplication.hierarchyChanged -= OnHierarchyWindowChanged;
        }

        private void OnHierarchyWindowChanged()
        {
            CheckLoadedScene();
        }

        private void CheckLoadedScene()
        {
            // check if aircraft scene model is loaded and active
            //isFlightSimSceneLoaded = (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("AirplaneSim") ||
            //    SceneManager.GetActiveScene() == SceneManager.GetSceneByName("HelicopterSim"));
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("AirplaneSim"))
                loadedScene = LoadedScene.Airplane;
            else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("HelicopterSim"))
                loadedScene = LoadedScene.Helicopter;
            else
                loadedScene = LoadedScene.Other;
        }

        /*protected override void OnEnable()
        {
            base.OnEnable();
            propertyField = new PropertyField(null, "flight scenario editor");

            // create category tabs
            List<string> timelinePropertyList = new List<string>();
            foreach (var timelinePropertyName in timelinePropertiesTabs)
            {
                timelinePropertyList.Add(timelinePropertyName.ToString().Replace('_', ' '));
            }
            propertyField.CreateTab("Timeline Properties", OnTimelinePropertiesTabCallback, timelinePropertyList.ToArray());

            propertyField.CreateListView("Track Bindings", null, null, TrackBindingElementCallback, null, null, false, typeof(TrackBinding));
        }*/

        protected override void Initialize()
        {
            // if scriptable object removed
            if (ScriptableObjectOriginalFile == null)
                return;
            base.Initialize();

            flightScenarioPlayer = FindObjectOfType<FlightScenarioPlayer>(true);
            animationRecorder = FindObjectOfType<FlightSimRecorder>(true);
            animationPlayer = FindObjectOfType<FlightSimRecordingPlayer>(true);

            splitter = new EditorGUISplitView(EditorGUISplitView.Direction.Vertical, WindowId + "description");

            propertyField = new PropertyField(null, "flight scenario editor");

            timelinePropertiesTabs = new TimelinePropertiesTab[] { TimelinePropertiesTab.TRACK_BINDINGS, TimelinePropertiesTab.STEP_MARKERS };

            // create timeline properties tabs
            List<string> timelinePropertyList = new List<string>();
            foreach (var timelinePropertyName in timelinePropertiesTabs)
            {
                timelinePropertyList.Add(timelinePropertyName.ToString().Replace('_', ' '));
            }
            propertyField.CreateTab("Timeline Properties", OnTimelinePropertiesTabCallback, timelinePropertyList.ToArray());

            propertyField.CreateListView("Track Bindings", null, null, TrackBindingElementCallback, null, null, false, typeof(TrackBinding));

            stepMarkers = new List<StepMarker>();
            propertyField.CreateListView("Step Markers", null, null, StepMarkersElementCallback, null, null, false, typeof(StepMarker));
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            //if (flightScenarioTimelineContentEditor != null)
            //    flightScenarioTimelineContentEditor.SaveCameraDestination();
        }

        protected override void OnSelectionChanged()
        {
            base.OnSelectionChanged();
            Debug.Log("Selection changed!");
        }


        private void OnTimelinePropertiesTabCallback(int selected, object data, string fieldId)
        {
            FlightScenarioTimelineTreeElement selectedElement = data as FlightScenarioTimelineTreeElement;

            if (timelinePropertiesTabs.Length == 0)
                return;

            if (!(selected < timelinePropertiesTabs.Length))
                selected = 0;

            TimelinePropertiesTab selectedTab = timelinePropertiesTabs[selected];

            switch (selectedTab)
            {
                // figure and figure list case sama aja isinya
                case TimelinePropertiesTab.TRACK_BINDINGS:

                    TrackBindingsInspector(selectedElement);
                    break;

                case TimelinePropertiesTab.STEP_MARKERS:

                    StepMarkersInspector(selectedElement);
                    break;
            }

        }

        private object TrackBindingElementCallback(string listId, object element, int index)
        {
            TrackBinding trackBinding = element as TrackBinding;

            GUILayout.BeginVertical();

            EditorGUILayout.LabelField("Track Name", trackBinding.track.name, GUILayout.Width(500));
            GUILayout.Space(5f);
            EditorGUILayout.LabelField("Track Type", trackBinding.track.GetType().ToString(), GUILayout.Width(500));

            if (trackBinding.bindingObject != null)
            {
                // if binding object available (got from timeline & scene), use gameobject from casted bindingObject
                GUILayout.Space(5f);
                EditorGUILayout.LabelField("Binding Object Type", trackBinding.bindingObject.GetType().ToString(), GUILayout.Width(500));

                if (trackBinding.track.GetType() == typeof(SignalTrack))
                {
                    SignalReceiver signalReceiverBinding = trackBinding.bindingObject as SignalReceiver;
                    trackBinding.gameObjectTypeBinding = new GameObjectType(signalReceiverBinding.gameObject);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject Name", trackBinding.gameObjectTypeBinding.gameObject.name);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject ID", trackBinding.gameObjectTypeBinding.Id.ToString());
                }
                else if (trackBinding.track.GetType() == typeof(ActivationTrack))
                {
                    trackBinding.gameObjectTypeBinding = new GameObjectType(trackBinding.bindingObject as GameObject);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject Name", trackBinding.gameObjectTypeBinding.gameObject.name);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject ID", trackBinding.gameObjectTypeBinding.Id.ToString());
                }
                else if (trackBinding.track.GetType() == typeof(AnimationTrack))
                {
                    Animator animatorBinding = trackBinding.bindingObject as Animator;
                    trackBinding.gameObjectTypeBinding = new GameObjectType(animatorBinding.gameObject);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject Name", trackBinding.gameObjectTypeBinding.gameObject.name);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject ID", trackBinding.gameObjectTypeBinding.Id.ToString());
                }
                else if (trackBinding.track.GetType() == typeof(AudioTrack))
                {
                    AudioSource audioSourceBinding = trackBinding.bindingObject as AudioSource;
                    trackBinding.gameObjectTypeBinding = new GameObjectType(audioSourceBinding.gameObject);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject Name", trackBinding.gameObjectTypeBinding.gameObject.name);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject ID", trackBinding.gameObjectTypeBinding.Id.ToString());
                }
            }
            else if (trackBinding.gameObjectTypeBinding.Id != 0) 
            {
                // if only gameobject reference id is available, get gameobject using gameobject reference (expensive operation)
                GameObject gameObject = trackBinding.gameObjectTypeBinding.gameObject;
                if (gameObject != null)
                {
                    if (trackBinding.track.GetType() == typeof(SignalTrack))
                    {
                        trackBinding.bindingObject = gameObject.GetComponent<SignalReceiver>();
                    }
                    else if (trackBinding.track.GetType() == typeof(ActivationTrack))
                    {
                        trackBinding.bindingObject = gameObject;
                    }
                    else if (trackBinding.track.GetType() == typeof(AnimationTrack))
                    {
                        trackBinding.bindingObject = gameObject.GetComponent<Animator>();
                    }
                    else if (trackBinding.track.GetType() == typeof(AudioTrack))
                    {
                        trackBinding.bindingObject = gameObject.GetComponent<AudioSource>();
                    }

                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("Binding Object Type", trackBinding.bindingObject.GetType().ToString(), GUILayout.Width(500));
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject Name", trackBinding.gameObjectTypeBinding.gameObject.name);
                    GUILayout.Space(5f);
                    EditorGUILayout.LabelField("GameObject ID", trackBinding.gameObjectTypeBinding.Id.ToString());
                }
                else
                {
                    Debug.LogWarning("Failed to get GameObjectReference for Object Id " + trackBinding.gameObjectTypeBinding.Id.ToString());
                }
            }
            else
            {
                GUIContent warnIcon = EditorGUIUtility.IconContent("console.warnicon");
                // display line warning if a track need a binding object
                if (trackBinding.track.GetType() == typeof(SignalTrack))
                {
                    GUILayout.Space(5f);
                    GUI.color = Color.yellow;
                    EditorGUILayout.HelpBox(new GUIContent("Signal Track needs Signal Receiver object binding! Please set binding on Timeline Panel", warnIcon.image), true);
                    GUI.color = GUI.backgroundColor;
                }
                else if (trackBinding.track.GetType() == typeof(AnimationTrack))
                {
                    GUILayout.Space(5f);
                    GUI.color = Color.yellow;
                    EditorGUILayout.HelpBox(new GUIContent("Animation Track needs Signal Receiver object binding! Please set binding on Timeline Panel", warnIcon.image), true);
                    GUI.color = GUI.backgroundColor;
                }
                else if (trackBinding.track.GetType() == typeof(ActivationTrack))
                {
                    GUILayout.Space(5f);
                    GUI.color = Color.yellow;
                    EditorGUILayout.HelpBox(new GUIContent("Activation Track needs Signal Receiver object binding! Please set binding on Timeline Panel", warnIcon.image), true);
                    GUI.color = GUI.backgroundColor;
                }
                else if (trackBinding.track.GetType() == typeof(AudioTrack))
                {
                    GUILayout.Space(5f);
                    GUI.color = Color.yellow;
                    EditorGUILayout.HelpBox(new GUIContent("Audio Track needs Signal Receiver object binding! Please set binding on Timeline Panel", warnIcon.image), true);
                    GUI.color = GUI.backgroundColor;
                }

            }

            GUILayout.EndVertical();

            return trackBinding;
        }

        private object StepMarkersElementCallback(string listId, object element, int index)
        {
            StepMarker stepMarker = element as StepMarker;

            EditorGUILayout.LabelField("", stepMarker.name + "\t(" + stepMarker.time.ToString("F2") + "s)", GUILayout.Width(500));

            return stepMarker;
        }

        protected override TreeViewWithTreeModel<FlightScenarioTimelineTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<FlightScenarioTimelineTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<FlightScenarioTimelineTreeElement>(TreeViewState, treeModel);
        }

        protected override FlightScenarioTimelineTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new FlightScenarioTimelineTreeElement("New Flight Scenario Timeline Data", depth, id, false);
        }

        protected override void Save()
        {
            base.Save();
        }

        private void ReloadTimeline(TimelineAsset timeline, PlayableDirector playableDirector)
        {
            if (playableDirector != null)
            {
                Selection.activeGameObject = playableDirector.gameObject;
                if (timeline != null)
                {
                    // set selected timeline asset to playableDirector playableAsset
                    playableDirector.playableAsset = timeline;
                    TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                }
            }
        }

        // Prepare objects and settings to enable playing
        private void PreparePlaying(FlightSimRecorder animationRecorder, FlightSimRecordingPlayer animationPlayer, 
            FlightScenarioPlayer scenarioPlayer, FlightSimRecordingData animRecAsset, TimelineAsset scenarioTimeline)
        {
            // disable animation recorder
            animationRecorder.gameObject.SetActive(false);
            animationRecorder.enabled = false;

            // enable animation player
            animationPlayer.gameObject.SetActive(true);
            animationPlayer.enabled = true;
            animationPlayer.playableDirector.enabled = true;
            // enable scenario player
            scenarioPlayer.gameObject.SetActive(true);
            scenarioPlayer.enabled = true;
            scenarioPlayer.playableDirector.enabled = true;
            //scenarioPlayer.

            if (animRecAsset.aircraftType == AircraftType.Helicopter)
            {
                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController == null)
                {
                    Debug.LogError("Gameobject with Helicopter Controller (Rotary Wing) component not found!");
                    return;
                }

                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController != null)
                    airplaneController.gameObject.SetActive(false);

                //helicopterController.enabled = false;
                helicopterController.gameObject.SetActive(true);
                helicopterController.m_playMode = HelicopterController.PlayMode.Animation;
                helicopterController.m_startMode = (HelicopterController.StartMode)animRecAsset.aircraftMode; // as HelicopterController.StartMode;

                // enable kinematic (disable physics) on helicopter
                Rigidbody rb = helicopterController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = true;
                }

                // disable lever components to prevent conflicts with timeline player
                HelicopterLever[] levers = helicopterController.gameObject.GetComponentsInChildren<HelicopterLever>();
                foreach (HelicopterLever lever in levers)
                {
                    lever.enabled = false;
                }

                /*// disable dial components with animator movement type to prevent conflicts with timeline player
                HelicopterDial[] dials = helicopterController.gameObject.GetComponentsInChildren<HelicopterDial>();
                foreach (HelicopterDial dial in dials)
                {
                    if (dial.movementType == HelicopterDial.MovementType.Animator)
                        dial.enabled = false;
                }*/

            }
            else if (animRecAsset.aircraftType == AircraftType.Airplane)
            {
                FixedController airplaneController = FindObjectOfType<FixedController>(true);
                if (airplaneController == null)
                {
                    Debug.LogError("Gameobject with Silantro Controller (Fixed Wing) component not found!");
                    return;
                }

                HelicopterController helicopterController = FindObjectOfType<HelicopterController>(true);
                if (helicopterController != null)
                    helicopterController.gameObject.SetActive(false);

                //airplaneController.enabled = false;
                airplaneController.gameObject.SetActive(true);
                airplaneController.m_playMode = FixedController.PlayMode.Animation;
                airplaneController.m_startMode = (FixedController.StartMode)animRecAsset.aircraftMode; // as AirplaneController.StartMode;

                // enable kinematic (disable physics) on airplane
                Rigidbody rb = airplaneController.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = true;
                }
                // disable lever components to prevent conflicts with timeline player
                SilantroLever[] levers = airplaneController.gameObject.GetComponentsInChildren<SilantroLever>();
                foreach (SilantroLever lever in levers)
                {
                    lever.enabled = false;
                }

                /*// disable dial components with animator movement type to prevent conflicts with timeline player
                SilantroDial[] dials = airplaneController.gameObject.GetComponentsInChildren<SilantroDial>();
                foreach (SilantroDial dial in dials)
                {
                    if (dial.movementType == SilantroDial.MovementType.Animator)
                        dial.enabled = false;
                }*/

            }

            // setup environment from recorded data
            float atmosPosXNormalized = animRecAsset.environmentTime / 24f;
            float atmosPosYNormalized = 1 - animRecAsset.environmentCloud;

            AtmosPad atmosPad = FindObjectOfType<AtmosPad>(true);
            if (atmosPad != null)
            {
                atmosPad.SetPointer(new Vector2(atmosPosXNormalized, atmosPosYNormalized));
            }

            RunwayLights cityLights = FindObjectOfType<RunwayLights>(true);
            if (cityLights != null)
            {
                cityLights.gameObject.SetActive(animRecAsset.cityLightsStatus);
            }

            // load timelineAsset
            TimelineAsset timeline = animRecAsset.timeline;

            // set playableDirector to loaded timelineAsset
            animationPlayer.playableDirector.playableAsset = timeline;

            // loop on timeline output tracks
            int animTrackIdx = 0;
            for (int trackIdx = 0; trackIdx < timeline.outputTrackCount; trackIdx++)
            {
                // get timeline track
                TrackAsset track = timeline.GetOutputTrack(trackIdx);
                // check if timeline track type is animation track
                if (track.GetType() == typeof(AnimationTrack))
                {
                    // get clip data
                    ClipData clipData = animRecAsset.clipDatas[animTrackIdx];

                    // get gameobject reference
                    GameObject go = clipData.clipObject.gameObject;

                    if (go != null)
                    {
                        // make sure gameobject is active
                        go.SetActive(true);

                        // get gameobject animator component
                        Animator animator = go.GetComponent<Animator>();
                        if (animator == null)
                        {
                            Debug.Log("Create Animator...");
                            // add Animator component if not exist
                            animator = go.AddComponent(typeof(Animator)) as Animator;
                        }
                        // make sure animator component is active
                        animator.enabled = true;
                        // bind gameobject with animator component
                        animationPlayer.playableDirector.SetGenericBinding(track, go);

                        // set Animator Recording Helper status to Play Animation
                        if (clipData.clipType == FlightSimRecordingItem.FlightSimRecordingType.AnimatorParameter)
                        {
                            AnimatorRecordingHelper animParamController = go.GetComponent<AnimatorRecordingHelper>();
                            if (animParamController != null)
                            {
                                animParamController.enabled = true;
                                animParamController.helperMode = AnimatorRecordingHelper.HelperMode.PlayAnimation;
                            }
                            else
                            {
                                Debug.LogWarning("Animator Recording Helper component not found...");
                            }
                        }
                    }
                    animTrackIdx++;
                }
            }

            if (scenarioPlayer != null && scenarioTimeline != null)
            {
                scenarioPlayer.playableDirector.playableAsset = scenarioTimeline;
                // refresh timeline editor window
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);

                // loop on scenario timeline root tracks
                //int scenarioTrackIdx = 0;
                for (int trackIdx = 0; trackIdx < scenarioTimeline.outputTrackCount; trackIdx++)
                {
                    // get timeline track
                    TrackAsset track = scenarioTimeline.GetOutputTrack(trackIdx);

                    // check if timeline track type is signal track
                    if (track.GetType() == typeof(SignalTrack))
                    {
                        // bind signal track to scenarioPlayer signalReceiver
                        SignalReceiver signalReceiver = scenarioPlayer.gameObject.GetComponent<SignalReceiver>();
                        scenarioPlayer.playableDirector.SetGenericBinding(track, signalReceiver);
                    }
                    else if (track.GetType() == typeof(ControlTrack))
                    {
                        // get the first clip (assume there is only one control clip exist)
                        TimelineClip controlClip = track.GetClips().ElementAt(0);

                        if (controlClip != null)
                        {
                            // update control clip duration to match recorded timeline duration
                            controlClip.duration = animRecAsset.timeline.duration;
                        }
                        else
                        {
                            Debug.LogError("Scenario Timeline Control Clip not found!");
                        }
                    }
                }

                // play scenario
                //scenarioPlayer.playableDirector.Play();
            }
        }

        private void GetTrackBindings(TimelineAsset scenarioTimeline, PlayableDirector scenarioPlayer, List<TrackBinding> trackBindings)
        {
            if (scenarioPlayer != null)
            {
                //Selection.activeGameObject = scenarioPlayer.gameObject;
                if (scenarioTimeline != null)
                {
                    // set selected timeline asset to playableDirector playableAsset
                    scenarioPlayer.playableAsset = scenarioTimeline;
                    // refresh timeline editor window
                    TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
                    // reset trackbindings data
                    trackBindings.Clear();

                    // loop on timeline output tracks
                    for (int trackIdx = 0; trackIdx < scenarioTimeline.outputTrackCount; trackIdx++)
                    {
                        TrackBinding trackBinding = new TrackBinding();
                        //trackBinding.track = scenarioTimeline.GetRootTrack(trackIdx);
                        trackBinding.track = scenarioTimeline.GetOutputTrack(trackIdx);

                        /*// get control track clip gameobject target 
                        if (trackBinding.track.GetType() == typeof(ControlTrack))
                        {
                            // get the first clip (assume there is only one control clip exist)
                            TimelineClip controlClip = trackBinding.track.GetClips().ElementAt(0);
                            if (controlClip != null)
                            {
                                // get exposed reference
                                ControlPlayableAsset controlAsset = controlClip.asset as ControlPlayableAsset;
                                Object exposedRef = playableDirector.GetReferenceValue(controlAsset.sourceGameObject.exposedName, out bool isValid);
                                if (isValid)
                                {
                                    Debug.Log("Exposed Ref type: " + exposedRef.GetType().ToString());
                                }
                            }

                        }*/

                        // get binding object
                        trackBinding.bindingObject = scenarioPlayer.GetGenericBinding(trackBinding.track);

                        if (trackBinding.bindingObject != null)
                        {
                            // get gameobject binding based on track type
                            if (trackBinding.track.GetType() == typeof(SignalTrack))
                            {
                                SignalReceiver signalReceiverBinding = trackBinding.bindingObject as SignalReceiver;
                                if (signalReceiverBinding != null)
                                    trackBinding.gameObjectTypeBinding = new GameObjectType(signalReceiverBinding.gameObject);
                            }
                            else if (trackBinding.track.GetType() == typeof(ActivationTrack))
                            {
                                trackBinding.gameObjectTypeBinding = new GameObjectType(trackBinding.bindingObject as GameObject);
                            }
                            else if (trackBinding.track.GetType() == typeof(AnimationTrack))
                            {
                                Animator animatorBinding = trackBinding.bindingObject as Animator;
                                if (animatorBinding != null)
                                    trackBinding.gameObjectTypeBinding = new GameObjectType(animatorBinding.gameObject);
                            }
                            else if (trackBinding.track.GetType() == typeof(AudioTrack))
                            {
                                AudioSource audioSourceBinding = trackBinding.bindingObject as AudioSource;
                                if (audioSourceBinding != null)
                                    trackBinding.gameObjectTypeBinding = new GameObjectType(audioSourceBinding.gameObject);
                            }
                            else
                                trackBinding.gameObjectTypeBinding = new GameObjectType();
                        }
                        else
                            trackBinding.gameObjectTypeBinding = new GameObjectType();

                        trackBindings.Add(trackBinding);
                    }

                    //EditorUtility.SetDirty(selectedElements[0].data);
                    //EditorUtility.SetDirty(ScriptableObjectTemp);
                }
            }
        }

        private void SetTrackBindings(TimelineAsset scenarioTimeline, PlayableDirector scenarioPlayer, List<TrackBinding> trackBindings)
        {
            if (scenarioPlayer != null)
            {
                //Selection.activeGameObject = scenarioPlayer.gameObject;
                if (scenarioTimeline != null)
                {
                    // set selected timeline asset to playableDirector playableAsset
                    scenarioPlayer.playableAsset = scenarioTimeline;

                    // check tracks count
                    if (scenarioTimeline.outputTrackCount != trackBindings.Count)
                    {
                        EditorUtility.DisplayDialog("Tracks count doesn't match!",
                            "Timeline tracks count does not match with track bindings data, please check!", "Ok");
                    }
                    else
                    {
                        // loop on trackbindings data
                        for (int trackIdx = 0; trackIdx < trackBindings.Count; trackIdx++)
                        {
                            TrackBinding trackBinding = trackBindings[trackIdx];
                            TrackAsset track = scenarioTimeline.GetOutputTrack(trackIdx);

                            // check gameObject binding and track type
                            if (trackBinding.gameObjectTypeBinding.gameObject != null && trackBinding.track.GetType() == track.GetType())
                            {
                                // make sure gameobject is active
                                trackBinding.gameObjectTypeBinding.gameObject.SetActive(true);

                                // set binding based on track type
                                if (track.GetType() == typeof(SignalTrack))
                                {
                                    SignalReceiver signalReceiverBinding = trackBinding.gameObjectTypeBinding.gameObject.GetComponent<SignalReceiver>();
                                    // make sure component is enabled
                                    signalReceiverBinding.enabled = true;
                                    // set signalreceiver binding
                                    if (signalReceiverBinding != null)
                                        scenarioPlayer.SetGenericBinding(track, signalReceiverBinding);
                                }
                                else if (track.GetType() == typeof(ActivationTrack))
                                {
                                    // set gameobject binding
                                    scenarioPlayer.SetGenericBinding(track, trackBinding.gameObjectTypeBinding.gameObject);
                                }
                                else if (track.GetType() == typeof(AnimationTrack))
                                {
                                    Animator animatorBinding = trackBinding.gameObjectTypeBinding.gameObject.GetComponent<Animator>();
                                    // make sure component is enabled
                                    animatorBinding.enabled = true;
                                    // set animator binding
                                    if (animatorBinding != null)
                                        scenarioPlayer.SetGenericBinding(track, animatorBinding);
                                }
                                else if (track.GetType() == typeof(AudioTrack))
                                {
                                    AudioSource audioSourceBinding = trackBinding.gameObjectTypeBinding.gameObject.GetComponent<AudioSource>();
                                    // make sure component is enabled
                                    audioSourceBinding.enabled = true;
                                    // set AudioSource binding
                                    if (audioSourceBinding != null)
                                        scenarioPlayer.SetGenericBinding(track, audioSourceBinding);
                                }
                            }
                        }
                    }
                    //EditorUtility.SetDirty(selectedElements[0].data);
                    EditorUtility.SetDirty(ScriptableObjectTemp);
                }
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
            }
        }

        private void ClearTrackBindings(PlayableDirector playableDirector)
        {
            if (playableDirector != null)
            {
                var go = playableDirector.gameObject;
            }
        }

        private void SetFlightSimRecordingData(FlightSimRecordingData flightSimRecordingData, PlayableDirector animationPlayer)
        {
            if (flightSimRecordingData != null)
            {
                //flightSimRecordingData = selectedElement.data.recordedFlightData;
                if (animationPlayer != null)
                {
                    // set recording data asset to animationPlayer playableDirector
                    //ReloadTimeline(flightSimRecordingData.timeLine, animationPlayer.playableDirector);

                    // load timelineAsset
                    TimelineAsset timeline = flightSimRecordingData.timeline;

                    // set playableDirector to loaded timelineAsset
                    animationPlayer.playableAsset = timeline;

                    // loop on timeline output tracks
                    int animTrackIdx = 0;
                    for (int trackIdx = 0; trackIdx < flightSimRecordingData.timeline.outputTrackCount; trackIdx++)
                    {
                        // get timeline track
                        TrackAsset track = flightSimRecordingData.timeline.GetOutputTrack(trackIdx);
                        // check if timeline track type is animation track
                        if (track.GetType() == typeof(AnimationTrack))
                        {
                            // get clip data
                            ClipData clipData = flightSimRecordingData.clipDatas[animTrackIdx];

                            // get gameobject reference
                            GameObject go = clipData.clipObject.gameObject;

                            if (go != null)
                            {
                                // make sure gameobject is active
                                go.SetActive(true);

                                // get gameobject animator component
                                Animator animator = go.GetComponent<Animator>();
                                if (animator == null)
                                {
                                    Debug.Log("Create Animator...");
                                    // add Animator component if not exist
                                    animator = go.AddComponent(typeof(Animator)) as Animator;
                                }
                                // make sure animator component is enabled
                                animator.enabled = true;
                                // bind gameobject with animator component
                                animationPlayer.SetGenericBinding(track, go);

                                // set Animator Recording Helper status to Play Animation
                                if (clipData.clipType == FlightSimRecordingItem.FlightSimRecordingType.AnimatorParameter)
                                {
                                    AnimatorRecordingHelper animParamController = go.GetComponent<AnimatorRecordingHelper>();
                                    if (animParamController != null)
                                    {
                                        animParamController.enabled = true;
                                        animParamController.helperMode = AnimatorRecordingHelper.HelperMode.PlayAnimation;
                                    }
                                    else
                                    {
                                        Debug.LogWarning("Animator Recording Helper component not found...");
                                    }
                                }
                            }
                            animTrackIdx++;
                        }
                    }
                }
            }
        }

        private void TopDetailInspector(FlightScenarioTimelineTreeElement selectedElement, Rect panelRect)
        {
            GUI.color = Color.cyan;
            EditorGUILayout.HelpBox("Flight Scenario Properties", MessageType.None);
            GUI.color = GUI.backgroundColor;

            GUILayout.Space(5f);
            propertyField.ShowSimpleField("Scenario Title", ref selectedElement.data.title, 150, 400);

            GUILayout.Space(5f);
            propertyField.ShowAssetField("Scenario Timeline", ref selectedElement.data.scenarioTimeline, 150, 400);

            TimelineAsset scenarioTimeline = selectedElement.data.scenarioTimeline;

            /*// check for scenarioTimeline changes 
            if (scenarioTimeline != currentScenarioTimeline)
            {
                // automatically get track bindings from new scenarioTimeline
                GetTrackBindings(scenarioTimeline, flightScenarioPlayer.playableDirector, selectedElement.data.trackBindings);
                currentScenarioTimeline = scenarioTimeline;
            }*/

            GUILayout.Space(5f);
            GUILayout.BeginHorizontal();
            propertyField.ShowAssetField("Recorded Flight Data", ref selectedElement.data.recordedFlightData, 150, 400);

            FlightSimRecordingData recordedFlightData = selectedElement.data.recordedFlightData;

            /*// check for flightSimRecordingData changes
            if (recordedFlightData != currentRecordedFlightData)
            {
                // automatically set flightSimRecordingData to animatinPlayer
                SetFlightSimRecordingData(recordedFlightData, animationPlayer.playableDirector);
                currentRecordedFlightData = recordedFlightData;
            }*/

            GUILayout.Space(5f);
            if (recordedFlightData != null)
            {
                if ((recordedFlightData.aircraftType == AircraftType.Helicopter && loadedScene == LoadedScene.Helicopter) ||
                    (recordedFlightData.aircraftType == AircraftType.Airplane && loadedScene == LoadedScene.Airplane))
                {
                    GUI.color = Color.green;
                    EditorGUILayout.LabelField("Aircraft Type: " + recordedFlightData.aircraftType.ToString(), GUILayout.Width(350));
                    GUI.color = GUI.backgroundColor;
                }
                else
                {
                    // display warning status
                    GUI.color = Color.red;
                    GUIContent warnIcon = EditorGUIUtility.IconContent("console.warnicon");
                    EditorGUILayout.LabelField(new GUIContent("Aircraft Type: " + recordedFlightData.aircraftType.ToString() + " (Simulation scene does not match!)", warnIcon.image), GUILayout.Width(350));
                    GUI.color = GUI.backgroundColor;
                }
                
            }
            else
                EditorGUILayout.LabelField("Unknown Aircraft Type!", GUILayout.Width(350));
            GUILayout.EndHorizontal();


            GUI.enabled = scenarioTimeline != null;
            GUILayout.Space(5f);
            if (GUILayout.Button(new GUIContent(" Load scenario in timeline panel"), GUILayout.Height(30)))
            {
                /*if (flightScenarioPlayer != null)
                {
                    // set flightSimRecordingData to animationPlayer playableDirector
                    if (recordedFlightData != null)
                        SetFlightSimRecordingData(recordedFlightData, animationPlayer.playableDirector);

                    // set scenarioTimeline to scenarioPlayer playableDirector
                    if (scenarioTimeline != null)
                        ReloadTimeline(scenarioTimeline, flightScenarioPlayer.playableDirector);

                }*/
                PreparePlaying(animationRecorder, animationPlayer, flightScenarioPlayer, recordedFlightData, scenarioTimeline);
                ReloadTimeline(scenarioTimeline, flightScenarioPlayer.playableDirector);

            }
            GUI.enabled = true;

            // **** Timeline control buttons ****
            GUILayout.BeginHorizontal();

            if (GUILayout.Button(new GUIContent(" Rewind", EditorGUIUtility.IconContent("Animation.FirstKey").image), GUILayout.Height(30)))
            {
                if (flightScenarioPlayer.playableDirector.state == PlayState.Paused)
                    flightScenarioPlayer.playableDirector.time = 0f;
                else
                {
                    flightScenarioPlayer.playableDirector.Pause();
                    flightScenarioPlayer.playableDirector.time = 0f;
                    flightScenarioPlayer.playableDirector.Resume();
                    flightScenarioPlayer.aircraftController.Pause(false);
                }
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
            }
            if (GUILayout.Button(new GUIContent(" Play", EditorGUIUtility.IconContent("Animation.Play").image), GUILayout.Height(30)))
            {
                flightScenarioPlayer.playableDirector.Resume();
                flightScenarioPlayer.aircraftController.Pause(false);
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
            }
            if (GUILayout.Button(new GUIContent(" Pause", EditorGUIUtility.IconContent("PauseButton").image), GUILayout.Height(30)))
            {
                flightScenarioPlayer.playableDirector.Pause();
                flightScenarioPlayer.aircraftController.Pause(true);
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
            }

            if (stepMarkers == null)
                GUI.enabled = false;
            else if (stepMarkers.Count <= 0)
                GUI.enabled = false;

            if (GUILayout.Button(new GUIContent(" Previous Marker", EditorGUIUtility.IconContent("Animation.PrevKey").image), GUILayout.Height(30)))
            {
                markerNum--;
                if (markerNum < 0) markerNum = 0;
                if (flightScenarioPlayer.playableDirector.state == PlayState.Paused)
                    flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                else
                {
                    flightScenarioPlayer.playableDirector.Pause();
                    flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                    flightScenarioPlayer.playableDirector.Resume();
                    flightScenarioPlayer.aircraftController.Pause(false);
                }
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
            }
            if (GUILayout.Button(new GUIContent(" Next Marker", EditorGUIUtility.IconContent("Animation.NextKey").image), GUILayout.Height(30)))
            {
                markerNum++;
                if (markerNum >= stepMarkers.Count) markerNum = stepMarkers.Count-1;
                if (flightScenarioPlayer.playableDirector.state == PlayState.Paused)
                    flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                else
                {
                    flightScenarioPlayer.playableDirector.Pause();
                    flightScenarioPlayer.playableDirector.time = stepMarkers[markerNum].time;
                    flightScenarioPlayer.playableDirector.Resume();
                    flightScenarioPlayer.aircraftController.Pause(false);
                }
                TimelineEditor.Refresh(RefreshReason.WindowNeedsRedraw);
            }
            GUI.enabled = true;

            GUILayout.EndHorizontal();
            // **************

            GUILayout.Space(5f);
            propertyField.ShowTextArea("Description", detailPanelRect.width - 15f, ref selectedElement.data.description, false);

        }

        private void TrackBindingsInspector(FlightScenarioTimelineTreeElement selectedElement)
        {
            TimelineAsset scenarioTimeline = selectedElement.data.scenarioTimeline;

            EditorGUILayout.BeginHorizontal();
            //GUIContent resetIcon = EditorGUIUtility.IconContent("Refresh");
            GUI.contentColor = Color.yellow;
            if (GUILayout.Button(new GUIContent(" Reload Track Bindings from Scenario Timeline"), GUILayout.Height(30), GUILayout.Width(300)))
            {
                GetTrackBindings(scenarioTimeline, flightScenarioPlayer.playableDirector, selectedElement.data.trackBindings);
            }
            GUI.contentColor = Color.white;

            if (GUILayout.Button(new GUIContent(" Set Track Bindings to Scenario Timeline"), GUILayout.Height(30), GUILayout.Width(300)))
            {
                SetTrackBindings(scenarioTimeline, flightScenarioPlayer.playableDirector, selectedElement.data.trackBindings);
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Label(" Please make sure all track bindings has no warning message");

            propertyField.ShowListFieldReadOnly("Track Bindings", selectedElement.data.trackBindings);

        }

        private void StepMarkersInspector(FlightScenarioTimelineTreeElement selectedElement)
        {
            TimelineAsset scenarioTimeline = selectedElement.data.scenarioTimeline;

            if (scenarioTimeline != null)
            {
                stepMarkers = scenarioTimeline.markerTrack.GetMarkers().OfType<StepMarker>().ToList();
                propertyField.ShowListFieldReadOnly("Step Markers", stepMarkers);
            }
            else
            {
                GUILayout.Label("No step markers found!");
            }
        }

        private void BottomDetailInspector(FlightScenarioTimelineTreeElement selectedElement, Rect panelRect)
        {
            propertyField.ShowTab("Timeline Properties", selectedElement);
        }
    
        protected override void ContentView(List<FlightScenarioTimelineTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder)
                {
                    var selectedElement = selectedElements[0];

                    splitter.BeginSplitView(0f, detailPanelRect.width);

                    TopDetailInspector(selectedElement, detailPanelRect);

                    splitter.Split();

                    BottomDetailInspector(selectedElement, detailPanelRect);

                    splitter.EndSplitView();
                }
            }
            /*else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        selectedElements[i].name = flightScenarioTimelineContentEditor.PreviewInspector(selectedElements[i].name, i);
                }
            }*/

            if (GUI.GetNameOfFocusedControl() == "Scenario Title")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].name = selectedElements[i].data.title;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].data.title = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }
            EditorGUILayout.EndVertical();            
        }

        protected override void OnGUI()
        {
            if (loadedScene != LoadedScene.Other)
            {
                base.OnGUI();
            }
            else
            {
                GUI.color = Color.yellow;
                EditorGUILayout.HelpBox("Please load and activate AirplaneSim Scene or HelicopterSim Scene to use Flight Scenario Timeline Editor!", MessageType.Warning);
            }
        }
    }
}
﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class FavoriteEditor : EditorWindow
    {
        Vector2 ScrollPos = new Vector2();

        class GameobjectList
        {
            public string Name;
            public List<GameObjectType> Gameobjects = new List<GameObjectType>();

            public GameobjectList(string name)
            {
                Name = name;
            }
        }

        class CameraData
        {
            public string name;
            public CameraDestination camDestination;

            public CameraData()
            {
                name = "";
                camDestination = new CameraDestination();
            }

            public CameraData(string name, CameraDestination camDestination)
            {
                this.name = name;
                this.camDestination = camDestination;
            }
        }

        public class FavoriteColor
        {
            public string name;
            public Color color;

            public FavoriteColor()
            {
                name = "new color name";
                color = Color.white;
            }

            public FavoriteColor(string name, Color color)
            {
                this.name = name;
                this.color = color;
            }
        }

        public static CameraDestination tempCameraDestination;

        List<FavoriteColor> favoriteColors = new List<FavoriteColor>();
        List<GameobjectList> favoriteObjectList = new List<GameobjectList>();
        List<CameraData> favoriteCameras = new List<CameraData>();

        List<GameObjectType> tempGameobjectDatas = new List<GameObjectType>();

        PropertyField propertyField;

        string saveFolder;

        [MenuItem("Virtual Training/Favorite")]
        public static void ShowWindow()
        {
            var window = GetWindow(typeof(FavoriteEditor)) as FavoriteEditor;
            window.titleContent = new GUIContent("favorite editor");
        }

        public static FavoriteEditor GetFavoriteEditor()
        {
            var window = GetWindow(typeof(FavoriteEditor)) as FavoriteEditor;
            window.titleContent = new GUIContent("favorite editor");
            return window;
        }

        public List<GameObjectType> GetTempGameobjectList()
        {
            return tempGameobjectDatas;
        }

        public void AddGameobjectList(string name, List<GameObject> objs)
        {
            GameobjectList newList = new GameobjectList(name);
            for (int i = 0; i < objs.Count; i++)
            {
                newList.Gameobjects.Add(new GameObjectType(objs[i]));
            }
            favoriteObjectList.Add(newList);
        }

        public void AddCameraDestination(CameraDestination newCamDes)
        {
            favoriteCameras.Add(new CameraData("new camera", newCamDes));
        }

        private object OnGameobjectElementDrawCallback(string listId, object element, int index)
        {
            GameObjectType got = element as GameObjectType;
            propertyField.ShowSimpleField("gameobject", listId + index, ref got);
            return got;
        }

        private void OnEnable()
        {
            saveFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + @"\" + Application.productName;

            propertyField = new PropertyField(null, "favorite");
            propertyField.CreateListView("gameobjects", null, null, OnGameobjectElementDrawCallback, null, null, false, typeof(GameObject));

            favoriteColors.Clear();
            GetData("/" + Application.productName + "/FavoriteColor.txt", (string data) =>
            {
                string[] colorDatas = data.Split(',');
                try
                {
                    favoriteColors.Add(new FavoriteColor(colorDatas[0], new Color(float.Parse(colorDatas[1]), float.Parse(colorDatas[2]), float.Parse(colorDatas[3]), float.Parse(colorDatas[4]))));
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e);
                }
            });


            favoriteCameras.Clear();
            GetData("/" + Application.productName + "/FavoriteCamera.txt", (string data) =>
            {
                string[] cameraDatas = data.Split('#');
                for (int i = 0; i < cameraDatas.Length; i++)
                {
                    cameraDatas[i] = cameraDatas[i].Trim();
                    if (!string.IsNullOrEmpty(cameraDatas[i]))
                    {
                        CameraData newCamData = JsonUtility.FromJson<CameraData>(cameraDatas[i]);
                        favoriteCameras.Add(newCamData);
                    }
                }
            });

            favoriteObjectList.Clear();
            GetData("/" + Application.productName + "/FavoriteGameobjectList.txt", (string data) =>
            {
                string[] lists = data.Split('#');
                foreach (var item in lists)
                {
                    string[] contents = item.Split('%');
                    if (!string.IsNullOrEmpty(contents[0].Trim()))
                    {
                        GameobjectList newItem = new GameobjectList(contents[0]);

                        for (int c = 0; c < contents.Length; c++)
                        {
                            string[] objects = contents[c].Split('^');
                            for (int o = 1; o < objects.Length; o++)
                            {
                                string id = objects[o].Trim();
                                if (!string.IsNullOrEmpty(id))
                                {
                                    id = id.Replace("~", "");
                                    int intId = int.Parse(id);
                                    newItem.Gameobjects.Add(new GameObjectType(intId));
                                }
                            }
                        }

                        favoriteObjectList.Add(newItem);
                    }

                }
            });
        }

        public static void GetData(string filePath, Action<string> addAction)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + filePath;
            if (File.Exists(path))
            {
                StreamReader reader = new StreamReader(path);
                string data = reader.ReadToEnd();
                reader.Close();

                string[] datas = data.Split('#');
                foreach (string item in datas)
                {
                    if (item != "")
                    {
                        addAction(item);
                    }
                }
            }
        }

        private void OnDisable()
        {
            Save();
        }

        private void Save()
        {
            SaveData("/" + Application.productName, "/FavoriteColor.txt", () =>
            {
                string clr = "";
                foreach (var item in favoriteColors)
                {
                    clr += item.name + "," + item.color.r + "," + item.color.g + "," + item.color.b + "," + item.color.a + "#";
                }

                return clr;
            });

            SaveData("/" + Application.productName, "/FavoriteGameobjectList.txt", () =>
            {
                string obj = "";
                foreach (var item in favoriteObjectList)
                {
                    obj += "#" + item.Name + "%";
                    foreach (var go in item.Gameobjects)
                    {
                        obj += "^" + go.Id + "~";
                    }
                }

                return obj;
            });

            SaveData("/" + Application.productName, "/FavoriteCamera.txt", () =>
            {
                string data = "";
                foreach (var item in favoriteCameras)
                {
                    data += (JsonUtility.ToJson(item) + "#");
                }

                return data;
            });
        }

        void SaveData(string directory, string fileName, Func<string> parseDataAction)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + directory;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path += fileName;
            string data = parseDataAction();
            StreamWriter writer = new StreamWriter(path);
            writer.WriteLine(data);
            writer.Close();
        }

        private void OnGUI()
        {
            ScrollPos = GUILayout.BeginScrollView(ScrollPos);

            GUILayout.BeginVertical();

            GUILayout.BeginVertical("Box");

            if (GUILayout.Button("Add Color"))
            {
                favoriteColors.Add(new FavoriteColor());
            }

            for (int i = 0; i < favoriteColors.Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.BeginVertical();
                GUILayout.Label((i + 1).ToString());
                favoriteColors[i].name = EditorGUILayout.TextField("name", favoriteColors[i].name);
                favoriteColors[i].color = EditorGUILayout.ColorField("color", favoriteColors[i].color);
                GUILayout.EndVertical();

                if (GUILayout.Button("Delete"))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Delete ?", "yes", "no"))
                    {
                        favoriteColors.RemoveAt(i);
                    }
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();

            GUILayout.Space(30);

            GUILayout.BeginVertical("Box");

            if (GUILayout.Button("Add Gameobject List"))
            {
                favoriteObjectList.Add(new GameobjectList("new list"));
            }

            for (int i = 0; i < favoriteObjectList.Count; i++)
            {
                GUILayout.BeginVertical("Box");

                GUILayout.BeginHorizontal();

                favoriteObjectList[i].Name = EditorGUILayout.TextField((i + 1).ToString() + ". list name", favoriteObjectList[i].Name);

                if (GUILayout.Button("Delete"))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Delete ?", "yes", "no"))
                    {
                        favoriteObjectList.RemoveAt(i);
                        break;
                    }
                }

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("add gameobject"))
                {
                    favoriteObjectList[i].Gameobjects.Add(new GameObjectType(0));
                }

                if (GUILayout.Button("copy gameobjects"))
                {
                    tempGameobjectDatas.Clear();
                    for (int t = 0; t < favoriteObjectList[i].Gameobjects.Count; t++)
                    {
                        tempGameobjectDatas.Add(new GameObjectType(favoriteObjectList[i].Gameobjects[t].Id));
                    }
                }

                GUILayout.EndHorizontal();

                propertyField.ShowListField("gameobjects", favoriteObjectList[i].Gameobjects, i.ToString());

                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();

            GUILayout.Space(30);

            GUILayout.Label("Favorite Camera Position Count : " + favoriteCameras.Count.ToString());

            for (int i = 0; i < favoriteCameras.Count; i++)
            {
                GUILayout.BeginVertical("Box");

                GUILayout.BeginHorizontal();
                GUILayout.Label((i + 1) + ". camera name ");
                favoriteCameras[i].name = EditorGUILayout.TextField(favoriteCameras[i].name, GUILayout.Width(400));

                if (GUILayout.Button("copy camera position"))
                {
                    tempCameraDestination = new CameraDestination(favoriteCameras[i].camDestination);
                }
                GUILayout.FlexibleSpace();

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("pos " + favoriteCameras[i].camDestination.position);
                GUILayout.Label(", rot " + favoriteCameras[i].camDestination.rotation);
                if (favoriteCameras[i].camDestination.target.gameObject != null)
                    GUILayout.Label(", target " + favoriteCameras[i].camDestination.target.gameObject.name);
                GUILayout.FlexibleSpace();

                if (GUILayout.Button("delete"))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Delete ?", "yes", "no"))
                    {
                        favoriteCameras.Remove(favoriteCameras[i]);
                    }
                }

                GUILayout.EndHorizontal();

                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();

            GUILayout.EndScrollView();

            GUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("save to -> " + saveFolder))
            {
                if (EditorUtility.DisplayDialog("favorite editor", "save data?", "yes", "no"))
                {
                    Save();
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}

#endif
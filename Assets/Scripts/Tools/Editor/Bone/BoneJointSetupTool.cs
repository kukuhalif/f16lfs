#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    public class BoneJointSetupTool : EditorWindow
    {
        [MenuItem("Virtual Training/Model/Bone Joint Setup")]
        public static void GetWindow()
        {
            GetWindow<BoneJointSetupTool>("Bone Joint Tool");
        }

        [MenuItem("Virtual Training/Model/Bone Joint Setup", true)]
        public static bool GetWindowValidation()
        {
            return !Application.isPlaying;
        }

        private GameObjectType boneObject;
        private GameObjectType root;
        private List<BoneJointData.Line> lines = new List<BoneJointData.Line>();

        private PropertyField propertyField;
        private Vector2 scrollPosition = new Vector2();

        private void OnEnable()
        {
            boneObject = new GameObjectType();
            root = new GameObjectType();
            propertyField = new PropertyField(null, "bone joint setup");
            propertyField.CreateListView("lines", OnLineFoldoutTextCallback, null, OnLineElementCallback, OnLineDetailCallback);
            propertyField.CreateListView("joints", null, null, OnJointElementCallback, null);
        }

        private object OnJointElementCallback(string listId, object element, int index)
        {
            GameObjectType joint = element as GameObjectType;

            GUILayout.BeginVertical();

            propertyField.ShowSimpleField("joint", listId + index, ref joint);

            GUILayout.EndVertical();

            return joint;
        }

        private string OnLineFoldoutTextCallback(object element)
        {
            BoneJointData.Line line = element as BoneJointData.Line;

            string text;
            text = line.startAnchor.gameObject == null ? "start anchor : empty" : "start anchor : " + line.startAnchor.gameObject.name;
            text += " | ";
            text += line.endAnchor.gameObject == null ? "end anchor : empty" : "end anchor : " + line.endAnchor.gameObject.name;

            return "";
        }

        private object OnLineElementCallback(string listId, object element, int index)
        {
            BoneJointData.Line line = element as BoneJointData.Line;

            GUILayout.Space(10);
            propertyField.ShowSimpleField("start anchor", ref line.startAnchor, 80, 180);
            GUILayout.Space(50);
            propertyField.ShowSimpleField("end anchor", ref line.endAnchor, 80, 180);

            return line;
        }

        private object OnLineDetailCallback(string listId, object element, int index)
        {
            BoneJointData.Line line = element as BoneJointData.Line;

            GUILayout.BeginVertical();

            //propertyField.ShowSimpleField("start anchor", "start" + listId + index, ref line.startAnchor);
            //propertyField.ShowSimpleField("end anchor", "end" + listId + index, ref line.endAnchor);

            propertyField.ShowListField("joints", line.joints, listId + index);

            GUILayout.EndVertical();

            return line;
        }

        private void OnGUI()
        {
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);

            GUILayout.BeginHorizontal();
            propertyField.ShowSimpleField("bone object", ref boneObject);
            if (GUILayout.Button("find root", GUILayout.Width(100)))
            {
                Transform[] childs = boneObject.gameObject.transform.GetAllChilds();
                foreach (var child in childs)
                {
                    if (child.name.ToLower() == "root")
                    {
                        root.Id = child.GetComponent<GameObjectReference>().Id;
                        break;
                    }
                }
            }
            GUILayout.EndHorizontal();
            propertyField.ShowSimpleField("root", ref root);

            EditorGUI.BeginDisabledGroup(boneObject.gameObject == null || root.gameObject == null);

            if (GUILayout.Button("Load Data From Bone Object"))
            {
                LoadDataFromRoot();
            }

            if (GUILayout.Button("Get Hierarchy"))
            {
                GetHierarchy();
            }

            if (GUILayout.Button("Add Joint Component & Setup"))
            {
                AddJointComponent();
                if (EditorUtility.DisplayDialog("bone joint tool", "use gravity ?", "yes", "no"))
                {
                    ComponentSetup(true);
                }
                else
                {
                    ComponentSetup(false);
                }
            }

            if (GUILayout.Button("Clear Component"))
            {
                ClearComponent();
            }

            if (GUILayout.Button("Save Data To Bone Object"))
            {
                SaveDataToBoneObject();
            }

            propertyField.ShowListField("lines", lines);

            EditorGUI.EndDisabledGroup();

            GUILayout.EndScrollView();
        }

        private void SaveDataToBoneObject()
        {
            BoneJointData data = boneObject.gameObject.GetComponent<BoneJointData>();
            if (data == null)
                data = boneObject.gameObject.AddComponent<BoneJointData>();
            data.SetData(lines);
        }

        private void LoadDataFromRoot()
        {
            BoneJointData data = boneObject.gameObject.GetComponent<BoneJointData>();
            if (data == null)
            {
                if (EditorUtility.DisplayDialog("bone joint tool", "no data in bone object", "ok"))
                {

                }
            }
            else
                lines = data.GetData();
        }

        private void GetHierarchy()
        {
            lines.Clear();
            if (root.gameObject.transform.childCount > 0)
            {
                BoneJointData.Line newLine = new BoneJointData.Line();
                lines.Add(newLine);
                GetLine(root.gameObject, newLine.joints);
            }
        }

        private void GetLine(GameObject current, List<GameObjectType> joints)
        {
            int id = current.GetComponent<GameObjectReference>().Id;
            GameObjectType newGot = new GameObjectType(id);
            joints.Add(newGot);

            if (current.transform.childCount > 0)
                GetLine(current.transform.GetChild(0).gameObject, joints);

            if (current.transform.childCount > 1)
            {
                for (int i = 1; i < current.transform.childCount; i++)
                {
                    BoneJointData.Line newLine = new BoneJointData.Line();
                    lines.Add(newLine);
                    GetLine(current.transform.GetChild(i).gameObject, newLine.joints);
                }
            }
        }

        private void ClearComponent(GameObject obj)
        {
            if (obj == null)
                return;

            ConfigurableJoint[] cjs = obj.GetComponents<ConfigurableJoint>();
            for (int i = 0; i < cjs.Length; i++)
            {
                DestroyImmediate(cjs[i]);
            }

            Rigidbody rb = obj.GetComponent<Rigidbody>();
            DestroyImmediate(rb);
        }

        private void ClearComponent()
        {
            for (int l = 0; l < lines.Count; l++)
            {
                ClearComponent(lines[l].startAnchor.gameObject);
                ClearComponent(lines[l].endAnchor.gameObject);

                for (int j = 0; j < lines[l].joints.Count; j++)
                {
                    ClearComponent(lines[l].joints[j].gameObject);
                }
            }
        }

        private void AddJointComponent(GameObject obj, bool isAnchor)
        {
            if (obj != null)
            {
                Rigidbody rb = obj.GetComponent<Rigidbody>();
                if (rb == null)
                {
                    obj.AddComponent<Rigidbody>();
                }

                if (!isAnchor)
                {
                    ConfigurableJoint[] cjs = obj.GetComponents<ConfigurableJoint>();
                    if (cjs.Length == 0)
                    {
                        obj.AddComponent<ConfigurableJoint>();
                        obj.AddComponent<ConfigurableJoint>();
                    }
                    else if (cjs.Length == 1)
                    {
                        obj.AddComponent<ConfigurableJoint>();
                    }
                }
            }
        }

        private void AddJointComponent()
        {
            for (int l = 0; l < lines.Count; l++)
            {
                AddJointComponent(lines[l].startAnchor.gameObject, true);
                AddJointComponent(lines[l].endAnchor.gameObject, true);

                for (int j = 0; j < lines[l].joints.Count; j++)
                {
                    AddJointComponent(lines[l].joints[j].gameObject, false);
                }
            }
        }

        private void ComponentSetup(bool useGravity)
        {
            for (int l = 0; l < lines.Count; l++)
            {
                Rigidbody startAnchor = lines[l].startAnchor.gameObject.GetComponent<Rigidbody>();
                Rigidbody endAnchor = lines[l].endAnchor.gameObject.GetComponent<Rigidbody>();

                startAnchor.isKinematic = true;
                endAnchor.isKinematic = true;

                startAnchor.useGravity = false;
                endAnchor.useGravity = false;

                ConfigurableJoint[] cjsStart = lines[l].joints[0].gameObject.GetComponents<ConfigurableJoint>();
                ConfigurableJoint[] cjsEnd = lines[l].joints[lines[l].joints.Count - 1].gameObject.GetComponents<ConfigurableJoint>();

                cjsStart[0].connectedBody = startAnchor;
                cjsStart[1].connectedBody = lines[l].joints[1].gameObject.GetComponent<Rigidbody>();

                cjsEnd[0].connectedBody = lines[l].joints[lines[l].joints.Count - 2].gameObject.GetComponent<Rigidbody>(); ;
                cjsEnd[1].connectedBody = endAnchor;

                for (int i = 0; i < cjsStart.Length; i++)
                {
                    cjsStart[i].xMotion = ConfigurableJointMotion.Limited;
                    cjsStart[i].yMotion = ConfigurableJointMotion.Limited;
                    cjsStart[i].zMotion = ConfigurableJointMotion.Limited;

                    cjsStart[i].angularXMotion = ConfigurableJointMotion.Limited;
                    cjsStart[i].angularYMotion = ConfigurableJointMotion.Limited;
                    cjsStart[i].angularZMotion = ConfigurableJointMotion.Limited;

                    cjsStart[i].rotationDriveMode = RotationDriveMode.Slerp;
                    cjsStart[i].projectionMode = JointProjectionMode.PositionAndRotation;

                    cjsStart[i].enableCollision = false;

                    JointDrive jd = new JointDrive();
                    jd.positionSpring = 100f;
                    jd.positionDamper = 100f;
                    jd.maximumForce = float.MaxValue;
                    cjsStart[i].slerpDrive = jd;
                }

                for (int i = 0; i < cjsEnd.Length; i++)
                {
                    cjsEnd[i].xMotion = ConfigurableJointMotion.Limited;
                    cjsEnd[i].yMotion = ConfigurableJointMotion.Limited;
                    cjsEnd[i].zMotion = ConfigurableJointMotion.Limited;

                    cjsEnd[i].angularXMotion = ConfigurableJointMotion.Limited;
                    cjsEnd[i].angularYMotion = ConfigurableJointMotion.Limited;
                    cjsEnd[i].angularZMotion = ConfigurableJointMotion.Limited;

                    cjsEnd[i].rotationDriveMode = RotationDriveMode.Slerp;
                    cjsEnd[i].projectionMode = JointProjectionMode.PositionAndRotation;

                    cjsEnd[i].enableCollision = false;

                    JointDrive jd = new JointDrive();
                    jd.positionSpring = 100f;
                    jd.positionDamper = 100f;
                    jd.maximumForce = float.MaxValue;
                    cjsEnd[i].slerpDrive = jd;
                }

                for (int j = 1; j < lines[l].joints.Count - 1; j++)
                {
                    ConfigurableJoint[] cjs = lines[l].joints[j].gameObject.GetComponents<ConfigurableJoint>();

                    for (int i = 0; i < cjs.Length; i++)
                    {
                        cjs[i].xMotion = ConfigurableJointMotion.Limited;
                        cjs[i].yMotion = ConfigurableJointMotion.Limited;
                        cjs[i].zMotion = ConfigurableJointMotion.Limited;

                        cjs[i].angularXMotion = ConfigurableJointMotion.Free;
                        cjs[i].angularYMotion = ConfigurableJointMotion.Free;
                        cjs[i].angularZMotion = ConfigurableJointMotion.Free;

                        cjs[i].projectionMode = JointProjectionMode.PositionAndRotation;

                        cjs[i].rotationDriveMode = RotationDriveMode.Slerp;

                        cjs[i].enableCollision = false;

                        JointDrive jd = new JointDrive();
                        jd.positionSpring = 100f;
                        jd.positionDamper = 100f;
                        jd.maximumForce = float.MaxValue;
                        cjs[i].slerpDrive = jd;
                    }

                    Rigidbody rbBefore = lines[l].joints[j - 1].gameObject.GetComponent<Rigidbody>();
                    rbBefore.isKinematic = false;
                    rbBefore.useGravity = useGravity;
                    cjs[0].connectedBody = rbBefore;

                    Rigidbody rbNext = lines[l].joints[j + 1].gameObject.GetComponent<Rigidbody>();
                    rbNext.isKinematic = false;
                    rbNext.useGravity = useGravity;
                    cjs[1].connectedBody = rbNext;
                }
            }
        }
    }
}

#endif
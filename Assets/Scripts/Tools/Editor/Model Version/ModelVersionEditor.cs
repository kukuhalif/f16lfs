#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEditor;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class ModelVersionEditor : EditorWindowDatabaseBase<ModelVersionDatabase, ModelVersionEditor, ModelVersion>
    {
        [MenuItem("Virtual Training/Model/Model Version")]
        public static void GetWindow()
        {
            ModelVersionEditor window = OpenEditorWindow();
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            ModelVersionEditor window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                return true;
            }
            return false;
        }

        PropertyField propertyField;
        Vector2 topScrollPos = Vector2.zero;
        Vector2 enabledScrollPos = Vector2.zero;
        Vector2 disabledScrollPos = Vector2.zero;
        int selectedModelVersion = 0;

        protected override void OnEnable()
        {
            base.OnEnable();

            propertyField = new PropertyField(null, () => WindowId);
            propertyField.CreateListView("version", null, null, VersionElementCallback, null, null);
            propertyField.CreateListView("enable", null, null, GameobjectElementCallback, null, null, false, typeof(GameObject));
            propertyField.CreateListView("disable", null, null, GameobjectElementCallback, null, null, false, typeof(GameObject));
        }

        private object GameobjectElementCallback(string listId, object element, int index)
        {
            GameObjectType gameObjectType = element as GameObjectType;

            propertyField.ShowSimpleField("gameobject", listId + index, ref gameObjectType);

            return gameObjectType;
        }

        private object VersionElementCallback(string listId, object element, int index)
        {
            MateriTypeModelVersion materiTypeModelVersion = element as MateriTypeModelVersion;

            string buttonText = materiTypeModelVersion.sceneMode.ToString() + " -> " + materiTypeModelVersion.materiType.ToString();

            if (selectedModelVersion == index)
                GUI.color = propertyField.SelectedElementColor;

            if (GUILayout.Button(buttonText, GUILayout.Width(160)))
            {
                selectedModelVersion = index;
            }

            GUI.color = Color.white;

            propertyField.ShowSimpleField("scene mode", listId + index + 0, ref materiTypeModelVersion.sceneMode, 80, 200);
            propertyField.ShowSimpleField("materi type", listId + index + 1, ref materiTypeModelVersion.materiType, 80, 200);

            return materiTypeModelVersion;
        }

        protected override void RenderGUI()
        {
            var data = ScriptableObjectTemp.GetData();

            GUILayout.BeginVertical(EditorStyles.helpBox);
            topScrollPos = GUILayout.BeginScrollView(topScrollPos, GUILayout.MaxHeight(250));
            GUILayout.Label("model version list, selected = " + (selectedModelVersion + 1).ToString());
            propertyField.ShowListField("version", data.materiTypeModelVersions);
            GUILayout.EndScrollView();
            GUILayout.EndVertical();

            if (selectedModelVersion >= 0 && selectedModelVersion < data.materiTypeModelVersions.Count)
            {
                GUILayout.BeginHorizontal();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                enabledScrollPos = GUILayout.BeginScrollView(enabledScrollPos, GUILayout.MaxWidth(800));
                GUILayout.Label("to enable object");
                propertyField.ShowListField("enable", data.materiTypeModelVersions[selectedModelVersion].enableObjects);
                GUILayout.EndScrollView();
                GUILayout.EndVertical();

                GUILayout.BeginVertical(EditorStyles.helpBox);
                disabledScrollPos = GUILayout.BeginScrollView(disabledScrollPos, GUILayout.MaxWidth(800));
                GUILayout.Label("to disable object");
                propertyField.ShowListField("disable", data.materiTypeModelVersions[selectedModelVersion].disableObjects);
                GUILayout.EndScrollView();
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }

            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("load"))
            {
                if (EditorUtility.DisplayDialog("model version", "load ?", "ok", "cancel"))
                    Load();
            }

            if (GUILayout.Button("save"))
            {
                if (EditorUtility.DisplayDialog("model version", "save ?", "ok", "cancel"))
                {
                    Save();
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}

#endif
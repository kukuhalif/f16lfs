#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class PlayTestShortcut
    {
        private static string PRESSED_WINDOW_ID;
        public static string PressedWindowId { get => PRESSED_WINDOW_ID; }
        public static void Played()
        {
            PRESSED_WINDOW_ID = "";
        }

        [MenuItem("Virtual Training Shortcut/Play Test &t", false)]
        static void Play()
        {
            PRESSED_WINDOW_ID = VirtualTrainingEditorState.GetLastFocusedWindowId();
        }

        [MenuItem("Virtual Training Shortcut/Play Test &t", true)]
        static bool PlayShortcutVerify()
        {
            return Application.isPlaying && !string.IsNullOrEmpty(VirtualTrainingEditorState.GetLastFocusedWindowId());
        }
    }
}
#endif
﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class AnimationDataTemp
    {
        public Vector3 startPosition;
        public Vector3 endPosition;
        public Vector3 startRotation;
        public Vector3 endRotation;
    }

    public class ObjectStartData
    {
        public Transform gameObject;
        public Vector3 startPosition;
        public Vector3 startRotation;
    }

    public class ScriptedAnimationContentEditor : PropertyField
    {
        bool startRecordPressed = false;
        //Vector3 _startPos;
        //Vector3 _startRotation;
        //float objectID;
        public AnimationDataTemp dataTemp;
        public List<ObjectStartData> objectStartDatas = new List<ObjectStartData>();
        bool isPreview;
        bool isPlayAnimation;
        bool isPlayEditor;
        float sliderValue;
        Action _beforeModifiedCallback;

        public ScriptedAnimationContentEditor(Action beforeModifiedCallback, Func<string> windowId) : base(beforeModifiedCallback, windowId)
        {
            _beforeModifiedCallback = beforeModifiedCallback;
            Initialize();
        }

        void Initialize()
        {
            CreateListView("Keyframe Script List", OnDrawHeader, null, OnDrawElement, null);
            CreateListView("Keyframe Script List Global", OnDrawHeader, null, OnDrawElementGlobal, null);
            var objectRoot = VirtualTrainingSceneManager.GameObjectRoot;
            if (objectRoot != null)
            {
                objectStartDatas.Clear();
                GetGameObject(objectRoot.transform);
            }
            else
            {
                Debug.LogError("object root not found");
            }
        }

        void GetGameObject(Transform objectRoot)
        {
            ObjectStartData objectStart = new ObjectStartData();
            objectStart.gameObject = objectRoot;
            objectStart.startPosition = objectRoot.gameObject.transform.position;
            objectStart.startRotation = objectRoot.gameObject.transform.localEulerAngles;
            objectStartDatas.Add(objectStart);

            for (int i = 0; i < objectRoot.childCount; i++)
            {
                GetGameObject(objectRoot.GetChild(i).gameObject.transform);
            }
        }

        string OnDrawHeader(object element)
        {
            SADataKeyframe figure = element as SADataKeyframe;
            return figure.duration.ToString();
        }

        object OnDrawElement(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            SADataKeyframe kc = element as SADataKeyframe;

            if (GUILayout.Button(kc.isStartRecording ? "Stop Record" : "Start Record"))
            {
                if (Selection.gameObjects.Length > 0)
                {
                    kc.isStartRecording = !kc.isStartRecording;
                    if (kc.isStartRecording)
                    {
                        dataTemp = new AnimationDataTemp();
                        startRecordPressed = true;
                        dataTemp.startPosition = Selection.gameObjects[0].transform.localPosition;
                        dataTemp.startRotation = Selection.gameObjects[0].transform.localEulerAngles;
                    }
                    else
                    {
                        if (startRecordPressed == true)
                        {
                            dataTemp.endPosition = Selection.gameObjects[0].transform.localPosition;
                            kc.deltaPosition = dataTemp.endPosition - dataTemp.startPosition;

                            dataTemp.endRotation = Selection.gameObjects[0].transform.localEulerAngles;
                            kc.deltaRotation = dataTemp.endRotation - dataTemp.startRotation;
                            startRecordPressed = false;
                        }
                    }
                }
                else
                {
                    if (EditorUtility.DisplayDialog("object not selection", "Select Object First", "Ok")) { }
                }

            }

            ShowSimpleField("Duration", listId + index + 1, ref kc.duration);

            EditorGUILayout.LabelField("Delta Position");
            EditorGUILayout.BeginVertical();
            ShowSimpleField("   X", listId + index + 3, ref kc.deltaPosition.x);
            ShowSimpleField("   Y", listId + index + 4, ref kc.deltaPosition.y);
            ShowSimpleField("   Z", listId + index + 5, ref kc.deltaPosition.z);
            EditorGUILayout.EndVertical();

            EditorGUILayout.LabelField("Delta Rotation");
            EditorGUILayout.BeginVertical();
            ShowSimpleField("   X", listId + index + 6, ref kc.deltaRotation.x);
            ShowSimpleField("   Y", listId + index + 7, ref kc.deltaRotation.y);
            ShowSimpleField("   Z", listId + index + 8, ref kc.deltaRotation.z);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndVertical();
            return kc;
        }

        object OnDrawElementGlobal(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            SADataKeyframe kc = element as SADataKeyframe;


            ShowSimpleField("Duration", listId + index + 1, ref kc.duration);

            EditorGUILayout.LabelField("Delta Position");
            EditorGUILayout.BeginVertical();
            ShowSimpleField("   X", listId + index + 3, ref kc.deltaPosition.x);
            ShowSimpleField("   Y", listId + index + 4, ref kc.deltaPosition.y);
            ShowSimpleField("   Z", listId + index + 5, ref kc.deltaPosition.z);
            EditorGUILayout.EndVertical();

            EditorGUILayout.LabelField("Delta Rotation");
            EditorGUILayout.BeginVertical();
            ShowSimpleField("   X", listId + index + 6, ref kc.deltaRotation.x);
            ShowSimpleField("   Y", listId + index + 7, ref kc.deltaRotation.y);
            ShowSimpleField("   Z", listId + index + 8, ref kc.deltaRotation.z);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndVertical();
            return kc;
        }

        public void ShowContent(ScriptedAnimationDataModel scriptedAnimationData, int index)
        {
            EditorGUILayout.BeginVertical();
            if (GUILayout.Button("Reset"))
            {
                ResetAllTransform();
            }
            if (Application.isPlaying)
                isPlayEditor = true;
            else
                isPlayEditor = false;

            EditorGUI.BeginDisabledGroup(isPlayEditor == false);
            if (GUILayout.Button(isPreview ? "Stop Preview" : "Preview Animation"))
            {
                if (Selection.gameObjects.Length > 0)
                {
                    isPreview = !isPreview;
                    if (isPreview)
                    {
                        AnimationPlayerScript anim = Selection.gameObjects[0].GetComponent<AnimationPlayerScript>();
                        if (anim == null)
                            anim = Selection.gameObjects[0].AddComponent<AnimationPlayerScript>();

                        for (int i = 0; i < scriptedAnimationData.animData.Count; i++)
                        {
                            anim.scriptedAnimationDatas.Add(scriptedAnimationData.animData[i]);
                        }
                        anim.isAnimatedScript = true;
                        anim.SetupAnimationScriptTools(scriptedAnimationData.globalCoordinate);
                    }
                    else
                    {
                        sliderValue = 0;
                        AnimationPlayerScript anim = Selection.gameObjects[0].GetComponent<AnimationPlayerScript>();
                        if (anim != null)
                        {
                            anim.DestroyItself();
                        }
                    }
                }
                else
                    if (EditorUtility.DisplayDialog("object not selection", "Select Object First", "Ok", "Cancel")) { }
            }
            EditorGUI.BeginDisabledGroup(isPreview == false);
            if (GUILayout.Button(isPlayAnimation ? "Stop Animation" : "Play Animation"))
            {
                if (Selection.gameObjects.Length > 0)
                {
                    isPlayAnimation = !isPlayAnimation;
                    AnimationPlayerScript anim = Selection.gameObjects[0].GetComponent<AnimationPlayerScript>();
                    if (anim != null)
                    {
                        if (isPlayAnimation)
                        {
                            if (sliderValue >= 1)
                                anim.SetSliderPercentage(0);

                            anim.StartPlayAnimation(1, anim.getVBOutputData.speed);
                        }
                        else
                            anim.StopAnimation();
                    }
                }
                else
                    if (EditorUtility.DisplayDialog("object not selection", "Select Object First", "Ok", "Cancel")) { }
            }

            if (isPreview)
            {
                if (Selection.gameObjects.Length > 0)
                {
                    AnimationPlayerScript anim = Selection.gameObjects[0].GetComponent<AnimationPlayerScript>();

                    if (anim != null)
                    {
                        if (isPlayAnimation)
                        {
                            sliderValue = anim.sliderValue;
                            if (sliderValue >= 1)
                            {
                                //sliderValue = 0;
                                isPlayAnimation = false;
                            }
                        }
                        else
                        {
                            anim.StartPlayAnimation(sliderValue, anim.getVBOutputData.speed);
                            anim.SetSliderPercentage(sliderValue);
                        }
                    }
                }
            }

            sliderValue = EditorGUILayout.Slider(sliderValue, 0, 1);
            EditorGUI.EndDisabledGroup();
            EditorGUI.EndDisabledGroup();
            ShowSimpleField("Global Coordinate", ref scriptedAnimationData.globalCoordinate);
            if (scriptedAnimationData.globalCoordinate)
                ShowListField("Keyframe Script List Global", scriptedAnimationData.animDataGlobal);
            else
                ShowListField("Keyframe Script List", scriptedAnimationData.animData);
            EditorGUILayout.EndVertical();
        }

        public void ResetAllTransform()
        {
            if (objectStartDatas.Count > 0)
            {
                for (int i = 0; i < objectStartDatas.Count; i++)
                {
                    objectStartDatas[i].gameObject.transform.position = objectStartDatas[i].startPosition;
                    objectStartDatas[i].gameObject.transform.localEulerAngles = objectStartDatas[i].startRotation;
                }
            }
        }
    }
}
#endif

﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    class ScriptedAnimationEditorWindow : TreeViewEditorBase<ScriptedAnimationTreeAsset, ScriptedAnimationEditorWindow, ScriptedAnimationData, ScriptedAnimationTreeElement>
    {
        ScriptedAnimationContentEditor scriptedAnimationContentEditor;

        [MenuItem("Virtual Training/Materi/Scripted Animation Editor &a")]
        public static void GetWindow()
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
                return true;
            }
            return false;
        }

        public static ScriptedAnimationEditorWindow GetWindow(int instanceID)
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Initialize();
            }
            return window;
        }

        public static ScriptedAnimationEditorWindow GetWindow(int instanceId, bool newWindow = false, bool alwaysInitialize = true)
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow(instanceId, newWindow);
            if (window != null)
            {
                if (alwaysInitialize || window.TreeView == null)
                {
                    window.Load();
                    window.Initialize();
                }
                return window;
            }
            return null;
        }

        protected override TreeViewWithTreeModel<ScriptedAnimationTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<ScriptedAnimationTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<ScriptedAnimationTreeElement>(TreeViewState, treeModel);
        }

        protected override ScriptedAnimationTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new ScriptedAnimationTreeElement("new scripted animation", depth, id, false);
        }


        protected override void Initialize()
        {
            if (scriptedAnimationContentEditor != null)
                scriptedAnimationContentEditor.ResetAllTransform();

            base.Initialize();

            scriptedAnimationContentEditor = new ScriptedAnimationContentEditor(RecordUndo, () => WindowId);
        }

        protected override void OnDisable()
        {
            if (scriptedAnimationContentEditor != null)
                scriptedAnimationContentEditor.ResetAllTransform();
            base.OnDisable();
        }

        protected override void ContentView(List<ScriptedAnimationTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();
            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder && scriptedAnimationContentEditor != null)
                    scriptedAnimationContentEditor.ShowContent(selectedElements[0].data, 0);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        scriptedAnimationContentEditor.ShowContent(selectedElements[i].data, i);
                }
            }

            EditorGUILayout.EndVertical();
        }

        protected override bool UseTopLeftButton()
        {
            return true;
        }

        private void OpenDatabase(int elementId)
        {
            var databases = DatabaseManager.GetScriptedAnimationDatabases();
            for (int d = 0; d < databases.Count; d++)
            {
                var elements = databases[d].GetData().treeElements;
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elementId == elements[i].id)
                    {
                        if (InstanceId != databases[d].GetInstanceID())
                        {
                            Close();
                            var window = GetWindow(databases[d].GetInstanceID(), false);
                            window.TreeView.SetSelection(new List<int> { elementId });
                        }
                        else
                        {
                            TreeView.SetSelection(new List<int> { elementId });
                        }
                        break;
                    }
                }
            }
        }

        protected override void TopLeftButtonAction()
        {
            var animationDatabase = DatabaseManager.GetScriptedAnimationDatabases();
            var animationTree = DatabaseManager.GetScriptedAnimationTree();

            List<ScriptedAnimationTreeElement> animationList = new List<ScriptedAnimationTreeElement>();
            TreeElementUtility.TreeToList(animationTree, animationList);

            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("edit content config"), false, () =>
            {
                VirtualTrainingContentConfigEditor.ShowContentConfig();
            });

            for (int i = 0; i < animationDatabase.Count; i++)
            {
                ScriptedAnimationTreeAsset asset = animationDatabase[i];
                if (InstanceId != asset.GetInstanceID())
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " open -> " + asset.name), false, () =>
                    {
                        Close();
                        GetWindow(asset.GetInstanceID());
                    });
                }
                else
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " current animation -> " + asset.name), false, null);
                }
            }

            for (int i = 0; i < animationList.Count; i++)
            {
                string name = animationList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && animationList[i].depth > -1)
                {
                    int elementId = animationList[i].id;
                    menu.AddItem(new GUIContent("all content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            menu.ShowAsContext();
        }
    }
}

#endif

#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Timeline;
using UnityEditor.Timeline;
using VirtualTraining.Core;
using System.Linq;
using NUnit.Framework;

namespace VirtualTraining.Tools
{
    public class FlightScenarioContentEditor : ContentEditorBase<FlightScenarioFigure>
    {
        Action applyOverrideCallback;
        FlightScenarioDataModel playedFlightScenario;
        //TimelineAsset scenarioTimeline;

        int currentFigureIndex = 0;
        int currentCameraIndex = 0;

        public FlightScenarioContentEditor(Action applyOverrideCallback, Action beforeModifiedCallback, Func<string> windowId, List<Material> xrayMats) :
            base(beforeModifiedCallback, windowId, xrayMats,
                new CategoryTab[] { CategoryTab.FIGURE_LIST, CategoryTab.ANIMATION, CategoryTab.VIRTUAL_BUTTON },
                new FigureTab[] { FigureTab.CAMERA, FigureTab.PART_OBJECT, FigureTab.CALLOUT, FigureTab.HELPER, FigureTab.CUTAWAY })
        {
            this.applyOverrideCallback = applyOverrideCallback;
        }

        protected override FlightScenarioFigure GetFigure(object data, int index)
        {
            FlightScenarioDataModel flightScenario = data as FlightScenarioDataModel;
            return flightScenario.figures[index];
        }

        protected override int GetFigureCount(object data)
        {
            FlightScenarioDataModel flightScenario = data as FlightScenarioDataModel;
            return flightScenario.figures.Count;
        }

        protected override List<Schematic> GetSchematicFromData(object data)
        {
            return null;
        }

        protected override void ShowFiguresField(object data, string listName, string fieldId)
        {
            FlightScenarioDataModel flightScenario = data as FlightScenarioDataModel;
            ShowListField(listName, flightScenario.figures, fieldId);
        }

        protected override void CameraTab(object data, string fieldId)
        {
            FlightScenarioFigure figure = data as FlightScenarioFigure;

            ShowReferenceField<FlightScenarioElementType, FlightScenarioTreeElement>("overide camera", fieldId + "override cam", ref figure.cameraOverrideSource);

            if (figure.cameraOverrideSource.GetId() != -1)
            {
                var figures = figure.cameraOverrideSource.GetData().figures;
                List<string> figOption = new List<string>();
                for (int i = 0; i < figures.Count; i++)
                {
                    figOption.Add(figures[i].name);
                }
                if (figures.Count > figure.cameraOverrideSourceFigureIndex)
                    figure.cameraOverrideSourceFigureIndex = ShowDropdownField("select figure source", fieldId + "figure select", figures[figure.cameraOverrideSourceFigureIndex].name, figOption);
                else
                    figure.cameraOverrideSourceFigureIndex = figures.Count - 1;
            }

            if (figure.cameraOverrideSource.GetId() == -1)
            {
                base.CameraTab(data, fieldId);
            }
        }

        protected override void PartObjectTab(object data, string fieldId)
        {
            FlightScenarioFigure figure = data as FlightScenarioFigure;

            ShowReferenceField<FlightScenarioElementType, FlightScenarioTreeElement>("overide part object", fieldId + "override part obj", ref figure.partObjectOverrideSource);

            if (figure.partObjectOverrideSource.GetId() != -1)
            {
                var figures = figure.partObjectOverrideSource.GetData().figures;
                List<string> figOption = new List<string>();
                for (int i = 0; i < figures.Count; i++)
                {
                    figOption.Add(figures[i].name);
                }
                if (figures.Count > figure.partObjectOverrideSourceFigureIndex)
                    figure.partObjectOverrideSourceFigureIndex = ShowDropdownField("select figure source", fieldId + "figure select", figures[figure.partObjectOverrideSourceFigureIndex].name, figOption);
                else
                    figure.partObjectOverrideSourceFigureIndex = figures.Count - 1;
            }

            if (figure.partObjectOverrideSource.GetId() == -1)
            {
                base.PartObjectTab(data, fieldId);
            }
        }

        protected override void TopDetailInspector(object data, Rect panelRect)
        {
            FlightScenarioTreeElement flightScenarioTreeElement = data as FlightScenarioTreeElement;
            FlightScenarioDataModel flightScenarioData = flightScenarioTreeElement.data;

            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test flight scenario steps (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("Prepare Play Flight Scenario"))
                {
                    EventManager.TriggerEvent(new FlightScenarioPreparePlayEvent());
                }

                if (GUILayout.Button("play flight scenario step (alt + t)") || PlayTestShortcut.PressedWindowId == WindowId)
                {
                    PlayTestShortcut.Played();
                    //applyOverrideCallback.Invoke();
                    playedFlightScenario = flightScenarioData;
                    currentFigureIndex = 0;
                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new FlightScenarioPlayEvent(flightScenarioTreeElement));
                }

                if (playedFlightScenario == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("reset flight scenario"))
                {
                    playedFlightScenario = null;
                    EventManager.TriggerEvent(new FlightScenarioPlayEvent());
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous figure"))
                {
                    currentFigureIndex--;
                    if (currentFigureIndex < 0)
                        currentFigureIndex = playedFlightScenario.figures.Count - 1;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PlayFigureLinkEvent(playedFlightScenario.figures[currentFigureIndex].name, MateriElementType.FlightScenario, playedFlightScenario));
                }

                if (GUILayout.Button("play next figure"))
                {
                    currentFigureIndex++;
                    if (playedFlightScenario.figures.Count <= currentFigureIndex)
                        currentFigureIndex = 0;

                    currentCameraIndex = 0;
                    EventManager.TriggerEvent(new PlayFigureLinkEvent(playedFlightScenario.figures[currentFigureIndex].name, MateriElementType.FlightScenario, playedFlightScenario));
                }

                GUILayout.Label("current figure : " + currentFigureIndex);

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous camera"))
                {
                    currentCameraIndex--;
                    if (currentCameraIndex < 0)
                        currentCameraIndex = playedFlightScenario.figures[currentFigureIndex].cameraDestinations.Count - 1;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedFlightScenario.figures[currentFigureIndex].name, playedFlightScenario.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.FlightScenario, playedFlightScenario));
                }

                if (GUILayout.Button("play next camera"))
                {
                    currentCameraIndex++;
                    if (playedFlightScenario.figures[currentFigureIndex].cameraDestinations.Count <= currentCameraIndex)
                        currentCameraIndex = 0;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedFlightScenario.figures[currentFigureIndex].name, playedFlightScenario.figures[currentFigureIndex].cameraDestinations[currentCameraIndex].displayName, MateriElementType.FlightScenario, playedFlightScenario));
                }

                GUILayout.Label("current camera : " + currentCameraIndex);

                GUILayout.EndHorizontal();

                if (playedFlightScenario == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);
            ShowSimpleField("flight scenario step name", ref flightScenarioData.title);
            //ShowAssetField("recorded flight data", ref flightScenarioData.recordedFlightData);
            //ShowAssetField("scenario timeline", ref flightScenarioData.scenarioTimeline);
            ShowReferenceField<FlightScenarioTimelineElementType, FlightScenarioTimelineTreeElement>("scenario timeline", ref flightScenarioData.flightScenarioTimelineElmtType);

            if (flightScenarioData.flightScenarioTimelineElmtType != null)
            {
                FlightScenarioTimelineDataModel flightScenarioTimelineData = flightScenarioData.flightScenarioTimelineElmtType.GetData();
                if (flightScenarioTimelineData != null)
                {
                    if (flightScenarioTimelineData.scenarioTimeline != null)
                    {
                        List<StepMarker> stepMarkers = new List<StepMarker>();
                        stepMarkers = flightScenarioTimelineData.scenarioTimeline.markerTrack.GetMarkers().OfType<StepMarker>().ToList();

                        List<string> stepMarkerNames = new List<string>();
                        foreach (var stepMarker in stepMarkers)
                        {
                            stepMarkerNames.Add(stepMarker.name);
                        }
                        flightScenarioData.markerNum = ShowDropdownField("selected marker", stepMarkerNames[flightScenarioData.markerNum], stepMarkerNames);
                        //flightScenarioData.markerName = stepMarkerNames[flightScenarioData.markerNum];
                    }
                    else
                    {
                        ShowDropdownField("selected marker", "no marker found!", new List<string>());
                    }
                }
            }

            ShowTextArea("description", panelRect.width - 15f, ref flightScenarioData.description, true);
            //ShowSimpleField("marker name", ref flightScenarioData.markerName);
            //ShowSimpleField("marker number", ref flightScenarioData.markerNum);
            //ShowSimpleField("step start frame", ref flightScenarioData.startFrame);
            //ShowSimpleField("step stop frame", ref flightScenarioData.stopFrame);
        }

        protected override void BottomDetailInspector(object data, Rect panelRect)
        {
            FlightScenarioTreeElement flightScenarioTreeElement = data as FlightScenarioTreeElement;
            ShowTab("categories", flightScenarioTreeElement.data);
        }
    }
}

#endif

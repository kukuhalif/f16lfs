#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(FlightScenarioTreeAsset))]
    public class FlightScenarioTreeAssetEditor : TreeAssetEditorBase<FlightScenarioTreeAsset, FlightScenarioTreeElement, FlightScenarioData>
    {
        protected override FlightScenarioTreeAsset GetAsset()
        {
            return (FlightScenarioTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<FlightScenarioData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<FlightScenarioTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}
#endif

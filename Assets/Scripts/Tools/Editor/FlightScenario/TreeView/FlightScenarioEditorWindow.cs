#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEngine.Timeline;
using UnityEditor.Timeline;
using System.Collections.Generic;
using VirtualTraining.Core;


namespace VirtualTraining.Tools
{

    class FlightScenarioEditorWindow : TreeViewEditorBase<FlightScenarioTreeAsset, FlightScenarioEditorWindow, FlightScenarioData, FlightScenarioTreeElement>
    {
        FlightScenarioContentEditor flightScenarioContentEditor;

        public static void GetWindow()
        {
            FlightScenarioEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }

        //[OnOpenAsset]
        public static FlightScenarioEditorWindow OnOpenAsset(int instanceID)
        {
            FlightScenarioEditorWindow window = OpenEditorWindow(instanceID, true);
            if (window != null)
            {
                window.Initialize();
                return window;
            }

            return null;
        }

        protected override TreeViewWithTreeModel<FlightScenarioTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<FlightScenarioTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<FlightScenarioTreeElement>(TreeViewState, treeModel);
        }

        protected override FlightScenarioTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new FlightScenarioTreeElement("New Flight Scenario Data", depth, id, false);
        }

        protected override void Initialize()
        {
            // if scriptable object removed
            if (ScriptableObjectOriginalFile == null)
                return;

            base.Initialize();
            var xrayMats = DatabaseManager.GetXrayMaterials();

            flightScenarioContentEditor = new FlightScenarioContentEditor(ApplyOverrideData, RecordUndo, () => WindowId, xrayMats);
        }

        protected override void OnDisable()
        {
            ApplyOverrideData();
            base.OnDisable();

            if (flightScenarioContentEditor != null)
                flightScenarioContentEditor.SaveCameraDestination();
        }

        protected override void Save()
        {
            ApplyOverrideData();
            base.Save();
        }

        private void ApplyOverrideData()
        {
            var trees = ScriptableObjectTemp.GetData().treeElements;
            for (int i = 0; i < trees.Count; i++)
            {
                for (int f = 0; f < trees[i].data.figures.Count; f++)
                {
                    if (trees[i].data.figures[f].cameraOverrideSource.GetId() != -1)
                    {
                        trees[i].data.figures[f].cameraDestinations.Clear();
                        var cams = trees[i].data.figures[f].cameraOverrideSource.GetData().figures[trees[i].data.figures[f].cameraOverrideSourceFigureIndex].cameraDestinations;
                        for (int s = 0; s < cams.Count; s++)
                        {
                            trees[i].data.figures[f].cameraDestinations.Add(VirtualTrainingEditorUtility.CloneObject(cams[s]));
                        }
                    }

                    if (trees[i].data.figures[f].partObjectOverrideSource.GetId() != -1)
                    {
                        trees[i].data.figures[f].partObjects.Clear();
                        var parts = trees[i].data.figures[f].partObjectOverrideSource.GetData().figures[trees[i].data.figures[f].partObjectOverrideSourceFigureIndex].partObjects;
                        for (int s = 0; s < parts.Count; s++)
                        {
                            trees[i].data.figures[f].partObjects.Add(VirtualTrainingEditorUtility.CloneObject(parts[s]));
                        }
                    }
                }
            }
        }

        protected override void ContentView(List<FlightScenarioTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder && flightScenarioContentEditor != null)
                    flightScenarioContentEditor.DetailInspector(selectedElements[0].id, selectedElements[0], detailPanelRect, true);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        selectedElements[i].name = flightScenarioContentEditor.PreviewInspector(selectedElements[i].name, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "flight scenario name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].name = selectedElements[i].data.title;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].data.title = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            EditorGUILayout.EndVertical();
        }
    }
}

#endif
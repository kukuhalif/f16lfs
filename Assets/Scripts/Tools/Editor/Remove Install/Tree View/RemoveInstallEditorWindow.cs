#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class RemoveInstallEditorWindow : TreeViewEditorBase<RemoveInstallTreeAsset, RemoveInstallEditorWindow, RemoveInstallData, RemoveInstallTreeElement>
    {
        RemoveInstallContentEditor removeInstallContentEditor;

        public static void GetWindow()
        {
            RemoveInstallEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Initialize();
            }
        }
        public static RemoveInstallEditorWindow OnOpenAsset(int instanceID)
        {
            RemoveInstallEditorWindow window = OpenEditorWindow(instanceID, true);
            if (window != null)
            {
                window.Initialize();
                return window;
            }

            return null;
        }

        protected override void Initialize()
        {
            // if scriptable object removed
            if (ScriptableObjectOriginalFile == null)
                return;

            base.Initialize();

            var xray = DatabaseManager.GetXrayMaterials();
            removeInstallContentEditor = new RemoveInstallContentEditor(RecordUndo, () => WindowId, xray);
        }

        protected override TreeViewWithTreeModel<RemoveInstallTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<RemoveInstallTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<RemoveInstallTreeElement>(TreeViewState, treeModel);
        }

        protected override RemoveInstallTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new RemoveInstallTreeElement("New Remove Install Data", depth, id, false);
        }

        protected override void ContentView(List<RemoveInstallTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder && removeInstallContentEditor != null)
                    removeInstallContentEditor.DetailInspector(selectedElements[0].id, selectedElements[0], detailPanelRect, true);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        selectedElements[i].name = removeInstallContentEditor.PreviewInspector(selectedElements[i].name, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "remove install name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].name = selectedElements[i].data.title;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].data.title = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            EditorGUILayout.EndVertical();
        }
    }
}

#endif
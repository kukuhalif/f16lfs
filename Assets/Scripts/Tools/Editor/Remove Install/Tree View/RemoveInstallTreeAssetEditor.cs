#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(RemoveInstallTreeAsset))]
    public class RemoveInstallTreeAssetEditor : TreeAssetEditorBase<RemoveInstallTreeAsset, RemoveInstallTreeElement, RemoveInstallData>
    {
        protected override RemoveInstallTreeAsset GetAsset()
        {
            return (RemoveInstallTreeAsset)target;
        }

        protected override ScriptableObjectTreeBase<RemoveInstallData> GetScriptableObject()
        {
            return GetAsset();
        }

        protected override List<RemoveInstallTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}
#endif

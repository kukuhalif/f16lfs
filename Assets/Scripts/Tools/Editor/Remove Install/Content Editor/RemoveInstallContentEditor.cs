#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class RemoveInstallContentEditor : ContentEditorBase<RemoveInstallFigure>
    {
        RemoveInstallTreeElement playedRemoveInstall;
        int currentCameraIndex;

        public RemoveInstallContentEditor(Action beforeModifiedCallback, Func<string> windowId, List<Material> xrayMats) :
            base(beforeModifiedCallback, windowId, xrayMats,
                new CategoryTab[] { CategoryTab.FIGURE, CategoryTab.ANIMATION },
                new FigureTab[] { FigureTab.CAMERA })
        {
            CreateListView("target", null, null, OnGameobjectTargetDrawCallback, null, null, false, typeof(GameObject));
        }

        private object OnGameobjectTargetDrawCallback(string listId, object element, int index)
        {
            GameObjectType target = element as GameObjectType;
            ShowSimpleField("gameobject", listId + index, ref target);
            return target;
        }

        protected override RemoveInstallFigure GetFigure(object data, int index)
        {
            RemoveInstallDataModel ridm = data as RemoveInstallDataModel;
            return ridm.figure;
        }

        protected override int GetFigureCount(object data)
        {
            // figure is only one
            return 1;
        }

        protected override List<Schematic> GetSchematicFromData(object data)
        {
            // no schematic data
            return null;
        }

        protected override void ShowFiguresField(object data, string listName, string fieldId)
        {
            // show only one figure
            RemoveInstallDataModel ridm = data as RemoveInstallDataModel;
            RemoveInstallFigure figure = ridm.figure;

            ShowSimpleField("figure name", "figure name" + fieldId + 1, ref figure.name);
            ShowAssetField("audio clip", fieldId + 2, ref figure.clip);
            ShowListField("target", figure.targets, fieldId + 3);
            ShowTab("figure category", figure, fieldId + 4);
        }

        protected override void TopDetailInspector(object data, Rect panelRect)
        {
            RemoveInstallTreeElement rite = data as RemoveInstallTreeElement;

            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("play remove"))
                {
                    playedRemoveInstall = rite;
                    currentCameraIndex = 0;
                    var tree = rite.Root();
                    EventManager.TriggerEvent(new RemoveInstallPlayEvent((RemoveInstallTreeElement)tree, playedRemoveInstall, true));
                }

                if (GUILayout.Button("play install"))
                {
                    playedRemoveInstall = rite;
                    currentCameraIndex = 0;
                    var tree = rite.Root();
                    EventManager.TriggerEvent(new RemoveInstallPlayEvent((RemoveInstallTreeElement)tree, playedRemoveInstall, false));
                }
                GUILayout.EndHorizontal();

                if (playedRemoveInstall == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("reset remove install"))
                {
                    playedRemoveInstall = null;
                    EventManager.TriggerEvent(new RemoveInstallPlayEvent());
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous camera"))
                {
                    currentCameraIndex--;
                    if (currentCameraIndex < 0)
                        currentCameraIndex = playedRemoveInstall.data.figure.cameraDestinations.Count - 1;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedRemoveInstall.name, playedRemoveInstall.data.figure.cameraDestinations[currentCameraIndex].displayName, MateriElementType.RemoveInstall, playedRemoveInstall));
                }

                if (GUILayout.Button("play next camera"))
                {
                    currentCameraIndex++;
                    if (playedRemoveInstall.data.figure.cameraDestinations.Count <= currentCameraIndex)
                        currentCameraIndex = 0;

                    EventManager.TriggerEvent(new PlayCameraFigureLinkEvent(playedRemoveInstall.name, playedRemoveInstall.data.figure.cameraDestinations[currentCameraIndex].displayName, MateriElementType.RemoveInstall, playedRemoveInstall));
                }

                GUILayout.Label("current camera : " + currentCameraIndex);

                GUILayout.EndHorizontal();

                if (playedRemoveInstall == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);

            ShowSimpleField("remove install name", ref rite.data.title);
            ShowTextArea("remove description", panelRect.width - 15f, ref rite.data.removeDescription, true, 120f);
            ShowTextArea("install description", panelRect.width - 15f, ref rite.data.installDescription, true, 120f);
        }

        protected override void BottomDetailInspector(object data, Rect panelRect)
        {
            RemoveInstallTreeElement ritm = data as RemoveInstallTreeElement;
            RemoveInstallDataModel removeInstall = ritm.data;

            ShowTab("categories", removeInstall);
        }

        protected override object OnAnimationDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            MateriViewData animationOnFigure = element as MateriViewData;

            if (animationOnFigure.figureOption.Count > 0)
            {
                // force data for remove install animation
                animationOnFigure.IsSequenceAnimation = true;
                animationOnFigure.isShowAnimationUI = false;
                animationOnFigure.isReverseAnimationOnReset = false;
                animationOnFigure.figure = animationOnFigure.figureOption[0];

                ShowListField("animation element", animationOnFigure.animations, listId + index + 5);
            }

            GUILayout.EndVertical();
            return animationOnFigure;
        }
    }
}

#endif
﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(Callout))]
    [CanEditMultipleObjects]
    public class CalloutParentDrawer : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GUILayout.Label("prefab path harus sesuai dengan lokasi foldernya saat mengedit callout, tekan 'Edit callout' untuk menggeneratenya");

            if (!Application.isPlaying)
            {
                GUILayout.Label("\nPlay at related materi to create or edit callout for easier preview !!!");
                EditorGUI.BeginDisabledGroup(true);
            }

            Callout calloutParent = target as Callout;

            if (!string.IsNullOrEmpty(calloutParent.gameObject.scene.name) && string.IsNullOrEmpty(calloutParent.PrefabPath))
            {
                GUILayout.Label("gunakan tombol 'Edit callout' untuk mengedit callout di prefab untuk memulai mengedit prefab, agar prefab path sesuai dengan lokasi prefab");
                GUILayout.Label("gunakan tombol 'Save callout' untuk menyimpan perubahan callout");

                return;
            }

            if (calloutParent.pivotPoint == null)
                calloutParent.CreatePivot();

            if (!Application.isPlaying)
                calloutParent.created = false;

            GUILayout.BeginVertical();

            if (string.IsNullOrEmpty(calloutParent.gameObject.scene.name) && GUILayout.Button("Edit callout"))
            {
                var helperManager = FindObjectOfType<HelperManager>();
                if (helperManager != null)
                {
                    calloutParent.GeneratePrefabPath();
                    var instantiated = Instantiate(calloutParent.gameObject, helperManager.transform);
                    EditorGUIUtility.PingObject(instantiated);
                    Selection.activeGameObject = instantiated;
                }
            }

            if (!string.IsNullOrEmpty(calloutParent.gameObject.scene.name) && GUILayout.Button("create callout element"))
            {
                GameObject newCallout = new GameObject("Callout");
                newCallout.transform.SetParent(calloutParent.transform);
                CalloutElement ct = newCallout.AddComponent<CalloutElement>();
                ct.CreateCalloutPoint();
                ct.Setup(calloutParent);
            }

            if (!string.IsNullOrEmpty(calloutParent.gameObject.scene.name))
            {
                int index = 0;

                for (int i = 0; i < calloutParent.transform.childCount; i++)
                {
                    CalloutElement callout = calloutParent.transform.GetChild(i).GetComponent<CalloutElement>();

                    if (callout == null)
                        continue;

                    GUILayout.BeginHorizontal();

                    GUILayout.BeginHorizontal();

                    if (GUILayout.Button("index : " + index + " : " + calloutParent.transform.GetChild(i).name))
                    {
                        EditorGUIUtility.PingObject(calloutParent.transform.GetChild(i).gameObject);
                        Selection.activeGameObject = calloutParent.transform.GetChild(i).gameObject;
                    }

                    index++;

                    GUILayout.EndVertical();

                    if (GUILayout.Button("delete"))
                    {
                        DestroyImmediate(calloutParent.transform.GetChild(i).gameObject);
                        break;
                    }

                    if (GUILayout.Button("copy"))
                    {
                        CalloutElement source = calloutParent.transform.GetChild(i).GetComponent<CalloutElement>();
                        calloutParent.copySource = source;
                    }

                    if (calloutParent.copySource != null && GUILayout.Button("paste"))
                    {
                        CalloutElement destination = calloutParent.transform.GetChild(i).GetComponent<CalloutElement>();

                        destination.inversePivot = calloutParent.copySource.inversePivot;
                        destination.ignoreX = calloutParent.copySource.ignoreX;
                        destination.ignoreY = calloutParent.copySource.ignoreY;
                        destination.ignoreZ = calloutParent.copySource.ignoreZ;
                        destination.lineLengthAdd = calloutParent.copySource.lineLengthAdd;
                        destination.cameraDistance = calloutParent.copySource.cameraDistance;
                    }

                    GUILayout.EndHorizontal();
                }

                GUILayout.Space(10);
            }


            if (!string.IsNullOrEmpty(calloutParent.gameObject.scene.name) && GUILayout.Button("Save callout"))
            {
                if (calloutParent.PrefabPath != null)
                {
                    PrefabUtility.SaveAsPrefabAssetAndConnect(calloutParent.gameObject, calloutParent.PrefabPath, InteractionMode.AutomatedAction);
                }
                else
                {
                    Debug.LogError("prefab path empty");
                }
            }

            GUILayout.EndVertical();

            if (!Application.isPlaying)
            {
                EditorGUI.EndDisabledGroup();
            }
        }
    }
}

#endif
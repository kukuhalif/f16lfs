﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(CalloutPoint))]
    [CanEditMultipleObjects]
    public class CalloutPointEditor : Editor
    {
        void OnSceneGUI()
        {
            //right click
            if (Event.current.type == EventType.MouseDown && Event.current.button == 1 && Event.current.shift)
            {
                Ray worldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(worldRay, out hitInfo, float.PositiveInfinity))
                {
                    CalloutPoint cp = target as CalloutPoint;
                    cp.ResetTarget();
                    cp.transform.position = hitInfo.point;
                    cp.CalculateTargetObject();
                }
                Event.current.Use();
            }
        }
    }
}
#endif
﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using VirtualTraining.Feature;
using TMPro;

namespace VirtualTraining.Tools
{
    public class CalloutValueEditor : EditorWindow
    {
        [MenuItem("Virtual Training/Helper/Callout Value Editor")]
        static void GetWindow()
        {
            EditorWindow window = GetWindow<CalloutValueEditor>();
            window.titleContent = new GUIContent("Callout Value Editor");
        }

        [MenuItem("Virtual Training/Helper/Callout Value Editor", true)]
        static bool ShowValidation()
        {
            return !Application.isPlaying;
        }


        string filter = "";
        string[] pathOptions;
        string path = "";
        Vector2 scrollPos = new Vector2();
        List<Callout> callouts = new List<Callout>();

        // callout values
        bool changeLineLengthScale = false;
        float lineLengthScale = 10f;
        bool changeBoxScaleConstant = false;
        float boxScaleConstant = 0.035f;
        bool changeLineWidthConstant = false;
        float lineWidthConstant = 2.5f;
        bool changeCalloutLineMaterial = false;
        Material calloutLineMaterial;
        bool changeTextMargin = false;
        float topMargin = 5, bottomMargin = 5, leftMargin = 10, rightMargin = 10;
        bool changeBackgroundTexture = false;
        Sprite backgroundTexture;
        bool slicedImage;
        bool changeLineTextAndBoxColor = false;
        Color lineColor = Color.red;
        Color textColor = Color.green;

        private void OnEnable()
        {
            pathOptions = Directory.GetDirectories(Application.dataPath, "*.*", SearchOption.AllDirectories);
            for (int i = 0; i < pathOptions.Length; i++)
            {
                pathOptions[i] = pathOptions[i].Replace(@"\", @"/");
            }
            path = EditorPrefs.GetString("callout value editor");
        }

        private void OnDisable()
        {
            EditorPrefs.SetString("callout value editor", path);
        }

        private void OnGUI()
        {
            filter = EditorGUILayout.TextField("path filter", filter);

            GUILayout.BeginVertical();
            GUILayout.Label("callout folder select");

            int selectedIndex = 0;
            for (int i = 0; i < pathOptions.Length; i++)
            {
                if (path == pathOptions[i])
                {
                    selectedIndex = i;
                    break;
                }
            }

            selectedIndex = EditorGUILayout.Popup(selectedIndex, pathOptions);
            path = pathOptions[selectedIndex];

            GUILayout.EndVertical();

            if (string.IsNullOrEmpty(path))
                EditorGUI.BeginDisabledGroup(true);
            if (GUILayout.Button("load callout from : " + path))
            {
                GetAllCallouts();
            }

            if (string.IsNullOrEmpty(path))
                EditorGUI.EndDisabledGroup();

            if (callouts.Count == 0)
                EditorGUI.BeginDisabledGroup(true);

            float originalLabelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 180f;

            changeLineLengthScale = EditorGUILayout.Toggle("change line length scale ?", changeLineLengthScale);
            lineLengthScale = EditorGUILayout.FloatField("line length scale", lineLengthScale);

            changeBoxScaleConstant = EditorGUILayout.Toggle("change box scale constant ?", changeBoxScaleConstant);
            boxScaleConstant = EditorGUILayout.FloatField("box scale constant", boxScaleConstant);

            changeLineWidthConstant = EditorGUILayout.Toggle("change line width constant ?", changeLineWidthConstant);
            lineWidthConstant = EditorGUILayout.FloatField("line width constant", lineWidthConstant);

            changeCalloutLineMaterial = EditorGUILayout.Toggle("change callout line material ?", changeCalloutLineMaterial);
            calloutLineMaterial = EditorGUILayout.ObjectField("line material", calloutLineMaterial, typeof(Material), true) as Material;

            changeTextMargin = EditorGUILayout.Toggle("change text margin ?", changeTextMargin);
            topMargin = EditorGUILayout.FloatField("top margin", topMargin);
            bottomMargin = EditorGUILayout.FloatField("bottom margin", bottomMargin);
            leftMargin = EditorGUILayout.FloatField("left margin", leftMargin);
            rightMargin = EditorGUILayout.FloatField("right margin", rightMargin);

            changeBackgroundTexture = EditorGUILayout.Toggle("change background texture ?", changeBackgroundTexture);
            backgroundTexture = (Sprite)EditorGUILayout.ObjectField(backgroundTexture, typeof(Sprite), allowSceneObjects: true);
            slicedImage = EditorGUILayout.Toggle("sliced image ?", slicedImage);

            changeLineTextAndBoxColor = EditorGUILayout.Toggle("change text and line color ?", changeLineTextAndBoxColor);
            textColor = EditorGUILayout.ColorField("text color", textColor);
            lineColor = EditorGUILayout.ColorField("line color", lineColor);

            EditorGUIUtility.labelWidth = originalLabelWidth;

            if (GUILayout.Button("modify value"))
            {
                ModifyValue();
                Debug.Log("changing the callout value is complete");
            }

            if (callouts.Count == 0)
                EditorGUI.EndDisabledGroup();

            if (callouts.Count > 0)
            {
                scrollPos = GUILayout.BeginScrollView(scrollPos);
                for (int i = 0; i < callouts.Count; i++)
                {
                    GUILayout.Label((i + 1).ToString() + " " + callouts[i].gameObject.name);
                }
                GUILayout.EndScrollView();
            }
        }

        void GetAllCallouts()
        {
            callouts.Clear();
            string[] calloutPaths = Directory.GetFiles(path, "*.prefab", SearchOption.AllDirectories);
            for (int i = 0; i < calloutPaths.Length; i++)
            {
                calloutPaths[i] = calloutPaths[i].Replace(Application.dataPath, "");
                calloutPaths[i] = calloutPaths[i].Replace(@"\", @"/");
                calloutPaths[i] = "Assets" + calloutPaths[i];

                GameObject prefab = (GameObject)AssetDatabase.LoadAssetAtPath(calloutPaths[i], typeof(GameObject));
                if (prefab != null)
                {
                    Callout cp = prefab.GetComponent<Callout>();
                    if (cp != null)
                    {
                        callouts.Add(cp);
                    }
                }
            }
        }

        void ModifyValue()
        {
            for (int i = 0; i < callouts.Count; i++)
            {
                Callout instantiatedCallout = Instantiate(callouts[i]);

                if (changeLineLengthScale)
                    instantiatedCallout.lineLengthScale = lineLengthScale;
                if (changeBoxScaleConstant)
                    instantiatedCallout.boxScaleConstant = boxScaleConstant;
                if (changeLineWidthConstant)
                    instantiatedCallout.lineWidthConstant = lineWidthConstant;

                if (changeCalloutLineMaterial)
                {
                    LineRenderer[] lines = instantiatedCallout.transform.GetAllComponentsInChilds<LineRenderer>();
                    for (int l = 0; l < lines.Length; l++)
                    {
                        if (lines[l] != null)
                            lines[l].sharedMaterial = calloutLineMaterial;
                    }
                }

                if (changeTextMargin)
                    instantiatedCallout.SetMargin(topMargin, bottomMargin, leftMargin, rightMargin);

                if (changeBackgroundTexture)
                {
                    Image[] images = instantiatedCallout.transform.GetAllComponentsInChilds<Image>();
                    foreach (var image in images)
                    {
                        if (image != null)
                        {
                            image.sprite = backgroundTexture;
                            image.type = slicedImage ? Image.Type.Sliced : Image.Type.Simple;
                        }
                    }
                }

                if (changeLineTextAndBoxColor)
                {
                    LineRenderer[] lines = instantiatedCallout.transform.GetAllComponentsInChilds<LineRenderer>();
                    foreach (var line in lines)
                    {
                        if (line != null)
                        {
                            line.startColor = lineColor;
                            line.endColor = lineColor;
                        }
                    }

                    TextMeshProUGUI[] texs = instantiatedCallout.transform.GetAllComponentsInChilds<TextMeshProUGUI>();
                    foreach (var text in texs)
                    {
                        if (text != null)
                            text.color = textColor;
                    }
                }

                CalloutTextBox[] textBoxes = instantiatedCallout.transform.GetAllComponentsInChilds<CalloutTextBox>();
                foreach (var box in textBoxes)
                {
                    var images = box.transform.GetAllComponentsInChilds<Image>();
                    for (int g = 0; g < images.Length; g++)
                    {
                        box.SetBackground(images[g]);
                    }
                }

                // GetPrefabAssetPathOfNearestInstanceRoot cuma bisa dipake pas ngga di play / gameobject prefab di scene warnanya biru alias masih berupa prefab, kalo udah berupa instantiated object ga bakal bisa ini
                string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(callouts[i].gameObject);
                PrefabUtility.SaveAsPrefabAssetAndConnect(instantiatedCallout.gameObject, prefabPath, InteractionMode.AutomatedAction);

                DestroyImmediate(instantiatedCallout.gameObject);
            }
        }
    }
}

#endif
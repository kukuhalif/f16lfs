﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.UI;
using UnityEngine.Video;

namespace VirtualTraining.Tools
{
    public class VirtualTrainingAppConfigEditor : EditorWindowDatabaseBase<VirtualTrainingAppConfig, VirtualTrainingAppConfigEditor, AppConfig>
    {
        [MenuItem("Virtual Training/Config/App Config")]
        static void OpenWindow()
        {
            VirtualTrainingAppConfigEditor window = OpenEditorWindow();
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualTrainingAppConfigEditor window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                return true;
            }
            return false;
        }

        public static void OpenCutawayConfig()
        {
            VirtualTrainingAppConfigEditor window = OpenEditorWindow();
            window.propertyField.SetSelectedTab("app config", 10);
        }

        PropertyField propertyField;
        Vector2 scrollView = new Vector2();

        protected override void OnEnable()
        {
            base.OnEnable();

            propertyField = new PropertyField(RecordUndo, "config editor");
            propertyField.CreateTab("app config", DrawTabCallback, "AUDIO", "UI", "CURSOR TEXTURE", "VIDEO", "SPECIAL PATH", "VR");
            propertyField.CreateTab("cursor tab", DrawCursorTabCallback, "LINK", "HORIZONTAL", "VERTICAL", "SLASH", "BACKSLASH", "LOADING", "VIRTUAL BUTTON");
            propertyField.CreateTab("virtual button tab", DrawVirtualTabCallback, "CLICK", "PRESS", "DRAG HORIZONTAL", "DRAG VERTICAL", "DRAG BOTH");
            propertyField.CreateListView("scene asset", null, null, SceneElementCallback, null, null, true, typeof(SceneAsset));
            propertyField.CreateListView("sfx", null, null, SFXDataElementCallback, null, null, false, typeof(AudioClip));
            propertyField.CreateListView("icon", null, null, IconElementCallback, null, null, false, typeof(Texture2D));
            propertyField.CreateListView("ui theme", null, null, OnUIThemeElementDrawCallback, UIThemeCallback, null, false);
            propertyField.CreateListView("frames", null, null, CursorElementCallback, null, null, false, typeof(Texture2D));
            propertyField.CreateListView("video clip", null, null, VideoClipElementCallback, null, null, true, typeof(VideoClip));
        }

        private object VideoClipElementCallback(string listId, object element, int index)
        {
            VideoClip video = element as VideoClip;
            GUILayout.BeginHorizontal();

            propertyField.ShowAssetField("video", listId + index, ref video);

            GUILayout.EndHorizontal();

            return video;
        }

        private void DrawCursorTabCallback(int selected, object data, string fieldId)
        {
            AppConfig config = data as AppConfig;
            if (selected == 0) // link
            {
                SetAllCursorHotspot(config.cursorTexture.link);
                PreviewCursorAnimation(() => VirtualTrainingCursor.Link());
                propertyField.ShowSimpleField("fps", "link fps", ref config.cursorTexture.link.fps);
                propertyField.ShowListField("frames", config.cursorTexture.link.frames, "link cursor");
            }
            else if (selected == 1) // horizontal
            {
                SetAllCursorHotspot(config.cursorTexture.horizontalResize);
                PreviewCursorAnimation(() => VirtualTrainingCursor.HorizontalResize());
                propertyField.ShowSimpleField("fps", "horizontal fps", ref config.cursorTexture.horizontalResize.fps);
                propertyField.ShowListField("frames", config.cursorTexture.horizontalResize.frames, "horizontal resize");
            }
            else if (selected == 2) // vertical
            {
                SetAllCursorHotspot(config.cursorTexture.verticalResize);
                PreviewCursorAnimation(() => VirtualTrainingCursor.VerticalResize());
                propertyField.ShowSimpleField("fps", "vertical fps", ref config.cursorTexture.verticalResize.fps);
                propertyField.ShowListField("frames", config.cursorTexture.verticalResize.frames, "vertical resize");
            }
            else if (selected == 3) // slash
            {
                SetAllCursorHotspot(config.cursorTexture.slashResize);
                PreviewCursorAnimation(() => VirtualTrainingCursor.BottomLeftResize());
                propertyField.ShowSimpleField("fps", "slash fps", ref config.cursorTexture.slashResize.fps);
                propertyField.ShowListField("frames", config.cursorTexture.slashResize.frames, "slash resize");
            }
            else if (selected == 4) // backslash
            {
                SetAllCursorHotspot(config.cursorTexture.backSlashResize);
                PreviewCursorAnimation(() => VirtualTrainingCursor.BottomRightResize());
                propertyField.ShowSimpleField("fps", "backslash fps", ref config.cursorTexture.backSlashResize.fps);
                propertyField.ShowListField("frames", config.cursorTexture.backSlashResize.frames, "back slash resize");
            }
            else if (selected == 5) // loading
            {
                SetAllCursorHotspot(config.cursorTexture.loading);
                PreviewCursorAnimation(() => VirtualTrainingCursor.Loading());
                propertyField.ShowSimpleField("fps", "loading fps", ref config.cursorTexture.loading.fps);
                propertyField.ShowListField("frames", config.cursorTexture.loading.frames, "loading");
            }
            else if (selected == 6) // virtual button
            {
                propertyField.ShowTab("virtual button tab", config);
            }
        }

        private void DrawVirtualTabCallback(int selected, object data, string fieldId)
        {
            AppConfig config = data as AppConfig;

            if (selected == 0) // vbClick
            {
                SetAllCursorHotspot(config.cursorTexture.vbClick);
                PreviewCursorAnimation(() => VirtualTrainingCursor.VBClickInteraction());
                propertyField.ShowSimpleField("fps", "click fps", ref config.cursorTexture.vbClick.fps);
                propertyField.ShowListField("frames", config.cursorTexture.vbClick.frames, "vb click interaction");
            }
            else if (selected == 1) // vbPress
            {
                SetAllCursorHotspot(config.cursorTexture.vbPress);
                PreviewCursorAnimation(() => VirtualTrainingCursor.VBPressInteraction());
                propertyField.ShowSimpleField("fps", "press fps", ref config.cursorTexture.vbPress.fps);
                propertyField.ShowListField("frames", config.cursorTexture.vbPress.frames, "vb press interaction");
            }
            else if (selected == 2) // vbDragHorizontal
            {
                SetAllCursorHotspot(config.cursorTexture.vbDragHorizontal);
                PreviewCursorAnimation(() => VirtualTrainingCursor.VBHorizontalInteraction());
                propertyField.ShowSimpleField("fps", "drag horizontal fps", ref config.cursorTexture.vbDragHorizontal.fps);
                propertyField.ShowListField("frames", config.cursorTexture.vbDragHorizontal.frames, "vb drag horizontal interaction");
            }
            else if (selected == 3) // vbDragVertical
            {
                SetAllCursorHotspot(config.cursorTexture.vbDragVertical);
                PreviewCursorAnimation(() => VirtualTrainingCursor.VBVerticalInteraction());
                propertyField.ShowSimpleField("fps", "drag vertical fps", ref config.cursorTexture.vbDragVertical.fps);
                propertyField.ShowListField("frames", config.cursorTexture.vbDragVertical.frames, "vb drag vertical interaction");
            }
            else if (selected == 4) // vbDragBoth
            {
                SetAllCursorHotspot(config.cursorTexture.vbDragBoth);
                PreviewCursorAnimation(() => VirtualTrainingCursor.VBBothInteraction());
                propertyField.ShowSimpleField("fps", "drag both interaction fps", ref config.cursorTexture.vbDragBoth.fps);
                propertyField.ShowListField("frames", config.cursorTexture.vbDragBoth.frames, "vb drag both interaction");
            }
        }

        private void PreviewCursorAnimation(System.Action onPreviewPressed)
        {
            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(!Application.isPlaying);

            if (GUILayout.Button("preview"))
            {
                Save();
                VirtualTrainingCursor.DefaultCursor();
                onPreviewPressed.Invoke();
            }

            if (GUILayout.Button("default cursor"))
            {
                VirtualTrainingCursor.DefaultCursor();
            }

            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();
        }

        Vector2 setHotspot;

        private void SetAllCursorHotspot(AnimatedCursor animatedCursor)
        {
            GUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(animatedCursor.frames.Count == 0);

            GUILayout.Label("all hotspot setter -> ", GUILayout.Width(120));
            propertyField.ShowSimpleField("hotspot x", "all hotspot x", ref setHotspot.x, 80, 150);
            GUILayout.Space(30);
            propertyField.ShowSimpleField("hotspot y", "all hotspot y", ref setHotspot.y, 80, 150);
            GUILayout.Space(30);

            if (GUILayout.Button("set all frames hotspot"))
            {
                for (int i = 0; i < animatedCursor.frames.Count; i++)
                {
                    animatedCursor.frames[i].hotspot = setHotspot;
                }
            }

            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();
        }

        private void DrawTabCallback(int selected, object data, string fieldId)
        {
            AppConfig config = data as AppConfig;

            scrollView = GUILayout.BeginScrollView(scrollView);

            if (selected == 0) // audio
            {
                propertyField.ShowListField("sfx", config.sfxs);
            }
            else if (selected == 1) // ui
            {
                GUILayout.Label("ui texture");

                propertyField.ShowAssetField("play", ref config.uiConfig.uiTexture.play);
                propertyField.ShowAssetField("pause", ref config.uiConfig.uiTexture.pause);
                propertyField.ShowAssetField("minimize panel", ref config.uiConfig.uiTexture.minimizePanel);
                propertyField.ShowAssetField("windowed panel", ref config.uiConfig.uiTexture.windowedPanel);
                propertyField.ShowAssetField("maximize panel", ref config.uiConfig.uiTexture.maximizePanel);
                propertyField.ShowAssetField("close panel", ref config.uiConfig.uiTexture.closePanel);
                propertyField.ShowAssetField("dropdown arrow", ref config.uiConfig.uiTexture.dropdownArrow);
                propertyField.ShowAssetField("checkmark", ref config.uiConfig.uiTexture.checkmark);

                GUILayout.Space(10f);

                propertyField.ShowSimpleField("fade in curve", ref config.uiConfig.fadeInCurve);
                propertyField.ShowSimpleField("fade out curve", ref config.uiConfig.fadeOutCurve);
                propertyField.ShowSimpleField("transition speed", ref config.uiConfig.transitionSpeed);

                GUILayout.Space(10f);

                propertyField.ShowListField("ui theme", config.uiConfig.uiThemes);


            }
            else if (selected == 2) // cursor
            {
                propertyField.ShowTab("cursor tab", config);
            }
            else if (selected == 3) // video
            {
                propertyField.ShowAssetField("intro", ref config.video.intro);

                GUILayout.Space(10f);
                GUILayout.Label("apps loading videos");
                propertyField.ShowListField("video clip", config.video.appsLoadingVideos, "apps");

                GUILayout.Space(10f);
                GUILayout.Label("quiz loading videos");
                propertyField.ShowListField("video clip", config.video.quizLoadingVideos, "quiz");
            }
            else if (selected == 4) // asset
            {
                propertyField.ShowSimpleField("content path", ref config.pathConfig.contentAssetPath, 100, 400);
            }
            else if (selected == 5) // vr
            {
                GUILayout.Space(15);
                GUILayout.Label("Setting Default position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position default x", ref config.vrData.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position default y", ref config.vrData.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position default z", ref config.vrData.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Setting Default rotation");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("rotation x", "rotation default x", ref config.vrData.defaultRotation.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("rotation y", "rotation default y", ref config.vrData.defaultRotation.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("rotation z", "rotation default z", ref config.vrData.defaultRotation.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas System Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position system x", ref config.vrData.systemPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position system y", ref config.vrData.systemPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position system z", ref config.vrData.systemPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Confirmation Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position confirmation x", ref config.vrData.confirmationPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position confirmation y", ref config.vrData.confirmationPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position confirmation z", ref config.vrData.confirmationPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Help Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position help x", ref config.vrData.helpPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position help y", ref config.vrData.helpPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position help z", ref config.vrData.helpPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Setting Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position setting x", ref config.vrData.settingPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position setting y", ref config.vrData.settingPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position setting z", ref config.vrData.settingPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Description Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position description x", ref config.vrData.descriptionPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position description y", ref config.vrData.descriptionPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position description z", ref config.vrData.descriptionPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Cutaway Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position cutaway x", ref config.vrData.cutawayPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position cutaway y", ref config.vrData.cutawayPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position cutaway z", ref config.vrData.cutawayPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Video Schematic Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position video x", ref config.vrData.videoSchematicPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position video y", ref config.vrData.videoSchematicPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position video z", ref config.vrData.videoSchematicPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Maintenance Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position maintenance x", ref config.vrData.maintenancePosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position maintenance y", ref config.vrData.maintenancePosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position maintenance z", ref config.vrData.maintenancePosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Troubleshoot Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position troubleshoot x", ref config.vrData.troubleshootPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position troubleshoot y", ref config.vrData.troubleshootPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position troubleshoot z", ref config.vrData.troubleshootPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.Label("Canvas Montitor Camera Default Position");
                GUILayout.BeginHorizontal();
                propertyField.ShowSimpleField("position x", "position Montitor Camera x", ref config.vrData.monitorCameraPosition.defaultPosition.x, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position y", "position Montitor Camera y", ref config.vrData.monitorCameraPosition.defaultPosition.y, 80, 150);
                GUILayout.Space(15);
                propertyField.ShowSimpleField("position z", "position Montitor Camera z", ref config.vrData.monitorCameraPosition.defaultPosition.z, 80, 150);
                GUILayout.EndHorizontal();
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
        }

        protected override void Save()
        {
            SetEditorIconLookup();
            base.Save();
        }

        private void SetEditorIconLookup()
        {
            EditorIconResources.LoadIcons();
        }

        private object CursorElementCallback(string listId, object element, int index)
        {
            CursorTexture cursorTexture = element as CursorTexture;
            GUILayout.BeginHorizontal();

            propertyField.ShowSimpleField("hotspot x", listId + index + "hotspot x", ref cursorTexture.hotspot.x, 80, 150);
            GUILayout.Space(30);
            propertyField.ShowSimpleField("hotspot y", listId + index + "hotspot y", ref cursorTexture.hotspot.y, 80, 150);
            GUILayout.Space(30);
            propertyField.ShowAssetField("texture", listId + index + "texture", ref cursorTexture.texture, 80, 150);
            GUILayout.Space(30);

            GUILayout.EndHorizontal();

            return cursorTexture;
        }

        private object SceneElementCallback(string listId, object element, int index)
        {
            SceneAsset scene = element as SceneAsset;

            propertyField.ShowAssetField("scene asset", listId + index, ref scene);

            if (index == 0)
                GUILayout.Label("<- starting scene");
            if (index == 1)
                GUILayout.Label("<- UI scene");

            return scene;
        }

        private object SFXDataElementCallback(string listId, object element, int index)
        {
            SFXData sfxData = element as SFXData;
            GUILayout.BeginVertical();

            propertyField.ShowSimpleField("SFX", listId + index, ref sfxData.sfx);
            propertyField.ShowAssetField("audio clip", listId + index + 1, ref sfxData.clip);

            GUILayout.EndVertical();

            return sfxData;
        }

        private object IconElementCallback(string listId, object element, int index)
        {
            EditorIcon editorIcon = element as EditorIcon;
            GUILayout.BeginHorizontal();

            propertyField.ShowSimpleField("category", listId + index, ref editorIcon.category);
            propertyField.ShowAssetField("icon", listId + index + 1, ref editorIcon.icon);

            GUILayout.EndHorizontal();

            return editorIcon;
        }

        private object OnUIThemeElementDrawCallback(string listId, object element, int index)
        {
            UITheme theme = element as UITheme;

            EditorGUILayout.LabelField("theme name : " + theme.name, GUILayout.Width(135f));

            EditorGUI.BeginDisabledGroup(!Application.isPlaying);
            if (GUILayout.Button("switch theme", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH)))
            {
                Save();
                EventManager.TriggerEvent(new SwitchUIThemeEvent(index));
            }
            EditorGUI.EndDisabledGroup();

            return element;
        }

        private void ShowUIInteractionTheme(string name, UIInteractionTheme uIInteractionTheme, int id, int index)
        {
            propertyField.ShowSimpleField(name + " background color", "bg cl" + id + index, ref uIInteractionTheme.backgroundColor, 200f, 400f);
            propertyField.ShowSimpleField(name + " content color", "ct cl" + id + index, ref uIInteractionTheme.contentColor, 200f, 400f);
        }

        private int ShowButtonTheme(ButtonColorTheme buttonColorTheme, int id, int index)
        {
            ShowUIInteractionTheme("normal", buttonColorTheme.normal, id++, index);
            ShowUIInteractionTheme("hover", buttonColorTheme.hover, id++, index);
            ShowUIInteractionTheme("pressed", buttonColorTheme.pressed, id++, index);

            return id;
        }

        private int ShowToggleTheme(ToggleColorTheme toggleColorTheme, int id, int index)
        {
            ShowUIInteractionTheme("normal off", toggleColorTheme.normalOff, id++, index);
            ShowUIInteractionTheme("hover off", toggleColorTheme.hoverOff, id++, index);
            ShowUIInteractionTheme("pressed off", toggleColorTheme.pressedOff, id++, index);
            ShowUIInteractionTheme("normal on", toggleColorTheme.normalOn, id++, index);
            ShowUIInteractionTheme("hover on", toggleColorTheme.hoverOn, id++, index);
            ShowUIInteractionTheme("pressed on", toggleColorTheme.pressedOn, id++, index);

            return id;
        }

        private object UIThemeCallback(string listId, object element, int index)
        {
            int idx = 0;

            UITheme uiTheme = element as UITheme;

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            propertyField.ShowSimpleField("theme name", idx++.ToString() + index.ToString(), ref uiTheme.name);

            GUILayout.EndHorizontal();

            GUILayout.Space(10f);
            GUILayout.Label("Landing Page");
            propertyField.ShowSimpleField("background", idx++.ToString() + index.ToString(), ref uiTheme.landingPageBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Generic Text");
            propertyField.ShowSimpleField("generic text", idx++.ToString() + index.ToString(), ref uiTheme.genericTextColor);

            GUILayout.Space(10f);
            GUILayout.Label("Background");
            propertyField.ShowSimpleField("background", idx++.ToString() + index.ToString(), ref uiTheme.backgroundColor);

            GUILayout.Space(10f);
            GUILayout.Label("Hover");
            propertyField.ShowSimpleField("hover underline color", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.underlineColor);
            propertyField.ShowAssetField("hover cover underline", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.underlineSprite);
            GUILayout.Space(5f);
            propertyField.ShowSimpleField("hover cover color", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.color);
            propertyField.ShowAssetField("hover cover bottom", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.bottom);
            propertyField.ShowAssetField("hover cover top", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.top);
            propertyField.ShowAssetField("hover cover left", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.left);
            propertyField.ShowAssetField("hover cover right", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.right);
            propertyField.ShowAssetField("hover cover bottom right", idx++.ToString() + index.ToString(), ref uiTheme.hoverCoverTheme.bottomRight);

            GUILayout.Space(10f);
            GUILayout.Label("Button");
            idx = ShowButtonTheme(uiTheme.button, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Tree folder button");
            idx = ShowButtonTheme(uiTheme.treeElementFolderButton, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Button panel minimize");
            idx = ShowButtonTheme(uiTheme.uiPanelMinimizeButton, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Button panel restore");
            idx = ShowButtonTheme(uiTheme.uiPanelRestoreButton, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Button panel close");
            idx = ShowButtonTheme(uiTheme.uiPanelCloseButton, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Toggle");
            idx = ShowToggleTheme(uiTheme.toggle, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Lock panel toggle");
            idx = ShowToggleTheme(uiTheme.lockPaneltoggle, idx, index);

            GUILayout.Space(10f);

            GUILayout.Label("Panel");
            propertyField.ShowSimpleField("title", idx++.ToString() + index.ToString(), ref uiTheme.panelTitleColor);
            propertyField.ShowSimpleField("title dragged", idx++.ToString() + index.ToString(), ref uiTheme.panelTitleDraggedColor);
            propertyField.ShowSimpleField("content", idx++.ToString() + index.ToString(), ref uiTheme.panelContentColor);
            propertyField.ShowSimpleField("title text", idx++.ToString() + index.ToString(), ref uiTheme.panelTitleTextColor);
            propertyField.ShowSimpleField("dragged title text", idx++.ToString() + index.ToString(), ref uiTheme.panelTitleTextDraggedColor);

            GUILayout.Space(10f);
            GUILayout.Label("Slider");
            propertyField.ShowSimpleField("handle default", idx++.ToString() + index.ToString(), ref uiTheme.sliderHandleDefaultColor);
            propertyField.ShowSimpleField("handle highlighted", idx++.ToString() + index.ToString(), ref uiTheme.sliderHandleHighlightedColor);
            propertyField.ShowSimpleField("handle pressed", idx++.ToString() + index.ToString(), ref uiTheme.sliderHandlePressedColor);
            propertyField.ShowSimpleField("handle selected", idx++.ToString() + index.ToString(), ref uiTheme.sliderHandleSelectedColor);
            propertyField.ShowSimpleField("handle disabled", idx++.ToString() + index.ToString(), ref uiTheme.sliderHandleDisabledColor);
            propertyField.ShowSimpleField("slider fill", idx++.ToString() + index.ToString(), ref uiTheme.sliderFillColor);
            propertyField.ShowSimpleField("slider background", idx++.ToString() + index.ToString(), ref uiTheme.sliderBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Scrollbar");
            propertyField.ShowSimpleField("handle default", idx++.ToString() + index.ToString(), ref uiTheme.scrollbarHandleDefaultColor);
            propertyField.ShowSimpleField("handle highlighted", idx++.ToString() + index.ToString(), ref uiTheme.scrollbarHandleHighlightedColor);
            propertyField.ShowSimpleField("handle pressed", idx++.ToString() + index.ToString(), ref uiTheme.scrollbarHandlePressedColor);
            propertyField.ShowSimpleField("handle selected", idx++.ToString() + index.ToString(), ref uiTheme.scrollbarHandleSelectedColor);
            propertyField.ShowSimpleField("handle disabled", idx++.ToString() + index.ToString(), ref uiTheme.scrollbarHandleDisabledColor);
            propertyField.ShowSimpleField("scrollbar background", idx++.ToString() + index.ToString(), ref uiTheme.scrollbarBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Input Field");
            propertyField.ShowSimpleField("input field text normal", idx++.ToString() + index.ToString(), ref uiTheme.inputFieldTextNormalColor);
            propertyField.ShowSimpleField("input field text highlighted", idx++.ToString() + index.ToString(), ref uiTheme.inputFieldTextHighlightColor);
            propertyField.ShowSimpleField("input field text pressed", idx++.ToString() + index.ToString(), ref uiTheme.inputFieldTextPressedColor);
            propertyField.ShowSimpleField("input field text selected", idx++.ToString() + index.ToString(), ref uiTheme.inputFieldTextSelectedColor);
            propertyField.ShowSimpleField("input field text disabled", idx++.ToString() + index.ToString(), ref uiTheme.inputFieldTextDisabledColor);
            propertyField.ShowSimpleField("input field background", idx++.ToString() + index.ToString(), ref uiTheme.inputFieldBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Dropdown");
            propertyField.ShowSimpleField("dropdown normal", idx++.ToString() + index.ToString(), ref uiTheme.dropdownTextNormalColor);
            propertyField.ShowSimpleField("dropdown highlighted", idx++.ToString() + index.ToString(), ref uiTheme.dropdownTextHighlightColor);
            propertyField.ShowSimpleField("dropdown pressed", idx++.ToString() + index.ToString(), ref uiTheme.dropdownTextPressedColor);
            propertyField.ShowSimpleField("dropdown selected", idx++.ToString() + index.ToString(), ref uiTheme.dropdownTextSelectedColor);
            propertyField.ShowSimpleField("dropdown disabled", idx++.ToString() + index.ToString(), ref uiTheme.dropdownTextDisabledColor);

            GUILayout.Space(10f);
            GUILayout.Label("Text Link");
            propertyField.ShowSimpleField("text link normal", idx++.ToString() + index.ToString(), ref uiTheme.TextLinkNormalColor);
            propertyField.ShowSimpleField("text link pressed", idx++.ToString() + index.ToString(), ref uiTheme.TextLinkPressedColor);
            propertyField.ShowSimpleField("text link hover", idx++.ToString() + index.ToString(), ref uiTheme.TextLinkHoverColor);
            propertyField.ShowSimpleField("text link after", idx++.ToString() + index.ToString(), ref uiTheme.TextLinkAfterColor);

            GUILayout.Space(10f);
            GUILayout.Label("Callout");
            propertyField.ShowSimpleField("box background", idx++.ToString() + index.ToString(), ref uiTheme.calloutBackgroundColor);
            propertyField.ShowSimpleField("text", idx++.ToString() + index.ToString(), ref uiTheme.calloutTextColor);

            GUILayout.Space(10f);
            GUILayout.Label("Tooltip");
            propertyField.ShowSimpleField("tooltip background", idx++.ToString() + index.ToString(), ref uiTheme.tooltipBackgroundColor);
            propertyField.ShowSimpleField("text", idx++.ToString() + index.ToString(), ref uiTheme.tooltipTextColor);

            GUILayout.Space(10f);
            GUILayout.Label("Quiz");
            propertyField.ShowSimpleField("history answer", idx++.ToString() + index.ToString(), ref uiTheme.quizUiTheme.historyAnswer);
            propertyField.ShowSimpleField("history true answer", idx++.ToString() + index.ToString(), ref uiTheme.quizUiTheme.historyTrueAnswer);

            GUILayout.Space(10f);
            GUILayout.Label("default question button");
            //propertyField.ShowAssetField("default question button icon", idx++.ToString() + index.ToString(), ref uiTheme.quizUiTheme.defaultQuestionButtonIcon, 180f, 280f);
            idx = ShowButtonTheme(uiTheme.quizUiTheme.defaultQuestionButton, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("answered question button");
            //propertyField.ShowAssetField("answered question button icon", idx++.ToString() + index.ToString(), ref uiTheme.quizUiTheme.answeredQuestionButtonIcon, 180f, 280f);
            idx = ShowButtonTheme(uiTheme.quizUiTheme.answeredQuestionButton, idx, index);

            GUILayout.Space(10f);
            GUILayout.Label("Rounded Panel");
            propertyField.ShowSimpleField("rounded panel radius", idx++.ToString() + index.ToString(), ref uiTheme.roundedPanelRadius);

            GUILayout.EndVertical();

            return uiTheme;
        }

        protected override void RenderGUI()
        {
            propertyField.ShowTab("app config", ScriptableObjectTemp.GetData());

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("load"))
            {
                if (EditorUtility.DisplayDialog("config", "load ?", "ok", "cancel"))
                    Load();
            }


            if (GUILayout.Button("save"))
            {
                if (EditorUtility.DisplayDialog("config", "save ?", "ok", "cancel"))
                {
                    Save();
                    propertyField.LoadColor();
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}

#endif

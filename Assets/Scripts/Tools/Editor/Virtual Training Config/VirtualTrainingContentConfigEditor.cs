﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using VirtualTraining.Core;
using Paroxe.PdfRenderer;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    public class VirtualTrainingContentConfigEditor : EditorWindowDatabaseBase<VirtualTrainingContentConfig, VirtualTrainingContentConfigEditor, ContentConfig>
    {
        [MenuItem("Virtual Training/Config/Content Config")]
        static void OpenWindow()
        {
            VirtualTrainingContentConfigEditor window = OpenEditorWindow();
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualTrainingContentConfigEditor window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                return true;
            }
            return false;
        }

        public static void ShowContentConfig()
        {
            VirtualTrainingContentConfigEditor window = OpenEditorWindow();
            if (window != null)
            {
                window.propertyField.SetSelectedTab("content config", 0);
            }
        }

        PropertyField propertyField;
        Vector2 scrollView = new Vector2();

        protected override void OnEnable()
        {
            base.OnEnable();

            propertyField = new PropertyField(RecordUndo, () => WindowId);
            propertyField.CreateTab("content config", DrawTabCallback, "MATERI DATABASES", "OTHER DATABASES", "PDF", "MATERIAL", "PROJECT DETAILS", "QUIZ", "CAMERA", "CUTAWAY", "OUTLINE", "VR"); //, "FLIGHT SCENARIO");
            propertyField.CreateListView("materi asset", null, null, MateriElementCallback, null, null, false, typeof(MateriTreeAsset));
            propertyField.CreateListView("pdf materi", null, null, PdfElementCallback, null, null, true, typeof(PDFAsset));
            propertyField.CreateListView("virtual button asset", null, null, VirtualButtonElementCallback, null, null, true, typeof(VirtualButtonTreeAsset));
            propertyField.CreateListView("scripted animation asset", null, null, ScriptedElementCallback, null, null, true, typeof(ScriptedAnimationTreeAsset));
            propertyField.CreateListView("flight scenario timeline asset", null, null, FlightScenarioTimelineCallback, null, null, true, typeof(FlightScenarioTimelineTreeAsset));
            propertyField.CreateListView("part number asset", null, null, PartNumberCallback, null, null, true, typeof(PartNumberTreeAsset));
            propertyField.CreateListView("pdf ui", null, null, PdfElementCallback, null, null, true, typeof(PDFAsset));
            propertyField.CreateListView("pdf link", null, null, PdfLinkElementCallback, null, null, true, typeof(PDFAsset));
            propertyField.CreateListView("xray material", null, null, MaterialElementCallback, null, null, true, typeof(Material));
            propertyField.CreateListView("camera preset", null, null, CameraPresetCallback, null, null, false);

        }

        protected override void OnDisable()
        {
            propertyField.SaveCameraDestination();
            base.OnDisable();
        }



        private object MateriElementCallback(string listId, object element, int index)
        {
            MateriContainer materi = element as MateriContainer;
            GUILayout.BeginVertical();

            propertyField.ShowAssetField("materi asset", listId + index, ref materi.materi);
            propertyField.ShowListField("pdf materi", materi.pdfs, listId + index + 1);

            GUILayout.EndVertical();

            return materi;
        }

        private object VirtualButtonElementCallback(string listId, object element, int index)
        {
            VirtualButtonTreeAsset vb = element as VirtualButtonTreeAsset;
            propertyField.ShowAssetField("virtual button asset", listId + index, ref vb);
            return vb;
        }

        private object ScriptedElementCallback(string listId, object element, int index)
        {
            ScriptedAnimationTreeAsset sa = element as ScriptedAnimationTreeAsset;
            propertyField.ShowAssetField("scripted animation asset", listId + index, ref sa);
            return sa;
        }

        private object FlightScenarioTimelineCallback(string listId, object element, int index)
        {
            FlightScenarioTimelineTreeAsset fstl = element as FlightScenarioTimelineTreeAsset;
            propertyField.ShowAssetField("flight scenario timeline asset", listId + index, ref fstl);
            return fstl;
        }

        private object PartNumberCallback(string listId, object element, int index)
        {
            PartNumberTreeAsset pn = element as PartNumberTreeAsset;
            propertyField.ShowAssetField("part number asset", listId + index, ref pn);
            return pn;
        }

        private object PdfElementCallback(string listId, object element, int index)
        {
            PDFAsset pdf = element as PDFAsset;
            propertyField.ShowAssetField("pdf asset", listId + index, ref pdf);
            return pdf;
        }

        private object PdfLinkElementCallback(string listId, object element, int index)
        {
            PDFAsset pdf = element as PDFAsset;
            propertyField.ShowAssetField("pdf asset", listId + index, ref pdf);

            if (GUILayout.Button("copy link") && pdf != null)
            {
                string link = @"\link[pdf_" + pdf.name + "]{link to -> " + pdf.name + "}";
                VirtualTrainingEditorUtility.CopyToClipboard(link);
            }

            return pdf;
        }

        private object MaterialElementCallback(string listId, object element, int index)
        {
            Material mat = element as Material;
            propertyField.ShowAssetField("material", listId + index, ref mat);
            return mat;
        }

        private object CameraPresetCallback(string listId, object element, int index)
        {
            CameraPreset camPreset = element as CameraPreset;

            propertyField.ShowSimpleField("name", listId + index + "preset name", ref camPreset.name);

            if (GUILayout.Button("copy link"))
            {
                string link = @"\link[camerapreset_" + camPreset.name + "]{link to -> " + camPreset.name + "}";
                VirtualTrainingEditorUtility.CopyToClipboard(link);
            }

            propertyField.ShowCameraPositionField("camera preset", listId + index + "preset", ref camPreset.destination, false, true);

            return camPreset;
        }

        private void DrawTabCallback(int selected, object data, string fieldId)
        {
            ContentConfig config = data as ContentConfig;

            scrollView = GUILayout.BeginScrollView(scrollView);

            // materi databases
            if (selected == 0)
            {
                propertyField.ShowListField("materi asset", config.materiContainers);
            }
            else if (selected == 1) // other databases
            {
                propertyField.ShowAssetField("object list asset", ref config.objectListDatabase);

                GUILayout.Space(10f);
                propertyField.ShowAssetField("culling data asset", ref config.cullingDatabase);

                GUILayout.Space(10f);
                propertyField.ShowAssetField("model version asset", ref config.modelVersionDatabase);

                GUILayout.Space(10f);
                propertyField.ShowListField("part number asset", config.partNumberDatabases);

                GUILayout.Space(10f);
                propertyField.ShowListField("virtual button asset", config.virtualButtonDatabases);

                GUILayout.Space(10f);
                propertyField.ShowListField("scripted animation asset", config.scriptedAnimationDatabases);

                GUILayout.Space(10f);
                propertyField.ShowListField("flight scenario timeline asset", config.flightScenarioTimelineDatabases);
            }
            else if (selected == 2) // pdf
            {
                propertyField.ShowListField("pdf ui", config.pdfUis);

                GUILayout.Space(10f);
                propertyField.ShowListField("pdf link", config.pdfLinks);
            }
            else if (selected == 3) // material
            {
                propertyField.ShowSimpleField("default highlight color", ref config.highlightColor);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("default blink color", ref config.blinkColor);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("default fresnel color", ref config.fresnelColor);

                GUILayout.Space(10f);
                propertyField.ShowListField("xray material", config.xRayMaterials);
            }
            else if (selected == 4) // project details
            {
                propertyField.ShowAssetField("landing page icon", ref config.projectDetails.landingPageIcon);
                propertyField.ShowAssetField("main menu icon", ref config.projectDetails.mainMenuIcon);
                propertyField.ShowAssetField("about icon", ref config.projectDetails.aboutIcon);
                propertyField.ShowAssetField("quiz icon", ref config.projectDetails.quizIcon);
                propertyField.ShowTextArea("project description", 500, ref config.projectDetails.projectDescription, false);

                GUILayout.Space(10f);

                propertyField.ShowAssetField("help pdf", ref config.projectDetails.helpPDF);

                GUILayout.Space(10f);

                propertyField.ShowTextArea("about", 500, ref config.projectDetails.about, false);
            }
            else if (selected == 5) // quiz
            {
                propertyField.ShowSimpleField("quiz all amount", ref config.quizData.quizAllAmount);
                propertyField.ShowSimpleField("quiz persection amount", ref config.quizData.quizSectionAmount);
            }
            else if (selected == 6) // camera
            {
                GUILayout.Label("default camera position, target adalah object yg dilihat kamera (jika diisi kamera tidak bisa di gerakkan)");
                propertyField.ShowCameraPositionField("main camera", fieldId + "main camera", ref config.cameraData.mainCamera, false, true);
                GUILayout.Label("landing page camera position, target adalah object yang di orbit oleh kamera (tidak perlu diisi jika ingin kamera diam)");
                propertyField.ShowCameraPositionField("landing page camera", fieldId + "landing page camera", ref config.cameraData.landingPageCamera, false, true);
                GUILayout.Label("default VR camera position (masih perlu dibenahi)");
                propertyField.ShowCameraPositionField("vr camera", fieldId + "vr camera", ref config.cameraData.vrCamera, false, true);

                GUILayout.Space(20);
                GUILayout.Label("content camera link, paste link to description (materi / maintenance / troubleshoot)");

                propertyField.ShowListField("camera preset", config.cameraData.cameraPresets, fieldId + "camPreset");
            }
            else if (selected == 7) // cutaway
            {
                propertyField.ShowSimpleField("line width", ref config.cutawayConfig.lineWidth);
                propertyField.ShowSimpleField("gizmo size", ref config.cutawayConfig.gizmoSize);
                propertyField.ShowSimpleField("line color", ref config.cutawayConfig.lineColor);

                GUILayout.Space(10);
                if (GUILayout.Button("set min max value from object bounds (enter play mode)", GUILayout.Width(400)))
                {
                    EventManager.TriggerEvent(new GetCutawayObjectBoundsEvent((bounds) =>
                    {
                        config.cutawayConfig.minPos = bounds.center - bounds.size;
                        config.cutawayConfig.maxPos = bounds.center + bounds.size;
                        config.cutawayConfig.maxScale = bounds.size;
                    }));
                }

                GUILayout.Space(10);
                propertyField.ShowSimpleField("min x pos", ref config.cutawayConfig.minPos.x);
                propertyField.ShowSimpleField("min y pos", ref config.cutawayConfig.minPos.y);
                propertyField.ShowSimpleField("min z pos", ref config.cutawayConfig.minPos.z);

                GUILayout.Space(10);
                propertyField.ShowSimpleField("max x pos", ref config.cutawayConfig.maxPos.x);
                propertyField.ShowSimpleField("max y pos", ref config.cutawayConfig.maxPos.y);
                propertyField.ShowSimpleField("max z pos", ref config.cutawayConfig.maxPos.z);

                GUILayout.Space(10);
                propertyField.ShowSimpleField("max x scale", ref config.cutawayConfig.maxScale.x);
                propertyField.ShowSimpleField("max y scale", ref config.cutawayConfig.maxScale.y);
                propertyField.ShowSimpleField("max z scale", ref config.cutawayConfig.maxScale.z);
            }
            else if (selected == 8) // outline
            {
                propertyField.ShowAssetField<OutlineNormalVector>("outline normal vector data", ref config.outlineNormalVector);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("default selected color", ref config.selectedColor);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("outline width", ref config.outlineWidth);
            }
            else if (selected == 9) // VR
            {
                GUILayout.Space(10f);
                propertyField.ShowSimpleField("VR Enable", ref config.vrDataContent.isVREnable);
            }
            /*else if (selected == 10) // Flight Scenario
            {
                GUILayout.Space(10f);
                propertyField.ShowSimpleField("FlightSim Recording Save Folder", ref config.flightSimRecordingSaveFolderRef, 250, 500);
                if (config.flightSimRecordingSaveFolderRef != null)
                    config.flightSimRecordingSaveFolder = config.flightSimRecordingSaveFolderRef.path;
                EditorGUILayout.LabelField(" ", config.flightSimRecordingSaveFolder);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("FlightSim Recording File Prefix", ref config.flightSimRecordingFilePrefix, 250, 500);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("FlightSim Recording Skip Frames", ref config.flightSimRecordingSkipFrames, 250, 500);

                GUILayout.Space(20f);
                propertyField.ShowSimpleField("Flight Scenario Save Folder", ref config.flightScenarioSaveFolderRef, 250, 500);
                if (config.flightScenarioSaveFolderRef != null)
                    config.flightScenarioSaveFolder = config.flightScenarioSaveFolderRef.path;
                EditorGUILayout.LabelField(" ", config.flightScenarioSaveFolder);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("Flight Scenario File Prefix", ref config.flightScenarioFilePrefix, 250, 500);
            }*/

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
        }

        protected override void RenderGUI()
        {
            propertyField.ShowTab("content config", ScriptableObjectTemp.GetData());

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("load"))
            {
                if (EditorUtility.DisplayDialog("config", "load ?", "ok", "cancel"))
                    Load();
            }

            if (GUILayout.Button("save"))
            {
                if (EditorUtility.DisplayDialog("config", "save ?", "ok", "cancel"))
                {
                    Save();
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}

#endif

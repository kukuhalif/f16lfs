#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{

    public class VirtualTrainingSystemConfigEditor : EditorWindowDatabaseBase<VirtualTrainingSystemConfig, VirtualTrainingSystemConfigEditor, SystemConfig>
    {
        [MenuItem("Virtual Training/Config/System Config")]
        static void OpenWindow()
        {
            VirtualTrainingSystemConfigEditor window = OpenEditorWindow();
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualTrainingSystemConfigEditor window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                return true;
            }
            return false;
        }

        PropertyField propertyField;
        Vector2 scrollView = new Vector2();

        protected override void OnEnable()
        {
            base.OnEnable();

            propertyField = new PropertyField(RecordUndo, "system config");
            propertyField.CreateTab("sys config", DrawTabCallback, "SCENES", "SETTING", "EDITOR ICON", "EDITOR");
            propertyField.CreateListView("scene asset", null, null, SceneElementCallback, null, null, true, typeof(SceneAsset));
            propertyField.CreateListView("icon", null, null, IconElementCallback, null, null, false, typeof(Texture2D));
            propertyField.CreateListView("persistent ui prefab", null, null, PersistentUiCallback, null, null, true);
        }

        protected override void Save()
        {
            SetSceneNames();
            base.Save();
            VirtualTrainingEditorUtility.SetBuildSettingScenes();
        }

        protected override void RenderGUI()
        {
            propertyField.ShowTab("sys config", ScriptableObjectTemp.GetData());

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("set build setting scenes"))
            {
                SetSceneNames();
                VirtualTrainingEditorUtility.SetBuildSettingScenes();
            }

            if (GUILayout.Button("load"))
            {
                if (EditorUtility.DisplayDialog("config", "load ?", "ok", "cancel"))
                    Load();
            }


            if (GUILayout.Button("save"))
            {
                if (EditorUtility.DisplayDialog("config", "save ?", "ok", "cancel"))
                {
                    Save();
                    VirtualTrainingEditorUtility.SetBuildSettingScenes();
                    propertyField.LoadColor();
                }
            }

            GUILayout.EndHorizontal();
        }

        private void SetSceneNames()
        {
            // starting
            string startingSceneName = ScriptableObjectTemp.GetData().startingSceneAsset.name;
            ScriptableObjectTemp.GetData().startingScene = startingSceneName;

            foreach (var sceneConfig in ScriptableObjectTemp.GetData().sceneConfigs)
            {
                sceneConfig.scenes.Clear();

                if (!sceneConfig.includeInBuildSetting)
                    continue;

                var scenes = sceneConfig.sceneAssets;
                foreach (var scene in scenes)
                {
                    sceneConfig.scenes.Add(scene.name);
                }
                sceneConfig.mainScenePath = AssetDatabase.GetAssetPath(sceneConfig.sceneAssets[0]);

                // quiz
                if (sceneConfig.quizSceneAsset == null)
                {
                    sceneConfig.quizScene = "";
                }
                else
                {
                    string quizSceneName = sceneConfig.quizSceneAsset.name;
                    sceneConfig.quizScene = quizSceneName;
                }

                // vrhead
                if (sceneConfig.vrHeadSceneAsset == null)
                {
                    sceneConfig.vrHeadScene = "";
                }
                else
                {
                    string vrHeadSceneName = sceneConfig.vrHeadSceneAsset.name;
                    sceneConfig.vrHeadScene = vrHeadSceneName;
                }

                // networking
                if (sceneConfig.networkingSceneAsset == null)
                {
                    sceneConfig.networkingScene = "";
                }
                else
                {
                    string networkingSceneName = sceneConfig.networkingSceneAsset.name;
                    sceneConfig.networkingScene = networkingSceneName;
                }
            }
        }

        private void SceneConfigEditor(SceneConfig sceneConfig, SceneMode mode)
        {
            sceneConfig.mode = mode;
            GUILayout.BeginVertical("Box");
            GUILayout.Label(mode.ToString() + " scenes configuration");
            propertyField.ShowSimpleField("include in build setting", mode.ToString() + "include", ref sceneConfig.includeInBuildSetting);
            GUILayout.BeginHorizontal();
            propertyField.ShowListField("scene asset", sceneConfig.sceneAssets, mode.ToString());
            propertyField.ShowListField("persistent ui prefab", sceneConfig.persistentUiPrefabs, mode.ToString());
            GUILayout.BeginVertical();
            propertyField.ShowAssetField("quiz scene", mode.ToString() + "quiz", ref sceneConfig.quizSceneAsset);
            propertyField.ShowAssetField("vr head scene", mode.ToString() + "vr head", ref sceneConfig.vrHeadSceneAsset);
            propertyField.ShowAssetField("networking scene", mode.ToString() + "networking", ref sceneConfig.networkingSceneAsset);
            GUILayout.FlexibleSpace();
            propertyField.ShowAssetField<DeviceBehaviour>("object selector", mode.ToString() + "selector", ref sceneConfig.behaviourPackage.objectSelector);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        private void DrawTabCallback(int selected, object data, string fieldId)
        {
            SystemConfig config = data as SystemConfig;

            scrollView = GUILayout.BeginScrollView(scrollView);

            if (selected == 0) // scenes
            {
                if (config.sceneConfigs == null || config.sceneConfigs.Count == 0)
                {
                    config.sceneConfigs = new List<SceneConfig>();
                    config.sceneConfigs.Add(new SceneConfig(SceneMode.Desktop));
                    config.sceneConfigs.Add(new SceneConfig(SceneMode.Tablet));
                    config.sceneConfigs.Add(new SceneConfig(SceneMode.Smartphone));
                    config.sceneConfigs.Add(new SceneConfig(SceneMode.VR));
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("Switch to " + (config.isAddressableAssetMode ? "Regular" : "Addressable") + " Asset Mode"))
                {
                    Save();

                    if (config.isAddressableAssetMode)
                        VirtualTrainingEditorUtility.SetToRegularBuildMode();
                    else
                        VirtualTrainingEditorUtility.SetToAddressableAssetsBuildMode();

                    Load();
                }

                if (config.isAddressableAssetMode)
                {
                    if (GUILayout.Button("configure asset group"))
                        Builder.ConfigureAddressableAssets();

                    if (GUILayout.Button("configure and build asset group"))
                        Builder.ConfigureAndBuildAddressableAssets();
                }

                GUILayout.Label("current asset mode : " + (config.isAddressableAssetMode ? "Addressable Asset Mode" : "Regular Asset Mode"));

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                GUILayout.Space(10f);

                propertyField.ShowSimpleField("starting mode", ref config.startingSceneMode);
                propertyField.ShowAssetField("starting scene", ref config.startingSceneAsset);

                GUILayout.Space(10f);

                for (int i = 0; i < config.sceneConfigs.Count; i++)
                {
                    SceneConfigEditor(config.sceneConfigs[i], (SceneMode)i);
                }
            }
            else if (selected == 1) // setting
            {
                GUILayout.Label("system configuration");
                propertyField.ShowSimpleField("max raycast distance", ref config.maxRaycastDistance, 200, 350);

                GUILayout.Space(10f);

                GUILayout.Label("default setting");
                propertyField.ShowSimpleField("rotate speed", ref config.defaultSetting.rotateSpeed, 200, 350);
                propertyField.ShowSimpleField("zoom speed", ref config.defaultSetting.zoomSpeed, 200, 350);
                propertyField.ShowSimpleField("drag speed", ref config.defaultSetting.dragSpeed, 200, 350);
                propertyField.ShowSimpleField("move camera speed", ref config.defaultSetting.cameraTransitionSpeed, 200, 350);
                propertyField.ShowSimpleField("voice volume", ref config.defaultSetting.voiceVolume, 200, 350);
                propertyField.ShowSimpleField("sfx volume", ref config.defaultSetting.sfxVolume, 200, 350);
                propertyField.ShowSimpleField("vr canvas distance", ref config.defaultSetting.vrCanvasDistance, 200, 350);
                propertyField.ShowSimpleField("vr menu height", ref config.defaultSetting.vrMenuHeight, 200, 350);
                propertyField.ShowSimpleField("show environment object", ref config.defaultSetting.showEnvironmentObject, 200, 350);
                propertyField.ShowSimpleField("dynamic ui size", ref config.defaultSetting.dynamicUiSize, 200, 350);
                propertyField.ShowSimpleField("ui theme", ref config.defaultSetting.uiTheme, 200, 350);
                propertyField.ShowSimpleField("rotation speed playerVR", ref config.defaultSetting.speedRotationVR, 200, 350);
                propertyField.ShowSimpleField("speed scrollbar", ref config.defaultSetting.speedScrollbar, 200, 350);
                propertyField.ShowSimpleField("speed menu vr", ref config.defaultSetting.speedLerpMenuVR, 200, 350);
                propertyField.ShowSimpleField("duration confirmation button", ref config.defaultSetting.durationConfirmationButton, 200, 350);

                GUILayout.Space(10f);
                GUILayout.Label("min and max setting");
                propertyField.ShowSimpleField("min rotate speed", ref config.minMaxSetting.rotateSpeed.x, 200, 350);
                propertyField.ShowSimpleField("max rotate speed", ref config.minMaxSetting.rotateSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min zoom speed", ref config.minMaxSetting.zoomSpeed.x, 200, 350);
                propertyField.ShowSimpleField("max zoom speed", ref config.minMaxSetting.zoomSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min drag speed", ref config.minMaxSetting.dragSpeed.x, 200, 350);
                propertyField.ShowSimpleField("max drag speed", ref config.minMaxSetting.dragSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min move camera speed", ref config.minMaxSetting.moveCameraSpeed.x, 200, 350); ;
                propertyField.ShowSimpleField("max move camera speed", ref config.minMaxSetting.moveCameraSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min bgm volume speed", ref config.minMaxSetting.bgmVolume.x, 200, 350);
                propertyField.ShowSimpleField("max bgm volume speed", ref config.minMaxSetting.bgmVolume.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min sfx volume speed", ref config.minMaxSetting.sfxVolume.x, 200, 350);
                propertyField.ShowSimpleField("max sfx volume speed", ref config.minMaxSetting.sfxVolume.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min vr canvas distance", ref config.minMaxSetting.vrCanvasDistance.x, 200, 350);
                propertyField.ShowSimpleField("max vr canvas distance", ref config.minMaxSetting.vrCanvasDistance.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min vr menu height", ref config.minMaxSetting.vrMenuHeight.x, 200, 350);
                propertyField.ShowSimpleField("max vr menu height", ref config.minMaxSetting.vrMenuHeight.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min rotation speed playerVR", ref config.minMaxSetting.speedRotationVR.x, 200, 350);
                propertyField.ShowSimpleField("max rotation speed playerVR", ref config.minMaxSetting.speedRotationVR.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min speed scrollbar", ref config.minMaxSetting.speedScrollbar.x, 200, 350);
                propertyField.ShowSimpleField("max speed scrollbar", ref config.minMaxSetting.speedScrollbar.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min speed menu vr", ref config.minMaxSetting.speedLerpMenuVR.x, 200, 350);
                propertyField.ShowSimpleField("max speed menu vr", ref config.minMaxSetting.speedLerpMenuVR.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min duration confirmation button", ref config.minMaxSetting.durationConfirmationButton.x, 200, 350);
                propertyField.ShowSimpleField("max duration confirmation button", ref config.minMaxSetting.durationConfirmationButton.y, 200, 350);
            }
            else if (selected == 2) // editor icon
            {
                if (GUILayout.Button("add all category"))
                {
                    for (int i = 0; i < System.Enum.GetValues(typeof(IconEnum)).Length; i++)
                    {
                        var icon = new EditorIcon((IconEnum)i, null);
                        config.editorIcons.Add(icon);
                    }
                }

                if (GUILayout.Button("sort category"))
                {
                    for (int i = 0; i < config.editorIcons.Count; i++)
                    {
                        config.editorIcons[i].category = (IconEnum)i;
                    }
                }

                propertyField.ShowListField("icon", config.editorIcons);
            }
            else if (selected == 3) // Editor configuration
            {
                propertyField.ShowSimpleField("selected element color", ref config.editorConfig.selectedElementColor);
                propertyField.ShowSimpleField("max item list field page", ref config.editorConfig.maxItemListFieldPage);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
        }

        private object SceneElementCallback(string listId, object element, int index)
        {
            SceneAsset scene = element as SceneAsset;

            propertyField.ShowAssetField("scene asset", listId + index, ref scene);

            if (index == 0)
                GUILayout.Label("<- main scene");
            else if (index == 1)
                GUILayout.Label("<- ui scene (unloaded when quiz is active)");
            else if (index == 2)
                GUILayout.Label("<- camera scene (unloaded when vr head is active)");

            return scene;
        }

        private object IconElementCallback(string listId, object element, int index)
        {
            EditorIcon editorIcon = element as EditorIcon;
            GUILayout.BeginHorizontal();

            propertyField.ShowSimpleField("category", listId + index, ref editorIcon.category);
            propertyField.ShowAssetField("icon", listId + index + 1, ref editorIcon.icon);

            GUILayout.EndHorizontal();

            return editorIcon;
        }

        private object PersistentUiCallback(string listId, object element, int index)
        {
            GameObject gameObject = element as GameObject;
            propertyField.ShowAssetField("ui prefab", listId + index, ref gameObject);
            return gameObject;
        }
    }
}

#endif
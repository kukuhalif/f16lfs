#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class ColliderDestroyer : EditorWindow
    {
        bool destroyRigidbody = true;

        [MenuItem("Virtual Training/Model/Collider Destroyer")]
        private static void ShowWindow()
        {
            var window = GetWindow<ColliderDestroyer>();
            window.titleContent = new GUIContent("Collider Destroyer");
        }

        private void OnGUI()
        {
            destroyRigidbody = EditorGUILayout.Toggle("destroy rigidbody", destroyRigidbody);

            string names = "";

            if (Selection.gameObjects.Length == 0)
            {
                GUILayout.Label("please select target objects !");
                EditorGUI.BeginDisabledGroup(true);
            }
            else
                names = "on : ";

            var selection = Selection.gameObjects;

            for (int i = 0; i < selection.Length; i++)
            {
                if (i < selection.Length - 2)
                    names += selection[i].name + " , ";
                else if (i < selection.Length - 1)
                    names += selection[i].name + " and ";
                else
                    names += selection[i].name;
            }

            if (GUILayout.Button("destroy collider" + (destroyRigidbody ? " and rigidbody " : " ") + names))
                DestroyCollider();

            if (Selection.gameObjects.Length == 0)
            {
                EditorGUI.EndDisabledGroup();
            }

            Repaint();
        }

        private void DestroyCollider()
        {
            for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
                if (EditorUtility.DisplayDialog("Collider Destroyer", "destroy collider" + (destroyRigidbody ? " and rigidbody " : " ") + Selection.gameObjects[i].name + " ?", "yes", "no"))
                {
                    DestroyCollider(Selection.gameObjects[i]);
                }
            }
        }

        private void DestroyCollider(GameObject obj)
        {
            Collider renderer = obj.GetComponent<Collider>();

            if (renderer != null)
            {
                Undo.RegisterCompleteObjectUndo(renderer, "destroy collider " + obj.name);

                DestroyImmediate(renderer);
            }

            if (destroyRigidbody)
            {
                Rigidbody rigid = obj.GetComponent<Rigidbody>();

                if (rigid != null)
                {
                    Undo.RegisterCompleteObjectUndo(rigid, "destroy rigidbody " + obj.name);

                    DestroyImmediate(rigid);
                }
            }

            for (int i = 0; i < obj.transform.childCount; i++)
            {
                DestroyCollider(obj.transform.GetChild(i).gameObject);
            }
        }
    }
}

#endif
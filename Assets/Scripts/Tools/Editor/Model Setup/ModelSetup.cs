﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.EditorCoroutines.Editor;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using UnityEditor.SceneManagement;

namespace VirtualTraining.Tools
{
    public class ModelSetup : EditorWindow
    {
        [MenuItem("Virtual Training/Model/Model Setup")]
        static void OpenWindow()
        {
            var window = GetWindow<ModelSetup>();
            window.titleContent = new GUIContent("Model Setup");
        }

        [MenuItem("Virtual Training/Model Setup", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        const int MAX_OBJ_PER_FRAME = 500;

        static Action DONE_ACTION;

        GameObject root;
        PropertyField propertyField;

        static bool isDisableAnimator = true;
        static bool isAddMeshCollider = true;

        class NewModel
        {
            public GameObjectType parent;
            public GameObject model;

            public NewModel(object obj) : this()
            {
                GameObject oo = obj as GameObject;
                if (oo != null)
                    model = oo;
            }

            public NewModel()
            {
                parent = new GameObjectType();
            }
        }

        List<NewModel> newModels = new List<NewModel>();
        SceneMode modelSceneMode = SceneMode.Desktop;

        private void OnEnable()
        {
            root = VirtualTrainingSceneManager.GameObjectRoot;
            propertyField = new PropertyField(null, "model setup");
            propertyField.CreateListView("model", null, null, OnModelElementDrawCallback, null, null, false, typeof(GameObject));
            modelSceneMode = DatabaseManager.GetStartingSceneConfig().mode;
        }

        private object OnModelElementDrawCallback(string listId, object element, int index)
        {
            NewModel newModel = element as NewModel;

            propertyField.ShowAssetField("model", listId + index + "a", ref newModel.model);
            propertyField.ShowSimpleField("new model parent", listId + index + "b", ref newModel.parent);

            return newModel;
        }

        private void OnGUI()
        {
            if (!VirtualTrainingEditorUtility.LoadMainSceneWarningField(ref modelSceneMode, position.width))
            {
                return;
            }

            GUILayout.Label("untuk parent object dibawah ini masukkan object yang ingin di setup saja / object yg baru dimasukkan, namun jika ingin nge setup semua object masukkan object root");
            root = EditorGUILayout.ObjectField("Parent object", root, typeof(GameObject), true) as GameObject;
            isDisableAnimator = GUILayout.Toggle(isDisableAnimator, "disable Animator component (agar object dapat digerakkan oleh fitur pull apart");
            isAddMeshCollider = GUILayout.Toggle(isAddMeshCollider, "add Mesh Collider component (agar object dapat dipilih dan digerakkan oleh user");

            GUILayout.Space(10);

            GUILayout.Label("masukkan model (fbx / prefab) yang akan dimasukkan ke dalam object root");
            propertyField.ShowListField("model", newModels);

            GUILayout.Space(10);

            if (GUILayout.Button("Create new object root"))
            {
                root = new GameObject(VirtualTrainingSceneManager.OBJECT_ROOT_NAME);
            }

            if (GUILayout.Button("Clear object"))
            {
                if (EditorUtility.DisplayDialog("model setup", "clear all object root childs ?", "yes", "no"))
                {
                    List<GameObject> objTemp = new List<GameObject>();

                    for (int i = 0; i < root.transform.childCount; i++)
                    {
                        objTemp.Add(root.transform.GetChild(i).gameObject);
                    }

                    foreach (var obj in objTemp)
                    {
                        DestroyImmediate(obj);
                    }
                }
            }

            if (root == null)
                EditorGUI.BeginDisabledGroup(true);

            if (GUILayout.Button("Setup"))
            {
                AddModelToScene();
                StartSetup(root, this, null, modelSceneMode);
            }

            GUILayout.Label("yang dilakukan saat model setup sesuai setting adalah :");
            GUILayout.Label("1. add gameobject reference component pada setiap gameobject model, sehingga gameobject tersebut dapat di reference oleh database");
            if (isAddMeshCollider)
                GUILayout.Label("2. add collider component as trigger");
            else
                GUILayout.Label("2. collider component not added");
            if (isDisableAnimator)
                GUILayout.Label("3. disable animator");
            else
                GUILayout.Label("3. animator not disabled");
            GUILayout.Label("4. check duplicated gameobject id");

            if (root == null)
                EditorGUI.EndDisabledGroup();

            Repaint();
        }

        private void AddModelToScene()
        {
            Debug.Log("add model prefab to scene");

            for (int i = 0; i < newModels.Count; i++)
            {
                GameObject newInstance = Instantiate(newModels[i].model);
                newInstance.name = newModels[i].model.name;
                newInstance.transform.position = newModels[i].model.transform.position;
                newInstance.transform.localScale = newModels[i].model.transform.localScale;
                newInstance.transform.SetParent(newModels[i].parent.gameObject == null ? root.transform : newModels[i].parent.gameObject.transform, true);
            }
        }

        public static void StartSetup(GameObject root, object owner, Action doneAction, SceneMode modelSceneMode)
        {
            Debug.Log("start setup for " + root.name);
            DONE_ACTION = doneAction;
            EditorUtility.DisplayProgressBar("model setup", "setup is working", 0f);
            EditorCoroutineUtility.StartCoroutine(Starting(root, owner, modelSceneMode), owner);
        }

        private static IEnumerator Starting(GameObject root, object owner, SceneMode modelSceneMode)
        {
            yield return EditorCoroutineUtility.StartCoroutine(Setup(root), owner);
            yield return EditorCoroutineUtility.StartCoroutine(CheckDuplicatedId(), owner);
            DoneAction(modelSceneMode);
        }

        private static void DoneAction(SceneMode modelSceneMode)
        {
            Debug.Log("set model scene dirty");
            VirtualTrainingEditorUtility.SetModelSceneDirty(modelSceneMode);
            EditorUtility.ClearProgressBar();
            if (DONE_ACTION != null)
            {
                Debug.Log("invoke done callback action");
                DONE_ACTION.Invoke();
            }
            Debug.Log("model setup done");
            EditorUtility.DisplayDialog("model setup", "setup is complete", "ok");
        }

        static IEnumerator Setup(GameObject root)
        {
            Debug.Log("get all childs and root");
            GameObject[] childArray = root.GetAllChilds();
            List<GameObject> childs = new List<GameObject>(childArray)
            {
                root
            };

            int childCount = childs.Count;
            Debug.Log("total childs is " + childCount);

            GameObject obj = null;
            int counter = 0;

            for (int i = 0; i < childCount; i++)
            {
                obj = childs[i];

                float progress = (float)i / (float)childCount;

                EditorUtility.DisplayProgressBar("model setup", "add gameobject reference " + obj.name, progress);

                // 1. Gameobject reference
                GameObjectReference gameObjectReference = obj.GetComponent<GameObjectReference>();
                if (gameObjectReference == null)
                    gameObjectReference = obj.AddComponent<GameObjectReference>();

                // if id is empty then set with transform instance id
                if (gameObjectReference.Id == 0)
                    gameObjectReference.SetId(obj.transform.GetInstanceID());

                EditorUtility.DisplayProgressBar("model setup", "set object layer " + obj.name, progress);

                EditorUtility.DisplayProgressBar("model setup", "set collider " + obj.name, progress);

                // 2. Collider
                if (isAddMeshCollider)
                {
                    MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
                    if (meshRenderer != null)
                    {
                        Collider collider = obj.GetComponent<Collider>();
                        if (collider == null)
                        {
                            obj.AddComponent<MeshCollider>();
                        }
                    }
                }

                EditorUtility.DisplayProgressBar("model setup", "disable animator " + obj.name, progress);

                // 3. Disable Animator
                if (isDisableAnimator)
                {
                    Animator animator = obj.GetComponent<Animator>();
                    if (animator != null)
                        animator.enabled = false;
                }

                EditorUtility.DisplayProgressBar("model setup", "initialize gameobject reference " + obj.name, progress);

                if (counter > MAX_OBJ_PER_FRAME)
                {
                    counter = 0;
                    yield return null;
                }
                counter++;
            }
        }

        static IEnumerator CheckDuplicatedId()
        {
            Debug.Log("check duplicated Id");
            Debug.Log("get all references");
            GameObjectReference[] references = FindObjectsOfType<GameObjectReference>();

            int referenceCount = references.Length;
            Debug.Log("total references is " + referenceCount);

            int counter = 0;
            int invalidIdCounter = 0;

            Dictionary<int, GameObjectReference> objectReferenceLookup = new Dictionary<int, GameObjectReference>();

            for (int i = 0; i < referenceCount; i++)
            {
                float progress = (float)i / (float)referenceCount;

                EditorUtility.DisplayProgressBar("model setup", "check duplicated id " + references[i].gameObject.name, progress);

                int id = references[i].Id;

                // if id valid
                if (!objectReferenceLookup.ContainsKey(id))
                {
                    objectReferenceLookup.Add(id, references[i]);
                }
                else // if id is invalid
                {
                    // create new id
                    Debug.Log("create new id for " + references[i].gameObject.name);

                    int newId = 0;
                    string parentName = "";

                    do
                    {
                        var parents = references[i].transform.GetAllParents();
                        parentName = "";

                        for (int p = parents.Count - 1; p >= 0; p--)
                        {
                            parentName += parents[p].name + " / ";
                        }

                        newId = Animator.StringToHash(parentName + invalidIdCounter);

                        invalidIdCounter++;
                    } while (objectReferenceLookup.ContainsKey(newId));

                    Debug.Log("new id generated for " + parentName);

                    references[i].SetId(newId);
                }

                if (counter > MAX_OBJ_PER_FRAME)
                {
                    counter = 0;
                    yield return null;
                }
                counter++;
            }
        }
    }
}
#endif
﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.EditorCoroutines.Editor;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Tools
{
    public class ObjectReferenceFinder : EditorWindow
    {
        GameObject root;
        PropertyField propertyField;
        string searchId;
        List<GameObject> result = new List<GameObject>();
        //GameObject resultGameObject;

        //[MenuItem("Virtual Training/Database Tools/Object Reference Finder")]
        static void OpenWindow()
        {
            var window = GetWindow<ObjectReferenceFinder>();
            window.titleContent = new GUIContent("Object Reference Finder");
        }

        private void OnEnable()
        {
            //root = VirtualTrainingSceneManager.GameObjectRoot;
            propertyField = new PropertyField(null, "model setup");
            propertyField.CreateListView("result", null, null, OnModelElementDrawCallback, null, null, false, typeof(GameObject));
        }

        private object OnModelElementDrawCallback(string listId, object element, int index)
        {
            GameObject gameObjectRef = element as GameObject;

            propertyField.ShowSimpleField("Gameobject", listId + index + "b", ref gameObjectRef);

            return gameObjectRef;
        }

        private void OnGUI()
        {
            GUILayout.Label("Masukkan object id yg ingin dicari...");

            GUILayout.Space(10f);
            searchId = EditorGUILayout.TextField("Object Id to search: ", searchId);

            if (GUILayout.Button("Search"))
            {
                Debug.Log("get all gameobject refs");
                GameObjectReference[] gameObjectRefs = FindObjectsOfType<GameObjectReference>(true);

                int objCount = gameObjectRefs.Length;
                Debug.Log("total object refs is " + objCount);

                GameObjectReference objRef = null;
                //int counter = 0;

                for (int i = 0; i < objCount; i++)
                {
                    objRef = gameObjectRefs[i];
                    if (objRef.Id.ToString() == searchId)
                    {
                        Debug.Log("Found: " + objRef.gameObject.name);
                        result.Add(objRef.gameObject);
                        GUILayout.Space(5f);
                        GUILayout.Label(objRef.gameObject.name);
                        //break;
                    }
                }
                if (result.Count > 0)
                {
                    //GUILayout.Space(5f);
                    //resultGameObject = EditorGUILayout.ObjectField("Gameobject result ", resultGameObject, typeof(GameObject), true) as GameObject;
                    //result = propertyField.ShowListFieldReadOnly("result", result);
                }
            }
        }
    }
}
#endif
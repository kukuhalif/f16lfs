using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class HostStartedEvent : VirtualTrainingEvent
    {
        public bool success;

        public HostStartedEvent(bool success)
        {
            this.success = success;
        }
    }

    public class ClientStartedEvent : VirtualTrainingEvent
    {
        public bool success;
        public int code;
        public string message;

        public ClientStartedEvent(bool success, int code, string message)
        {
            this.success = success;
            this.code = code;
            this.message = message;
        }
    }

    public class NewPlayerInstantiatedEvent : VirtualTrainingEvent
    {
        public string playerName;

        public NewPlayerInstantiatedEvent(string playerName)
        {
            this.playerName = playerName;
        }
    }

    public class PlayerDestroyedEvent : VirtualTrainingEvent
    {
        public string playerName;

        public PlayerDestroyedEvent(string playerName)
        {
            this.playerName = playerName;
        }
    }

    public class NetworkFoundResponseEvent : VirtualTrainingEvent
    {
        public DiscoveryResponse discoveryResponse;

        public NetworkFoundResponseEvent(DiscoveryResponse discoveryResponse)
        {
            this.discoveryResponse = discoveryResponse;
        }
    }

    public class MessageReceivedEvent : VirtualTrainingEvent
    {
        public string sender;
        public string message;

        public MessageReceivedEvent(string sender, string message)
        {
            this.sender = sender;
            this.message = message;
        }
    }

    public class HostSyncStateChangedEvent : VirtualTrainingEvent
    {
        public bool syncOn;
        public bool fromNetwork;

        public HostSyncStateChangedEvent(bool syncOn, bool fromNetwork)
        {
            this.syncOn = syncOn;
            this.fromNetwork = fromNetwork;
        }
    }

    public class ClientSyncStateChangedEvent : VirtualTrainingEvent
    {
        public bool syncOn;

        public ClientSyncStateChangedEvent(bool syncOn)
        {
            this.syncOn = syncOn;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class NetworkingGameobject : MonoBehaviour
    {
        [SerializeField] bool forHostOnly;
        bool disabled;

        private void Start()
        {
            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(SessionEndedListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            if (e.isClientOnly || !forHostOnly)
            {
                gameObject.SetActive(false);
                disabled = true;
            }
        }

        private void SessionEndedListener(OnClientStopEvent e)
        {
            if (disabled)
            {
                gameObject.SetActive(true);
                disabled = false;
            }
        }
    }
}


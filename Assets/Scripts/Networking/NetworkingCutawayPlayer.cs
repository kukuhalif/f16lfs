using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Networking
{
    public class NetworkingCutawayPlayer : NetworkBehaviour
    {
        [SyncVar] CutawayState state;
        [SyncVar] bool translate;
        [SyncVar] Vector3 gizmoPosition;
        [SyncVar] Vector3 gizmoRotation;

        Gizmo cutawayGizmo;

        float speed = 5;
        float time;

        SessionManager sessionManager;

        private void Start()
        {
            CutawayManager cutawayManager = FindObjectOfType<CutawayManager>();
            cutawayGizmo = cutawayManager.Gizmo;

            gizmoPosition = cutawayGizmo.transform.position;
            gizmoRotation = cutawayGizmo.transform.rotation.eulerAngles;

            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(ClientStoppedListener);

            sessionManager = FindObjectOfType<SessionManager>();
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            if (isClientOnly)
                return;

            // host
            EventManager.AddListener<ResetCutawayEvent>(ResetCutawayListener);
            EventManager.AddListener<SwitchCutawayModeEvent>(SwitchCutawayModeListener);
            EventManager.AddListener<SwitchCutawayGizmoModeEvent>(SwitchCutawayGizmoListener);
        }

        private void ClientStoppedListener(OnClientStopEvent e)
        {
            if (isClientOnly)
                return;

            // host
            EventManager.AddListener<ResetCutawayEvent>(ResetCutawayListener);
            EventManager.AddListener<SwitchCutawayModeEvent>(SwitchCutawayModeListener);
            EventManager.AddListener<SwitchCutawayGizmoModeEvent>(SwitchCutawayGizmoListener);
        }

        private void Update()
        {
            if (!sessionManager.SyncOn || !cutawayGizmo.gameObject.activeSelf)
                return;

            if (isClientOnly)
            {
                time = Time.deltaTime * speed;
                cutawayGizmo.transform.SetPositionAndRotation(Vector3.Lerp(cutawayGizmo.transform.position, gizmoPosition, time), Quaternion.Slerp(cutawayGizmo.transform.rotation, Quaternion.Euler(gizmoRotation), time));
            }
            else
            {
                gizmoPosition = cutawayGizmo.transform.position;
                gizmoRotation = cutawayGizmo.transform.rotation.eulerAngles;
            }
        }

        public void ReSync()
        {
            EventManager.TriggerEvent(new SwitchCutawayModeEvent(state));
            EventManager.TriggerEvent(new SwitchCutawayGizmoModeEvent(translate));
        }

        private void ResetCutawayListener(ResetCutawayEvent e)
        {
            CmdResetSend();
        }

        // reset host
        [Command(requiresAuthority = false)]
        private void CmdResetSend()
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcResetReceive();
        }

        // reset client
        [ClientRpc]
        private void RpcResetReceive()
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
                EventManager.TriggerEvent(new ResetCutawayEvent());
        }

        private void SwitchCutawayModeListener(SwitchCutawayModeEvent e)
        {
            CmdSwitchModeSend(e.state);
        }

        // switch host
        [Command(requiresAuthority = false)]
        private void CmdSwitchModeSend(CutawayState state)
        {
            if (!isClientOnly) // host
            {
                this.state = state;

                if (sessionManager.SyncOn)
                    RpcSwitchModeReceive(state);
            }
        }

        // switch client
        [ClientRpc]
        private void RpcSwitchModeReceive(CutawayState state)
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
                EventManager.TriggerEvent(new SwitchCutawayModeEvent(state));
        }

        private void SwitchCutawayGizmoListener(SwitchCutawayGizmoModeEvent e)
        {
            CmdGizmoMode(e.translate);
        }

        // gizmo host
        [Command(requiresAuthority = false)]
        private void CmdGizmoMode(bool translate)
        {
            if (!isClientOnly) // host
            {
                this.translate = translate;

                if (sessionManager.SyncOn)
                    RpcGizmoModeReceive(translate);
            }
        }

        // gizmo client
        [ClientRpc]
        private void RpcGizmoModeReceive(bool translate)
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
                EventManager.TriggerEvent(new SwitchCutawayGizmoModeEvent(translate));
        }
    }
}

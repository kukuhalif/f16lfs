using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class PlayerAuthenticator : NetworkAuthenticator
    {
        [SerializeField] SessionManager sessionManager;
        private string localPlayerName;

        #region messages

        public struct AuthRequestMessage : NetworkMessage
        {
            public string authUserName;
        }

        public struct AuthResponseMessage : NetworkMessage
        {
            public byte code;
            public string message;
        }

        #endregion

        #region server

        public override void OnStartServer()
        {
            NetworkServer.RegisterHandler<AuthRequestMessage>(OnAuthRequestMessage, false);
        }

        public override void OnStopServer()
        {
            NetworkServer.UnregisterHandler<AuthRequestMessage>();
        }

        public override void OnServerAuthenticate(NetworkConnectionToClient conn)
        {
            // do nothing...wait for AuthRequestMessage from client
        }

        public void OnAuthRequestMessage(NetworkConnectionToClient conn, AuthRequestMessage msg)
        {
            Debug.Log("request connection from " + msg.authUserName);

            if (!sessionManager.SessionStarted) // boleh masuk jika session belum dimulai
            {
                // Store username in authenticationData
                // This will be read in Player.OnStartServer
                // to set the playerName SyncVar.
                conn.authenticationData = msg.authUserName;

                // create and send msg to client so it knows to proceed
                AuthResponseMessage authResponseMessage = new AuthResponseMessage
                {
                    code = 1,
                    message = "Connection success"
                };

                conn.Send(authResponseMessage);

                // Accept the successful authentication
                ServerAccept(conn);

                EventManager.TriggerEvent(new HostStartedEvent(true));
            }
            else // koneksi ditolak karena session sudah dimulai
            {
                // create and send msg to client so it knows to disconnect
                AuthResponseMessage authResponseMessage = new AuthResponseMessage
                {
                    code = 2,
                    message = "Connection failed, session has started"
                };

                conn.Send(authResponseMessage);

                // must set NetworkConnection isAuthenticated = false
                conn.isAuthenticated = false;

                // disconnect the client after 1 second so that response message gets delivered
                StartCoroutine(DelayedDisconnect(conn, 1f));

                EventManager.TriggerEvent(new HostStartedEvent(false));
            }
        }

        IEnumerator DelayedDisconnect(NetworkConnectionToClient conn, float waitTime)
        {
            yield return new WaitForSeconds(waitTime);

            // Reject the unsuccessful authentication
            ServerReject(conn);
        }

        #endregion

        #region client

        public void SetLocalPlayerName(string newName)
        {
            localPlayerName = newName;
        }

        public string GetLocalPlayerName()
        {
            return localPlayerName;
        }

        public override void OnStartClient()
        {
            NetworkClient.RegisterHandler<AuthResponseMessage>(OnAuthResponseMessage, false);
        }

        public override void OnStopClient()
        {
            NetworkClient.UnregisterHandler<AuthResponseMessage>();
        }

        public override void OnClientAuthenticate()
        {
            AuthRequestMessage authRequestMessage = new AuthRequestMessage
            {
                authUserName = localPlayerName,
            };

            NetworkClient.connection.Send(authRequestMessage);
        }

        public void OnAuthResponseMessage(AuthResponseMessage msg)
        {
            if (msg.code == 1)
            {
                // Authentication has been accepted
                ClientAccept();

                EventManager.TriggerEvent(new ClientStartedEvent(true, msg.code, msg.message));
            }
            else if (msg.code == 2)
            {
                // Authentication has been rejected
                // StopHost works for both host client and remote clients
                NetworkManager.singleton.StopHost();

                EventManager.TriggerEvent(new ClientStartedEvent(false, msg.code, msg.message));
            }
        }

        #endregion
    }
}

using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class NetworkingObjectListPlayer : NetworkBehaviour
    {
        SessionManager sessionManager;

        private void Start()
        {
            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(ClientStoppedListener);

            sessionManager = FindObjectOfType<SessionManager>();
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            if (isClientOnly)
                return;

            // host
            EventManager.AddListener<PartObjectEvent>(PartObjectListener);
            EventManager.AddListener<ResetPartObjectEvent>(ResetPartObjectListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        private void ClientStoppedListener(OnClientStopEvent e)
        {
            if (isClientOnly)
                return;

            // host
            EventManager.RemoveListener<PartObjectEvent>(PartObjectListener);
            EventManager.RemoveListener<ResetPartObjectEvent>(ResetPartObjectListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        private void ResetPartObjectListener(ResetPartObjectEvent e)
        {
            CmdResetSend();
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            CmdResetSend();
        }

        // reset host
        [Command(requiresAuthority = false)]
        private void CmdResetSend()
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcResetReceive();
        }

        // reset client
        [ClientRpc]
        private void RpcResetReceive()
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
                EventManager.TriggerEvent(new ResetPartObjectEvent());
        }

        private void PartObjectListener(PartObjectEvent e)
        {
            if (!sessionManager.SyncOn)
                return;

            if (e.fromContentPlayer)
                return;

            List<int> parts = new List<int>();
            for (int i = 0; i < e.objects.Count; i++)
            {
                int action = (int)e.objects[i].action;
                int id = e.objects[i].target.Id;

                parts.Add(id);
                parts.Add(action);
            }

            CmdPartObjectSend(parts.ToArray());
        }

        // part object host
        [Command(requiresAuthority = false)]
        private void CmdPartObjectSend(int[] parts)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcPartObjectReceive(parts);
        }

        // part object client
        [ClientRpc]
        private void RpcPartObjectReceive(int[] parts)
        {
            if (!sessionManager.SyncOn)
                return;

            if (!isClientOnly) // client only
                return;

            List<PartObject> partObjects = new List<PartObject>();
            for (int i = 0; i < parts.Length;)
            {
                PartObject part = new PartObject();
                part.target.Id = parts[i];
                i++;
                part.action = (PartObjectAction)parts[i];
                partObjects.Add(part);
                i++;
            }

            EventManager.TriggerEvent(new PartObjectEvent(false, partObjects));
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;
using VirtualTraining.UI;

namespace VirtualTraining.Networking
{
    // todo : flight scenario dan remove install belum di handle
    public class NetworkingMateriPlayer : NetworkBehaviour
    {
        #region cache

        NetworkingCameraController cameraController;
        SessionManager sessionManager;

        List<MateriTreeElement> materies = new List<MateriTreeElement>();
        List<MaintenanceTreeElement> maintenanceDatas = new List<MaintenanceTreeElement>();
        List<TroubleshootNode> troubleshootNodes = new List<TroubleshootNode>();

        [SyncVar] MateriElementType lastMateriElementType;
        [SyncVar(hook = nameof(SetClientCache))] int lastMateriIndex = -1;
        [SyncVar] int lastMaintenanceIndex = -1;
        [SyncVar] int lastTroubleshootIndex = -1;

        #endregion

        #region initialization

        private void Start()
        {
            cameraController = FindObjectOfType<NetworkingCameraController>();
            sessionManager = FindObjectOfType<SessionManager>();

            SetMateriList();

            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void SetMateriList()
        {
            var materiTree = DatabaseManager.GetMateriTree();
            TreeElementUtility.TreeToList(materiTree, materies);
        }

        #endregion

        #region get index

        private int GetMateriIndex(int id)
        {
            for (int i = 0; i < materies.Count; i++)
            {
                if (id == materies[i].id)
                {
                    return i;
                }
            }

            return -1;
        }

        private int GetMaintenanceIndex(int id)
        {
            for (int i = 0; i < maintenanceDatas.Count; i++)
            {
                if (id == maintenanceDatas[i].id)
                {
                    return i;
                }
            }

            return -1;
        }

        private int GetTroubleshootIndex(int id)
        {
            for (int i = 0; i < troubleshootNodes.Count; i++)
            {
                if (id == troubleshootNodes[i].id)
                {
                    return i;
                }
            }

            return -1;
        }

        #endregion

        #region event system

        private void SessionStartedListener(SessionStartedEvent e)
        {
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
            EventManager.AddListener<MaintenancePlayEvent>(PlayMaintenanceListener);
            EventManager.AddListener<TroubleshootPlayEvent>(PlayTroubleshootListener);
            EventManager.AddListener<ClientSyncStateChangedEvent>(ClientSyncStateChangedListener);

            if (!isClientOnly) // host only
                EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        private void ClientStoppedListener(OnClientStopEvent e)
        {
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
            EventManager.RemoveListener<MaintenancePlayEvent>(PlayMaintenanceListener);
            EventManager.RemoveListener<TroubleshootPlayEvent>(PlayTroubleshootListener);
            EventManager.RemoveListener<ClientSyncStateChangedEvent>(ClientSyncStateChangedListener);

            if (!isClientOnly) // host only
                EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        #endregion

        #region event listener

        private void FreePlayListener(FreePlayEvent e)
        {
            if (isClientOnly)
                return;

            lastMateriElementType = MateriElementType.Materi;
            lastMateriIndex = -1;
            lastMaintenanceIndex = -1;
            lastTroubleshootIndex = -1;
        }


        private void ClientSyncStateChangedListener(ClientSyncStateChangedEvent e)
        {
            if (!e.syncOn)
                return;

            bool played = false;
            if (lastMateriIndex > -1)
            {
                switch (lastMateriElementType)
                {
                    case MateriElementType.Materi:

                        cameraController.PauseSyncTemporary();
                        EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[lastMateriIndex]));
                        played = true;

                        break;
                    case MateriElementType.Maintenance:

                        if (lastMaintenanceIndex > -1)
                        {
                            cameraController.PauseSyncTemporary();
                            EventManager.TriggerEvent(new TriggerMaintenanceInsidePanelEvent(maintenanceDatas[lastMaintenanceIndex]));
                            played = true;
                        }

                        break;
                    case MateriElementType.Troubleshoot:

                        if (lastTroubleshootIndex > -1)
                        {
                            cameraController.PauseSyncTemporary();
                            EventManager.TriggerEvent(new TriggerTroubleshootInsidePanelEvent(troubleshootNodes[lastTroubleshootIndex]));
                            played = true;
                        }

                        break;
                    case MateriElementType.FlightScenario:

                        Debug.LogWarning("flight scenario belum di handle");

                        break;
                    case MateriElementType.RemoveInstall:

                        Debug.LogWarning("remove install belum di handle");

                        break;
                }
            }
            else if (lastMateriIndex == -1) // free play
            {
                played = true;
                EventManager.TriggerEvent(new FreePlayEvent());
            }

            if (played)
                cameraController.ReSyncCamera(true);
            else
                cameraController.ReSyncCamera(false);
        }

        private void SetClientCache(int _, int newVal)
        {
            if (isClientOnly)
            {
                if (newVal > -1 && newVal < materies.Count)
                    SetCache(materies[newVal]);
            }
        }

        private void SetCache(MateriTreeElement materiTreeElement)
        {
            switch (materiTreeElement.MateriData.elementType)
            {
                // cache materi tidak diperlukan

                case MateriElementType.Maintenance:

                    maintenanceDatas = materiTreeElement.MateriData.maintenanceTree.GetTreeElementListWrapper().treeElements;

                    break;
                case MateriElementType.Troubleshoot:

                    troubleshootNodes = materiTreeElement.MateriData.troubleshootData.troubleshootGraph.GetToubleshootNodes();

                    break;
                case MateriElementType.FlightScenario:

                    Debug.LogWarning("flight scenario materi belum di handle");

                    break;
                case MateriElementType.RemoveInstall:

                    Debug.LogWarning("remove install materi belum di handle");

                    break;
            }
        }

        private void PlayMateriListener(MateriEvent e)
        {
            if (e.materiTreeElement == null)
            {
                if (!isClientOnly)
                    lastMateriIndex = -1;

                return;
            }

            SetCache(e.materiTreeElement);

            int index = GetMateriIndex(e.materiTreeElement.id);
            var type = e.materiTreeElement.MateriData.elementType;

            if (isClientOnly) // host only
                return;

            CmdMateriSend(index, type);
        }

        private void PlayMaintenanceListener(MaintenancePlayEvent e)
        {
            if (e.MaintenanceTreeElement == null)
            {
                if (!isClientOnly)
                    lastMaintenanceIndex = -1;

                return;
            }

            int index = GetMaintenanceIndex(e.MaintenanceTreeElement.id);

            if (isClientOnly) // host only
                return;

            CmdMaintenanceSend(index);
        }

        private void PlayTroubleshootListener(TroubleshootPlayEvent e)
        {
            if (e.troubleshootNode == null)
            {
                if (!isClientOnly)
                    lastTroubleshootIndex = -1;

                return;
            }

            int index = GetTroubleshootIndex(e.troubleshootNode.id);

            if (isClientOnly) // host only
                return;

            CmdTroubleshootSend(index);
        }

        #endregion

        #region network cmd and rpc

        // materi host
        [Command(requiresAuthority = false)]
        private void CmdMateriSend(int materiIndex, MateriElementType materiElementType)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
            {
                lastMateriIndex = materiIndex;
                lastMateriElementType = materiElementType;
                RpcMateriReceive(materiIndex);
            }
        }

        // materi client
        [ClientRpc]
        private void RpcMateriReceive(int materiIndex)
        {
            if (!isClientOnly)
                return;

            if (sessionManager.SyncOn)
            {
                cameraController.PauseSyncTemporary();
                EventManager.TriggerEvent(new TriggerMateriInsideTreeViewUIEvent(materies[materiIndex]));
            }
        }

        // maintenance host
        [Command(requiresAuthority = false)]
        private void CmdMaintenanceSend(int maintenanceIndex)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
            {
                lastMaintenanceIndex = maintenanceIndex;
                lastMateriElementType = MateriElementType.Maintenance;
                RpcMaintenanceReceive(maintenanceIndex);
            }
        }

        // maintenance client
        [ClientRpc]
        private void RpcMaintenanceReceive(int maintenanceIndex)
        {
            if (!isClientOnly)
                return;

            if (sessionManager.SyncOn)
            {
                cameraController.PauseSyncTemporary();
                EventManager.TriggerEvent(new TriggerMaintenanceInsidePanelEvent(maintenanceDatas[maintenanceIndex]));
            }
        }

        // troubleshoot host
        [Command(requiresAuthority = false)]
        private void CmdTroubleshootSend(int troubleshootIndex)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
            {
                lastTroubleshootIndex = troubleshootIndex;
                RpcTroubleshootReceive(troubleshootIndex);
            }
        }

        // troubleshoot client
        [ClientRpc]
        private void RpcTroubleshootReceive(int troubleshootIndex)
        {
            if (!isClientOnly)
                return;

            if (sessionManager.SyncOn)
            {
                cameraController.PauseSyncTemporary();
                EventManager.TriggerEvent(new TriggerTroubleshootInsidePanelEvent(troubleshootNodes[troubleshootIndex]));
            }
        }

        #endregion
    }
}


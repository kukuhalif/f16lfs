using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Mirror;
using Mirror.Discovery;
using System.Net;

namespace VirtualTraining.Networking
{
    public class DiscoveryRequest : NetworkMessage
    {
        public string clientName;
    }

    public class DiscoveryResponse : NetworkMessage
    {
        // The server that sent this
        // this is a property so that it is not serialized,  but the
        // client fills this up after we receive it
        public IPEndPoint EndPoint { get; set; }

        public Uri uri;

        // Prevent duplicate server appearance when a connection can be made via LAN on multiple NICs
        public long serverId;

        public string serverName;
        public bool sessionStarted;
    }

    public class VirtualTrainingNetworkDiscovery : NetworkDiscoveryBase<DiscoveryRequest, DiscoveryResponse>
    {
        [SerializeField] PlayerAuthenticator playerAuthenticator;
        [SerializeField] SessionManager sessionManager;

        public long ServerId { get; private set; }
        public Action<DiscoveryResponse> OnServerFound;

        [Tooltip("Transport to be advertised during discovery")]
        public Transport transport;

        public override void Start()
        {
            ServerId = RandomLong();

            // active transport gets initialized in awake
            // so make sure we set it here in Start()  (after awakes)
            // Or just let the user assign it in the inspector
            if (transport == null)
                transport = Transport.active;

            base.Start();
        }

        protected override DiscoveryResponse ProcessRequest(DiscoveryRequest request, IPEndPoint endpoint)
        {
            // In this case we don't do anything with the request
            // but other discovery implementations might want to use the data
            // in there,  This way the client can ask for
            // specific game mode or something

            try
            {
                var req = new DiscoveryResponse();
                req.serverName = playerAuthenticator.GetLocalPlayerName();
                req.sessionStarted = sessionManager.SessionStarted;
                req.uri = transport.ServerUri();
                req.serverId = ServerId;
                return req;

                // this is an example reply message,  return your own
                // to include whatever is relevant for your game
            }
            catch (NotImplementedException)
            {
                Debug.LogError($"Transport {transport} does not support network discovery");
                throw;
            }
        }

        protected override void ProcessResponse(DiscoveryResponse response, IPEndPoint endpoint)
        {
            // we received a message from the remote endpoint
            response.EndPoint = endpoint;

            // although we got a supposedly valid url, we may not be able to resolve
            // the provided host
            // However we know the real ip address of the server because we just
            // received a packet from it,  so use that as host.
            UriBuilder realUri = new UriBuilder(response.uri)
            {
                Host = response.EndPoint.Address.ToString()
            };
            response.uri = realUri.Uri;

            OnServerFound.Invoke(response);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class NetworkingCameraController : NetworkBehaviour
    {
        [SerializeField] PlayerAuthenticator authenticator;
        [SerializeField] VirtualTrainingNetworkManager networkManager;
        [SerializeField] SessionManager sessionManager;
        [SerializeField] NetworkingToolbarPlayer toolbarPlayer;

        [SyncVar(hook = nameof(MoveCamera))]
        Vector3 cameraPosition;
        [SyncVar(hook = nameof(MoveCamera))]
        Vector3 cameraRotation;

        [SyncVar(hook = nameof(FovCamera))]
        float cameraFov;
        [SyncVar(hook = nameof(OrthographicCamera))]
        float cameraOrthographic;

        public bool pauseSyncTemporary = false;

        private void Start()
        {
            EventManager.AddListener<SessionStartedEvent>(SessionListener);
            EventManager.AddListener<OnClientStopEvent>(ClientStoppedListner);
            EventManager.AddListener<ClientSyncStateChangedEvent>(ClientSyncStateChangedListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionListener);
            EventManager.RemoveListener<OnClientStopEvent>(ClientStoppedListner);
            EventManager.RemoveListener<ClientSyncStateChangedEvent>(ClientSyncStateChangedListener);
        }

        private void ClientSyncStateChangedListener(ClientSyncStateChangedEvent e)
        {
            VirtualTrainingCamera.IsMovementEnabled = !e.syncOn;
        }

        public void ReSyncCamera(bool onCameraArrive)
        {
            if (onCameraArrive)
                EventManager.AddListener<CameraArriveEvent>(ReSyncCamera);
            else
            {
                VirtualTrainingCamera.SetCameraDataFromServer(cameraPosition, cameraRotation, cameraFov, cameraOrthographic);
                toolbarPlayer.ReSync();
            }
        }

        private void ReSyncCamera(CameraArriveEvent e)
        {
            EventManager.RemoveListener<CameraArriveEvent>(UnpauseSyncCamera);
            VirtualTrainingCamera.SetCameraDataFromServer(cameraPosition, cameraRotation, cameraFov, cameraOrthographic);
            toolbarPlayer.ReSync();
        }

        public void PauseSyncTemporary()
        {
            pauseSyncTemporary = true;
            EventManager.AddListener<CameraArriveEvent>(UnpauseSyncCamera);
        }

        private void UnpauseSyncCamera(CameraArriveEvent e)
        {
            EventManager.RemoveListener<CameraArriveEvent>(UnpauseSyncCamera);
            pauseSyncTemporary = false;
        }

        private void Update()
        {
            if (!sessionManager.SyncOn || isClientOnly || !networkManager.HostStarted)
                return;

            cameraPosition = VirtualTrainingCamera.CameraPosition;
            cameraRotation = VirtualTrainingCamera.CameraRotation;

            cameraFov = VirtualTrainingCamera.CurrentCamera.fieldOfView;
            cameraOrthographic = VirtualTrainingCamera.CurrentCamera.orthographicSize;
        }

        private void MoveCamera(Vector3 _, Vector3 newVal)
        {
            if (sessionManager.SyncOn && isClientOnly && !pauseSyncTemporary)
                VirtualTrainingCamera.SetCameraDataFromServer(cameraPosition, cameraRotation, cameraFov, cameraOrthographic);
        }

        private void FovCamera(float _, float newVal)
        {
            if (sessionManager.SyncOn && isClientOnly && !pauseSyncTemporary)
                VirtualTrainingCamera.SetCameraDataFromServer(cameraPosition, cameraRotation, cameraFov, cameraOrthographic);
        }

        private void OrthographicCamera(float _, float newVal)
        {
            if (sessionManager.SyncOn && isClientOnly && !pauseSyncTemporary)
                VirtualTrainingCamera.SetCameraDataFromServer(cameraPosition, cameraRotation, cameraFov, cameraOrthographic);
        }

        private void SessionListener(SessionStartedEvent e)
        {
            if (isClientOnly)
                VirtualTrainingCamera.IsMovementEnabled = false;
            else
                VirtualTrainingCamera.IsMovementEnabled = true;

            pauseSyncTemporary = false;
        }

        private void ClientStoppedListner(OnClientStopEvent e)
        {
            VirtualTrainingCamera.IsMovementEnabled = true;
        }
    }
}

using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI;

namespace VirtualTraining.Networking
{
    // todo : flight scenario dan remove install belum di handle
    public class NetworkingFigurePlayer : NetworkBehaviour
    {
        NetworkingCameraController cameraController;
        SessionManager sessionManager;

        private void Start()
        {
            cameraController = FindObjectOfType<NetworkingCameraController>();
            sessionManager = FindObjectOfType<SessionManager>();

            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            EventManager.AddListener<UpdateFigurePanelEvent>(FigurePlayListener);
            EventManager.AddListener<UpdateCameraPanelEvent>(CameraPanelUpdateListener);
        }

        private void ClientStoppedListener(OnClientStopEvent e)
        {
            EventManager.RemoveListener<UpdateFigurePanelEvent>(FigurePlayListener);
            EventManager.RemoveListener<UpdateCameraPanelEvent>(CameraPanelUpdateListener);
        }

        private void FigurePlayListener(UpdateFigurePanelEvent e)
        {
            if (isClientOnly) // for host only
                return;

            CmdFigureSend(e.currentFigure, e.type);
        }

        // figure host
        [Command(requiresAuthority = false)]
        private void CmdFigureSend(int index, MateriElementType materiElementType)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcFigureReceive(index, materiElementType);
        }

        // figure client
        [ClientRpc]
        private void RpcFigureReceive(int figureIndex, MateriElementType materiElementType)
        {
            if (!isClientOnly) // for client only
                return;

            if (sessionManager.SyncOn)
            {
                cameraController.PauseSyncTemporary();
                EventManager.TriggerEvent(new PlayFigureEvent(materiElementType, figureIndex));
            }
        }

        private void CameraPanelUpdateListener(UpdateCameraPanelEvent e)
        {
            CmdCameraSend(e.index);
        }

        // camera host
        [Command(requiresAuthority = false)]
        private void CmdCameraSend(int index)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcCameraReceive(index);
        }

        // camera client
        [ClientRpc]
        private void RpcCameraReceive(int index)
        {
            if (!isClientOnly) // for client only
                return;

            cameraController.PauseSyncTemporary();
            EventManager.TriggerEvent(new UpdateCameraPanelEvent(index));
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;
//using Utp;
using System;
//using Unity.Services.Authentication;
//using Unity.Services.Core;

namespace VirtualTraining.Networking
{
    public class VirtualTrainingNetworkManager : NetworkManager
    {
        bool hostStarted;

        public bool HostStarted { get => hostStarted; }

        public bool isLoggedIn = false;

        public override void Start()
        {
            base.Start();

            // pake lan biasa aja
            //UnityLogin();
        }

        public override void OnStartHost()
        {
            hostStarted = true;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.TriggerEvent(new FreePlayEvent());
        }

        //public async void UnityLogin()
        //{
        //    try
        //    {
        //        await UnityServices.InitializeAsync();
        //        await AuthenticationService.Instance.SignInAnonymouslyAsync();
        //        Debug.Log("Logged into Unity, player ID: " + AuthenticationService.Instance.PlayerId);
        //        isLoggedIn = true;
        //    }
        //    catch (Exception e)
        //    {
        //        isLoggedIn = false;
        //        Debug.Log(e);
        //    }
        //}

        public void Disconnect()
        {
            if (hostStarted)
                StopHost();
            else
                StopClient();

            hostStarted = false;
        }

        public void SetServerNetworkAddress(string networkAddress)
        {
            this.networkAddress = networkAddress;
        }

        public override void OnStopClient()
        {
            Debug.Log("stop client");
            EventManager.TriggerEvent(new OnClientStopEvent());
        }
    }
}

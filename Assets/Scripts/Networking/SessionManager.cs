using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class SessionManager : NetworkBehaviour
    {
        bool sessionStarted;

        public bool SessionStarted { get => sessionStarted; }

        #region start session

        public void StartSession()
        {
            CmdStartSession();
        }

        public void StopSession()
        {
            sessionStarted = false;
        }

        [Command(requiresAuthority = false)]
        private void CmdStartSession()
        {
            if (!isClientOnly) // host
                RpcStartSession();
        }

        [ClientRpc]
        private void RpcStartSession()
        {
            sessionStarted = true;
            EventManager.TriggerEvent(new SessionStartedEvent(isClientOnly));
        }

        #endregion

        #region sync unsync

        private bool syncOn = true;

        public bool SyncOn
        {
            get
            {
                return syncOn;
            }
        }

        public void SetSync(bool sync)
        {
            syncOn = sync;

            if (isClientOnly)
            {
                EventManager.TriggerEvent(new ClientSyncStateChangedEvent(sync));
            }
            else
            {
                EventManager.TriggerEvent(new HostSyncStateChangedEvent(sync, false));
                CmdSyncSession(sync);
            }
        }

        [Command(requiresAuthority = false)]
        private void CmdSyncSession(bool sync)
        {
            RpcSyncSession(sync);
        }

        [ClientRpc]
        private void RpcSyncSession(bool sync)
        {
            if (!isClientOnly) // client only
                return;

            syncOn = sync;
            EventManager.TriggerEvent(new HostSyncStateChangedEvent(sync, true));
        }

        #endregion
    }
}
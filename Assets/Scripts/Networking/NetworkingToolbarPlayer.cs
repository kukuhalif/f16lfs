using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Networking
{
    public class NetworkingToolbarPlayer : NetworkBehaviour
    {
        class ClientTransform
        {
            public Transform transform;
            public Vector3 position;
            public Quaternion rotation;

            public ClientTransform()
            {

            }
        }

        float speed = 5;
        float time;

        ObjectSelector objectSelector;
        NetworkingCameraController cameraController;
        NetworkingCutawayPlayer cutawayPlayer;
        List<NetworkingHostObjectState> networkingHostObjectStates = new List<NetworkingHostObjectState>();
        Dictionary<int, ClientTransform> clientTransformLookup = new Dictionary<int, ClientTransform>();

        [SyncVar] bool isPullApartActive = false;
        [SyncVar] bool isMultipleSelectActive = false;

        readonly SyncList<int> selectedObjects = new SyncList<int>(); //public List<int> debugSelecteds = new List<int>();

        readonly SyncList<int> hidedObjects = new SyncList<int>(); //public List<int> debugHideds = new List<int>();

        readonly SyncList<int> xrayObjects = new SyncList<int>(); //public List<int> debugXrays = new List<int>();

        PartObjectManager partObjectManager;

        SessionManager sessionManager;

        private void Start()
        {
            objectSelector = FindObjectOfType<ObjectSelector>();
            cameraController = FindObjectOfType<NetworkingCameraController>();
            sessionManager = FindObjectOfType<SessionManager>();
            partObjectManager = FindObjectOfType<PartObjectManager>();
            cutawayPlayer = FindObjectOfType<NetworkingCutawayPlayer>();

            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(ClientStoppedListener);
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            if (isClientOnly) // client
            {
                EventManager.AddListener<ObjectInteractionResetObjectEvent>(ClientResetObjectPositionListener);
                EventManager.AddListener<FreePlayEvent>(ClientFreePlayListener);
                EventManager.AddListener<ClientSyncStateChangedEvent>(UnSyncListener);
                return;
            }

            // host
            EventManager.AddListener<ObjectInteractionResetObjectEvent>(HostResetObjectPositionListener);
            EventManager.AddListener<FreePlayEvent>(HostFreePlayListener);
            EventManager.AddListener<ObjectSelectionEvent>(HostObjectSelectionListener);
            EventManager.AddListener<ObjectInteractionVisibleEvent>(VisibleObjectListener);
            EventManager.AddListener<ObjectInteractionXRayEvent>(XRayListener);
        }

        private void ClientStoppedListener(OnClientStopEvent e)
        {
            if (isClientOnly) // client
            {
                EventManager.RemoveListener<ObjectInteractionResetObjectEvent>(ClientResetObjectPositionListener);
                EventManager.RemoveListener<FreePlayEvent>(ClientFreePlayListener);
                EventManager.RemoveListener<ClientSyncStateChangedEvent>(UnSyncListener);
                return;
            }

            // host
            EventManager.RemoveListener<ObjectInteractionResetObjectEvent>(HostResetObjectPositionListener);
            EventManager.RemoveListener<FreePlayEvent>(HostFreePlayListener);
            EventManager.RemoveListener<ObjectSelectionEvent>(HostObjectSelectionListener);
            EventManager.RemoveListener<ObjectInteractionVisibleEvent>(VisibleObjectListener);
            EventManager.RemoveListener<ObjectInteractionXRayEvent>(XRayListener);

            ClearNetworkingObjectStates();
        }

        private void HostResetObjectPositionListener(ObjectInteractionResetObjectEvent e)
        {
            if (isClientOnly)
                return;

            //ClearNetworkingObjectStates();
            CmdResetObjectSend();
        }

        private void HostFreePlayListener(FreePlayEvent e)
        {
            if (isClientOnly)
                return;

            //ClearNetworkingObjectStates();
            CmdFreePlaySend();
        }

        private void ClientResetObjectPositionListener(ObjectInteractionResetObjectEvent e)
        {

        }

        private void ClientFreePlayListener(FreePlayEvent e)
        {

        }

        private void UnSyncListener(ClientSyncStateChangedEvent e) // client only
        {
            if (e.syncOn) // un sync only
                return;

        }

        public void ReSync()
        {
            // set selected object
            objectSelector.SelectObject(0, isPullApartActive, isMultipleSelectActive);
            foreach (var selected in selectedObjects)
            {
                objectSelector.SelectObject(selected, isPullApartActive, isMultipleSelectActive);
            }

            // set hide show object
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
            foreach (var hided in hidedObjects)
            {
                objectSelector.HideObject(hided);
            }

            // set xray obj
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(false));
            foreach (var xray in xrayObjects)
            {
                partObjectManager.XrayObject(xray);
            }

            cutawayPlayer.ReSync();
        }

        #region reset object

        // freeplay host
        [Command(requiresAuthority = false)]
        private void CmdResetObjectSend()
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcResetObjectReceive();
        }

        // freeplay client
        [ClientRpc]
        private void RpcResetObjectReceive()
        {
            if (!isClientOnly)
                return;

            //if (sessionManager.SyncOn)
            //    EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
        }

        #endregion

        #region freeplay

        // freeplay host
        [Command(requiresAuthority = false)]
        private void CmdFreePlaySend()
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
                RpcFreePlayReceive();
        }

        // freeplay client
        [ClientRpc]
        private void RpcFreePlayReceive()
        {
            if (!isClientOnly)
                return;

            if (sessionManager.SyncOn)
            {
                cameraController.PauseSyncTemporary();
                EventManager.TriggerEvent(new FreePlayEvent());
            }
        }

        #endregion

        #region moved object (selected)

        private void ClearNetworkingObjectStates()
        {
            for (int i = 0; i < networkingHostObjectStates.Count; i++)
            {
                Destroy(networkingHostObjectStates[i]);
            }

            networkingHostObjectStates.Clear();
        }

        private void HostObjectSelectionListener(ObjectSelectionEvent e)
        {
            if (isClientOnly)
                return;

            HostSelectObject(e.id);

            isPullApartActive = objectSelector.IsPullApartActive;
            isMultipleSelectActive = objectSelector.IsMultipleSelectActive;

            if (e.id == 0)
            {
                // ClearNetworkingObjectStates();

                selectedObjects.Clear();

                return;
            }
            else
            {
                if (!selectedObjects.Contains(e.id))
                    selectedObjects.Add(e.id);
            }

            GameObjectReference gor = GameObjectReference.GetReference(e.id);
            if (gor == null)
                return;

            NetworkingHostObjectState nos = gor.gameObject.GetComponent<NetworkingHostObjectState>();
            if (nos == null)
            {
                nos = gor.gameObject.AddComponent<NetworkingHostObjectState>();
                networkingHostObjectStates.Add(nos);
                nos.Setup(gor.Id, SendPosition, SendRotation);

                CmdAddObjectPosition(gor.Id);
            }
        }

        // add object position host
        [Command(requiresAuthority = false)]
        private void CmdAddObjectPosition(int id)
        {
            if (!isClientOnly) // host
                RpcAddObjectPosition(id);
        }

        // add object position client
        [ClientRpc]
        private void RpcAddObjectPosition(int id)
        {
            if (!isClientOnly) // client only
                return;

            GameObjectReference gor = GameObjectReference.GetReference(id);

            if (!clientTransformLookup.ContainsKey(gor.Id))
            {
                var newCT = new ClientTransform();
                newCT.position = gor.transform.position;
                newCT.rotation = gor.transform.rotation;
                newCT.transform = gor.transform;
                clientTransformLookup.Add(gor.Id, newCT);
            }
        }

        private void SendPosition(int id, Vector3 position)
        {
            CmdPositionSend(id, position);
        }

        private void SendRotation(int id, Vector3 rotation)
        {
            CmdRotationSend(id, rotation);
        }

        // object position host
        [Command(requiresAuthority = false)]
        private void CmdPositionSend(int id, Vector3 position)
        {
            if (!isClientOnly) // host
                RpcPositionReceive(id, position);
        }

        // object position client
        [ClientRpc]
        private void RpcPositionReceive(int id, Vector3 position)
        {
            if (!isClientOnly) // client only
                return;

            GameObjectReference gor = GameObjectReference.GetReference(id);

            if (clientTransformLookup.ContainsKey(gor.Id))
            {
                clientTransformLookup[gor.Id].position = position;
            }
        }

        // object position host
        [Command(requiresAuthority = false)]
        private void CmdRotationSend(int id, Vector3 rotation)
        {
            if (!isClientOnly) // host
                RpcRotationReceive(id, rotation);
        }

        // object position client
        [ClientRpc]
        private void RpcRotationReceive(int id, Vector3 rotation)
        {
            if (!isClientOnly) // client only
                return;

            GameObjectReference gor = GameObjectReference.GetReference(id);

            if (clientTransformLookup.ContainsKey(gor.Id))
            {
                clientTransformLookup[gor.Id].rotation = Quaternion.Euler(rotation);
            }
        }

        private void Update()
        {
            //debugSelecteds.Clear();
            //foreach (var item in selectedObjects)
            //{
            //    debugSelecteds.Add(item);
            //}

            //debugHideds.Clear();
            //foreach (var item in hidedObjects)
            //{
            //    debugHideds.Add(item);
            //}

            //debugXrays.Clear();
            //foreach (var item in xrayObjects)
            //{
            //    debugXrays.Add(item);
            //}

            if (!sessionManager.SyncOn)
                return;

            time = Time.deltaTime * speed;
            foreach (var transform in clientTransformLookup.Values)
            {
                transform.transform.SetPositionAndRotation(Vector3.Lerp(transform.transform.position, transform.position, time),
                    Quaternion.Slerp(transform.transform.rotation, transform.rotation, time));
            }
        }

        #endregion

        #region select object

        private void HostSelectObject(int id)
        {
            CmdSelectObjectSend(id);
        }

        // object position host
        [Command(requiresAuthority = false)]
        private void CmdSelectObjectSend(int id)
        {
            if (!isClientOnly && sessionManager.SyncOn) // host
            {
                RpcSelectObjectReceive(id, objectSelector.IsPullApartActive, objectSelector.IsMultipleSelectActive);
            }
        }

        // object position client
        [ClientRpc]
        private void RpcSelectObjectReceive(int id, bool pullApartActive, bool multipleSelect)
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
                objectSelector.SelectObject(id, pullApartActive, multipleSelect);
        }

        #endregion

        #region hide show object

        private void VisibleObjectListener(ObjectInteractionVisibleEvent e)
        {
            CmdHideObjectSend(e.visible);
        }

        // hide object host
        [Command(requiresAuthority = false)]
        private void CmdHideObjectSend(bool visible)
        {
            if (!isClientOnly)
            {
                var selecteds = objectSelector.HidedGameobjects;

                if (visible)
                {
                    hidedObjects.Clear();
                }
                else
                {
                    foreach (var selected in selecteds)
                    {
                        GameObjectReference gor = selected.GetComponent<GameObjectReference>();
                        if (!hidedObjects.Contains(gor.Id))
                            hidedObjects.Add(gor.Id);
                    }
                }

                if (sessionManager.SyncOn) // host
                {
                    if (visible)
                    {
                        RpcUnHideObjectReceive();
                    }
                    else
                    {
                        foreach (var selected in selecteds)
                        {
                            GameObjectReference gor = selected.GetComponent<GameObjectReference>();
                            if (gor != null)
                                RpcHideObjectReceive(gor.Id);
                        }
                    }
                }
            }
        }

        // hide object client
        [ClientRpc]
        private void RpcHideObjectReceive(int id)
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
            {
                objectSelector.HideObject(id);
            }
        }

        // unhide object client
        [ClientRpc]
        private void RpcUnHideObjectReceive()
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
            {
                objectSelector.UnhideObjects();
            }
        }

        #endregion

        #region xray object

        private void XRayListener(ObjectInteractionXRayEvent e)
        {
            CmdXRayObjectSend(e.on);
        }

        // hide object host
        [Command(requiresAuthority = false)]
        private void CmdXRayObjectSend(bool on)
        {
            if (!isClientOnly)
            {
                var selecteds = partObjectManager.XrayGameobjects;

                if (!on)
                {
                    xrayObjects.Clear();
                }
                else
                {
                    foreach (var selected in selecteds)
                    {
                        GameObjectReference gor = selected.GetComponent<GameObjectReference>();
                        if (!xrayObjects.Contains(gor.Id))
                            xrayObjects.Add(gor.Id);
                    }
                }

                if (sessionManager.SyncOn) // host
                {
                    if (on)
                    {
                        foreach (var selected in selecteds)
                        {
                            GameObjectReference gor = selected.GetComponent<GameObjectReference>();
                            if (gor != null)
                                RpcXRayReceive(gor.Id);
                        }
                    }
                    else
                        RpcUnXRayReceive();
                }
            }
        }

        // xray object client
        [ClientRpc]
        private void RpcXRayReceive(int id)
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
            {
                partObjectManager.XrayObject(id);
            }
        }

        // un xray object client
        [ClientRpc]
        private void RpcUnXRayReceive()
        {
            if (!isClientOnly) // client only
                return;

            if (sessionManager.SyncOn)
            {
                partObjectManager.XraySelected(false);
            }
        }

        #endregion
    }
}

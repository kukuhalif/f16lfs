using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class Messaging : NetworkBehaviour
    {
        [SerializeField] PlayerAuthenticator authenticator;
        [SerializeField] VirtualTrainingNetworkManager networkManager;

        public void Send(string message)
        {
            if (networkManager.isNetworkActive)
                CmdSend(authenticator.GetLocalPlayerName(), message);
        }

        [Command(requiresAuthority = false)]
        private void CmdSend(string playerName, string message)
        {
            RpcReceive(playerName, message);
        }

        [ClientRpc]
        private void RpcReceive(string playerName, string message)
        {
            EventManager.TriggerEvent(new MessageReceivedEvent(playerName, message));
        }
    }
}

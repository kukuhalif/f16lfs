using Mirror.Discovery;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class ServerFinder : MonoBehaviour
    {
        [SerializeField] VirtualTrainingNetworkDiscovery networkDiscovery;

        private void Start()
        {
            networkDiscovery.OnServerFound += ServerFound;
        }

        private void OnDestroy()
        {
            networkDiscovery.OnServerFound -= ServerFound;
        }

        private void ServerFound(DiscoveryResponse serverResponse)
        {
            EventManager.TriggerEvent(new NetworkFoundResponseEvent(serverResponse));
        }

        public void StartFindHost(string hostIpAddress)
        {
            networkDiscovery.BroadcastAddress = hostIpAddress;
            networkDiscovery.enableActiveDiscovery = true;
            networkDiscovery.StartDiscovery();
        }

        public void StopFindHost()
        {
            networkDiscovery.enableActiveDiscovery = false;
            networkDiscovery.StopDiscovery();
        }

        public void AdvertiseServer()
        {
            networkDiscovery.AdvertiseServer();
        }
    }
}

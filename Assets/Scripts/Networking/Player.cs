using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using VirtualTraining.Core;

namespace VirtualTraining.Networking
{
    public class Player : NetworkBehaviour
    {
        [SyncVar(hook = nameof(SetGameObjectName))] string playerDataName;

        public string PlayerName { get => playerDataName; }

        private void SetGameObjectName(string oldName, string newName)
        {
            gameObject.name = "Player : " + newName;
        }

        #region server

        public override void OnStartServer()
        {
            base.OnStartServer();

            playerDataName = (string)connectionToClient.authenticationData;
        }

        public override void OnStopServer()
        {
            Destroy(gameObject);
        }

        #endregion

        #region client

        public override void OnStartClient()
        {
            base.OnStartClient();

            EventManager.TriggerEvent(new NewPlayerInstantiatedEvent(playerDataName));

            var networkManager = FindObjectOfType<VirtualTrainingNetworkManager>();
            transform.SetParent(networkManager.transform);

            SetGameObjectName("", playerDataName);
        }

        public override void OnStopClient()
        {
            EventManager.TriggerEvent(new PlayerDestroyedEvent(playerDataName));

            Destroy(gameObject);
        }

        #endregion
    }
}

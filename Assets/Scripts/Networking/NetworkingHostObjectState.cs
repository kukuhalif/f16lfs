using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Networking
{
    public class NetworkingHostObjectState : MonoBehaviour
    {
        int id;

        //Vector3 lastPosition = new Vector3();
        //Vector3 lastRotation = new Vector3();

        Action<int, Vector3> sendPosition;
        Action<int, Vector3> sendRotation;

        public void Setup(int id, Action<int, Vector3> sendPosition, Action<int, Vector3> sendRotation)
        {
            this.id = id;
            this.sendPosition = sendPosition;
            this.sendRotation = sendRotation;
            //lastPosition = transform.position;
            //lastRotation = transform.rotation.eulerAngles;
        }

        private void OnEnable()
        {
            InvokeRepeating("SendUpdate", 0.1f, 0.1f);
        }

        private void OnDisable()
        {
            CancelInvoke("SendUpdate");
        }

        private void SendUpdate()
        //private void Update()
        {
            //if (lastPosition != transform.position)
            //{
            sendPosition(id, transform.position);
            //    lastPosition = transform.position;
            //}

            //if (lastRotation != transform.rotation.eulerAngles)
            //{
            sendRotation(id, transform.rotation.eulerAngles);
            //    lastRotation = transform.rotation.eulerAngles;
            //}
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.UI;

namespace VirtualTraining.Networking.UI
{
    public class ServerButton : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] InteractionButton button;
        [SerializeField] bool displayAddress;

        ClientSetupPanel clientSetupPanel;
        string serverName;
        string serverAddress;
        DiscoveryResponse discoveryResponse;

        public string ServerName { get => serverName; }

        public void Setup(DiscoveryResponse discoveryResponse, ClientSetupPanel clientSetupPanel)
        {
            this.clientSetupPanel = clientSetupPanel;
            this.serverName = discoveryResponse.serverName;
            this.serverAddress = discoveryResponse.uri.AbsoluteUri;
            this.discoveryResponse = discoveryResponse;

            if (displayAddress)
                text.text = serverName + " " + serverAddress;
            else
                text.text = serverName;
        }

        private void Start()
        {
            button.OnClickEvent += OnClick;
        }

        private void OnDestroy()
        {
            button.OnClickEvent -= OnClick;
        }

        private void OnClick()
        {
            clientSetupPanel.ConnectToHost(discoveryResponse);
        }
    }
}

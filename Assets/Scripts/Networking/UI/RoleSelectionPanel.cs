using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using VirtualTraining.SceneManagement;

namespace VirtualTraining.Networking.UI
{
    public class RoleSelectionPanel : UIPanel
    {
        [SerializeField] InteractionButton hostButton;
        [SerializeField] InteractionButton clientButton;
        [SerializeField] HostSetupPanel hostSetupPanel;
        [SerializeField] ClientSetupPanel clientSetupPanel;
        [SerializeField] InteractionButton quitButton;
        [SerializeField] VirtualTrainingNetworkManager networkManager;

        protected override void Start()
        {
            base.Start();

            hostButton.OnClickEvent += Host;
            clientButton.OnClickEvent += Client;
            quitButton.OnClickEvent += Quit;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            hostButton.OnClickEvent -= Host;
            clientButton.OnClickEvent -= Client;
            quitButton.OnClickEvent -= Quit;
        }

        private void Quit()
        {
            if (networkManager.isNetworkActive)
            {
                EventManager.AddListener<OnClientStopEvent>(StopClientListener);
                networkManager.Disconnect();
            }
            else
            {
                VirtualTrainingSceneManager.CloseNetworking();
                EventManager.TriggerEvent(new UnloadNetworkingSceneEvent());
            }
        }

        private void StopClientListener(OnClientStopEvent e)
        {
            EventManager.RemoveListener<OnClientStopEvent>(StopClientListener);
            EventManager.TriggerEvent(new UnloadNetworkingSceneEvent());
            VirtualTrainingSceneManager.CloseNetworking();
        }

        private void Host()
        {
            ClosePanel();
            hostSetupPanel.ShowPanel();
        }

        private void Client()
        {
            ClosePanel();
            clientSetupPanel.ShowPanel();
        }
    }
}

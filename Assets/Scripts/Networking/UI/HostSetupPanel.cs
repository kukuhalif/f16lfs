using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.Networking.UI
{
    public class HostSetupPanel : UIPanel
    {
        [SerializeField] TMP_InputField hostNameInputField;
        [SerializeField] InteractionButton startHostButton;
        [SerializeField] LobbyPanel lobbyPanel;
        [SerializeField] ServerFinder serverFinder;
        [SerializeField] VirtualTrainingNetworkManager networkManager;
        [SerializeField] PlayerAuthenticator playerAuthenticator;
        [SerializeField] InteractionButton backButton;
        [SerializeField] InteractionButton quitButton;
        [SerializeField] RoleSelectionPanel roleSelectionPanel;

        protected override void Start()
        {
            base.Start();

            startHostButton.OnClickEvent += StartHostButtonListener;
            hostNameInputField.onValueChanged.AddListener(HostNameUpdated);
            backButton.OnClickEvent += BackButton_OnClickEvent;
            quitButton.OnClickEvent += BackButton_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            startHostButton.OnClickEvent -= StartHostButtonListener;
            hostNameInputField.onValueChanged.RemoveListener(HostNameUpdated);
            backButton.OnClickEvent -= BackButton_OnClickEvent;
            quitButton.OnClickEvent -= BackButton_OnClickEvent;
        }

        private void StartHostButtonListener()
        {
            if (string.IsNullOrEmpty(playerAuthenticator.GetLocalPlayerName()))
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Name is empty", null, null));
                return;
            }

            EventManager.AddListener<HostStartedEvent>(HostStartedListener);

            //networkManager.StartRelayHost(5);
            networkManager.StartHost();
            serverFinder.AdvertiseServer();
        }

        private void HostStartedListener(HostStartedEvent e)
        {
            EventManager.RemoveListener<HostStartedEvent>(HostStartedListener);

            if (e.success)
            {
                ClosePanel();
                lobbyPanel.ShowPanel();
            }
            else
                EventManager.TriggerEvent(new ConfirmationPanelEvent("host creation failed", null, null));
        }

        private void HostNameUpdated(string newName)
        {
            playerAuthenticator.SetLocalPlayerName(newName);
        }

        private void BackButton_OnClickEvent()
        {
            ClosePanel();
            roleSelectionPanel.ShowPanel();
        }
    }
}

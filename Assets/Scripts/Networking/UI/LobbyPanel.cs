using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;
using System.Net;

namespace VirtualTraining.Networking.UI
{
    public class LobbyPanel : UIPanel
    {
        [SerializeField] Transform playerParent;
        [SerializeField] GameObject playerUiTemplate;
        [SerializeField] InteractionButton disconnectButton;
        [SerializeField] InteractionButton startSessionButton;
        [SerializeField] RoleSelectionPanel roleSelectionPanel;

        [SerializeField] TextMeshProUGUI chatHistoryText;
        [SerializeField] TMP_InputField chatInputField;
        [SerializeField] InteractionButton sendChatButton;
        [SerializeField] ScrollRect chatScrollrect;

        [SerializeField] VirtualTrainingNetworkManager networkManager;
        [SerializeField] Messaging messaging;
        [SerializeField] PlayerAuthenticator playerAuthenticator;
        [SerializeField] SessionManager sessionManager;

        [SerializeField] TextMeshProUGUI relayJoinCodeText;
        [SerializeField] TextMeshProUGUI hostIpAddresses;

        VerticalLayoutGroup verticalLayout;

        protected override void Start()
        {
            base.Start();

            verticalLayout = chatHistoryText.GetComponentInParent<VerticalLayoutGroup>();

            EventManager.AddListener<NewPlayerInstantiatedEvent>(NewPlayerListener);
            EventManager.AddListener<PlayerDestroyedEvent>(OnPlayerDestroyedListener);
            EventManager.AddListener<OnClientStopEvent>(OnClientStoppedListener);
            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);

            disconnectButton.OnClickEvent += Disconnect;

            sendChatButton.OnClickEvent += SendMessage;
            startSessionButton.OnClickEvent += StartSession;
            chatInputField.onEndEdit.AddListener(EndEditListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<NewPlayerInstantiatedEvent>(NewPlayerListener);
            EventManager.RemoveListener<PlayerDestroyedEvent>(OnPlayerDestroyedListener);
            EventManager.RemoveListener<OnClientStopEvent>(OnClientStoppedListener);
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);

            disconnectButton.OnClickEvent -= Disconnect;

            sendChatButton.OnClickEvent -= SendMessage;
            startSessionButton.OnClickEvent -= StartSession;
            chatInputField.onEndEdit.RemoveListener(EndEditListener);
        }

        private void Disconnect()
        {
            networkManager.Disconnect();
            ClosePanel();

            for (int i = 0; i < playerParent.childCount; i++)
            {
                Destroy(playerParent.GetChild(i).gameObject);
            }

            roleSelectionPanel.ShowPanel();
        }

        private void OnClientStoppedListener(OnClientStopEvent e)
        {
            if (!IsPanelActive)
                return;

            Debug.Log("server stopped");

            ClosePanel();
            roleSelectionPanel.ShowPanel();
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            chatHistoryText.text = "";
            startSessionButton.gameObject.SetActive(networkManager.HostStarted);

            EventManager.AddListener<MessageReceivedEvent>(MessageReceivedListener);

            // show host ip address
            if (!sessionManager.isClientOnly)
            {
                // Retrive the Name of HOST
                string hostName = Dns.GetHostName();
                // Get the IP

                hostIpAddresses.text = "Host Ip Address";

                foreach (var ip in Dns.GetHostEntry(hostName).AddressList)
                {
                    string myIP = ip.MapToIPv4().ToString();
                    // set broadcast ip for host
                    Debug.Log("my ip address " + myIP);
                    hostIpAddresses.text = hostIpAddresses.text + " | " + myIP;
                }
            }
            else
                hostIpAddresses.text = "";

            // set relay join code to ui if this is host
            //if (sessionManager.isClientOnly)
            //    relayJoinCodeText.gameObject.SetActive(false);
            //else
            //{
            //    relayJoinCodeText.gameObject.SetActive(true);
            //    relayJoinCodeText.text = networkManager.relayJoinCode;
            //}
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            chatHistoryText.text = "";

            EventManager.RemoveListener<MessageReceivedEvent>(MessageReceivedListener);
        }

        private void NewPlayerListener(NewPlayerInstantiatedEvent e)
        {
            var newPlayerUI = Instantiate(playerUiTemplate);

            newPlayerUI.transform.SetParent(playerParent);
            newPlayerUI.transform.transform.localScale = Vector3.one;

            var text = newPlayerUI.GetComponentInChildren<TextMeshProUGUI>();
            text.text = e.playerName;
        }

        private void OnPlayerDestroyedListener(PlayerDestroyedEvent e)
        {
            for (int i = 0; i < playerParent.childCount; i++)
            {
                var text = playerParent.GetChild(i).GetComponentInChildren<TextMeshProUGUI>();
                if (text.text == e.playerName)
                {
                    Destroy(playerParent.GetChild(i).gameObject);

                    break;
                }
            }
        }

        private void EndEditListener(string message)
        {
            SendMessage();
        }

        private void SendMessage()
        {
            if (string.IsNullOrEmpty(chatInputField.text))
                return;

            messaging.Send(chatInputField.text);
            chatInputField.text = "";
            chatInputField.ActivateInputField();
        }

        private void MessageReceivedListener(MessageReceivedEvent e)
        {
            string prettyMessage = "";

            if (playerAuthenticator.GetLocalPlayerName() == e.sender)
                prettyMessage = $"<color=red>{e.sender}:</color> {e.message}";
            else
                prettyMessage = $"<color=yellow>{e.sender}:</color> {e.message}";

            AppendMessage(prettyMessage);
        }

        private void AppendMessage(string message)
        {
            StartCoroutine(AppendAndScroll(message));
        }

        IEnumerator AppendAndScroll(string message)
        {
            chatHistoryText.text += message + "\n";

            verticalLayout.CalculateLayoutInputVertical();
            yield return null;
            chatHistoryText.SetLayoutDirty();
            yield return null;

            while (chatScrollrect.verticalNormalizedPosition > 0)
            {
                chatScrollrect.verticalNormalizedPosition -= Time.deltaTime * 10f;
                yield return null;
            }

            chatScrollrect.verticalNormalizedPosition = 0;
        }

        private void StartSession()
        {
            sessionManager.StartSession();
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            ClosePanel();
            EventManager.TriggerEvent(new CloseAllUIPanelEvent());
            EventManager.TriggerEvent(new FreePlayEvent());
        }
    }
}

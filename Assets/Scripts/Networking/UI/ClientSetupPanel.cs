using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.Networking.UI
{
    public class ClientSetupPanel : UIPanel
    {
        [SerializeField] TextMeshProUGUI label;
        [SerializeField] TMP_InputField clientNameInputField;
        [SerializeField] LobbyPanel lobbyPanel;
        [SerializeField] Transform serverButtonParent;
        [SerializeField] ServerButton serverButtonTemplate;
        [SerializeField] InteractionButton clearServerButton;
        [SerializeField] VirtualTrainingNetworkManager networkManager;
        [SerializeField] PlayerAuthenticator playerAuthenticator;
        [SerializeField] ServerFinder serverFinder;
        [SerializeField] InteractionButton backButton;
        [SerializeField] InteractionButton quitButton;
        [SerializeField] RoleSelectionPanel roleSelectionPanel;
        [SerializeField] TMP_InputField relayJoinCodeInputField;
        [SerializeField] TMP_InputField hostIpAddressInputField;

        protected override void Start()
        {
            base.Start();

            clearServerButton.OnClickEvent += RefreshHostList;
            clientNameInputField.onValueChanged.AddListener(ClientNameUpdated);
            backButton.OnClickEvent += BackButton_OnClickEvent;
            quitButton.OnClickEvent += BackButton_OnClickEvent;

            EventManager.AddListener<NetworkFoundResponseEvent>(NetworkDiscoveredListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            clearServerButton.OnClickEvent -= RefreshHostList;
            clientNameInputField.onValueChanged.RemoveListener(ClientNameUpdated);
            backButton.OnClickEvent -= BackButton_OnClickEvent;
            quitButton.OnClickEvent -= BackButton_OnClickEvent;

            EventManager.RemoveListener<NetworkFoundResponseEvent>(NetworkDiscoveredListener);
        }

        private void BackButton_OnClickEvent()
        {
            ClosePanel();
            roleSelectionPanel.ShowPanel();
        }

        protected override void OnEnablePanel(bool confinePanel)
        {
            base.OnEnablePanel(confinePanel);

            ClearServers();

            hostIpAddressInputField.text = "0.0.0.0";
            serverFinder.StartFindHost(hostIpAddressInputField.text);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            serverFinder.StopFindHost();
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            label.color = theme.genericTextColor;
        }

        private void ClientStartedListener(ClientStartedEvent e)
        {
            EventManager.RemoveListener<ClientStartedEvent>(ClientStartedListener);

            if (e.success)
            {
                ClosePanel();
                lobbyPanel.ShowPanel();
            }
            else
                EventManager.TriggerEvent(new ConfirmationPanelEvent(e.message, null, null));
        }

        private void ClientNameUpdated(string newName)
        {
            playerAuthenticator.SetLocalPlayerName(newName);
        }

        public void ConnectToHost(DiscoveryResponse discoveryResponse)
        {
            if (string.IsNullOrEmpty(playerAuthenticator.GetLocalPlayerName()))
            {
                EventManager.TriggerEvent(new ConfirmationPanelEvent("Name is empty", null, null));
                return;
            }

            EventManager.AddListener<ClientStartedEvent>(ClientStartedListener);

            //networkManager.relayJoinCode = relayJoinCodeInputField.text;
            //networkManager.JoinRelayServer();
            networkManager.StartClient(discoveryResponse.uri);
        }

        private void RefreshHostList()
        {
            ClearServers();
            serverFinder.StartFindHost(hostIpAddressInputField.text);
        }

        private void ClearServers()
        {
            List<GameObject> toDestroy = new List<GameObject>();
            for (int i = 0; i < serverButtonParent.childCount; i++)
            {
                toDestroy.Add(serverButtonParent.GetChild(i).gameObject);
            }

            foreach (var item in toDestroy)
            {
                Destroy(item);
            }
        }

        private void NetworkDiscoveredListener(NetworkFoundResponseEvent e)
        {
            if (e.discoveryResponse.sessionStarted)
            {
                Debug.Log(e.discoveryResponse.serverName + " session is already started");
                return;
            }

            for (int i = 0; i < serverButtonParent.childCount; i++)
            {
                var serverButton = serverButtonParent.GetChild(i).GetComponent<ServerButton>();

                if (serverButton.ServerName == e.discoveryResponse.serverName)
                    return;
            }

            var newButton = Instantiate(serverButtonTemplate);
            newButton.Setup(e.discoveryResponse, this);
            newButton.transform.SetParent(serverButtonParent);
            newButton.transform.localScale = Vector3.one;
        }
    }
}

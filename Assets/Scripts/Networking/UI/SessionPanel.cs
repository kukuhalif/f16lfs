using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;
using VirtualTraining.UI;
using VirtualTraining.UI.Desktop;

namespace VirtualTraining.Networking.UI
{
    public class SessionPanel : UIPanel
    {
        [SerializeField] SessionManager sessionManager;
        [SerializeField] VirtualTrainingNetworkManager networkManager;
        [SerializeField] InteractionButton stopSessionButton;
        [SerializeField] InteractionButton disconnectButton;

        [SerializeField] Messaging messaging;
        [SerializeField] PlayerAuthenticator playerAuthenticator;

        [SerializeField] TextMeshProUGUI chatHistoryText;
        [SerializeField] TMP_InputField chatInputField;
        [SerializeField] InteractionButton sendChatButton;
        [SerializeField] ScrollRect chatScrollrect;

        [SerializeField] Transform playerParent;
        [SerializeField] GameObject playerUiTemplate;

        VerticalLayoutGroup verticalLayout;
        bool playerListGenerated = false;

        [SerializeField] InteractionToggle chatToggle;
        [SerializeField] GameObject chatContainer;
        [SerializeField] GameObject playerContainer;

        [SerializeField] InteractionButton floatingNetworkButton;
        [SerializeField] GameObject sessionContent;

        [SerializeField] InteractionToggle syncToggle;

        protected override void Start()
        {
            base.Start();

            verticalLayout = chatHistoryText.GetComponentInParent<VerticalLayoutGroup>();

            EventManager.AddListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.AddListener<OnClientStopEvent>(StopClientListener);
            EventManager.AddListener<PlayerDestroyedEvent>(PlayerDestroyedListener);
            EventManager.AddListener<HostSyncStateChangedEvent>(HostSyncStateChangedListener);

            stopSessionButton.OnClickEvent += Disconnect;
            disconnectButton.OnClickEvent += Disconnect;

            sendChatButton.OnClickEvent += SendMessage;
            chatInputField.onEndEdit.AddListener(EndEditListener);

            chatToggle.OnStateChangedEvent += ChatToggle_OnStateChangedEvent;

            floatingNetworkButton.OnClickEvent += FloatingNetworkButton_OnClickEvent;

            syncToggle.OnStateChangedEvent += SyncToggle_OnStateChangedEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<SessionStartedEvent>(SessionStartedListener);
            EventManager.RemoveListener<OnClientStopEvent>(StopClientListener);
            EventManager.RemoveListener<PlayerDestroyedEvent>(PlayerDestroyedListener);
            EventManager.RemoveListener<HostSyncStateChangedEvent>(HostSyncStateChangedListener);

            stopSessionButton.OnClickEvent -= Disconnect;
            disconnectButton.OnClickEvent -= Disconnect;

            sendChatButton.OnClickEvent -= SendMessage;
            chatInputField.onEndEdit.RemoveListener(EndEditListener);

            chatToggle.OnStateChangedEvent -= ChatToggle_OnStateChangedEvent;

            floatingNetworkButton.OnClickEvent -= FloatingNetworkButton_OnClickEvent;

            syncToggle.OnStateChangedEvent += SyncToggle_OnStateChangedEvent;
        }

        private void HostSyncStateChangedListener(HostSyncStateChangedEvent e)
        {
            if (e.fromNetwork)
            {
                syncToggle.IsOn = e.syncOn;
            }
        }

        private void SyncToggle_OnStateChangedEvent(bool on)
        {
            sessionManager.SetSync(on);
        }

        private void SessionStartedListener(SessionStartedEvent e)
        {
            ShowPanel(() =>
            {
                sessionContent.SetActive(false);
                Setup();
            });

            EventManager.AddListener<MessageReceivedEvent>(MessageReceivedListener);
        }

        private void Setup()
        {
            if (!playerListGenerated)
            {
                var players = FindObjectsOfType<Player>();
                foreach (var player in players)
                {
                    var newPlayerUI = Instantiate(playerUiTemplate);

                    newPlayerUI.transform.SetParent(playerParent);
                    newPlayerUI.transform.transform.localScale = Vector3.one;

                    var text = newPlayerUI.GetComponentInChildren<TextMeshProUGUI>();
                    text.text = player.PlayerName;
                }

                playerListGenerated = true;

                Debug.Log("player list instantiated");
            }

            StartCoroutine(ScrollUp());

            if (networkManager.HostStarted)
            {
                stopSessionButton.gameObject.SetActive(true);
                disconnectButton.gameObject.SetActive(false);
            }
            else
            {
                stopSessionButton.gameObject.SetActive(false);
                disconnectButton.gameObject.SetActive(true);
            }
        }

        private void PlayerDestroyedListener(PlayerDestroyedEvent e)
        {
            for (int i = 0; i < playerParent.childCount; i++)
            {
                var text = playerParent.GetChild(i).GetComponentInChildren<TextMeshProUGUI>();
                if (text.text == e.playerName)
                {
                    Destroy(playerParent.GetChild(i).gameObject);

                    break;
                }
            }
        }

        private void Disconnect()
        {
            networkManager.Disconnect();
        }

        private void StopClientListener(OnClientStopEvent e)
        {
            // close networking kalo udah di start ajah
            if (!sessionManager.SessionStarted)
                return;

            sessionManager.StopSession();
            EventManager.RemoveListener<OnClientStopEvent>(StopClientListener);
            EventManager.RemoveListener<MessageReceivedEvent>(MessageReceivedListener);
            VirtualTrainingSceneManager.CloseNetworking();
            EventManager.TriggerEvent(new UnloadNetworkingSceneEvent());
        }

        private void EndEditListener(string message)
        {
            SendMessage();
        }

        private void SendMessage()
        {
            if (string.IsNullOrEmpty(chatInputField.text))
                return;

            messaging.Send(chatInputField.text);
            chatInputField.text = "";
            chatInputField.ActivateInputField();
        }

        private void MessageReceivedListener(MessageReceivedEvent e)
        {
            string prettyMessage = "";

            if (playerAuthenticator.GetLocalPlayerName() == e.sender)
                prettyMessage = $"<color=red>{e.sender}:</color> {e.message}";
            else
                prettyMessage = $"<color=yellow>{e.sender}:</color> {e.message}";

            AppendMessage(prettyMessage);
        }

        private void AppendMessage(string message)
        {
            if (IsPanelActive)
                StartCoroutine(AppendAndScroll(message));
            else
                chatHistoryText.text += message + "\n";
        }

        IEnumerator AppendAndScroll(string message)
        {
            chatHistoryText.text += message + "\n";

            verticalLayout.CalculateLayoutInputVertical();
            yield return null;
            chatHistoryText.SetLayoutDirty();
            yield return null;

            while (chatScrollrect.verticalNormalizedPosition > 0)
            {
                chatScrollrect.verticalNormalizedPosition -= Time.deltaTime * 10f;
                yield return null;
            }

            chatScrollrect.verticalNormalizedPosition = 0;
        }

        IEnumerator ScrollUp()
        {
            while (chatScrollrect.verticalNormalizedPosition > 0)
            {
                chatScrollrect.verticalNormalizedPosition -= Time.deltaTime * 10f;
                yield return null;
            }

            chatScrollrect.verticalNormalizedPosition = 0;
        }

        private void ChatToggle_OnStateChangedEvent(bool on)
        {
            chatContainer.gameObject.SetActive(on);
            playerContainer.gameObject.SetActive(!on);
        }

        private void FloatingNetworkButton_OnClickEvent()
        {
            sessionContent.SetActive(!sessionContent.gameObject.activeSelf);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.SceneManagement.VR
{
    public class VRSceneModeManager : FeatureInitializer
    {
        private void Start()
        {
            Initialize(InitDoneCallback);
        }

        private void InitDoneCallback()
        {
            SceneReady();
           
        }
    }
}

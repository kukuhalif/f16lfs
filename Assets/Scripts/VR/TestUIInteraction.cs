﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

public class TestUIInteraction : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void ClickButton()
    {
        Debug.Log("Click Button");
        VirtualTrainingSceneManager.SwitchSceneMode(SceneMode.Desktop);
    }
}

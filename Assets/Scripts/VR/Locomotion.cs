using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System;
using VirtualTraining.UI;
using Valve.VR;

namespace VirtualTraining.VR
{
    public class Locomotion : MonoBehaviour
    {
        private float jumpHeight = 1.0f;
        private float gravityValue = -9.81f;
        [SerializeField] SteamVR_Action_Boolean jump;
        [SerializeField] bool isJumping;

        bool isLeft;
        bool isRight;

        [SerializeField] float rotationSpeed;
        [SerializeField] InteractionButton buttonRight;
        [SerializeField] InteractionButton buttonLeft;
        [SerializeField] SteamVR_Action_Vector2 rotateTrigger;
        [SerializeField] SteamVR_Action_Vector2 walkingJoystick;
        [SerializeField] SteamVR_Action_Boolean rotateOn;

        [SerializeField] CharacterController characterController;

        [SerializeField] float playerSpeed;
        [SerializeField] PlayerControllerVR playerControllerVR;

        Vector3 postCont;

        private Vector3 playerVelocity;

        private void Start()
        {
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            rotationSpeed = DatabaseManager.GetDefaultSetting().speedRotationVR * 10;
            buttonRight.OnPointerDownEvent += RotateRightDown;
            buttonRight.OnPointerUpEvent += RotateRightUp;
            buttonLeft.OnPointerDownEvent += RotateLeftDown;
            buttonLeft.OnPointerUpEvent += RotateLeftUp;
            playerControllerVR = GetComponent<PlayerControllerVR>();
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);

            buttonRight.OnPointerDownEvent -= RotateRightDown;
            buttonRight.OnPointerUpEvent -= RotateRightUp;
            buttonLeft.OnPointerDownEvent -= RotateLeftDown;
            buttonLeft.OnPointerUpEvent -= RotateLeftUp;
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            rotationSpeed = e.data.speedRotationVR * 10;
        }
        private void Update()
        {
            if (VirtualTrainingCamera.IsPulling)
                return;

            if (playerControllerVR.isDragUI == false)
            {
                if (rotateOn.state)
                {
                    //Debug.Log("rotate");
                    if (isLeft || Input.GetKey(KeyCode.E) || walkingJoystick.axis.x < -0.5)
                        //transform.Rotate(Vector3.down * Time.deltaTime * rotationSpeed, Space.Self);
                        transform.Rotate(0, -Time.deltaTime * rotationSpeed, 0);
                    else if (isRight || Input.GetKey(KeyCode.R) || walkingJoystick.axis.x > 0.5)
                        //transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed, Space.Self);
                        transform.Rotate(0, Time.deltaTime * rotationSpeed, 0);
                }
            }

            if (!rotateOn.state)
            {
                //Debug.Log("Move");
                //MovePlayer();
                MovePlayer2();
            }
        }

        void MovePlayer()
        {
            if (walkingJoystick.axis.y > 0.5f)
            {
                Vector3 forward = VirtualTrainingCamera.CurrentCamera.transform.forward;
                Vector3 forwardDir = new Vector3(forward.x, 0, forward.z).normalized;
                transform.position += (forwardDir * playerSpeed) * Time.deltaTime;
            }
            else if (walkingJoystick.axis.y < -0.5f)
            {
                Vector3 forward = VirtualTrainingCamera.CurrentCamera.transform.forward;
                Vector3 forwardDir = new Vector3(forward.x, 0, forward.z).normalized;
                transform.position += (forwardDir * -playerSpeed) * Time.deltaTime;
            }

            if (walkingJoystick.axis.x > 0.5f)
            {
                Vector3 right = VirtualTrainingCamera.CurrentCamera.transform.right;
                Vector3 rightDir = new Vector3(right.x, 0, right.z).normalized;
                transform.position += (rightDir * playerSpeed) * Time.deltaTime;
            }
            else if (walkingJoystick.axis.x < -0.5f)
            {
                Vector3 right = VirtualTrainingCamera.CurrentCamera.transform.right;
                Vector3 rightDir = new Vector3(right.x, 0, right.z).normalized;
                transform.position += (rightDir * -playerSpeed) * Time.deltaTime;
            }
        }

        void MovePlayer2()
        {
            // Get Horizontal and Vertical Input
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            if (walkingJoystick.axis.y > 0.5f)
                verticalInput = 1;
            else if (walkingJoystick.axis.y < -0.5f)
                verticalInput = -1;
            else
                verticalInput = 0;

            if (walkingJoystick.axis.x > 0.5f)
                horizontalInput = 1;
            else if (walkingJoystick.axis.x < -0.5f)
                horizontalInput = -1;
            else
                horizontalInput = 0;


            // Calculate the Direction to Move based on the tranform of the Player
            Vector3 moveDirectionForward = transform.forward * verticalInput;
            Vector3 moveDirectionSide = transform.right * horizontalInput;

            //find the direction
            Vector3 direction = (moveDirectionForward + moveDirectionSide).normalized;

            //find the distance
            Vector3 distance = direction * playerSpeed * Time.deltaTime;

            // Apply Movement to Player
            characterController.Move(distance);

            //Debug.Log(characterController.isGrounded);

           // if (jump.state)// !isJumping
           // {
                //Debug.Log("jump input");
                //if (characterController.isGrounded)
                //{
                   // isJumping = true;
                   // Debug.Log("gogogo");
                   // playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
                //}
          //  }

            playerVelocity.y += gravityValue * Time.deltaTime;
            characterController.Move(playerVelocity * Time.deltaTime);

            //if (characterController.isGrounded)
            //    isJumping = false;
        }

        public void RotateRightDown()
        {
            if (isRight == false)
                isRight = true;
        }

        public void RotateRightUp()
        {
            if (isRight == true)
                isRight = false;
        }

        public void RotateLeftDown()
        {
            if (isLeft == false)
                isLeft = true;
        }

        public void RotateLeftUp()
        {
            if (isLeft == true)
                isLeft = false;
        }
    }
}

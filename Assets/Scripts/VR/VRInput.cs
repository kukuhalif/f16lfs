using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using VirtualTraining.Core;

public class VRInput : MonoBehaviour
{
    [SerializeField] SteamVR_Action_Boolean trigger;
    [SerializeField] SteamVR_Action_Boolean grab;
    [SerializeField] float grabRadius = 0.1f;

    VirtualTrainingInputSystem inputSystem;

    private void Start()
    {
        inputSystem = GameObject.FindObjectOfType<VirtualTrainingInputSystem>();
        trigger.onStateUp += Trigger_onStateUp;
        trigger.onStateDown += Trigger_onStateDown;
        grab.onStateUp += Grab_onStateUp;
        grab.onStateDown += Grab_onStateDown;
    }

    private void OnDestroy()
    {
        trigger.onStateUp -= Trigger_onStateUp;
        trigger.onStateDown -= Trigger_onStateDown;
    }

    private void Trigger_onStateDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        inputSystem.LeftClickDown();
    }

    private void Trigger_onStateUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        inputSystem.LeftClickUp();
    }

    private void Grab_onStateDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        Collider[] colliders = Physics.OverlapSphere(VirtualTrainingCamera.handVR.position, grabRadius);

        foreach (var colli in colliders)
        {
            EventManager.TriggerEvent(new RightHandGripEvent(colli.gameObject, VirtualTrainingCamera.handVR));
        }
    }

    private void Grab_onStateUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        EventManager.TriggerEvent(new RightHandGripEvent());
    }
}

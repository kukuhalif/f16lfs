using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using VirtualTraining.Feature;
using VirtualTraining.Feature.VR;
using UnityEngine.XR.Management;
using Valve.VR.Extras;
using VirtualTraining.Core;
using System;
using ViveHandTracking;
namespace VirtualTraining.VR
{
    public class HandVR : MonoBehaviour
    {
        [SerializeField] PlayerControllerVR playerControllerVR;
        [SerializeField] GameObject rightHandModel;
        [SerializeField] GameObject eulerChildHelper;
        GameObject gameObjects;
        Vector3 deltaPosition;
        Vector3 deltaEuler;
        bool isMultipleSelectActive;
        bool isPullApartActive;

        [SerializeField] VRObjectSelector vrObjectSelector;
        [SerializeField] SteamVR_Action_Boolean gripController;

        private void Start()
        {
            rightHandModel = this.gameObject;
            EventManager.AddListener<ObjectInteractionMultipleSelectionEvent>(MultipleSelectionListener);
            EventManager.AddListener<ObjectInteractionPullApartEvent>(PullApartListener);
        }
        private void OnDestroy()
        {
            EventManager.RemoveListener<ObjectInteractionMultipleSelectionEvent>(MultipleSelectionListener);
            EventManager.RemoveListener<ObjectInteractionPullApartEvent>(PullApartListener);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(true));
                EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(false));
            }
        }

        private void MultipleSelectionListener(ObjectInteractionMultipleSelectionEvent e)
        {
            isMultipleSelectActive = e.on;
        }
        private void PullApartListener(ObjectInteractionPullApartEvent e)
        {
            isPullApartActive = e.on;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (vrObjectSelector != null && isMultipleSelectActive == false && isPullApartActive)
            {
                vrObjectSelector.ClearSelection();

                GameObjectReference gor = other.gameObject.GetComponent<GameObjectReference>();
                if (gor != null)
                {
                    vrObjectSelector.isTouch = true;

                    if (vrObjectSelector.handTouchObject == null)
                        vrObjectSelector.handTouchObject = rightHandModel;

                    vrObjectSelector.SelectObject(gor);

                    playerControllerVR.laserVR.laserStatus = false;
                    gameObjects = gor.gameObject;

                    eulerChildHelper.transform.position = other.gameObject.transform.position;
                    eulerChildHelper.transform.eulerAngles = other.gameObject.transform.eulerAngles;
                    Debug.Log("trigger");
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject == gameObjects)
            {
                if (gripController.state)
                {
                    DregObject(1);
                }
                else
                {
                    playerControllerVR.laserVR.OnStateChanged(1);
                    DregObject(0);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject == gameObjects && isMultipleSelectActive == false && vrObjectSelector.isPress == false)
            {
                if (vrObjectSelector != null)
                {
                    vrObjectSelector.ClearSelection();
                    vrObjectSelector.isTouch = false;
                }
                if (playerControllerVR.isHandTracking == false)
                {
                    playerControllerVR.laserVR.OnStateChanged(1);
                }
            }

            playerControllerVR.laserVR.ResetTeleport(true);
            playerControllerVR.laserVR.laserStatus = true;
        }

        public void DregObject(int state)
        {
            if (state == 1)
            {
                if (gameObjects != null)
                {
                    playerControllerVR.laserVR.laserStatus = false;
                    deltaPosition = gameObjects.transform.position - rightHandModel.transform.position;
                    //eulerChildHelper.transform.eulerAngles = gameObjects.transform.eulerAngles;

                    if (vrObjectSelector != null)
                    {
                        vrObjectSelector.helperObject = eulerChildHelper;
                        vrObjectSelector.isPress = true;
                    }
                }
            }
            else
            {
                //vrObjectSelector.helperObject = null;
                vrObjectSelector.isPress = false;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.UI;
using UnityEngine.UI;
using Valve.VR;
using VirtualTraining.Core;

namespace VirtualTraining.VR
{
    public class VRPlayerManager : MonoBehaviour
    {
        public bool IsHandTracking;
        public GameObject playerController;
        public GameObject playerHandTracking;

        public GameObject laserControllerObject;


        private void Awake()
        {
            SteamVR.Initialize(true);
            playerHandTracking.gameObject.SetActive(IsHandTracking);
            playerController.gameObject.SetActive(!IsHandTracking);

            if (IsHandTracking)
            {
                PlayerControllerVR player = playerHandTracking.GetComponent<PlayerControllerVR>();
                player.SetupPlayerControllerVR();
                IsHandTracking = true;
            }
            else
            {
                PlayerControllerVR player = playerController.GetComponent<PlayerControllerVR>();
                player.SetupPlayerControllerVR();
            }
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(2);
            if (IsHandTracking == false)
            {
                InteractionButtonConfirmation.SetConfirmationDuration(0);
                Debug.Log("set confirmation time");
            }
        }
    }
}

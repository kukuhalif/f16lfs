using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public class DummyScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        EventManager.TriggerEvent(new CameraControllerReadyEvent());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System;
using VirtualTraining.UI;
using Valve.VR;
using UnityEngine.UI;
using VirtualTraining.SceneManagement;
using VirtualTraining.UI.VR;

namespace VirtualTraining.VR
{
    public class PlayerControllerVR : CameraController
    {
        public Camera vrCamera;
        public GameObject handVR;
        public Collider collider;
        public LaserVR laserVR;
        public bool isHandTracking;
        public GameObject featureContainer;
        public GameObject systemPanelContainer;
        public GameObject mainMenuContainer;

        [SerializeField] LineRenderer lineRendererLaser;
        [SerializeField] GameObject laserStartObject;
        [SerializeField] Image imageTransition;

        public LineRenderer lineRenderer;
        [SerializeField] TeleportPlayer teleportPlayer;
        [SerializeField] UIControllerVR uiControllerVR;
        [SerializeField] MainMenuVR mainMenuVR;

        [SerializeField] bool isVRHead;
        public bool isDragUI;

        GameObject cameraOffset;

        public override bool IsMoving()
        {
            return false;
        }

        void Start()
        {
            //EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(true));
            if (isVRHead)
            {
                Debug.Log("setupp vr");
                VirtualTrainingCamera.SetCurrentCameraController(this, true, null, handVR);
                EventManager.TriggerEvent(new CameraControllerReadyEvent());
            }
            else
            {
                transform.position = DatabaseManager.GetCameraData().vrCamera.position;
            }

            //cameraOffset = new GameObject("camera offset");
            //cameraOffset.transform.SetParent(transform.GetChild(0), true);
            //cameraOffset.transform.localPosition = new Vector3();
            //vrCamera.transform.SetParent(cameraOffset.transform, true);
        }

        //IEnumerator SetOffset()
        //{
        //    yield return new WaitForSeconds(1);

        //    Vector3 camPos = vrCamera.transform.localPosition;

        //    yield return null;

        //    GameObject cameraOffset = new GameObject("camera offset");
        //    cameraOffset.transform.SetParent(transform.GetChild(0), true);
        //    cameraOffset.transform.localPosition = new Vector3();
        //    vrCamera.transform.SetParent(cameraOffset.transform, true);

        //    yield return null;

        //    cameraOffset.transform.localPosition = camPos * -1;
        //}

        //private void Update()
        //{
        //    Vector3 camPos = vrCamera.transform.localPosition;
        //    cameraOffset.transform.localPosition = camPos;
        //}

        public override void ForceSetPositionAndRotation(Vector3 position, Vector3 rotation)
        {
            //transform.position = position;
            //transform.rotation = Quaternion.Euler(rotation);
            this.transform.position = new Vector3(position.x,
              position.y - 1, position.z);
            this.transform.eulerAngles = new Vector3(0, rotation.y, 0);
        }

        public override void SetCameraDataFromServer(Vector3 position, Vector3 rotation, float fov, float orthographicSize)
        {

        }

        public override void AudioListenerSetup()
        {
            AudioListener audioListener = GetComponent<AudioListener>();
            if (audioListener == null)
                gameObject.AddComponent<AudioListener>();
        }

        public void SetupPlayerControllerVR()
        {
            uiControllerVR = FindObjectOfType<UIControllerVR>();
            VirtualTrainingCamera.SetCurrentCameraController(this, true, collider, handVR);
            laserVR.LaserVRSetup(handVR, lineRenderer, laserStartObject);
            teleportPlayer.SetTeleportPlayer(this.gameObject, imageTransition);
            if (uiControllerVR != null)
            {
                uiControllerVR.SetAllPanel(featureContainer, systemPanelContainer, mainMenuContainer);
                //mainMenuVR.SetupAllPanel(uiControllerVR.systemPanelVR, uiControllerVR.descriptionPanelVR, uiControllerVR.helpPanelVR,
                //uiControllerVR.settingPanelVR, uiControllerVR.confirmationPanelVR);
            }
        }

        public override Camera GetCamera()
        {
            return vrCamera;
        }

        public override void ResetCamera(Action arriveAction)
        {
            teleportPlayer.TeleportPlayerVR(DatabaseManager.GetCameraData().vrCamera.position, DatabaseManager.GetCameraData().vrCamera.rotation.eulerAngles);
        }

        public override void MoveCamera(CameraDestination cameraDestination, Action arriveAction)
        {
            teleportPlayer.TeleportPlayerVR(cameraDestination.position, cameraDestination.rotation.eulerAngles, arriveAction);
        }

        public override void MoveCamera(Vector3 position, Quaternion rotation, Transform cameraTarget, bool isOrthographic, bool isRotateFromView, float fov, float orthographicSize, Action arriveAction)
        {
            teleportPlayer.TeleportPlayerVR(position, rotation.eulerAngles, arriveAction);
        }
    }
}

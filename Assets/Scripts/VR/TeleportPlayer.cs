using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using System;
using VirtualTraining.UI;

namespace VirtualTraining.VR
{
    public class TeleportPlayer : MonoBehaviour
    {
        [SerializeField] Image imageTransition;
        [SerializeField] GameObject player;
        [SerializeField] GameObject systemUIBlocker;

        float fadeAmount;
        [SerializeField] float fadeSpeed = 5;
        bool isTeleporting;

        private void Start()
        {
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.AddListener<ConfirmationPanelEvent>(OpenConfirmationPanel);
            EventManager.AddListener<CloseConfirmationPanelEvent>(CloseConfirmationPanel);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<ConfirmationPanelEvent>(OpenConfirmationPanel);
            EventManager.RemoveListener<CloseConfirmationPanelEvent>(CloseConfirmationPanel);
        }

        public void SetTeleportPlayer(GameObject player, Image imageTransition)
        {
            this.player = player;
            this.imageTransition = imageTransition;
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            fadeSpeed = e.data.cameraTransitionSpeed * 2;
        }

        public void TeleportPlayerVR(Vector3 Position, Vector3 Rotation, Action arriveFunc = null)
        {
            fadeAmount = 0;
            if (isTeleporting == true)
                return;

            if (player.transform.position == Position)
                StartCoroutine(PositionisSame(arriveFunc));
            else
                StartCoroutine(DelayTeleportPlayer(Position, Rotation, arriveFunc));
        }

        IEnumerator PositionisSame(Action arriveFunc = null)
        {
            if (systemUIBlocker != null)
                systemUIBlocker.gameObject.SetActive(true);
            isTeleporting = true;
            while (fadeAmount < 1)
            {
                //fadeAmount = fadeAmount + (fadeSpeed ) * Time.deltaTime;
                fadeAmount += (fadeSpeed / 2) * Time.deltaTime;
                yield return null;
            }

            if (arriveFunc != null)
            {
                arriveFunc();
            }
            Debug.Log("teleport");
            isTeleporting = false;
            if (systemUIBlocker != null)
                systemUIBlocker.gameObject.SetActive(false);
        }

        IEnumerator DelayTeleportPlayer(Vector3 Position, Vector3 Rotation, Action arriveFunc = null)
        {
            if (systemUIBlocker != null)
                systemUIBlocker.gameObject.SetActive(true);
            isTeleporting = true;
            imageTransition.color = new Color(0, 0, 0, 0);
            while (imageTransition.color.a < 1)
            {
                //fadeAmount = imageTransition.color.a + (fadeSpeed/3 * Time.deltaTime);
                fadeAmount += (fadeSpeed / 2 * Time.deltaTime);
                imageTransition.color = new Color(0, 0, 0, fadeAmount);
                yield return null;
            }

            yield return new WaitForSeconds(0.1f);

            player.transform.position = Position;
            player.transform.rotation = Quaternion.Euler(0f, Rotation.y, 0f);
            yield return new WaitForSeconds(0.1f);
            if (arriveFunc != null)
                arriveFunc();

            while (imageTransition.color.a > 0)
            {
                //fadeAmount = imageTransition.color.a - (fadeSpeed/3 * Time.deltaTime);
                fadeAmount -= (fadeSpeed * Time.deltaTime);
                imageTransition.color = new Color(0, 0, 0, fadeAmount);
                yield return null;
            }
            isTeleporting = false;
            if (systemUIBlocker != null)
                systemUIBlocker.gameObject.SetActive(false);
        }

        void OpenConfirmationPanel(ConfirmationPanelEvent e)
        {
            StartCoroutine(DelayConfirmationLayer());
        }

        void CloseConfirmationPanel(CloseConfirmationPanelEvent e)
        {
            StartCoroutine(CloseConfirmationLayerDelay());
        }

        IEnumerator DelayConfirmationLayer()
        {
            fadeAmount = 0;
            imageTransition.color = new Color(0, 0, 0, 0);
            while (imageTransition.color.a < .9f)
            {
                //fadeAmount = imageTransition.color.a + (fadeSpeed/3 * Time.deltaTime);
                fadeAmount += (fadeSpeed / 2 * Time.deltaTime);
                imageTransition.color = new Color(0, 0, 0, fadeAmount);
                yield return null;
            }

            yield return new WaitForSeconds(0.1f);
        }

        IEnumerator CloseConfirmationLayerDelay()
        {
            fadeAmount = .9f;
            imageTransition.color = new Color(0, 0, 0, 0);
            while (imageTransition.color.a > 0)
            {
                //fadeAmount = imageTransition.color.a + (fadeSpeed/3 * Time.deltaTime);
                fadeAmount -= (fadeSpeed / 2 * Time.deltaTime);
                imageTransition.color = new Color(0, 0, 0, fadeAmount);
                yield return null;
            }

            yield return new WaitForSeconds(0.1f);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using ViveHandTracking;
using EasyCurvedLine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Valve.VR;
namespace VirtualTraining.VR
{
    public class LaserVR : MonoBehaviour
    {
        [SerializeField] GameObject laserVR;
        [SerializeField] GameObject teleportPoint;
        [SerializeField] GameObject handLaser;
        [SerializeField] TeleportPlayer teleport;
        [SerializeField] bool isOn;
        [SerializeField] LineRenderer lineRenderer;
        [SerializeField] LineRenderer lineRendererTeleport;
        [SerializeField] GameObject handTransformStart;
        Vector2 startPositionPanel;

        Vector3[] positionsTemp;
        [SerializeField] LayerMask layer;
        int fingerId = -1;
        [SerializeField] VRPlayerManager vrPlayerManager;
        [SerializeField] SteamVR_Action_Boolean trigger;
        public bool laserStatus
        {
            set
            {
                isOn = value;
            }
            get => isOn;
        }

        public void LaserVRSetup(GameObject handLaser, LineRenderer lineRenderer, GameObject handTransformStart)
        {
            this.handLaser = handLaser;
            this.lineRenderer = lineRenderer;
            this.handTransformStart = handTransformStart;
            if (vrPlayerManager.IsHandTracking == false)
                OnStateChanged(1);
        }

        public void OnStateChanged(int state)
        {
            if (state == 1)
            {
                isOn = !isOn;
                if (laserVR != null)
                {
                    laserVR.SetActive(isOn);
                    if (lineRenderer == null)
                        lineRenderer = laserVR.GetComponent<LineRenderer>();
                }

                if (lineRenderer.enabled != isOn)
                    lineRenderer.enabled = isOn;

                if (!isOn && teleportPoint.activeInHierarchy)
                    SetUnactiveTeleport();
            }
        }

        private void Update()
        {
            if (isOn && handLaser != null)
            {
                Ray ray = new Ray(handLaser.transform.position, handLaser.transform.forward);
                RaycastHit hitData;

                if (Physics.Raycast(ray, out hitData, layer))
                {
                    //Debug.Log(hitData.transform.gameObject.name);
                    if (hitData.transform.gameObject != null)
                    {
                        if (hitData.collider.gameObject.layer == LayerMask.NameToLayer("UI"))
                        {
                            ResetTeleport(true);
                            //Debug.Log("Touched a UI");
                            return;
                        }

                        if (hitData.collider.gameObject.layer == LayerMask.NameToLayer("Floor"))
                        {
                            lineRenderer.enabled = false;
                            lineRendererTeleport.enabled = true;
                            if (vrPlayerManager.IsHandTracking == false)
                            {
                                vrPlayerManager.laserControllerObject.SetActive(false);
                            }
                            Vector3 first = handTransformStart.transform.position;
                            Vector3 second = new Vector3(hitData.point.x, handTransformStart.transform.position.y - 0.3f, hitData.point.z);
                            Vector3 third = hitData.point;

                            positionsTemp = new Vector3[] { first, second, third };
                            positionsTemp = LineSmoother.SmoothLine(positionsTemp, 0.2f);

                            lineRendererTeleport.positionCount = positionsTemp.Length;
                            lineRendererTeleport.SetPositions(positionsTemp);

                            SetTeleportPoint(hitData.point);

                            if (trigger.stateDown) //for controller
                                TeleportPlayer(1);
                        }
                        else
                        {
                            lineRenderer.enabled = true;
                            lineRendererTeleport.enabled = false;
                            SetUnactiveTeleport();
                            if (vrPlayerManager.IsHandTracking == false)
                            {
                                vrPlayerManager.laserControllerObject.SetActive(true);
                            }
                        }
                    }
                    else
                        ResetTeleport(true);
                }
                else
                    ResetTeleport(true);
            }
            else
                ResetTeleport(false);

        }

        public void ResetTeleport(bool isLineActive)
        {
            lineRenderer.enabled = isLineActive;
            lineRendererTeleport.enabled = false;
            SetUnactiveTeleport();
            if (vrPlayerManager.IsHandTracking == false)
            {
                vrPlayerManager.laserControllerObject.SetActive(isLineActive);
            }
        }

        public void SetTeleportPoint(Vector3 position)
        {
            if (teleportPoint != null)
            {
                if (!teleportPoint.activeInHierarchy)
                    teleportPoint.SetActive(true);
                teleportPoint.transform.position = position;
            }
        }

        public void SetUnactiveTeleport()
        {
            if (teleportPoint != null && teleportPoint.activeInHierarchy)
            {
                teleportPoint.SetActive(false);
                lineRenderer.enabled = false;
                lineRendererTeleport.enabled = false;
            }
        }

        public void TeleportPlayer(int state)
        {
            if (state == 1)
            {
                if (teleportPoint.activeInHierarchy)
                {
                    Debug.Log("teleporting !");
                    teleport.TeleportPlayerVR(teleportPoint.transform.position, teleportPoint.transform.rotation.eulerAngles);
                }
            }
        }

    }
}

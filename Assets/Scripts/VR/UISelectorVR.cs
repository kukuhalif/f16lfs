using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using System;
using VirtualTraining.UI;
using VirtualTraining.UI.VR;
using VirtualTraining.Feature.VR;
using Valve.VR;

namespace VirtualTraining.VR
{
    public class UISelectorVR : MonoBehaviour
    {
        public PlayerControllerVR playerControllerVR;
        public VRObjectSelector vrObjectSelector;
        public SteamVR_Action_Boolean triggerActive;
        public SteamVR_Action_Boolean triggerActiveLeft;
        public bool isClicked;

        bool isZoomingIn;
        bool isZoomingOut;

        bool isDragUI;

        public WorldSpacePanel worldSpace;

        Vector3 startPosition;
        Vector3 startPositionPanel;

        [SerializeField] float zoomSpeed = 0.05f;

        Vector3 deltaPosition;
        Vector3 tempPosition;
        Vector3 playerStartPosition;

        [SerializeField] SteamVR_Action_Vector2 zoomAxis;
        bool isActiveMenu;
        private void Start()
        {
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }
        private void ApplySettingListener(ApplySettingEvent e)
        {
            zoomSpeed = e.data.zoomSpeed / 200;
        }

        private void Update()
        {
            if (isZoomingIn || zoomAxis.axis.y < -0.5)
            {
                Debug.Log("zoom in");
                VirtualTrainingCamera.DistanceVR -= zoomSpeed;
            }
            else if (isZoomingOut || zoomAxis.axis.y > 0.5)
                VirtualTrainingCamera.DistanceVR += zoomSpeed;

            if (isDragUI)
            {
                if (worldSpace != null)
                {
                    Vector3 deltaPositionPlayer = playerStartPosition - this.transform.position;
                    float x = Vector3.Distance(VirtualTrainingCamera.handVR.position + deltaPositionPlayer, startPosition) * VirtualTrainingCamera.DistanceVR;
                    tempPosition = VirtualTrainingCamera.handVR.position + (VirtualTrainingCamera.handVR.forward * x);
                    Vector3 deltaPositionPanel = startPositionPanel - startPosition;
                    deltaPosition = tempPosition + deltaPositionPanel;
                    worldSpace.DragUI(deltaPosition);
                }
            }

            if (triggerActive.stateDown)
            {
                isClicked = false;
                Debug.Log("trigger");
                ClickPlayer(1, true);
                EventManager.TriggerEvent(new RightHandTriggerEvent(triggerActive.stateDown));
            }

            if (triggerActive.stateUp)
            {
                ClickPlayer(0, false);
            }

            if (triggerActiveLeft.stateDown)
            {
                playerControllerVR.mainMenuContainer.SetActive(isActiveMenu);
                isActiveMenu = !isActiveMenu;
            }

            if (playerControllerVR.laserVR.laserStatus && vrObjectSelector != null)
            {
                vrObjectSelector.VRHoverObject();
            }
        }

        public void ClickPlayer(int state, bool triggered = false)
        {
            if ((state == 1 && playerControllerVR.laserVR.laserStatus) || triggered == true)
            {
                if (isClicked == false)
                {
                    RaycastHit hitInfo = new RaycastHit();
                    if (VirtualTrainingCamera.Raycast(ref hitInfo))
                    {
                        if (hitInfo.transform.gameObject.tag == "UI")
                        {
                            VirtualTrainingCamera.DistanceVR = 1;
                            Debug.Log("uii masuk");
                            HeaderPanelVR header = hitInfo.transform.GetComponent<HeaderPanelVR>();
                            if (header != null)
                            {
                                Debug.Log("headerr masuk");
                                worldSpace = header.worldSpacePanel;
                                startPosition = hitInfo.point;
                                isDragUI = true;
                                playerControllerVR.isDragUI = true;
                                startPositionPanel = worldSpace.cameraOrbit.transform.position;
                                playerStartPosition = this.transform.position;
                            }
                        }
                    }

                    if (hitInfo.transform != null)
                    {
                        playerControllerVR.isDragUI = true;
                        vrObjectSelector.VRStartSelection();
                    }
                    else
                    {
                        vrObjectSelector.ClearSelection();
                    }

                    if (VirtualTrainingCamera.IsClickVR == false)
                        VirtualTrainingCamera.IsClickVR = true;

                    isClicked = true;
                }
            }
            else
            {
                if (isClicked == true)
                {
                    Debug.Log("end click");
                    VirtualTrainingCamera.IsClickVR = false;
                    vrObjectSelector.VREndSelection();
                    isClicked = false;
                    isDragUI = false;
                    playerControllerVR.isDragUI = false;
                    worldSpace = null;
                }
            }
        }

        public void ZoomIn(int state)
        {
            if (state == 1)
            {
                isZoomingIn = true;
                Debug.Log("zoom in");
            }
            else
                isZoomingIn = false;
        }

        public void ZoomOut(int state)
        {
            if (state == 1)
            {
                isZoomingOut = true;
                Debug.Log("zoom out");
            }
            else
                isZoomingOut = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Management;
using VirtualTraining.Core;
using VirtualTraining.SceneManagement;

public class TestVPTVR : MonoBehaviour
{

    [SerializeField] GameObject vrPlayer;
    [SerializeField] GameObject desktopCamera;

    private void Start()
    {
        //vrPlayer.gameObject.SetActive(false);
        //desktopCamera.gameObject.SetActive(true);
        StartCoroutine(StartXR());
    }

    public void VRView()
    {
        StartCoroutine(StartXR());
    }

    public void DesktopView()
    {
        StopXR();
        vrPlayer.SetActive(false);
        VirtualTrainingCamera.CurrentCamera.GetComponent<Camera>().enabled = true;
        VirtualTrainingSceneManager.CloseVRHead();
        desktopCamera.SetActive(true);
    }

    public IEnumerator StartXR()
    {
        //yield return new WaitForSeconds(1);
        Debug.Log("Initializing XR...");
        yield return XRGeneralSettings.Instance.Manager.InitializeLoader();

        if (XRGeneralSettings.Instance.Manager.activeLoader == null)
        {
            Debug.Log("Initializing XR Failed. Directing to Normal Interaciton Mode...!.");
            StopXR();
        }
        else
        {
            Debug.Log("Initialization Finished.Starting XR Subsystems...");

            //Try to start all subsystems and check if they were all successfully started ( thus HMD prepared).
            bool loaderSuccess = XRGeneralSettings.Instance.Manager.activeLoader.Start();
            XRGeneralSettings.Instance.Manager.StartSubsystems();
            Debug.Log(XRGeneralSettings.Instance.Manager.activeLoader.name);

            if (isHardwarePresent() == false)
            {
                loaderSuccess = false;
                yield return null;
            }
            Debug.Log(isHardwarePresent());
            if (loaderSuccess)
            {
                if (isHardwarePresent() == false)
                {
                    XRGeneralSettings.Instance.Manager.activeLoader.Stop();
                }
                else
                {
                    yield return null;
                    //desktopCamera.SetActive(false);
                    VirtualTrainingCamera.CurrentCamera.GetComponent<Camera>().enabled = false;
                    vrPlayer.SetActive(true);
                }
            }
            else
            {
                Debug.LogError("Starting Subsystems Failed. Directing to Normal Interaciton Mode...!");
                StopXR();
            }
        }
    }
    public bool isHardwarePresent()
    {
        var xrDisplaySubsystems = new List<XRDisplaySubsystem>();
        SubsystemManager.GetInstances<XRDisplaySubsystem>(xrDisplaySubsystems);
        foreach (var xrDisplay in xrDisplaySubsystems)
        {
            if (xrDisplay.running)
            {
                xrDisplay.Start();
                return true;
            }
        }
        return false;
    }
    void StopXR()
    {
        if (XRGeneralSettings.Instance.Manager.activeLoader != null)
        {
            XRGeneralSettings.Instance.Manager.activeLoader.Stop();
            Debug.Log("XR stopped completely.");
            //XRGeneralSettings.Instance.Manager.activeLoader.Stop();
            //XRGeneralSettings.Instance.Manager.StopSubsystems();
            //XRGeneralSettings.Instance.Manager.DeinitializeLoader();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.VR
{
    public class MainMenuVRController : MonoBehaviour
    {
        [SerializeField] GameObject touchCanvas1;

        [SerializeField] float heightHead;
        [SerializeField] float distanceToPlayer;
        [SerializeField] float distanceToPlayerX;
        [SerializeField] float speedLerpMenu;

        public PlayerControllerVR playerVR;
        Vector3 positionTemp;
        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(DelayStart());
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

            speedLerpMenu = DatabaseManager.GetDefaultSetting().speedLerpMenuVR;
            heightHead = DatabaseManager.GetDefaultSetting().vrMenuHeight / 25;
            distanceToPlayer = (DatabaseManager.GetDefaultSetting().vrMenuHeight / 100) * -1;
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            speedLerpMenu = e.data.speedLerpMenuVR;
            heightHead = e.data.vrMenuHeight / 10f;
            distanceToPlayer = (e.data.vrCanvasDistance / 10) * -1;
        }

        IEnumerator DelayStart()
        {
            yield return new WaitForSeconds(.5f);

            //SteamVR.Initialize(true);
            yield return new WaitForSeconds(1);
            if (touchCanvas1 != null)
                touchCanvas1.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 eulersTemp = new Vector3(touchCanvas1.transform.eulerAngles.x, playerVR.vrCamera.transform.eulerAngles.y, touchCanvas1.transform.eulerAngles.z);
            touchCanvas1.transform.rotation = Quaternion.Slerp(Quaternion.Euler(touchCanvas1.transform.eulerAngles), Quaternion.Euler(eulersTemp), Time.deltaTime * speedLerpMenu);


            positionTemp = new Vector3(playerVR.vrCamera.transform.localPosition.x - distanceToPlayerX, playerVR.vrCamera.transform.localPosition.y - heightHead,
                playerVR.vrCamera.transform.localPosition.z - distanceToPlayer);
            touchCanvas1.transform.localPosition = Vector3.Lerp(touchCanvas1.transform.localPosition, positionTemp, Time.deltaTime * speedLerpMenu);
        }
    }
}


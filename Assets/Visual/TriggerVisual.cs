using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TriggerVisual : MonoBehaviour
{
    public FormulaParameter formulaParameter;

    private void OnEnable()
    {
        EventBus.Trigger(FormulaEvent.Calculate, formulaParameter);
    }
}

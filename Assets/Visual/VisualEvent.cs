using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

//Register a string name for Formula Event to hook it to an Event. You can save this class in a separate file and add multiple Events to it as public static strings.
public static class FormulaEvent
{
    public static string Calculate = "Calculate";
    public static string Result = "Result";
}

[System.Serializable]
public class ParameterData
{
    public string name;
    public float value;
}

//Parameter data
[IncludeInSettings(true)]
[System.Serializable]
public class FormulaParameter
{
    [SerializeField]
    //public List<ParameterData> parameters { get; private set; }
    public List<ParameterData> parameters;
}
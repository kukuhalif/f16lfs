using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ReceiveVisual : MonoBehaviour
{
    void Start()
    {
        EventBus.Register<float>(FormulaEvent.Result, FormulaResultListener);
    }

    private void FormulaResultListener(float result)
    {
        Debug.Log("result : " + result);
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ParameterNode : Unit
{
    [DoNotSerialize] // No need to serialize ports
    public ValueInput formulaParameter; // Adding the ValueInput variable for FormulaParameter

    [DoNotSerialize] // No need to serialize ports
    public ValueInput name; // Adding the ValueInput variable for parameter name

    [DoNotSerialize] // No need to serialize ports
    public ValueOutput value; // Adding the ValueOutput variable for parameter value

    protected override void Definition()
    {
        //Making the name input value port visible, setting the port label name to formula parameter
        formulaParameter = ValueInput<FormulaParameter>("formula parameter");

        //Making the name input value port visible, setting the port label name to name and setting its default value to empty string.
        name = ValueInput<string>("name", "");

        //Making the result output value port visible, setting the port label name to value and get value from formula parameter data
        value = ValueOutput<float>("value", (flow) =>
        {
            var fp = flow.GetValue<FormulaParameter>(formulaParameter);
            var n = flow.GetValue<string>(name);

            for (int i = 0; i < fp.parameters.Count; i++)
            {
                if (n.ToLower() == fp.parameters[i].name.ToLower())
                    return fp.parameters[i].value;
            }

            return 0f;
        });
    }
}

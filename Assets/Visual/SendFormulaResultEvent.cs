using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[UnitTitle("Send Formula Result")]//Custom Unit to send the event
[UnitCategory("Events\\FormulaVisualEvent")]//Setting the path to find the unit in the fuzzy finder in Events > My Events.
public class SendFormulaResultEvent : Unit
{
    [DoNotSerialize]// Mandatory attribute, to make sure we don�t serialize data that should never be serialized.
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }
    [DoNotSerialize]
    public ValueInput myValue;
    [DoNotSerialize]
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition()
    {

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        myValue = ValueInput<float>(nameof(myValue), 0);
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow)
    {
        EventBus.Trigger(FormulaEvent.Result, flow.GetValue<float>(myValue));
        return outputTrigger;
    }
}
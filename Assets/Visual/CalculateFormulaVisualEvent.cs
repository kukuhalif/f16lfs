using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[UnitTitle("On Calculate Formula Event")]//The Custom Scripting Event node to receive the Event. Add "On" to the node title as an Event naming convention.
[UnitCategory("Events\\FormulaVisualEvent")]//Set the path to find the node in the fuzzy finder as Events > My Events.
public class CalculateFormulaVisualEvent : EventUnit<FormulaParameter>
{
    [DoNotSerialize]// No need to serialize ports.
    public ValueOutput parameter { get; private set; }// The Event output data to return when the Event is triggered.
    protected override bool register => true;

    // Add an EventHook with the name of the Event to the list of Visual Scripting Events.
    public override EventHook GetHook(GraphReference reference)
    {
        return new EventHook(FormulaEvent.Calculate);
    }
    protected override void Definition()
    {
        base.Definition();
        // Setting the value on our port.
        parameter = ValueOutput<FormulaParameter>(nameof(parameter));
    }
    // Setting the value on our port.
    protected override void AssignArguments(Flow flow, FormulaParameter data)
    {
        flow.SetValue(parameter, data);
    }
}
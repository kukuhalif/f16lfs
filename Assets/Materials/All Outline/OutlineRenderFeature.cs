using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class OutlineRenderFeature : ScriptableRendererFeature
{
    [System.Serializable]
    public class OutlineSettings
    {
        public RenderPassEvent WhenToInsert;
        public Material MaterialToBlit;
    }

    public OutlineSettings settings = new OutlineSettings();

    OutlineRenderPass outlineRenderPass;

    public override void Create()
    {
        outlineRenderPass = new OutlineRenderPass("Outline Render Pass", settings.WhenToInsert, settings.MaterialToBlit);
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        var cameraColorTargetIdent = renderer.cameraColorTarget;
        outlineRenderPass.Setup(cameraColorTargetIdent);

        renderer.EnqueuePass(outlineRenderPass);
    }
}
